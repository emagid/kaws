<?php if (count($model->users)>0) { ?>
<div class="box box-table">
<table class="table">
  <thead>
    <tr>
      <th>Email</th>
      <th>Name</th>
      <th>Edit</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($model->users as $obj) { ?>
    <tr>
      <td>
      	 <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $obj->id; ?>">
           <?php echo $obj->email;?>
         </a>
      </td>
      <td>
      	<a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $obj->id; ?>">
           <?php echo $obj->full_name();?>
        </a>
      </td>
      <td>
         <a class="btn-actions" href="<?php echo ADMIN_URL; ?>users/update/<?php echo $obj->id; ?>">
           <i class="icon-pencil"></i> 
         </a>
      </td>
      <td class="text-center">
          <a class="btn-actions"
             href="<?php echo ADMIN_URL; ?>users/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
             onClick="return confirm('Are You Sure?');">
              <i class="icon-cancel-circled"></i>
          </a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
  
</table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>

<?php footer();?>

<script>
	var site_url = '<?= ADMIN_URL.'users';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
