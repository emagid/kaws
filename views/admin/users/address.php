<?php
$states = get_states();
unset($states['GU']);unset($states['PR']);unset($states['VI']);
?>
<form method="post" action="<?php echo ADMIN_URL."users/address";?>" id="address-form">
  <input type="hidden" name="id" value="<?php echo $model->address->id;?>" />
  <input type="hidden" name="user_id" value="<?php echo $model->user->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
      <div class="box">
        <h4>Edit Address</h4>
        <div class="form-group">
          <label>Label</label>
          <?php echo $model->form->editorFor("label");?>
        </div>
        <div class="form-group">
          <label>First Name</label>
          <?php echo $model->form->editorFor("first_name");?>
        </div>
         <div class="form-group">
          <label>Last Name</label>
          <?php echo $model->form->editorFor("last_name");?>
        </div>
         <div class="form-group">
          <label>Phone Number</label>
          <?php echo $model->form->textBoxFor("phone",['class'=>'phone-us']);?>
        </div>
         <div class="form-group">
          <label>Address</label>
          <?php echo $model->form->editorFor("address");?>
        </div>
         <div class="form-group">
          <label>Address 2</label>
          <?php echo $model->form->editorFor("address2");?>
        </div>
         <div class="form-group">
          <label>Zip Code</label>
          <?php echo $model->form->editorFor("zip");?>
        </div>
         <div class="form-group">
          <label>City</label>
           <?php echo $model->form->editorFor("city");?>
        </div>
        
        <div class="form-group">
          <label>State</label>
          <?php echo $model->form->dropDownListFor("state",$states);?>
        </div>
        
         <div class="form-group">
          <label>Country</label>
          <?php echo $model->form->dropDownListFor("country", get_countries());?>
        </div>
        
        <div class="form-group">
          <button type="submit" class="btn btn-save">Save</button>
        </div>
      </div>
    </div>
  </div>
</form>
<?php footer();?>
<script type="text/javascript">
  
    jQuery.validator.addMethod("validate_phone",function(value,element) {
    var patt = new RegExp(/^\(\d{3}\)\d{3}-\d{4}$/);
    if(value!=="" && !patt.test(value)) {
      return false;
    } else {return true;}
  },"invalid phone number format.");
  
  $("#address-form").validate({
  onkeyup: false,
  onfocusout: false,
  onclick: false,
  focusInvalid :false,
  rules: {
    address: {required:true},
    zip: {required:true},
    state: {required:true},
    phone: {validate_phone:true},
    country: {required:true},
    city: {required:true},
  },
});

$(".phone-us").inputmask("(999)999-9999");
</script>
