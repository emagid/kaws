<style type="text/css">
    @media print {
        .top-bar, .site-nav, .banner.cms, footer .container, h2, .order_status, h3, .form {
            display: none;
        }

        .ordered_events img {
            width: 50px;
            height: 50px;
        }

        .PrintOnly {
            display: block;
            margin: 0 auto;
            width: 199px;
            height: 71px;
        }

        a[href^="/"]:after {
            content: " ";
        }

    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->event->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("title"); ?>
                </div>
                <div class="form-group">
                    <label>Icon Image</label>
                    <p><input type="file" name="icon" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->event->icon != "" && file_exists(UPLOAD_PATH . 'events' . DS . $model->event->icon)) {
                            $img_path = UPLOAD_URL . 'events/' . $model->event->icon;
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'events/delete_image/' . $model->event->id; ?>?icon=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="icon"
                                       value="<?= $model->event->icon ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Date Range</label>
                    <input type="text" name="daterange" value="<?php echo ($model->event->id > 0) ? date("m/d/Y g:iA", strtotime($model->event->start_date)) . ' - ' . date("m/d/Y g:iA", strtotime($model->event->end_date)) : ""; ?>"/>
                </div>

                <div class="form-group">
                    <label>Discount Type</label>
                    <select name="discount_type" class="form-control">
                        <?php foreach (\Model\Event::$discount_type as $key => $val) {
                            $select = ($model->event->discount_type == $key) ? " selected='selected'" : "";
                            ?>
                            <option value="<?php echo $key; ?>"<?php echo $select; ?>><?php echo $val; ?></option>
                        <?php } ?>
                    </select>

                </div>
                <div class="form-group">
                    <label>Discount Amount</label>
                    <?php echo $model->form->textBoxFor("discount_amount", ['class'=>"numeric"]); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
    <button class="btn btn-save" id="print">Print</button>
</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'events/');?>;
    $(document).ready(function () {
        $(function () {
            $('input[name="daterange"]').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mmA',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                showDropdowns: true,
                <?php if($model->event->id > 0) { ?>
                startDate: "<?php echo date("m/d/Y g:iA",strtotime($model->event->start_date));?>",
                endDate: "<?php echo date("m/d/Y g:iA",strtotime($model->event->end_date));?>",
                <?php } ?>

            });
        });

        $("input.image").change(function () {
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".currency-us").autoNumeric('init', {aSign: '$'});
        $(".numeric").numericInput();
    });
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>