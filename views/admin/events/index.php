<?php if (count($model->events) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="4%"></th>
                <th width="20%">Title</th>
                <th width="10%">Discount Amount</th>
                <th width="10%">Discount Type</th>
                <th width="10%">Start Date</th>
                <th width="10%">End Date</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->events as $obj) { ?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>events/update/<?php echo $obj->id; ?>"><img
                                src="<?= $obj->getIcon(); ?>"/></a></td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>events/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td><?php echo $obj->discount_amount; ?></td>
                    <td><?php echo \Model\Event::$discount_type[$obj->discount_type]; ?></td>
                    <td><?php echo date("M d, Y g:iA", strtotime($obj->start_date)); ?></td>
                    <td><?php echo date("M d, Y g:iA", strtotime($obj->end_date)); ?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>events/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>events/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'events';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

