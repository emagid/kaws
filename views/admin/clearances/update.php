<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->clearance->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
<!--            <li role="presentation"><a href="#variant-tab" aria-controls="general" role="tab" data-toggle="tab">Variants</a></li>-->
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Product</label>
                                <select name="product_id" class="multiselect">
                                    <? foreach ($model->products as $product) { ?>
                                        <option
                                            value="<?= $product->id ?>" <?= $product->id == $model->product->id ? 'selected' : '' ?>><?= $product->name ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <!--
                            <div class="form-group">
                                <label>Size</label>
                                <select name="size_id[]" class="multiselect" multiple>
                                    <? foreach ($model->sizes as $size) { ?>
                                        <option
                                            value="<?= $size->id ?>" <?= isset($model->size_id) && in_array($size->id, $model->size_id) ? 'selected' : '' ?>><?= $size->us_size ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Color</label>
                                <select name="color_id[]" class="multiselect" multiple>
                                    <? foreach ($model->colors as $color) { ?>
                                        <option
                                            value="<?= $color->id ?>" <?= isset($model->color_id) && in_array($color->id, $model->color_id) ? 'selected' : '' ?>><?= $color->name ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            -->
                            <div class="form-group">
                                <label>Price</label>
                                <?php echo $model->form->textBoxFor("price"); ?>
                            </div>
                            <div class="form-group">
                                <label>Start Date</label>
                                <?php echo $model->form->textBoxFor("start_date",['class'=>'datetime']); ?>
                            </div>
                            <div class="form-group">
                                <label>End Date</label>
                                <?php echo $model->form->textBoxFor("end_date",['class'=>'datetime']); ?>
                            </div>
                            <? if ($model->clearance->id) { ?>
                                <div class="form-group">
                                    <label>Current Quantity</label>
                                    <?php echo $model->form->editorFor("current_quantity"); ?>
                                </div>
                                <div class="form-group">
                                    <label>Starting Quantity</label>
                                    <?php echo $model->form->textBoxFor("starting_quantity", ['type' => 'hidden']); ?>
                                    <div><?= $model->clearance->starting_quantity ?></div>
                                </div>
                            <? } else { ?>
                                <div class="form-group">
                                    <label>Starting Quantity
                                        <small>(Unchangeable)</small>
                                    </label>
                                    <?php echo $model->form->textBoxFor("starting_quantity", ['class' => 'numeric', 'value' => 1]); ?>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!--Find later use for this code-->
            <!--<div role="tabpanel" class="tab-pane" id="variant-tab">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            <h4>Variants</h4>
                            <div class="form-group">
                                <?/*$c = 0;
                                foreach($model->clearance_details as $clearance_detail) {
                                    $color = \Model\Color::getItem($clearance_detail->color_id);
                                    $size = \Model\Size::getItem($clearance_detail->size_id)*/?>
                                    <?/*if($c != $clearance_detail->color_id){$c = $clearance_detail->color_id; echo "<h1>$color->name</h1><br>";}*/?>
                                    <div class="variant-container" data-id="<?/*= $clearance_detail->id */?>">
                                        <div class="variant-word">
                                            <p style="cursor: pointer;" data-id="<?/*= $clearance_detail->id */?>" class="var-name"><?/*= $model->product->name*/?></p>
                                            <p style="cursor: pointer;" data-id="<?/*= $clearance_detail->id */?>" class="var-datetime"><?/*= date('Y-m-d',strtotime($clearance_detail->start_date)) .' - '. date('Y-m-d',strtotime($clearance_detail->end_date))*/?></p>
                                            <p style="cursor: pointer;" data-id="<?/*= $clearance_detail->id */?>" class="var-size"><?/*= 'US: '.$size->us_size.' EU: '.$size->eur_size*/?></p>
                                            <p style="cursor: pointer;" data-id="<?/*= $clearance_detail->id */?>" class="var-price"><?/*= '$'.number_format($clearance_detail->price,2) */?></p>
                                            <p style="cursor: pointer;" data-id="<?/*= $clearance_detail->id */?>" class="var-qty"><?/*= 'Stock: '.$clearance_detail->current_quantity.'/'.$clearance_detail->starting_quantity */?></p>
                                        </div>
                                        <div class="variant-details-container" style="display: none;" data-id="<?/*= $clearance_detail->id */?>"><div class="row"><div class="col-md-12">
                                            <label>Start Date</label><input name="update-start" type="text" class="datetime" value="<?/*= date('Y-m-d',strtotime($clearance_detail->start_date)) */?>"></div><div class="col-md-12">
                                            <label>End Date</label><input name="update-end" type="text" class="datetime" value="<?/*= date('Y-m-d',strtotime($clearance_detail->end_date)) */?>"></div></div>
                                            <label>Price</label><input name="update-price" type="text" class="numeric" value="<?/*= $clearance_detail->price */?>">
                                            <label>Quantity</label><input name="update-qty" type="text" class="numeric" value="<?/*= $clearance_detail->current_quantity */?>">
                                            <button class="save-variants">Save All</button>
                                        </div>
                                    </div>
                                    <hr/>
                                <?/* } */?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>
<link type="text/css" rel="stylesheet" href="<?=ADMIN_CSS.'jquery.datetimepicker.css'?>"/>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'clearances/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('.datetime').datetimepicker({
            format: 'Y-m-d',
            timepicker:false,
            scrollInput:false
        });
        $('[name=product_id]').on('change',function(){
            $.post('/admin/clearances/getAttributes',{id:$(this).val()},function(data){
                var json = $.parseJSON(data);
                if(json.status == 'success'){
                    var color_id = [];
                    $('[name="color_id[]"]').empty();
                    $.each(json.colors,function(i,e){
                        color_id.push(e.id);
                        $('[name="color_id[]"]').append('<option value="'+ e.id+'">'+ e.name+'</option>');
                    });
                    var size_id = [];
                    $('[name="size_id[]"]').empty();
                    $.each(json.sizes,function(i,e){
                        size_id.push(e.id);
                        $('[name="size_id[]"]').append('<option value="'+ e.id+'">'+ e.name+'</option>');
                    });

                    $('[name="color_id[]"]').val(color_id);
                    $('[name="size_id[]"]').val(size_id);
                    $("select.multiselect").multiselect("rebuild");
                }
            });
        });
        $(".numeric").numericInput();

        $("select.multiselect").each(function (i, e) {
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $('.save-variants').on('click',function(e){
            e.preventDefault();
            e.stopPropagation();
            var container = $(this).parent();
            var sib = container.siblings('.variant-word');
            var clearance = container.parent().attr('data-id');
            var price = container.find('input[name=update-price]').val();
            var qty = container.find('input[name=update-qty]').val();
            var start = container.find('input[name=update-start]').val();
            var end = container.find('input[name=update-end]').val();
            var data = {
                clearance:clearance,
                price:price,
                qty:qty,
                start:start,
                end:end
            };
            $.post('/admin/clearances/save_clearance',data,function(obj){
                var jObj = $.parseJSON(obj);
                sib.find('.var-datetime').html(jObj.daterange);
                sib.find('.var-price').html(jObj.price);
                sib.find('.var-qty').html(jObj.quantity);
                container.hide();
            });
        });

        $('.variant-word').on('click',function(){
            $(this).parent().find('.variant-details-container').toggle();
        });
    });
</script>