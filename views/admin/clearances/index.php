<div class="row">
    <div class="col-md-16">
        <div class="box search-box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id, Name or Slug"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
</div>
<?php if (count($model->clearances) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width="20%">Product</th>
<!--                <th width="20%">Color</th>-->
<!--                <th width="20%">Size</th>-->
                <th width="20%">Price</th>
                <th width="20%">Range</th>
                <th width="20%">Quantity Remaining</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->clearances as $obj) {
                $product = \Model\Product::getItem($obj->product_id)?>
                <tr class="originalColors">
                    <td><a href="<?php echo ADMIN_URL; ?>clearances/update/<?php echo $obj->id; ?>"><?php echo $product->name; ?></a></td>
<!--                    <td><a href="--><?php //echo ADMIN_URL; ?><!--clearances/update/--><?php //echo $obj->id; ?><!--">--><?php //echo implode('<br>',array_map(function($id){return \Model\Color::getItem($id)->name;},json_decode($obj->color_id,true))); ?><!--</a></td>-->
<!--                    <td><a href="--><?php //echo ADMIN_URL; ?><!--clearances/update/--><?php //echo $obj->id; ?><!--">--><?php //echo implode(', ',array_map(function($id){return number_format(\Model\Size::getItem($id)->us_size,1);},json_decode($obj->size_id,true))); ?><!--</a></td>-->
                    <td><a href="<?php echo ADMIN_URL; ?>clearances/update/<?php echo $obj->id; ?>">$<?php echo number_format($obj->price,2); ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>clearances/update/<?php echo $obj->id; ?>"><?php echo date('M d, Y',strtotime($obj->start_date)).' - '.date('M d, Y',strtotime($obj->end_date)); ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>clearances/update/<?php echo $obj->id; ?>"><?php echo $obj->current_quantity.'/'.$obj->starting_quantity; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>clearances/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black"
                           href="<?php echo ADMIN_URL; ?>clearances/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'clearances';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>clearances/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalColors').remove();
                    $('.paginationContent').hide();
                    $('.originalColors').hide();

                    var list = JSON.parse(data);
                    $.each(list,function(i,e){
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>clearances/update/' + e.id).html(e.product));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>clearances/update/' + e.id).html(e.color.join('<br>')));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>clearances/update/' + e.id).html(e.size.join(',')));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>clearances/update/' + e.id).html('$'+e.price));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>clearances/update/' + e.id).html(e.range));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>clearances/update/' + e.id).html(e.qty));
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>clearances/update/' + e.id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>clearances/delete/' + e.id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    });

                });
            } else {
                $("#data-list tbody tr").not('.originalColors').remove();
                $('.paginationContent').show();
                $('.originalColors').show();
            }
        });
    })
</script>

