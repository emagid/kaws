<?php if (count($model->inquiry) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="1%">Image</th>
                <th width="5%">Name</th>
                <th width="1%">Budget</th>
                <th width="20%">Description</th>
                <th width="5%">Notes</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->inquiry as $obj) { ?>
                <tr>
                    <td>
                        <? $img_path = UPLOAD_URL . 'inquiry/' . $obj->featuredImage() ?>
                        <a href="<?php echo ADMIN_URL; ?>inquiry/update/<?php echo $obj->id; ?>"><img
                                src="<?php echo $img_path; ?>" width="50"/></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>inquiry/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>inquiry/update/<?php echo $obj->id; ?>"><?php echo '$'.number_format($obj->estimated_budget,2); ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>inquiry/update/<?php echo $obj->id; ?>"><?php echo $obj->description; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>inquiry/update/<?php echo $obj->id; ?>"><?php echo $obj->notes; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>inquiry/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black"
                           href="<?php echo ADMIN_URL; ?>inquiry/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'inquiry';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

