<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->inquiry->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <?php echo $model->form->editorFor("email"); ?>
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <?php echo $model->form->editorFor("phone"); ?>
                </div>
                <div class="form-group">
                    <label>Estimated Budget</label>
                    <?php echo $model->form->editorFor("estimated_budget"); ?>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreaFor("description",['rows'=>6]); ?>
                </div>
                <div class="form-group">
                    <label>Featured image</label>

                    <p>
                        <small>(ideal featured image size is 500 x 300)</small>
                    </p>
                    <p><input type="file" name="featured_image" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->inquiry->featured_image != "" && file_exists(UPLOAD_PATH . 'inquiry' . DS . $model->inquiry->featured_image)) {
                            $img_path = UPLOAD_URL . 'inquiry/' . $model->inquiry->featured_image;
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'inquiry/delete_image/' . $model->inquiry->id; ?>?featured_image=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="featured_image"
                                       value="<?= $model->inquiry->featured_image ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <?if($model->inquiry->questionnaire){?>
                    <div class="form-group">
                        <label>Questionnaire</label>
                        <pre style="overflow-y: scroll; height: 20pc;"><?foreach(json_decode($model->inquiry->questionnaire,true) as $q=>$a){echo 'Q: '.str_replace('_',' ',$q).'<br>A: '.$a.'<br><br>';}?></pre>
                    </div>
                <?}?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <h4>Admin</h4>

                <div class="form-group">
                    <label>Status</label>
                    <?php echo $model->form->dropDownListFor("status",\Model\Inquiry::$status,'',['class'=>'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Notes</label>
                    <?php echo $model->form->textAreaFor("notes",['rows'=>6]); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>