<?$tab = isset($model->tab)? $model->tab:'products-tab';?>
<div class="row">
    <div class="col-md-8">
        <div class="box search-box">
            <div class="form-group">
                <label>Search</label>
                <div class="input-group">
                    <input id="search" type="text" name="search" class="form-control" placeholder="Search"/>
                    <span class="input-group-addon">
                        <i class="icon-search"></i>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label>Range</label>
                <div class="input-group">
                    <input id="range" type="text" name="range" class="form-control" value="<?= !$model->start || !$model->end ? '': date('Y-m-d', strtotime($model->start)) . ' - ' . date('Y-m-d', strtotime($model->end)) ?>"/>
                    <span class="input-group-addon remove-date" style="cursor: pointer;">
                        <i class="icon-cancel-circled"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="box transparent_box">
            <div class="form-group">
                <label>Show on page:</label>
                <select class="how_many form-control" name="how_many">
                    <? $arr = [50, 100, 500, 1000];
                    foreach ($arr as $a) {?>
                        <option value="<?= $a ?>" <?= isset($_GET['how_many']) && $_GET['how_many'] == $a ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Availability Filter</label>
                <select class="availability_filter form-control" name="availability_filter">
                    <option value="-1" <?= !isset($_GET['ava']) ? 'selected' : '' ?>>No Filter</option>
                    <? foreach (\Model\Product::$availability as $k=>$a) {?>
                        <option value="<?= $k ?>" <?= isset($_GET['ava']) && $_GET['ava'] == $k ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <!--<div class="form-group">
            <label>Type Filter</label>
            <select class="type_filter form-control" name="availability_filter">
                <option value="-1">No Filter</option>
                <?/* foreach (\Model\Product::$type as $k=>$a) {*/?>
                    <option value="<?/*= $k */?>" <?/*= (isset($_GET['type']) && $_GET['type'] == $k) || (!isset($_GET['type']) && $k == 1)? 'selected' : '' */?>><?/*= $a */?></option>
                <?/* } */?>
            </select>
        </div>-->
        <!--<div class="box transparent_box" style="align-content: center">
            <a href="<?/*= ADMIN_URL*/?>products/export" class="btn btn-warning" style="width: 49%">Export</a>
            <a class="btn btn-warning mass-email" style="width: 49%">Mass Email</a>
            <form action="<?/*= ADMIN_URL*/?>products/multi_update/" method="get">
                <a href="<?/*= ADMIN_URL*/?>products/google_product_feed" class="btn btn-warning" style="width: 49%;">Google Feed</a>
            </form>
        </div>-->
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        function getQueryObj(str) {
            return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
        }
        $('body').on('change', '.change', function () {
            var br = $(this).val();
            $("#ex").attr("href", "/admin/parts/export?brand=" + br);
            window.location.href = "/admin/parts?status_show=" + br;
        });

        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            <?if (isset($_GET['status_show'])){?>
            <?$br = $_GET['status_show'];?>
            window.location.href = '/admin/parts?status_show=<?=$br?>&how_many=' + how_many;
            <?}elseif(isset($_GET['search'])){?>
            <?$br = $_GET['search'];?>
            window.location.href = '/admin/parts?search=<?=$br?>&how_many=' + how_many;
            <?}else{?>
            window.location.href = '/admin/parts?how_many=' + how_many;
            <?}?>
        });

        $('body').on('change', '.availability_filter', function () {
            var availability_filter = $(this).val();
            var queryString;
            if(location.search && availability_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.ava = availability_filter;
            } else if(availability_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.ava;
            } else {
                queryString = {ava:availability_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('<?=$_SERVER['PATH_INFO']?>'+qs);
        });
        $('body').on('change', '.type_filter', function () {
            var type_filter = $(this).val();
            var queryString;
            if(location.search && type_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.type = type_filter;
            } else if(type_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.type;
            } else {
                queryString = {type:type_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('<?=$_SERVER['PATH_INFO']?>'+qs);
        });
        $('#range').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo $model->start ? : date('Y-m-d')?>",
            endDate: "<?php echo $model->end ? : date('Y-m-d')?>"
        },function(start,end){
            end.hour(23);end.minute(59);end.second(59);
            var queryString = getQueryObj(location.search);
            queryString.t = start.unix()+','+end.unix();
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            window.location.replace('<?=$_SERVER['PATH_INFO']?>'+'?'+ $.param(queryString));
        });
    });
</script>

<div role="tabpanel">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane <?=($tab == 'products-tab')?"active":""?>" id="products-tab">
            <?php if (count($model->products) > 0): ?>
                <div class="rox">
                    <div class="box box-table">
                        <table id="data-list" class="table">
                            <thead>
                            <tr>
                                <th width="5%"><input style="cursor:pointer;" name="edit-all" type="checkbox"/></th>
                                <th width="10%">Image</th>
                                <th width="10%">Image Alt-Description</th>
                                <th width="15%">Name</th>
            <!--                    <th width="21%">Alias</th>-->
                                <th width="10%">MSRP</th>
                                <th width="10%">Price</th>
                                <th width="10%">Type</th>
                                <th width="10%">Availability</th>
                                <!--                  	<th width="15%" class="text-center">Edit</th>-->
                                <!--                  	<th width="15%" class="text-center">Delete</th>	-->
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            foreach ($model->products as $obj) { ?>
                                <? if ($obj->kenjo == '1') { //TODO st-dev remove kenjo eventually
                                    $b = 'style="background-color:yellow;"';
                                } else {
                                    $b = "";
                                } ?>
                                <tr class="originalProducts" <?= $b ?>>

                                    <td><input style="cursor:pointer;" name="edit<?php echo $i++ ?>" type="checkbox"
                                               value="<?php echo $obj->id ?>"/></td>
                                    <td>
                                        <?$img_path = $obj->featuredImage()?>
                                        <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><img
                                                src="<?php echo $img_path; ?>" width="50"/></a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?php echo $obj->image_alt; ?></a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                                    </td>
            <!--                        <td>-->
            <!--                            <a href="--><?php //echo ADMIN_URL; ?><!--products/update/--><?php //echo $obj->id; ?><!--">--><?php //echo $obj->alias; ?><!--</a>-->
            <!--                        </td>-->
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?= number_format($obj->msrp, 2) ?></a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?= number_format($obj->price, 2) ?></a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?= \Model\Product::$type[$obj->type] ?></a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?= $obj->getAvailability() ?></a>
                                    </td>
                                    <!--                 <td class="text-center">-->
                                    <!--                   <a class="btn-actions" href="-->
                                    <?php //echo ADMIN_URL; ?><!--products/update/--><?php //echo $obj->id; ?><!--">-->
                                    <!--                   <i class="icon-pencil"></i> -->
                                    <!--                   </a>-->
                                    <!--                 </td>-->
                                    <!--                 <td class="text-center">-->
                                    <!--                   <a class="btn-actions" href="-->
                                    <?php //echo ADMIN_URL; ?><!--products/delete/--><?php //echo $obj->id; ?><!--?token_id=-->
                                    <?php //echo get_token();?><!--" onClick="return confirm('Are You Sure?');">-->
                                    <!--                     <i class="icon-cancel-circled"></i> -->
                                    <!--                   </a>-->
                                    <!--                 </td>-->
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <div class="box-footer clearfix">
                            <div class='paginationContent'></div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '/admin/parts?<?if (isset($_GET['status_show'])){echo "status_show=";echo $_GET['status_show'] ;echo"&";} if (isset($_GET['how_many'])){echo "how_many=";echo $_GET['how_many'] ;echo"&";}?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>
<script type="text/javascript">
    $(function () {

        $('input[name="edit-all"]').change(function () {
            if ($(this).prop('checked')) {
                $('#data-list tr').each(function () {
                    if ($(this).css('display') == 'table-row') {
                        $('input[type="checkbox"]', $(this)).prop('checked', 'checked');
                    }
                })
            } else {
                $('#data-list tr').each(function () {
                    $('input[type="checkbox"]', $(this)).prop('checked', false);
                })
            }
        })

        var timeout;
        var interval = 500;
        var search = $('#search');

        search.keyup(function(){
            clearTimeout(timeout);
            timeout = setTimeout(execute,interval);
        });
//        search.keydown(function(){
//            clearTimeout(timeout);
//        });

        function execute () {
            var keywords = search.val();
            var url = "<?php echo ADMIN_URL; ?>parts/search";
            if (keywords.length > 1) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();

                    var list = JSON.parse(data);

                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<input type="checkbox" style="cursor:pointer;" name="edit' + list[key].id + '" value="' + list[key].id + '" />'));
                        if (list[key].featured_image == '') {
                            var img = $('<img />').prop('width', 50).prop('src', "<?=ADMIN_IMG?>");/*TODO st-dev default product image*/
                        } else {
                            var img = $('<img />').prop('width', 50).prop('src', "" + list[key].featured_image);
                        }
                        $('<td />').appendTo(tr).html(img);
                        $('<td />').appendTo(tr).html('');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id).html(list[key].name));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//products/update/' + list[key].id).html(list[key].alias));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id).html(list[key].msrp));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id).html(list[key].price));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id).html(list[key].type));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id).html(list[key].availability));

//                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
//                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?//= ADMIN_URL;?>//products/update/' + list[key].id);
//                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
//                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
//                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?//= ADMIN_URL;?>//products/delete/' + list[key].id);
//                        deleteLink.click(function () {
//                            return confirm('Are You Sure?');
//                        });
//                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        }
    })
</script>
































