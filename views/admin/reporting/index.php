<!--<style type="text/css">
    header.header {
        display: none;
    }
</style>-->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-24 dashboardnotify">
            <div class="top_panels">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-usd fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">
                                    <div class="huge">$<?= number_format($model->total_sales, 2) ?></div>
                                    <div class="panel_text">
                                        <p>
                                            Total Sales<br/><br/>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= ADMIN_URL ?>orders">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-orange">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tags fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">

                                    <div class="huge">$<?= number_format($model->total_taxes, 2) ?>/<?=round($model->total_tax_percent,2)?:'0'?>% </div>
                                    <div class="panel_text">
                                        <p>Total taxes<br/>/(% of total)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= ADMIN_URL ?>orders">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-truck fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">

                                    <div class="huge">$<?= number_format($model->total_fees, 2) ?>/<?=round($model->total_ship_percent,2)?:'0'?>% </div>
                                    <div class="panel_text">
                                        <p>Total Shipping Fees <br/>/ (% of total)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= ADMIN_URL ?>orders">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-blue">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-cube fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">

                                    <div class="huge">$<?= number_format($model->avg,2)?>
                                    </div>
                                    <div class="panel_text">
                                        <p>Average Ticket Price (<?=$model->orders_count?> orders)<br/><br/></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?= ADMIN_URL ?>orders">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- top_panels -->
    <div class="row">
        <div class="col-md-8">
            <div class="box search-box">
                <div class="form-group">
                    <label>Date Range Interval</label>
                    <div class="input-group">
                        <input id="range" type="text" name="range" class="form-control" value="<?= !$model->start || !$model->end ? '': date('Y-m-d', strtotime($model->start)) . ' - ' . date('Y-m-d', strtotime($model->end)) ?>"/>
                        <span class="input-group-addon remove-date" style="cursor: pointer;">
                            <i class="icon-cancel-circled"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="box transparent_box">
                <div class="form-group">
                    <label>Group results by:</label>
                    <select class="group_by form-control" id="group">
                        <? $arr = ['day', 'week', 'month', 'year'];
                        foreach ($arr as $a) {?>
                            <option value="<?= $a ?>" <?= isset($_GET['group']) && $_GET['group'] == $a ? 'selected' : '' ?>><?= ucfirst($a) ?></option>
                        <? } ?>
                    </select>
                </div>
            </div>
            <div class="box transparent_box" style="align-content: center">
                <a id="filter" type="submit" name="range" class="btn btn-warning" >FILTER</a>
                <a href="/admin/reporting/index" class="btn btn-warning">CLEAR</a>
            </div>
        </div>
        <div class="col-md-8">
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>

    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="40%"><?=$model->group?></th>
                <th width="20%">Sales</th>
                <th width="20%">Authorize Sales</th>
                <th width="20%">Paypal Sales</th>
                <th width="20%">Tax Fees</th>
                <th width="20%">Shipping Fees</th>
                <th width="20%"># Orders</th>
            </tr>
            </thead>
            <? foreach($model->results['results'] as $result){?>
                <tr>
                    <td><?=$result[$model->group]?></td>
                    <td>$<?=number_format($result['total'])?></td>
                    <td>$<?=number_format($result['authorize'])?></td>
                    <td>$<?=number_format($result['paypal'])?></td>
                    <td>$<?=number_format($result['tax'])?></td>
                    <td>$<?=number_format($result['shipping_fees'])?></td>
                    <td><?=$result['count']?></td>
                </tr>
            <? } ?>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="7">Totals</th>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td>Revenue</td>
                    <td>$<?=number_format($model->results['total'])?></td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td>Paypal Sales</td>
                    <td>$<?=number_format($model->results['paypalTotal'])?></td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td>Authorize Sales</td>
                    <td>$<?=number_format($model->results['authTotal'])?></td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td>Tax</td>
                    <td>$<?=number_format($model->results['tax'])?></td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                    <td>Shipping Fees</td>
                    <td>$<?=number_format($model->results['shipping_fees'])?></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <?if(false){?>
        <div class="col-md-8 chartsales">
            <div class="panel panel-default" style="min-height: 390px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-money"></i> Monthly Sales</h3>
                </div>

                <!-- html executes chart javascript -->
                <div id="web-sales-daily" style="width: 100%">
                    <canvas id="D" height="400"></canvas>
                </div>

            </div>
        </div>
        <div class="col-md-8 chartgross">
            <div class="panel panel-default" style="min-height: 390px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-area-chart"></i> Weekly Sales</h3>
                </div>

                <div id="web-sales-weekly" style="width: 100%">
                    <canvas id="E" height="400"></canvas>
                </div>

            </div>
        </div>
        <div class="col-md-8 chartorders">
            <div class="panel panel-default" style="min-height: 390px;">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-truck"></i> Monthly Orders</h3>
                </div>

                <!-- html executes chart javascript -->
                <div id=web-sales-monthly" style="width: 100%">
                    <canvas id="F" height="400"></canvas>
                </div>
            </div>
        </div>
    <? } ?>

</div>
<script>
    var queryString = {<?=isset($_GET['start'])? "start: '{$_GET['start']}',":''?>
                       <?=isset($_GET['end'])? "end: '{$_GET['end']}',":''?>
                       group: "<?=$model->group?>" };
    $(document).ready(function() {
        function getQueryObj(str) {
            return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
        }
        $('#range').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo $model->start ?: date('Y-m-d')?>",
            endDate: "<?php echo $model->end ?: date('Y-m-d')?>"
        }, function (start, end) {
            console.log(start.month());
            console.log(end.month());
            var q_start  = ''+start.year()+'-'+(start.month()+1)+'-'+(start.date()+1);
            var q_end  = ''+end.year()+'-'+(end.month()+1)+'-'+(end.date()+1);
            queryString.start = q_start;
            queryString.end = q_end;
            $.each(queryString, function (i, e) {
                queryString[i] = decodeURIComponent(e);
            });
        });
        $('#filter').click(function(){
            window.location.replace('<?=$_SERVER['PATH_INFO']?>' + '?' + $.param(queryString));
        });
        $('#group').change(function(){
           queryString.group = this.value;
        });
    });
</script>

<?if(false){?>
<script>
//    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
//    var randomScalingFactor = function () {
//        return Math.round(Math.random() * 100)
//    };
//    var initChartOpt = {
//        type: 'bar',
//        data:{
//            labels: monthNames,
//            datasets:[{
//                label: 'Monthly Sales',
//                data: [12,42,232,12,42,1,3,1,323,2,12,42],
//                backgroundColor
//                }
//            ]
//        }
//    }
    var dataSet = <?=json_encode($model->graphsData)?>;
    var weeklySet = <?=json_encode($model->weeklySet)?>;

    var months = [];
    for (key in dataSet.previous) {
        var month = new Date(key.split('-')[0], key.split('-')[1] - 1);
        months.push(monthNames[month.getMonth()]);
    }

    var weeks = ['7 Weeks', '6 Weeks', '5 Weeks', '4 Weeks', '3 Weeks', '2 Weeks', 'Last Week'];

    var set = [[], []];
    for (key in dataSet.previous) {
        set[0].push(dataSet.previous[key].monthly_sales);
    }
    for (key in dataSet.current) {
        set[1].push(dataSet.current[key].monthly_sales);
    }
    var barChartData1 = {
        labels: months,
        datasets: [
            {
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: set[0]
            },
            {
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: set[1]
            }
        ]
    }


    var set = [[], []];
    for (key in weeklySet.previous) {
        set[0].push(weeklySet.previous[key].weekly_sales);
    }
    for (key in weeklySet.current) {
        set[1].push(weeklySet.current[key].weekly_sales);
    }

    var lineChartData = {
        labels: weeks,
        datasets: [
            {
                label: "Previous",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
//				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                data: set[0]
            },
            {
                label: "Current",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
//				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                data: set[1]
            }
        ]

    }

    set = [[], []];
    for (key in dataSet.previous) {
        set[0].push(dataSet.previous[key].monthly_orders);
    }
    for (key in dataSet.current) {
        set[1].push(dataSet.current[key].monthly_orders);
    }
    var barChartData2 = {
        labels: months,
        datasets: [
            {
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data: set[0]
            },
            {
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: set[1]
            }
        ]

    }

    window.onload = function () {
        var ctxB = document.getElementById("B").getContext("2d");
        window.myLine = new Chart(ctxB).Line(lineChartData, {
            responsive: true
        });
        var ctxA = document.getElementById("A").getContext("2d");
        window.myBar = new Chart(ctxA).Bar(barChartData1, {
            responsive: true
        });
        var ctxC = document.getElementById("C").getContext("2d");
        window.myBar = new Chart(ctxC).Bar(barChartData2, {
            responsive: true
        });
    }


</script>
<?}?>

<?php echo footer(); ?>