<?php if (count($model->gift_cards) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%">Name</th>
                <th width="10%">Coupon Code</th>
                <th width="10%">Amount</th>
                <th width="10%">Remaining</th>
                <th width="10%">Start Date</th>
                <th width="10%">End Date</th>
                <th width="5%" class="text-center">Edit</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->gift_cards as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>gift_cards/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td><?php echo $obj->code; ?></td>
                    <td><?php echo $obj->amount; ?></td>
                    <td><?php echo $obj->remaining; ?></td>
                    <td><?php echo date("m/d/Y g:iA", strtotime($obj->start_date)); ?></td>
                    <td><?php echo date("m/d/Y g:iA", strtotime($obj->end_date)); ?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>gift_cards/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>gift_cards/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php if (count($model->gift_obj) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%"></th>
                <th width="10%">Name</th>
                <th width="10%">Amount</th>
                <th width="5%" class="text-center">Edit</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->gift_obj as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>gift_cards/update/<?php echo $obj->id; ?>"><img width="100px" src="<?=UPLOAD_URL.'gift_cards/'.$obj->image?>"></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>gift_cards/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>gift_cards/update/<?php echo $obj->id; ?>"><?php echo $obj->amount; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>gift_cards/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>gift_cards/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'gift_cards';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

