<style type="text/css">
    .PrintOnly {
        display: none;
    }

    @media print {
        .top-bar, .site-nav, .banner.cms, footer .container, h2, .order_status, h3, .form {
            display: none;
        }

        .ordered_products img {
            width: 50px;
            height: 50px;
        }

        .PrintOnly {
            display: block;
            margin: 0 auto;
            width: 199px;
            height: 71px;
        }

        a[href^="/"]:after {
            content: " ";
        }

    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->gift_card->id; ?>"/>
    <input type="hidden" name="old_amount" value="<?php echo $model->gift_card->amount; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Type</label>
                    <?php echo $model->form->dropDownListFor("gift_card_type",\Model\Gift_Card::$type,'',['class'=>'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Featured image</label>
                    <p><input type="file" name="image" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->gift_card->image != "" && file_exists(UPLOAD_PATH . 'gift_cards' . DS . $model->gift_card->image)) {
                            $img_path = UPLOAD_URL . 'gift_cards/' . $model->gift_card->image;
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'gift_cards/delete_image/' . $model->gift_card->id; ?>?image=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="image"
                                       value="<?= $model->gift_card->image ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-22">
                        <div class="form-group">
                            <label>Gift Card Code</label>
                            <?php echo $model->form->editorFor("code"); ?>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label></label>
                            <button type="button" id="generate-token">Generate Unique Code</button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Date Range</label>
                    <input type="text" name="daterange"
                           value="<?php echo ($model->gift_card->id > 0) ? date("m/d/Y g:iA", strtotime($model->gift_card->start_date)) . ' - ' . date("m/d/Y g:iA", strtotime($model->gift_card->end_date)) : ""; ?>"/>
                </div>
                <div class="form-group">
                    <label>Gift Card Amount <small>(Changing this value will change the Remaining amount)</small></label>
                    <?php echo $model->form->textBoxFor("amount"); ?>
                </div>
                <div class="form-group">
                    <label>Remaining</label>
                    <div><?=$model->gift_card->remaining?></div>
                </div>
                <input hidden name="remaining" type="number" value="<?=$model->gift_card->remaining?>"/>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
    <button class="btn btn-save" id="print">Print</button>
</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'gift_cards/');?>;
    $(document).ready(function () {
        $(function () {
            $('input[name="daterange"]').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mmA',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                showDropdowns: true,
                <?php if($model->gift_card->id > 0) { ?>
                startDate: "<?php echo date("m/d/Y g:iA",strtotime($model->gift_card->start_date));?>",
                endDate: "<?php echo date("m/d/Y g:iA",strtotime($model->gift_card->end_date));?>",
                <?php } ?>

            });
        });

        $.validator.addMethod("validate_code", function (value, element) {
            var pass_validation = false;
            var data = {val: value, id: $("#gift_card-form").data('id')};
            $.ajax({
                url: site_url + "validate_code",
                async: false,
                data: data,
                method: "GET",
                success: function (response) {
                    if (response.success) {
                        pass_validation = true;
                    }
                }
            });
            return pass_validation;
        }, 'Code is invalid or already exists');

        $.validator.addMethod("validate_daterange", function (value, element) {
            var pass_validation = false;
            var data = {val: value};
            $.ajax({
                url: site_url + "validate_daterange",
                async: false,
                data: data,
                method: "GET",
                success: function (response) {
                    if (response.success) {
                        pass_validation = true;
                    }
                }
            });
            return pass_validation;
        }, 'Invalid Date Range');

        $.validator.addMethod("validate_money", function (value, element, params) {
            if (params == 0 && value == "") {
                return true;
            }
            var pass_validation = false;
            var data = {val: value, gtzero: params};
            $.ajax({
                url: site_url + "validate_money",
                async: false,
                data: data,
                method: "GET",
                success: function (response) {
                    if (response.success) {
                        pass_validation = true;
                    }
                }
            });
            return pass_validation;
        }, 'Invalid money amount');

        $("#gift_card-form").validate({
            onkeyup: false,
            onclick: false,

            rules: {
                name: {required: true},
                code: {required: true, validate_code: true},
                discount_amount: {required: true, validate_money: 1},
                min_amount: {validate_money: 0},
                daterange: {required: true, validate_daterange: true}
            }

        });

        $('#generate-token').on('click',function(){
            $.post('/admin/gift_cards/generateGiftCode',{},function(data){
                $('input[name=code]').val(data);
            });
        });

        $(".currency-us").autoNumeric('init', {aSign: '$'});
        $(".numeric").numericInput();
    });
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>