<?php
$gender = [1 => 'Unisex', 2 => "Men", 3 => "Women"];
?>
<style type="text/css">
    .legshot {
        border:1px black solid;
    }
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->new_product->id; ?>"/>
<!--    <input type="hidden" name="redirectTo" value="--><?//= $_SERVER['HTTP_REFERER'] ?><!--"/>-->
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
            <li role="presentation"><a href="#categories-tab" aria-controls="categories" role="tab" data-toggle="tab">Categories</a></li>
            <li role="presentation"><a href="#linked-tab" aria-controls="linked" role="tab" data-toggle="tab">Linked Items</a></li>
            <li role="presentation"><a href="#related-tab" aria-controls="related" role="tab" data-toggle="tab">Related</a></li>
            <li role="presentation"><a href="#variation-tab" aria-controls="variation" role="tab" data-toggle="tab">Variations</a></li>
            <?php if ($model->new_product->id > 0) { ?>
            <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
            <?}?>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>DELETE</label>
                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>new_products/delete/<?php echo $model->new_product->id; ?>?token_id=<?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
                                    <i class="icon-cancel-circled"></i>
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Name</label>

                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

<!--                            <div class="form-group">-->
<!--                                <label>Alias</label>-->
<!--                                --><?php //echo $model->form->editorFor("alias"); ?>
<!--                            </div>-->

                            <div class="form-group">
                                <label>Featured image</label>

                                <p>
                                    <small style="color:#A81927"><b>(ideal featured image dimensions are 800 x 800, zoomed out with white space surrounding new_product)</b></small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->new_product->featured_image != "" && file_exists(UPLOAD_PATH . 'new_products' . DS . $model->new_product->featured_image)) {
                                        $img_path = UPLOAD_URL . 'new_products/' . $model->new_product->featured_image;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'new_products/delete_image/' . $model->new_product->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->new_product->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Featured Video</label>
                                <p>
                                    <small>(ideal featured video is less then 10M)</small>
                                </p>
                                <p><input type="file" name="video_link" class='video'/></p>
                                <?php if($model->new_product->video_link) { ?>
                                    <div style="display:inline-block">
                                        <video controls width="400" height="300">
                                            <source src="<?=$model->new_product->videoLink()?>" type="video/mp4">
                                        </video>
                                        <div class='preview-container'><?=$model->new_product->video_link?></div>
                                        <input type="hidden" name="video_link" class='video' value="<?=$model->new_product->video_link?>"/>
                                    </div>
                                <? } ?>
                            </div>

    <!--                    <div class="form-group">-->
    <!--                        <a href="#" id="add-to-hottest">Add this new_product to hottest deals</a>-->
    <!--                    </div>-->
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description"); ?>
                            </div>
                            <div class="form-group">
                                <label>Details</label><p><small>Separate details using | symbol</small></p>
                                <?php echo $model->form->textAreaFor("details"); ?>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input name="quantity" type="text" value="<?=$model->new_product->quantity ? : 0?>"/>
                            </div>
                            <div class="form-group">
                                <label>Heel Height</label>
                                <input name="heel_height" type="text" value="<?=$model->new_product->heel_height ? : 0?>"/>
                            </div>
                            <div class="form-group">
                                <label>Sizes</label>
                                <select name="size[]" class="multiselect" multiple data-placeholder="Sizes">
                                    <?$sizes = json_decode($model->new_product->size,true) ? : [];
                                    foreach($model->sizes as $size){?>
                                        <option value="<?=$size->id?>" <?=$sizes && in_array($size->id,$sizes) ? 'selected': ''?>><?="US: $size->us_size | EUR: $size->eur_size"?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Available Colors</label>
                                <select name="color[]" class="multiselect" multiple data-placeholder="Colors">
                                    <?php foreach ($model->colors as $c) {
                                        $name = $c->made_to_order ? $c->name. " (Made to order)": $c->name;?>
                                        <option value="<?php echo $c->id; ?>"><?php echo $name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Availability</label>
                                <select name="availability" class="form-control">
                                    <?$selected_stock = ($model->new_product->availability)?:2; ?>
                                    <option value="2" <?php if ($selected_stock == 2) {echo "selected";} ?>>Active</option>
                                    <option value="1" <?php if ($selected_stock == 1) {echo "selected";} ?>>In stock</option>
                                    <option value="0" <?php if ($selected_stock == 0) {echo "selected";} ?>>Inactive</option>

                                </select>
                            </div>


                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Prices</h4>

                            <div class="form-group">
                                <label>Discounted Price</label><p><small>0 for no discount</small></p>
                                <?php echo $model->form->editorFor("msrp"); ?>
                            </div>
                            <div class="form-group">
                                <label>Sell price</label>
                                <?php echo $model->form->editorFor("price"); ?>
                            </div>

                        </div>
                    </div>
                    <?php if (count($model->product_questions) > 0) { ?>
                        <div class="col-md-12">
                            <div class="box">
                                <h4>Questions</h4>
                                <table id="data-list" class="table">
                                    <thead>
                                    <tr>
                                        <th width="30%">Date</th>
                                        <th width="60%">Subject</th>
                                        <th width="10%">View</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($model->product_questions as $obj) { ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?php echo $obj->insert_time; ?></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?= $obj->subject ?></a>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="seo-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <?php echo $model->form->editorFor("tags"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->editorFor("meta_description"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="categories-tab">
                <select name="new_product_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
                    <?php foreach ($model->categories as $cat) { ?>
                        <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div role="tabpanel" class="tab-pane" id="linked-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Linked Items</h4>
                            <div class="form-group">
                                <label>Linked Name</label> <small>(appears in corresponding links)</small>
                                <?php echo $model->form->editorFor('linked_name')?>
                            </div>
                            <div class="form-group">
                                <label>Linked List</label>
                                <select name="linked_list[]" class="multiselect" data-placeholder="Linked List" multiple="multiple">
                                    <?php foreach ($model->new_products as $product) { ?>
                                        <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <?if($model->new_product->linked_list){?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Item Name</th>
                                    <th>Linked Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach (json_decode($model->new_product->linked_list,true) as $prodId) {
                                    $item = \Model\New_Product::getItem($prodId)?>
                                    <tr data-image_id="<?php echo $item->id; ?>">
                                        <td><a href="<?=ADMIN_URL.'new_products/update/'.$item->id?>"><img src="<?php echo UPLOAD_URL.'new_products/'.$item->featuredImage(); ?>" width="100" height="100"/></a></td>
                                        <td><a href="<?=ADMIN_URL.'new_products/update/'.$item->id?>"><?php echo $item->name; ?></a></td>
                                        <td><a href="<?=ADMIN_URL.'new_products/update/'.$item->id?>"><?php echo $item->linked_name ? : 'N/A'; ?></a></td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <? } ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="related-tab">
                <select name="related_products[]" class="multiselect" data-placeholder="Related Products" multiple="multiple">
                    <?php foreach ($model->new_products as $product) { ?>
                        <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div role="tabpanel" class="tab-pane" id="variation-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Variants</h4>

                            <?
                            if($model->new_product->color){
                                $colors = [];
                                foreach(json_decode($model->new_product->color,true) as $c){
                                    if($col = \Model\Color::getItem($c)) {
                                        $colors[] = $col;
                                    }
                                }?>
                                <div class="form-group">
                                    <label>Colors</label>

                                    <?foreach ($colors as $color) {
                                        $product_image = \Model\New_Product_Image::getList(['where'=>"new_product_id = {$model->new_product->id} and color_id like '%\"{$color->id}\"%'", 'orderBy'=>"display_order"]);
                                        $attribute = \Model\New_Product_Attributes::getItem(null,['where'=>"new_product_id = {$model->new_product->id} and color_id = $color->id and name = 'price'"]);
                                        $mto = \Model\New_Product_Attributes::getItem(null,['where'=>"new_product_id = {$model->new_product->id} and color_id = $color->id and name = 'mto'"]);
                                        $eve = \Model\New_Product_Attributes::getEventById($model->new_product->id, $color->id);
                                        $stat = \Model\New_Product_Attributes::getStatusById($model->new_product->id, $color->id);
                                        ?>
                                        <div class="variant-container" data-id="<?=$color->id?>">
                                            <div class="variant-word">
                                                <p style="cursor: pointer;" data-id="<?= $color->id ?>"><?= $color->name ?></p>
                                            </div>
                                            <div class="variant-details-container" style="display: none;" data-id="<?=$color->id?>">
                                                <label>Set Variant Price</label><input name="update-price" type="text" class="numeric" value="<?=$attribute ? $attribute->value : 0?>">
                                                <label>Set Event</label><select class="form-control" name="update-event"><option value="-1">None</option><?foreach($model->events as $event){?><option value="<?=$event->id?>" <?=$eve && $event->id == $eve->value ? 'selected': ''?>><?=$event->title?></option><?}?></select>
                                                <label>Set Status</label><select class="form-control" name="update-status"><?foreach(\Model\Product_Attributes::$status as $id=>$status){?><option value="<?=$id?>" <?=$stat && $id == $stat->value ? 'selected': ''?>><?=$status?></option><?}?></select>
                                                <label>Made to Order</label> <input name="update-mto" type="checkbox" value="<?=$mto ? $mto->value: 0?>" <?=$mto && $mto->value == 1? 'checked': ''?>/>
                                                <button class="save-variants">Save All</button>
                                                <hr/>
                                            </div>
                                            <? if ($product_image) { ?>
                                                <? foreach($product_image as $pi) { ?>
                                                    <img src="<?= $pi->get_image_url() ?>" width="100"
                                                         class="select-legshot <?=$pi->legshot == 1 ? 'legshot': ''?>"
                                                         data-id="<?= $pi->id ?>"/>
                                                <? } ?>
                                            <? } ?>
                                        </div>
                                        <hr/>
                                    <? } ?>
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($model->new_product->id > 0) { ?>
                <div role="tabpanel" class="tab-pane" id="images-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <div class="dropzone" id="dropzoneForm"
                                     action="<?php echo ADMIN_URL . 'new_products/upload_images/' . $model->new_product->id; ?>">

                                </div>
                                <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                                <br/>
                                <p>
                                    <small style="color:#A81927"><b>(ideal new_product angle image dimensions are 1024px wide x 683px height)</b></small>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <label>Set Default Color</label>
                                <select name="default_color_id" class="multiselect">
                                    <? if ($model->new_product->color) { ?>
                                        <? foreach (json_decode($model->new_product->color, true) as $color) {
                                            $c = \Model\Color::getItem($color);
                                            $selected = $model->new_product->default_color_id == $c->id ? 'selected' : '' ?>
                                            <option value="<?= $c->id ?>" <?= $selected ?>><?= $c->name ?></option>
                                        <? } ?>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <table id="image-container" class="table table-sortable-container">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>File Name</th>
                                        <th>Variant</th>
                                        <th>Display Order</th>
<!--                                        <th>Leg Shot</th>-->
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($prImg = \Model\New_Product_Image::getList(['where' => 'new_product_id=' . $model->new_product->id, 'orderBy' => 'display_order', 'sort' => 'DESC']) as $pimg) {?>
                                            <tr data-image_id="<?php echo $pimg->id; ?>">
                                                <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100" height="100"/></td>
                                                <td><?php echo $pimg->image; ?></td>
                                                <td>
                                                    <select name="color_image[]" class="color_image color_image<?=$pimg->id?> multiselect" data-img_id="<?=$pimg->id?>" multiple="multiple">
                                                        <?if($model->new_product->color){?>
                                                            <?foreach(json_decode($model->new_product->color,true) as $color){
                                                                $c = \Model\Color::getItem($color);
                                                                $selected = $pimg->color_id == $c->id ? 'selected' : ''?>
                                                                <option value="<?=$c->id?>" <?=$selected?>><?=$c->name?></option>
                                                            <? } ?>
                                                        <? } ?>
                                                    </select>
                                                </td>
                                                <td class="display-order-td"><?=$pimg->display_order?></td>
<!--                                                <td class="leg_shot"><input id="leg_shot" name="leg_shot" type="radio" value="--><?//=$pimg->id?><!--" --><?//=$pimg->leg_shot == 'TRUE' ? 'checked': ''?><!--/></td>-->
                                                <td class="text-center">
                                                    <a class="btn-actions delete-new_product-image"
                                                       href="<?php echo ADMIN_URL; ?>new_products/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                        <i class="icon-cancel-circled"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <? } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <button type="submit" class="btn btn-save" style="display:none">Save</button>
    <div id="target" class="btn btn-save">Save</div>
</form>

<?php echo footer(); ?>
<script type="text/javascript">
    var categories = <?php echo json_encode($model->product_categories); ?>;
    var colors = <?php echo ($model->new_product->color?:'0'); ?>;
    var sizes = <?php echo ($model->new_product->size?:0); ?>;
    var related = <?php echo ($model->new_product->related_products?:0); ?>;
    var default_color_id = <?php echo ($model->new_product->default_color_id?:0); ?>;
    var linked_list = <?php echo ($model->new_product->linked_list?:0); ?>;
//    var collections = <?php //echo json_encode($model->product_collections); ?>//;
//    var materials = <?php //echo json_encode($model->product_materials); ?>//;
    //console.log(categories);
    var site_url =<?php echo json_encode(ADMIN_URL.'new_products/'); ?>;
    $(document).ready(function () {
//        var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
        var video_thumbnail = new mult_image($("input[name='video_thumbnail']"), $("#preview-thumbnail-container"));

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('mousewheel.disableScroll')
        });

        $('.variant-word').on('click',function(){
            $(this).parent().find('.variant-details-container').toggle();
        });

        $('.color_image').on('change',function(){
            var img_id = $(this).attr('data-img_id');
            var color_id = $(this).val();
            var data = {
                product_image_id:img_id,
                color_id:color_id
            };
            $.post('/admin/new_products/save_variant_image',data,function(json){
                console.log(json,json.status,jQuery.parseJSON(json).status);
            })
        });

        $('.save-variants').on('click',function(e){
            e.preventDefault();
            e.stopPropagation();
            var container = $(this).parent();
            var color_id = container.parent().attr('data-id');
            var price = container.find('input[name=update-price]').val();
            var size_val = container.find('select[name=size_price-size]').val();
            var size_price = container.find('input[name=size_price-price]').val();
            var mto = container.find('input[name=update-mto]').is(':checked') ? 1: 0;
            var event = container.find('select[name=update-event]').val();
            var status = container.find('select[name=update-status]').val();
            var data = {
                product_id:<?=$model->new_product->id?>,
                color_id:color_id,
                price:price,
                size_val:size_val,
                size_price:size_price,
                mto:mto,
                event:event,
                status:status
            };
            $.post('/admin/new_products/save_attribute',data,function(obj){
                var jObj = $.parseJSON(obj);
                console.log(jObj.status);
                container.hide();
            });
        });

        $('.select-legshot').on('click',function(){
            var img_id = $(this).attr('data-id');
            var product_id = <?=$model->new_product->id?>;
            var color_id = $(this).parent().find('p').attr('data-id');
            var data = {product_image_id:img_id,product_id:product_id,color_id:color_id};
            var self = $(this);
            $.post('/admin/new_products/save_legshot_image',data,function(json){
                console.log(json,json.status,jQuery.parseJSON(json).status);
                if($.parseJSON(json).status == 'success'){
                    var rem = self.parent().find('.legshot').attr('data-id');
                    var add = self.attr('data-id');
                    var remItem = $('img[data-id='+rem+']');
                    var addItem = $('img[data-id='+add+']');
                    remItem.removeClass('legshot');
                    if(rem == add){
                        //do nothing, removes border
                    } else {
                        addItem.addClass('legshot');
                    }
//                    self.parent().find('.legshot').removeClass('legshot');
//                    self.addClass('legshot');
                }
            })
        });

        $("#target").click(function () {
            var price = $("input[name='price']").val();
            var name = $("input[name='name']").val();
            var slug = $("input[name='slug']").val();
            var id = <?=$model->new_product->id?>;
            var errors = new Array();
            if ($.trim(price) > 0) {
                $("input[name='price']").css({
                    "border-color": ""
                });
            } else {
                $("input[name='price']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect price");
            }
            if ($.trim(name) == "") {
                $("input[name='name']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect name");
            } else {
                $("input[name='name']").css({
                    "border-color": ""
                });
            }
            if ($.trim(slug) == "") {
                $("input[name='slug']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect slug");
            } else {

                $.ajax({
                    async: false,
                    url: '/admin/new_products/check_slug',
                    enctype: 'multipart/form-data',
                    method: 'POST',
                    data: {
                        slug: slug,
                        id: id
                    },
                    success: function (data) {

                        if (data == 1) {
                            errors.push("Slug must be uniq!");

                            $("input[name='slug']").css({
                                "border-color": "red"
                            });

                        } else {
                            $("input[name='slug']").css({
                                "border-color": ""
                            });
                        }
                    }
                });
            }
            var text = "";
            for (i = 0; i < errors.length; i++) {

                text += "<li>" + errors[i] + "</li>";
            }
            if (errors.length > 0) {
                $('html, body').animate({
                    scrollTop: 0
                });
                $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
                errors = [];
            } else {
                $(".form").submit();
            }
        });

        $("input.image").change(function () {
            readURL(this);
        });


        $("input[name='name']").on('keyup', function (e) {
            var val = $.trim($(this).val());
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='new_product_category[]']").val(categories);
        $("select[name='color[]']").val(colors);
        $("select[name='size[]']").val(sizes);
        $("select[name='related_products[]']").val(related);
        $("select[name='default_color_id']").val(default_color_id);
        $("select[name='linked_list[]']").val(linked_list);
        <?if(isset($prImg) && $prImg){
        foreach($prImg as $pimg){?>
            $(".color_image"+<?=$pimg->id?>).val(<?=$pimg->color_id?>);
        <?}?>
        <?}?>
//        $("select[name='product_collection[]']").val(collections);
//        $("select[name='product_material[]']").val(materials);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        $("a.delete-new_product-image").on("click", function () {
            if (confirm("are you sure?")) {
                location.href = $(this).attr("href");
            } else {
                return false;
            }
        });
        var adjustment;
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden; width: 100px; height: 100px;">.</td></tr>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-new_product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
        $(".numeric").numericInput();
    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'new_products/upload_images/'.$model->new_product->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>
