<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->blog->id; ?>"/>
    <input type="hidden" name="create_date" value="<?php echo $model->blog->create_date; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("title"); ?>
                </div>
                <div class="form-group">
                    <label>Slug</label>
                    <?php echo $model->form->editorFor("slug"); ?>
                </div>
                <div class="form-group">
                    <label>Subtitle</label>
                    <?php echo $model->form->editorFor("subtitle"); ?>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <?php echo $model->form->dropDownListFor("blog_category_id",\Model\Blog_Category::getOptions()); ?>
                </div>
                <div class="form-group">
                    <label>Featured Image</label><p>
                        <small style="color:#abb0b3"><b>(ideal featured image dimensions are 800 x 800, zoomed out with white space surrounding product)</b></small>
                    </p>
                    <p><input type="file" name="featured_image" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->blog->featured_image != "") {
                            $img_path = $model->blog->getFeaturedImage();
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'blogs/delete_image/' . $model->blog->id; ?>?featured_image=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="featured_image"
                                       value="<?= $model->blog->featured_image ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Body</label>
                    <?php echo $model->form->textAreaFor("body"); ?>
                </div>
                <div class="form-group">
                    <label>Author</label>
                    <?php echo $model->form->editorFor("author"); ?>
                </div>
                <div class="form-group">
                    <label>Modify Author</label>
                    <?php echo $model->form->editorFor("modify_author"); ?>
                </div>
                <div class="form-group">
                    <label>Footer</label>
                    <?php echo $model->form->textAreaFor("footer"); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="btn btn-save saveorupdate">Save</div>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'blog/');?>;
    $(document).ready(function () {
        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
    $(".saveorupdate").click(function () {
        var title = $("input[name='title']").val();
        var slug = $("input[name='slug']").val();
        var id = <?=$model->blog->id?>;
        var errors = new Array();
        if ($.trim(title) == "") {
            $("input[name='title']").css({
                "border-color": "red"
            });
            errors.push("Incorrect title");
        } else {
            $("input[name='title']").css({
                "border-color": ""
            });
        }
        if ($.trim(slug) == "") {
            $("input[name='slug']").css({
                "border-color": "red"
            });
            errors.push("Incorrect slug");
        } else {

            $.ajax({
                async: false,
                url: '/admin/blog_categorys/check_slug',
                enctype: 'multipart/form-data',
                method: 'POST',
                data: {
                    slug: slug,
                    id: id
                },
                success: function (data) {

                    if (data == 1) {
                        errors.push("Slug must be uniq!");

                        $("input[name='slug']").css({
                            "border-color": "red"
                        });

                    } else {
                        $("input[name='slug']").css({
                            "border-color": ""
                        });
                    }
                }
            });
        }
        var text = "";
        for (i = 0; i < errors.length; i++) {

            text += "<li>" + errors[i] + "</li>";
        }
        if (errors.length > 0) {
            $('html, body').animate({
                scrollTop: 0
            });
            $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
            errors = [];
        } else {
            $(".form").submit();
        }
    });
</script>