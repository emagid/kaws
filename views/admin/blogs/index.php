<?php if (count($model->blogs) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='20%'></th>
                <th width='20%'>Title</th>
                <th width='20%'>Subtitle</th>
                <th width='20%'>Category</th>
                <th width='20%'>Body</th>
                <th width='20%'>Create Date</th>
                <th width='20%'>Modify Date</th>
                <th width='20%'>Author</th>
                <th width='20%'>Modify Author</th>
                <th width='20%'>Footer</th>
                <th width='20%'>Slug</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->blogs as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><img style="width: 50px;" src="<?php echo $obj->getFeaturedImage(); ?>"/></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->subtitle; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo \Model\Blog_Category::getItem($obj->blog_category_id)->name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->body; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->create_date; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->modify_date; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->author; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->modify_author; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->footer; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>"><?php echo $obj->slug; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>blogs/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'blogs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>