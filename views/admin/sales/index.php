<?php if (count($model->sales) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="20%">Title</th>
                <th width="20%">Start</th>
                <th width="20%">End</th>
                <th width="20%">Discount</th>
                <th width="20%">Sale Type</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->sales as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sales/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sales/update/<?php echo $obj->id; ?>"><?php echo date('F d, Y',strtotime($obj->start_date)); ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sales/update/<?php echo $obj->id; ?>"><?php echo date('F d, Y',strtotime($obj->end_date)); ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sales/update/<?php echo $obj->id; ?>"><?php echo $obj->discount_amount.' ('.$obj->getDiscountSymbol().')' ?></a>
                    </td>
                    <td>
                        <?$json = json_decode($obj->sale_options,true);
                        if($obj->sale_type == 2) {
                            $key = key($json);
                            $cat = \Model\Category::getItem($key);
                            $typeStr = \Model\Sale::$saleType[$obj->sale_type] . ' - ' . $cat->name;
                        } else {
                            $typeStr = \Model\Sale::$saleType[$obj->sale_type];
                        }?>
                        <a href="<?php echo ADMIN_URL; ?>sales/update/<?php echo $obj->id; ?>"><?php echo $typeStr ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>sales/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>sales/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'sales';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

