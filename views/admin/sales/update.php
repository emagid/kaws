<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->sale->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>
    <?$selectedCat = 0;?>
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("title"); ?>
                </div>
                <div class="form-group">
                    <label>Start Date</label>
                    <?php echo $model->form->editorFor("start_date"); ?>
                </div>
                <div class="form-group">
                    <label>End Date</label>
                    <?php echo $model->form->editorFor("end_date"); ?>
                </div>
                <div class="form-group">
                    <label>Sale Type</label>
                    <?php echo $model->form->dropdownListFor("sale_type", \Model\Sale::$saleType,null,['class'=>'multiselect']); ?>
                </div>
                <div class="category-container">
                    <?if($model->sale->sale_type == 2 && $model->sale->sale_options){
                        $sale_options = json_decode($model->sale->sale_options,true)?>
                        <div class="form-group">
                            <label>Category</label>
                            <select name="category" class="multiselect">
                                <?foreach(\Model\Category::getList() as $item){
                                    if(array_key_exists($item->id,$sale_options)){
                                        $selected = 'selected';
                                        $selectedCat = $item->id;
                                    } else {
                                        $selected = '';
                                    }?>
                                    <option value="<?=$item->id?>" <?= $selected ?>><?=$item->name?></option>
                                <?}?>
                            </select>
                        </div>
                    <?}?>
                </div>
                <div class="product-container">
                    <?if($model->sale->sale_type == 2 && $model->sale->sale_options){
                        $sale_options = json_decode($model->sale->sale_options,true);
                        $prod = \Model\Category::getItem(key($sale_options))->getProducts();?>
                        <div class="form-group">
                            <label>Products</label>
                            <select name="products[]" class="multiselect" multiple="multiple">
                                <?foreach($prod as $item){?>
                                    <option value="<?=$item->id?>"><?=$item->name?></option>
                                <?}?>
                            </select>
                        </div>
                    <?}?>
                </div>
                <div class="buy-x-get-y-container">
                    <?if($model->sale->sale_type == 3 && $model->sale->sale_options){
                        $sale_options = json_decode($model->sale->sale_options,true);?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-20">
                                    <label>Buy</label>
                                    <input type="text" name="buy" value="<?= $sale_options['buy'] ?>"/>
                                </div>
                                <div class="col-sm-4">
                                    <label>Type</label>
                                    <select name="buy_type" class="multiselect">
                                        <?foreach(\Model\Sale::$buyType as $key=>$value){?>
                                            <option value="<?=$key?>" <?=$sale_options['buy_type'] == $key ? 'selected': '';?>><?=$value?></option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Get</label>
                            <input type="text" name="get" value="<?=$sale_options['get']?>"/>
                        </div>
                    <?}?>
                </div>
                <div class="form-group">
                    <label>Discount Amount</label>
                    <?php echo $model->form->editorFor("discount_amount"); ?>
                </div>
                <div class="form-group">
                    <label>Discount Type</label>
                    <?php echo $model->form->dropdownListFor("discount_type", \Model\Sale::$typeSymbol,null,['class'=>'multiselect']); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>
<link type="text/css" rel="stylesheet" href="<?=ADMIN_CSS.'jquery.datetimepicker.css'?>"/>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'sales/');?>;
    $(document).ready(function () {
        var products = <?php echo ($model->sale->sale_options && $selectedCat? json_encode(json_decode($model->sale->sale_options,true)[$selectedCat]): 0) ?>;
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('input[name=start_date], input[name=end_date]').datetimepicker({
            format: 'Y-m-d',
            timepicker:false
        });
        function multiselect() {
            $("select.multiselect").each(function (i, e) {
//            $(e).val('');
                var placeholder = $(e).data('placeholder');
                $(e).multiselect({
                    nonSelectedText: placeholder,
                    includeSelectAllOption: true,
                    maxHeight: 415,
                    checkboxName: '',
                    enableCaseInsensitiveFiltering: true,
                    buttonWidth: '100%'
                });
            });
        }
        multiselect();
        $('select[name=sale_type]').on('change',function(){
            console.log('test');
            var cat_container = $('.category-container');
            var prod_container = $('.product-container');
            var buyget_container = $('.buy-x-get-y-container');
            switch($(this).find(':selected').val()){
                case '1':
                    cat_container.empty();
                    prod_container.empty();
                    buyget_container.empty();
                    break;
                case '2':
                    $.post('/admin/sales/buildCategory',{},function(data){
                        var json = $.parseJSON(data);
                        cat_container.append(JSON.parse(data).appendHtml);
                        multiselect();
                    });
                    buyget_container.empty();
                    break;
                case '3':
                    <?$sale_options = json_decode($model->sale->sale_options,true);?>
                    var html =
                        '<div class="form-group">' +
                            '<div class="row"> ' +
                                '<div class="col-sm-20"> ' +
                                    '<label>Buy</label> ' +
                                    '<input type="text" name="buy" value="<?= isset($sale_options['buy']) && $sale_options['buy'] ? $sale_options['buy']: ''; ?>"/> ' +
                                '</div> ' +
                                '<div class="col-sm-4"> ' +
                                    '<label>Type</label> ' +
                                    '<select name="buy_type" class="multiselect">';
                    <?foreach(\Model\Sale::$buyType as $key=>$value){?>
                        html += '<option value="<?=$key?>"><?=$value?></option>';
                    <?}?>
                        html +=
                                    '</select> ' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<label>Get</label>' +
                            '<input type="text" name="get" value="<?= isset($sale_options['get']) && $sale_options['get'] ? $sale_options['get']: ''; ?>"/>' +
                        '</div>';
                    cat_container.empty();
                    prod_container.empty();
                    buyget_container.append(html);
                    multiselect();
                    break;
            }
        });
        $(document).on('change','select[name=category]',function(){
            console.log('test');
            var select = $('select[name=category]');
            if(select.find(':selected').val()){
                var cat_id = select.find(':selected').val();
                $.post('/admin/sales/buildProducts',{cat_id:cat_id},function(data){
                    var json = $.parseJSON(data);
                    $('.product-container').html(JSON.parse(data).appendHtml);
                    multiselect();
                });
            }
        });
        $("select[name='products[]']").val(products);
        $("select.multiselect").multiselect("rebuild");
    });
</script>