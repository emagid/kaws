<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#fulfilled-tab" aria-controls="fulfilled"  role="tab" data-toggle="tab">Fulfilled Orders</a></li>
        <li role="presentation" ><a href="#pending-tab" aria-controls="pending"  role="tab" data-toggle="tab">Pending</a></li>
        <li role="presentation" ><a href="#returned-tab" aria-controls="returned"  role="tab" data-toggle="tab">Returned</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="fulfilled-tab">
            <?php if (count($model->orders) > 0) { ?>
            <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                    <tr>
                        <th width="25%">Date</th>
                        <th width="16%">Order #</th>
                        <th width="16%">Bill Name</th>
                        <th width="31%">Order Details</th>
                        <th width="16%">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($model->orders as $obj) {?>
                        <? $obj->insert_time = new DateTime($obj->insert_time);
                        $obj->insert_time = $obj->insert_time->format('m-d-Y H:i:s');
                        if($order->fulfillment_status != 'Fulfilled') continue; ?>
                                <tr class="originalProducts" <?= $style ?>>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <?php echo $obj->insert_time; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>fulfillment/campaign/<?php echo $obj->id; ?>">
                                            <?php echo $obj->id; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                                $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);


                                            $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                                $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);


                                            if (count(array_diff($shp, $blng)) == 0) {
                                                ?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                                            <? } else {
                                                ?>
                                                <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                                        disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                                            <? } ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? if (($orderProducts = $obj->getOrderProducts())) {
                                                foreach ($orderProducts as $orderProduct) {
                                                    $json = json_decode($orderProduct->details, true);
                                                    if ($orderProduct->product_id) {
                                                        if($orderProduct->clearance){
                                                            echo '<span style="color:green">'.\Model\Product::getItem($orderProduct->product_id)->name.' (Clearance)</span>';
                                                        } else {
                                                            echo \Model\Product::getItem($orderProduct->product_id)->name;
                                                        }
                                                    } else if(isset($json['misc_name'])){
                                                        echo $json['misc_name'] . '(Custom)';
                                                    } else if($obj->ticket_id){
                                                        echo $orderProduct->details;
                                                    } else {
                                                        echo \Model\Gift_Card::getItem($json['gift_card_id'])->name;
                                                    }
                                                    echo '<br/>';
                                                }
    //                                echo implode('<br/>',array_map(function($item){return \Model\Product::getItem($item->product_id)->name;},$orderProducts));
                                            } ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? if ($obj->payment_method == 2) { ?>
                                                <button type="button" class="btn btn-success" style="position:relative;"
                                                        disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                                            <? } else { ?>$<?php echo number_format($obj->total, 2);
                                            } ?>
                                        </a>
                                    </td>

                                </tr>
                            <? }
                    } ?>
                </tbody>
            </table>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="pending-tab">
            <?php if (count($model->orders) > 0) { ?>
            <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                    <tr>
                        <th width="25%">Date</th>
                        <th width="16%">Order #</th>
                        <th width="16%">Bill Name</th>
                        <th width="31%">Order Details</th>
                        <th width="16%">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($model->orders as $obj) {?>
                        <? /*$obj->insert_time = new DateTime($obj->insert_time);
                        $obj->insert_time = $obj->insert_time->format('m-d-Y H:i:s');*/
                        if($order->fulfillment_status == 'Pending' ) continue; ?>
                                <tr class="originalProducts" <?= $style ?>>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <?php echo $obj->insert_time; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>fulfillment/campaign/<?php echo $obj->id; ?>">
                                            <?php echo $obj->id; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                                $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);


                                            $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                                $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);


                                            if (count(array_diff($shp, $blng)) == 0) {
                                                ?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                                            <? } else {
                                                ?>
                                                <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                                        disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                                            <? } ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? if (($orderProducts = $obj->getOrderProducts())) {
                                                foreach ($orderProducts as $orderProduct) {
                                                    $json = json_decode($orderProduct->details, true);
                                                    if ($orderProduct->product_id) {
                                                        if($orderProduct->clearance){
                                                            echo '<span style="color:green">'.\Model\Product::getItem($orderProduct->product_id)->name.' (Clearance)</span>';
                                                        } else {
                                                            echo \Model\Product::getItem($orderProduct->product_id)->name;
                                                        }
                                                    } else if(isset($json['misc_name'])){
                                                        echo $json['misc_name'] . '(Custom)';
                                                    } else if($obj->ticket_id){
                                                        echo $orderProduct->details;
                                                    } else {
                                                        echo \Model\Gift_Card::getItem($json['gift_card_id'])->name;
                                                    }
                                                    echo '<br/>';
                                                }
    //                                echo implode('<br/>',array_map(function($item){return \Model\Product::getItem($item->product_id)->name;},$orderProducts));
                                            } ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? if ($obj->payment_method == 2) { ?>
                                                <button type="button" class="btn btn-success" style="position:relative;"
                                                        disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                                            <? } else { ?>$<?php echo number_format($obj->total, 2);
                                            } ?>
                                        </a>
                                    </td>

                                </tr>
                            <? }
                    } ?>
                </tbody>
            </table>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="returned-tab">
            <?php if (count($model->orders) > 0) { ?>
            <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                    <tr>
                        <th width="25%">Date</th>
                        <th width="16%">Order #</th>
                        <th width="16%">Bill Name</th>
                        <th width="31%">Order Details</th>
                        <th width="16%">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($model->orders as $obj) {?>
                        <? /*$obj->insert_time = new DateTime($obj->insert_time);
                        $obj->insert_time = $obj->insert_time->format('m-d-Y H:i:s');*/
                        if($order->fulfillment_status != 'Returned') continue; ?>
                                <tr class="originalProducts" <?= $style ?>>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <?php echo $obj->insert_time; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>fulfillment/campaign/<?php echo $obj->id; ?>">
                                            <?php echo $obj->id; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                                $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);


                                            $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                                $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);


                                            if (count(array_diff($shp, $blng)) == 0) {
                                                ?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                                            <? } else {
                                                ?>
                                                <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                                        disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                                            <? } ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? if (($orderProducts = $obj->getOrderProducts())) {
                                                foreach ($orderProducts as $orderProduct) {
                                                    $json = json_decode($orderProduct->details, true);
                                                    if ($orderProduct->product_id) {
                                                        if($orderProduct->clearance){
                                                            echo '<span style="color:green">'.\Model\Product::getItem($orderProduct->product_id)->name.' (Clearance)</span>';
                                                        } else {
                                                            echo \Model\Product::getItem($orderProduct->product_id)->name;
                                                        }
                                                    } else if(isset($json['misc_name'])){
                                                        echo $json['misc_name'] . '(Custom)';
                                                    } else if($obj->ticket_id){
                                                        echo $orderProduct->details;
                                                    } else {
                                                        echo \Model\Gift_Card::getItem($json['gift_card_id'])->name;
                                                    }
                                                    echo '<br/>';
                                                }
    //                                echo implode('<br/>',array_map(function($item){return \Model\Product::getItem($item->product_id)->name;},$orderProducts));
                                            } ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                            <? if ($obj->payment_method == 2) { ?>
                                                <button type="button" class="btn btn-success" style="position:relative;"
                                                        disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                                            <? } else { ?>$<?php echo number_format($obj->total, 2);
                                            } ?>
                                        </a>
                                    </td>

                                </tr>
                            <? }
                    } ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
<?php echo footer(); ?>