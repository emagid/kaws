<div class="row">
    <div class="col-md-8">
        <div class="box transparent_box" style="align-content: center">
            <a href="<?= ADMIN_URL ?>fulfillment/export" class="btn btn-warning" style="width: 49%">Export</a>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box transparent_box" style="align-content: center">
            <a href="<?= ADMIN_URL ?>orders/report" class="btn btn-warning" style="width: 49%">Order Status Report</a>
        </div>
    </div>
</div>

<?php if (count($model->campaigns) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
                <tr>
                    <th width="1%"><input type="checkbox" id="check-all"></th>
                    <th width="5%">Campaign Name</th>
                    <th width="5%">Total Orders</th>
                    <th width="5%">Amount Ready To Process</th>
                    <th width="5%">Amount Processed</th>
                    <th width="5%">Amount Shipped</th>
                    <th width="5%">Amount Returned</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model->campaigns as $obj) {
                    if(isset($_GET['debug'])){
                        var_dump($obj);
                    }
                    ?>
                            <tr class="originalProducts" <?= $style ?>>
                                <td>
                                    <input type="checkbox" name="multibox" value="<?= $obj->id ?>">
                                </td>

                                <td>
                                    <a href="<?php echo ADMIN_URL; ?>campaigns/update/<?php echo $obj->id; ?>">
                                        <?php echo $obj->name; ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?php echo ADMIN_URL; ?>fulfillment/campaign/<?php echo $obj->id; ?>">
                                        <?php echo $obj->total?:0; ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?php echo ADMIN_URL; ?>fulfillment/campaign/<?php echo $obj->id; ?>">
                                        <?php echo $obj->pending?:0; ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?php echo ADMIN_URL; ?>fulfillment/campaign/<?php echo $obj->id; ?>">
                                        <?php echo $obj->fulfilled?:'0'; ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?php echo ADMIN_URL; ?>fulfillment/campaign/<?php echo $obj->id; ?>">
                                        <?php echo $obj->shipped?:'0'; ?>
                                    </a>
                                </td>
                                <td>
                                    <a href="<?php echo ADMIN_URL; ?>fulfillment/campaign/<?php echo $obj->id; ?>">
                                        <?php echo $obj->returned?:'0'; ?>
                                    </a>
                                </td>
                            </tr>
                <? } ?>
            </tbody>
        </table>
    </div>

<? } ?>
<? echo(footer())?>
