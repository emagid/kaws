<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->combo->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab"
                                                      data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#product-tab" aria-controls="product" role="tab"
                                       data-toggle="tab">Product</a></li>
            <li role="presentation"><a href="#price-tab" aria-controls="price" role="tab" data-toggle="tab">Price</a>
            </li>

        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>DELETE</label>
                                <a class="btn-actions"
                                   href="<?php echo ADMIN_URL; ?>combo/delete/<?php echo $model->combo->id; ?>?token_id=<?php echo get_token(); ?>"
                                   onClick="return confirm('Are You Sure?');">
                                    <i class="icon-cancel-circled"></i>
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <!-- TODO Change to s3 -->
                            <div class="form-group">
                                <label>Featured image</label>
                                <p>
                                    <small style="color:#A81927"><b>(ideal featured image dimensions are 800 x 800,
                                            zoomed out with white space surrounding product)</b></small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>
                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->product->featured_image != "" && file_exists(UPLOAD_PATH . 'products' . DS . $model->product->featured_image)) {
                                        $img_path = UPLOAD_URL . 'combo/' . $model->combo->featured_image;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'combo/delete_image/' . $model->product->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->combo->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Featured Video</label>
                                <p>
                                    <small>(ideal featured video is less then 10M)</small>
                                </p>
                                <p><input type="file" name="video_link" class='video'/></p>
                                <?php if ($model->combo->video_link) { ?>
                                    <div style="display:inline-block">
                                        <video controls width="400" height="300">
                                            <source src="<?= $model->combo->videoLink() ?>" type="video/mp4">
                                        </video>
                                        <div class='preview-container'><?= $model->combo->video_link ?></div>
                                        <input type="hidden" name="video_link" class='video'
                                               value="<?= $model->combo->video_link ?>"/>
                                    </div>
                                <? } ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description"); ?>
                            </div>
                            <div class="form-group">
                                <label>Overview Description</label>
                                <p>
                                    <small>Separate details using | symbol</small>
                                </p>
                                <?php echo $model->form->textAreaFor("details"); ?>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input name="quantity" type="text" value="<?= $model->combo->quantity ?: 0 ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Availability</label>
                                <select name="availability" class="form-control">
                                    <? $selected_stock = ($model->combo->availability) ?: 2; ?>
                                    <? foreach (\Model\Product::$availability as $key => $item) { ?>
                                        <option value="<?= $key ?>" <?php if ($selected_stock == $key) {
                                            echo "selected";
                                        } ?>><?= $item ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->editorFor("meta_description"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="product-tab">
                <div class="row">
                    <div class="col-md-24">
                        <select name="product_id[]" class="multiselect" data-placeholder="Categories"
                                multiple="multiple">
                            <?php foreach (\Model\Product::getList(['orderBy' => 'name']) as $item) { ?>
                                <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <table id="products-list" class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>SKU</th>
                                    <th>Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($model->product_id as $i) {
                                    $item = \Model\Product::getItem($i); ?>
                                    <tr data-image_id="<?php echo $pimg->id; ?>">
                                        <td><?php echo $item->name; ?></td>
                                        <td><?php echo $item->upc; ?></td>
                                        <td><?php echo $item->getPrice(); ?></td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="price-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Prices</h4>
                            <div class="form-group">
                                <label>Discounted Price</label>
                                <p>
                                    <small>0 for no discount</small>
                                </p>
                                <?php echo $model->form->editorFor("msrp"); ?>
                            </div>
                            <div class="form-group">
                                <label>Sell price(overrides defined price)</label>
                                <?php echo $model->form->editorFor("price"); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save" style="display:none">Save</button>
    <div id="target" class="btn btn-save">Save</div>
</form>
<?php footer(); ?>

<script>
    var product_id = <?php echo json_encode($model->prod_id); ?>;
//    var product_id = <?php //echo json_encode($model->combo->product_id); ?>//;

    $(document).ready(function () {

        var video_thumbnail = new mult_image($("input[name='video_thumbnail']"), $("#preview-thumbnail-container"));

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#target").click(function () {
            var price = $("input[name='price']").val();
            var name = $("input[name='name']").val();
            var slug = $("input[name='slug']").val();
            var id = <?=$model->combo->id?>;
            var errors = new Array();
            if ($.trim(price) > 0) {
                $("input[name='price']").css({
                    "border-color": ""
                });
            } else {
                $("input[name='price']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect price");
            }
            if ($.trim(name) == "") {
                $("input[name='name']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect name");
            } else {
                $("input[name='name']").css({
                    "border-color": ""
                });
            }
            if ($.trim(slug) == "") {
                $("input[name='slug']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect slug");
            } else {

                $.ajax({
                    async: false,
                    url: '/admin/combo/check_slug',
                    enctype: 'multipart/form-data',
                    method: 'POST',
                    data: {
                        slug: slug,
                        id: id
                    },
                    success: function (data) {

                        if (data == 1) {
                            errors.push("Slug must be uniq!");

                            $("input[name='slug']").css({
                                "border-color": "red"
                            });

                        } else {
                            $("input[name='slug']").css({
                                "border-color": ""
                            });
                        }
                    }
                });
            }
            var text = "";
            for (i = 0; i < errors.length; i++) {

                text += "<li>" + errors[i] + "</li>";
            }
            if (errors.length > 0) {
                $('html, body').animate({
                    scrollTop: 0
                });
                $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
                errors = [];
            } else {
                $(".form").submit();
            }
        });

        $("input.image").change(function () {
            readURL(this);
        });


        $("input[name='name']").on('keyup', function (e) {
            var val = $.trim($(this).val());
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });


        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='product_id[]']").val(product_id);
        $("select.multiselect").multiselect("rebuild");


        $("select[name='product_id[]']").on('change', function () {
            var product_id = $(this).val();
            var url = "<?=ADMIN_URL?>combo/add"
            $.post(url, {product_id: product_id}, function (data) {
                $("#products-list tbody tr").not('.originalProducts').remove();
                var list = JSON.parse(data);
                var sum = 0;
                for (key in list) {
                    var tr = $('<tr />');
                    $('<td />').appendTo(tr).html(list[key].name);
                    $('<td />').appendTo(tr).html(list[key].upc);
                    $('<td />').appendTo(tr).html(list[key].price);
                    tr.appendTo($("#products-list tbody"));
                    var price = list[key].price * 1;
                    sum += price;
                }
                $('input[name="price"]').val(sum.toFixed(2));
                $('input[name="msrp"]').val(sum.toFixed(2));
            })
        })
    })
</script>





















