<?php if (count($model->combo) > 0): ?>
    <div class="rox">
        <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                <tr>
                    <th width="10%">Image</th>
                    <th width="15%">Name</th>
                    <th width="10%">MSRP</th>
                    <th width="10%">Price</th>
                    <th width="10%">Availability</th>
                    <!--                  	<th width="15%" class="text-center">Edit</th>-->
                    <!--                  	<th width="15%" class="text-center">Delete</th>	-->
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;
                foreach ($model->combo as $obj) { ?>
                    <tr class="originalProducts" <?= $b ?>>
                        <td>
                            <?$img_path = UPLOAD_URL.'combo/'.$obj->featured_image?>
                            <a href="<?php echo ADMIN_URL; ?>combo/update/<?php echo $obj->id; ?>"><img
                                    src="<?php echo $img_path; ?>" width="50"/></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>combo/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>combo/update/<?php echo $obj->id; ?>"><?= number_format($obj->msrp, 2) ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>combo/update/<?php echo $obj->id; ?>"><?= number_format($obj->price, 2) ?></a>
                        </td>
                        <td>
                            <a href="<?php echo ADMIN_URL; ?>combo/update/<?php echo $obj->id; ?>"><?= $obj->getAvailability() ?></a>
                        </td>
                        <!--                 <td class="text-center">-->
                        <!--                   <a class="btn-actions" href="-->
                        <?php //echo ADMIN_URL; ?><!--products/update/--><?php //echo $obj->id; ?><!--">-->
                        <!--                   <i class="icon-pencil"></i> -->
                        <!--                   </a>-->
                        <!--                 </td>-->
                        <!--                 <td class="text-center">-->
                        <!--                   <a class="btn-actions" href="-->
                        <?php //echo ADMIN_URL; ?><!--products/delete/--><?php //echo $obj->id; ?><!--?token_id=-->
                        <?php //echo get_token();?><!--" onClick="return confirm('Are You Sure?');">-->
                        <!--                     <i class="icon-cancel-circled"></i> -->
                        <!--                   </a>-->
                        <!--                 </td>-->
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="box-footer clearfix">
                <div class='paginationContent'></div>
            </div>
        </div>
    </div>
<?php endif; ?>
