<style type="text/css">
    .legshot {
        border:1px black solid;
    }
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<?php if (count($model->sale_banners) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="banner-container">
            <thead>
            <tr>
                <th width="20%">Title</th>
                <th width="20%">Subtitle</th>
                <th width="20%">Url</th>
                <th width="20%">Display Order</th>
                <th width="10%" class="text-center">Edit</th>
                <th width="10%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->sale_banners as $obj) { ?>
                <tr data-id="<?=$obj->id?>">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sale_banners/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sale_banners/update/<?php echo $obj->id; ?>"><?php echo $obj->subtitle; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sale_banners/update/<?php echo $obj->id; ?>"><?php echo $obj->url; ?></a>
                    </td>
                    <td class="display-order-td">
                        <a href="<?php echo ADMIN_URL; ?>sale_banners/update/<?php echo $obj->id; ?>"><?php echo $obj->display_order; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>sale_banners/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>sale_banners/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'sale_banners/';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    function sort_number_display() {
        var counter = 1;
        $('#banner-container >tbody>tr').each(function (i, e) {
            $(e).find('td.display-order-td').html(counter);
            counter++;
        });
    }
    var adjustment;
    $('#banner-container').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        placeholder: '<tr class="placeholder"><td style="visibility: hidden; width: 100px; height: 100px;">.</td></tr>',
        onDrop: function ($item, container, _super, event) {

            if ($(event.target).hasClass('delete-product-image')) {
                $(event.target).trigger('click');
            }

            var ids = [];
            var tr_containers = $("#banner-container > tbody > tr");
            tr_containers.each(function (i, e) {
                ids.push($(e).data("id"));

            });
            $.post(site_url + 'sort_images', {ids: ids}, function (response) {
                sort_number_display();
            });
            _super($item);
        },
        onDragStart: function ($item, container, _super) {
            var offset = $item.offset(),
                pointer = container.rootGroup.pointer;

            adjustment = {
                left: pointer.left - offset.left,
                top: pointer.top - offset.top
            };

            _super($item, container);
        },
        onDrag: function ($item, position) {
            $item.css({
                left: position.left - adjustment.left,
                top: position.top - adjustment.top
            });
        }
    });
</script>