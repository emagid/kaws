<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->sale_banner->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>Title</label>
                <?php echo $model->form->editorFor("title"); ?>
            </div>
            <div class="form-group">
                <label>Subtitle</label>
                <?php echo $model->form->editorFor("subtitle"); ?>
            </div>
            <div class="form-group">
                <label>Url</label>
                <?php echo $model->form->editorFor("url"); ?>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
  
</form>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'sale_banners/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
});
</script>