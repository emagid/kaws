<style type="text/css">
    input.quantity_input {
        border: none;
        outline: none;
        background-color: transparent;
        font-family: inherit;
        font-size: inherit;
        width: 100%;
    }
    .quantity_input.updated {
        background-color: #5CFF46;
    }
    .quantity_input.failed {
        background-color: #FF5858;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box search-box">
            <div class="form-group">
                <label>Search</label>
                <div class="input-group">
                    <input id="search" type="text" name="search" class="form-control" placeholder="Search"/>
                    <span class="input-group-addon">
                        <i class="icon-search"></i>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label>Search by Serial No.</label>
                <div class="input-group">
                    <input id="search_by_serial" type="text" name="search" class="form-control" placeholder="Search"/>
                    <span class="input-group-addon">
                        <i class="icon-search"></i>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label>Range</label>
                <div class="input-group">
                    <input id="range" type="text" name="range" class="form-control" value="<?= !$model->start || !$model->end ? '': date('Y-m-d', strtotime($model->start)) . ' - ' . date('Y-m-d', strtotime($model->end)) ?>"/>
                    <span class="input-group-addon remove-date" style="cursor: pointer;">
                        <i class="icon-cancel-circled"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box transparent_box">
            <div class="form-group">
                <label>Show on page:</label>
                <select class="how_many form-control" name="how_many">
                    <? $arr = [10, 50, 100, 500, 1000];
                    foreach ($arr as $a) {?>
                        <option value="<?= $a ?>" <?= isset($_GET['how_many']) && $_GET['how_many'] == $a ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
            </div>

            <!--<div class="form-group">
                <label>Status Filter</label>
                <select class="status_filter form-control" name="status_filter">
                    <?/* $pFilter = [-1=>'All Orders','inc'=>'Incomplete','comp'=>'Complete'];
                    foreach ($pFilter as $k=>$a) {*/?>
                        <option value="<?/*= $k */?>" <?/*= isset($_GET['sf']) && $_GET['sf'] == $k ? 'selected' : '' */?>><?/*= $a */?></option>
                    <?/* } */?>
                </select>
            </div>-->
            <!--<div class="form-group">
                <label>Payment Filter</label>
                <select class="payment_filter form-control" name="payment_filter">
                    <option value="-1" <?/*= !isset($_GET['filter']) ? 'selected' : '' */?>>No Filter</option>
                    <?/* $pFilter = ['onl'=>'Online','sto'=>'Store','inv'=>'Invoice'];
                    foreach ($pFilter as $k=>$a) {*/?>
                        <option value="<?/*= $k */?>" <?/*= isset($_GET['filter']) && $_GET['filter'] == $k ? 'selected' : '' */?>><?/*= $a */?></option>
                    <?/* } */?>
                </select>
            </div>-->
        </div>
    </div>
    <!--    <div class="col-md-8">-->
    <!--        <div class="box transparent_box">-->
    <!--        </div>-->
    <!--        <div class="box transparent_box" style="align-content: center">-->
    <!--            <a href="--><?//= ADMIN_URL ?><!--orders/export" class="btn btn-warning" style="width: 49%">Export</a>-->
    <!--            <a class="btn btn-warning mass-email" style="width: 49%">Mass Email</a>-->
    <!--        </div>-->
    <!--    </div>-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script>
    $(document).ready(function(){

        function getQueryObj(str) {
            return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
        }

        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            var queryString;
            if(location.search && how_many != -1){
                queryString = getQueryObj(location.search);
                queryString.how_many = how_many;
            } else if(how_many == -1){
                queryString = getQueryObj(location.search);
                delete queryString.how_many;
            } else {
                queryString = {how_many:how_many};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/actual_inventory'+qs);
        });

        $('body').on('change', '.status_filter', function () {
            var status_filter = $(this).val();
            var queryString;
            if(location.search && status_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.sf = status_filter;
            } else if(status_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.sf;
            } else {
                queryString = {sf:status_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/actual_inventory'+qs);
        });
        $('#range').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo $model->start ? : date('Y-m-d')?>",
            endDate: "<?php echo $model->end ? : date('Y-m-d')?>"
        },function(start,end){
            end.hour(23);end.minute(59);end.second(59);
            var queryString = getQueryObj(location.search);
            queryString.t = start.unix()+','+end.unix();
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            window.location.replace('<?=ADMIN_URL.'actual_inventory'?>'+'?'+ $.param(queryString));
        });

        $('.remove-date').on('click',function(){
            var queryString = getQueryObj(location.search);
            if(queryString.t ) {
                delete queryString.t;
            }
            window.location.replace('<?=ADMIN_URL.'actual_inventory'?>'+'?'+ $.param(queryString));
        });
    })
</script>
<?php if (count($model->actual_inventorys) > 0) { ?>
    <div class="box-table horizontal-scrollable-table">
        <table class="table">
            <thead>
                <tr>
                    <th width='5%'>Amazon</th>
                    <th width='5%'>Ebay</th>
                    <th width='5%'>Square</th>
                    <th width='5%'>Warehouse</th>
                    <th width='5%'>Apt 310</th>
                    <th width='5%'>Website</th>
                    <? foreach (\Model\Custom_Inventory::getList() as $ci){?>
                        <th width='5%' class="custom"><?=$ci->name?></th>
                    <? } ?>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <?php foreach (array_merge([(object) array('name'=>'Amazon'),(object) array('name'=>'Ebay'),(object) array('name'=>'Square'),(object) array('name'=>'Warehouse'),(object) array('name'=>'Apt'),(object) array('name'=>'Website')],\Model\Custom_Inventory::getList()) AS $i => $inv){?>
                        <td>
                            <input id="view_<?=$inv->id?:$inv->name?>" class="inventory_view" type="checkbox" checked name="inventory" value="<?=$i+3?>"/>
                        </td>
                    <?}?>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="box box-table horizontal-scrollable-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='1%'>Id</th>
                <th width='20%'>Product (click to edit)</th>
                <th width='5%'>Amazon</th>
                <th width='5%'>Ebay</th>
                <th width='5%'>Square</th>
                <th width='5%'>Warehouse</th>
                <th width='5%'>Apt 310</th>
                <th width='5%'>Website</th>
                <? foreach (\Model\Custom_Inventory::getList() as $ci){?>
                    <th width='5%' class="custom"><?=$ci->name?></th>
                <? } ?>
                <th width='5%'>Total</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->actual_inventorys as $obj) {
                $obj->include_custom();?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>actual_inventory/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->product_id; ?>"><?php echo $obj->product_id ? \Model\Product::getItem($obj->product_id)->name : ''; ?></a></td>
                    <td><input class="quantity_input" data-product="<?=$obj->product_id?>" data-inventory="amazon" value="<?php echo $obj->amazon ? : ''; ?>" data-original="<?php echo $obj->amazon ? : ''; ?>"/></td>
                    <td><input class="quantity_input" data-product="<?=$obj->product_id?>" data-inventory="ebay" value="<?php echo $obj->ebay ? :''; ?>" data-original="<?php echo $obj->ebay ? :''; ?>"/></td>
                    <td><input class="quantity_input" data-product="<?=$obj->product_id?>" data-inventory="square" value="<?php echo $obj->square ? : ''; ?>" data-original="<?php echo $obj->square ? : ''; ?>"/></td>
                    <td><input class="quantity_input" data-product="<?=$obj->product_id?>" id="<?=$obj->product_id?>_wh" disabled readonly data-inventory="warehouse" value="<?php echo $obj->warehouse ? : ''; ?>" data-original="<?php echo $obj->warehouse ? : ''; ?>"/></td>
                    <td><input class="quantity_input" data-product="<?=$obj->product_id?>" data-inventory="apt_310" value="<?php echo $obj->apt_310 ? : ''; ?>" data-original="<?php echo $obj->apt_310 ? : ''; ?>"/></td>
                    <td><input class="quantity_input" data-product="<?=$obj->product_id?>" data-inventory="website" value="<?php echo $obj->website ? : ''; ?>" data-original="<?php echo $obj->website ? : ''; ?>"/></td>
                    <? foreach (\Model\Custom_Inventory::getList() as $ci){?>
                        <td><input class="quantity_input" data-product="<?=$obj->product_id?>" data-inventory="<? echo $ci->id ?>" value="<?=$obj->{"inventory_".$ci->id}?>" data-original="<?=$obj->{"inventory_".$ci->id}?>"/></td>
                    <? } ?>
                    <td><a><?php echo $obj->get_total_w_custom() ? : ''; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions-edit btn-solid-primary" href="<?php echo ADMIN_URL; ?>actual_inventory/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black"
                           href="<?php echo ADMIN_URL; ?>actual_inventory/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'actual_inventory';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
        var custom_inventories = [<?=implode(',',array_keys(\Model\Custom_Inventory::get_inventories()))?>];
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>actual_inventory/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    $.each(list,function(i,list){
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>actual_inventory/update/' + list.id).html(list.id));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list.product_id).html(list.product_name));
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.amazon+'" value="'+list.amazon+'"     data-inventory="amazon" />').html(list.amazon));
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.ebay+'" value="'+list.ebay+'"       data-inventory="ebay" />').html(list.ebay));
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.square+'" value="'+list.square+'"     data-inventory="square" />').html(list.square));
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.warehouse+'" value="'+list.warehouse+'"  data-inventory="warehouse" disabled readonly id="'+list.product_id+'_wh" />').html(list.warehouse));
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.apt_310+'" value="'+list.apt_310+'"    data-inventory="apt_310" />').html(list.apt_310));
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.website+'" value="'+list.website+'"    data-inventory="website" />').html(list.website));
                        let truetotal = parseInt(list.total);
                        for(q of custom_inventories){
                            $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.custom[q]+'" value="'+list.custom[q]+'" data-inventory="q" />').html(list.custom[q]));
                            truetotal += parseInt(list.custom[q]);
                        }
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>actual_inventory/update/' + list.id).html(truetotal));
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>actual_inventory/update/' + list.id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');

                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black').prop('href', '<?= ADMIN_URL;?>products/delete/' + list.id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');
                        tr.appendTo($("#data-list tbody"));
                    });
                    $('input[name=multibox]').prop('checked',false);
                    $('#check-all').prop('checked',false);

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
        $("#search_by_serial").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>actual_inventory/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords, serial:true}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    $('.inventory_view:checked').click();
                    if(list.length == 0){
                        $('.inventory_view').click();
                    }
                    let inventories = [];
                    $.each(list,function(i,list){
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>actual_inventory/update/' + list.id).html(list.id));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list.product_id).html(list.product_name));
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.amazon+'" value="'+list.amazon+'"     data-inventory="amazon" />').html(list.amazon)).hide();
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.ebay+'" value="'+list.ebay+'"       data-inventory="ebay" />').html(list.ebay)).hide();
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.square+'" value="'+list.square+'"     data-inventory="square" />').html(list.square)).hide();
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.warehouse+'" value="'+list.warehouse+'"  data-inventory="warehouse" disabled readonly id="'+list.product_id+'_wh" />').html(list.warehouse)).hide();
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.apt_310+'" value="'+list.apt_310+'"    data-inventory="apt_310" />').html(list.apt_310)).hide();
                        $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.website+'" value="'+list.website+'"    data-inventory="website" />').html(list.website)).hide();
                        let truetotal = parseInt(list.total);
                        for(q of custom_inventories){
                            $('<td />').appendTo(tr).html($('<input class="quantity_input" data-product="'+list.product_id+'" data-original="'+list.custom[q]+'" value="'+list.custom[q]+'" data-inventory="q" />').html(list.custom[q])).hide();
                            truetotal += parseInt(list.custom[q]);
                        }
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>actual_inventory/update/' + list.id).html(truetotal));
                        inventories = [...new Set([...inventories, ...list.inventories])];
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>products/update/' + list.id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');

                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black').prop('href', '<?= ADMIN_URL;?>products/delete/' + list.id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');
                        tr.appendTo($("#data-list tbody"));
                    });
                    for(inv of inventories){
                        if(inv == 'Apt_310') {
                            inv = 'Apt';
                        }
                        $('#view_'+inv).not(':checked').click();
                    }
                    $('input[name=multibox]').prop('checked',false);
                    $('#check-all').prop('checked',false);

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
        $(document).on('focus','.quantity_input',function(){
            $(this).removeClass('updated').removeClass('failed');
        });
        $(document).on('blur','.quantity_input:not(.updated)',function(){
            $(this).val($(this).data('original'));
        });
        $(document).on('keypress','.quantity_input',function(e){
            if(e.which == 13) {
                let input = $(this);
                let product_id = $(this).data('product');
                let inv = $(this).data('inventory');
                let val = $(this).val();
                let url = "<?php echo ADMIN_URL; ?>actual_inventory/update_inventory";
                let info = {product_id: product_id, inventory: inv, quantity: val}
                $.post(url,info,function(data){
                    if(data.status){
                        input.addClass('updated');
                        input.data('original',data.val);
                        $('#'+product_id+'_wh').val(data.wh);
                    } else {
                        input.addClass('failed');
                    }
                });
            }
        });
        $('.inventory_view').change(function(){
           let column = this.value;
           if($(this).is(":checked")){
               $('#data-list tr > *:nth-child('+column+')').show();
           } else {
               $('#data-list tr  > *:nth-child('+column+')').hide();
           }
        });
    })
</script>