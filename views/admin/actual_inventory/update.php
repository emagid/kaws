<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->actual_inventory->id; ?>"/>
    <input type="hidden" name="product_id" value="<?php echo $model->actual_inventory->product_id; ?>"/>
    <? $product = \Model\Product::getItem($model->actual_inventory->product_id); ?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab"
                                                      data-toggle="tab">General</a></li>
            <li role="presentation" ><a href="#custom-tab" aria-controls="general" role="tab"
                                                      data-toggle="tab">Custom Inventories</a></li>
            <li role="presentation"><a href="#items-tab" aria-controls="items" role="tab" data-toggle="tab">Items</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Product <?= $product->name ?></label>
                                <? //php echo $model->form->editorFor("product_id"); ?>
                            </div>
                            <div class="form-group">
                                <label>Amazon</label>
                                <?php echo $model->form->editorFor("amazon"); ?>
                            </div>
                            <div class="form-group">
                                <label>Ebay</label>
                                <?php echo $model->form->editorFor("ebay"); ?>
                            </div>
                            <div class="form-group">
                                <label>Square</label>
                                <?php echo $model->form->editorFor("square"); ?>
                            </div>
                            <div class="form-group">
                                <label>Warehouse</label>
                                <input type="text" name="warehouse" readonly
                                       value="<?= $model->actual_inventory->warehouse ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Apt 310</label>
                                <input type="text" name="apt_310" readonly
                                       value="<?= $model->actual_inventory->apt_310 ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <?php echo $model->form->editorFor("website"); ?>
                            </div>
                            <div class="form-group">
                                <label>Total</label>
                                <input type="hidden" name="total" value="<?=$model->actual_inventory->total?>">
                                <input type="text" readonly
                                       value="<?= $model->actual_inventory->get_total_w_custom(); ?>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="custom-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Custom</h4>
                            <? foreach(\Model\Custom_Inventory::getList() as $ci) {?>
                                <? $cq = \Model\Product_Inventory::getItem(null,['where'=>"product_id = $product->id AND inventory_id = $ci->id"]); ?>
                                <div class="form-group">
                                    <label><?= $ci->name ?></label>
                                    <input type="text" name="inventory[<?= $ci->id ?>]" value="<?= $cq == null ? '' : $cq->quantity ?>">
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="items-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Items</h4>
                            <div class="form-group">
                                <label><a href="/admin/receivings/update/<?=$model->actual_inventory->product_id?>">Product: <?= \Model\Product::getItem($model->actual_inventory->product_id)->name ?></a></label>
                                <br />
                                <label><a href="/admin/receivings/update/<?=$model->actual_inventory->product_id?>">Product id: <?= $model->actual_inventory->product_id?> </a></label>
                            </div>
                            <div class="row">
                                <? foreach ($model->receivings as $key => $receiveId) { ?>
                                    <div class="col-md-6">
                                        <h4><?= is_numeric($key)? \Model\Custom_Inventory::getItem($key)->name : $key?></h4>
                                        <?foreach ($receiveId as $id=>$items){?>
                                        <div class="row">
                                            <? foreach ($items as $item) { ?>
                                                <div class="form-group col">
                                                        <label>Name </label>
                                                        <p><?= strReplace($item->name,15,'...') ?></p>
                                                        <label>Serial No.</label>
                                                        <p><?= strReplace($item->sku,15,'...') ?></p>
                                                        <? if(strtolower(\Model\Inventory_Status::getItem($item->status)->name) != 'sold') {?>
                                                            <a class="btn rec_status btn-primary" data-status="5" data-receiving_id="<?=$item->id?>">Mark Sold</a>
                                                        <? } else {?>
                                                            <a class="btn rec_status btn-primary" data-status="2" data-receiving_id="<?=$item->id?>">Revert Sold status</a>
                                                        <? } ?>
                                                </div>
                                            <hr />
                                            <? } ?>
                                        </div>
                                        <?}?>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL . 'back_order/');?>;
    $(document).ready(function () {
        $('.rec_status').click(function (e){
           $.post('<?=ADMIN_URL?>receivings/change_receiving',{rec_id:$(this).data('receiving_id'), status:$(this).data('status'), redirectTo: "<?=ADMIN_URL?>/actual_inventory/update/<?php echo $model->actual_inventory->id; ?>"}, function(data){
               location.reload();
           });
        });
    });
</script>