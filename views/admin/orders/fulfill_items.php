<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input name="id" type="hidden" value="<?=$model->order->id?>"/>
    <div role="tabpanel">
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Product Fulfillment</h4>
                            <?$selectElement = [];
                            foreach ($model->order_products as $order_product){
                                $selectElement[] = "fulfillment[$order_product->id][]";
                                $receiveItems = \Model\Receive_Item::getList(['where'=>"order_product_id = $order_product->id"]);
                                $skuCount = count($receiveItems);?>
                                <div class="form-group">
                                    <label><?=$order_product->getProductName()?></label>
                                    <? if ($receiveItems) {
                                        foreach ($receiveItems as $receiveItem) {?>
                                            <p><?=$receiveItem->sku?></p>
                                        <? } ?>
                                    <? } ?>
                                    <?for ($i = $skuCount; $i < $order_product->quantity; $i++){?>
                                        <select name="fulfillment[<?=$order_product->id?>][]" class="form-control">
                                            <option value="-1">None</option>
                                            <?foreach (\Model\Receive_Item::getList(['where'=>"(order_product_id is null or order_product_id = 0) and receiving_id in (select id from receiving where product_id = $order_product->product_id)"]) as $item){?>
                                                <option value="<?=$item->id?>"><?=$item->sku?></option>
                                            <?}?>
                                        </select>
                                    <?}?>
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script>
    $(document).ready(function(){
        <?if($selectElement){
            foreach ($selectElement as $value){?>
                $('[name="<?=$value?>"]').on('change',function(){
                    var self = $(this);
                    var others = $('[name="<?=$value?>"]').not(this);
                    others.each(function(i,e){
                        e = $(e);
                        if(e.val() == self.val() && self.val() > 0){
                            $(e).val(-1);
                        }
                    });
                });
            <?}?>
        <?}?>
        $('select').each(function(){
            if($(this).find('option').length >= 2){
                $(this).find('option:eq(2)').attr('selected', 'selected');
            }
        });
    })
</script>
