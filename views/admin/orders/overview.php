<div class="row">
    <div class="col-md-16">
        <div class="box search-box">
            <div class="form-group">
                <label>Search</label>
                <div class="input-group">
                    <input id="search" type="text" name="search" class="form-control" placeholder="Search"/>
                    <span class="input-group-addon search-icon">
                        <i class="icon-search"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<!-- <div class="row">
  <label>Sales Tally</label>
  <div class="row col-md-24">
      <? foreach ($model->product_counts AS $pid => $prod){ ?>
          <div class="form-group col-md-8"><?=$prod->name?> (<?=$prod->color?>): <?=$prod->purchased_count?:'0'?></div>
      <? } ?>
  </div>
</div> -->
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        function getQueryObj(str) {
            return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
        }

        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            var queryString;
            if(location.search && how_many != -1){
                queryString = getQueryObj(location.search);
                queryString.how_many = how_many;
            } else if(how_many == -1){
                queryString = getQueryObj(location.search);
                delete queryString.how_many;
            } else {
                queryString = {how_many:how_many};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/orders'+qs);
        });

        $('body').on('change', '.payment_filter', function () {
            var pay_filter = $(this).val();
            var queryString;
            if(location.search && pay_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.filter = pay_filter;
            } else if(pay_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.filter;
            } else {
                queryString = {filter:pay_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/orders'+qs);
        });

        $('body').on('change', '.price_range', function () {
            var low_filter = $('.price_range[low_price]').val();
            var high_filter = $('.price_range[high_price]').val();
            var queryString;
            if(location.search && pay_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.filter = pay_filter;
            } else if(pay_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.filter;
            } else {
                queryString = {filter:pay_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/orders'+qs);
        });

        $('body').on('change', '.campaign_filter', function () {
            var campaign_filter = $(this).val();
            var queryString;
            if(location.search && campaign_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.cf = campaign_filter;
            } else if(campaign_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.cf;
            } else {
                queryString = {cf:campaign_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/orders'+qs);
        });

        $('body').on('change', '.status_filter', function () {
            var status_filter = $(this).val();
            var queryString;
            if(location.search && status_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.sf = status_filter;
            } else if(status_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.sf;
            } else {
                queryString = {sf:status_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/orders'+qs);
        });

        $('body').on('change', '.sort', function () {
            var status = $(this).val();
            var multipleStatus = status.join(',').replace(/,/g, '%2c');
            if (status == "all") {
                window.location.href = '/admin/orders';
            }
            else {
                window.location.href = '/admin/orders?status_show=' + multipleStatus;
            }
        });

        $('#range').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo $model->start ? : date('Y-m-d')?>",
            endDate: "<?php echo $model->end ? : date('Y-m-d')?>"
        },function(start,end){
            end.hour(23);end.minute(59);end.second(59);
            var queryString = getQueryObj(location.search);
            queryString.t = start.unix()+','+end.unix();
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            window.location.replace('<?=ADMIN_URL.'orders'?>'+'?'+ $.param(queryString));
        });

        $('.remove-date').on('click',function(){
            var queryString = getQueryObj(location.search);
            if(queryString.t ) {
                delete queryString.t;
            }
            window.location.replace('<?=ADMIN_URL.'orders'?>'+'?'+ $.param(queryString));
        });

        $('body').on('change', '#range', function () {
            var pay_filter = $(this).val();
            var queryString;
            if(location.search && pay_filter != -1){
                queryString = getQueryObj(location.search);
                $.each(queryString,function(i,e){
                    queryString[i] = decodeURIComponent(e);
                });
                queryString.filter = pay_filter;
            } else if(pay_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.filter;
            } else {
                queryString = {filter:pay_filter};
            }
            window.location.replace('/admin/orders?'+ $.param(queryString));
        });

        $('#pull_ship').click(function(e){
            $.post('/admin/orders/sync',{},function(data){
                console.log(data);
            })
        });
    });
</script>
<?php if (true) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="1%"><input type="checkbox" id="check-all"/></th>
                <th width="1%"></th>
                <th width="1%">
                    <?php
                    if (isset($_GET['sort']) && ($_GET['sort'] == 'id')) {

                        if ($_GET['sort'] == 'id' && $_GET['sort_param'] == 'asc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=id&sort_param=desc">ID</a>';
                        } elseif ($_GET['sort'] == 'id' && $_GET['sort_param'] == 'desc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=id&sort_param=asc">ID</a>';
                        }
                    } else {
                        echo '<a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=id&sort_param=desc">ID</a>';
                    } ?>
                </th>
                <th width="3%">
                    Campaign
                </th>
                <th width="3%">
                    <?php
                    if (isset($_GET['sort']) && ($_GET['sort'] == 'insert_time')) {
                        if ($_GET['sort'] == 'insert_time' && $_GET['sort_param'] == 'asc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=insert_time&sort_param=desc">Date</a>';
                        } elseif ($_GET['sort'] == 'insert_time' && $_GET['sort_param'] == 'desc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=insert_time&sort_param=asc">Date</a>';
                        }
                    } else {
                        echo '<a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=insert_time&sort_param=desc">Date</a>';
                    } ?>
                </th>
                <th width="5%">
                    <?php
                    if (isset($_GET['sort']) && ($_GET['sort'] == 'status')) {
                        if ($_GET['sort'] == 'Status' && $_GET['sort_param'] == 'asc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=status&sort_param=desc">Status</a>';
                        } elseif ($_GET['sort'] == 'status' && $_GET['sort_param'] == 'desc') {
                            echo ' <a  style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=status&sort_param=asc">Status</a>';
                        }
                    } else {
                        echo '<a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=status&sort_param=desc">Status</a>';
                    } ?>
                <th width="5%">Fulfillment Status</th>
                <th width="5%">Bill Name</th>
                <th width="10%">Products</th>
                <th width="5%">Difference</th>
                <th width="1%">
                    <?php
                    if (isset($_GET['sort']) && ($_GET['sort'] == 'total')) {
                        if ($_GET['sort'] == 'total' && $_GET['sort_param'] == 'asc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=total&sort_param=desc">Total</a>';
                        } elseif ($_GET['sort'] == 'total' && $_GET['sort_param'] == 'desc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=total&sort_param=asc">Total</a>';
                        }
                    } else {
                        echo '<a  style="color:rgba(27, 27, 32, 0.74);" href="/admin/orders?sort=total&sort_param=desc">Total</a>';
                    } ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->orders as $obj) {?>
                <tr class="originalProducts" <?= $style ?>>
                    <td>
                        <input type="checkbox" name="multibox" value="<?=$obj->id?>"/>
                    </td>
                    <td>
                        <? if (is_null($obj->viewed) || !$obj->viewed) { ?>
                            <button type="button" class="btn btn-success" disabled="disabled">New</button>
                        <? } ?>
                        <? if ($obj->tracking_number != '' ) { ?>
                            <button type="button" class="btn btn-group-justified" title="shipped" disabled="disabled"><i class="fa fa-truck"></i></button>
                        <? } ?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->id; ?>
                        </a>
                    </td>
                    <td>
                        <? if($obj->campaign_id != '') {?>
                            <? $campaign = \Model\Campaign::getItem($obj->campaign_id) ?>
                            <a href="<?php echo ADMIN_URL; ?>campaigns/update/<?php echo $obj->campaign_id; ?>">
                                <?= $campaign->name ?>
                            </a>
                        <? } else { ?>
                            <? $product = $obj->getOrderProducts()[0] ?>
                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $product->product_id; ?>">
                                <?= \Model\Product::getItem($product->product_id)->name ?>
                            </a>
                        <? } ?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->insert_time; ?>
                        </a>
                    </td>
                    <td>
                        <? $color = 'black';
                        if (in_array($obj->status, ['Paid', 'Shipped', 'Complete', 'Delivered'])) {
                            $color = '#41bb2d';
                        } else if (in_array($obj->status, ['Declined', 'Canceled', 'Incomplete PayPal','Banned','Limit Reached'])) {
                            $color = '#D94F74';
                        } ?>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"
                           style="color: <?= $color ?>;">
                            <?= $obj->status ?><? if ($obj->payment_status) {
                                echo "<br/>($obj->payment_status)";
                            } ?>
                        </a>
                    </td>
                    <td>
                        <? $color = 'black';
                        if ($obj->fulfillment_status == 'Ready') {
                            $color = '#41bb2d';
                        } ?>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"
                           style="color: <?= $color ?>;">
                        <?= $obj->fulfillment_status ?>
                        </a>
                        <? if(in_array($obj->fulfillment_status, ['Ready','Processed']) === false) {?>
                            <br />
                            <button class='btn mark_ready'
                               style="color: <?= $color ?>;" data-order_id="<?=$obj->id?>">
                                Mark Ready
                            </button>
                        <? } ?>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);


                            $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);


                            if (count(array_diff($shp, $blng)) == 0) {
                                ?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                            <? } else {
                                ?>
                                <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                        disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                            <? } ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? if (($orderProducts = $obj->getOrderProducts())) {
                                foreach ($orderProducts as $orderProduct) {
                                    $json = json_decode($orderProduct->details, true);
                                    if ($orderProduct->product_id) {
                                        if($orderProduct->clearance){
                                            echo '<span style="color:green">'.\Model\Product::getItem($orderProduct->product_id)->name."(".\Model\Product::getItem($orderProduct->product_id)->color.")".' (Clearance)</span>';
                                        } else {
                                            echo \Model\Product::getItem($orderProduct->product_id)->name." (".\Model\Product::getItem($orderProduct->product_id)->color.")";
                                        }
                                    } else if(isset($json['misc_name'])){
                                        echo $json['misc_name'] . '(Custom)';
                                    } else if($obj->ticket_id){
                                        echo $orderProduct->details;
                                    } else {
                                        echo \Model\Gift_Card::getItem($json['gift_card_id'])->name;
                                    }
                                    echo '<br/>';
                                }
//                                echo implode('<br/>',array_map(function($item){return \Model\Product::getItem($item->product_id)->name;},$orderProducts));
                            } ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            $<?php echo number_format($obj->difference,2); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? if ($obj->payment_method == 2) { ?>
                                <button type="button" class="btn btn-success" style="position:relative;"
                                        disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                            <? } else { ?>$<?php echo number_format($obj->total, 2);
                            } ?>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } else {
    echo "<center>No matches!<br><a href='' onclick='history.go(-1)'>Go back!</a></center>";
} ?>

<?php footer(); ?>

<script>
    var site_url = '/admin/orders/overview?<?if (isset($_GET)){unset($_GET['page']); echo urldecode(http_build_query($_GET).'&');}?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").change(function () {
            var url = "<?php echo ADMIN_URL; ?>orders/search";
            var keywords = $(this).val();
            if (keywords.length > 2) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    $.each(list,function(i,list){
                        if($.inArray(list.status,['Paid', 'Shipped', 'Complete', 'Delivered']) != -1){
                            var color = 'green';
                        } else if($.inArray(list.status,['Declined', 'Canceled', 'Incomplete PayPal']) != -1){
                            color = '#D94F74';
                        } else {
                            color = 'black'
                        }
                        if(list.payment_method == 2){
                            var total = '<button type="button" class="btn btn-success" style="position:relative;" disabled="disabled">$'+parseFloat(list.total).toFixed(2)+'</button>';
                        } else {
                            total = '$'+parseFloat(list.total).toFixed(2);
                        }
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html('<input type="checkbox" name="multibox" value="'+list.id+'"/>');
                        $('<td />').appendTo(tr).html(list.viewed ? '<button type="button" class="btn btn-success" disabled="disabled">New</button>' : '');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list.id).html(list.id));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list.id).html(list.campaign));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list.id).html(list.insert_time));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list.id).html(list.tracking_number));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list.id).css('color',color).html(list.status+'<br>('+list.payment_status+')'));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list.id).html(list.fulfillment_status));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list.id).html(list.bill_name));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list.id).html(list.products.join('<br>')));

                        $('<td />').appendTo(tr).html(total);
                        tr.appendTo($("#data-list tbody"));
                    });
                    $('input[name=multibox]').prop('checked',false);
                    $('#check-all').prop('checked',false);

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        }).change();
        $(".search-icon").click(function(){
            if(!isNaN($("#search").val())){
                window.location.href = "/admin/orders/update/"+$("#search").val();
            }
        })
    });
    $(function(){
        $('.mass-email').on('click',function(){
            if($('input[name=multibox]:checked').length == 0){
                alert('Select at least one order checkbox');
            } else {
                var arr = [];
                $('input[name=multibox]:checked').each(function(i,e){
                    arr.push(e.value);
                });
                var orders = {orders:arr.join(',')};
                window.location.replace('/admin/orders/mass_email?'+ $.param(orders));
            }
        })
    });
    $('#check-all').on('click',function(){
        if($(this).is(':checked')){
            $('input[name=multibox]:visible').prop('checked',true);
        } else {
            $('input[name=multibox]:visible').prop('checked',false);
        }
    });
    $(function () {
        $('input[name="daterange"]').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo \Carbon\Carbon::now()->subWeek();?>",
            endDate: "<?php echo \Carbon\Carbon::now();?>"
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $(".btn.mark_ready").click(function () {
           var orderId = $(this).data('order_id');
           var button = $(this);
           $.post('/admin/orders/mark_ready/',{orderId:orderId},function (data) {
               if(data.status){
                   button.siblings('a').css('color','#41bb2d').text('Ready');
                   button.hide();
               }
            });
        });
        $("#search_by_mpn").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>orders/search_by_mpn";
            var keywords = $.trim($(this).val());
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {

                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html();
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                        $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html(list[key].products);
                        $('<td />').appendTo(tr).html("$" + list[key].total);
                        /*   var editTd = $('<td />').addClass('text-center').appendTo(tr);
                         var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '
                        <?= ADMIN_URL;?>orders/update/' + list[key].id);
                         var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');*/
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>

