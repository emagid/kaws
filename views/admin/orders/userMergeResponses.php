<?php if (true) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="5%">     Customer email       </th>
                <th width="5%">     Response time        </th>
                <th width="3%">     Orders to join       </th>
                <th width="5%">     Orders to discard    </th>
                <th width="5%">     Products to fulfill  </th>
                <th width="5%">     Created Order Number </th>
                <th width="5%">            Details       </th>
                <th width="10%">           Finalize      </th>
            </tr>
            </thead>
            <tbody>
            <?php
                global $emagid;
                $db = $emagid->getDb();
            foreach ($model->records as $row) {
                $obj = (Object)$row;
                $obj->insert_time = new DateTime($obj->insert_time);
                $obj->insert_time = $obj->insert_time->format('M d, Y. H:i');?>
                <tr class="originalProducts">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/userResponse/<?php echo $obj->id; ?>">
                            <?php echo $obj->user_identifier; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/userResponse/<?php echo $obj->id; ?>">
                            <?php echo $obj->insert_time; ?>
                        </a>
                    </td>
                    <td>
                        <? $obj->wanted_orders = str_replace('{','',$obj->wanted_orders) ?>
                        <? $obj->wanted_orders = str_replace('}','',$obj->wanted_orders) ?>
                        <? $obj->wanted_orders = explode(',',$obj->wanted_orders); ?>
                        <?
                        foreach($obj->wanted_orders as $order_id){?>
                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $order_id; ?>">
                                <?php echo $order_id; ?>
                            </a>
                        <?}?>
                    </td>
                    <td>
                        <? $obj->unwanted_orders = str_replace('{','',$obj->unwanted_orders) ?>
                        <? $obj->unwanted_orders = str_replace('}','',$obj->unwanted_orders) ?>
                        <? $obj->unwanted_orders = explode(',',$obj->unwanted_orders); ?>
                        <?
                        foreach($obj->unwanted_orders as $order_id){?>
                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $order_id; ?>">
                                <?php echo $order_id; ?>
                            </a>
                        <?}?>
                    </td>
                    <td>
                        <?
                            $productSql = "SELECT DISTINCT p.name || ' (' || p.color || ')' as product_name FROM  product p, order_products op, order_verifications v WHERE p.id = op.product_id AND op.order_id::text = ANY (v.wanted_orders) AND v.id = $obj->id";
                            $results = $db->getResults($productSql);
                            foreach ($results as $product) {
                        ?>
                        <a>
                            <?=$product['product_name']?>
                        </a>
                        <? } ?>
                    </td>
                    <td>
                        <? if($obj->generated_order) {?>
                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->generated_order; ?>">
                                <?php echo $obj->generated_order; ?>
                            </a>
                        <? } ?>
                    </td>
                    <!--<td>
                        <a href="<?php /*echo ADMIN_URL; */?>orders/update/<?php /*echo $obj->id; */?>">
                            <?/* if (($orderProducts = $obj->getOrderProducts())) {
                                foreach ($orderProducts as $orderProduct) {
                                    $json = json_decode($orderProduct->details, true);
                                    if ($orderProduct->product_id) {
                                        if($orderProduct->clearance){
                                            echo '<span style="color:green">'.\Model\Product::getItem($orderProduct->product_id)->name."(".\Model\Product::getItem($orderProduct->product_id)->color.")".' (Clearance)</span>';
                                        } else {
                                            echo \Model\Product::getItem($orderProduct->product_id)->name." (".\Model\Product::getItem($orderProduct->product_id)->color.")";
                                        }
                                    } else if(isset($json['misc_name'])){
                                        echo $json['misc_name'] . '(Custom)';
                                    } else if($obj->ticket_id){
                                        echo $orderProduct->details;
                                    } else {
                                        echo \Model\Gift_Card::getItem($json['gift_card_id'])->name;
                                    }
                                    echo '<br/>';
                                }
//                                echo implode('<br/>',array_map(function($item){return \Model\Product::getItem($item->product_id)->name;},$orderProducts));
                            } */?>
                        </a>
                    </td>-->
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/userResponse/<?php echo $obj->id; ?>">
                            <button type="button" class="btn btn-success" style="position:relative;" disabled="disabled">DETAILS</button>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/userResponse/<?php echo $obj->id; ?>">
                            <button type="button" class="btn btn-success" style="position:relative;" disabled="disabled">FINALIZE</button>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>
<script>
    var site_url = '/admin/orders/userMergeResponses?<?if (isset($_GET)){unset($_GET['page']); echo urldecode(http_build_query($_GET).'&');}?>';
    var total_pages = <?= $model->total_pages;?>;
    var page = <?= $model->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function(){

    });
</script>

