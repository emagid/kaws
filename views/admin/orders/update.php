<style type="text/css">
    .order_content {
  margin-top: 50px;
}

.order_tabs {
  display: flex;
  justify-content: flex-start;
}

.order_tab {
  margin: 10px;
  padding: 10px 30px;
  padding-top: 13px;
  border-top: 1px solid #eaeaea;
  border-left: 1px solid #eaeaea;
  border-right: 1px solid #eaeaea;
  border-radius: 12px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
  cursor: pointer;
}

.order_tab.active_tab {
  background-color: black;
  color: white;
}

.tab_click {
  display: none;
}

.tab_click.general {
  display: block;
}

.alert {
    padding-top: 43px;
}

.save_btns {
  border-bottom: 1px solid #eaeaea;
  margin-bottom: 112px;
  background: white;
  position: fixed;
  right: 0;
  z-index: 100;
    top: 180px;
  width: 100%;
  padding-right: 32px;
  padding-bottom: 6px;
}

.save_btns .btn {
    transform: scale(.8);
}

.left-side {
  z-index: 102;
}

.tab-pane {
    display: none;
}

section.content .box {
    /*padding-bottom: 88px;*/
    margin-bottom: 20px;
    overflow: hidden;
    overflow-x: auto;
}

.form_stacker {
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
}

.form_stacker .form-stack {
    margin-right: 20px;
    padding-right: 20px;
    border-right: 1px solid #eaeaea;
}

.form_stacker .form-group:last-child {
    margin-right: 0;
    padding-right: 0;
    border-right: none;
}

input[type="text"], input[type="password"], textarea {
    font-size: 13px;
    height: 35px;
}

.form-group label {
    font-size: 13px;
}

.form-group {
    margin-bottom: 3px;
}


.form-group select.form-control {
    height: 35px;
    font-size: 13px;
    line-height: 34px;
}

.half_form {
    width: 49%;
    padding-right: 2%;
}

.form_stacker .half_form:last-child {
    padding: 0;
}

.box h4 {
    font-size: 18px;
    margin-bottom: 10px;
}

.moveup {
    transform: translateY(-25%);
}

@media (max-height: 700px) {
  .right-side > .content-header > h1 {
    font-size: 15px !important;
  }

  section.content>form button.btn.btn-save, .send_emails.button, #delete {
    transform: scale(.8);
  }

  .save_btns {
    top: 162px !important;
  }

  .skin-default .right-side > .content-header {
    padding-top: 0;
  }
}


@media (min-height: 701px) {

  .save_btns .btn, #delete {
    transform: scale(1) !important;
  }

  .save_btns {
    top: 193px !important;
  }
}

    @media print {
        /* Стиль для печати */
        h1, h2, p {
            color: #000; /* Черный цвет текста */
        }

        .nav {
            display: none;
        }

        .btn {
            display: none;
        }

        .qqq {
            color: black;
        }

        #general-tab, #billing-info-tab, #shipping-info-tab, #products-tab {
            display: block;
            visibility: visible;
        }

        .form-group {
            margin-bottom: 3px;
        }

        a {
            border: 0;
            text-decoration: none;
        }

        input[type="text"] {
            border-color: white;
        }

        .form-control {
            border: 2px solid blue;
        }

        a img {
            border: 0
        }

        a:after {
            content: " (" attr(href) ") ";
            font-size: 90%;
        }

        a[href^="/"]:after {
            content: " ";
        }
    }
</style>

<? if ($model->order->payment_method == 1) { ?>
<!--    <div class="row">-->
<!--        <div class="col-md-24">-->
<!--            <div class="box text-right">-->
<!--                <a href="--><?//= ADMIN_URL ?><!--orders/pay/--><?php //echo $model->order->id; ?><!--" class="btn btn-warning">VOID-->
<!--                    PAYMENT</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<? } ?>

<section class='orderUpdate_page'>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->order->id; ?>"/>
    <input type="hidden" name="campaign_id" value="<?php echo $model->order->campaign_id; ?>"/>
    <input type="hidden" name="old_status" value="<?= $model->order->status ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
<p class="qq" style=" text-align:center; color:#F9F9F9;">ORDER #<?php echo $model->order->id; ?><? if($model->original_order) { ?>( Duplicate of <a href="/admin/orders/update/<?=$model->original_order->id?>">order #<?=$new_order->id?> </a> )

<?} ?></p>
        <div class="tab-content">
            <center class='center save_btns' style="    margin-bottom: 112px;">
                <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
                <button type="submit" id="save" class="btn btn-save">Save</button>
                <!--                <button type="submit" id="resendConfirmation" class="btn btn-save">Save + Resend Email</button>-->
                <!--                <button class="btn btn-save" id="print">Print</button>-->
                <!--                <a href="/admin/orders/print_packing_slip/--><?//= $model->order->id ?><!--" class="btn btn-save">PRINT PACKING SLIP</a>-->
                <a class="btn btn-save btn-grey" id="delete">Delete</a>
                <button id='blacklist' class="btn btn-save btn-grey">BLACKLIST</a>
            </center>
        </div>
        <ul class="nav nav-tabs" role="tablist" style='    margin-top: 80px;'>
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#email-tab" aria-controls="general" role="tab" data-toggle="tab">Email</a></li>
<!--             <li role="presentation"><a href="#notes-tab" aria-controls="general" role="tab" data-toggle="tab">Notes</a></li>  -->
            <li role="presentation"><a href="#returnLabel-tab" aria-controls="general" role="tab" data-toggle="tab">Return Label</a></li>
            <li role="presentation"><a href="#matching-tab" aria-controls="general" role="tab" data-toggle="tab">Matching Orders</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            
                            <div class='form_stacker'>   
                                <div class="form-group form-stack">
                                    <label>Order ID</label>

                                    <p>#<?= $model->order->id ?> </p>
                                </div>
                                <? if($model->original_order) { ?>
                                    <div class="form-group form-stack">
                                        <label>Original Order ID</label>

                                        <p><a href="/admin/orders/update/<?=$model->original_order->id?>">#<?= $model->original_order->id ?></a></p>
                                    </div>
                                <? } ?>
                                <div class="form-group form-stack">
                                    <label>User Ip address</label>

                                    <p><?= $model->order->user_ip ?> </p>
                                </div>
                                <!---->
                                <!--                            <div class="form-group">-->
                                <!--                                <label>Receipt Id</label>-->
                                <!--                                --><?php //$links = explode(',', $model->order->payment_id);?>
                                <!--                                --><?php //foreach($links as $link){ ?>
                                <!--                                <a target="_blank" style="display: block; color: #2996cc;" href="https://squareup.com/receipt/preview/--><?//=$link?><!--">Receipt #--><?//=substr($link, 0, 4)?><!--</a>-->
                                <!--                                --><?php //} ?>
                                <!--                            </div>-->
                                <div class="form-group form-stack">
                                    <label>Date of order</label>

                                    <p><?= date('F j Y, g:i a',strtotime($model->order->insert_time)) ?> </p>
                                </div>
                                <!--                            <div class="form-group">-->
                                <!--                                <label>Fraud status</label><br>-->
                                <!--                                <span class="order_fraud_status">--><?//=$model->order->orderFraudStatus?><!--</span>-->
                                <!--                            </div>-->
                                <?if($model->order->error_message){?>
                                    <div class="form-group form-stack">
                                        <label>Error</label><br>
                                        <?= $model->order->error_message ?>
                                    </div>
                                <?}?>
                                <?if($model->order->payment_status){?>
                                    <div class="form-group form-stack">
                                        <label>Payment Status</label><br>
                                        <?= $model->order->payment_status ?>
                                    </div>
                                <?}?>
                                <div class="form-group form-stack">
                                    <label>Order Confirmation URL</label><br>
                                    <a href="<?= '/checkout/confirmation/'.$model->order->ref_num ?>" target="_blank">Click Here</a>
                                </div>
                                <div class="form-group form-stack">
                                    <label>Create Duplicate Order</label><br>
                                    <a href="<?= '/admin/orders/duplicate/'.$model->order->id ?>" target="_blank">Create</a>
                                </div>
                            <?if($model->order->gift_card){?>
                                <div class="form-group form-stack">
                                    <label>Gift Code</label><br>
                                    <?= $model->order->gift_card?>
                                </div>
                            <?}?>
                        </div>

                            <div class='form_stacker' style='margin-top: 10px;'>
                                <div class="form-group half_form">
                                    <label>Status</label>
                                    <? sort(\Model\Order::$status, SORT_LOCALE_STRING); ?>
                                    <select class="change form-control" name="status">
                                        <?php $current_status = $model->order->status; ?>
                                        <option value="">N/A</option>
                                        <? foreach (\Model\Order::$status as $key => $stat) { ?>
                                            <option <? if ($current_status == $stat) {
                                                echo "selected";
                                            } ?> value="<?= $stat ?>"><?= $stat ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <div class="form-group half_form">
                                    <label>Fulfillment Status</label>
                                    <? sort(\Model\Order::$fulfillment_status, SORT_LOCALE_STRING); ?>
                                    <select class="change form-control" name="fulfillment_status">
                                        <?php $current_status = $model->order->fulfillment_status; ?>
                                        <option value="">N/A</option>
                                        <? foreach (\Model\Order::$fulfillment_status as $key => $stat) { ?>
                                            <option <? if ($current_status == $stat) {
                                                echo "selected";
                                            } ?> value="<?= $stat ?>"><?= $stat ?></option>
                                        <? } ?>
                                    </select>

                                </div>
                            </div>
                            <div class='form_stacker'>
                                <div class="form-group half_form">
                                    <label>Customer </label>

                                    <div class="input-group">
                                        <? if (is_null($model->order->user)) { ?>
                                            <div class="input-group-addon">
                                                Guest
                                            </div>
                                            <input type="text" name="email" value="<?= $model->order->email ?>"/>
                                        <? } else { ?>
                                            <div class="input-group-btn">
                                                <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $model->order->user->id; ?>">
                                                    <button type="button" class="btn"><i class="icon-eye"></i></button>
                                                </a>
                                            </div>
                                            <input type="text" name="email" value="<?= $model->order->user->email ?>"/>
                                        <? } ?>
                                    </div>
                                </div>
                                <div class="form-group half_form">
                                    <label>Phone Number</label>
                                    <input name="phone" id="phone-us" type="text" value="<?= $model->order->phone ?>">

                                </div>
                            </div>
                            <div class="form-group">
                                <label>Comment</label>
                                <?= $model->order->comment ?>
                            </div>
                            <div class="row">
                                <div class="col-xs-22">
                                    <div class="form-group">
                                        <label>Tracking Number</label>
                                        <?php echo $model->form->textBoxFor('tracking_number'); ?>
                                    </div>
                                </div>
                                <!--                                <div class="col-xs-4">-->
                                <!--                                    <div class="form-group">-->
                                <!--                                        <label>Send Email</label>-->
                                <!--                                        <input type="checkbox" name="tracking_email"/>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </div>
                            <div class="form-group">
                                <label><a class="btn btn-warning app_ship_cost">Apply Shipping Costs</a></label>
                                <!--                                --><?//= $model->form->dropDownListFor('shipping_method', [0 => 'Standard', 80 => 'International'], '', ['disabled' => 'disabled'], ['class' => 'form-control']); ?>
                                <select class="app_ship_cost">
                                    <? foreach($model->shippings as $shipping) {?>
                                        <option value="<?=$shipping[1]?>" <?=$shipping[1] == $model->order->shipping_cost?'selected':''?>><?=$shipping[0]?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Shipping Cost</label>
                                <!--                                --><?//= $model->form->dropDownListFor('shipping_method', [0 => 'Standard', 80 => 'International'], '', ['disabled' => 'disabled'], ['class' => 'form-control']); ?>
                                <input type="number" step=".01" name="shipping_cost" value="<?=$model->order->shipping_cost?>">
                            </div>
                            <div class="form-group">
                                <label>Payment Method</label>
                                <?= $model->form->dropDownListFor('payment_method', \Model\Order::$payment_method, '',['class' => 'form-control']); ?>
                            </div>
                            <!--<div class="form-group">
                                <label>In Store</label>
                                <?/*= $model->form->checkBoxFor('in_store',1); */?>
                            </div>-->
                            <div class="row cc-info"
                                 style="<?= ($model->order->payment_method != 1 && $model->order->payment_method != null) ? 'display:none;' : ''; ?>">
                                <div class="col-xs-10 cc_number form-group">
                                    <label>Credit Card #</label>

                                    <div class="input-group">
                                        <?php echo $model->form->textBoxFor('cc_number', ['disabled' => 'disabled']); ?>
                                        <div class="input-group-btn">
                                            <button style='height:35px;' type="button" class="btn btn-success ">Show</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-10 form-group">
                                    <div class="col-xs-24">
                                        <label>Expiration</label>
                                    </div>
                                    <div>
                                        <div class="col-xs-16">
                                            <?= $model->form->dropDownListFor('cc_expiration_month', get_month(), '', ['disabled' => 'disabled', 'class'=>'form-control']); ?>
                                        </div>
                                        <div class="col-xs-8">
                                            <?= $model->form->dropDownListFor('cc_expiration_year', range(date('Y') - 15, date('Y') + 9), '', ['disabled' => 'disabled', 'class'=>'form-control']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4 form-group">
                                    <label>CCV</label>
                                    <?php echo $model->form->textBoxFor('cc_ccv', ['disabled' => 'disabled']); ?>
                                </div>
                            </div>
                            <? if (!is_null($model->order->coupon_code)) { ?>
                                <div class="form-group">
                                    <label>Coupon</label>

                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $model->coupon->id; ?>">
                                                <button type="button" class="btn"><i class="icon-eye"></i></button>
                                            </a>
                                        </div>
                                        <input type="text" disabled="disabled" value="<?= $model->coupon->code ?>"/>
                                    </div>
                                </div>
                            <? } ?>
                            <table class="table" style="margin-top: 10px;">
                                <tr>
                                    <td>Subtotal</td>
                                    <td id="sub">$<?php echo number_format($model->order->subtotal, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Tax</>
                                    <td id="tax">$<?php echo number_format($model->order->tax, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Duty</>
                                    <td id="tax">$<?php echo number_format($model->order->duty, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Shipping(charged to user)</td>
                                    <td id="shipping">$<?php echo number_format($model->order->shipping_cost, 2); ?></td>
                                </tr>
                                <!--<tr>
                                    <td>Shipping(paid by us)</td>
                                    <td id="shipping_charge">$<?php /*echo number_format($model->order->shipping_charge, 2); */?></td>
                                </tr>-->
                                <?if(false){?>
                                    <tr>
                                        <td>Discount</td>
                                        <!--                                    <td>$--><?//= number_format($model->savings, 2); ?><!--</td>-->
                                        <td id="discount" style="color: #abb0b3;">-$<?= number_format($model->order->coupon_amount, 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Gift Card</td>
                                        <td id="discount" style="color: #abb0b3;">-$<?= number_format($model->order->gift_card_amount, 2); ?></td>
                                    </tr>
                                <?}?>
                                <tr>
                                    <td>Current Total</td>
                                    <td id="total">$<?= number_format($model->order->total, 2) ?></td>
                                </tr>
                                <tr>
                                    <td>Difference</td>
                                    <td >$<?= number_format($model->order->difference, 2) ?></td>
                                </tr>
                                <tr>
                                    <td>Orignal Charged Total</td>
                                    <td>$<?= number_format($model->order->orig_total, 2) ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-24" style='padding: 0;' >
                            <div class="box">
                                <h4>Notes</h4>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Text</th>
                                        <th>Date</th>
                                        <th>Author</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <? foreach($model->order->getNotes() AS $note) {?>
                                        <?
                                        $note->insert_time = strtotime($note->insert_time);
                                        $note->insert_time = date('M d, Y - h:i A',$note->insert_time-14400);
                                        $admin = \Model\Admin::getItem($note->admin_id);
                                        if($note->admin_id == 0){
                                            $admin = (object)['username'=>''];
                                        }
                                        ?>
                                        <tr>
                                            <td><?=$note->note?></td>
                                            <td><?=$note->insert_time?></td>
                                            <td><?=$admin->username?></td>
                                        </tr>
                                    <? } ?>
                                    </tbody>
                                </table>

                                <div class="form-group">
                                    <label>New Note</label>
                                    <?php echo $model->form->textAreaFor("note", ['rows' => '10']); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?
                    $shp = array($model->order->ship_first_name, $model->order->ship_last_name, $model->order->ship_address, $model->order->ship_address2,
                        $model->order->ship_country, $model->order->ship_city, $model->order->ship_state, $model->order->ship_zip);


                    $blng = array($model->order->bill_first_name, $model->order->bill_last_name, $model->order->bill_address, $model->order->bill_address2,
                        $model->order->bill_country, $model->order->bill_city, $model->order->bill_state, $model->order->bill_zip);
                    $isSame = ($shp === $blng);
                    ?>


                    <div class="col-md-<?=$isSame ? '12': '6'?>">
                        <div class="box">
                            <h4>Shipping
                                <? if ($isSame) {
                                    echo " and Billing <center id='same'><font color='#ccc' style='font-size: 12px'>SAME</font></center>";
                                } ?>
                            </h4>

                            <div class="await_stacker">                                
                                <div class="form-group await_stack">
                                    <label>First Name</label>
                                    <?php echo $model->form->textBoxFor('ship_first_name'); ?>
                                </div>
                                <div class="form-group await_stack">
                                    <label>Last Name</label>
                                    <?php echo $model->form->textBoxFor('ship_last_name'); ?>
                                </div>
                            </div>
                            <div class="await_stacker"> 
                                <div class="form-group await_stack">
                                    <label>Address</label>
                                    <?php echo $model->form->textBoxFor('ship_address'); ?>
                                </div>
                                <div class="form-group await_stack">
                                    <label>Address 2</label>
                                    <?php echo $model->form->textBoxFor('ship_address2'); ?>
                                </div>
                            </div>
                            <div class="await_stacker"> 
                                <div class="form-group await_stack">
                                    <label>City</label>
                                    <?php echo $model->form->textBoxFor('ship_city'); ?>
                                </div>
                                <div class="form-group await_stack">
                                    <label>State</label>
                                    <!--                                --><?//= $model->form->dropDownListFor('ship_state', get_states(), 'Select', ['disabled' => 'disabled']); ?>
                                    <?= $model->form->textBoxFor('ship_state'); ?>
                                </div>
                            </div>
                            <div class="await_stacker"> 
                                <div class="form-group await_stack">
                                    <label>Zip Code</label>
                                    <?php echo $model->form->textBoxFor('ship_zip'); ?>
                                </div>
                                <div class="form-group await_stack">
                                    <label>Country</label>
                                    <select name="ship_country" class="form-control">
                                        <? foreach (get_countries() as $code=>$country) {?>
                                            <? $selected = strtolower(trim($model->order->ship_country)) == strtolower(trim($code)) || strtolower(trim($model->order->ship_country)) == strtolower(trim($country))?'selected':''?>
                                            <option value="<?=$code?>" <?=$selected?>><?=$country?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" <? if ($isSame) {
                        echo "style='display:none;'";
                    } else {
                    } ?>>
                        <div class="box">
                            <h4>Billing</h4>

                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->textBoxFor('bill_first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->textBoxFor('bill_last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('bill_address'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('bill_address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('bill_city'); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <!--                                --><?//= $model->form->dropDownListFor('bill_state', get_states(), 'Select', ['disabled' => 'disabled'], ['class' => 'form-control']); ?>
                                <?= $model->form->textBoxFor('bill_state'); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('bill_zip'); ?>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <select name="bill_country" class="form-control">
                                    <? foreach (get_countries() as $code=>$country) {?>
                                        <? $selected = strtolower(trim($model->order->bill_country)) == strtolower(trim($code)) || strtolower(trim($model->order->bill_country)) == strtolower(trim($country))?'selected':''?>
                                        <option value="<?=$country?>" <?=$selected?>><?=$country?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Products</h4>
                            <!--                            <h4>Products<a href="/admin/orders/fulfill_items/--><?//=$model->order->id?><!--" style="float: right;">Fulfill Items</a></h4>-->
                            <table class="table">
                                <thead>
                                <th></th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Unit Price</th>
                                <th>Item Sku</th>
                                <!--                                <th>Color</th>-->
                                <!--                                <th>Size</th>-->
                                <!--                                <th>Progress</th>-->
                                <!--                                <th>View</th>-->
                                </thead>
                                <tbody>
                                <? foreach ($model->order_products as $order_product) { ?>
                                    <tr>
                                        <td><?php
                                            if(!is_null($order_product->details) && $order_product->details != ''){
                                                $productDetails = json_decode($order_product->details,true);
                                            } else {
                                                $productDetails = ['color'=>'','size'=>''];
                                            }
                                            $img_path = ADMIN_IMG . 'logo.png'; //TODO st-dev default product image
                                            $color = isset($productDetails['color']) && $productDetails['color'] ? $productDetails['color']: '';

                                            if($order_product->type == 'product'){
                                                $product = $order_product->product->id ? \Model\Product::getItem($order_product->product->id) : null;
                                                $img_path = $product ? $product->featuredImage(null,$color) : '';
                                            } else if($order_product->type == 'misc'){
                                                $img_path = "/content/uploads/products/".$order_product->product->featured_image;
                                            } else if($order_product->type == 'gift card'){
                                                $img_path = UPLOAD_URL . 'gift_cards/' . $order_product->product->image;
                                            } else {
                                                $img_path = '';
                                            }
                                            if($order_product->clearance){
                                                $style = 'style="color: green"';
                                                $name = ' (Clearance)';
                                            } else {
                                                $style = $name = '';
                                            }
                                            ?>
                                            <div class="mouseover-thumbnail-holder">
                                                <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product_id; ?>">
                                                    <img src="<?php echo $img_path; ?>" width="50"/></a>
                                                <img class="large-thumbnail-style" src="<?php echo $img_path; ?>"/></a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= $order_product->product->id ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>" <?=$style?>><?= $order_product->product_id?$order_product->product->name.$name." ({$order_product->product->color})":$order_product->details ?></a>
                                        </td>
                                        <td>
                                            <input type="number" id="op_<?=$order_product->id?>" data-price="<?=$order_product->product->price?>" data-orig="<?= $order_product->quantity ?>" data-opid="<?=$order_product->id?>" value="<?= $order_product->quantity ?>" />
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= number_format($order_product->quantity*$order_product->unit_price, 2) ?></a>
                                        </td>
                                        <td>
                                            <a class="btn btn-warning update_op" data-opid="<?=$order_product->id?>">Update</a>
                                        </td>
                                        <!--<td>
                                            <?/*foreach (\Model\Receive_Item::getList(['where'=>"order_product_id = $order_product->id"]) as $item){*/?>
                                                <p><?/*=$item->sku*/?></p>
                                            <?/*}*/?>
                                        </td>-->
                                        <!--
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= isset($productDetails['color']) ? \Model\Color::getItem($productDetails['color'])->name : ''?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= isset($productDetails['size']) ? \Model\Size::getItem($productDetails['size'])->us_size : ''?></a>
                                        </td>

                                        <td>
                                            <select class="work-in-progress form-control" data-id="<?=$order_product->id?>">
                                                <option value="0">No progress</option>
                                                <?$ar = [1=>'1. Materials',2=>'2. Stitched',3=>'3. Mold'];
                                        foreach($ar as $k=>$a){?>
                                                    <option value="<?=$k?>" <?=$k == $order_product->status ? 'selected': ''?>><?=$a?></option>
                                                <?}?>
                                            </select>
                                        </td>
                                         -->
                                        <!--                                        <td>-->
                                        <!--                                            <button type="button" class="submit-wip">Send Email</button>-->
                                        <!--                                        </td>-->
                                        <!--                                        <td><a target="_blank" class="btn-actions"-->
                                        <!--                                               href="/products/--><?php //echo $order_product->product->slug; ?><!--"><i-->
                                        <!--                                                    class="icon-eye"></i></a></td>-->
                                    </tr>
                                <? } ?>

                                <tr>
                                    <th colspan="2">
                                        <a class="btn btn-warning disabled" id="add_op">
                                            Add Product
                                        </a>
                                    </th>
                                    <td>
                                        <select id="new_op" >
                                            <option value>
                                                Select a product to add
                                            </option>
                                            <? foreach ($model->campaign->products AS $product) {?>
                                                <option value="<?=$product->id?>" data-price="<?=$product->price?>">
                                                    <?=$product->name." ($product->color)"?>
                                                </option>
                                            <? } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" id="new_quantity" value="1" />
                                    </td>
                                    <td>
                                        $<span id="new_op_cost">0.00</span>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Transactions</h4>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Amount</th>
                                    <th>Trans. ID</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($model->transactions as $trans){ ?>
                                    <? $authTrans = $trans->transaction?:null ?>
                                    <tr>
                                        <td><?=date('M j, Y h:i a', strtotime($trans->insert_time))?></td>
                                        <td><?=$trans->type?><?=$trans->import?' (Imported)':''?></td>
                                        <td><?= $authTrans?$authTrans->transaction->transactionStatus:'';?></td>
                                        <td><?=$trans->amount?></td>
                                        <td><?=$trans->ref_tran_id?></td>
                                        <td><?
                                            if($authTrans){
                                                if(in_array($authTrans->transaction->transactionStatus,['authorizedPendingCapture','capturedPendingSettlement','refundPendingSettlement']) ){ ?>
                                                    <a class="btn btn-warning void" data-trans-id="<?=$trans->id?>" data-id="<?=$trans->ref_tran_id?>">Cancel/Void</a>
                                                <? } else if ($authTrans->transaction->transactionStatus == 'settledSuccessfully'){ ?>
                                                    <a class="btn btn-warning refund" data-amount="<?=$trans->amount?>" data-trans-id="<?=$trans->id?>" data-id="<?=$trans->ref_tran_id?>">Refund</a>
                                                <? }
                                            }
                                            if($trans->import){
                                                echo "<a class='btn btn-warning removeTrans' data-id='{$trans->id}' '><i class=\"icon-cancel-circled\"></i></a>";
                                            }
                                            ?></td>
                                    </tr>
                                <? } ?>
                                <tr>
                                    <td colspan="2">Add Transaction from Authorize</td>
                                    <td colspan="1"><input id="auth_import_id" placeholder="Transaction Id"/></td>
                                    <td colspan="1">Automatically adjust price? <input id="import_adjPrice" type="checkbox" checked/></td>
                                    <td colspan="1"><a class="btn btn-warning" id="import_btn">Import Transaction</a></td>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th colspan="2">Add A New Charge </th>
                                </tr>
                                <tr>
                                    <th colspan="1">Default amount = difference between current total and calculated:</th>
                                    <th><a class="btn apply_btn btn-grey" href="/admin/orders/apply_breakdown/<?=$model->order->id?>" style="color:black !important">Apply calc'ed numbers</a></th>
                                </tr>
                                <? $bd = $model->order->calculate_total();
                                foreach ($bd['breakdown'] AS $i => $val) { ?>
                                    <tr>
                                        <td><?=$bd['breakdown'][$i]?></td>
                                        <td><?=$bd['charges'][$i]?></td>
                                    </tr>
                                <? } ?>
                                <tr>
                                    <td> Total </td>
                                    <td> $<?=number_format(array_sum($bd['charges']),2)?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Difference:
                                    </td>
                                    <td>
                                        <input name="difference" type="number" value="<?=number_format(array_sum($bd['charges'])- $model->order->total,2)?>"/>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <form id="charge_form">
                                        <td><input id='form_charge_amt' value="<?=number_format(array_sum($bd['charges'])- $model->order->total,2)?>"/></td>
                                        <td><a class="btn btn-danger" onclick="charge()">Authorize<a/></td>
                                    </form>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                        <!-- <div class="col-md-12 notes_box" >
                            <div class="box">
                                <h4>Notes</h4>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Text</th>
                                        <th>Date</th>
                                        <th>Author</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <? foreach($model->order->getNotes() AS $note) {?>
                                        <?
                                        $note->insert_time = strtotime($note->insert_time);
    //                                    $note->insert_time->setTimezone(new DateTimeZone('America/New_York'));
                                        $note->insert_time = date('M d, Y - h:i A',$note->insert_time-14400);
                                        $admin = \Model\Admin::getItem($note->admin_id);
                                        if($note->admin_id == 0){
                                            $admin = (object)['username'=>''];
                                        }
                                        ?>
                                        <tr>
                                            <td><?=$note->note?></td>
                                            <td><?=$note->insert_time?></td>
                                            <td><?=$admin->username?></td>
                                        </tr>
                                    <? } ?>
                                    </tbody>
                                </table>

                                <div class="form-group">
                                    <label>New Note</label>
                                    <?php echo $model->form->textAreaFor("note", ['rows' => '10']); ?>
                                </div>

                            </div>
                        </div> -->
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="email-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4> Emails </h4>

                            <h2>Returns</h2>
                            <div class="form-group">
                                <label>Send Email Link to return Label?</label>
                                <input type="checkbox" name="send_return"/>
                            </div>

                            <h2>Email Templates</h2>
                            <div class="form-group">
                                <label>Resend Confirmation Email</label>
                                <input type="radio" class="email_radio" name="send_confirm"  id="send_confirm" onclick="$('.email_radio:not(#send_confirm)').prop('checked',false)" />
                            </div>
                            <div class="form-group">
                                <label>Send Confirmation Update Email</label>
                                <input type="radio" class="email_radio" name="send_update"   id="send_update"   onclick="$('.email_radio:not(#send_update)').prop('checked',false)" />
                            </div>
                            <div class="form-group">
                                <label>Send Tracking Email</label>
                                <input type="radio" class="email_radio" name="send_shipping" id="send_shipping" onclick="$('.email_radio:not(#send_shipping)').prop('checked',false)" />
                            </div>
                            <div class="form-group">
                                <label>Send Remaining Balance Email</label>
                                <input type="radio" class="email_radio" name="send_balance" id="send_balance" onclick="$('.email_radio:not(#send_balance)').prop('checked',false)" />
                            </div>
                            <div class="form-group">
                                <label>Send Possible Duplicate Email</label>
                                <input type="radio" class="email_radio" name="send_dupe" id="send_dupe" onclick="$('.email_radio:not(#send_dupe)').prop('checked',false)" />
                            </div>

                            <h2>Custom</h2>
                            <div class="form-group">
                                <label><font >Mail Title</font></label>
                                <?php echo $model->form->textBoxFor("mailtitle",['id' => 'mailtitle','disabled'=>'true']); ?>
                            </div>
                            <div class="form-group">
                                <label><font>Subject </font></label>
                                <?php echo $model->form->textBoxFor("subject",['id' => 'subject','disabled'=>'true']); ?>
                            </div>
                            <div class="form-group">
                                <label><font>Mail </font></label>
                                <?php echo $model->form->textAreaFor("mail", ['id' => 'mail', 'rows' => '10','disabled'=>'true']); ?>
                            </div>


                            <!--<div class="form-group">
                                <label>Overwrite default values?</label>
                                <input name="emailOverwrite[NAME]"          placeholder="Customer Name" />
                                <input name="emailOverwrite[SUBTOTAL]"      placeholder="Subtotal" />
                                <select name="emailOverwrite[TAXNAME]" id="duty_tax_name">
                                    <option disabled selected value="">Display duty or tax?</option>
                                    <option value="Duty & Imports" data-feeName="Duty Amt">Duty & Imports</option>
                                    <option value="Taxes" data-feeName="Tax Amt">Taxes</option>
                                </select>
                                <input name="emailOverwrite[SHIPPINGFEE]"   placeholder="Shipping Fee" />
                                <input name="emailOverwrite[TAXFEE]" id="taxAmt" placeholder="Tax Amt" />
                                <input name="emailOverwrite[TOTAL]"         placeholder="Order Total" />
                            </div>-->

                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="returnLabel-tab">
                <div class="row">
                    <div class="col-md-24" >
                        <div class="box">
                            <h4>Return Labels</h4>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Tracking Number</th>
                                    <th>Creation Date</th>
                                    <th>Shipped Date</th>
                                    <th>Link to Label</th>
                                </tr>
                                </thead>
                                <tbody id="labels">
                                <? foreach($model->order->getLabels() AS $label) {?>
                                    <?
                                    $label->insert_time = strtotime($label->insert_time);
                                    $label->insert_time = date('M d, Y  g:i A',$label->insert_time-14400);
                                    $label->shipped_time = strtotime($label->shipped_time);
                                    $label->shipped_time = date('M d, Y  g:i A',$label->shipped_time-14400);
                                    $admin = \Model\Admin::getItem($note->admin_id);
                                    ?>
                                    <tr>
                                        <td><?=\Model\Labels::$types[$label->type]?> <?=$label->type==2 && $label->claimed == 0?"(Unclaimed)":''?></td>
                                        <td class="label_tracking"><?=$label->tracking_number?></td>
                                        <td><?=$label->insert_time?></td>
                                        <td><input type="text" class='label_ship_time' id="<?=$label->id?>" value="<?=$label->shipped_time?>" disabled /></td>
                                        <td><? if($label->image) {?><a href="<?=$label->image()?>" target="_blank" class="btn">View</a><?}?></td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                            <div>
                                <h1>Generate New Label</h1>
                                <div class="form-group">
                                    <label>Label Type</label>
                                    <select id="label_type">
                                        <? foreach (\Model\Labels::$types as $id => $type) {?>
                                            <option value="<?=$id?>"><?=$type?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Height<sup>(in)</sup></label>
                                    <input type="number" id="label_height" />
                                </div>
                                <div class="form-group">
                                    <label>Width<sup>(in)</sup></label>
                                    <input type="number" id="label_width" />
                                </div>
                                <div class="form-group">
                                    <label>Length<sup>(in)</sup></label>
                                    <input type="number" id="label_length" />
                                </div>
                                <div class="form-group">
                                    <label>Weight<sup>(lbs)</sup></label>
                                    <input type="number" id="label_weight" />
                                </div>
                                <a class="btn btn-actions" id="label_submit">Generate</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- <div role="tabpanel" class="tab-pane" id="matching-tab">
                <?
                    // global $emagid;
                    // $db = $emagid->getDb();
                    // $matchingSql = "SELECT o.id, o.charge_added, o.status, o.fulfillment_status, o.total, o.ship_first_name || ' ' || o.ship_last_name AS name, o.user_ip, o.email, o.phone,
                    //                          count(brc.*) AS brown_count, count(grc.*) AS grey_count, count(blc.*) AS black_count, o.trans_id
                    //                 FROM public.order o
                    //                   LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.product_id = 105 AND brc.active = 1
                    //                   LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.product_id = 106 AND grc.active = 1
                    //                   LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.product_id = 107 AND blc.active = 1
                    //                   INNER JOIN public.order ord ON ord.id = {$model->order->id}
                    //                 WHERE o.campaign_id = 6 AND (
                    //                   TRIM(LOWER(o.email))= TRIM(LOWER(ord.email))OR
                    //                   TRIM(LOWER(o.phone))= TRIM(LOWER(ord.phone))OR (
                    //                     TRIM(LOWER(o.ship_first_name)) = TRIM(LOWER(ord.ship_first_name))AND
                    //                     TRIM(LOWER(o.ship_last_name))  = TRIM(LOWER(ord.ship_last_name))AND 
                    //                     TRIM(LOWER(o.ship_zip))        = TRIM(LOWER(ord.ship_zip))
                    //                   )
                    //                 ) AND o.active = 1 AND ord.active = 1
                    //                 GROUP BY o.id, o.charge_added, ord.id, o.ship_first_name, o.ship_last_name, o.user_ip, o.email, o.phone
                    //                 ORDER BY CASE WHEN o.id = ord.id THEN 0 ELSE 1 END ASC, o.status, o.trans_id ";
                    // $doResults = $db->getResults($matchingSql);
                ?>
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <div class="form-group">
                                <h4>Possible Matching orders: <?//count($doResults)?> </h4>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        Current Order:
                                    </div>
                                    <div class="box box-table">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th width="10%">Order No.</th>
                                                    <th width="10%">Status</th>
                                                    <th width="10%">Amount Paid.</th>
                                                    <th width="10%">Cust. Name</th>
                                                    <th width="10%">IP Addr</th>
                                                    <th width="10%">Email</th>
                                                    <th width="10%">Phone</th>
                                                    <th width="10%">Brown Count. </th>
                                                    <th width="10%">Grey Count. </th>
                                                    <th width="10%">Black Count. </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <? $thisOrder = $doResults[0] ?>
                                                   <td><?=$thisOrder['id']?></td>
                                                   <td><?=$thisOrder['status']?></td>
                                                   <td><span style="<?=$thisOrder['charge_added']?'font-weight:bold':''?>">$<?=number_format($thisOrder['total'],2)?></span></td>
                                                   <td><?= $thisOrder['name']?></td>
                                                   <td><?= $thisOrder['user_ip']?></td>
                                                   <td><?= $thisOrder['email']?></td>
                                                   <td><?= $thisOrder['phone']?></td>
                                                   <td><?= $thisOrder['brown_count']?></td>
                                                   <td><?= $thisOrder['grey_count']?></td>
                                                   <td><?= $thisOrder['black_count']?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="rox">
                                    <div class="box box-table" style="min-height:500px">
                                        <table>
                                            <thead>
                                            <tr>
                                                <th width="10%">Order No.</th>
                                                <th width="10%">Status</th>
                                                <th width="6%">Amount Paid. </th>
                                                <th width="10%">Cust. Name</th>
                                                <th width="10%">IP Addr</th>
                                                <th width="10%">Email</th>
                                                <th width="10%">Phone</th>
                                                <th width="5%">Brown Count. </th>
                                                <th width="5%">Grey Count. </th>
                                                <th width="5%">Black Count. </th>
                                                <th width="10">Transaction Id</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
    <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
    <? //=$model->order->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
</form>
</section>

<?php footer(); ?>
<!--<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>-->
<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }
    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }
    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }
    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>


<script src="<?= FRONT_LIBS ?>masonry-docs/masonry.pkgd.min.js"></script>

<script>

    // $('.row').masonry({
    //   // options
    //   itemSelector: '.col-md-12',
    //   itemSelector: '.col-md-6'
    // });


    function charge(){
        if(confirm('Are you sure?')){
            data = {
                charge: $('#form_charge_amt').val(),
                orderID: <?=$model->order->id?>
            };
            $.post('/admin/orders/charge',data,function (data) {
                if(data.status == true){
                    window.location.reload();
                }
            });
        }
    }

    $(function () {
        $('#label_submit').click(function(){
           var data = {
               id:<?=$model->order->id?:0?>,
               weight:$('#label_weight').val()?$('#label_weight').val():2,
               length:$('#label_height').val()?$('#label_height').val():0,
               width:$('#label_width').val()?$('#label_width').val():0,
               height:$('#label_length').val()?$('#label_length').val():0,
               type:$('#label_type').val()?$('#label_type').val():0
           }
           $.post('/admin/orders/generateLabel',data,function(data){
               if(data.status){
                   let label = $('<tr>');
                   label.append( $('<td>').text(data.label.type) );
                   label.append( $('<td>').text(data.label.tracking) );
                   label.append( $('<td>').text(data.label.insert_time) );
                   label.append( $('<td>').html("<input type='text' class='label_ship_time' id = '"+data.label.id+"' value='"+data.label.shipped_time+"' disabled />") );
                   label.append( $('<td>').append($("<a href='"+data.label.img+"' target='_blank'>View</a>")));
                   label.appendTo('#labels');
                   $('input[name=tracking_number]').val(data.label.tracking);
                   //buildNotification(true,'Label generated');
               } else {
                   buildNotification(false,data.message);
                   alert(data.message);
               }
           },'json');
        });
        $('#delete').click(function () {
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                $.ajax({
                    'type': 'POST',
                    'url': '/admin/orders/unlock',
                    'data': {
                        pwd: pwd,
                        orderId: <?=$model->order->id?>
                    },
                    success: function (data) {
                        data = JSON.parse(data);

                        if (data == "false") {
                            alert('Incorrect code!');
                        } else {
                            window.location.replace(data);
                        }
                    }
                })
            }


        });
        $('#import_btn').click(function(){
           var tran_id = $('#auth_import_id').val();
           var adj_price = $('#import_adjPrice').is(':checked')?1:0;
           var orderId = <?=$model->order->id?>;
           $.post('/admin/orders/addTransaction',{tran_id:tran_id, order_id: orderId, adj_price:adj_price}, function (data) {
               if(data.status){
                   window.location.reload();
               }
           });
        });
        $('a.btn.void').click(function(e){
            if(confirm('Are you sure you want to continue?')){
                var authId = $(this).data('id');
                var url = "<?=ADMIN_URL?>orders/void";
                $.post(url,{authId:authId},function(data){
                   if(data.status == true){
                       window.location.reload();
                   }
                });
            }
        });

        $('a.btn.refund').click(function(e){

            var amount = prompt("How much would you like to refund?",$(this).data('amount'));
            if(!isNaN(amount)){
                if(amount != null){
                    if(confirm('Are you sure you want to authorize a refund of $'+amount+'?')) {
                        var authId = $(this).data('id');
                        var url = "<?=ADMIN_URL?>orders/refund";
                        var orderId = <?=$model->order->id?:0?>;
                        $.post(url,{authId:authId,orderId:orderId,amount:amount},function(data){
                            if(data.status == true){
                                window.location.reload();
                            }
                        });
                    }
                }
            } else {
                alert('Invalid input for refund');
            }
        });
        $('.upd_trans_id').keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    var tran_id = this.value;
                    var order_id = $(this).data('order_id');
                    var original = $(this).data('original');
                    var torder = $(this);
                    var tstatus = $('.o_status[data-order_id='+order_id+']');
                    var confirmed = false;
                    var removed = false;
                    if(tran_id === '0'){
                        removed = confirm("Do you want to remove the current transaction from order #"+order_id+" and change the order's status to 'Canceled'?");
                    } else {
                        confirmed = confirm("Do you want to assign transaction '"+tran_id+"' to order #"+order_id+" and change the order's status to 'Updated'?");
                    }
                    if(confirmed){
                        $.post('/admin/orders/addTransaction',{tran_id:tran_id, order_id: order_id, adj_price:0}, function (data) {
                            if(data.status){
                                torder.data('original',tran_id);
                                tstatus.text('Updated');
                            }
                        });
                    } else if(removed) {
                        $.post('/admin/orders/clearTransaction',{order_id: order_id}, function (data) {
                            if(data.status){
                                torder.data('original','');
                                tstatus.text('Canceled');
                            }
                        });
                    } else {
                        this.value = original;
                    }
                    return false;
                }
        });
        $('a.btn.removeTrans').click(function(e){
            var tran_id = $(this).data('id');
            var url = "/admin/orders/deleteTransaction/";
            var adj_price = confirm('Update the current total with the imported transaction?\n (Cancel will remove the transaction but not change the total)')?1:0;
            $.post(url,{tran_id:tran_id, adj_price: adj_price}, function (data) {
                if(data.status){
                    window.location.reload();
                }
            });

        });

        $('.fulfill-product').on('click',function(){
            $(this).parent().find('.fulfill-container').show();
            $(this).parent().find('.fulfill-container select').prop('disabled',false);
            $(this).hide();
        });
        $('.close-fulfilment').on('click',function(){
            $(this).parent().parent().find('.fulfill-container').hide();
            $(this).parent().parent().find('.fulfill-container select').prop('disabled',true);
            $(this).parent().parent().find('.fulfill-product').show();
        });
        $('#new_op,#new_quantity').change(function(){
            var prodVal = $('#new_op :selected').data('price');
            var quantity = $('#new_quantity').val();
            var newPrice = prodVal * quantity;
            if(newPrice !== undefined && newPrice > 0 ){
                $('#new_op_cost').text(newPrice);
                $('#add_op').removeClass('disabled');
            } else {
                $('#add_op').addClass('disabled');
            }
        });
        $('#add_op').click(function(){
            var prodVal = $('#new_op :selected').data('price');
            var prod_id = $('#new_op').val();
            var quantity = $('#new_quantity').val();
            var newPrice = prodVal;
            var orderID = <?=$model->order->id?>;
            var _data = {
              prod_id: prod_id,
              quantity: quantity,
              price: newPrice,
              order_id: orderID
            };
            $.post('/admin/orders/add_product',_data,function(data){
              if(data.status){
                  window.location.reload();
              }
            });
        });
        $('.update_op').click(function(){
           var opID = $(this).data('opid');
           var quantity = $('#op_'+opID).val();
           var newPrice = $('#op_'+opID).data('price');
           var _data = {
               op_id: opID,
               quantity: quantity,
               price: newPrice
           }
           $.post('/admin/orders/update_orderProd',_data,function(data){
               if(data.status){
                   window.location.reload();
               }
           });
        });
//        $('.item-fulfilment').autocomplete({
//            minLength: 2,
//            delay: 0,
//            source: function(request,response){
//                var self = $(this.element);
//                $.post('/admin/orders/product_list',{product_id:self.data('product_id'),request:request.term},function(data){
//                    response(data);
//                })
//            }
//        });

        $('select[name="payment_method"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.cc-info').show();
            } else {
                $('.cc-info').hide();
            }
        });
        $('.cc_number button').click(function () {
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                $.ajax({
                    'type': 'POST',
                    'url': '<?=ADMIN_URL?>orders/getCcNumber',
                    'data': 'password=' + pwd + '&order_id=<?=$model->order->id?>',
                    'success': function (data) {
                        data = JSON.parse(data);
                        if (data == "false") {
                            alert('Incorrect code.');
                        } else {
                            $('.cc_number input').val(data);
                        }
                    }
                })
            }
        });

        $('select[name="shipping_cost"]').on('change', function () {
            if ($(this).val() == 0) {
                $('#shipping').html('$0.00');
                var total = '$'+'<?=number_format($model->order->total-$model->order->shipping_cost,2)?>';
                $('#total').text(total)
            } else {
                $('#shipping').html('$80.00');
                var total = '$'+'<?=number_format($model->order->total-$model->order->shipping_cost + 80,2)?>';
                $('#total').text(total)
            }
        });

        $('button#blacklist').click(function (e) {
            e.preventDefault();
            window.location.href ='/admin/orders/blacklist/<?=$model->order->id?>';
        });

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("orders/");
        });

        $('#save,#resendConfirmation').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("orders/update/" + id);
        });

        $('#resendConfirmation').click(function() {
            $("[name=send_confirm]").prop('checked',true);
        });

        $('.submit-wip').on('click',function(){
            var select = $(this).parent().siblings().find('.work-in-progress');
            if(select.val() != 0){
                var orderId = <?=$model->order->id?>;
                var orderProdId = select.data('id');
                var status = select.val();
                var data = {order_id:orderId,order_product_id:orderProdId,status:status};
                $.post('/admin/orders/send_progress',data,function(dat){
                    var json = $.parseJSON(dat);
                    console.log(json);
                    if(json.status == 'success'){
                        alert(json.message);
                    }
                });
            } else {
                alert('Select a level of progression');
            }
        });

    });
    // $("#phone-us").inputmask("+1(999)999-9999");
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>


<script>

    $(document).ready(function () {

        $('input[name=tracking_number]').on('input',function(){
            if($(this).val() == '<?=$model->order->tracking_number?>' || $(this).val() == ''){
                $('input[name=tracking_email]').prop('checked',false);
            } else {
                $('input[name=tracking_email]').prop('checked',true);
            }
        });

        $('a.app_ship_cost').click(function(){
           $('input[name=shipping_cost]').val($('select.app_ship_cost').val());
        });

        $('body').on('change', '.change', function () {
            var status = $(this).val();
            $.getJSON('/status_management/status', {status: status, orderId: '<?=$model->order->id?>'}, function(r){
                $("#mail").html(r.text);
                $("#subject").html(r.subject);
                $("#mailtitle").html(r.mailtitle);
            });
//            if (status == "Processed") {
//                $("#mail").html('<p>Hello  <?//=$model->order->ship_first_name?>//,<p>Thank you for your recent order.</p><p>This is to confirm that your orders is in stock and your credit card has now been processed.</p><p>You will receive an email with the tracking number as soon as it has shipped.</p><p>If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p><p>Thank you again for your business,</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>&ldquo;If it tells time, we sell it&rdquo;</p><p>(877) 752-6919</p>');
//            } else if (status == "Shipped") {
//                $("#mail").html('<p>Hello &nbsp;<?//=$model->order->ship_first_name?>//,</p><p>Your ModernVice order has shipped and the tracking number is _________.</p><p>If you have any questions please call or email us.</p><p>Thank you again for your business,</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
//            } else if (status == "Delivered") {
//                $("#mail").html('<p>Hello &nbsp;<?//=$model->order->ship_first_name?>//,</p><p>We have been informed by the manufacturer that the item you ordered from ModernVice has been discontinued.</p><p>And we checked with our other suppliers and they too do not have any in stock.</p><p>Would you like us to provide you with alternatives to that item&gt;&gt;SHOW ME OTHERS&lt;&lt; &nbsp;or would you prefer to just have your order canceled &gt;&gt;Cancel&lt;&lt;.?</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
//            } else if (status == "Discontinued") {
//                $("#mail").html('<p>Hello &nbsp;<?//=$model->order->ship_first_name?>//,</p><p>We have been informed by the manufacturer that the item you ordered from ModernVice has been discontinued.</p><p>And we checked with our other suppliers and they too do not have any in stock.</p><p>Would you like us to provide you with alternatives to that item&gt;&gt;SHOW ME OTHERS&lt;&lt; &nbsp;or would you prefer to just have your order canceled &gt;&gt;Cancel&lt;&lt;.?</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
//            } else if (status == "Billing_Shipping") {
//                $("#mail").html('<p>Hello &nbsp;(&lsquo;name&rsquo;),</p><p>In our efforts to protect all our customers we do credit card billing and shipping address verification on all orders.</p><p>When we contacted your credit card issuing bank to confirm the shipping address that you provided they were unable to do so.</p><p>Please just call the toll free number on the back of your credit card and ask them to add this as an &quot;Alternate Shipping Address&quot;.&nbsp;</p><p>Then just let us know that this has been done &gt;&gt;SHIPPING ADDRESS ADDED&lt;&lt; and we will be able to ship the watch to you at your requested address.</p><p>Thank you for your business,</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
//            } else if (status == "Canceled") {
//                $("#mail").html('Dear <b><?//=$model->order->ship_first_name?>//</b>, We have canceled your order as requested.<br> Thank you for giving us the opportunity to try to help you and we hope that you will come back to ModernVice.com in the future for any watch needs- Remember, "If it tells time we sell it".<br><br> Please note that your credit card was never charged for this purchase - only an authorization was received to confirm that it was not a fraudulent charge.<br><br> The Sales Team at ModernVice.com <br>(877) 752-6919<br> www.modernvice.com');
//            } else {
//            }

        });

//        $.ajax({
//            url: 'https://api.signifyd.com/v2/cases/<?//=$model->order->case_id?>//',
//            beforeSend: function (xhr) {
//                xhr.setRequestHeader ("Authorization", "Basic " + "S2l4T2M1Q3Q1UEJQaUk0NG4zMjExVVo4QTo=");
//            },
//            success: function(data) {
//                //$('.order_fraud_status').text(data.status);
//                $('.order_fraud_status').text(data.reviewDisposition);
//            }});


    });
</script>

<script type="text/javascript">
    if ( $('#same').text() == 'SAME' ) {
        $('.await_stacker').addClass('form_stacker');
        $('.await_stack').addClass('half_form');
        $('.notes_box').addClass('moveup');
    };
</script>