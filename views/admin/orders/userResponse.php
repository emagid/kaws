<? $black_count = 0;
   $grey_count = 0;
   $brown_count = 0;?>

<section class='orderUpdate_page'>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?=$model->response->id?>" />
        <input type="hidden" name="paid_amt" value="<?=$model->response->good_totals?>" />
        <input type="hidden" name="refund_amt" value="<?=$model->response->bad_totals?>" />
        <div role="tabpanel">
            <div class="tab-content">
                <center class='center save_btns' style="    margin-bottom: -110px !important;;">
                    <button type="submit" id="save" class="btn btn-save">Generate</button>
                    <!--                <button type="submit" id="resendConfirmation" class="btn btn-save">Save + Resend Email</button>-->
                    <!--                <button class="btn btn-save" id="print">Print</button>-->
                    <!--                <a href="/admin/orders/print_packing_slip/--><?//= $model->order->id ?><!--" class="btn btn-save">PRINT PACKING SLIP</a>-->
                </center>
            </div>
            <ul class="nav nav-tabs" role="tablist" style='    margin-top: 80px;'>
                <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="details-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <div class="form-group">
                                    <h4>Approved Orders: <?=count($model->response->good_orders)?>. Total Paid: $<?=number_format($model->response->good_totals,2)?> </h4>
                                    <div class="rox">
                                        <div class="box box-table" >
                                            <table>
                                                <thead>
                                                <tr>
                                                    <th width="10%">Order No.</th>
                                                    <th width="10%">Status</th>
                                                    <th width="6%">Amount Paid. </th>
                                                    <th width="10%">Cust. Name</th>
                                                    <th width="10%">Email</th>
                                                    <th width="10%">Address</th>
                                                    <th width="10%">Phone</th>
                                                    <th width="5%">Brown Count. </th>
                                                    <th width="5%">Grey Count. </th>
                                                    <th width="5%">Black Count. </th>
                                                    <th>View Transactions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <? foreach($model->response->good_orders as $order_id => $order){
                                                    $brown_count += $order['brown_count'];
                                                    $grey_count += $order['grey_count'];
                                                    $black_count += $order['black_count'];
                                                    $om = \Model\Order::getItem($order_id);
                                                    $addr = $om->getShippingAddr();
                                                    ?>
                                                    <tr>
                                                        <td ><a href="<?=ADMIN_URL.'orders/update/'.$order['id']?>"><?=$order['id']?></a></td>
                                                        <td class="o_status" data-order_id="<?=$order['id']?>"><?=$order['status']?:$order['fulfillment_status']?></td>
                                                        <td><span style="<?=$order['charge_added']?'font-weight:bold':''?>">$<?=number_format($order['total'],2)?></span></td>
                                                        <td><?= $order['name']?></td>
                                                        <td><?= $order['email']?></td>
                                                        <td><?= $addr ?></td>
                                                        <td><?= $order['phone']?></td>
                                                        <td><?= $order['brown_count']?></td>
                                                        <td><?= $order['grey_count']?></td>
                                                        <td><?= $order['black_count']?></td>
                                                        <td><a class="view_transactions btn btn-actions" data-order_id="<?=$order['id']?>">View</a></td>
                                                    </tr>
                                                    <?  if($i != count($doResults)-1) { ?>
                                                        <tr>
                                                            <td colspan="11">
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                    <? } ?>
                                                <? } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="form-group">
                                    <h4>Discarded Orders: <?=count($model->response->bad_orders)?>. Total Paid: $<?=number_format($model->response->bad_totals,2)?> </h4>
                                    <div class="rox">
                                        <div class="box box-table">
                                            <table>
                                                <thead>
                                                <tr>
                                                    <th width="10%">Order No.</th>
                                                    <th width="10%">Status</th>
                                                    <th width="6%">Amount Paid. </th>
                                                    <th width="10%">Cust. Name</th>
                                                    <th width="5%">Email</th>
                                                    <th width="20%">Address</th>
                                                    <th width="10%">Phone</th>
                                                    <th width="5%">Brown Count. </th>
                                                    <th width="5%">Grey Count. </th>
                                                    <th width="5%">Black Count. </th>
                                                    <th>View Transactions</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <? foreach($model->response->bad_orders as $order_id => $order){
                                                    $om = \Model\Order::getItem($order_id);
                                                    $addr = $om->getShippingAddr(); ?>
                                                    <tr>
                                                        <td><a href="<?=ADMIN_URL.'orders/update/'.$order['id']?>"><?=$order['id']?></a></td>
                                                        <td class="o_status" data-order_id="<?=$order['id']?>"><?=$order['status']?:$order['fulfillment_status']?></td>
                                                        <td><span style="<?=$order['charge_added']?'font-weight:bold':''?>">$<?=number_format($order['total'],2)?></span></td>
                                                        <td><?= $order['name']?></td>
                                                        <td><?= $order['email']?></td>
                                                        <td><?= $addr?></td>
                                                        <td><?= $order['phone']?></td>
                                                        <td><?= $order['brown_count']?></td>
                                                        <td><?= $order['grey_count']?></td>
                                                        <td><?= $order['black_count']?></td>
                                                        <td><a class="view_transactions btn btn-actions" data-order_id="<?=$order['id']?>">View</a></td>
                                                    </tr>
                                                    <?  if($i != count($doResults)-1) { ?>
                                                        <tr>
                                                            <td colspan="11">
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                    <? } ?>
                                                <? } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <? if(count($model->response->good_orders) > 0) {?>
                            <div class="box">
                                <div class="form-group">
                                    <?
                                    $gorder = $model->generated_order == []?current($model->response->good_orders):$model->generated_order;
                                    $om = \Model\Order::getItem($gorder['id']);
                                        $addr = $om->getShippingAddr();
                                        $gorder['brown_count'] = $gorder['brown_count']?:$brown_count;
                                        $gorder['grey_count']  = $gorder['grey_count'] ?:$grey_count;
                                        $gorder['black_count'] = $gorder['black_count']?:$black_count;
                                        if($model->response->generated_order ){
                                            $om = \Model\Order::getItem($model->response->generated_order);
                                            $addr = $om->getShippingAddr();
                                        } else {
                                            $om->id = $gorder['id'] = 0;
                                        }
                                        $bd = $om->calculate_total([105=>$brown_count,106=>$grey_count,107=>$black_count]);
                                    ?>
                                    <?
                                    $gt = $model->response->good_totals;
                                    $bt = $model->response->bad_totals;
                                    $nt = array_sum($bd['charges']);
                                    ?>
                                    <h4>Generated Order - <?=$model->response->generated_order?> Total Owed: $<?=number_format($gt - $nt,2)?> </h4>

                                    <div class="rox">
                                        <div class="box box-table">
                                            <table>
                                                <thead>
                                                    <tr>
                                                    <th width="10%">Order No.</th>
                                                    <th width="10%">Status</th>
                                                    <th width="6%">Amount Paid. </th>
                                                    <th width="10%">Cust. Name</th>
                                                    <th width="5%">Email</th>
                                                    <th width="20%">Address</th>
                                                    <th width="10%">Phone</th>
                                                    <th width="5%">Brown Count. </th>
                                                    <th width="5%">Grey Count. </th>
                                                    <th width="5%">Black Count. </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><a href="<?=ADMIN_URL.'orders/update/'.$gorder['id']?>"><?=$gorder['id']?:'N/A'?></a></td>
                                                        <td><?=$gorder['status']?:$gorder['fulfillment_status']?></td>
                                                        <td>$<?=number_format(array_sum($bd['charges']),2)?></td>
                                                        <td><?= $gorder['name']?></td>
                                                        <td><?= $gorder['email']?></td>
                                                        <td><?= $addr?></td>
                                                        <td><?= $gorder['phone']?></td>
                                                        <td><?= $gorder['brown_count']?></td>
                                                        <td><?= $gorder['grey_count']?></td>
                                                        <td><?= $gorder['black_count']?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="10"> <hr /> </td>
                                                    </tr>
                                                    <tr>
                                                        <? foreach ($bd['updates'] as $name => $charge) {?>
                                                            <th colspan="2">
                                                                <?=ucwords(str_replace('_',' ',$name))?>:
                                                            </th>
                                                        <? } ?>
                                                    </tr>
                                                    <tr>
                                                        <? foreach ($bd['updates'] as $name => $charge) {?>
                                                            <td colspan="2">
                                                                $<?=number_format($charge,2)?>
                                                            </td>
                                                        <? } ?>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="act_for_click" name="redirectTo" value="orders/userResponse/<?= $model->response->id ?>">
        <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
        <? //=$model->order->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
    </form>
</section>

<?php footer(); ?>