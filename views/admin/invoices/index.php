<?php if (count($model->invoices) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Date</th>
                <th>Status</th>
                <th>Recipient</th>
                <th>Recipient Email</th>
                <th>Products</th>
                <th>Subtotal</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
                <th class="text-center">In-store</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->invoices as $obj) { ?>
                <tr class="originalProducts" <?=$obj->in_store ? 'style="background:#ffff9e;"': ''?>>
                    <td><a href="<?php echo ADMIN_URL; ?>invoices/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>invoices/update/<?php echo $obj->id; ?>"><?php echo date('M d, Y h:ia',strtotime($obj->insert_time)); ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>invoices/update/<?php echo $obj->id; ?>" style="color: <?switch($obj->status){case 'Paid':echo '#41BB2D';break; case 'Canceled':case 'Refunded': echo '#D94F74';break;}?>"><?= $obj->status ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>invoices/update/<?php echo $obj->id; ?>"><?=$obj->fullName()?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>invoices/update/<?php echo $obj->id; ?>"><?=$obj->to_email?></a></td>
                    <?$itemList = [];
                    $total = 0;
                    $details = json_decode($obj->details,true);
                    foreach($details as $detail){
                        if(isset($detail['misc_name']) && $detail['misc_name']) {
                            $itemList[] = $detail['misc_name'];
                            $total += $detail['custom_price'];
                        } elseif(isset($detail['product_id']) && $detail['product_id']) {
                            $product = \Model\Product::getItem($detail['product_id']);
                            $itemList[] = $product->name;
                            if(isset($detail['custom_price']) && $detail['custom_price'] != ''){
                                $total += $detail['custom_price'];
                            } else {
                                $total += $product->getPrice($detail['color']);
                            }
                        }
                    }?>
                    <td><?=implode('<br/>',$itemList)?></td>
                    <td><?='$'.number_format($total,2)?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>invoices/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black"
                           href="<?php echo ADMIN_URL; ?>invoices/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <? if ($obj->status == 'New') { ?>
                            <a class="btn-actions"
                               href="<?php echo ADMIN_URL; ?>orders/create_order?inv=<?= $obj->ref_num ?>">
                                <i class="icon-dollar"></i>
                            </a>
                        <? } else { ?>
                            <i class="icon-dollar"></i>
                        <? } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } else {
    echo "<center>No matches!<br><a href='' onclick='history.go(-1)'>Go back!</a></center>";
} ?>

<?php footer(); ?>

<script>
    var site_url = '/admin/invoices<?if (isset($_GET['status_show'])){echo "status_show=";echo $_GET['status_show'] ;echo"&";}?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>invoices/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html();
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>invoices/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                        $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html(list[key].products);

                        $('<td />').appendTo(tr).html("$" + list[key].total);
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>invoices/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>

<script type="text/javascript">
    $(function () {
        $("#search_by_mpn").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>invoices/search_by_mpn";
            var keywords = $.trim($(this).val());
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {

                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html();
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>invoices/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                        $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html(list[key].products);
                        $('<td />').appendTo(tr).html("$" + list[key].total);
                        /*   var editTd = $('<td />').addClass('text-center').appendTo(tr);
                         var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '
                        <?= ADMIN_URL;?>invoices/update/' + list[key].id);
                         var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');*/
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>

