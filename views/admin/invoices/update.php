<style type="text/css">


    @media print {
        /* Стиль для печати */
        h1, h2, p {
            color: #000; /* Черный цвет текста */
        }

        .nav {
            display: none;
        }

        .btn {
            display: none;
        }

        .qqq {
            color: black;
        }

        #general-tab, #billing-info-tab, #shipping-info-tab, #products-tab {
            display: block;
            visibility: visible;
        }

        .form-group {
            margin-bottom: 3px;
        }

        a {
            border: 0;
            text-decoration: none;
        }

        input[type="text"] {
            border-color: white;
        }

        .form-control {
            border: 2px solid blue;
        }

        a img {
            border: 0
        }

        a:after {
            content: " (" attr(href) ") ";
            font-size: 90%;
        }

        a[href^="/"]:after {
            content: " ";
        }
    }
</style>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?=$model->invoice->id?>"/>
    <input type="hidden" name="ref_num" value="<?=$model->invoice->ref_num?>"/>
    <input type="hidden" name="order_id" value="<?=$model->invoice->order_id?>"/>
    <?if($model->invoice->insert_time){?>
        <input type="hidden" name="insert_time" value="<?=$model->invoice->insert_time?>"/>
    <?}?>
    <input type="hidden" name="old_status" value="<?=$model->invoice->status?>"/>
    <input type=hidden name="token" value="<?php echo $model->invoice->ref_num ? : generateToken(); ?>"/>

    <div role="tabpanel">
        <div class="tab-content">
            <div role="tabpanel">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>
                            <?if ($model->invoice->ref_num) { ?>
                                <div class="form-group">
                                    <label>Url</label>
                                    <p><a href="https://djinyc.com/checkout/invoice?ref=<?=$model->invoice->ref_num?>">https://djinyc.com/checkout/invoice?ref=<?=$model->invoice->ref_num?></a></p>
                                </div>
                            <? }?>
                            <div class="form-group">
                                <label>Status</label>
                                <?php echo $model->form->dropDownListFor('status', \Model\Invoice::$status, '',['class'=>'form-control']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-24">
                        <div class="box">
                            <div class="col-sm-12">
                                <h4>Send To</h4>

                                <div class="form-group">
                                    <label>First Name</label>
                                    <?php echo $model->form->textBoxFor('to_first_name', ['required' => 'required']) ?>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <?php echo $model->form->textBoxFor('to_last_name', ['required' => 'required']) ?>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <?php echo $model->form->textBoxFor('to_email', ['required' => 'required']) ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <h4>Sent From</h4>

                                <div class="form-group">
                                    <label>First Name</label>
                                    <?php echo $model->form->textBoxFor('from_first_name', ['required' => 'required']) ?>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <?php echo $model->form->textBoxFor('from_last_name', ['required' => 'required']) ?>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <?php echo $model->form->textBoxFor('', ['readonly' => 'readonly', 'value' => "info@djinyc.com"]) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Subject</label>
                                <?php echo $model->form->textBoxFor('subject') ?>
                            </div>
                            <div class="form-group">
                                <label>Body</label>
                                <?php echo $model->form->textAreaFor('body', ['rows' => '10']) ?>
                            </div>
                            <table class="table" style="margin-top: 10px;">
                                <tr>
                                    <td>Subtotal</td>
                                    <td id="sub">$<?php echo number_format(0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Tax</td>
                                    <td id="tax">$<?php echo number_format(0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Shipping</td>
                                    <td id="shipping">$<?php echo number_format(0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Discount</td>
                                    <td id="discount">$<?= number_format(0, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td id="total">$<?= number_format(0, 2) ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <div class="box product_parent">
                                <h4>Products*</h4>
                                <span style="float: right">Tax Free <?php echo $model->form->checkBoxFor("tax_free",1)?></span>

                                <? if($model->invoice->details) {
                                    foreach (json_decode($model->invoice->details, true) as $items) {
                                        $product_id = isset($items['product_id']) && $items['product_id'] ? $items['product_id'] : 0;
                                        $colors = $product_id ? \Model\Product::getItem($product_id)->getColors() : [];
                                        $sizes = $product_id ? \Model\Product::getItem($product_id)->getSizes() : [] ?>
                                        <div class="product_container">
                                            <label>Product</label>
                                            <select name="product_id[]" class="form-control" required>
                                                <option value="-1">Select a product</option>
                                                <option value="0" <?= $product_id == 0 ? 'selected' : '' ?>>*
                                                    Miscellaneous Charge
                                                </option>
                                                <? foreach (\Model\Product::getList(['orderBy' => 'name']) as $item) { ?>
                                                    <option
                                                        value="<?= $item->id ?>" <?= $product_id == $item->id ? 'selected' : '' ?>><?= $item->name ?></option>
                                                <? } ?>
                                            </select>

                                            <div
                                                class="product_select" <?= $product_id == 0 ? 'style="display:none"' : '' ?>>
                                                <label>Color Variant</label>
                                                <select name="color[]"
                                                        class="form-control" <?= $product_id == 0 ? 'readonly' : '' ?>>
                                                    <option value="0">None</option>
                                                    <? foreach ($colors as $color) { ?>
                                                        <option
                                                            value="<?= $color->id ?>" <?= isset($items['color']) && $items['color'] && $items['color'] == $color->id ? 'selected' : '' ?>><?= $color->name ?></option>
                                                    <?
                                                    } ?>
                                                </select>
                                                <label>Size
                                                    <small>(US)</small>
                                                </label>
                                                <select name="size[]"
                                                        class="form-control" <?= $product_id == 0 ? 'readonly' : '' ?>>
                                                    <option value="0">None</option>
                                                    <? foreach ($sizes as $size) { ?>
                                                        <option
                                                            value="<?= $size->id ?>" <?= isset($items['size']) && $items['size'] && $items['size'] == $size->id ? 'selected' : '' ?>><?= $size->us_size ?></option>
                                                    <?
                                                    } ?>
                                                </select>
                                            </div>
                                            <div
                                                class="misc_select" <?= $product_id == 0 ? '' : 'style="display: none"' ?>>
                                                <label>Name</label>
                                                <input type="text" name="misc_name[]" class="form-control"
                                                       value="<?= isset($items['misc_name']) && $items['misc_name'] ? $items['misc_name'] : '' ?>" <?= $product_id == 0 ? '' : 'readonly' ?>
                                                       required/>
                                            </div>
                                            <label>Custom Price
                                                <small>(overrides defined price)</small>
                                            </label>
                                            <input name="custom_price[]" type="number" class="form-control"
                                                   value="<?= isset($items['custom_price']) && $items['custom_price'] ? $items['custom_price'] : '' ?>">
                                            <hr/>
                                        </div>
                                    <? }
                                }?>
                            </div>
                            <div class="box">
                                <a class="add-another">Add another product</a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box">
                                <h4>Notes</h4>

                                <div class="form-group">
                                    <label>Note</label>
                                    <?php echo $model->form->textAreaFor("notes", ['rows' => '10']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
        <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
        <? //=$model->order->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
        <button type="submit" class="btn btn-save">Save</button>
</form>
<?php footer(); ?>

<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>
<script src="<?= FRONT_JS . 'cc_type.js' ?>"></script>
<script>


    $(function () {

        var gsubtotal = [];
        var gtax = 0;
        var gshipping = 0;
        var gdiscount = 0;
        var gtotal = 0;

        function updateTotal() {
            var parseSub = gsubtotal.map(function (value) {
                return parseFloat(value);
            });
            var subtotal = parseSub.reduce(function (a, b) {
                return a + b;
            }, 0);
            gtax = parseFloat(gtax);
            gshipping = parseFloat(gshipping);
            gdiscount = parseFloat(gdiscount);
            gtotal = parseFloat(gtotal);

            if (!$('input[name=tax_free]').is(':checked')) {
                gtax = subtotal * .08875;
            } else {
                gtax = 0;
            }
            gtotal = subtotal + gtax + gshipping - gdiscount;
            var sub = $('#sub');
            var tax = $('#tax');
            var shipping = $('#shipping');
            var discount = $('#discount');
            var total = $('#total');

            sub.html('$' + subtotal.toFixed(2));
            tax.html('$' + gtax.toFixed(2));
            shipping.html('$' + gshipping.toFixed(2));
            discount.html('$' + gdiscount.toFixed(2));
            total.html('$' + gtotal.toFixed(2));
        }

        $($('input[name=tax_free]')).on('change', function () {
            updateTotal();
        });

        $(document).on('change', "select[name='product_id[]']", function () {
            var self = $(this);
            var index = self.index("select[name='product_id[]']");
            var product_id = self.find(':selected').val();
            var color = self.parent().find($('select[name="color[]"]'));
            var size = self.parent().find($('select[name="size[]"]'));
            var name = self.parent().find($('input[name="misc_name[]"]'));
            var custom_price = self.parent().find($('input[name="custom_price[]"]'));
            var product_select = self.parent().find($('.product_select'));
            var misc_select = self.parent().find($('.misc_select'));
            if (product_id > 0) {
                product_select.show();
                misc_select.hide();
                color.prop('readonly', false);
                size.prop('readonly', false);
                name.prop('readonly', true);
                $.getJSON('/admin/orders/getVariations', {product_id: product_id}, function (data) {
                    if (data.status == 'success') {
                        gsubtotal[index] = data.subtotal;
                        color.html(data.colorHtml);
                        size.html(data.sizeHtml);
                        updateTotal();
                    }
                });
            } else if (product_id == 0) {
                product_select.hide();
                misc_select.show();
                color.prop('readonly', true);
                size.prop('readonly', true);
                name.prop('readonly', false);
                gsubtotal[index] = 0;
                updateTotal();
            } else {
                product_select.show();
                misc_select.hide();
                color.prop('readonly', false);
                size.prop('readonly', false);
                name.prop('readonly', true);
                gsubtotal[index] = 0;
                updateTotal();
                color.html("");
                size.html("");
            }
        });
        $(document).on('change', "select[name='color[]']", function () {
            var self = $(this);
            var index = self.index("select[name='color[]']");
            var product_id = self.parent().parent().find('select[name="product_id[]"]').find(':selected').val();
            var color_id = self.find(':selected').val();
            $.getJSON('/admin/orders/setColorVariant', {product_id: product_id, color_id: color_id}, function (data) {
                if (data.status == 'success') {
                    gsubtotal[index] = data.price;
                    updateTotal();
                }
            });
        });

        $(document).on('keyup', 'input[name="custom_price[]"]', function () {
            var value;
            var index = $(this).index('input[name="custom_price[]"]');
            if ($(this).val() > 0) {
                value = $(this).val();
            } else {
                value = 0;
            }
            gsubtotal[index] = value;
            updateTotal();
        });

        $('.add-another').on('click', function () {
//            var container = $('.product_container').clone();
            var container = '<div class="product_container"> ' +
                '<label>Product</label> ' +
                '<select name="product_id[]" class="form-control" required> ' +
                '<option value="-1">Select a product</option> ' +
                '<option value="0">* Miscellaneous Charge</option>';
            <? foreach (\Model\Product::getList(['orderBy' => 'name']) as $item) { ?>
            container += '<option value="<?= $item->id ?>"><?= addslashes(str_replace(["\r","\n"],'',$item->name)) ?></option>';
            <? } ?>
            container += '</select> ' +
                '<div class="product_select"> ' +
                '<label>Color Variant</label> ' +
                '<select name="color[]" class="form-control"><option value="0">None</option></select> ' +
                '<label>Size <small>(US)</small> ' +
                '</label> ' +
                '<select name="size[]" class="form-control"><option value="0">None</option></select> ' +
                '</div> ' +
                '<div class="misc_select" style="display: none"> ' +
                '<label>Name</label> ' +
                '<input type="text" name="misc_name[]" class="form-control" required/> ' +
                '</div> ' +
                '<label>Custom Price <small>(overrides defined price)</small> ' +
                '</label> ' +
                '<input name="custom_price[]" type="number" class="form-control"><button class="deleteBtn" type="button">Delete</button> ' +
                '<hr/> ' +
                '</div>';
            $('.product_parent').append(container);
        });

        $('select[name="payment_method"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.cc-info').show();
            } else {
                $('.cc-info').hide();
            }
        });
        $('#save_close').mouseover(function () {
            $("#act_for_click").val("orders/");

        });
    });
    $("#phone-us").inputmask("+1(999)999-9999");
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        });

        $(document).on('click','.deleteBtn',function(){
            $(this).parent().remove();
        })
    })
</script>