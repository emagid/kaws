<?php if (count($model->products_list) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="2%">Product</th>
                <th width="2%">Sell Count</th>
            </tr>
            </thead>
            <tbody>
            <?php $inc = 1;foreach ($model->products_list as $obj) {?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj['id']; ?>"><?php echo $obj['name']; ?></a></td>
                    <td><?php echo $obj['c']; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<!--<script type="text/javascript">
    var site_url = '<?/*= ADMIN_URL.'products_list';*/?>';
    var total_pages = <?/*= $model->pagination->total_pages;*/?>;
    var page = <?/*= $model->pagination->current_page_index;*/?>;
</script>
-->
<script>
    $(function () {
        $('#limit').on('change',function(){
            var t = {'l':$(this).val()};
            window.location.replace('<?=ADMIN_URL.'reports/abandoned_top_seller'?>'+'?'+ $.param(t));
        })
    });
</script>
