<div class="row">
    <div class="col-md-24 panel_large">
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-24 text-right">
                            <div class="huge">$<?= number_format($model->sales, 2) ?></div>
                            <div class="panel_text">
                                <p>Sales</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-blue">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-24 text-right">
                            <div class="huge">$<?= number_format($model->declined, 2); ?></div>
                            <div class="panel_text">
                                <p>Declined</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-24 text-right">
                            <div class="huge">$<?= number_format($model->unfulfilled, 2); ?></div>
                            <div class="panel_text">
                                <p>Unfulfilled</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-orange">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-24 text-right">
                            <div class="huge">$<?= number_format($model->unfulfilled + $model->sales + $model->declined, 2); ?></div>
                            <div class="panel_text">
                                <p>Total</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box">
            Date Range:
            <input name="daterange" type="text" value="<?= date('Y-m-d', strtotime($model->start)) . ' - ' . date('Y-m-d', strtotime($model->end)) ?>"/>
        </div>
    </div>
</div>
<?php if (count($model->orders) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="2%">Id</th>
                <th width="2%">Date</th>
                <th width="2%">Tracking Number</th>
                <th width="2%">Status</th>
                <th width="2%">Bill Name</th>
                <th width="2%">Products</th>
                <th width="2%">Total</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->orders as $obj) { ?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"><?php echo $obj->insert_time; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"><?php echo $obj->tracking_number; ?></a></td>
                    <td>
                        <?$color = 'black';
                        if(in_array($obj->status,['Paid','Shipped','Complete','Delivered'])){$color = 'green';}
                        else if(in_array($obj->status,['Declined','Canceled','Incomplete PayPal'])){$color = 'red';}?>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>" style="color: <?= $color ?>;">
                            <?= $obj->status ?><?if($obj->payment_status){echo "<br/>($obj->payment_status)";}?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);

                            $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);

                            if (count(array_diff($shp, $blng)) == 0) {?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                            <? } else {?>
                                <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                        disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                            <? } ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?if(($orderProducts = $obj->getOrderProducts())){
                                foreach($orderProducts as $orderProduct){
                                    if($orderProduct->product_id){
                                        echo \Model\Product::getItem($orderProduct->product_id)->name;
                                    } else {
                                        $json = json_decode($orderProduct->details,true);
                                        echo $json['misc_name'] .'(Custom)';
                                    }
                                    echo '<br/>';
                                }
                            }?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? if ($obj->payment_method == 2) { ?>
                                <button type="button" class="btn btn-success" style="position:relative;"
                                        disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                            <? } else { ?>$<?php echo number_format($obj->total, 2);
                            } ?>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<!--<script type="text/javascript">
    var site_url = '<?/*= ADMIN_URL.'orders';*/?>';
    var total_pages = <?/*= $model->pagination->total_pages;*/?>;
    var page = <?/*= $model->pagination->current_page_index;*/?>;
</script>
-->
<script>
    $(function () {
        $('input[name="daterange"]').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo $model->start?>",
            endDate: "<?php echo $model->end?>"
        },function(start,end){
            end.hour(23);end.minute(59);end.second(59);
            var t = {'t':start.unix()+','+end.unix()};
            window.location.replace('<?=ADMIN_URL.'reports/orders_summary'?>'+'?'+ $.param(t));
        });
    });
</script>
