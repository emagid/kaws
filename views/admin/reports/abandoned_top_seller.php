<div class="row">
    <div class="col-md-8">
        <div class="box">
            Limit:
            <select id="limit">
                <?$l = [20,50,100];
                foreach($l as $c){?>
                    <option value="<?=$c?>" <?=isset($_GET['l']) && is_numeric($_GET['l']) && $_GET['l'] == $c ? 'selected': ''?>><?=$c?></option>
                <?}?>
            </select>
        </div>
    </div>
</div>
<?php if (count($model->products_list) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="1%"></th>
                <th width="2%">Product</th>
                <th width="2%">Abandon Count</th>
                <th width="2%"></th>
            </tr>
            </thead>
            <tbody>
            <?php $inc = 1;foreach ($model->products_list as $obj) {
                $product = \Model\Product::getItem($obj['product_id'])?>
                <tr>
                    <td><?php echo $inc; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $product->id; ?>"><?php echo $product->name; ?></a></td>
                    <td><?php echo $obj['c']; ?></td>
                    <td><a href="<?php echo ADMIN_URL; ?>abandoned_cart/update/<?php echo $obj->id; ?>"><?php echo '' ?></a></td>
                </tr>
            <?php $inc ++;} ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<!--<script type="text/javascript">
    var site_url = '<?/*= ADMIN_URL.'products_list';*/?>';
    var total_pages = <?/*= $model->pagination->total_pages;*/?>;
    var page = <?/*= $model->pagination->current_page_index;*/?>;
</script>
-->
<script>
    $(function () {
        $('#limit').on('change',function(){
            var t = {'l':$(this).val()};
            window.location.replace('<?=ADMIN_URL.'reports/abandoned_top_seller'?>'+'?'+ $.param(t));
        })
    });
</script>
