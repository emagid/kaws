<?php if (count($model->reports) > 0) { ?>
    <div class="box">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <a class="btn-actions form-control" href="<?= ADMIN_URL . 'reports/orders_summary' ?>">
                        <div>Orders Summary</div>
                    </a>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="col-lg-24 col-md-24" style='margin-top: 20px;'>
                                <div class="panel panel-green">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-24 text-right">
                                                <h1>$<?= number_format($model->sales, 2) ?></h1>
                                                <div class="panel_text">
                                                    <p>Sales</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-24 col-md-24">
                                <div class="panel panel-blue">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-24 text-right">
                                                <h1>$<?= number_format($model->declined, 2); ?></h1>
                                                <div class="panel_text">
                                                    <p>Declined</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-24 col-md-24">
                                <div class="panel panel-red">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-24 text-right">
                                                <h1>$<?= number_format($model->unfulfilled, 2); ?></h1>
                                                <div class="panel_text">
                                                    <p>Unfulfilled</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-24 col-md-24">
                                <div class="panel panel-orange">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-24 text-right">
                                                <h1>$<?= number_format($model->unfulfilled + $model->sales, 2); ?></h1>
                                                <div class="panel_text">
                                                    <p>Total</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (count($model->orders) > 0) { ?>
                        <div class="box box-table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="2%">Id</th>
                                    <th width="2%">Date</th>
                                    <th width="2%">Tracking Number</th>
                                    <th width="2%">Status</th>
                                    <th width="2%">Bill Name</th>
                                    <th width="2%">Products</th>
                                    <th width="2%">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($model->orders as $obj) { ?>
                                    <tr>
                                        <td><a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a></td>
                                        <td><a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"><?php echo $obj->insert_time; ?></a></td>
                                        <td><a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"><?php echo $obj->tracking_number; ?></a></td>
                                        <td>
                                            <?$color = 'black';
                                            if(in_array($obj->status,['Paid','Shipped','Complete','Delivered'])){$color = 'green';}
                                            else if(in_array($obj->status,['Declined','Canceled','Incomplete PayPal'])){$color = 'red';}?>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>" style="color: <?= $color ?>;">
                                                <?= $obj->status ?><?if($obj->payment_status){echo "<br/>($obj->payment_status)";}?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                                    $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);

                                                $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                                    $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);

                                                if (count(array_diff($shp, $blng)) == 0) {?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                                                <? } else {?>
                                                    <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                                            disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                                                <? } ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <?if(($orderProducts = $obj->getOrderProducts())){
                                                    foreach($orderProducts as $orderProduct){
                                                        if($orderProduct->product_id){
                                                            echo \Model\Product::getItem($orderProduct->product_id)->name;
                                                        } else {
                                                            $json = json_decode($orderProduct->details,true);
                                                            echo $json['misc_name'] .'(Custom)';
                                                        }
                                                        echo '<br/>';
                                                    }
                                                }?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <? if ($obj->payment_method == 2) { ?>
                                                    <button type="button" class="btn btn-success" style="position:relative;"
                                                            disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                                                <? } else { ?>$<?php echo number_format($obj->total, 2);
                                                } ?>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="box-footer clearfix">
                                <div class='paginationContent'></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <a class="btn-actions form-control" href="<?= ADMIN_URL . 'reports/abandoned_top_seller' ?>">
                        <div>Abandoned Top Sellers</div>
                    </a>
                </div>
                <div class="form-group">
                    <?php if (count($model->abandoned_list) > 0) { ?>
                        <div class="box box-table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="75%">Product</th>
                                    <th width="25%">Abandon Count</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($model->abandoned_list as $obj) {
                                    $product = \Model\Product::getItem($obj['product_id'])?>
                                    <tr>
                                        <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $product->id; ?>"><?php echo $product->name; ?></a></td>
                                        <td><?php echo $obj['c']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <a class="btn-actions form-control" href="<?= ADMIN_URL . 'reports/product_top_seller' ?>">
                        <div>Product Top Sellers</div>
                    </a>
                </div>
                <div class="form-group">
                    <?php if (count($model->products_list) > 0) { ?>
                        <div class="box box-table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="75%">Product</th>
                                    <th width="25%">Sell Count</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $inc = 1;foreach ($model->products_list as $obj) {?>
                                    <tr>
                                        <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj['product_id']; ?>"><?php echo $obj['name']; ?></a></td>
                                        <td><?php echo $obj['c']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
