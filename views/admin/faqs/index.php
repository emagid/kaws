<?php if(count($model->faqs)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="15%">Category</th>
          <th width="50%">Question</th>
          <th width="10%">Display</th>
          <th width="15%" class="text-center">Edit</th>	
          <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->faqs as $item): ?>
        <tr>
         <td><a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $item->id; ?>"><?php echo \Model\Faq::$category[$item->category]; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $item->id; ?>"><?php echo $item->question; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $item->id; ?>"><?php echo $item->display_order; ?></a></td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $item->id; ?>">
           <i class="icon-pencil"></i> 
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions" href="<?php echo ADMIN_URL; ?>faqs/delete/<?php echo $item->id; ?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i> 
           </a>
         </td>
       </tr>
     <?php endforeach; ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'faqs/index';?>';
  var total_pages = <?= $model->pagination->total_pages;?>;
  var page = <?= $model->pagination->current_page_index;?>;

</script>

