    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation" ><a href="#inventory-tab" aria-controls="inventory" role="tab" data-toggle="tab">Inventory</a></li>
            <li role="presentation" ><a href="#transfers-tab" aria-controls="transfers" role="tab" data-toggle="tab">Transfers</a></li>
            <li role="presentation" ><a href="#receivings-tab" aria-controls="receivings" role="tab" data-toggle="tab">Intake</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="id" value="<?php echo $model->custom_inventory->id; ?>"/>
                    <input type="hidden" name="redirectTo" value="<?php echo '/admin/custom_inventory/update/'.$model->custom_inventory->id; ?>"/>

                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <h4>General</h4>
                                <div class="form-group">
                                    <label>Name</label>
                                    <? if(!is_numeric($model->custom_inventory->id) && in_array($model->custom_inventory->id,array_keys(\Model\Actual_Inventory::$places))){?>
                                        <input disabled readonly type="text" value="<?=Model\Actual_Inventory::$places[$model->custom_inventory->id]?>">
                                    <? } else { ?>
                                        <?php echo $model->form->editorFor("name"); ?>
                                    <? } ?>
                                </div>
                                <? foreach ($model->low_counts as $prod => $counts) {?>
                                    <div class="btn btn-danger">Running low on <?=$prod?>. Only <?= $counts[0] ?> available. Need to order <?=$counts[1]-$counts[0]?></div>
                                    <hr />
                                <? } ?>
                            </div>
                            <button type="submit" class="btn btn-save">Save</button>
                        </div>
                    </div>

                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="inventory-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="form-group">
                                <h4>Inventory</h4>
                                <div class="form-group">
                                    <input type="text" id="inventory-search" placeholder="Search by serial, name">
                                </div>
                                <table class="table" id="inv_table">
                                    <? foreach(Model\Inventory_Status::getList() as $status) {?>
                                        <tr style="background-color: #f3f3f4" class="sort" data-status="<?=$status->name ?>">
                                            <th>
                                                <?=$status->name ?>
                                            </th>
                                        </tr>
                                        <? if(!isset($model->custom_inventory->byStatus[$status->id])) continue; ?>
                                        <? foreach($model->custom_inventory->byProduct as $pid => $prod) {?>
                                            <? $prods = array_intersect($prod,$model->custom_inventory->byStatus[$status->id]);
                                            if(count($prods) > 0) { ?>
                                                <tr class="sort" data-product="<?=$pid?>">
                                                    <th>
                                                        <? if(is_numeric($pid)){ ?>
                                                            <?= Model\Product::getItem($pid)->name ?>
                                                        <? } else { ?>
                                                            <?= $pid ?>
                                                        <? } ?>
                                                    </th>
                                                </tr>

                                                <? foreach($prods as $rid) {?>
                                                    <? $ri = $model->custom_inventory->items[$rid]?>
                                                    <tr class='receive_item' data-sku="<?=$ri->sku?>" data-prodid="<?=$ri->product_id?>" data-ppno="<?= Model\Product::getItem($pid)->sku?>" id="<?=$ri->id?>" data-status="<?=$status->id?>" data-name="<?=$ri->name?>" data-product="<?=$ri->product_id?>" data-product_name="<?=strtolower($ri->name)?>">
                                                        <td>
                                                            <?=trim($ri->sku) != '' ? $ri->sku :'No Sku available, ITEM id: '.$ri->id?>
                                                        </td>
                                                    </tr>
                                                <? } ?>
                                            <? } ?>
                                        <? } ?>
                                    <? } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <form class="form" action="/admin/custom_inventory/update_item" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <h4>Details</h4>
                                    <div class="form-group">
                                        <label>Product Name</label>
                                        <input type="hidden" name="id" id="inv-id">
                                        <input type="hidden" name="prodId" id="inv-prodid">
                                        <input type="hidden" name="redirectTo" value="<?= $this->emagid->uri ?>">
                                        <input readonly name ="name" type="text" id="inv-name">
                                    </div>
                                    <div class="form-group">
                                        <label>Product Sku</label>
                                        <input disabled type="text" id="inv-ppno">
                                    </div>
                                    <div class="form-group">
                                        <label>Item Serial Number/Count</label>
                                        <input disabled type="text" id="inv-serial">
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Item Status</label>
                                            <select id="inv-status" name='status' class="form-control" disabled>
                                                <option value selected disabled>N/A</option>
                                                <? foreach(\Model\Inventory_Status::getList() as $status) {?>
                                                    <option value="<?=$status->id?>"><?=$status->name?></option>
                                                <? } ?>
                                            </select>
                                            <input id="inv-oStatus" type="hidden" name="oStatus">
                                        </div>
                                        <div class="form-group col-md-12" style="display:none">
                                            <label>Number to change</label>
                                            <input type="numeric" id="inv-count" name='count' class="form-control" disabled></input>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-save">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="transfers-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="form-group">
                                <h4>Select Items for transfer</h4>
                                <div class="form-group">
                                    <input type="text" id="tinventory-search" placeholder="Search by serial, name">
                                </div>
                                <table class="table" id="tinv_table"></table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <form class="form" id="transfer_form" action="/admin/custom_inventory/update_transfer" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="redirectTo" value="<?php echo '/admin/custom_inventory/update/'.$model->custom_inventory->id; ?>"/>
                            <div class="box">
                                <div class="form-group">
                                    <label>Destination: </label>
                                    <input type="hidden" name="from_loc" value="<?=$model->loc?>">
                                    <select name="to_loc">
                                        <? foreach($model->inventories as $inv){ ?>
                                            <option value="<?= $inv->id ?>"><?= $inv->name ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <h4>Items to Send Out</h4>
                                    <table class="table" id="rec_table">
                                    </table>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-save">Finalize and download order</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="receivings-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="form-group">
                                <h4>Mark Items as Received</h4>
                                <div class="form-group">
                                    <input type="text" id="rinventory-add" placeholder="Search by serial, name">
                                </div>
                                <table class="table" id="rinv_table"></table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="form-group" id="pending_orders">
                                <h4>Pending Orders</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"></script>
<script type="text/javascript">
    var transfer = {};
    if(Cookies.getJSON('transfer_<?=$model->loc?>')){
        transfer = Cookies.getJSON('transfer_<?=$model->loc?>');
    }
    function add_transfer(entry) {
        var prod_name = entry.data('product_name');
        var prod_id = entry.data('prodid');
        var count = entry.data('count')? entry.data('count'):1;
        var oStatus = entry.data('status');
        if(!transfer.hasOwnProperty(prod_name)){
            transfer[prod_name] = [];
        }
        transfer[prod_name].push({sku: entry.data('sku'), status:oStatus, prod_id:prod_id,  name:prod_name, quantity: count, id: entry.attr('id')});
    }
    function remove_transfer(entry) {
        var prod_name = entry[0]
        var prod_index = entry[1];
        transfer[prod_name].splice(prod_index,1);
    }
    $(document).ready(function () {
        $('#tinv_table').html($('#inv_table').html());
        $('#tinv_table tr.receive_item:not([id^=Bundle]) td').append($('<a class="add_transfer btn btn-info">+</a>'));
        $('#tinv_table tr.receive_item[id^=Bundle] td').append($('<a class="add_transfer btn btn-info">+</a><input type="number" />'));
        $('input#inventory-search,input#tinventory-search').on('input',function(){
           var rows = $(this).parents('.box').find('table tr');
           if(this.value == ''){
               rows.show();
           } else {
               rows.not("[data-product_name*='"+this.value.toLowerCase()+"'],[data-sku*='"+this.value+"']").hide();
               rows.filter("[data-product_name*='"+this.value.toLowerCase()+"'],[data-sku*='"+this.value+"']").show();
           }
        });
        $.post('/admin/custom_inventory/get_pending_orders',{loc:"<?=$model->loc?>"},function(data){
            data.orders.forEach(function(order){
                var table = $('<table class="table">').data('status',order.status).append('<tbody>').appendTo('#pending_orders').find('tbody');
                order.items.forEach(function(item){
                    var sorting = table.find('th').parent('tr.sort[data-product="'+item.name+'"],tr.sort[data-prodid="'+item.product_id+'"]:not([data-prodid="null"])');
                    if(sorting.length == 0){
                        table.append($("<tr class='sort' style='background-color: #f3f3f4' data-product='"+item.name+"' data-prodid='"+item.product_id+"'>").append('<th>'+item.name+'</th>'));
                        sorting = table.find('th').parent('tr.sort[data-product="'+item.name+'"],tr.sort[data-prodid="'+item.product_id+'"]:not([data-prodid="null"])');
                    }
                    sorting.after($("<tr class='transfer_item' data-sku='"+item.sku+"' data-status='"+item.status+"' data-id='"+item.id+"' data-name='"+item.name+"'>").append($('<td>').text(item.sku)));
                });
                table.parent().before('<h3>Order #'+order.id+'</h3>');
                table.parent().before('<label>Coming From: '+order.to_loc+'</label>');
                table.parent().after('<hr>');
                table.find('tr.sort').each(function(index){
                   let prodName = $(this).data('product');
                   let skuCount =  table.find('tr.transfer_item').filter(function(){
                       return ($(this).data('name') == prodName && !$(this).data('sku'));
                   }).hide();
                   if(skuCount.length > 0) {
                       $(this).after($("<tr class='bundle_item' data-name='"+prodName+"' ></tr>").append($('<td>').text('x '+skuCount.length)));
                   }
                });
            });
        });
        $('input#tinventory-search').keypress(function (e) {
            if(e.which == 13) {
                var rows = $(this).parents('.box').find('table tr');
                var transfers = $('table#rec_table');
                var entry = rows.filter("[data-sku='"+this.value+"']").addClass('in_order disabled');
                if(transfers.find("tr.sort[data-product='"+entry.data('name')+"']").length === 0){
                    transfers.append($("<tr class='sort' data-product='"+entry.data('name')+"' >").text(entry.data('name')));
                }
                var prod_heading = transfers.find("tr.sort[data-product='"+entry.data('name')+"']");
                entry.clone().insertAfter(prod_heading);
                add_transfer(entry);
            }
        });
        $('#rinventory-add').keypress(function(e){
            var entry = this.value;
           if(e.which == 13){
                $.post('/admin/custom_inventory/receive',{serial:entry, inv:'<?=$model->loc?>'},function(data){
                    $('tr.transfer_item[data-id='+data.titem.id+']').css('background-color','green');
                });
           }
        });
        $('#transfer_form').submit(function(){
            $(this).append($('<input type="hidden" name="items">').val(JSON.stringify(transfer)));
        });
        $('#inv_table tr.receive_item').click(function(){
            $('#inv-name').val($(this).data('name'));
            $('#inv-id').val($(this).attr('id'));
            $('#inv-prodid').val($(this).attr('prodid'));
            $('#inv-ppno').val($(this).data('ppno'));
            $('#inv-serial').val($(this).data('sku'));
            $('#inv-oStatus').val($(this).data('status'));
            var status = $('#inv-status');
            status.removeAttr('disabled').val($(this).data('status'));
            var counts = $('#inv-count');
            if($(this).attr('id').indexOf('Bundle') == 0){
                status.parent().addClass('col-md-12');
                counts.parent().show();
                counts.removeAttr('disabled');
            } else {
                status.parent().removeClass('col-md-12');
                counts.parent().hide();
                counts.attr('disabled','disabled');
            }
        });
        $('#tinv_table tr.receive_item').find('.add_transfer').click(function(){
            if($(this).parents('tr').hasClass('in_order') || $(this).parents('tr').hasClass('disabled') ) {
                return true;
            }
            var transfers = $('table#rec_table');
            var entry = $(this).parents('tr').addClass('in_order disabled');
            var tCount = $(this).siblings('input').val();
            if($(this).siblings('input').length){
                entry.data('count',tCount);
            }
            if(transfers.find("tr.sort[data-product='"+entry.data('name')+"']").length === 0){
                transfers.append($("<tr class='sort' data-product='"+entry.data('name')+"' >").text(entry.data('name')));
            }
            var prod_heading = transfers.find("tr.sort[data-product='"+entry.data('name')+"']");
            if($(this).siblings('input').length){
                entry.clone().insertAfter(prod_heading).find('.add_transfer').parent().text('(x '+tCount+')');
            } else {
                entry.clone().insertAfter(prod_heading).find('.add_transfer').remove();
            }
            add_transfer(entry);
        });
    });
</script>