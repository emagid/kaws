<?php if (count($model->custom_inventorys) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='20%'>Name</th>
                <th width='20%'>Total # of items (unique)</th>
                <th width='20%'>Total # of items in stock</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach (\Model\Actual_Inventory::$places as $id => $place) {?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>custom_inventory/update/<?php echo $id; ?>"><?php echo $place; ?></a></td>
                    <td><? echo Model\Actual_Inventory::getCount(['where'=>"$id IS NOT NULL AND $id != 0"]) ?></td>
                    <td><?= Model\Custom_Inventory::get_total_stock($id); ?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>custom_inventory/update/<?php echo $id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                </tr>
            <? } ?>
            <?php foreach ($model->custom_inventorys as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>custom_inventory/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
                    <td><?= Model\Product_Inventory::getCount(['where'=>"inventory_id = $obj->id"])?></td>
                    <td><?= Model\Custom_Inventory::get_total_stock($obj->id); ?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>custom_inventory/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>custom_inventory/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'custom_inventory';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>