<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->order_status->id;?>" />
    <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-12">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>Name</label>
                <?php echo $model->form->editorFor("name"); ?>
            </div>
            <div class="form-group">
                <label>Mail Title</label>
                <?php echo $model->form->editorFor("mailtitle"); ?>
            </div>
            <div class="form-group">
                <label>Subject</label>
                <?php echo $model->form->editorFor("subject"); ?>
            </div>
            <div class="form-group">
                <label>Pre Message text</label>
                <?php echo $model->form->textAreaFor("message" ,['class'=>'ckeditor']); ?>
            </div>
            <div class="form-group">
                <label>Available Syntax</label>
                <p><?=implode('<br>',['*|FIRST_NAME|*','*|BRAND|*','*|MPN|*','*|ORDER#|*'])?></p>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php footer();?>
</script>
 