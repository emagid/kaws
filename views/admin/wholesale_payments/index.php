<?php if (count($model->wholesale_payments) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='20%'>Order Id</th>
                <th width='20%'>Tracking Number</th>
                <th width='20%'>Status</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->wholesale_payments as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>wholesale_payments/update/<?php echo $obj->id; ?>"><?php echo $obj->order_id; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesale_payments/update/<?php echo $obj->id; ?>"><?php echo $obj->tracking_number; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesale_payments/update/<?php echo $obj->id; ?>"><?php echo $obj->status; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>wholesale_payments/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>wholesale_payments/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'wholesale_payments';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>