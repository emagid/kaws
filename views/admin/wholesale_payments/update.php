<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->wholesale_payment->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Order Id</label>
                    <?php echo $model->form->editorFor("order_id"); ?>
                </div>
                <div class="form-group">
                    <label>Tracking Number</label>
                    <?php echo $model->form->editorFor("tracking_number"); ?>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <?php echo $model->form->editorFor("status"); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'wholesale_payment/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>