<?php if(count($model->emails)>0): ?>
    <div class="box box-table">
        <table class="table" style='min-width: 1000px;'>
            <thead>
            <tr>
                <th width="5%">To</th>
                <th width="5%">From</th>
                <th width="5%">Subject</th>
                <th width="5%">State</th>
                <th width="5%">Date Sent</th>
                <th width="10%">Date last Opened</th>
                <th width="10%" class="text-center">View</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($model->emails as $obj): ?>
            <? $obj = (object)$obj; ?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>email_logs/update/<?php echo $obj->_id; ?>"><?=$obj->email?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>email_logs/update/<?php echo $obj->_id; ?>"><?=$obj->sender?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>email_logs/update/<?php echo $obj->_id; ?>"><?=$obj->subject?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>email_logs/update/<?php echo $obj->_id; ?>"><?=$obj->state?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>email_logs/update/<?php echo $obj->_id; ?>"><?=date('M d, Y',$obj->ts)?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>email_logs/update/<?php echo $obj->_id; ?>"><?
                            $last_open = $obj->opens_detail[0]['ts'];
                            if($last_open){
                                echo(date('M d, Y',$last_open));
                            } else if(isset($obj->reject)){
                                echo('Rejected: '.$obj->reject['reason']);
                            } else {
                                echo('Not Opened');
                            }
                            ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>email_types/update/<?php echo $email_type->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'email_types/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>

