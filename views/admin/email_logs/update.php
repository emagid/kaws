<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <input type="hidden" name="id" value="<?php echo $model->email_type->id?>"/>
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>To</label>
                    <input type="text" disabled value="<?=$model->mail_log->to_?>" />
                </div>
                <div class="form-group">
                    <label>From</label>
                    <input type="text" disabled value="<?=$model->mail_log->from_?>" />
                </div>

                <div class="form-group">
                    <label>Subject</label>
                    <input type="text" disabled value="<?=$model->mail_log->subject?>" />
                </div>

                <div class="form-group">
                    <label>Message</label>
                    <div class='message'>
                        <?=$model->mail_log->text?>
                    </div>
                </div>

                <!--<div class="form-group">
                    <label>Reply</label>
                </div>-->
            </div>
        </div>

        <style type="text/css">
            .disable{
                pointer-events:none;
            }

            .message {
                width: 100%;
                height: 200px;
                overflow: hidden;
                overflow-y: auto;
                border: 1px solid #cccccc;
                border-radius: 7px;
            }
        </style>

<!--        <div class="col-lg-24">-->
<!--            <button type="submit" class="btn btn-success btn-lg">Save</button>-->
<!--        </div>-->
    </div>
    </div>
</form>


<?= footer(); ?>


<script type='text/javascript'>


</script>