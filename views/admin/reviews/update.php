<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->review->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>Product</label>
                <input value="<?=\Model\Product::getItem($model->review->product_id)->name?>" class="form-control" disabled/>
                <input value="<?=$model->review->product_id?>" name="product_id" type="hidden" class="form-control" />
            </div>
            <div class="form-group">
                <label>Show this review on the front end?</label>
                <label><input type="radio" <?= $model->review->approved ? 'checked' : '' ?> name="approved" value="1" class="form-group"/>Yes</label>
                <label><input type="radio" <?= $model->review->approved ? '' : 'checked' ?> name="approved" value="0" class="form-group"/>No</label>
            </div>
            <div class="form-group">
                <label>Rating</label>
                <?php echo $model->form->textBoxFor("rating",['class'=>'numeric'])?>
            </div>
            <div class="form-group">
                <label>Customer</label>
                <?php echo $model->form->editorFor("name")?>
            </div>
            <div class="form-group">
                <label>Email</label>
                <?php echo $model->form->editorFor("email")?>
            </div>
            <div class="form-group">
                <label>Title</label>
                <?php echo $model->form->editorFor("title")?>
            </div>
            <div class="form-group">
                <label>Body</label>
                <?php echo $model->form->editorFor("body")?>
            </div>
            <div class="form-group">
                <label>Helpful</label>
                <?php echo $model->form->textBoxFor("helpful",['class'=>'numeric'])?>
            </div>
            <div class="form-group">
                <label>Unhelpful</label>
                <?php echo $model->form->textBoxFor("helpful",['class'=>'numeric'])?>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
  
</form>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'reviews/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
    $('.numeric').numericInput();
});
</script>