<?php if (count($model->reviews) > 0) { ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%">Product</th>
                <th width="10%">Rating</th>
                <th width="10%">Customer Name</th>
                <th width="10%">Customer Email</th>
                <th width="10%">Viewable</th>
                <th width="10%">Helpful</th>
                <th width="5%" class="text-center">Edit</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->reviews as $obj) { ?>
                <tr>
                    <td>
                        <!--Cripples loading times of page. Disabled for now until reviews have product_id-->
                        <? if($obj->product_id != 0) {?>
                            <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo \Model\Product::getItem($obj->product_id)->name; ?></a>
                        <? } ?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->rating; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->approved?'Visibile':'Hidden'; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>"><?php echo $obj->helpful .' of '.($obj->helpful + $obj->unhelpful); ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>reviews/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black"
                           href="<?php echo ADMIN_URL; ?>reviews/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'reviews';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

