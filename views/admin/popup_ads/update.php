<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input name="id" type="hidden" value="<?php echo $model->popup_ad->id; ?>"/>
    <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor('title'); ?>
                    <small>Use |PROD| and |COUP| for product name and coupon code in the title</small>
                </div>
                <div class="form-group">
                    <label>Subtitle</label>
                    <?php echo $model->form->editorFor('subtitle'); ?>
                    <small>Use |PROD| and |COUP| for product name and coupon code in the subtitle</small>
                </div>
<!--                <div class="form-group">-->
<!--                    <label>Styles</label>-->
<!--                    <select name="style" class="form-control">-->
<!--                        --><?//foreach(\Model\Popup_Ad::$styles as $id=>$style){?>
<!--                            <option value="--><?//=$id?><!--">--><?//=$style?><!--</option>-->
<!--                        --><?//}?>
<!--                    </select>-->
<!--                </div>-->

                <!--<div class="form-group">
                    <label>Featured image</label>
                    <?php /*$img_path = ""; */?>
                    <p><input type="file" name="featured_image" class='image'/></p>
                    <div style="display:inline-block">
                        <?php /*if ($model->popup_ad->featured_image != "" && file_exists(UPLOAD_PATH . 'popup_ads' . DS . $model->popup_ad->featured_image)) {
                            $img_path = UPLOAD_URL . 'popup_ads/' . $model->popup_ad->featured_image; */?>
                            <div class="well well-sm pull-left">
                                <img src="<?php /*echo $img_path; */?>" width="100"/>
                                <br/>

                                <a href="<?/*= ADMIN_URL . 'popup_ads/delete_image/' . $model->popup_ad->id; */?>?token_id=<?php /*echo get_token(); */?>"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="featured_image" value="<?/*= $model->popup_ad->featured_image */?>"/>
                            </div>
                        <?php /*} */?>
                        <div class='preview-container'></div>
                    </div>
                </div>-->
                <div class="form-group">
                    <label>Product</label>
                    <select name="product_id" class="form-control">
                        <option value="-1" <?=-1 == $model->popup_ad->product_id ? 'selected': ''?>>Display Current Product</option>
                        <?foreach($model->products as $product){?>
                            <option value="<?=$product->id?>" <?=$product->id == $model->popup_ad->product_id ? 'selected': ''?>><?=$product->name?></option>
                        <?}?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Coupon</label>
                    <select name="coupon_id" class="form-control">
                        <?foreach($model->coupons as $coupon){?>
                            <option value="<?=$coupon->id?>" <?=$coupon->id == $model->popup_ad->coupon_id ? 'selected': ''?>><?=$coupon->name?></option>
                        <?}?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '600');
                    img.attr('height', '172');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);

        });

        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

    });

</script>