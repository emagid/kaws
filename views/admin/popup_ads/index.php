<div class="row">
    <div class="col-md-16"></div>
    <div class="col-md-8">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
</div>
<?php if (count($model->popup_ads) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
<!--                <th width="5%">Image</th>-->
                <th width="15%">Title</th>
                <th width="15%">Subtitle</th>
                <th width="15%">Product</th>
                <th width="15%">Coupon</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->popup_ads as $obj) { ?>
                <tr class="originalBrands">
                    <!--<td>
                        <?php
/*                        $img_path = "";
                        if ($obj->featured_image != "" && file_exists(UPLOAD_PATH . 'popup_ads' . DS . $obj->featured_image)) {
                            $img_path = UPLOAD_URL . 'popup_ads/' . $obj->featured_image;
                            */?>
                            <img src="<?php /*echo $img_path; */?>" width="50"/>
                        <?php /*} */?>
                    </td>-->
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>popup_ads/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>popup_ads/update/<?php echo $obj->id; ?>"><?php echo $obj->subtitle; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->product_id; ?>"><?php echo $obj->product_id == -1 ? 'Display current product':\Model\Product::getItem($obj->product_id)->name; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $obj->coupon_id; ?>"><?php echo \Model\Coupon::getItem($obj->coupon_id)->name; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>popup_ads/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>popup_ads/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'popup_ads';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>popup_ads/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalBrands').remove();
                    $('.paginationContent').hide();
                    $('.originalBrands').hide();

                    var list = JSON.parse(data);

                    for (key in list) {
                        var tr = $('<tr />');
                        if (list[key].image != '') {
                            var img = $('<img />').prop('width', 50).prop('src', list[key].image);
                        }
                        $('<td />').appendTo(tr).html(img);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>popup_ads/update/' + list[key].id).html(list[key].name));
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>popup_ads/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>popup_ads/delete/' + list[key].id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalBrands').remove();
                $('.paginationContent').show();
                $('.originalBrands').show();
            }
        });
    })
</script>

