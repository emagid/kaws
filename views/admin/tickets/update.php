<?php
$sender = $model->service_ticket->email;
$inbox = new EmagidService\MailBox('gator4149.hostgator.com', 'Service@Camrise.com', 'Hello1010');
$emails = $inbox->getMailBySender($sender);

$logs = \Model\Service_Log::getList(['where' => ['service_ticket_id' => $model->service_ticket->id]]);

?>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->service_ticket->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#product-tab" aria-controls="product" role="tab" data-toggle="tab">Product</a></li>
            <li role="presentation"><a href="#history-tab" aria-controls="history" role="tab" data-toggle="tab">RMA Process</a></li>
            <li role="presentation"><a href="#status-tab" aria-controls="history" role="tab" data-toggle="tab">Status</a></li>
            <li class="<?=!isset($model->rma_history[6]) ? 'hidden' : ''?>" role="presentation"><a href="#estimate-tab" aria-controls="estimate" role="tab" data-toggle="tab">Estimate</a></li>
            <li role="presentation"><a href="#technician-tab" aria-controls="technician" role="tab" data-toggle="tab">Technicians</a></li>
            <li role="presentation" class="active" style="float: right;"><a><span style="color:black">Progress:</span>
                    <?
                        $progress_text ='';
                    if(isset($model->rma_history[4])) {//RMA Approved
                        $progress_text .= "<span style='color:black'>RMA Approved</span>";
                    }
                    if(isset($model->rma_history[6])) {//Received
                        $progress_text .= "<span style='color:black'>-Received</span>";
                    }
                    if(isset($model->rma_history[37])) {//Estimate submitted by Manager
                        $progress_text .= "<span style='color:black'>-Estimate submitted to Manager</span>";
                    }
                    if(isset($model->rma_history[12])) {//Estimate approved by Manager
                        $progress_text .= "<span style='color:black'>-Estimate approved by Manager</span>";
                    }
                    if(isset($model->rma_history[21])) {//Customer Approved Repair
                        $progress_text .= "<span style='color:black'>-Customer Approved Repair</span>";
                    } else  if(isset($model->rma_history[16])) {//Customer Declined Repair
                        $progress_text .= "<span style='color:black'>-Customer Declined Repair</span>";
                    }
                    if(isset($model->rma_history[3])) {//RMA complete
                        $progress_text .= "<span style='color:black'>-RMA complete</span>";
                    }
                    echo $progress_text
                    ?>
                <??></a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12" style="width:100%;">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Creation Date</label>
                                <input type="text" name="insert_time" readonly value="<?=(new \Carbon\Carbon($model->service_ticket->insert_time))->toDateTimeString()?>" />
                            </div>

                            <div class="form-group">
                                <label>Modify Date</label>
                                <input type="text" readonly value="<?=(new \Carbon\Carbon($model->service_ticket->modified_time))->toDateTimeString()?>" />
                            </div>

                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" readonly name="firstname" value="<?=$model->service_ticket->firstname?>" />
                            </div>

                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" readonly name="lastname" value="<?=$model->service_ticket->lastname?>" />
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" value="<?=$model->service_ticket->email?>" />
                            </div>

                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" readonly name="phone" value="<?=$model->service_ticket->phone?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="width:100%;">
                        <div class="box">
                            <h4>Inbox</h4>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Date</td>
                                        <td>Content</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($emails){ ?>
                                    <?php foreach($emails as $email){ ?>
                                    <tr>
                                        <td><?=$email->date?></td>
                                        <td><?=$email->textHtml?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php } else { ?>
                                    <tr>
                                        <td colspan="2">No Emails</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="box">
                            <h4>Action Log</h4>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>Date</td>
                                    <td>Action</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($logs){ ?>
                                    <?php foreach($logs as $log){ ?>
                                        <tr>
                                            <td><?=$log->insert_time?></td>
                                            <td><?=$log->action?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="2">No Actions</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="product-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Product</h4>
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" readonly name="product" value="<?=$model->service_ticket->product?>" />
                            </div>
                            <div class="form-group">
                                <label>Model</label>
                                <input type="text" readonly name="product" value="<?=$model->service_ticket->product_model?>" />
                            </div>

                            <div class="form-group">
                                <label>Serial #</label>
                                <input type="text" readonly name="model" value="<?=$model->service_ticket->model?>" />
                            </div>

                            <div class="form-group">
                                <label>Comment</label>
                                <textarea name="comment" readonly><?=$model->service_ticket->comment?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="estimate-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Estimate</h4>
                            <div class="form-group">
                                <select style="width: 50%" id="estimate-product-list">
                                    <option id="0">--SELECT--</option>
                                    <?php foreach(\Model\Product::getRepairProducts() as $product){ ?>
                                        <option value="<?=$product->id?>" data-name="<?=$product->name?>" data-price="<?=$product->price?>"><?=$product->name.' --- $'. $product->price?></option>
                                    <?php } ?>
                                </select>

                                <button class="btn btn-info" id="estimate-add-part">Add Parts</button>
                            </div>
                            <div class="form-group">
                                <label>Misc Charges (labor, etc)</label>
                                <input id="estimate_misc_product" placeholder="Name" type="text"/>
                                <input id="estimate_misc_cost" placeholder="Cost" type="text" pattern="(-){0,1}[0-9]+(\.[0-9]+){0,1}"/>
                                <button class="btn btn-info" id="estimate-add-misc">Add Custom</button>
                            </div>

<!--                            <div class="form-group">-->
<!--                                <input type="text" id="estimate-charge-name" placeholder="Charge Name" style="display: inline;width: 30%"/>-->
<!--                                <input type="text" id="estimate-charge-amount" placeholder="Charge Amount" style="display: inline;width: 50%"/>-->
<!--                                <button class="btn btn-info" id="estimate-add-charge">Add Charges</button>-->
<!--                            </div>-->

                            <div class="form-group">
                                <label>Manager</label>
                                <select id="estimate-approve-manager">
                                    <option value="0">--SELECT--</option>
                                    <?php foreach(\Model\Admin::getList() as $admin){ ?>
                                        <option value="<?=$admin->id?>"><?=$admin->first_name.' '.$admin->last_name?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <button class="btn btn-danger" id="estimate-submit">Submit estimate to manager</button>
                            <button class="btn btn-danger" id="estimate-clear">Clear</button>
                        </div>

                        <div class="box">
                            <h4>History</h4>
                            <?php $histories = $model->service_ticket->getEstimateHistoryData(); ?>
                            <?php if($histories){ ?>
                            <?php foreach($histories as $data){ if(!$data) continue; $total = 0; ?>
                            <div class="box">
                                <table class="table">
                                    <tbody>
                                    <?php foreach(array_merge($data['parts'], isset($data['charges'])?$data['charges']:[]) as $d){ $total += floatval($d['price']);?>
                                        <tr>
                                            <td><?=$d['name']?></td>
                                            <td>$<?=$d['price']?></td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Total</td>
                                        <td>$<?=$total?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <?php $data = $model->service_ticket->getEstimateData();
                        $managerName = $model->service_ticket->getAssignedManagerName();
                        if($model->service_ticket->estimate_details && $model->service_ticket->assigned_manager){
                            $total = 0;
                        ?>
                        <h4>Existing Details</h4>
                        <div class="box">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Manager</td>
                                        <td colspan="2"><?=$managerName?></td>
                                    </tr>
                                    <?php foreach(array_merge($data['parts'], []) as $index => $d){ $total += floatval($d['price']);?>
                                        <tr>
                                            <td><?=$d['name']?></td>
                                            <td>$<?=$d['price']?></td>
                                            <td><button class="btn btn-danger delete-existing" data-parts-index="<?=$index?>">Delete</button></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if($model->service_ticket->shipping){ $total += floatval($model->service_ticket->shipping);?>
                                    <tr>
                                        <td>Shipping</td>
                                        <td colspan="2"> $<?=$model->service_ticket->shipping?></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($model->service_ticket->discount){ $total -= floatval($model->service_ticket->discount);?>
                                    <tr>
                                        <td>Discount</td>
                                        <td colspan="2">- $<?=$model->service_ticket->discount?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>Total</td>
                                        <td colspan="2">$<?=$total?></td>
                                    </tr>
                                    <tr>
                                        <td>Invoice Link</td>
                                        <td colspan="2"><a href="<?='http://'.SITE_DOMAIN.'/repair_invoice/'.$model->service_ticket->invoice_number;?>"><?=SITE_DOMAIN.'/repair_invoice/'.$model->service_ticket->invoice_number;?></a></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php $manager = $model->service_ticket->getAssignedManger(); if($manager && $manager->id == \Emagid\Core\Membership::userId()){ ?>
                                <div class="form-group">
                                    <label>Manager</label>
                                    <select id="estimate-manager">
                                        <option value="0">--SELECT--</option>
                                        <?php foreach(\Model\Admin::getList() as $admin){ ?>
                                            <option value="<?=$admin->id?>"><?=$admin->first_name.' '.$admin->last_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <button class="btn btn-primary" id="estimate-approve">Approve</button><br>
                            <input type="number" id="estimate-discount" placeholder="Discount Amount, put 0 to remove discount" style="display: inline;width: 50%"/>
                            <button class="btn btn-info" id="estimate-add-discount">Add Discount</button>
                                <br />
                            <input type="number" id="estimate-shipping" placeholder="Shipping Fee, defaults to 0" style="display: inline;width: 50%"/>
                            <button class="btn btn-info" id="estimate-set-shipping">Set Shipping</button>
                            <?php } ?>
                        </div>
                        <?php } ?>
                        <h4>Details</h4>
                        <div class="box">
                            <table class="table">
                                <tbody id="estimate-detail-body">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="history-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>History</h4>
                            <?php $histories = $model->service_ticket->getHistories();?>
                            <?php if($histories) { ?>
                                <table id="data-list" class="table">
                                    <thead>
                                    <tr>
                                        <th width="15%">Date</th>
                                        <th width="15%">Comment</th>
                                        <th width="15%">Status</th>
                                        <th width="15%">Notify</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($histories as $obj) { ?>
                                        <tr class="originalProducts">
                                            <td><?=(new \Carbon\Carbon($obj->insert_time))->toDateTimeString()?></td>
                                            <td><?=$obj->comment?></td>
                                            <td><?=$obj->getStatusName();?></td>
                                            <td><?=$obj->notify?'YES':'NO';?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } ?>

                            <? if($model->service_ticket->service_status_id == 4 ) { ?>
                                <button class="btn btn-info" id="rma-receive">Product Received</button>
                                <input type="hidden" name="history[status]">
                                <input type="hidden" name="history[notify]">
                                <input type="hidden" name="history[comment]">
                                <div class="form-group comment-history">
                                    <label>Received Items:
                                        <span id="products-extras"></span></label>
                                </div>
                                <label>Remote Controller</label>
                                <input type="checkbox" class='extras' data-extra="remote" value="Remote Controller">
                                <br />
                                <label>Case</label>
                                <input type="checkbox" class='extras' data-extra="case" value="Case">
                                <br />
                                <label>SD Card</label>
                                <input type="checkbox" class='extras' data-extra="sd" value="SD Card">
                                <br />
                                <label>Camera</label>
                                <input type="checkbox" class='extras' data-extra="camera" value="Camera">
                                <br />
                                <div class="form-group comment-history">
                                    <label>Received Extras</label>
                                    <input type="text" class="extras"></input>
                                </div>
                            <? } else if(!isset($model->rma_history[4])) { ?>
                                <button class="btn btn-info" id="rma-approve">Approve RMA</button>
                                <input type="hidden" name="history[status]">
                                <input type="hidden" name="history[notify]">
                                <input type="hidden" name="history[comment]">
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="status-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Update Status</h4>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="history[status]">
                                    <option value="0">--CHOOSE ONE --</option>
                                    <?php foreach(\Model\Service_Status::getList() as $status){ ?>
                                        <option value="<?=$status->id?>"><?=$status->name?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Notify Customer</label>
                                <input type="checkbox" value="1" name="history[notify]">
                            </div>

                            <div class="form-group">
                                <label>Comment</label>
                                <textarea name="history[comment]" class="ckeditor"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="technician-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Technician Notes</h4>
                            <?php $histories = $model->service_ticket->getTechnicians();?>
                            <?php if($histories) { ?>
                                <table id="data-list" class="table">
                                    <thead>
                                    <tr>
                                        <th width="15%">Date</th>
                                        <th width="15%">Comment</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($histories as $obj) { ?>
                                        <tr class="originalProducts">
                                            <td><?=(new \Carbon\Carbon($obj->insert_time))->toDateTimeString()?></td>
                                            <td><?=$obj->comment?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            <?php } ?>

                            <h4>New</h4>

                            <div class="form-group comment-estimate">
                                <label>Comment</label>
                                <textarea name="technician[comment]" class="ckeditor"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <center style="margin-bottom: 112px;">
        <button id="save" class="btn btn-save" type="submit">Save</button>
        <button id="save_close" class="btn btn-save">Save</button>
    </center>
    <!--</form>-->
    <?php footer(); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#estimate-product-list").chosen({width: "500px"});
            $('#save_close').on('click',function () {
//                window.location.replace('<?//=ADMIN_URL."tickets"?>//');
            });

            function printEstimate(estimate){
                var s = '';
                var total = 0;
                for(var i in estimate.parts){
                    var d = estimate.parts[i];
                    s += '<tr>';
                    s += '<td>' + d['name'] + '<td/>';
                    s += '<td>$' + d['price'] + '<td/>';
                    s += '<td><button class="btn btn-danger delete-parts" data-parts-index='+i+'>Delete</button><td/><tr/>';
                    total += parseFloat(d['price']);
                }

                //for(var j in estimate.charges){
                //    var d = estimate.charges[j];
                //    s += '<tr>';
                //    s += '<td>' + d['name'] + '<td/>';
                //    s += '<td>$' + d['price'] + '<td/><tr/>';
                //    total += parseFloat(d['price']);
                //}

                s += '<tr>';
                s += '<td>Total<td/>';
                s += '<td colspan="2">$' + total + '<td/><tr/>';

                $('#estimate-detail-body').empty().append(s);
            }
            var estimate = {parts: [], charges: []};

            if(Cookies.getJSON('estimate')){
                estimate = Cookies.getJSON('estimate');
            }

            if(estimate.parts.length > 0 || estimate.charges.length > 0){
                printEstimate(estimate);
            }

            var Part = function(id, name, price){
                this.id = id;
                this.name = name;
                this.price = price;
            };

            var Charge = function(name, price){
                this.name = name;
                this.price = price;
            };

            $('#estimate-add-part').on('click', function(e){
                e.preventDefault();
                var option = $('#estimate-product-list').find(':selected');
                if(!option || option.val() == 0){
                    alert('Select a product pls!'); return;
                }
                estimate.parts.push((new Part(option.val(), option.data('name'), option.data('price'))));
                Cookies.set('estimate', estimate);
                printEstimate(estimate);
            });

            $('#estimate-add-misc').on('click',function(e){
               e.preventDefault();
               let optionName = $('#estimate_misc_product').val();
               let optionCost = $('#estimate_misc_cost').val();
               if(isNaN(optionCost)){
                   alert('Enter a Valid Cost!');
               } else {
                   optionCost = Number(optionCost).toFixed(2);
                   estimate.parts.push(new Part(0,optionName,optionCost));
                   Cookies.set('estimate',estimate);
                   printEstimate(estimate);
               }
            });

            $('#estimate-add-charge').on('click', function(e){
                e.preventDefault();
                var name = $('#estimate-charge-name').val();
                var amount = $('#estimate-charge-amount').val();
                amount = parseFloat(amount);
                if(!name || !amount){
                    alert('Fill up charge name and amount pls!'); return;
                }
                estimate.charges.push((new Charge(name, amount)));
                printEstimate(estimate);
            });

            $('#estimate-clear').on('click', function(e){
                e.preventDefault();
                estimate = {parts: [], charges: []};
                printEstimate(estimate);
            });

            $('#estimate-submit').on('click', function(e){
                e.preventDefault();
                var manager = $('#estimate-approve-manager').val();

                if(manager == 0 || (estimate.parts.length == 0 && estimate.charges.length == 0)) {
                    alert('You have to select at least one parts and charges and a manager');
                    return;
                }
                $.post('/admin/tickets/add_estimate', {id: '<?=$model->service_ticket->id?>', payload: estimate, manager: manager}, function(e){
                    Cookies.remove('estimate');
                    $("select[name='history[status]']").val(37);
                    //$("[name='history[notify]']").click();

//                    $('html, body').animate({
//                        scrollTop: $("select[name='history[status]']").parents('.box').offset().top
//                    }, 1000);
                    $('#save_close').click();
                })
            });

            $('#estimate-approve').on('click', function(e){
                e.preventDefault();
                if(estimate.parts.length > 0 || estimate.charges.length > 0){
                    alert('You have unsaved estimate');
                    return;
                }
                var manager = $('#estimate-manager').val();
                if(manager == 0) {
                    alert('You have to select a manager');
                    return;
                }
                $.post('/admin/tickets/approve_estimate', {id: '<?=$model->service_ticket->id?>', manager: manager}, function(e){
                    alert(e.message);
                    $("[name='history[status]']").val(12);
                    $("[name='history[notify]']").val(1);
                    $("[name='history[notify]']").prop('checked',true);
                    $('a[href=#status-tab]').click();

                })
            });
            $('#rma-approve').on('click', function(){
                $('[name="history[status]"]' ).val(4);
                $("[name='history[notify]']").val(1);
                $("[name='history[notify]']").prop('checked',true);
                $("textarea[name='history[comment]']").parents('.form-group').remove();
                $('[name="history[comment]"]').val('RMA Process Approved');

                $('#save_close').click();
            });
            $('#rma-receive').on('click', function(){
                $('[name="history[status]"]' ).val(6);
                $("[name='history[notify]']").val(1);
                $("[name='history[notify]']").prop('checked',true);
                $("textarea[name='history[comment]']").parents('.form-group').remove();

                $('form').submit();
                $('#save_close').click();
            });
            $('input.extras').on('change', function(){displayExtras()});
            $('input[type="text"].extras').on('input', function(){displayExtras()});



            $(document).on('click', '.delete-parts', function(e){
                e.preventDefault();
                var index = $(this).data('parts-index');
                estimate.parts.splice(index, 1);
                printEstimate(estimate);
            });

            $('#estimate-add-discount').on('click', function(e){
                e.preventDefault();
                var amount = $('#estimate-discount').val();
                $.post('/admin/tickets/add_discount', {id: '<?=$model->service_ticket->id?>', amount: amount}, function(e){
                    window.location.reload();
                })
            });

            $('#estimate-set-shipping').on('click', function(e){
                e.preventDefault();
                var amount = $('#estimate-shipping').val();
                $.post('/admin/tickets/set_shipping', {id: '<?=$model->service_ticket->id?>', amount: amount}, function(e){
                    window.location.reload();
                })
            });

            $('.delete-existing').on('click', function(e){
                e.preventDefault();
                $.post('/admin/tickets/delete_existing_parts', {id: '<?=$model->service_ticket->id?>', index: $(this).data('parts-index')}, function(e){
                    window.location.reload();
                });
            })

        })

        function displayExtras(){
            console.log('h');
            let extraArr = $('input.extras:checked');
            let extras = '';
            if(extraArr.length == 0){
                extras = $('input[type="text"].extras').val();
                $("#products-extras").html("<b>"+extras+"</b>");
                $("input[name='history[comment]']").val(extras);
            } else {
                extraArr.each(function(index, e){
                    let comma = ',';
                    if(extraArr.length-1 == index) {
                        if(!$('input[type="text"].extras').val()){
                            comma = (extraArr.length == 2 ? ' and' : ', and');
                        }
                    }
                    extras = extras +
                        (extras == '' ? '' :comma) +
                        " <b>"+ e.value+"</b>";
                });
                if($('input[type="text"].extras').val()){
                    extras = extras + (extraArr.length == 1 ? ' and ' : ', and ') + $('input[type="text"].extras').val();
                }
                $("#products-extras").html("<b>"+extras+"</b>");
                $("input[name='history[comment]']").val(extras);
            }
        }

    </script>
</form>