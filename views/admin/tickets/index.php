<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="input-group" style="width: 100%" >
                <input id="date-picker" type="text" name="search" class="form-control" placeholder="Pick Date Range"/>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Customer Name or Product or #Serial or Status"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="" style="padding-bottom: 52px;">
            <div class="input-group" style="width: 100%" >
                <form id="status-form" method="POST" action="/admin/tickets/mass_update_status">
                    <label>Apply status to all selected:</label>
                    <select id="apply-status" name="status">
                        <?php foreach(\Model\Service_Status::getList() as $status){ ?>
                            <option value="<?=$status->id?>"><?=$status->name?></option>
                        <?php } ?>
                    </select>
                    <a id="status_btn" class="btn-actions btn" type="submit">APPLY</a>
                </form>
            </div>
        </div>
    </div>

<!--    <div class="col-md-4">-->
<!--        <div class="box">-->
<!--            <div class="input-group">-->
<!--                <select class="multiselect" id="status-fitler-select" multiple>-->
<!--                    --><?php //foreach(\Model\Service_Status::getList() as $status){ ?>
<!--                        <option value="--><?//=$status->id?><!--">--><?//=$status->name?><!--</option>-->
<!--                    --><?php //} ?>
<!--                </select>-->
<!--                <span class="input-group-addon" id="status-filter">-->
<!--                    <i class="icon-search"></i>-->
<!--                </span>-->
<!--            </span>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<?php if (count($model->service_tickets) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="5%"> </th>
                <th width="5%"> </th>
                <th width="15%">ID</th>
                <th width="15%">Date</th>
                <th width="15%">Modified</th>
                <th width="15%">Name</th>
                <th width="15%">Product</th>
                <th width="15%">Serial</th>
                <th width="15%">Status</th>
                <th class="text-center">Details</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->service_tickets as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <input form="status-form" type="checkbox" name="tickets[]" value="<?=$obj->id ?>">
                    </td>
                    <td><? if (is_null($obj->viewed) || !$obj->viewed) { ?>
                            <button type="button" class="btn btn-success" disabled="disabled">New</button>
                        <? } else if($obj->service_status_id == 37) { ?>
                            <button type="button" class="btn btn-success" disabled="disabled">Estimate Pending</button>
                        <? } else if($obj->service_status_id == 26) { ?>
                            <? $order = \Model\Order::getItem(null,['where'=>"ticket_id = $obj->id",'orderBy'=>'id DESC']);?>
                            <a href="/admin/orders/update/<?=$order->id?>"><button type="button" class="btn btn-success" disabled="disabled">Invoice Paid</button></a>
                        <? }?></td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>tickets/update/<?php echo $obj->id; ?>">
                            <?php  echo '#'.$obj->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>tickets/update/<?php echo $obj->id; ?>">
                            <?php $date = new DateTime($obj->insert_time); echo $date->format('Y-m-d H:i:s'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>tickets/update/<?php echo $obj->id; ?>">
                            <?php $date = new DateTime($obj->modified_time); echo $date->format('Y-m-d H:i:s'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>tickets/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->firstname .' '. $obj->lastname; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>tickets/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->product ; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>tickets/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->model ; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>tickets/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->getStatusName() ; ?>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>tickets/update/<?php echo $obj->id; ?>">
                            <i class="icon-eye"></i>
                        </a>
                    </td>
<!--                    <td class="text-center">-->
<!--                        <a class="btn-actions" href="--><?php //echo ADMIN_URL; ?><!--tickets/delete/--><?php //echo $obj->id; ?><!--"-->
<!--                           onClick="return confirm('Are You Sure?');">-->
<!--                            <i class="icon-cancel-circled"></i>-->
<!--                        </a>-->
<!--                    </td>-->

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '<?= ADMIN_URL.'tickets';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $('#date-picker').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: false,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?=\Carbon\Carbon::now()->toDateString()?>",
            endDate: "<?=\Carbon\Carbon::now()->toDateString()?>"
        }, function(){
            var dates = $('#date-picker').val().split(' - ');
            var start = null; var end = null;
            if(dates.length == 2){
                start = dates[0];
                end = dates[1];
            } else {
                return ;
            }

            if(!start || !end) return;

            console.log(start, end);

            var url = "<?php echo ADMIN_URL; ?>tickets/search";
            $.get(url, {date_start: start, date_end:end}, function (data) {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').hide();
                $('.originalProducts').hide();
                var list = JSON.parse(data);
                for (key in list) {
                    var tr = $('<tr />');
                    $('<td />').appendTo(tr).html('');
                    $('<td />').appendTo(tr).html(list[key].id);
                    $('<td />').appendTo(tr).html(list[key].insert_time.replace(/\.[0-9]+/g,''));
                    $('<td />').appendTo(tr).html(list[key].modified_time.replace(/\.[0-9]+/g,''));
                    $('<td />').appendTo(tr).html(list[key].firstname + ' ' + list[key].lastname);
                    $('<td />').appendTo(tr).html(list[key].product);
                    $('<td />').appendTo(tr).html(list[key].model);
                    $('<td />').appendTo(tr).html(list[key].status);
                    var editTd = $('<td />').addClass('text-center').appendTo(tr);
                    var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>tickets/update/' + list[key].id);
                    var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                    tr.appendTo($("#data-list tbody"));
                }

            });
        });

        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>tickets/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                var send = {keywords: keywords};

                var dates = $('#date-picker').val().split(' - ');
                if(dates.length == 2){
                    send['date_start'] = dates[0];
                    send['date_end'] = dates[1];
                }
                $.get(url, send, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html('');
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].modified_time);
                        $('<td />').appendTo(tr).html(list[key].firstname + ' ' + list[key].lastname);
                        $('<td />').appendTo(tr).html(list[key].product);
                        $('<td />').appendTo(tr).html(list[key].model);
                        $('<td />').appendTo(tr).html(list[key].status);
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>tickets/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });

        $('#status-filter').on('click', function(e){
            e.preventDefault();
            var statusArr = $('#status-fitler-select').val();
            var queryObj = {status: statusArr};
            window.location.href = '/admin/tickets?'+decodeURIComponent($.param(queryObj));
        });

        $('#status-fitler-select').multiselect();

        $('#status_btn').click(function(e){
            e.preventDefault();
           let status = $('#apply-status').find(":selected").text();
           var tickets = [];
           $('input[name="tickets[]"]:checked').each(function(){
              tickets.push(this.value);
           });
           if(confirm("Apply status '"+status+"' to tickets "+tickets.join(', ')+"?")){
               $('#status-form').submit();
           }
        });
    })
</script>