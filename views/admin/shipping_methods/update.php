<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<input type="hidden" name="id" value="<?php echo $model->shipping_method->id;?>" />
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Name</label>
					<?php echo $model->form->editorFor("name"); ?>
				</div>
				<div class="form-group">
					<label>Country</label>
					<?php echo $model->form->dropDownListFor("country",get_countries(),'',['class'=>'form-control']); ?>
				</div>
<!--				<div class="form-group">
					<label>Default?</label>
                    <input type="checkbox" name="is_default" value="1" <?/*=$model->shipping_method->is_default?'checked':''*/?>>
				</div>-->
				<div class="row clonable">
					<div class="col-xs-12">
						<div class="form-group">
							<label>Initial Cost</label>
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="text" name="cost[]" value="<?=(count($model->shipping_method->cost) > 0)?$model->shipping_method->cost[0]:''?>" />
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="form-group">
							<label>Additional Item Cost</label>
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="text" name="cost[]" value="<?=(count($model->shipping_method->cost) > 1)?$model->shipping_method->cost[1]:''?>" />
							</div>
						</div>
					</div>
					<div class="col-xs-8" style="display: none;">
						<div class="form-group">
							<label>Min cart subtotal</label>
							<div class="input-group">
								<span class="input-group-addon">$</span>
								<input type="text" name="cart_subtotal_range_min[]" value="<?=(count($model->shipping_method->cart_subtotal_range_min) > 0)?$model->shipping_method->cart_subtotal_range_min[0]:0?>" />
							</div>
							<? $max = max(count($model->shipping_method->cost), count($model->shipping_method->cart_subtotal_range_min));?>
							<p class="help-block" style="<?=($max > 1)?'display:none;':''?>" >Input 0 if not a range method</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<button type="submit" class="btn btn-save">Save</button>
</form>

<?php footer();?>

<script>
	$(function(){
		$('.add-range').click(function(e){
			e.preventDefault();
			var row = $('.clonable').clone();
			row.removeClass('clonable').addClass('clone').appendTo('form .box');
			row.find('label').remove();
			var rowBtn = row.find('button');
			rowBtn.removeClass('add-range').removeClass('btn-info').addClass('btn-warning');
			rowBtn.text('Remove');
			rowBtn.click(function(e){
				e.preventDefault();
				if ($('form .clone').size() == 1) {
					$('form p').show();		
				}
				$(this).parents('.clone').remove();
				return false;
			});
			$('form p').hide();
			return false;
		});
		$('.remove-range').click(function(e){
			e.preventDefault();
			if ($('form .clone').size() == 1) {
				$('form p').show();		
			}
			$(this).parents('.clone').remove();
			return false;
		})
	})
</script>