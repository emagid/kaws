<?php if(count($model->shipping_methods)>0) { ?>
<div class="box box-table">
	<table class="table">
		<thead>
			<tr>
				<th width="5%"></th> 
				<th width="25%">Name</th> 
				<th width="20%">Country</th>
				<th width="20%">Shipping Cost </th>
				<th width="20%">Addtl. cost p/item</th>
				<th width="15%" class="text-center">Edit</th>
				<th width="15%" class="text-center">Delete</th>	
			</tr>
		</thead>
		<tbody>
		<?php foreach($model->shipping_methods as $obj){ ?>
			<tr>
				<td>
					<? if ($obj->is_default) { ?>
						<button type="button" class="btn btn-success" disabled="disabled">Default</button>
					<? } ?>
				</td>
				<td><a href="<?php echo ADMIN_URL; ?>shipping_methods/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
				<td><a href="<?php echo ADMIN_URL; ?>shipping_methods/update/<?php echo $obj->id; ?>"><?php echo $obj->country; ?></a></td>
				<td><a href="<?php echo ADMIN_URL; ?>shipping_methods/update/<?php echo $obj->id; ?>">$<?php echo $obj->cost != null && count($obj->cost) > 0? number_format($obj->cost[0],2):'0.00'; ?></a></td>
				<td><a href="<?php echo ADMIN_URL; ?>shipping_methods/update/<?php echo $obj->id; ?>">$<?php echo $obj->cost != null && count($obj->cost) > 1? number_format($obj->cost[1],2):'0.00'; ?></a></td>
				<td class="text-center">
					<a class="btn-actions" href="<?php echo ADMIN_URL; ?>shipping_methods/update/<?php echo $obj->id; ?>">
						<i class="icon-pencil"></i> 
					</a>
				</td>
				<td class="text-center">
					<a class="btn-actions" href="<?php echo ADMIN_URL; ?>shipping_methods/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
						<i class="icon-cancel-circled"></i> 
					</a>
				</td>
			</tr>
		<?php } ?>
	</tbody>
	</table>
	<div class="box-footer clearfix">
		<div class='paginationContent'></div>
	</div>
</div>
<?php } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'shipping_methods';?>';
	var total_pages = <?= $model->pagination->total_pages;?>;
	var page = <?= $model->pagination->current_page_index;?>;
</script>

