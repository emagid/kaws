<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->color->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Slug</label>
                    <?php echo $model->form->editorFor("slug"); ?>
                </div>
                <div class="form-group">
                    <label>Swatch</label>

                    <!--                <p>-->
                    <!--                    <small style="color:#A81927"><b>(ideal featured image dimensions are 800 x 800, zoomed out with white space surrounding product)</b></small>-->
                    <!--                </p>-->
                    <p><input type="file" name="swatch" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->color->swatch != "" && file_exists(UPLOAD_PATH . 'colors' . DS . $model->color->swatch)) {
                            $img_path = UPLOAD_URL . 'colors/' . $model->color->swatch; ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'colors/delete_image/' . $model->color->id; ?>?swatch=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="swatch"
                                       value="<?= $model->color->swatch ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Made To Order</label>
                    <?php echo $model->form->checkboxFor('made_to_order',1)?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>