<div class="row">
    <div class="col-md-16">
        <div class="box search-box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id, Name or Slug"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
</div>
<?php if (count($model->colors) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="20%">Slug</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->colors as $obj) { ?>
                <tr class="originalColors">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>colors/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td><?php echo $obj->slug; ?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>colors/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black"
                           href="<?php echo ADMIN_URL; ?>colors/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'colors';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>colors/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalColors').remove();
                    $('.paginationContent').hide();
                    $('.originalColors').hide();

                    var list = JSON.parse(data);

                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>colors/update/' + list[key].id).html(list[key].name));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//colors/update/' + list[key].id).html(list[key].alias));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>colors/update/' + list[key].id).html(list[key].slug));

                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>colors/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>colors/delete/' + list[key].id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalColors').remove();
                $('.paginationContent').show();
                $('.originalColors').show();
            }
        });
    })
</script>

