<?php if (count($model->showrooms) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="10%">Name</th>
                <th width="10%">Email</th>
                <th width="10%">DateTime</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>

            </thead>
            <tbody>
            <?php foreach ($model->showrooms as $obj) { ?>
                <tr class="originalNews">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>showrooms/update/<?php echo $obj->id; ?>"><?php echo $obj->first_name . ' ' . $obj->last_name; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>showrooms/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>showrooms/update/<?php echo $obj->id; ?>"><?php echo $obj->datetime; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>showrooms/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>showrooms/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'showrooms';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

