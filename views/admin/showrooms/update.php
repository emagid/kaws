<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
<input name="id" type="hidden" value="<?=$model->showroom->id?>"/>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>First Name</label>
                    <?=$model->form->editorFor('first_name')?>
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <?=$model->form->editorFor('last_name')?>
                </div>
                <div class="form-group">
                    <label>Email Name</label>
                    <?=$model->form->editorFor('email')?>
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <?=$model->form->editorFor('phone')?>
                </div>
                <div class="form-group">
                    <label>Interest</label>
                    <?=$model->form->textAreaFor('interest',['rows'=>'5'])?>
                </div>
                <div class="form-group">
                    <label>DateTime</label>
                    <div><?=$model->showroom->datetime?></div>
                    <input type="hidden" name="datetime" value="<?=$model->showroom->datetime?>">
                    <!--                    --><?//=$model->form->editorFor('phone')?>
                </div>
            </div>
        </div>

    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 