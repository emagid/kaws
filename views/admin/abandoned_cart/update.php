<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->abandoned_cart->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>Id</label>
                <div><?=$model->abandoned_cart->id?></div>
            </div>
            <div class="form-group">
                <label>Last Updated</label>
                <div><?=date('F d, Y h:i:s a',strtotime($model->abandoned_cart->update_time))?></div>
            </div>
            <div class="form-group">
                <label>User</label>
                <div><?=$model->abandoned_cart->getUsername()?></div>
            </div>
            <div class="form-group">
                <label>Cart Items</label>
                <?foreach($model->abandoned_cart->getCartProducts() as $cartProduct){
                    $product = \Model\Product::getItem($cartProduct->product_id)?>
                    <div><b><?=$product->name?></b></div>
                    <?foreach(json_decode($cartProduct->variation,true)?:[] as $key=>$value){
                        $strModel = "\Model\\".ucfirst($key);
                        if($value){
                            $sql = "SELECT * FROM {$key} WHERE id = {$value}"?>
                            <div><b><?=ucfirst($key)?></b> : <?=($item = $strModel::getItem($value)) ? $item->name(): $strModel::getItem(null,['sql'=>$sql])->name()?></div>
                        <?}?>
                    <?}?>
                    <img src="<?=UPLOAD_URL.'products/'.$product->featuredImage()?>" style="width: 100px;"/>
                <?}?>
            </div>
            <div class="form-group">
                <label>Checkout Fields</label>
                <?foreach(json_decode($model->abandoned_cart->checkout_fields,true) as $key=>$value){
                    $presentable = ucwords(str_replace('_',' ',$key));
                    if(strtolower($key) == 'cc_number'){
                        $value = str_repeat('*', strlen($value) - 12) . substr($value, -4);
                    }
                    if($value){?>
                        <div><b><?=$presentable?></b> : <?=$value?></div>
                    <?}?>
                <?}?>
            </div>
        </div>
    </div>
  </div>
</form>
		 

<?php footer();?>
<script type="text/javascript">
  var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
$(document).ready(function() {
    $("input[name='name']").on('keyup', function (e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g, '-');
        $("input[name='slug']").val(val.toLowerCase());
    });
});
</script>