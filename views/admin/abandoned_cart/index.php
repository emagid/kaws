<?php if (count($model->abandoned_carts) > 0) { ?>

    <div class="row">
        <div class="col-md-2">
            <div class="box">
                <div class="form-group">
                    <a href="<?= ADMIN_URL ?>abandoned_cart/export" class="btn btn-warning">Export</a>
                </div>
            </div>
        </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    </div>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="2%">Id</th>
                <th width="10%">Time updated</th>
                <th width="10%">User</th>
                <th width="20%">Email</th>
                <th width="20%">Cart Items</th>
                <th width="2%" class="text-center">Edit</th>
                <th width="2%" class="text-center">Delete</th>
                <th width="2%" class="text-center">Send Email</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->abandoned_carts as $obj) { ?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>abandoned_cart/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>abandoned_cart/update/<?php echo $obj->id; ?>"><?php echo date('F d, Y h:i:s a',strtotime($obj->update_time)); ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>abandoned_cart/update/<?php echo $obj->id; ?>"><?php echo $obj->getUsername() ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>abandoned_cart/update/<?php echo $obj->id; ?>"><?php echo $obj->getEmail() ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>abandoned_cart/update/<?php echo $obj->id; ?>"><?php echo implode('<br/>',$obj->getProductNames()) ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>abandoned_cart/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>abandoned_cart/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <?$buildQueryString = isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING']: ''?>
                        <?if($obj->send_email == 0){?>
                            <a class="btn-actions" onClick="return confirm('Send email to <?=$obj->getEmail()?>');" href="<?php echo ADMIN_URL; ?>abandoned_cart/sendEmail/<?php echo $obj->id.$buildQueryString ?>">
                                <i class="icon-mail"></i>
                            </a>
                        <?} else if($obj->send_email == 1) {?>
                            <a class="btn-actions" onClick="return confirm('Resend email to <?=$obj->getEmail()?>');" href="<?php echo ADMIN_URL; ?>abandoned_cart/sendEmail/<?php echo $obj->id.$buildQueryString ?>">
                                <i class="icon-ok-circled"></i>
                            </a>
                        <?}?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'abandoned_cart';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

