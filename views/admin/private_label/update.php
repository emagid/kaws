<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->private_label->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>Title</label>
                <?php echo $model->form->editorFor("title"); ?>
            </div>
            <div class="form-group">
                <label>Subtitle</label>
                <?php echo $model->form->textAreaFor("subtitle", ["class" => "ckeditor"]); ?>
            </div>
            <div class="form-group">
                <label>Display Order</label>
                <select name="display_order">
                    <?foreach($model->displays as $display_order=>$text){?>
                        <option value="<?=$display_order?>" <?=$display_order == $model->private_label->display_order ? 'selected': ''?>><?=$text?></option>
                    <?}?>
                </select>
            </div>
            <div class="form-group" id="image">
                <label>Image</label>
                <p><small>(ideal image size is 1920 x 300)</small></p>
                <?php
                $img_path = "";
                if($model->private_label->featured_image != ""){
                    $img_path = UPLOAD_URL . 'private_labels/' . $model->private_label->featured_image;
                }
                ?>
                <p><input type="file" name="featured_image" class='image' /></p>
                <?php if($model->private_label->featured_image != ""){ ?>
                    <div class="well well-sm pull-left">
                        <img src="<?php echo $img_path; ?>" width="100" />
                        <br />
                        <a href="<?= ADMIN_URL.'private_label/delete_image/'.$model->private_label->id.'/?featured_image=1';?>" class="btn btn-default btn-xs">Delete</a>
                        <input type="hidden" name="featured_image"
                               value="<?= $model->private_label->featured_image ?>"/></div>
                <?php } ?>
                <div id='preview-container'></div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <label>Section</label>
                <select class="form-control" name="type">
                    <?foreach(\Model\Private_Label::$type as $key=>$value){?>
                        <option value="<?=$key?>" <?=$model->private_label->type == $key ? 'selected': ''?>><?=$value?></option>
                    <?}?>
                </select>
<!--                --><?php //echo $model->form->dropDownListFor("type",[1=>'Intro to Private Label', 2=>'Case Studies', 3=>'Services', 4=>'Additional Capabilities'],'',['class'=>"form-control"]); ?>
            </div>
            <div class="form-group" id="case_study" style="display: none;">
                <label>Case Study</label>
                <input type="text" name="case_study" value="<?=$model->private_label->case_study?>" disabled/>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer();?>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS?>jquery.datetimepicker.css">
<script src="<?=ADMIN_JS?>plugins/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'private_label/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('#product_select').on('change',function(){
            $('#url').val("<?=SITE_URL?>products/"+$(this).find(':selected').attr('data-slug'));
        });
        $('#display_date').datetimepicker({
            format: 'Y-m-d',
            timepicker:false
        });
        $('select[name=type]').on('change',function(){
            var self = $(this);
            $.getJSON('/admin/private_label/uDO',{type:$(this).val(), id:<?=$model->private_label->id?>},function(data){
                if(data.status == 'success'){
                    $('select[name=display_order]').html(data.appendHtml);
                }
            });
            if($(this).val() == 2){
                $('#case_study').show();
                $('input[name=case_study]').prop('disabled',false);
            } else {
                $('#case_study').hide();
                $('input[name=case_study]').prop('disabled',true);
            }
            if($(this).val() == 4){
                $('#image').hide();
            } else {
                $('#image').show();
            }
        }).trigger("change");
    });
</script>