<div class='bg-danger'>
	<?php if(isset($model->errors) && count($model->errors)>0) {
		echo implode("<br>",$model->errors);
	} ?>
</div>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->banner->id;?>" />
  <input type="hidden" name="banner_type" value="5" />
	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<h4>Featured</h4>
				<div class="form-group">
					<label>Button Title</label>
					<input type="text" value="<?=$model->banner->title?>" name="title" id="button_title"/>
				</div>
				<div class="form-group">
					<label>URL</label>
					<input type="text" value="<?=$model->banner->url?>" name="url"/>
				</div>
				<div class="form-group">
					<label>Text Color</label>
					<?php echo $model->form->dropDownListFor("text_color",\Model\Banner::$text_color,'',['class'=>'form-control']); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<div class="form-group">
					<h4>Image</h4>
					<? 	
						$size = "370 x ";
						if ($model->banner->featured_id == 1 || $model->banner->featured_id == 4 || $model->banner->featured_id == 5){
							$size .= "310";
						} else if ($model->banner->featured_id == 2 || $model->banner->featured_id == 3 || $model->banner->featured_id == 6){
							$size .= "220";
						}
						$height = explode(' ', $size);
						$height = array_pop($height);
					?>
					<p><small>(ideal "Featured #<?=$model->banner->featured_id?>" image size is <?=$size?>)</small></p>
					<?php 
						$img_path = '';
						if (!is_null($model->banner->image) && file_exists(UPLOAD_PATH.'banners/'.$model->banner->image)){
							$img_path = UPLOAD_URL.'banners/'.$model->banner->image;
						}
					?>
					<p><input type="file" name="image" class='image' /></p>
					<?php if($img_path != ''){ ?>
					<div class="well well-sm pull-left">
						<div id='image-preview'>
							<img src="<?php echo $img_path; ?>" width="370" height="<?=$height?>" />
							<br />
							<a href=<?= ADMIN_URL.'banners/featured_delete_image/'.$model->banner->featured_id?> class="btn btn-default btn-xs">Delete</a>
							<input type="hidden" name="image" value="<?=$model->banner->image?>" />
						</div>
					</div>
					<?php } ?>
					<div id='preview-container'></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>

<?= footer(); ?>

<script type='text/javascript'>
$(document).ready(function() {
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				var img = $("<img />");
				img.attr('src',e.target.result);
				img.attr('alt','Uploaded Image');
				img.attr("width",'370');
//				img.attr('height','<?//=$height?>//');
				console.log(img);
				$("#preview-container").html(img);
			};

			$(input).parent().parent().find('input[type="hidden"]').remove();
			
			reader.readAsDataURL(input.files[0]);
		}
	}

	$("input.image").change(function(){
		readURL(this);
		$('#previewupload').show();
	});
});
</script>