<div class='bg-danger'>
	<?php if(isset($model->errors) && count($model->errors)>0) {
		echo implode("<br>",$model->errors);
	} ?>
</div>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->banner->id;?>" />
  <input type="hidden" name="banner_type" value="2" />
	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<h4>Featured</h4>
				<div class="form-group">
					<label>Title</label>
					<input type="text" value="<?=$model->banner->title?>" name="title"/>
				</div>
				<div class="form-group">
					<label>Description</label>
					<input type="text" value="<?=$model->banner->description?>" name="description"/>
				</div>
				<div class="form-group">
					<label>URL</label>
					<input type="text" value="<?=$model->banner->url?>" name="url"/>
				</div>
				<div class="form-group">
					<label>Button</label>
					<input type="text" value="<?=$model->banner->tagline?>" name="tagline"/>
				</div>

				<!--				<div class="form-group">-->
<!--					<label>Text Color</label>-->
<!--					--><?php //echo $model->form->dropDownListFor("text_color",\Model\Banner::$text_color,'',['class'=>'form-control']); ?>
<!--				</div>-->
<!--				<div class="form-group">-->
<!--					<label>Category Selector</label>-->
<!--					<select name="category_id" class="form-control">-->
<!--						--><?//foreach($model->categories as $category){
//							$cat = '';
//							if($model->banner->options != ''){
//								$cat = json_decode($model->banner->options,true)['category'];
//							}?>
<!--							<option value="--><?//=$category->id?><!--" --><?//=$category->id == $cat ? 'selected': ''?><!--><?//=$category->name?><!--</option>-->
<!--						--><?//}?>
<!--					</select>-->
<!--				</div>-->
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<div class="form-group">
					<h4>Image</h4>
					<p><small>(ideal image size is 1200 x 540 )</small></p>
					<?php 
						$img_path = '';
						if (!is_null($model->banner->image) && file_exists(UPLOAD_PATH.'banners/'.$model->banner->image)){
							$img_path = UPLOAD_URL.'banners/'.$model->banner->image;
						}
					?>
					<p><input type="file" name="image" class='image' /></p>
					<?php if($img_path != ''){ ?>
					<div class="well well-sm pull-left">
						<div id='image-preview'>
							<img src="<?php echo $img_path; ?>" width="370" height="<?=$height?>" />
							<br />
							<a href=<?= ADMIN_URL.'banners/featured_delete_image/'.$model->banner->featured_id?> class="btn btn-default btn-xs">Delete</a>
							<input type="hidden" name="image" value="<?=$model->banner->image?>" />
						</div>
					</div>
					<?php } ?>
					<div id='preview-container'></div>
					<div class="clearfix"></div>
                    <div class="form-group">
                        <label>Image Alternative Description</label>
                        <?php echo $model->form->editorFor("image_alt"); ?>
                    </div>
				</div>
			</div>
		</div>
		<div class="col-md-24">
			<button type="submit" class="btn-save">Save</button>
		</div>
	</div>
</form>

<?= footer(); ?>

<script type='text/javascript'>
$(document).ready(function() {
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				var img = $("<img />");
				img.attr('src',e.target.result);
				img.attr('alt','Uploaded Image');
				img.attr("width",'370');
//				img.attr('height','<?//=$height?>//');
				console.log(img);
				$("#preview-container").html(img);
			};

			$(input).parent().parent().find('input[type="hidden"]').remove();
			
			reader.readAsDataURL(input.files[0]);
		}
	}

	$("input.image").change(function(){
		readURL(this);
		$('#previewupload').show();
	});
	$('#add_desc').on('click', function(){
		var content = $(
				'<div class="form-group"><input data-title-id="" type="text" name="button_title[]" placeholder="Button Title" value=""><input data-desc-id="" name="button_url[]" placeholder="Button URL" type="text"/><input type="number" name="content_order[]" value="" placeholder="Enter display order"><input type="file" name="image[]" class="image" /><br><a href="#" class="delete_content">Delete</a></div>'
		);
		$('#content_box').append(content);
	});
	$(document).on('click','.delete_content', function(){
		$(this).parent().remove();
	});
});
</script>