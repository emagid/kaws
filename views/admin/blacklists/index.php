<div class="row">
<!--    <div class="col-md-16">-->
    <div class="col-md-24">
        <div class="box search-box">
            <div class="form-group">
                <label>Search</label>
                <div class="input-group">
                    <input id="search" type="text" name="search" class="form-control" placeholder="Search" value="<?=isset($_GET['search'])?$_GET['search']:''?>"/>
                    <span class="input-group-addon">
                        <i class="icon-search"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!--<div class="col-md-8">
        <div class="box search-box">
            <div class="form-group">
                <label>Search</label>
                <div class="input-group">
                    <select class="form-control" id="blacklist_type">
                        <?/* foreach (\Model\Blacklist::$field_types as $a => $t) {*/?>
                            <?/* if($a == 4 || $a == 5) continue; */?>
                            <option value="<?/*=$a*/?>"><?/*= \Model\Blacklist::display_types()[$a]*/?></option>
                        <?/* } */?>
                    </select>
                </div>
            </div>
        </div>
    </div>-->
</div>
<?php if (count($model->blacklists)>0) { ?>
<div class="box box-table">
<table class="table" id="data-list">
  <thead>
    <tr>
      <th>Field</th>
      <th>Value</th>
      <th>Edit</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($model->blacklists as $obj) { ?>
        <? if($obj->field == 5 && $obj->blacklist_id != '') continue; ?>
    <tr class="oBl">
      <td>
      	 <a href="<?php echo ADMIN_URL; ?>blacklists/update/<?php echo $obj->id; ?>">
           <?php echo \Model\Blacklist::display_types()[$obj->field];?>
         </a>
      </td>
      <td>
      	<a href="<?php echo ADMIN_URL; ?>blacklists/update/<?php echo $obj->id; ?>">
            <? if(($parsedVal = json_decode($obj->value,true)) && $obj->field == 5){ ?>
                <?
                    foreach ($parsedVal as $field => $val){
                        echo ucfirst(str_replace('_','',$field)).":".$val."<br />";
                    }
                ?>
            <? } else { ?>
                <span class="<?=$obj->field == 4?'google_address':''?>" data-val="<?php echo $obj->value;?>">
                    <?php echo $obj->preview?:$obj->value;?>
                </span>
            <? } ?>
        </a>
      </td>
      <td>
         <a class="btn-actions" href="<?php echo ADMIN_URL; ?>blacklists/update/<?php echo $obj->id; ?>">
           <i class="icon-pencil"></i> 
         </a>
      </td>
      <td class="text-center">
          <a class="btn-actions"
             href="<?php echo ADMIN_URL; ?>blacklists/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
             onClick="return confirm('Are You Sure?');">
              <i class="icon-cancel-circled"></i>
          </a>
      </td>
    </tr>
    <?php } ?>
  </tbody>
  
</table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php } ?>

<?php footer();?>

<script>
	var site_url = '<?= ADMIN_URL.'blacklists';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.google_address').each(function(){
            var place_id = $(this).data('val');
            var dispAddress = $(this);
            $.post('<?=ADMIN_URL?>blacklists/geocodeLookup',{place_id:place_id},function(data){
                var results = data.results;
                var status = data.gStatus;
                if(status === 'OK'){
                    if(results[0]) {
                        $(dispAddress).text(results[0].formatted_address);
                    } else {
                        $(dispAddress).text('Google Could not find this address');
                    }
                } else {
                    $(dispAddress).text('There was an error displaying this address: '+ status);
                }
            });
        });

        var timeout;
        var search = $('#search');

        search.keyup(function(){
            clearTimeout(timeout);
            timeout = setTimeout(execute,250);
        });
//        search.keydown(function(){
//            clearTimeout(timeout);
//        });

        function execute () {
            var keywords = search.val();
            var url = "<?php echo ADMIN_URL; ?>blacklists/search";
            if (keywords.length > 1) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.oBl').remove();
                    $('.paginationContent').hide();
                    $('.oBl').hide();

                    var list = JSON.parse(data);

                    for (key in list) {
                        var tr = $('<tr />');

                        var types = [
                            'Email Address',
                            'Credit Card Number',
                            'Ip Address',
                            'Mailing  Address',
                            'Mailing Address',
                            'Phone Number'
                        ];

                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>blacklists/update/' + list[key].id).html(types[list[key].type-1]));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>blacklists/update/' + list[key].id).html(list[key].value));
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>blacklists/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>blacklists/delete/' + list[key].id);
                       deleteLink.click(function () {
                           return confirm('Are You Sure?');
                       });
                       var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');


                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.oBl').remove();
                $('.paginationContent').show();
                $('.oBl').show();
            }
        }
    });
</script>