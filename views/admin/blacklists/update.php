<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->blacklist->id;?>" />
  <input type=hidden name="blacklist_id" value="<?php echo $model->blacklist->blacklist_id?:0; ?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
        <div class="box">
            <div class="row">
                <h4>Blacklist</h4>
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Field</label>
                        <?php echo $model->form->dropDownListFor('field',\Model\Blacklist::display_types(),'',['class'=>'form-control']); ?>
                    </div>
                </div>
                <div class="col-md-10 address-group" style="display: none">
                    <div class="form-group">
                        <label>Search Google For Address </label>
                        <input type="text" id="address" placeholder="Enter the address to block"/>
                    </div>
                </div>
                <div class="col-md-10 value-group" >
                    <div class="form-group">
                        <label>Value</label>
                        <?php echo $model->form->editorFor("value"); ?>
                    </div>
                </div>
            </div>
            <div class="address-group row" style="display: none">
                <h2 class="address-group" style="display: none">Enter Manually</h2>
                <div class="col-md-10 address-group" style="display: none">
                    <div class="form-group">
                        <label>Address Line 1</label>
                        <input type="text" id="_address" <?=isset($model->address['address'])?"value='{$model->address['address']}'":''?> name="value2[address]"/>
                    </div>
                </div>
                <div class="col-md-10 address-group" style="display: none">
                    <div class="form-group">
                        <label>Address Line 2</label>
                        <input type="text" name="value2[address2]" <?=isset($model->address['address2'])?"value='{$model->address['address2']}'":''?> />
                    </div>
                </div>
                <div class="col-md-10 address-group" style="display: none">
                    <div class="form-group">
                        <label>Country </label>
                        <input type="text" id="_city" <?=isset($model->address['city'])?"value='{$model->address['city']}'":''?> name="value2[city]"/>
                    </div>
                </div>
                <div class="col-md-10 address-group" style="display: none">
                    <div class="form-group">
                        <label>City </label>
                        <input type="text" id="_state" <?=isset($model->address['state'])?"value='{$model->address['state']}'":''?> name="value2[state]"/>
                    </div>
                </div>
                <div class="col-md-10 address-group" style="display: none">
                    <div class="form-group">
                        <label>State/Region </label>
                        <input type="text" id="_country" <?=isset($model->address['country'])?"value='{$model->address['country']}'":''?> name="value2[country]"/>
                    </div>
                </div>
                <div class="col-md-10 address-group" style="display: none">
                    <div class="form-group">
                        <label>Postal Code</label>
                        <input type="text" id="_zip" <?=isset($model->address['zip'])?"value='{$model->address['zip']}'":''?> name="value2[zip]"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>
<?footer()?>

<script type="text/javascript">
    if(<?=$model->blacklist->field != 5?'true':'false'?>){
        $('option[value=4]').hide().prop('disabled',true).change();
    } else {
        $('option[value=5]').hide().prop('disabled',true).change();
    }

    $('[name=field').change(function(){
        if(this.value == 4 || this.value == 5){
            $('.address-group').show();
            $('.value-group').hide();
        } else {
            $('.address-group').hide();
            $('.value-group').show();
        }
    }).change();
    $('#_address,#_city,#_country,#_state,#_zip').keyup(function(){
        $('[name=value]').val('');
    });

    <? if($model->blacklist->field == 4) {?>
        $.post('<?=ADMIN_URL?>blacklists/geocodeLookup',{place_id:'<?=$model->blacklist->value?>'},function(data){
            var results = data.results;
            var status = data.gStatus;
            if(status === 'OK'){
                if(results[0]) {
                    $('input#address').val(results[0].formatted_address);
                } else {
                    $('input#address').val('Google Could not find this address');
                }
            } else {
                $('input#address').val('There was an error displaying this address: '+ status);
            }
        });
    <? } ?>
    function initMap() {
        var input = $('input#address')[0];
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed',function () {
            var place = autocomplete.getPlace();
            activate(place);
        });
    }
    function activate(result){
        console.log(result);
        activateSuggestions = false;
        var addr1 ='';
        var street_num = '';
        var route = '';
        var city =[];
        var ctry ='';
        var zip ='';
        var state ='';
        var place_id = result.place_id;
        result.address_components.forEach(function(component){
            if(component.types.includes('street_number')){
                street_num = component.long_name;
            }
            if(component.types.includes('route')){
                route = component.long_name;
            }
            if(component.types.includes('postal_code')){
                zip = component.long_name;
            }
            if(component.types.includes('country')){
                ctry = component.long_name;
            }
            if(component.types.includes('locality') || component.types.includes('sublocality')){
                city = component.long_name;
            }
            if(component.types.includes('administrative_area_level_1')){
                state = [component.long_name,component.short_name];
            }
        });
        addr1 = street_num+' '+route;
        $('#_address').val(addr1);
        $('#_city').val(city);
        $('#_country').val(ctry);
        $('#_state').val(state[1]);
        $('#_zip').val(zip);
        $('[name=value]').val(result.place_id);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTc_xONeYp-mEbhLrISuX28EBeZdIJwyI&libraries=places&callback=initMap" async defer />
