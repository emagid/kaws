<?php if (count($model->inventory_statuss) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='20%'>Name</th>
                <th width='20%'>Count Towards Stock?</th>
                <th width='20%'>Display Order</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->inventory_statuss as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>inventory_status/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
                    <td>
                        <? if($obj->sold_status){?>
                            <button type="button" class="btn btn-danger" disabled="disabled">Sold</button>
                        <?} else if(!$obj->count_to_total) {?>
                            <button type="button" class="btn btn-danger" disabled="disabled">Not counted</button>
                        <?}?>
                    </td>
                    <td><a href="<?php echo ADMIN_URL; ?>inventory_status/update/<?php echo $obj->id; ?>"><?php echo $obj->display_order; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>inventory_status/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>inventory_status/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'inventory_status';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>