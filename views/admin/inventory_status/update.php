<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->inventory_status->id; ?>"/>
    <input type="hidden" name="redirectTo" value="<?php echo '/admin/inventory_status'; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>When a project is sold, default to this status</label>
                    <?php echo $model->form->checkBoxFor("sold_status",1);?>
                </div>
                <div class="form-group">
                    <label>Items with this status should be considered part of inventory</label>
                    <?php echo $model->form->checkBoxFor("count_to_total",1);?>
                </div>
                <div class="form-group">
                    <label>Display Order</label>
                    <?php echo $model->form->editorFor("display_order"); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'inventory_status/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>