<form id="create-order" class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <div role="tabpanel">
        <div class="tab-content">
            <div role="tabpanel">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            <h4>Email</h4>
                            <div class="form-group">
                                <label>Select Email Type</label>
                                <select name="email_type" class="form-control">
                                    <?foreach(\Model\Order::$mass_email as $type=>$text){?>
                                        <option value="<?=$type?>"><?=$type?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group email_text"></div>
                        </div>
                    </div>
                    <div class="col-md-16">
                        <div class="box">
                            <h4>Orders</h4>
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="1%">Id</th>
                                    <th width="3%">Insert Time</th>
                                    <th width="5%">Tracking #</th>
                                    <th width="5%">Status</th>
                                    <th width="5%">Bill Name</th>
                                    <th width="10%">Products</th>
                                    <th width="1%">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($model->orders as $obj) {
                                    if ($obj->getInvoice()) {
                                        $style = 'style="background:#C6DDFF"';
                                    } elseif ($obj->in_store) {
                                        $style = 'style="background:#ffff9e"';
                                    } else {
                                        $style = '';
                                    } ?>
                                    <tr class="originalProducts" <?= $style ?>>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <?php echo $obj->id; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <?php echo $obj->insert_time; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <?php echo $obj->tracking_number; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <? $color = 'black';
                                            if (in_array($obj->status, ['Paid', 'Shipped', 'Complete', 'Delivered'])) {
                                                $color = 'green';
                                            } else if (in_array($obj->status, ['Declined', 'Canceled', 'Incomplete PayPal'])) {
                                                $color = 'red';
                                            } ?>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>"
                                               style="color: <?= $color ?>;">
                                                <?= $obj->status ?><? if ($obj->payment_status) {
                                                    echo "<br/>($obj->payment_status)";
                                                } ?>
                                            </a>
                                        </td>

                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                                    $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);


                                                $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                                    $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);


                                                if (count(array_diff($shp, $blng)) == 0) {
                                                    ?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                                                <? } else {
                                                    ?>
                                                    <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                                            disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                                                <? } ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <? if (($orderProducts = $obj->getOrderProducts())) {
                                                    foreach ($orderProducts as $orderProduct) {
                                                        if ($orderProduct->product_id) {
                                                            if($orderProduct->clearance){
                                                                echo '<span style="color:green">'.\Model\Product::getItem($orderProduct->product_id)->name.' (Clearance)</span>';
                                                            } else {
                                                                echo \Model\Product::getItem($orderProduct->product_id)->name;
                                                            }
                                                        } else {
                                                            $json = json_decode($orderProduct->details, true);
                                                            echo $json['misc_name'] . '(Custom)';
                                                        }
                                                        echo '<br/>';
                                                    }
//                                echo implode('<br/>',array_map(function($item){return \Model\Product::getItem($item->product_id)->name;},$orderProducts));
                                                } ?>
                                            </a>
                                        </td>

                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                                                <? if ($obj->payment_method == 2) { ?>
                                                    <button type="button" class="btn btn-success" style="position:relative;"
                                                            disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                                                <? } else { ?>$<?php echo number_format($obj->total, 2);
                                                } ?>
                                            </a>
                                        </td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
    <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
    <? //=$model->order->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
    <button type="submit" class="btn btn-save">Send Email</button>
</form>
<?php footer(); ?>
<script>

    $(document).ready(function () {
        $('[name=email_type]').on('click',function(){
            var mass_email =
                <?$arr = [];
                foreach(\Model\Order::$mass_email as $k=>$i){
                    $arr[$k] = sprintf(\Model\Order::$mass_email_template,'customer',$i);
                }
                echo json_encode($arr);
                ?>;
            var self = $(this).val();
            if(self in mass_email){
                $('.email_text').html(mass_email[self]);
            }
        });
    });
</script>