<?$orderProducts = $model->order->getOrderProducts();?>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="order_id" value="<?= $model->order->id ?>">

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#items" aria-controls="item" role="tab" data-toggle="tab">Item Details</a></li>
            <?if($model->wholesale_payments){?>
                <li role="presentation"><a href="#payments" aria-controls="item" role="tab" data-toggle="tab">Payments</a></li>
            <?}?>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box">
                            <h4>Order Details</h4>
                            <div class="form-group">
                                <label>Id</label>
                                <div><?= $model->order->id ?></div>
                            </div>
                            <div class="form-group">
                                <label>Subtotal</label>
                                <div><?= $model->order->subtotal ?></div>
                            </div>
                            <div class="form-group">
                                <label>Shipping</label>
                                <div><?= number_format($model->order->shipping,2) ?></div>
                            </div>
                            <div class="form-group">
                                <label>Taxes</label>
                                <div><?= number_format($model->order->tax,2) ?></div>
                            </div>
                            <div class="form-group">
                                <label>Total</label>
                                <div><?= $model->order->total ?></div>
                            </div>
                            <h4>Customer Details</h4>
                            <div class="form-group">
                                <label>Email</label>
                                <div><?= $model->order->email ?></div>
                            </div>
                            <div class="form-group">
                                <label>First Name (Billing)</label>
                                <div><?= $model->order->bill_first_name ?></div>
                            </div>
                            <div class="form-group">
                                <label>Last Name (Billing)</label>
                                <div><?= $model->order->bill_last_name ?></div>
                            </div>
                            <h4>Payment Details</h4>
                            <div class="form-group">
                                <label>Type</label>
                                <div><?= \Model\Wholesale::$payment_type[$model->order->wholesale_payment_type] ?></div>
                            </div>
                            <div class="form-group">
                                <label>Last 4</label>
                                <div><?= substr($model->order->cc_number,-4) ?></div>
                            </div>
                            <div class="form-group">
                                <label>Expiry</label>
                                <div><?= $model->order->cc_expiration_month.'/'.$model->order->cc_expiration_year ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="box">
                            <h4>Products</h4>
                            <?$total = 0;
                            foreach($orderProducts as $orderProduct){
                                $product = \Model\Product::getItem($orderProduct->product_id);
                                $wholesale = \Model\Wholesale_Payment::getList(['where'=>"order_id = {$model->order->id}"]);
                                $wholesaleIds = implode(',',array_map(function($item){return $item->id;},$wholesale));
                                $productsAccountedFor = \Model\Wholesale_Payment_Items::getCount(['where'=>"wholesale_payment_id in ($wholesaleIds) and product_id = $product->id"]);
                                $max = $orderProduct->quantity - $productsAccountedFor;
                                $total += $orderProduct->unit_price * $max?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label><?=$product->name?></label>
                                            <input type="number" class="form-control quantity-input" data-prod_id="<?=$product->id?>" data-prod_price="<?=$orderProduct->unit_price?>" min="0" max="<?=$max?>" value="<?=$max?>">
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Unit Price</label>
                                            <div><?=number_format($orderProduct->unit_price,2)?></div>
                                        </div>
                                    </div>
                                </div>
                            <?}?>
                            <h4>Total</h4>
                            <div class="form-group">
                                <input class="form-control" id="total_id" readonly value="<?=number_format($total,2)?>"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="items">
                <div class="row">
                    <?foreach ($orderProducts as $orderProduct) {
                        $product = \Model\Product::getItem($orderProduct->product_id);
                        $wholesale = \Model\Wholesale_Payment::getList(['where'=>"order_id = {$model->order->id}"]);
                        $wholesaleIds = implode(',',array_map(function($item){return $item->id;},$wholesale));
                        $productsAccountedFor = \Model\Wholesale_Payment_Items::getCount(['where'=>"wholesale_payment_id in ($wholesaleIds) and product_id = $product->id"]);
                        $max = $orderProduct->quantity - $productsAccountedFor;?>
                        <div class="col-sm-8">
                            <div class="box">
                                <h4><?=$product->name?></h4>
                                <?for($i = 1; $i <= $max; $i++){?>
                                    <div class="form-group" data-id="<?=$product->id?>" data-order="<?=$i?>">
                                        <label>SKU (<?=$i?>)</label>
                                        <input name="product_sku[<?=$product->id?>][sku][]" type="text" class="form-control">
                                        <input name="product_sku[<?=$product->id?>][unit_price]" value="<?=$orderProduct->unit_price?>" type="hidden">
                                    </div>
                                <?}?>
                            </div>
                        </div>
                    <? }?>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="payments">
                <div class="row">
                    <div class="col-sm-24">
                        <div class="box">
                            <h4>Payments (<?=count($model->wholesale_payments)?>)</h4>
                            <?foreach($model->wholesale_payments as $payment){?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Id</label>
                                            <div><?= $payment->id ?></div>
                                        </div>
                                        <div class="form-group">
                                            <label>Total</label>
                                            <div><?= $payment->total ?></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <?foreach($payment->items as $item){?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Product</label>
                                                        <div><?= \Model\Product::getItem($item->product_id)->name ?></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Sku</label>
                                                        <div><?= $item->sku ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?}?>
                                    </div>
                                </div>
                                <hr>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save payment-btn">Create Payment</button>
</form>

<?php footer()?>
<script>
    $(document).ready(function(){
        var wto;
        $('.quantity-input').on('change',function(){
            var self = $(this);
            var id = self.data('prod_id');
            var max = self.attr('max');
            clearTimeout(wto);
            wto = setTimeout(function(){
                var item = $('[data-id="'+id+'"]');
                item.hide();
                item.find('input').prop('disabled',true);
                for(var i = 1; i <= self.val(); i++){
                    var it = $('[data-id="'+id+'"][data-order="'+i+'"]');
                    it.show();
                    it.find('input').prop('disabled',false);
                }
                var total = 0;
                $.each($('.quantity-input'),function(i,e){
                    e = $(e);
                    if(e.val() > e.attr('max')){
                        total += e.data('prod_price') * e.attr('max');
                    } else {
                        total += e.data('prod_price') * e.val();
                    }
                });
                $('#total_id').val(parseFloat(total).toFixed(2));
            },500);
        });

        $(window).keydown(function(e){
            if(e.keyCode == 13){ /** Prevent Enter Key submission */
                e.preventDefault();
                return false;
            }
        })
    });
</script>
