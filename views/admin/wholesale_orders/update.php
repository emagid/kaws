<style type="text/css">


    @media print {
        /* Стиль для печати */
        h1, h2, p {
            color: #000; /* Черный цвет текста */
        }

        .nav {
            display: none;
        }

        .btn {
            display: none;
        }

        .qqq {
            color: black;
        }

        #general-tab, #billing-info-tab, #shipping-info-tab, #products-tab {
            display: block;
            visibility: visible;
        }

        .form-group {
            margin-bottom: 3px;
        }

        a {
            border: 0;
            text-decoration: none;
        }

        input[type="text"] {
            border-color: white;
        }

        .form-control {
            border: 2px solid blue;
        }

        a img {
            border: 0
        }

        a:after {
            content: " (" attr(href) ") ";
            font-size: 90%;
        }

        a[href^="/"]:after {
            content: " ";
        }
    }
</style>

<? if ($model->order->payment_method == 1) { ?>
    <div class="row">
        <div class="col-sm-24">
            <div class="box">
                <? if ($model->order->wholesale_payment_type == 0) { ?>
                    <a href="<?= ADMIN_URL ?>wholesale_orders/payment/<?php echo $model->order->id; ?>" class="btn btn-save">PRE-AUTH PAYMENT</a>
                <? } ?>
                <a href="<?= ADMIN_URL ?>wholesale_orders/void/<?php echo $model->order->id; ?>" class="btn btn-warning">VOID PAYMENT</a>
            </div>
        </div>
    </div>
<? } ?>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->order->id; ?>"/>
    <input type="hidden" name="old_status" value="<?= $model->order->status ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <p class="qq" style=" text-align:center; color:#F9F9F9;">ORDER #<?php echo $model->order->id; ?></p>


        <div class="tab-content">
            <center class='center' style="    margin-bottom: 112px;">
                <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
                <button type="submit" id="save" class="btn btn-save">Save</button>
                <button class="btn btn-save" id="print">Print</button>
                <a href="/admin/orders/print_packing_slip/<?= $model->order->id ?>" class="btn btn-save">Print packing slip</a>
                <a class="btn btn-save btn-grey" id="delete">Delete</a>
            </center>

            <div role="tabpanel">
                <div class="row">
                    <div class="col-md-10">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>ID order</label>

                                <p>#<?= $model->order->id ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Transaction Id</label>

                                <p>#<?= $model->order->trans_id ?> </p>
                            </div>

                            <div class="form-group">
                                <label>Receipt Id</label>
                                <?php $links = explode(',', $model->order->payment_id);?>
                                <?php foreach($links as $link){ ?>
                                <a target="_blank" style="display: block; color: #2996cc;" href="https://squareup.com/receipt/preview/<?=$link?>">Receipt #<?=substr($link, 0, 4)?></a>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label>Date of order</label>

                                <p><?= date('F j Y, g:i a',strtotime($model->order->insert_time)) ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Fraud status</label><br>
                                <?= $model->order->orderFraudStatus ?>
                            </div>
                            <?if($model->order->error_message){?>
                                <div class="form-group">
                                    <label>Error</label><br>
                                    <?= $model->order->error_message ?>
                                </div>
                            <?}?>
                            <?if($model->order->payment_status){?>
                                <div class="form-group">
                                    <label>Payment Status</label><br>
                                    <?= $model->order->payment_status ?>
                                </div>
                            <?}?>
                            <div class="form-group">
                                <label>Wholesale Payment</label><br>
                                <?= \Model\Wholesale::$payment_type[$model->order->wholesale_payment_type] ?>
                            </div>
                            <div class="form-group">
                                <label>Order Confirmation URL</label><br>
                                <a href="<?= '/checkout/confirmation/'.$model->order->ref_num ?>" target="_blank">Click Here</a>
                            </div>
                            <?if($model->order->gift_card){?>
                                <div class="form-group">
                                    <label>Gift Code</label><br>
                                    <?= $model->order->gift_card?>
                                </div>
                            <?}?>
                            <div class="form-group">
                                <label>Status</label>
                                <? sort(\Model\Order::$status, SORT_LOCALE_STRING); ?>
                                <select class="change form-control" name="status">
                                    <?php $current_status = $model->order->status; ?>
                                    <? foreach (\Model\Order::$status as $key => $stat) { ?>
                                        <option <? if ($current_status == $stat) {
                                            echo "selected";
                                        } ?> value="<?= $stat ?>"><?= $stat ?></option>
                                    <? } ?>
                                </select>

                            </div>
                            <div class="form-group">
                                <label><font color="red">Mail Title</font></label>
                                <?php echo $model->form->textAreaFor("mailtitle",['id' => 'mailtitle']); ?>
                            </div>
                            <div class="form-group">
                                <label><font color="red">Subject </font></label>
                                <?php echo $model->form->textAreaFor("subject",['id' => 'subject']); ?>
                            </div>
                            <div class="form-group">
                                <label><font color="red">Mail </font></label>
                                <?php echo $model->form->textAreaFor("mail", ['id' => 'mail', 'rows' => '10']); ?>
                            </div>
                            <div class="form-group">
                                <label>Customer </label>

                                <div class="input-group">
                                    <? if (is_null($model->order->user)) { ?>
                                        <div class="input-group-addon">
                                            Guest
                                        </div>
                                        <input type="text" value="<?= $model->order->email ?>"/>
                                    <? } else { ?>
                                        <div class="input-group-btn">
                                            <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $model->order->user->id; ?>">
                                                <button type="button" class="btn"><i class="icon-eye"></i></button>
                                            </a>
                                        </div>
                                        <input type="text" value="<?= $model->order->user->email ?>"/>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input name="phone" id="phone-us" type="text" value="<?= $model->order->phone ?>">

                            </div>
                            <div class="form-group">
                                <label>Comment</label>
                                <?= $model->order->comment ?>
                            </div>
                            <div class="row">
                                <div class="col-xs-22">
                                    <div class="form-group">
                                        <label>Tracking Number</label>
                                        <?php echo $model->form->textBoxFor('tracking_number'); ?>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <label>Send Email</label>
                                        <input type="checkbox" name="tracking_email"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Shipping Method</label>
<!--                                --><?//= $model->form->dropDownListFor('shipping_method', [0 => 'Standard', 80 => 'International'], '', ['disabled' => 'disabled'], ['class' => 'form-control']); ?>
                                <?= $model->form->dropDownListFor('shipping_cost', [0 => 'Standard', 80 => 'International'], '',['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <label>Payment Method</label>
                                <?= $model->form->dropDownListFor('payment_method', \Model\Order::$payment_method, '',['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <label>In Store</label>
                                <?= $model->form->checkBoxFor('in_store',1); ?>
                            </div>
                            <div class="row cc-info"
                                 style="<?= ($model->order->payment_method != 1 && $model->order->payment_method != null) ? 'display:none;' : ''; ?>">
                                <div class="col-xs-10 cc_number">
                                    <label>Credit Card #</label>

                                    <div class="input-group">
                                        <?php echo $model->form->textBoxFor('cc_number', ['disabled' => 'disabled']); ?>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default">Show</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-10">
                                    <div class="col-xs-24">
                                        <label>Expiration</label>
                                    </div>
                                    <div>
                                        <div class="col-xs-16">
                                            <?= $model->form->dropDownListFor('cc_expiration_month', get_month(), '', ['disabled' => 'disabled', 'class'=>'form-control']); ?>
                                        </div>
                                        <div class="col-xs-8">
                                            <?= $model->form->dropDownListFor('cc_expiration_year', range(date('Y') - 15, date('Y') + 9), '', ['disabled' => 'disabled', 'class'=>'form-control']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <label>CCV</label>
                                    <?php echo $model->form->textBoxFor('cc_ccv', ['disabled' => 'disabled']); ?>
                                </div>
                            </div>
                            <? if (!is_null($model->order->coupon_code)) { ?>
                                <div class="form-group">
                                    <label>Coupon</label>

                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <a href="<?php echo ADMIN_URL; ?>coupons/update/<?php echo $model->coupon->id; ?>">
                                                <button type="button" class="btn"><i class="icon-eye"></i></button>
                                            </a>
                                        </div>
                                        <input type="text" disabled="disabled" value="<?= $model->coupon->code ?>"/>
                                    </div>
                                </div>
                            <? } ?>
                            <table class="table" style="margin-top: 10px;">
                                <tr>
                                    <td>Subtotal</td>
                                    <td id="sub">$<?php echo number_format($model->order->subtotal, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Tax</>
                                    <td id="tax">$<?php echo number_format($model->order->tax, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Shipping</td>
                                    <td id="shipping">$<?php echo number_format($model->order->shipping_cost, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Discount</td>
                                    <!--                                    <td>$--><?//= number_format($model->savings, 2); ?><!--</td>-->
                                    <td id="discount" style="color: red;">-$<?= number_format($model->order->coupon_amount, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Gift Card</td>
                                    <td id="discount" style="color: red;">-$<?= number_format($model->order->gift_card_amount, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td id="total">$<?= number_format($model->order->total, 2) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?
                    $shp = array($model->order->ship_first_name, $model->order->ship_last_name, $model->order->ship_address, $model->order->ship_address2,
                        $model->order->ship_country, $model->order->ship_city, $model->order->ship_state, $model->order->ship_zip);


                    $blng = array($model->order->bill_first_name, $model->order->bill_last_name, $model->order->bill_address, $model->order->bill_address2,
                        $model->order->bill_country, $model->order->bill_city, $model->order->bill_state, $model->order->bill_zip);
                    $isSame = count(array_diff($shp, $blng)) == 0;
                    ?>
                    <div class="col-md-<?=$isSame ? '12': '6'?>">
                        <div class="box">
                            <h4>Billing
                                <? if ($isSame) {
                                    echo " and Shipping <center><font color='green'>SAME</font></center>";
                                } ?>
                            </h4>

                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->textBoxFor('bill_first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->textBoxFor('bill_last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('bill_address'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('bill_address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('bill_city'); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
<!--                                --><?//= $model->form->dropDownListFor('bill_state', get_states(), 'Select', ['disabled' => 'disabled'], ['class' => 'form-control']); ?>
                                <?= $model->form->textBoxFor('bill_state'); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('bill_zip'); ?>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <?= $model->form->dropDownListFor('bill_country', get_countries(), '', ['class' => 'form-control']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6" <? if (count(array_diff($shp, $blng)) == 0) {
                        echo "style='display:none;'";
                    } else {
                    } ?>>
                        <div class="box">
                            <h4>Shipping</h4>

                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->textBoxFor('ship_first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->textBoxFor('ship_last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('ship_address'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('ship_address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('ship_city'); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
<!--                                --><?//= $model->form->dropDownListFor('ship_state', get_states(), 'Select', ['disabled' => 'disabled']); ?>
                                <?= $model->form->textBoxFor('ship_state'); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('ship_zip'); ?>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <?= $model->form->dropDownListFor('ship_country', get_countries(), '', ['class' => 'form-control']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Products<a href="/admin/orders/fulfill_items/<?=$model->order->id?>" style="float: right;">Fulfill Items</a></h4>
                            <table class="table">
                                <thead>
                                <th></th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Unit Price</th>
<!--                                <th>Color</th>-->
<!--                                <th>Size</th>-->
<!--                                <th>Progress</th>-->
                                <th></th>
<!--                                <th>View</th>-->
                                </thead>
                                <tbody>
                                <? foreach ($model->order_products as $order_product) { ?>
                                    <tr>
                                        <td><?php
                                            if(!is_null($order_product->details) && $order_product->details != ''){
                                                $productDetails = json_decode($order_product->details,true);
                                            } else {
                                                $productDetails = ['color'=>'','size'=>''];
                                            }
                                            $img_path = ADMIN_IMG . 'logo.png'; //TODO st-dev default product image
                                            $color = isset($productDetails['color']) && $productDetails['color'] ? $productDetails['color']: '';

                                            if($order_product->type == 'product'){
                                                $product = $order_product->product->id ? \Model\Product::getItem($order_product->product->id) : null;
                                                $featured_image = $product ? $product->featuredImage(null,$color) : '';
                                                if (!is_null($featured_image) && $featured_image != "") {
                                                    $img_path = UPLOAD_URL . 'products/' . $order_product->product->featuredImage(null,$color);
                                                }
                                            } else if($order_product->type == 'misc'){
                                                $img_path = $order_product->product->featured_image;
                                            } else if($order_product->type == 'gift card'){
                                                $img_path = UPLOAD_URL . 'gift_cards/' . $order_product->product->image;
                                            } else {
                                                $img_path = '';
                                            }
                                            if($order_product->clearance){
                                                $style = 'style="color: green"';
                                                $name = ' (Clearance)';
                                            } else {
                                                $style = $name = '';
                                            }
                                            ?>
                                            <div class="mouseover-thumbnail-holder">
                                                <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product_id; ?>">
                                                    <img src="<?php echo $img_path; ?>" width="50"/></a>
                                                <img class="large-thumbnail-style" src="<?php echo $img_path; ?>"/></a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= $order_product->product->id ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>" <?=$style?>><?= $order_product->product->name.$name ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= $order_product->quantity ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= number_format($order_product->unit_price, 2) ?></a>
                                        </td>
                                        <!--
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= isset($productDetails['color']) ? \Model\Color::getItem($productDetails['color'])->name : ''?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->product->id; ?>"><?= isset($productDetails['size']) ? \Model\Size::getItem($productDetails['size'])->us_size : ''?></a>
                                        </td>

                                        <td>
                                            <select class="work-in-progress form-control" data-id="<?=$order_product->id?>">
                                                <option value="0">No progress</option>
                                                <?$ar = [1=>'1. Materials',2=>'2. Stitched',3=>'3. Mold'];
                                                foreach($ar as $k=>$a){?>
                                                    <option value="<?=$k?>" <?=$k == $order_product->status ? 'selected': ''?>><?=$a?></option>
                                                <?}?>
                                            </select>
                                        </td>
                                         -->
                                        <td>
                                            <button type="button" class="submit-wip">Send Email</button>
                                        </td>
<!--                                        <td><a target="_blank" class="btn-actions"-->
<!--                                               href="/products/--><?php //echo $order_product->product->slug; ?><!--"><i-->
<!--                                                    class="icon-eye"></i></a></td>-->
                                    </tr>
                                <? } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12 notes_section">
                        <div class="box">
                            <h4>Notes</h4>

                            <div class="form-group">
                                <label>Note</label>
                                <?php echo $model->form->textAreaFor("note", ['rows' => '10']); ?>
                            </div>
                        </div>
                        <button type="submit" id="save" class="btn btn-save">Save</button>
                    </div>

                    <div class="col-md-12">
                        <div class="box">
                            <h4>Received Payment</h4>

                            <div class="form-group">
                                <select name="received_payment_method" class="form-control">
                                    <option value="0">No Select</option>
                                    <?php foreach(\Model\Order::$payment_method as $value => $method){ ?>
                                        <option value="<?=$value?>"><?=$method?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
    <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
    <? //=$model->order->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
</form>
<?php footer(); ?>

<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>

<script>


    $(function () {
        $('#delete').click(function () {
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                $.ajax({
                    'type': 'POST',
                    'url': '/admin/orders/unlock',
                    'data': {
                        pwd: pwd,
                        orderId: <?=$model->order->id?>
                    },
                    success: function (data) {
                        data = JSON.parse(data);

                        if (data == "false") {
                            alert('Incorrect code!');
                        } else {
                            window.location.replace(data);
                        }
                    }
                })
            }


        });

        $('select[name="payment_method"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.cc-info').show();
            } else {
                $('.cc-info').hide();
            }
        });
        $('.cc_number button').click(function () {
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                $.ajax({
                    'type': 'POST',
                    'url': '<?=ADMIN_URL?>orders/getCcNumber',
                    'data': 'password=' + pwd + '&order_id=<?=$model->order->id?>',
                    'success': function (data) {
                        data = JSON.parse(data);
                        if (data == "false") {
                            alert('Incorrect code.');
                        } else {
                            $('.cc_number input').val(data);
                        }
                    }
                })
            }
        });

        $('select[name="shipping_cost"]').on('change', function () {
            if ($(this).val() == 0) {
                $('#shipping').html('$0.00');
                var total = '$'+'<?=number_format($model->order->total-$model->order->shipping_cost,2)?>';
                $('#total').text(total)
            } else {
                $('#shipping').html('$80.00');
                var total = '$'+'<?=number_format($model->order->total-$model->order->shipping_cost + 80,2)?>';
                $('#total').text(total)
            }
        });

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("orders/");

        });

        $('#save').mouseover(function () {

            var id = $("input[name=id]").val();
            $("#act_for_click").val("orders/update/" + id);

        });

        $('.submit-wip').on('click',function(){
            var select = $(this).parent().siblings().find('.work-in-progress');
            if(select.val() != 0){
                var orderId = <?=$model->order->id?>;
                var orderProdId = select.data('id');
                var status = select.val();
                var data = {order_id:orderId,order_product_id:orderProdId,status:status};
                $.post('/admin/orders/send_progress',data,function(dat){
                    var json = $.parseJSON(dat);
                    console.log(json);
                    if(json.status == 'success'){
                        alert(json.message);
                    }
                });
            } else {
                alert('Select a level of progression');
            }
        });

    });
    $("#phone-us").inputmask("+1(999)999-9999");
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>


<script>

    $(document).ready(function () {

        $('input[name=tracking_number]').on('input',function(){
            if($(this).val() == '<?=$model->order->tracking_number?>' || $(this).val() == ''){
                $('input[name=tracking_email]').prop('checked',false);
            } else {
                $('input[name=tracking_email]').prop('checked',true);
            }
        });


        $('body').on('change', '.change', function () {
            var status = $(this).val();
            $.getJSON('/status_management/status', {status: status, orderId: '<?=$model->order->id?>'}, function(r){
                $("#mail").html(r.text);
                $("#subject").html(r.subject);
                $("#mailtitle").html(r.mailtitle);
            });
//            if (status == "Processed") {
//                $("#mail").html('<p>Hello  <?//=$model->order->ship_first_name?>//,<p>Thank you for your recent order.</p><p>This is to confirm that your orders is in stock and your credit card has now been processed.</p><p>You will receive an email with the tracking number as soon as it has shipped.</p><p>If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p><p>Thank you again for your business,</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>&ldquo;If it tells time, we sell it&rdquo;</p><p>(877) 752-6919</p>');
//            } else if (status == "Shipped") {
//                $("#mail").html('<p>Hello &nbsp;<?//=$model->order->ship_first_name?>//,</p><p>Your ModernVice order has shipped and the tracking number is _________.</p><p>If you have any questions please call or email us.</p><p>Thank you again for your business,</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
//            } else if (status == "Delivered") {
//                $("#mail").html('<p>Hello &nbsp;<?//=$model->order->ship_first_name?>//,</p><p>We have been informed by the manufacturer that the item you ordered from ModernVice has been discontinued.</p><p>And we checked with our other suppliers and they too do not have any in stock.</p><p>Would you like us to provide you with alternatives to that item&gt;&gt;SHOW ME OTHERS&lt;&lt; &nbsp;or would you prefer to just have your order canceled &gt;&gt;Cancel&lt;&lt;.?</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
//            } else if (status == "Discontinued") {
//                $("#mail").html('<p>Hello &nbsp;<?//=$model->order->ship_first_name?>//,</p><p>We have been informed by the manufacturer that the item you ordered from ModernVice has been discontinued.</p><p>And we checked with our other suppliers and they too do not have any in stock.</p><p>Would you like us to provide you with alternatives to that item&gt;&gt;SHOW ME OTHERS&lt;&lt; &nbsp;or would you prefer to just have your order canceled &gt;&gt;Cancel&lt;&lt;.?</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
//            } else if (status == "Billing_Shipping") {
//                $("#mail").html('<p>Hello &nbsp;(&lsquo;name&rsquo;),</p><p>In our efforts to protect all our customers we do credit card billing and shipping address verification on all orders.</p><p>When we contacted your credit card issuing bank to confirm the shipping address that you provided they were unable to do so.</p><p>Please just call the toll free number on the back of your credit card and ask them to add this as an &quot;Alternate Shipping Address&quot;.&nbsp;</p><p>Then just let us know that this has been done &gt;&gt;SHIPPING ADDRESS ADDED&lt;&lt; and we will be able to ship the watch to you at your requested address.</p><p>Thank you for your business,</p><p>&nbsp;</p><p>The Sales Team at ModernVice.com</p><p>(877) 752-6919</p><p>&ldquo;If it tells time, we sell it&rdquo;</p>');
//            } else if (status == "Canceled") {
//                $("#mail").html('Dear <b><?//=$model->order->ship_first_name?>//</b>, We have canceled your order as requested.<br> Thank you for giving us the opportunity to try to help you and we hope that you will come back to ModernVice.com in the future for any watch needs- Remember, "If it tells time we sell it".<br><br> Please note that your credit card was never charged for this purchase - only an authorization was received to confirm that it was not a fraudulent charge.<br><br> The Sales Team at ModernVice.com <br>(877) 752-6919<br> www.modernvice.com');
//            } else {
//            }

        });


    });
</script> 




























