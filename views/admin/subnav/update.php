<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->subnav->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Parent Category</label>
                                <select id="parent_cat" name="category_id" class="form-control" required>
                                    <option value="-1">None</option>
                                    <?php foreach ($model->categories as $category) {
                                        $select = ($category->id == $model->subnav->category_id) ? " selected='selected'" : "";
                                        ?>
                                        <option
                                            value="<?php echo $category->id; ?>" <?php echo $select; ?> ><?php echo $category->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Sub Category</label>
                                <select id="sub_cat" name="sub_category_id" class="form-control" required>
                                    <option value="-1">None</option>
                                    <?php foreach ($model->subcategories as $category) {
                                        $select = ($category->id == $model->subnav->sub_category_id) ? " selected='selected'" : "";
                                        $hidden = $category->parent_category != $model->subnav->category_id ? 'style="display:none"': '';
                                        ?>
                                        <option data-slug="<?=$category->slug?>" data-parent="<?=$category->parent_category?>" value="<?php echo $category->id; ?>" <?php echo $select; ?> <?php echo $hidden; ?> ><?php echo $category->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Column Set</label>
                                <select name="column_num" class="form-control" required>
                                    <?=$arr = [1,2,3]?>
                                    <?foreach($arr as $ar){?>
                                        <option value="<?=$ar?>" <?=$model->subnav->column_num == $ar ? 'selected': ''?>><?=$ar?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Display Order</label>
                                <input type="number" name="display_order" value="<?=$model->subnav->display_order?>" required>
                                <br/>(Set display order to 1 for image display)
                            </div>
                            <div class="form-group">
                                <label>URL</label>
                                <input type="text" name="url" value="<?=$model->subnav->url?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="form-group">
                                <h4>Image</h4>
                                <p>
                                    <small>(ideal image size is 1200 x 344)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->subnav->image != "") {
                                    $img_path = UPLOAD_URL . 'subnavs/' . $model->subnav->image;
                                }
                                ?>
                                <p><input type="file" name="image" class='image'/></p>
                                <?php if ($model->subnav->image != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <div id='image-preview'>
                                            <img src="<?php echo $img_path; ?>" width="600" height="172"/>
                                            <br/>
                                            <a href=<?= ADMIN_URL . 'subnav/delete_image/' . $model->subnav->id . '?image=1'; ?> class="btn
                                               btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="image" value="<?= $model->subnav->image ?>"/>
                                        </div>
                                    </div>

                                <?php } ?>
                                <div id='preview-container'></div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                <label>Image Alternative Description</label>
                                    <?php echo $model->form->editorFor("image_alt"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '600');
                    img.attr('height', '172');
                    $("#preview-container").html(img);
                };

                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('#parent_cat').on('change',function(){
            var val = $(this).val();
            $('#sub_cat').find('option').each(function(){
                if($(this).attr('data-parent') == val || $(this).val() == -1){
                    $(this).show();
                } else {
                    $(this).hide();
                }
                if($(this).val() == -1){
                    $(this).prop('selected',true);
                }
            });
        });
        $('#sub_cat').on('change',function(){
            $('input[name=url]').val($(this).find(':selected').attr('data-slug'));
        });
        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('mousewheel.disableScroll')
        });
    });
</script>