<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->shop_instagram->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>Title</label>
                <?php echo $model->form->editorFor("title"); ?>
            </div>
            <div class="form-group">
                <label>Feature Product</label>
                <select name="product_id" class="form-control" id="product_select">
                    <?foreach(\Model\Product::getList() as $product){?>
                        <option value="<?=$product->id?>" data-slug="<?=$product->slug?>" <?=$product->id == $model->shop_instagram->product_id ? 'selected': ''?>><?=$product->name?></option>
                    <?}?>
                </select>
            </div>
            <div class="form-group">
                <label>Image</label>
                <p><small>(ideal image size is 1920 x 300)</small></p>
                <?php
                $img_path = "";
                if($model->shop_instagram->featured_image != ""){
                    $img_path = UPLOAD_URL . 'shop_instagrams/' . $model->shop_instagram->featured_image;
                }
                ?>
                <p><input type="file" name="featured_image" class='image' /></p>
                <?php if($model->shop_instagram->featured_image != ""){ ?>
                    <div class="well well-sm pull-left">
                        <img src="<?php echo $img_path; ?>" width="100" />
                        <br />
                        <a href="<?= ADMIN_URL.'shop_instagrams/delete_image/'.$model->shop_instagram->id.'/?featured_image=1';?>" class="btn btn-default btn-xs">Delete</a>
                        <input type="hidden" name="featured_image"
                               value="<?= $model->shop_instagram->featured_image ?>"/></div>
                <?php } ?>
                <div id='preview-container'></div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <label>URL</label>
                <input name="url" type="text" id="url" value="<?=$model->shop_instagram->url?>"/>
            </div>
            <div class="form-group">
                <label>Display Date</label>
                <input name="display_date" type="text" id="display_date" value="<?=date('Y-m-d',strtotime($model->shop_instagram->display_date))?>"/>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer();?>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS?>jquery.datetimepicker.css">
<script src="<?=ADMIN_JS?>plugins/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'shop_instagram/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('#product_select').on('change',function(){
            $('#url').val("<?=SITE_URL?>products/"+$(this).find(':selected').attr('data-slug'));
        });
        $('#display_date').datetimepicker({
            format: 'Y-m-d',
            timepicker:false
        });
    });
</script>