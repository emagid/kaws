<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<?php
if (count($model->shop_instagrams) > 0) { ?>
    <div class="box">
        <h4>Press</h4>
        <ol class="sortable">
            <?foreach(\Model\Shop_Instagram::getList(['orderBy'=>'display_order,id desc']) as $item){?>
                <li data-id="<?=$item->id?>"><?=$item->title?></li>
            <?}?>
        </ol>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        var adjustment;
        var serial = $('.sortable').sortable({
            group:'sortable',
            onDrop: function ($item, container, _super) {
                var data = serial.sortable("serialize").get();
                var jsonData = JSON.stringify(data,null,'');

//                $('.serialize-result').text(jsonData);
                $.post('/admin/shop_instagram/sorter_post',{data:jsonData},function(){
                    console.log('done');
                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
    });
    var site_url = '<?= ADMIN_URL.'shop_instagram';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

