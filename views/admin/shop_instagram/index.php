<?php if (count($model->shop_instagrams) > 0) { ?>
    <div class="row">
        <div class="col-sm-1">
            <div class="box">
                <a href="<?=ADMIN_URL.'shop_instagram/sorter'?>">
                    <i class="icon-arrows-ccw"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="20%">Title</th>
                <th width="20%">URL</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->shop_instagrams as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>shop_instagram/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td><?php echo $obj->url; ?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>shop_instagram/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>shop_instagram/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'shop_instagram';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

