<style type="text/css">


    @media print {
        /* Стиль для печати */
        h1, h2, p {
            color: #000; /* Черный цвет текста */
        }

        .nav {
            display: none;
        }

        .btn {
            display: none;
        }

        .qqq {
            color: black;
        }

        #general-tab, #billing-info-tab, #shipping-info-tab, #products-tab {
            display: block;
            visibility: visible;
        }

        .form-group {
            margin-bottom: 3px;
        }

        a {
            border: 0;
            text-decoration: none;
        }

        input[type="text"] {
            border-color: white;
        }

        .form-control {
            border: 2px solid blue;
        }

        a img {
            border: 0
        }

        a:after {
            content: " (" attr(href) ") ";
            font-size: 90%;
        }

        a[href^="/"]:after {
            content: " ";
        }
    }
</style>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
<!--    <input type="hidden" name="id" value="--><?php //echo $model->ssOrder->id; ?><!--"/>-->
<!--    <input type="hidden" name="old_status" value="--><?//= $model->ssOrder->status ?><!--"/>-->
<!--    <input type=hidden name="token" value="--><?php //echo get_token(); ?><!--"/>-->

    <div role="tabpanel">
        <p class="qq" style=" text-align:center; color:#F9F9F9;">ORDER #<?php echo $model->ssOrder->orderId; ?></p>


        <div class="tab-content">
<!--            <center style="    margin-bottom: 112px;">-->
<!--                <button type="submit" id="save_close" class="btn btn-save">Save and close</button>-->
<!--                <button type="submit" id="save" class="btn btn-save">Save</button>-->
<!--                <button class="btn btn-save" id="print">Print</button>-->
<!--            </center>-->

            <div role="tabpanel">
                <div class="row">
                    <div class="col-md-10">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>ID order</label>

                                <p>#<?= $model->ssOrder->orderId ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Date of order</label>

                                <p><?= date('F j Y, g:i a',strtotime($model->ssOrder->orderDate)) ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Email</label>

                                <p><?= $model->ssOrder->customerEmail ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <p><?= $model->ssOrder->orderStatus ?> </p>

                            </div>
                            <div class="form-group">
                                <label>Serial Number</label>
                                <p><?= $model->ssOrder->internalNotes?:'None'?> </p>

                            </div>
                            <table class="table" style="margin-top: 10px;">
                                <tr>
                                    <td>Tax</>
                                    <td id="tax">$<?php echo number_format($model->ssOrder->taxAmount, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Shipping</td>
                                    <td id="shipping">$<?php echo number_format($model->ssOrder->shippingAmount, 2); ?></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td id="total">$<?= number_format($model->ssOrder->orderTotal, 2) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box">
                            <h4>Billing</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <p><?=$model->ssOrder->billTo->name?></p>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <p><?=$model->ssOrder->billTo->phone?></p>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <p><?=$model->ssOrder->billTo->street1?></p>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <p><?=$model->ssOrder->billTo->street2?></p>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <p><?=$model->ssOrder->billTo->street3?></p>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <p><?=$model->ssOrder->billTo->city?></p>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <p><?=$model->ssOrder->billTo->state?></p>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <p><?=$model->ssOrder->billTo->postalCode?></p>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <p><?=$model->ssOrder->billTo->country?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box">
                            <h4>Shipping</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <p><?=$model->ssOrder->shipTo->name?></p>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <p><?=$model->ssOrder->shipTo->phone?></p>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <p><?=$model->ssOrder->shipTo->street1?></p>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <p><?=$model->ssOrder->shipTo->street2?></p>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <p><?=$model->ssOrder->shipTo->street3?></p>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <p><?=$model->ssOrder->shipTo->city?></p>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <p><?=$model->ssOrder->shipTo->state?></p>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <p><?=$model->ssOrder->shipTo->postalCode?></p>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <p><?=$model->ssOrder->shipTo->country?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Products</h4>
                            <table class="table">
                                <thead>
                                <th></th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Sku</th>
                                <th>Qty</th>
                                <th>Unit Price</th>
                                </thead>
                                <tbody>
                                <? foreach ($model->ssOrder->items as $order_product) { ?>
                                    <tr>
                                        <td>
                                            <div class="mouseover-thumbnail-holder">
                                                    <img src="<?php echo $order_product->imageUrl; ?>" width="50"/>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->orderItemId; ?>"><?= $order_product->orderItemId ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->orderItemId; ?>"><?= $order_product->name ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->orderItemId; ?>"><?= $order_product->sku ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->orderItemId; ?>"><?= $order_product->quantity ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $order_product->orderItemId; ?>"><?= number_format($order_product->unitPrice, 2) ?></a>
                                        </td>
                                    </tr>
                                <? } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-24">
                        <div class="box">
                            <h4>Comments</h4>
                            <?php if($model->comments){ ?>
                            <table class="table">
                                <thead>
                                <th>Date</th>
                                <th>Comment</th>
                                </thead>
                                <tbody>
                                <? foreach ($model->comments as $comment) { ?>
                                    <tr>
                                        <td><?=$comment->insert_time?></td>
                                        <td><?=$comment->log?></td>
                                    </tr>
                                <? } ?>

                                </tbody>
                            </table>
                            <?php } ?>
                            <textarea id="comment-text"></textarea>
                            <button class="btn btn-prime" id="comment-submit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
    <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
    <? //=$model->ssOrder->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
</form>
<?php footer(); ?>

<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>

<script>


    $(function () {

        $('select[name="payment_method"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.cc-info').show();
            } else {
                $('.cc-info').hide();
            }
        });

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("orders/");

        });

        $('#save').mouseover(function () {

            var id = $("input[name=id]").val();
            $("#act_for_click").val("orders/update/" + id);

        });

    });
    $("#phone-us").inputmask("+1(999)999-9999");
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        });

        $('#comment-submit').on('click', function(e){
            e.preventDefault();
            var content = $('#comment-text').val();
            if(!content){
                alert('You need put comment!');
                return;
            }
            $.post('/admin/shipstation_orders/create_comment', {id: '<?=$model->ssOrder->orderId?>', content: content}, function(){
                window.location.reload();
            });
        })
    })
</script>