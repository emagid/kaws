<div class="row">
    <div class="col-md-12">
        <div class="box search-box">
            <div class="form-group">
                <label>Search</label>
                <div class="input-group">
                    <input id="search" type="text" name="search" class="form-control" placeholder="Search By Order Id"/>
                    <span class="input-group-addon search-icon">
                        <i class="icon-search"></i>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label>Range</label>
                <div class="input-group">
                    <input id="range" type="text" name="range" class="form-control" value="<?= !$model->start || !$model->end ? '': date('Y-m-d', strtotime($model->start)) . ' - ' . date('Y-m-d', strtotime($model->end)) ?>"/>
                    <span class="input-group-addon remove-date" style="cursor: pointer;">
                        <i class="icon-cancel-circled"></i>
                    </span>
                </div>
            </div>
            <div class="box transparent_box" style="align-content: center">
                <a href="<?= ADMIN_URL ?>shipstation_orders/export_emails" class="btn btn-warning" style="width: 49%">Export DJI Emails</a>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box transparent_box">
            <div class="form-group">
                <label>Show on page:</label>
                <select class="how_many form-control" name="how_many">
                    <? $arr = [50, 100, 500, 1000];
                    foreach ($arr as $a) {?>
                        <option value="<?= $a ?>" <?= isset($_GET['how_many']) && $_GET['how_many'] == $a ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Store</label>
                <select class="store form-control" name="store">
                    <? foreach ($model->stores as $id=>$store) {?>
                        <option value="<?= $id ?>" <?= isset($_GET['store']) && $_GET['store'] == $id ? 'selected' : '' ?>><?= $store ?></option>
                    <? } ?>
                </select>
            </div>
            <div class="box transparent_box" style="align-content: center">
                <a href="<?= ADMIN_URL ?>shipstation_orders/export<?=$_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''?>" class="btn btn-warning" style="width: 49%">Export</a>
                <a class="btn btn-warning mass-email" style="width: 49%">Mass Email</a>
            </div>
        </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        function getQueryObj(str) {
            return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
        }

        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            var queryString;
            if(location.search && how_many != -1){
                queryString = getQueryObj(location.search);
                queryString.how_many = how_many;
            } else if(how_many == -1){
                queryString = getQueryObj(location.search);
                delete queryString.how_many;
            } else {
                queryString = {how_many:how_many};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/shipstation_orders'+qs);
        });

        $('body').on('change', '.store', function () {
            var store = $(this).val();
            var queryString;
            if(location.search && store != -1){
                queryString = getQueryObj(location.search);
                queryString.store = store;
            } else if(store == -1){
                queryString = getQueryObj(location.search);
                delete queryString.store;
            } else {
                queryString = {store:store};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/shipstation_orders'+qs);
        });

//        $('body').on('change', '.sort', function () {
//            var status = $(this).val();
//            var multipleStatus = status.join(',').replace(/,/g, '%2c');
//            if (status == "all") {
//                window.location.href = '/admin/shipstation_orders';
//            }
//            else {
//                window.location.href = '/admin/shipstation_orders?status_show=' + multipleStatus;
//            }
//        });

        $('#range').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo $model->start ? : date('Y-m-d')?>",
            endDate: "<?php echo $model->end ? : date('Y-m-d')?>"
        },function(start,end){
            end.hour(23);end.minute(59);end.second(59);
            var queryString;
            if(location.search){
                queryString = getQueryObj(location.search);
                queryString.t = start.unix()+','+end.unix();
            } else {
                queryString = {t:start.unix()+','+end.unix()};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            window.location.replace('<?=ADMIN_URL.'shipstation_orders'?>'+'?'+ $.param(queryString));
        });

        $('.remove-date').on('click',function(){
            var queryString = getQueryObj(location.search);
            if(queryString.t ) {
                delete queryString.t;
            }
            window.location.replace('<?=ADMIN_URL.'shipstation_orders'?>'+'?'+ $.param(queryString));
        });

        $('body').on('change', '#range', function () {
            var pay_filter = $(this).val();
            var queryString;
            if(location.search && pay_filter != -1){
                queryString = getQueryObj(location.search);
                $.each(queryString,function(i,e){
                    queryString[i] = decodeURIComponent(e);
                });
                queryString.filter = pay_filter;
            } else if(pay_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.filter;
            } else {
                queryString = {filter:pay_filter};
            }
            window.location.replace('/admin/shipstation_orders?'+ $.param(queryString));
        });
    });
</script>
<?php if ($model->ssOrders->total > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="1%"><input type="checkbox" id="check-all"/></th>
                <th width="1%"></th>
                <th width="1%">ID</th>
                <th width="3%">Date</th>
                <th width="5%">Tracking #</th>
                <th width="1%">Status</th>
                <th width="5%">Store</th>
                <th width="5%">Bill Name</th>
                <th width="10%">Products</th>
                <th width="1%">Total</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->ssOrders->orders as $obj) {
                if(!isset($model->stores[$obj->advancedOptions->storeId])){
                    continue 1;
                }
                $updateLink = $obj->type == 'emagid'?'/admin/orders/update/'.$obj->orderId:ADMIN_URL.'shipstation_orders/update/'.$obj->orderId;?>
                <tr class="originalProducts">
                    <td>
                        <input type="checkbox" name="multibox" value="<?=$obj->orderId?>"/>
                    </td>
                    <td>
<!--                        --><?// if (is_null($obj->viewed) || !$obj->viewed) { ?>
<!--                            <button type="button" class="btn btn-success" disabled="disabled">New</button>-->
<!--                        --><?// } ?>
                    </td>
                    <td>
                        <a href="<?=$updateLink?>">
                            <?php echo $obj->orderNumber; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?=$updateLink?>">
                            <?php echo date('Y-m-d H:i:s',strtotime($obj->createDate)); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?=$updateLink?>">
<!--                            --><?php //echo $obj->tracking_number; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?=$updateLink?>">
                            <?= $obj->orderStatus ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?=$updateLink?>">
                            <?= $model->stores[$obj->advancedOptions->storeId] ?>
                        </a>
                    </td>

                    <td>
                        <a href="<?=$updateLink?>">
                            <?php echo $obj->shipTo->name; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?=$updateLink?>">
                            <?$products = [];
                            foreach($obj->items as $item){
                                $products[] = str_replace(["\r","\n"],'',strReplace($item->name, 50, '...'));
                            }
                            echo implode('<br>',$products);?>
                        </a>
                    </td>

                    <td>
                        <a href="<?=$updateLink?>">
                            <?=$obj->orderTotal?>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } else {
    echo "<center>No matches!<br><a href='' onclick='history.go(-1)'>Go back!</a></center>";
} ?>

<?php footer(); ?>

<script>
    var site_url = '/admin/shipstation_orders';
//    var site_url = '/admin/shipstation_orders<?//if ($_GET){unset($_GET['page']); echo '?'.urldecode(http_build_query($_GET).'&');}?>//';
    var total_pages = '<?= $model->ssOrders->pages;?>';
    var page = '<?= $model->ssOrders->page;?>';
</script>
<script type="text/javascript">
    $(function () {
        $(".search-icon").on('click',function () {
            var url = "<?php echo ADMIN_URL; ?>shipstation_orders"+location.search;
            var keywords = $("#search").val();
            if(!keywords){
                window.location.href = '/admin/shipstation_orders';
            } else {
                window.location.href = location.search?url + '&id='+keywords : url + '?id='+keywords;
            }

//            var url = "<?php //echo ADMIN_URL; ?>//shipstation_orders/search"+location.search;
//            var keywords = $("#search").val();
//            if (keywords.length > 1) {
//                $.get(url, {keywords: keywords}, function (data) {
//                    $("#data-list tbody tr").not('.originalProducts').remove();
//                    $('.paginationContent').hide();
//                    $('.originalProducts').hide();
//                    var list = JSON.parse(data);
//                    $.each(list,function(i,list){
//                        if($.inArray(list.status,['Paid', 'Shipped', 'Complete', 'Delivered']) != -1){
//                            var color = 'green';
//                        } else if($.inArray(list.status,['Declined', 'Canceled', 'Incomplete PayPal']) != -1){
//                            color = '#D94F74';
//                        } else {
//                            color = 'black'
//                        }
//                        if(list.payment_method == 2){
//                            var total = '<button type="button" class="btn btn-success" style="position:relative;" disabled="disabled">$'+parseFloat(list.total).toFixed(2)+'</button>';
//                        } else {
//                            total = '$'+parseFloat(list.total).toFixed(2);
//                        }
//                        var tr = $('<tr />');
//                        $('<td />').appendTo(tr).html('<input type="checkbox" name="multibox" value="'+list.id+'"/>');
//                        $('<td />').appendTo(tr).html(list.viewed ? '<button type="button" class="btn btn-success" disabled="disabled">New</button>' : '');
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//shipstation_orders/update/' + list.id).html(list.id));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//shipstation_orders/update/' + list.id).html(list.insert_time));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//shipstation_orders/update/' + list.id).html(list.tracking_number));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//shipstation_orders/update/' + list.id).css('color',color).html(list.status+'<br>('+list.payment_status+')'));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//shipstation_orders/update/' + list.id).html(list.bill_name));
//                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?//= ADMIN_URL;?>//shipstation_orders/update/' + list.id).html(list.products.join('<br>')));
//
//                        $('<td />').appendTo(tr).html(total);
//                        tr.appendTo($("#data-list tbody"));
//                    });
//                    $('input[name=multibox]').prop('checked',false);
//                    $('#check-all').prop('checked',false);
//
//                });
//            } else {
//                $("#data-list tbody tr").not('.originalProducts').remove();
//                $('.paginationContent').show();
//                $('.originalProducts').show();
//            }
        });
    });
    $(function(){
        $('.mass-email').on('click',function(){
            if($('input[name=multibox]:checked').length == 0){
                alert('Select at least one order checkbox');
            } else {
                var arr = [];
                $('input[name=multibox]:checked').each(function(i,e){
                    arr.push(e.value);
                });
                var orders = {orders:arr.join(',')};
                window.location.replace('/admin/shipstation_orders/mass_email?'+ $.param(orders));
            }
        })
    });
    $('#check-all').on('click',function(){
        if($(this).is(':checked')){
            $('input[name=multibox]:visible').prop('checked',true);
        } else {
            $('input[name=multibox]:visible').prop('checked',false);
        }
    });
    $(function () {
        $('input[name="daterange"]').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo \Carbon\Carbon::now()->subWeek();?>",
            endDate: "<?php echo \Carbon\Carbon::now();?>"
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#search_by_mpn").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>shipstation_orders/search_by_mpn";
            var keywords = $.trim($(this).val());
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {

                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html();
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>shipstation_orders/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                        $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html(list[key].products);
                        $('<td />').appendTo(tr).html("$" + list[key].total);
                        /*   var editTd = $('<td />').addClass('text-center').appendTo(tr);
                         var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '
                        <?= ADMIN_URL;?>orders/update/' + list[key].id);
                         var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');*/
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>

