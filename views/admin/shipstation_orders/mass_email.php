<form id="create-order" class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <div role="tabpanel">
        <div class="tab-content">
            <div role="tabpanel">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            <h4>Email</h4>
                            <div class="form-group">
                                <label>Select Email Type</label>
                                <select name="email_type" class="form-control">
                                    <?foreach(\Model\Order::$mass_email as $type=>$text){?>
                                        <option value="<?=$type?>"><?=$type?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group email_text"></div>
                        </div>
                    </div>
                    <div class="col-md-16">
                        <div class="box">
                            <h4>Orders</h4>
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="1%">ID</th>
                                    <th width="3%">Date</th>
                                    <th width="1%">Email</th>
                                    <th width="5%">Tracking #</th>
                                    <th width="1%">Status</th>
                                    <th width="5%">Store</th>
                                    <th width="5%">Bill Name</th>
                                    <th width="10%">Products</th>
                                    <th width="1%">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($model->orders as $obj) {
                                        $style = 'style="background:#ffffff"'; ?>
                                    <tr class="originalProducts" <?= $style ?>>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <?php echo $obj->orderId; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <?php echo date('Y-m-d H:i:s',strtotime($obj->createDate)); ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <?php echo $obj->customerEmail; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <!--                            --><?php //echo $obj->tracking_number; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <?= $obj->orderStatus ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <?= $model->stores[$obj->advancedOptions->storeId] ?>
                                            </a>
                                        </td>

                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <?php echo $obj->shipTo->name; ?>
                                            </a>
                                        </td>

                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <?$products = [];
                                                foreach($obj->items as $item){
                                                    $products[] = str_replace(["\r","\n"],'',strReplace($item->name, 50, '...'));
                                                }
                                                echo implode('<br>',$products);?>
                                            </a>
                                        </td>

                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>shipstation_orders/update/<?php echo $obj->orderId; ?>">
                                                <?=$obj->orderTotal?>
                                            </a>
                                        </td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
    <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
    <? //=$model->order->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
    <button type="submit" class="btn btn-save">Send Email</button>
</form>
<?php footer(); ?>
<script>

    $(document).ready(function () {
        $('[name=email_type]').on('click',function(){
            var mass_email =
                <?$arr = [];
                foreach(\Model\Order::$mass_email as $k=>$i){
                    $arr[$k] = sprintf(\Model\Order::$mass_email_template,'customer',$i);
                }
                echo json_encode($arr);
                ?>;
            var self = $(this).val();
            if(self in mass_email){
                $('.email_text').html(mass_email[self]);
            }
        });
    });
</script>