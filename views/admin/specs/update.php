<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->spec->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select name="category" class="form-control">
                        <option value="0">None</option>
                        <?php foreach ($model->parentCat as $category) {
                            $select = ($category->id == $model->spec->category) ? " selected='selected'" : "";
                            ?>
                            <option
                                    value="<?php echo $category->id; ?>" <?php echo $select; ?> ><?php echo $category->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <h4>Icon</h4>
                    <?php
                    $img_path = "";
                    if ($model->spec->icon != "") {
                        $img_path = $model->spec->getIcon();
                    }
                    ?>
                    <p><input type="file" name="icon" class='image'/></p>
                    <?php if ($model->spec->icon != "") { ?>
                        <div class="well well-sm pull-left">
                            <div id='image-preview'>
                                <img src="<?php echo $img_path; ?>" width="172" height="172"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'specs/delete_image/' . $model->spec->id . '?icon=1'; ?>"
                                   class="btn
                                               btn-default btn-xs">Delete</a>
                                <input type="hidden" name="icon" value="<?= $model->spec->icon ?>"/>
                            </div>
                        </div>
                    <?php } ?>
                    <div class='preview-container'></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL . 'colors/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });
    });
</script>