<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->service_action->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>
    <div class="tab-content">
        <div role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <h4>General</h4>
                        <div class="form-group">
                            <label>Creation Date</label>
                            <div><?$insert_time = new DateTime($model->service_action->insert_time);
                                $insert_time = $insert_time->format('m-d-Y H:i:s');?>
                                <?php echo $insert_time ; ?></div>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" value="<?=$model->service_action->name?>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <center style="margin-bottom: 112px;">
            <button id="save" class="btn btn-save" type="submit">Save</button>
            <button id="save_close" class="btn btn-danger btn-save">Close</button>
        </center>
        <!--</form>-->
        <?php footer(); ?>

        <script type="text/javascript">

            $(document).ready(function () {

                $('#save_close').on('click',function () {
                    window.location.replace('<?=ADMIN_URL."ticket_action"?>');
                });

            })


        </script>
    </div>
</form>