<?php if (count($model->service_actions) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="15%">ID</th>
                <th width="15%">Name</th>
                <th class="text-center">Details</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->service_actions as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>ticket_action/update/<?php echo $obj->id; ?>">
                            <?php  echo $obj->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>ticket_action/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->name ; ?>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>ticket_action/update/<?php echo $obj->id; ?>">
                            <i class="icon-eye"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>ticket_action/delete/<?php echo $obj->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '<?= ADMIN_URL.'ticket_action';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {

    })
</script>