<?php if (count($model->wholesales) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='20%'>Name</th>
                <th width='20%'>Date Created</th>
                <th width='20%'>Status</th>
                <th width='20%'>Email</th>
                <th width='20%'>Phone</th>
                <th width='20%'>Address</th>
                <th width='20%'>Address2</th>
                <th width='20%'>City</th>
                <th width='20%'>State</th>
                <th width='20%'>Zip</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->wholesales as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php $date = new DateTime($obj->insert_time); echo $date->format('Y-m-d H:i:s'); ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo \Model\Wholesale::$status[$obj->status]; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo $obj->email; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo $obj->phone; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo $obj->address; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo $obj->address2; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo $obj->city; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo $obj->state; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>"><?php echo $obj->zip; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>wholesales/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>wholesales/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'wholesales';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>