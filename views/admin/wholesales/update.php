<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->wholesale->id; ?>"/>
    <? $wholesaleProductIDs = [];
       $wholesalePartIDs = [];?>


    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#items" aria-controls="item" role="tab" data-toggle="tab">Items</a></li>
            <li role="presentation"><a href="#orderHistory" aria-controls="item" role="tab" data-toggle="tab">Orders</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <?php echo $model->form->editorFor("email"); ?>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <?php echo $model->form->editorFor("phone"); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->editorFor("address"); ?>
                            </div>
                            <div class="form-group">
                                <label>Address2</label>
                                <?php echo $model->form->editorFor("address2"); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->editorFor("city"); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?php echo $model->form->editorFor("state"); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip</label>
                                <?php echo $model->form->editorFor("zip"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Configuration</h4>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control">
                                    <? foreach (\Model\Wholesale::$status as $key => $value) { ?>
                                        <option value="<?= $key ?>" <?=$key == $model->wholesale->status ? 'selected': ''?>><?= $value ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Terms</label>
                                <select name="terms[]" class="multiselect" multiple>
                                    <? foreach (\Model\Wholesale::$terms as $key => $value) {?>
                                        <option value="<?= $key ?>" <?=json_decode($model->wholesale->terms,true) && in_array($key, json_decode($model->wholesale->terms,true)) ? 'selected': ''?>><?= $value ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Payment Type (CC only)</label>
                                <select name="payment_type" class="form-control">
                                    <? foreach (\Model\Wholesale::$payment_type as $key => $value) { ?>
                                        <option value="<?= $key ?>" <?=$key == $model->wholesale->payment_type ? 'selected': ''?>><?= $value ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="items">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Product Selector</h4>
                            <div class="row">
                                <div class="col-sm-24">
                                    <div class="form-group">
                                        <select class="product_list form-control multiselect" multiple>
                                            <? foreach ($model->productGrid as $type => $prod) { ?>
                                                <optgroup label="<?= $type ?>">
                                                    <? foreach ($prod as $item) {
                                                        $wsKey = preg_match("/[\[,]".$item['id']."[,\]]/",$model->wholesale_ids);
                                                        if ($item['part'] != '1') {
                                                            if($wsKey) $wholesaleProductIDs[] = $item['id'];
                                                            ?>

                                                            <option value="<?= $item['id'] ?>" data-price="<?=$item['price']?>"><?= str_replace(["\r","\n"],'',strReplace($item['name'], 100, '...')) ?></option>
                                                        <? } else {
                                                            if($wsKey) $wholesalePartIDs[] = $item['id'];
                                                        }
                                                    } ?>
                                                </optgroup>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <h4>Parts Selector</h4>
                            <div class="row">
                                <div class="col-sm-24">
                                    <div class="form-group">
                                        <select class="part_list form-control multiselect" multiple>
                                            <? foreach ($model->productGrid as $type => $prod) { ?>
                                                <optgroup label="<?= $type ?>">
                                                    <? foreach ($prod as $item) { ?>
                                                        <? if ($item['part'] == '1') {?>
                                                            <option value="<?= $item['id'] ?>" data-price="<?=$item['price']?>"><?= str_replace(["\r","\n"],'',strReplace($item['name'], 100, '...')) ?></option>
                                                        <? } ?>
                                                    <? } ?>
                                                </optgroup>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <h4>Category Selector</h4>
                            <div class="row">
                                <div class="col-sm-24">
                                    <div class="form-group">
                                        <select class="category_list form-control multiselect" multiple>
                                            <? foreach ($model->categoryGrid as $cat) { ?>
                                                <option value="<?= $cat->id ?>" data-product_ids="<?=$cat->product_id?>"><?= str_replace(["\r","\n"],'',$cat->name)?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>
                                Items List
                            </h4>
                            <input id="uni_discount" type="number" max=100 min=0 placeholder="%discount">
                            <input type="button" value="Apply Discount" onclick="applyDiscount()">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <label>Product Name</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <label>MSRP</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <label>Price</label>
                                    </div>
                                </div>
                            </div>
                            <div class="wholesale_item_set">
                                <? foreach ($model->wholesale_items as $item) {
                                    if ($product = \Model\Product::getItem($item->product_id)) {?>
                                        <div class="row">
                                            <div class="form-group">
                                                <input hidden name="product_id" value="<?= $item->product_id ?>">
                                                <div class="col-sm-8">
                                                    <label></label>
                                                    <div><?= $product->name ?></div>
                                                </div>
                                                <div class="col-sm-8">
                                                    <label></label>
                                                    <div><?= $product->price ?></div>
                                                </div>
                                                <div class="col-sm-8">
                                                    <label></label>
                                                    <input name="price" data-base="<?=$product->price?>" value="<?= $item->price ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    <? }
                                }?>
                            </div>
                            <button type="button" class="btn save_items">Save Items</button>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="orderHistory">
                <div class="row">
                    <div class="col-sm-24">
                        <div class="box box-table">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="10">Id</th>
                                    <th width="10">Date</th>
                                    <th width="10">Tracking #</th>
                                    <th width="10">Status</th>
                                    <th width="10">Billing Name</th>
                                    <th width="10">Products</th>
                                    <th width="10">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?foreach($model->orders as $order){
                                    $products = $order->getOrderProducts();
                                    $productList = $products ? implode('<br>',array_map(function($items){return \Model\Product::getItem($items->product_id)->name;},$products)): '';?>
                                    <tr>
                                        <td><a href="<?=ADMIN_URL?>wholesale_orders/update/<?=$order->id?>"><?=$order->id?></a></td>
                                        <td><a href="<?=ADMIN_URL?>wholesale_orders/update/<?=$order->id?>"><?=date('Y-m-d H:i:s',strtotime($order->insert_time))?></a></td>
                                        <td><a href="<?=ADMIN_URL?>wholesale_orders/update/<?=$order->id?>"><?=$order->tracking_number?></a></td>
                                        <td><a href="<?=ADMIN_URL?>wholesale_orders/update/<?=$order->id?>"><?=$order->status?></a></td>
                                        <td><a href="<?=ADMIN_URL?>wholesale_orders/update/<?=$order->id?>"><?=$order->billName()?></a></td>
                                        <td><a href="<?=ADMIN_URL?>wholesale_orders/update/<?=$order->id?>"><?=$productList?></a></td>
                                        <td><a href="<?=ADMIN_URL?>wholesale_orders/update/<?=$order->id?>">$<?=number_format($order->total,2)?></a></td>
                                    </tr>
                                <?}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL . 'wholesale/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('.product_list').multiselect({
            maxHeight: 415,
            checkboxName: '',
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            onSelectAll: function () {//Does not fire, outdated version of multiselect

            },
            onChange: function (option, checked) {
                if(option !== undefined) {//workaround for outdated multiselect
                    var wholesaleItem = $('.wholesale_item_set').find('[name=product_id][value=' + option.val() + ']');
                    if (wholesaleItem.length) {
                        if (checked) {
                            wholesaleItem.parent().show();
                            wholesaleItem.prop('disabled', false);
                        } else {
                            wholesaleItem.parent().hide();
                            wholesaleItem.prop('disabled', true);
                        }
                    } else {
                        addWholesaleItem(option);
                    }
                    /** Item List Html */
                } else { //SelectAll option
                    console.log('findme');
                    var products = $('.product_list').parent().find('li').not('.multiselect-item,.multiselect-item-all,.group').find('input');
                    var i = 7;
                    products.each(function(){
                        var active  = $(this).parents('li').hasClass('active');
                        var item = $('.product_list').find('option[value='+$(this).val()+']');
                        var wholesaleItem = $('.wholesale_item_set').find('[name=product_id][value=' + item.val() + ']');
                        if (wholesaleItem.length) {
                            if (checked) {
                                wholesaleItem.parent().show();
                                wholesaleItem.prop('disabled', false);
                            } else {
                                wholesaleItem.parent().hide();
                                wholesaleItem.prop('disabled', true);
                            }
                        } else {
                            if (checked){
                                addWholesaleItem(item);
                            }
                        }
                    });
                }
            }
        });
        $('.part_list').multiselect({
            maxHeight: 415,
            checkboxName: '',
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            onSelectAll: function () {//Does not fire, outdated version of multiselect

            },
            onChange: function (option, checked) {
                if(option !== undefined) {//workaround for outdated multiselect
                    var wholesaleItem = $('.wholesale_item_set').find('[name=product_id][value=' + option.val() + ']');
                    if (wholesaleItem.length) {
                        if (checked) {
                            wholesaleItem.parent().show();
                            wholesaleItem.prop('disabled', false);
                        } else {
                            wholesaleItem.parent().hide();
                            wholesaleItem.prop('disabled', true);
                        }
                    } else {
                        addWholesaleItem(option);
                    }
                    /** Item List Html */
                } else { //SelectAll option
                    console.log('findme');
                    var parts = $('.part_list').parent().find('li').not('.multiselect-item,.multiselect-item-all,.group').find('input');
                    var i = 7;
                    parts.each(function(){
                        var active  = $(this).parents('li').hasClass('active');
                        var item = $('.part_list').find('option[value='+$(this).val()+']');
                        var wholesaleItem = $('.wholesale_item_set').find('[name=product_id][value=' + item.val() + ']');
                        if (wholesaleItem.length) {
                            if (checked) {
                                wholesaleItem.parent().show();
                                wholesaleItem.prop('disabled', false);
                            } else {
                                wholesaleItem.parent().hide();
                                wholesaleItem.prop('disabled', true);
                            }
                        } else {
                            if (checked){
                                addWholesaleItem(item);
                            }
                        }
                    });
                }
            }
        });
        $('.category_list').multiselect({
            maxHeight: 415,
            checkboxName: '',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            onChange: function (option, checked) {
                $.each(option.data('product_ids').split(','),function(i,e){
                    var productSelect = $('.product_list,.part_list').parent().find('input[value="'+e+'"]');
                    if((checked && !productSelect.parents('li').hasClass('active')) || (!checked && productSelect.parents('li').hasClass('active'))){
                        productSelect.trigger('click');
                    }
                });
                /** Item List Html */
            }
        });
        $("select.multiselect").each(function (i, e) {
//            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        <?$wholesaleProductIDs = implode(",",$wholesaleProductIDs)?>
        <?$wholesalePartIDs = implode(",",$wholesalePartIDs)?>

        $('.product_list').val(<? echo "[".$wholesaleProductIDs."]" ?>);
        $('.part_list').val(<? echo "[".$wholesalePartIDs."]" ?>);
//        $('.product_list,.category_list').val(<?//=$model->wholesale_ids?>//);
        $('select.multiselect').multiselect('rebuild');

        $('.save_items').on('click',function(){
            var wholesaleItems = $('.wholesale_item_set').find('[name=product_id]:enabled');
            var wholesaleIds = [];
            wholesaleItems.each(function(i,e){
                e = $(e);
                var id = e.val();
                var price = e.siblings().find("[name=price]").val() ? e.siblings().find("[name=price]").val() : e.siblings().find('[name=price]').data('base');
                var obj = {id: id,price: price};
                wholesaleIds.push(obj)
            });
            console.log(wholesaleItems);
            $.post('/admin/wholesales/save_items',{id:<?=$model->wholesale->id?>,items:wholesaleIds},function(data){
                console.log(data);
            })
        });
    });
    function applyDiscount(){
        var discount = $("#uni_discount").val();
        discount = discount/100;
        //discount = 1-discount //alternate
        $('.wholesale_item_set input[name="price"]').each(function(price) {
           var fullVal = parseInt($(this).data('base'));
            var discountVal = fullVal * discount;
            discountVal = (discountVal).toFixed(2);
            $(this).val(discountVal);
        });
    }
    function addWholesaleItem (item) {//jquery object of the multiselect checkbox
        var html = '<div class="row">';
        html += '<div class="form-group">';
        html += '<input hidden name="product_id" value="' + item.val() + '">';
        html += '<div class="col-sm-8">';
        html += '<label></label>';
        html += '<div>' + item.text() + '</div>';
        html += '</div>';
        html += '<div class="col-sm-8">';
        html += '<label></label>';
        html += '<div>' + item.data('price') + '</div>';
        html += '</div>';
        html += '<div class="col-sm-8">';
        html += '<label></label>';
        html += '<input name="price" data-base="'+item.data('price')+'" value="" class="form-control">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.wholesale_item_set').append(html);
    }
</script>