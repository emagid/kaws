<?php if (count($model->blog_categorys) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='20%'>Image</th>
                <th width='20%'>Name</th>
                <th width='20%'>Parent Category</th>
                <th width='20%'>Order</th>
                <th width='20%'>Slug</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->blog_categorys as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <?$img_path = $obj->getBanner()?>
                        <a href="<?php echo ADMIN_URL; ?>blog_categorys/update/<?php echo $obj->id; ?>"><img
                                src="<?php echo $img_path; ?>" width="50"/></a>
                    </td>
                    <td><a href="<?php echo ADMIN_URL; ?>blog_categorys/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blog_categorys/update/<?php echo $obj->id; ?>"><?php echo $obj->parent_id != 0 ? \Model\Blog_Category::getItem($obj->parent_id)->name : ''; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blog_categorys/update/<?php echo $obj->id; ?>"><?php echo $obj->display_order; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>blog_categorys/update/<?php echo $obj->id; ?>"><?php echo $obj->slug; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>blog_categorys/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>blog_categorys/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'blogs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>