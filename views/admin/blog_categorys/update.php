<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->blog_category->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Order</label>
                    <?php echo $model->form->editorFor("display_order",["type"=>"number"]); ?>
                </div>
                <div class="form-group">
                    <label>Parent</label>
                    <?php echo $model->form->dropDownListFor("parent_id",\Model\Blog_Category::getOptions($model->blog_category->id)); ?>
                </div>
                <div class="form-group">
                    <label>Banner image</label>

                    <p><input type="file" name="banner" class='image'/></p>

                    <div style="display:inline-block">
                        <?php
                        $img_path = "";
                        if ($model->blog_category->banner != "") {
                            $img_path = $model->blog_category->getBanner();
                            ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'blog_categorys/delete_image/' . $model->blog_category->id; ?>?banner=1"
                                   onclick="return confirm('Are you sure?');"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="banner"
                                       value="<?= $model->blog_category->banner ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Slug</label>
                    <?php echo $model->form->editorFor("slug"); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="btn btn-save saveorupdate">Save</div>


</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'blog/');?>;
    $(document).ready(function () {
        $("select[name='parent_id']").prepend("<option value=0 <?=$model->blog_category->parent_id == 0 ? 'Selected' : '' ?> >None</option>");
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
    $(".saveorupdate").click(function () {
        var name = $("input[name='name']").val();
        var slug = $("input[name='slug']").val();
        var id = <?=$model->blog_category->id?>;
        var errors = new Array();
        if ($.trim(name) == "") {
            $("input[name='name']").css({
                "border-color": "red"
            });
            errors.push("Incorrect name");
        } else {
            $("input[name='name']").css({
                "border-color": ""
            });
        }
        if ($.trim(slug) == "") {
            $("input[name='slug']").css({
                "border-color": "red"
            });
            errors.push("Incorrect slug");
        } else {

            $.ajax({
                async: false,
                url: '/admin/blog_categorys/check_slug',
                enctype: 'multipart/form-data',
                method: 'POST',
                data: {
                    slug: slug,
                    id: id
                },
                success: function (data) {

                    if (data == 1) {
                        errors.push("Slug must be uniq!");

                        $("input[name='slug']").css({
                            "border-color": "red"
                        });

                    } else {
                        $("input[name='slug']").css({
                            "border-color": ""
                        });
                    }
                }
            });
        }
        var text = "";
        for (i = 0; i < errors.length; i++) {

            text += "<li>" + errors[i] + "</li>";
        }
        if (errors.length > 0) {
            $('html, body').animate({
                scrollTop: 0
            });
            $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
            errors = [];
        } else {
            $(".form").submit();
        }
    });
</script>