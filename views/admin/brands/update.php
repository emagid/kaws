<?php
$brand = $model->brand;
?>


<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input name="id" type="hidden" value="<?php echo $brand->id;?>" />
  <input name="token" type="hidden" value="<?php echo get_token();?>" />

   <div class="row">
      <div class="col-md-12">
          <div class="box">
            <h4>General</h4>
            
             <div class="form-group">
              <label>Name</label>
              <?php echo $model->form->editorFor('name'); ?>
            </div>
            
               <div class="form-group">
                    <label>Featured image</label>
                    <p><small>(ideal featured image size is 1920 x 300)</small></p>
                    <?php 
                    $img_path = "";
                    
                    ?>


                    <p><input type="file" name="image" class='image' /></p>
                    <div style="display:inline-block">
                    <?php if($model->brand->image != "" && file_exists(UPLOAD_PATH.'brands'.DS.$model->brand->image)){
                      $img_path = UPLOAD_URL . 'brands/' . $model->brand->image;
                      ?>
                    <div class="well well-sm pull-left">
                        <img src="<?php echo $img_path; ?>" width="100" />
                        <br />

                        <a href="<?= ADMIN_URL.'brands/delete_image/'.$model->brand->id;?>?token_id=<?php echo get_token();?>" class="btn btn-default btn-xs">Delete</a>
                        <input type="hidden" name="image" value="<?=$model->brand->image?>" />
                    </div>
                    <?php } ?>
                    <div class='preview-container'></div>
                    </div>
                </div>
            
            
           
            <div class="form-group">
              <label>Description</label>
              <?php echo $model->form->textAreaFor('description',['class'=>'ckeditor']); ?>
            </div>
           
            <div class="form-group">
              <label>Display Order</label>
              <?php echo $model->form->editorFor("display_order");?>
            </div>
             <div class="form-group">
             <label>
                        <?php echo $model->form->checkBoxFor("show_in_menu", 1); ?>  Show In Menu?
                </label>
            </div>
          </div>
      </div>
     
     <div class="col-md-12">
       <div class="box">
         <h4>SEO</h4>
         <div class="form-group">
              <label>Slug</label>
              <?php echo $model->form->editorFor('slug'); ?>
            </div>
          <div class="form-group">
              <label>Meta Title</label>
              <?php echo $model->form->editorFor('meta_title'); ?>
            </div>
          <div class="form-group">
              <label>Meta Keywords</label>
              <?php echo $model->form->editorFor('meta_keywords'); ?>
            </div>
          <div class="form-group">
              <label>Meta Description</label>
              <?php echo $model->form->textAreaFor('meta_description'); ?>
            </div>
       </div>
       <div class="box">
         <h4>Add as subcategory</h4>
         <div class="form-group">
          <label>Parent Brand</label>
             <select name="parent_id">
                 <option value="0">NO PARENT</option>
                 <?foreach($model->brand->getParentBrands() as $parents){?>
                     <option value="<?=$parents->id?>" <?=$parents->id == $model->brand->parent_id?'selected':''?>><?=strtoupper($parents->name)?></option>
                 <?}?>
             </select>
        </div>
           <div class="form-group">
               <label>Sub-Categories:</label>
               <?foreach($model->brand->getChildrenBrands() as $childrenBrand){?>
                   <div><?=$childrenBrand->name?></div>
               <?}?>
           </div>
       </div>
		<div class="box">
			<div class="form-group">
				<h4>Banner</h4>
				<p><small>(ideal image size is 1200 x 344)</small></p>
				<?php 
				$img_path = "";
				if($model->brand->banner != ""){
					$img_path = UPLOAD_URL . 'brands/' . $model->brand->banner;
				}
				?>
				<p><input type="file" name="banner" class='image' /></p>
				<?php if($model->brand->banner != ""){ ?>
				<div class="well well-sm pull-left">
					<div id='image-preview'>
						<img src="<?php echo $img_path; ?>" width="600" height="172" />
						<br />
						<a href=<?= ADMIN_URL.'brands/delete_image/'.$model->brand->id.'?banner=1';?> class="btn btn-default btn-xs">Delete</a>
						<input type="hidden" name="banner" value="<?=$model->brand->banner?>" />
					</div>
				</div>
				<?php } ?>
				<div class='preview-container'></div>
				<div class="clearfix"></div>
			</div>
		</div>
     </div>
   </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
	$(document).ready(function() {

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				var img = $("<img />");
				reader.onload = function (e) {
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'600');
					img.attr('height','172');
				};
				$(input).parent().parent().find('.preview-container').html(img);
				$(input).parent().parent().find('input[type="hidden"]').remove();

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
			
		});	

		$("input[name='name']").on('keyup',function(e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-');
			val = val.replace(/[-]+/g,'-');
			$("input[name='slug']").val(val.toLowerCase());
		});

	});

</script>