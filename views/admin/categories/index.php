<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>

<div class="row">
    <div class="col-md-16">
        <div class="box search-box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
                <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
</div>
      <a href="<?=ADMIN_URL.'categories/sort'?>" style="margin-bottom:12px;" class="underlineBtn underlineBtn-blue underlineBtn-blue-black">Sort Categories</a>
<?php
 if(count($model->categories)>0): ?>
  <div class="box box-table">

    <table id="data-list" class="table">
      <thead>
        <tr>
          <th width="15%">Name</th>
          <th width="15%">Parent</th>
          <th width="15%">Displayed</th>
<!--          <th width="10%">Collection Item</th>-->
<!--          <th width="10%">Display Order</th>-->
<!--          <th width="10%">External Category</th>-->
<!--          <th width="10%">Coupons Blocked</th>-->
          <th width="1%" class="text-center">Edit</th>
          <th width="1%" class="text-center">Delete</th>
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->categories as $obj){ ?>
        <tr class="originalProducts">
         <td><a href="<?php echo ADMIN_URL; ?>categories/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>categories/update/<?= $obj->id ?>"><?php echo $obj->getParent() ? $obj->getParent()->name : '' ; ?></a></td>
         <td><a href="<?php echo ADMIN_URL; ?>categories/update/<?= $obj->id ?>"><?php echo $obj->display == 1 ? 'True': 'False'; ?></a></td>
<!--         <td><a href="--><?php //echo ADMIN_URL; ?><!--categories/update/--><?//= $obj->id ?><!--">--><?php //echo $obj->collection ? 'Yes': 'No'; ?><!--</a></td>-->
<!--         <td><a href="--><?php //echo ADMIN_URL; ?><!--categories/update/--><?//= $obj->id ?><!--">--><?php //echo $obj->display_order; ?><!--</a></td>-->
<!--         <td><a href="--><?php //echo ADMIN_URL; ?><!--categories/update/--><?//= $obj->id ?><!--">--><?php //echo $obj->external_category ? '<i class="icon-ok-circled">': ''; ?><!--</a></td>-->
         <td class="text-center">
           <a class="btn-actions" href="<?= ADMIN_URL ?>categories/update/<?= $obj->id ?>">
           <i class="icon-pencil"></i>
           </a>
         </td>
         <td class="text-center">
           <a class="btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black" href="<?= ADMIN_URL ?>categories/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
             <i class="icon-cancel-circled"></i>
           </a>
         </td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'categories';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>categories/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.post(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var json = $.parseJSON(data);
                    var list = json.data;

                    for(var key = 0; key<list.length ;key++) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>categories/update/' + list[key].id).html(list[key].name));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>categories/update/' + list[key].id).html(list[key].parent_cateName));
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);

                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>categories/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>categories/delete/' + list[key].id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
        var adjustment;
        /*$('#sortables').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden">.</td></tr>',
            onDrop: function ($item, container, _super) {
//                var $clonedItem = $('<tr/>').css({height: 0});
//                $item.before($clonedItem);
//                $clonedItem.animate({'height': $item.height()});
//
//                $item.animate($clonedItem.position(), function  () {
//                    $clonedItem.detach();
//                    _super($item, container);
//                });
//                if ($(event.target).hasClass('delete-product-image')) {
//                    $(event.target).trigger('click');
//                }
//
//                var ids = [];
//                var tr_containers = $("#image-container > tbody > tr");
//                tr_containers.each(function (i, e) {
//                    ids.push($(e).data("image_id"));
//
//                });
//                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
//                    sort_number_display();
//                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });*/
    })
</script>