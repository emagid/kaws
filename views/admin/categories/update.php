<style>
    .featured{
        border:1px solid black;
    }
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <?if($model->category->product_preview_ids){?>
                <li role="presentation"><a href="#sort-preview" aria-controls="general" role="tab" data-toggle="tab">Preview Order</a></li>
            <?}?>
            <?php if (isset($model->products) && count($model->products) > 0): ?>
                <li role="presentation"><a href="#cat-products" aria-controls="products" role="tab" data-toggle="tab">Products</a></li>
                <li role="presentation"><a href="#cat-image" aria-controls="products" role="tab" data-toggle="tab">Default Product</a></li>
            <? endif; ?>
            <?if($model->category->parent_category == 0 && isset($model->child_categories) && count($model->child_categories) > 0){?>
                <li role="presentation"><a href="#info-banner" aria-controls="info-banner" role="tab" data-toggle="tab">Child Categories</a></li>
            <?}?>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->category->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
<!--                            <div class="form-group">-->
<!--                                <label>Display Name</label>-->
<!--                                --><?php //echo $model->form->editorFor("alias"); ?>
<!--                            </div>-->
                            <div class="form-group">
                                <label>Title(Default <b>NAME Series</b>)</label>
                                <?php echo $model->form->editorFor("title"); ?>
                            </div>
<!--                            <div class="form-group">-->
<!--                                <label>Collection Item</label>-->
<!--                                --><?php //echo $model->form->checkboxFor("collection", 1); ?>
<!--                            </div>-->
                            <div class="form-group">
                                <label>Display Order</label>
                                <input name="display_order" type="number"
                                       value="<?= $model->category->display_order ?: 0 ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Parent Category</label>
                                <select name="parent_category" class="form-control">
                                    <option value="0">None</option>
                                    <?php foreach ($model->parentCategories as $category) {
                                        $select = ($category->id == $model->category->parent_category) ? " selected='selected'" : "";
                                        ?>
                                        <option
                                            value="<?php echo $category->id; ?>" <?php echo $select; ?> ><?php echo $category->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Featured Product</label>
                                <select name="featured_product" class="form-control">
                                    <option value="0">None</option>
                                    <?php foreach ($model->products as $product) {
                                        $select = ($product->id == $model->category->featured_product) ? " selected='selected'" : "";
                                        ?>
                                        <option value="<?php echo $product->id; ?>" <?php echo $select; ?> ><?php echo $product->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <!--
                            <div class="form-group">
                                <label>New Arrivals</label>
                                <select name="new_arrivals[]" class="multiselect" multiple>
                                    <option value="">None</option>
                                    <?php foreach ($model->products as $product) {
                                        $select = ($product->id == $model->category->featured_product) ? " selected='selected'" : "";
                                        ?>
                                        <option value="<?php echo $product->id; ?>" <?php echo $select; ?> ><?php echo $product->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Main Category Display </label> <small>(pick 4)</small>
                                <select name="product_preview_ids[]" class="product_preview_ids" multiple>
                                    <option value="">None</option>
                                    <?php foreach ($model->products as $product) {?>
                                        <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        -->
                            <?if($model->category->parent_category){?>
                                <div class="form-group">
                                    <label>Featured category</label>
                                    <?php echo $model->form->checkboxFor("featured_category", 1); ?>
                                </div>
                            <?}?>
                            <!--
                            <div class="form-group">
                                <label>External Category <small>(Designate this category to fall under 'All Shoes')</small></label>
                                <?php echo $model->form->checkboxFor("external_category", 1); ?>
                            </div>
                            -->
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ["rows" => "5"]); ?>
                            </div>
                            <div class="form-group">
                                <label>Display</label>
                                <select name="display" class="form-control">
                                    <option value="1" <?php if ($model->category->display == '1') {echo "selected";} ?>>Yes</option>
                                    <option value="2" <?php if ($model->category->display == '2') {echo "selected";} ?>>No</option>
                                </select>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor('slug'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor('meta_title'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor('meta_keywords'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->textAreaFor('meta_description'); ?>
                            </div>
                        </div>
                        <div class="box">
                            <div class="form-group">
                                <h4>Banner</h4>

                                <p>
                                    <small>(ideal image size is 1200 x 344)</small>
                                </p>
                                <?php
                               $img_path = "";
                                if ($model->category->banner != "") {
                                    $img_path = UPLOAD_URL . 'categories/' . $model->category->banner;
                                }
                                ?>
                                <p><input type="file" name="banner" class='image'/></p>
                                <?php if ($model->category->banner != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <div id='image-preview'>
                                            <img src="<?php echo $img_path; ?>" width="600" height="172"/>
                                            <br/>
                                            <a href=<?= ADMIN_URL . 'categories/delete_image/' . $model->category->id . '?banner=1'; ?> class="btn
                                               btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="banner" value="<?= $model->category->banner ?>"/>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class='preview-container'></div>
                                <div class="clearfix"></div>
                            </div>
                            <!--
                            <div class="form-group">
                                <label>Banner Description</label>
                                <?php echo $model->form->editorFor('banner_description')?>
                            </div>-->
                            <!--
                            <div class="form-group">
                                <label>Banner Black Text</label><p><small> (Default is white) </small></p>
                                <?php echo $model->form->checkboxFor('banner_text_color',1)?>
                            </div>
                            <div class="form-group">
                                <label>Hide Banner Text</label><p><small> (Allow custom text on banners) </small></p>
                                <?php echo $model->form->checkboxFor('hide_banner_text',1)?>
                            </div>
                            <div class="form-group">
                                <label>Block Coupons</label><p><small> (Coupons will not affect products in this category) </small></p>
                                <?php echo $model->form->checkboxFor('coupon_block',1)?>
                            </div>
                            -->
                        </div>
                        <div class="box">
                            <div class="form-group">
                                <h4>Icon</h4>
                                <?php
                                $img_path = "";
                                if ($model->category->icon != "") {
                                    $img_path = $model->category->getIcon();
                                }
                                ?>
                                <p><input type="file" name="icon" class='image'/></p>
                                <?php if ($model->category->icon != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <div id='image-preview'>
                                            <img src="<?php echo $img_path; ?>" width="172" height="172"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'categories/delete_image/' . $model->category->id . '?icon=1'; ?>" class="btn
                                               btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="icon" value="<?= $model->category->icon ?>"/>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class='preview-container'></div>
                                <div class="clearfix"></div>
                            </div>
                            <!--
                            <div class="form-group">
                                <label>Banner Description</label>
                                <?php echo $model->form->editorFor('banner_description')?>
                            </div>-->
                            <!--
                            <div class="form-group">
                                <label>Banner Black Text</label><p><small> (Default is white) </small></p>
                                <?php echo $model->form->checkboxFor('banner_text_color',1)?>
                            </div>
                            <div class="form-group">
                                <label>Hide Banner Text</label><p><small> (Allow custom text on banners) </small></p>
                                <?php echo $model->form->checkboxFor('hide_banner_text',1)?>
                            </div>
                            <div class="form-group">
                                <label>Block Coupons</label><p><small> (Coupons will not affect products in this category) </small></p>
                                <?php echo $model->form->checkboxFor('coupon_block',1)?>
                            </div>
                            -->
                        </div>
                    </div>
                </div>
            </div>
            <?if($model->category->product_preview_ids){?>
                <div role="tabpanel" class="tab-pane" id="sort-preview">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <h4>Products</h4>
                                <div class="form-group">
                                    <label>Product Previews</label>
                                    <div id="selector">
                                        <? foreach (json_decode($model->category->product_preview_ids) as $obj) {
                                            if (($prod = \Model\Product::getItem($obj))) {?>
                                                <div class="item" data-id="<?= $prod->id ?>"><img width="100px" src="<?= UPLOAD_URL . 'products/' . $prod->featuredImage() ?>"/></div>
                                            <? } ?>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?}?>
            <?php if (isset($model->products) && count($model->products) > 0): ?>
                <div role="tabpanel" class="tab-pane" id="cat-products">
                    <div class="rox">
                        <div class="box box-table">
                            <table id="image-container" class="table">
                                <thead>
                                <tr>
                                    <th width="5%">Image</th>
                                    <th width="10%">Name</th>
                                    <th width="5%">Type</th>
                                    <th width="5%">Price</th>
                                    <th width="5%">Featured Image</th>
                                    <th width="5%">Display Order</th>
                                    <th width="1%" class="text-center">Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($model->products as $obj) {
                                    $prodCat = \Model\Product_Category::getItem(null,['where'=>"product_id = $obj->id and category_id = {$model->category->id}"]) ?>
                                    <tr class="originalProducts" data-prodCat_id="<?=$prodCat->id?>">
                                        <input type="hidden" name="product_category[]" value="<?= $obj->id ?>"/>
                                        <td>
                                            <?php
                                            if ($obj->featuredImage() == '') {
                                                $img_path = ADMIN_IMG . 'modernvice_shoe.png'; //TODO st-dev image path was to live site... weird
                                            } else {
                                                $img_path = UPLOAD_URL.'products/'.$obj->featuredImage();
                                            }
                                            $imgArray = array_merge(['Default Image'],$obj->getAllProductImages(false));
                                            ?>
                                            <img src="<?php echo $img_path; ?>" width="50"/>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                                        </td>
                                        <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?php echo \Model\Product::$type[$obj->type]; ?></a></td>
                                        <td>$<?= number_format($obj->price, 2) ?></td>
                                        <td><?php echo $model->form->dropDownListFor('image_id', $imgArray,'',['class'=>"multiselect"])?></td>
                                        <td class="display-order-td"><?=$prodCat->display_order?></td>
                                        <td class="text-center">
                                            <a class="btn-actions"
                                               href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>">
                                                <i class="icon-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="box-footer clearfix">
                                <div class='paginationContent'></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="cat-image">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <h4>Default Product</h4>
                                <select name="default_product" class="form-control">
                                    <?$default = ($model->category->default_product); ?>
                                    <option value="0" selected>Default Product</option>
                                    <?foreach($model->products as $obj){
                                        if($obj->type == '1'){?>
                                        <option value="<?=$obj->id?>" <?php if ($default == $obj->id) {echo "selected";} ?>><?=$obj->name ?></option>
                                    <?}
                                    }?>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?if($model->category->parent_category == 0 && isset($model->child_categories) && count($model->child_categories) > 0){?>
                <div role="tabpanel" class="tab-pane" id="info-banner">
                    <div class="rox">
                        <div class="box box-table">
                            <table id="child" class="table">
                                <thead>
                                <tr>
                                    <th width="10%">Name</th>
                                    <th width="10%">Default Product</th>
                                    <th width="10%">Display</th>
                                    <th width="10%">Display Order</th>
                                    <th width="1%" class="text-center">Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($model->child_categories as $obj) {
                                    $product = ($obj->default_product>0)? \Model\Product::getItem($obj->default_product) : '';
//                                    $prodCat = \Model\Product_Category::getItem(null,['where'=>"product_id = $obj->id and category_id = {$model->category->id}"]) ?>
                                    <tr class="originalProducts" data-category_id="<?=$obj->id?>">
<!--                                        <input type="hidden" name="product_category[]" value="--><?//= $obj->id ?><!--"/>-->
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>categories/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $product->id; ?>"><?php echo $product->name; ?></a>
                                        </td>
                                        <td>
                                            <select name="child_display" data-cat_id="<?=$obj->id?>">
                                                <option value="1" <?php if ($obj->display == '1') {echo "selected";} ?>>Yes</option>
                                                <option value="2" <?php if ($obj->display == '2') {echo "selected";} ?>>No</option>
                                            </select>
                                        </td>
                                        <td class="cat-display-order-td">
                                            <a href="<?php echo ADMIN_URL; ?>categories/update/<?php echo $obj->id; ?>"><?php echo $obj->display_order; ?></a>
                                        </td>

                                        <td class="text-center">
                                            <a class="btn-actions"
                                               href="<?php echo ADMIN_URL; ?>categories/update/<?php echo $obj->id; ?>">
                                                <i class="icon-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="box-footer clearfix">
                                <div class='paginationContent'></div>
                            </div>
                        </div>
                    </div>
                </div>
            <!--
                <div role="tabpanel" class="tab-pane" id="info-banner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box">
                                <h4>Info Banners</h4>
                                <div class="form-group">
                                    <label>Add new banner</label>
                                    <div id="add-info-banner">Click here</div>
                                </div>
                                <hr/>
                                <div class="info-banner-container">
                                    <?if($model->category->info_banners){
                                        foreach(json_decode($model->category->info_banners, true) as $item){
                                            $img = $item['info-banner'] ? UPLOAD_URL.'categories/'.$item['info-banner']: '';?>
                                            <div class="info-banner-group">
                                                <div class="form-group">
                                                    <label>Banner Image</label>
                                                    <input type="file" name="info-banner[]" >
                                                    <img src="<?=$img?>" style="width: 200px;">
                                                    <input type="text" name="info-banner[]" value="<?=$item['info-banner']?>" style="display: none;"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>Info Header</label>
                                                    <input type="text" name="info-header[]" value="<?=$item['info-header']?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Info Description</label>
                                                    <textarea name="info-desc[]" rows="5"><?=$item['info-desc']?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Info Link</label>
                                                    <input type="text" name="info-link[]" value="<?=$item['info-link']?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Info Background Color</label>
                                                    <input type="text" name="info-color[]" value="<?=isset($item['info-color'])?$item['info-color']:''?>" class="jscolor">
                                                </div>
                                                <div class="form-group">
                                                    <label>Info Font Color</label>
                                                    <input type="text" name="info-fontcolor[]" value="<?=isset($item['info-fontcolor'])?$item['info-fontcolor']:''?>" class="jscolor">
                                                </div>
                                                <div class="delete-banner">Delete</div>
                                                <hr/>
                                            </div>
                                        <?}?>
                                    <?}?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                -->
            <?}?>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<script src="<?=ADMIN_JS.'jscolor.min.js'?>"></script>
<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        var new_arrivals = <?php echo ($model->category->new_arrivals?:0); ?>;
        var product_preview_ids = <?php echo ($model->category->product_preview_ids?:0); ?>;
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '172');
                    img.attr('height', '172');
                };

                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("select[name='child_display']").on('change',function(){
            var that = $(this);
            var id = that.attr("data-cat_id");
            var value = that.val();
            $.post('<?=ADMIN_URL?>categories/updateChild',{id:id,value:value},function (data) {
                var json = $.parseJSON(data);
                if(json.status == 'success'){
                    console.log('success');
                }else{
                    alert('failed');
                }
            })
        })
        $('.changeImg').on('click',function(){
            var product_id = $(this).attr('data-product_id');
            var category_id = <?=$model->category->id?>;
            var image = $(this).attr('data-image');
            var self = $(this);
            $.post('/admin/categories/updateCategoryProductImage', {product_id:product_id,category_id:category_id,image:image},function(data){
                var json = $.parseJSON(data);
                if(json.status == 'success'){
                    self.parent().find('.featured').css('border','0px').removeClass('featured');
                    self.addClass('featured');
                    self.css('border','1px solid black');
                }
            });
        });

        $('#add-info-banner').on('click',function(){
            var html =
                '<div class="info-banner-group">' +
                    '<div class="form-group">' +
                        '<label>Banner Image</label>' +
                        '<input type="file" name="info-banner[]">' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label>Info Header</label>' +
                        '<input type="text" name="info-header[]">' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label>Info Description</label>' +
                        '<textarea name="info-desc[]" rows="5"></textarea>' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label>Info Link</label>' +
                        '<input type="text" name="info-link[]">' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label>Info Color</label>' +
                        '<input type="text" name="info-color[]" class="jscolor">' +
                    '</div>' +
                    '<div class="delete-banner">Delete</div><hr/>' +
                '</div>';
            $('.info-banner-container').append(html);
            jscolor.installByClassName('jscolor');
        });

        $('.info-banner-container').on('click','.delete-banner',function(){
            if(confirm("Delete this banner?")){
                $(this).parent().remove();
            } else {
                return false;
            }
        });

        $('.product_preview_ids').multiselect({
            maxHeight: 415,
            checkboxName: '',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%',
            onChange: function(option, checked) {
                // Get selected options.
                var selectedOptions = $('.product_preview_ids option:selected');

                if (selectedOptions.length > 3) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('.product_preview_ids option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    var dropdown = $('.product_preview_ids').siblings('.multiselect-container');
                    nonSelectedOptions.each(function() {
                        var input = $(this).parent().parent().find('input[value="' + $(this).val() + '"]');
//                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    var dropdown = $('.product_preview_ids').siblings('.multiselect-container');
                    $('.product_preview_ids option').each(function() {
                        var input = $(this).parent().parent().find('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='new_arrivals[]']").val(new_arrivals);
        $("select[name='product_preview_ids[]']").val(product_preview_ids);
        $("select.multiselect").multiselect("rebuild");
        $(".product_preview_ids").multiselect("rebuild");

        $("input[name='name']").on('keyup', function (e) {
            var val = $.trim($(this).val());
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('mousewheel.disableScroll')
        });

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }
        var adjustment;
        $('#selector').sortable({
            containerSelector: 'div',
            itemSelector: '.item',
            placeholder: '<div class="item"><img width="100px" height="100px" src=""/></div>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("prodCat_id"));

                });
                var array = [];
                $.each($('.item'),function(key,item){
                    array.push(item.getAttribute('data-id'));
                });
                $.post('/admin/categories/orderPreview',{array:array,id:<?=$model->category->id?>});
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden;">.</td></tr>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("prodcat_id"));

                });
                $.post('<?=ADMIN_URL.'categories/orderProduct'?>', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });

        $('#child').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden;">.</td></tr>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#child > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("category_id"));

                });
                console.log(ids);
                $.post('<?=ADMIN_URL.'categories/orderChildCategory'?>', {ids: ids}, function (response) {
                    var counter = 1;
                    $('#child >tbody>tr').each(function (i, e) {
                        $(e).find('td.cat-display-order-td').html(counter);
                        counter++;
                    });
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });

    });
</script>