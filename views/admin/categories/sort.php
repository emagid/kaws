<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<div class="box">
    <h3>Categories</h3>
        <ol class="sortable">
            <?foreach($model->categories as $category){
                echo $category->buildSortable();
            }?>
        </ol>
    <div class="serialize-result"></div>
</div>
<?php echo footer(); ?>
<script>
    $(document).ready(function(){
        $(document).on('click','.display_category',function(){
            var value = $(this).val();
            var id = $(this).parent().data('id');
            if(value == 1){ /** Is checked, uncheck */
                var newVal = 2;
            } else { /** Is unchecked, check */
                newVal = 1;
            }
            $(this).val(newVal);
            $.post('/admin/categories/updateDisplay',{id:id,display:newVal},function(data){
                var json = $.parseJSON(data);
                console.log(json.status);
            });
        });
        var adjustment;
        var serial = $('.sortable').sortable({
            group:'sortable',
            onDrop: function ($item, container, _super) {
                var data = serial.sortable("serialize").get();
                var jsonData = JSON.stringify(data,null,'');

//                $('.serialize-result').text(jsonData);
                $.post('/admin/categories/updateCategory',{data:jsonData},function(){
                    console.log('done');
                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
    })
</script>