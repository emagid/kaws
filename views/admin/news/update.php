<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <input name="id" type="hidden" value="<?php echo $model->article->id;?>" />
    <input name="redirectTo" type="hidden" value="<?=ADMIN_URL.'news'?>" />
    <input name="insert_time" type="hidden" value="<?php echo $model->article->insert_time;?>" />
    <div class="row">
        <div class="col-md-12">
            <div class="box">
            	<h4>General</h4>
	            <div class="form-group">
	            	<label>Name</label>
	            	<?php echo $model->form->editorFor('name'); ?>
	            </div>
                <div class="form-group">
                    <label>Image</label>
                    <p><small>(ideal image size is 650 x 500)</small></p>
                    <?php 
                    	$img_path = "";
                    ?>
                    <p><input type="file" name="image" class='image' /></p>
                    <div style="display:inline-block">
	                    <?php if($model->article->image != "" && file_exists(UPLOAD_PATH.'articles'.DS.$model->article->image)){
	                    	$img_path = UPLOAD_URL . 'articles/' . $model->article->image;
	                    ?>
		                    <div class="well well-sm pull-left">
		                        <img src="<?php echo $img_path; ?>" width="650" />
		                        <br />
		                        <a href="<?= ADMIN_URL.'news/delete_image/'.$model->article->id;?>" class="btn btn-default btn-xs">Delete</a>
		                        <input type="hidden" name="image" value="<?=$model->article->image?>" />
		                    </div>
	                    <?php } ?>
                    <div class='preview-container'></div>
                    </div>
                </div>
                <div class="form-group">
	            	<label>Author</label>
	            	<?php echo $model->form->editorFor('author'); ?>
	            </div>
	            <div class="form-group">
	                <label>Text</label>
	                <?php echo $model->form->textAreaFor('content',['class'=>'ckeditor']); ?>
	            </div>
          	</div>
      	</div>
      	<div class="col-md-12">
      		<div class="box">
				<h4>Meta Information</h4>
				<div class="form-group">
					<label>Slug</label>
					<?php echo $model->form->editorFor("slug"); ?>
				</div>
				<div class="form-group">
					<label>Meta title</label>
					<?php echo $model->form->editorFor("meta_title"); ?>
				</div>
				<div class="form-group">
					<label>Meta keywords</label>
					<?php echo $model->form->textAreaFor("meta_keywords"); ?>
				</div>
				<div class="form-group">
					<label>Meta description</label>
					<?php echo $model->form->textAreaFor("meta_description", ["rows"=>"3"]); ?>
				</div>
			</div>
      	</div>
   	</div>
  	<button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
	$(document).ready(function() {
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				var img = $("<img />");
				reader.onload = function (e) {
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'650');
					img.attr('height','500');
				};
				$(input).parent().parent().find('.preview-container').html(img);
				$(input).parent().parent().find('input[type="hidden"]').remove();

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
		});	

		$("input[name='name']").on('keyup',function(e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-');
			val = val.replace(/[-]+/g,'-');
			$("input[name='slug']").val(val.toLowerCase());
		});
	});
</script>