<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->press->id;?>" />
  <input type=hidden name="token" value="<?php echo get_token(); ?>" />
  <div class="row">
    <div class="col-md-24">
        <div class="box">
            <h4>General</h4>
            <div class="form-group">
                <label>Title</label>
                <?php echo $model->form->editorFor("title"); ?>
            </div>
            <div class="form-group">
                <label>Image</label>
                <p><small>(ideal image size is 1920 x 300)</small></p>
                <?php
                $img_path = "";
                if($model->press->featured_image != ""){
                    $img_path = UPLOAD_URL . 'press/' . $model->press->featured_image;
                }
                ?>
                <p><input type="file" name="featured_image" class='image' /></p>
                <?php if($model->press->featured_image != ""){ ?>
                    <div class="well well-sm pull-left">
                        <img src="<?php echo $img_path; ?>" width="100" />
                        <br />
                        <a href="<?= ADMIN_URL.'press/delete_image/'.$model->press->id.'/?featured_image=1';?>" class="btn btn-default btn-xs">Delete</a>
                        <input type="hidden" name="featured_image"
                               value="<?= $model->press->featured_image ?>"/></div>
                <?php } ?>
                <div id='preview-container'></div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <label>URL</label>
                <input name="url" type="text" id="url" value="<?=$model->press->url?>"/>
            </div>
            <div class="form-group">
                <label>Display Date</label>
                <?$date = $model->press->display_date ? strtotime($model->press->display_date): time()?>
                <input name="display_date" type="text" id="display_date" value="<?=date('Y-m-d',$date)?>"/>
            </div>
        </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer();?>
<link rel="stylesheet" type="text/css" href="<?=ADMIN_CSS?>jquery.datetimepicker.css">
<script src="<?=ADMIN_JS?>plugins/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'press/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('#product_select').on('change',function(){
            $('#url').val("<?=SITE_URL?>products/"+$(this).find(':selected').attr('data-slug'));
        });
        $('#display_date').datetimepicker({
            format: 'Y-m-d',
            timepicker:false
        });
    });
</script>