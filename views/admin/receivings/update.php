<? function receivingItemHtml(\Model\Receive_Item $obj = null, $receivingId = null, $key = 0)
{
    /** Object fills html elements with relevant Receive_Item values */
    $id = $obj ? $obj->id : '';
    $name = $obj ? $obj->name : ($receivingId ? \Model\Product::getItem(\Model\Receiving::getItem($receivingId)->product_id)->name: '');
    $sku = $obj ? $obj->sku : '';
    $status = $obj ? $obj->status : '';
    $inventory = $obj ? $obj->inventory : '';

    /* Block editing */
    if($obj){
        $op = $obj->order_product_id ? \Model\Order_Product::getItem($obj->order_product_id) : '';
    } else {
        $op = null;
    }

    $fulfilled = $id && $op;
    $receive_item_row = $fulfilled ? 'receive_item_row': '';
    $receive_item_block = $fulfilled ? '<div class="receive_item_block"></div>' : '';
    $receive_item_info = $fulfilled ? '<span class="receive_item_info">Item fulfilled by Order #'.$op->order_id.'</span>' : '';

    $html = '<div class="form-group" data-id="' . $id . '"><input type="hidden" name="items['.$key.'][id]" value="'.$id.'"><input name="items['.$key.'][receiving_id]" type="hidden" value="'.$receivingId.'"><div class="row '.$receive_item_row.' item-row">'.$receive_item_info.$receive_item_block;
    $html .= '<div class="col-sm-1"><label></label><input data-del="'.$id.'" type="checkbox" checked name="update[]"  value="'.$key.'" /></div>';
    $html .='<div class="col-sm-2"><label>#</label><input type="text" class="form-control item_id" value="'.($key+1).'" readonly/></div><div class="col-sm-5"><label>Name</label><input name="items['.$key.'][name]" type="text" value="' . $name . '"></div><div class="col-sm-5"><label>Serial# <small>(Required)</small></label><input class="serial-check" id="serial-'.$id.'" data-id="'.$id.'" name="items['.$key.'][sku]" type="text" value="' . $sku . '" class="serial-input"></div><div class="col-sm-5"><label>Status</label><select name="items['.$key.'][status]" class="form-control status-select">';
    foreach (\Model\Inventory_Status::getList(['orderBy' => 'display_order']) as $is) {
        $isSelected = $is->id == $status ? 'selected' : '';
        $html .= '<option value="' . $is->id . '" ' . $isSelected . '>' . $is->name . '</option>';
    }
    $html .= '</select></div><div class="col-sm-5"><label>Inventory</label><select name="items['.$key.'][inventory]" class="form-control inventory-select">';
    $html .= '<option disabled selected>Select Location</option>';
    foreach (\Model\Actual_Inventory::$places as $text){
        $isSelected = $text == $inventory ? 'selected': '';
        $html .= '<option value="'.$text.'" '.$isSelected.'>'.$text.'</option>';
    }
    foreach (\Model\Custom_Inventory::get_inventories() as $iid => $text){
        $isSelected = $iid == $inventory ? 'selected': '';
        $html .= '<option value="'.$iid.'" '.$isSelected.'>'.$text.'</option>';
    }
    $html .= '</select></div></div></div>';
    return $html;
} ?>
<style>
    .receive_item_row {
        position: relative;
    }
    .receive_item_block {
        position: absolute;
        background-color: black;
        opacity:.3;
        width:100%;
        height:100%;
        z-index:99;
    }
    .receive_item_info {
        visibility: hidden;
        width: 200px;
        bottom:100%;
        left:50%;
        margin-left:-100px;
        background-color: black;
        color: white;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;
        position: absolute;
        z-index: 100;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->receiving->id; ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab"
                                                      data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#items-tab" aria-controls="items" role="tab" data-toggle="tab">Items</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-16">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Product</label>
                                <select id="product-list" name="product_id" class="form-control" <?=$model->receiving->id ? 'disabled': ''?>>
                                    <option value="0">--SELECT--</option>
                                    <?foreach($model->products as $product){?>
                                        <option value="<?=$product['id']?>" <?=$model->receiving->product_id == $product['id'] ? 'selected': ''?>><?= str_replace(["\r","\n"],'',strReplace($product['name'], 100, '...')) ?></option>
                                    <?}?>
                                </select>
                                <?if($model->receiving->id){?>
                                    <input type="hidden" name="product_id" value="<?=$model->receiving->product_id?>"/>
                                <?}?>
                            </div>
                            <div class="form-group">
                                <label>Part No</label>
                                <?php echo $model->form->editorFor("part_no"); ?>
                            </div>
                            <div class="form-group">
                                <label>Ean</label>
                                <?php echo $model->form->editorFor("ean"); ?>
                            </div>
                            <div class="form-group">
                                <label>Upc</label>
                                <?php echo $model->form->editorFor("upc"); ?>
                            </div>
                            <div class="form-group">
                                <label>Pi Num</label>
                                <?php echo $model->form->editorFor("pi_num"); ?>
                            </div>
                            <div class="form-group">
                                <label>Pi Name</label>
                                <?php echo $model->form->editorFor("pi_name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Qty</label>
                                <?php $readonly = $model->receiving->qty > 0 ? ['disabled'=>'disabled']: [];
                                echo $model->form->textBoxFor("qty",$readonly); ?>
                                <?if($model->receiving->id){?>
                                    <input name="qty" value="<?=$model->receiving->qty?>" type="hidden">
                                <?}?>
                            </div>
                            <div class="form-group">
                                <label>Tracking No</label>
                                <?php echo $model->form->editorFor("tracking_no"); ?>
                                <button class="btn btn-primary" id="check-tracking">Check On Google</button>
                            </div>
                            <div class="form-group">
                                <label>Shipping Date</label>
                                <?$dateValue = $model->receiving->shipping_date ? strtotime($model->receiving->shipping_date): time()?>
                                <?php echo $model->form->textBoxFor("shipping_date",['type'=>'date','class'=>'form-control','value'=>date('Y-m-d',$dateValue)]); ?>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control" disabled>
                                    <? foreach (\Model\Inventory_Status::getList(['orderBy' => 'display_order']) as $item) { ?>
                                        <option
                                            value="<?= $item->id ?>" <?= $item->id == $model->receiving->status ? 'selected' : '' ?>><?= $item->name ?></option>
                                    <? } ?>
                                </select>
                                <input name="status" type="hidden" value="<?=$model->receiving->status ? : 0?>"/>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="items-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Items</h4>
<!--                            <button type="button" class="btn create_item">Create New</button>-->
                            <div class="receiving_items">
                                <div>
                                    <input type="text" placeholder="Search by Serial#" id="search-by-serial">
                                </div>
                                <div>
                                    <h3>Bulk updates</h3>
                                    <div>
                                        <label>Status</label>
                                        <select id="bulk-status">
                                            <option value="0">--SELECT--</option>
                                            <?php foreach (\Model\Inventory_Status::getList(['orderBy' => 'display_order']) as $is){ ?>
                                                <option value="<?=$is->id?>"><?=$is->name?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div>
                                        <label>Inventory</label>
                                        <select id="bulk-inventory">
                                            <option value="0">--SELECT--</option>
                                            <?php foreach (\Model\Actual_Inventory::$places as $text){ ?>
                                                <option value="<?=$text?>"><?=$text?></option>
                                            <?php } ?>
                                            <?php foreach (\Model\Custom_Inventory::get_inventories() as $iid => $text){ ?>
                                                <option value="<?=$iid?>" ><?=$text?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                    <? if($model->receiving->id) {?>
                                        <div>
                                            <input type="button" class="btn add_items" value="Add new Items">
                                            <input type="number" class="add_items_count" placeholder="# items" />
                                        </div>
                                        <div>
                                            <input type="button" class="btn delete-select" value="Delete Selected" />
                                        </div>
                                    <? } ?>
                                </div>
                                <?$receivingItems = \Model\Receive_Item::getList(['where' => 'receiving_id = ' . $model->receiving->id,'orderBy'=>'id asc']);
                                $itemsCount = count($receivingItems);
                                foreach ($receivingItems as $key=>$item) { ?>
                                    <?php echo receivingItemHtml($item,$model->receiving->id,$key)?>
                                <? } ?>
                                <?for ($i = $itemsCount; $i < $model->receiving->qty; $i++){
                                    echo receivingItemHtml(null,$model->receiving->id,$i);
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
    <button type="button" class="btn btn-save print-view" style="left: 500px;">Print View</button>
</form>


<?php footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.jquery.min.js"></script>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'receiving/');?>;
    $(document).ready(function () {
        $("#product-list").chosen();
        $("#product-list").on('change', function(e){
            e.preventDefault();
            $.post('/admin/receivings/get_details', {id: $(this).val()}, function(e){
                $('input[name="part_no"]').val(e.part_number);
                $('input[name="ean"]').val(e.ean);
                $('input[name="upc"]').val(e.upc);
            })
        });
        $('.btn.delete-select').click(function(){
            var to_del = [];
            if($("input[data-del]:checked").length == 0){
                alert("Select items to delete first");
                return true;
            }
            $("input[data-del]:checked").each(function(i,v){
               to_del.push($(v).data('del'));
            });
            var del_items = '('+to_del.join(',')+')';
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                $.ajax({
                    'type': 'POST',
                    'url': '/admin/receivings/delete_items',
                    'data': {
                        pwd: pwd,
                        items: del_items,
                        rec_id: <?=$model->receiving->id?>,
                        product_id: <?=$model->receiving->product_id?:0?>
                    },
                    success: function (data) {
                        data = JSON.parse(data);

                        if (data == "false") {
                            alert('Incorrect code!');
                        } else {
                            window.location.replace(data);
                        }
                    }
                })
            }
        });


        $("input.add_items").on('click',function() {
            var count = $('.add_items_count').val();
            if(!count){
                alert('Enter a valid value');
                return true;
            }
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                if (pwd != null) {
                    $.ajax({
                        'type': 'POST',
                        'url': '/admin/receivings/add_items',
                        'data': {
                            pwd: pwd,
                            count: count,
                            rec_id: <?=$model->receiving->id?>,
                            product_id: <?=$model->receiving->product_id?:0?>
                        },
                        success: function (data) {
                            data = JSON.parse(data);

                            if (data == "false") {
                                alert('Incorrect code!');
                            } else {
                                window.location.replace(data);
                            }
                        }
                    })
                }
            }
        });

        $('#check-tracking').on('click', function(e){
            e.preventDefault();
            var tracking = $('input[name="tracking_no"]').val();
            if(!tracking){
                alert('No tracking code.');
                return;
            }
            var strWindowFeatures = "location=yes,height=570,width=1280,status=yes";
            var URL = "https://www.google.com/search?q=" + tracking;
            window.open(URL, "_blank", strWindowFeatures);
        });

        $('.form').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $('#bulk-inventory').on('change', function(){
            var value = $(this).val();
            $('.inventory-select').each(function(){
                $(this).val(value);
            });
        });

        $('#bulk-status').on('change', function(){
            var value = $(this).val();
            $('.status-select').each(function(){
                $(this).val(value);
            });
        });

        $('#search-by-serial').on('keyup', function(e){
            e.preventDefault();
            var q = $(this).val();

            if(!q){
                // show all
                $('.item-row').show();
            } else {
                // hide all
                $('.item-row').hide();
                $('.serial-input').each(function(){
                    if($(this).val().toLowerCase().indexOf(q.toLowerCase()) !== -1){
                        $(this).parents('.item-row').show();
                    }
                });
            }

        });

        $('.serial-check').on('change',function(e){
           var id = $(this).data('id');
           var sku = $(this).val();
           $.post('/admin/receivings/ensure_unique', {id: id, sku: sku}, function(data){
               $('.serial-check').css('background-color','auto').removeClass('duplicate');
               if((data.rids.indexOf(id) != -1 && data.rids.length > 1) || (data.rids.length > 0 && data.rids.indexOf(id) == -1)){
                   $(this).css('background-color','red')
                   for(key in data.rids){
                       $('#serial-'+data.rids[key]).css('background-color','red').addClass('duplicate');
                       $('#serial-'+id).css('background-color','red').addClass('duplicate');
                   }
               }
           });
        });

        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('[name=qty]').on('blur',function(){
            var qty = $(this).val();
            $(this).after('<div class="qty_msg">Updating</div>');
            var items = $('.receiving_items');
            items.empty();
            for(var i = 0; i < qty; i++){
                var html = '<?=receivingItemHtml(null,$model->receiving->id)?>';
                html = html.replace(/items\[0\]/g,"items["+i+"]");
//                html = html.replace(/value="1"/g,"value=\""+(i+1)+"\"");
                var jHtml = $(html);
                jHtml.find('[name="items['+i+'][name]"]').val($('[name=product_id] :selected').text());
                jHtml.find('.item_id').val((i+1));
                items.append(jHtml);
            }
            $(this).parent().find('.qty_msg').remove()
            $('.serial-check').not('.listened').addClass('listened').keypress(function(e){
                if(e.which == 13) {
                    $('.serial-check').eq( $(this).index('.serial-check') + 1 ).focus();
                }
            });
        });
        function nextInDOM(_selector, _subject) {
                var next = getNext(_subject);
                while(next.length != 0) {
                    var found = searchFor(_selector, next);
                    if(found != null) return found;
                    next = getNext(next);
                }
                return null;
            }
        function getNext(_subject) {
            if(_subject.next().length > 0) return _subject.next();
            return getNext(_subject.parent());
        }
        function searchFor(_selector, _subject) {
            if(_subject.is(_selector)) return _subject;
            else {
                var found = null;
                _subject.children().each(function() {
                    found = searchFor(_selector, $(this));
                    if(found != null) return false;
                });
                return found;
            }
            return null; // will/should never get here
        }
        $('.serial-check').not('.listened').addClass('listened').keypress(function(e){
            if(e.which == 13) {
                $('.serial-check').eq( $(this).index('.serial-check') + 1 ).focus();
            }
        });
        $(document).on('click', '.save_item,.delete_item', function () {
            var el = $(this).closest('.form-group');
            var id = el.data('id');
            var type = $(this).data('type');
            var params = el.find('[name=receiving_id],[name=name],[name=sku],[name=status],[name=inventory]');
            var data = {id: id, type: type};
            params.each(function (i, ele) {
                data[$(ele).attr('name')] = $(ele).val();
            });
            data['product_id'] = <?=$model->receiving->product_id ? : 0?>;
            $.post('/admin/receivings/updateItem', data, function (ret) {
                var json = $.parseJSON(ret);
                console.log(json.status);
                if(type == 'delete'){
                    el.remove();
                }
//                $('[name=qty]').val($('[name=receiving_id]').size());
            })
        });
//        $('.create_item').on('click',function(){
//            $('.receiving_items').prepend('<?//=receivingItemHtml(null,$model->receiving->id)?>//');
//        });
        $('.print-view').on('click',function(){
            window.location.replace('/admin/receivings/print_view/<?=$model->receiving->id?>');
        });
        $('.receive_item_block').hover(function(){
            $(this).parent().find('.receive_item_info').css('visibility','visible');
        },function(){
            $(this).parent().find('.receive_item_info').css('visibility','hidden');
        })
    });
</script>