<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  	<input type="hidden" name="id" value="<?php echo $model->question->id;?>" />
	<div class="row">
		<div class="col-md-24">
			<div class="box">
				<h4>General</h4>
				 <div class="form-group">
					<label>Status</label>
					<?= $model->form->dropDownListFor('status', \Model\Question::$status); ?>
				</div>
				
				<div class="form-group">
					<label>Subject</label>
					<input type="text" value="<?=$model->question->subject?>" name="subject"/>
				</div>
				<div class="form-group">
					<label>Body</label>
					<textarea disabled="disabled"><?=$model->question->body?></textarea>
				</div>
				<div class="form-group">
					<label>Sender's Email</label>
					<input type="text" disabled="disabled" value="<?=$model->question->sender_email?>" />
				</div>
				<div class="form-group">
					<label>Product:</label>
					<p><a href="<?=ADMIN_URL?>products/update/<?=$model->question_product->id?>"><?=$model->question_product->name?> <i class="icon-eye"></i> </a></p>
				</div>
				<div class="form-group">
					<label>Answer:</label>
					<textarea name="answer"><?=$model->question->answer?></textarea> 
				</div>
<!--				<div class="form-group">-->
<!--					<label>Answers:</label>-->
<!--					--><?//$id=$model->question->id;?>
<!--					--><?//if(count(\Model\Question_Answer::getList(["where"=>"question_id='$id'"]))>0){?>
<!--					--><?//foreach (\Model\Question_Answer::getList(["where"=>"question_id='$id'"]) as $answer) {?>
<!--					<p>Time:--><?//=date("F j \a\\t\ g:ia", strtotime($answer->insert_time))?><!-- </p>-->
<!--					<p>Author:--><?//=\Model\Admin::getItem($answer->author_id)->first_name;?><!-- --><?//=\Model\Admin::getItem($answer->author_id)->last_name;?><!--</p>-->
<!--					<p>Text:--><?//=$answer->answer;?><!--</p>-->
<!--					<hr>-->
<!--					--><?//}?>
<!--					-->
<!--					--><?//}?>
<!--				</div>-->
			</div>
		</div>
	</div>
	<button type="submit" class="btn btn-save">Save</button>
</form>

<?= footer(); ?>

<script>
	
	$(function(){
		$('select[name="preanswer"]').change(function(){
			$('textarea[name="answer"]').val($('option:selected', $(this)).attr('data-text'));
		})
	})

</script>