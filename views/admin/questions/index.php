<?php if(count($model->questions)>0): ?>
    <div class="box box-table">
        <table id="data-list" class="table">
          <form action="<?=ADMIN_URL?>questions/multi_delete" method="POST">
        <button type="submit" class="btn btn-success">Multi remove</button>
              <thead>
                <tr>
                  <th width="2%"><input style="cursor:pointer;" name="edit-all" type="checkbox" /></th>
                    <th width="5%"> </th>
                	  <th width="15%">Date</th>
                  	<th width="20%">Subject</th>
                    <th width="30%">Product</th>
                    <th width="30%">Admin Replies</th>
                  	<th width="15%" class="text-center">Edit</th>
                  	<th width="15%" class="text-center">Delete</th>	
                </tr>
              </thead>
              <tbody><?$i=0;?>
               <?php foreach($model->questions as $obj){ ?>
                <tr>
                  <td>  <input style="cursor:pointer;" name="<?php echo $i++?>" type="checkbox" value="<?php echo $obj->id?>"   />    </td>
                    <td><? if (is_null($obj->viewed) || !$obj->viewed) { ?>
                   <button type="button" class="btn btn-success" disabled="disabled">New</button>
                   <? } ?></td>
                    <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?=$obj->insert_time?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?=$obj->subject?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?=$obj->product->name?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?=$obj->adminReply()?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i> 
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>questions/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i> 
                        </a>
                    </td>
               </tr>
               <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
          <div class='paginationContent'></div>
        </div>
    </div>
  </form>
<?php endif; ?>

<?php echo footer(); ?>

<script>
      $('input[name="edit-all"]').change(function(){
        if ($(this).prop('checked')){
          $('#data-list tr').each(function(){
            if ($(this).css('display') == 'table-row'){
              $('input[type="checkbox"]', $(this)).prop('checked', 'checked');
            }
          })
        } else {
          $('#data-list tr').each(function(){
            $('input[type="checkbox"]', $(this)).prop('checked', false);
          })
        }
      })
	var site_url = '<?= ADMIN_URL.'questions';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>