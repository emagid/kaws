<div class="row">
    <div class="col-md-12">
        <div class="box search-box">
            <div class="form-group">
                <label>Search</label>
                <div class="input-group">
                    <input id="search" type="text" name="search" class="form-control" placeholder="Search"/>
                    <span class="input-group-addon">
                        <i class="icon-search"></i>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label>Range</label>
                <div class="input-group">
                    <input id="range" type="text" name="range" class="form-control" value="<?= !$model->start || !$model->end ? '': date('Y-m-d', strtotime($model->start)) . ' - ' . date('Y-m-d', strtotime($model->end)) ?>"/>
                    <span class="input-group-addon remove-date" style="cursor: pointer;">
                        <i class="icon-cancel-circled"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box transparent_box">
            <div class="form-group">
                <label>Show on page:</label>
                <select class="how_many form-control" name="how_many">
                    <? $arr = [10, 50, 100, 500, 1000];
                    foreach ($arr as $a) {?>
                        <option value="<?= $a ?>" <?= isset($_GET['how_many']) && $_GET['how_many'] == $a ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Status Filter</label>
                <select class="status_filter form-control" name="status_filter">
                    <? $pFilter = [-1=>'All Orders','inc'=>'Incomplete','comp'=>'Complete'];
                    foreach ($pFilter as $k=>$a) {?>
                        <option value="<?= $k ?>" <?= isset($_GET['sf']) && $_GET['sf'] == $k ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
            </div>
            <!--<div class="form-group">
                <label>Payment Filter</label>
                <select class="payment_filter form-control" name="payment_filter">
                    <option value="-1" <?/*= !isset($_GET['filter']) ? 'selected' : '' */?>>No Filter</option>
                    <?/* $pFilter = ['onl'=>'Online','sto'=>'Store','inv'=>'Invoice'];
                    foreach ($pFilter as $k=>$a) {*/?>
                        <option value="<?/*= $k */?>" <?/*= isset($_GET['filter']) && $_GET['filter'] == $k ? 'selected' : '' */?>><?/*= $a */?></option>
                    <?/* } */?>
                </select>
            </div>-->
        </div>
    </div>
<!--    <div class="col-md-8">-->
<!--        <div class="box transparent_box">-->
<!--        </div>-->
<!--        <div class="box transparent_box" style="align-content: center">-->
<!--            <a href="--><?//= ADMIN_URL ?><!--orders/export" class="btn btn-warning" style="width: 49%">Export</a>-->
<!--            <a class="btn btn-warning mass-email" style="width: 49%">Mass Email</a>-->
<!--        </div>-->
<!--    </div>-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script>
    $(document).ready(function(){

        function getQueryObj(str) {
            return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
        }

        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            var queryString;
            if(location.search && how_many != -1){
                queryString = getQueryObj(location.search);
                queryString.how_many = how_many;
            } else if(how_many == -1){
                queryString = getQueryObj(location.search);
                delete queryString.how_many;
            } else {
                queryString = {how_many:how_many};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/back_orders'+qs);
        });

        $('body').on('change', '.status_filter', function () {
            var status_filter = $(this).val();
            var queryString;
            if(location.search && status_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.sf = status_filter;
            } else if(status_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.sf;
            } else {
                queryString = {sf:status_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/back_orders'+qs);
        });
        $('#range').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo $model->start ? : date('Y-m-d')?>",
            endDate: "<?php echo $model->end ? : date('Y-m-d')?>"
        },function(start,end){
            end.hour(23);end.minute(59);end.second(59);
            var queryString = getQueryObj(location.search);
            queryString.t = start.unix()+','+end.unix();
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            window.location.replace('<?=ADMIN_URL.'back_orders'?>'+'?'+ $.param(queryString));
        });

        $('.remove-date').on('click',function(){
            var queryString = getQueryObj(location.search);
            if(queryString.t ) {
                delete queryString.t;
            }
            window.location.replace('<?=ADMIN_URL.'back_orders'?>'+'?'+ $.param(queryString));
        });
    })
</script>
<?php if (count($model->back_orders) > 0) { ?>
    <div class="box box-table horizontal-scrollable-table">
        <table class="table" id="data-list">
            <thead>
            <tr>
                <th width='1%'>Id</th>
                <th width='20%'>Product</th>
                <th width='5%'>Part No</th>
                <th width='5%'>Ean</th>
                <th width='5%'>Upc</th>
                <th width='5%'>Pi Num</th>
                <th width='5%'>Qty Request</th>
                <th width='5%'>Qty Receive</th>
                <th width='5%'>Status</th>
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->back_orders as $obj) { ?>
                <tr class="originalProducts">
                    <td><a href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->product_id; ?>"><?php echo \Model\Product::getItem($obj->product_id)->name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>"><?php echo $obj->part_no; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>"><?php echo $obj->ean; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>"><?php echo $obj->upc; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>"><?php echo $obj->pi_num; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>"><?php echo $obj->qty_request; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>"><?php echo $obj->qty_receive; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>"><?php echo \Model\Inventory_Status::getItem($obj->status)->name; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions-edit btn-solid-primary" href="<?php echo ADMIN_URL; ?>back_orders/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black"
                           href="<?php echo ADMIN_URL; ?>back_orders/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'back_orders';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>back_orders/search";
            var keywords = $(this).val();
            if (keywords.length > 2) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    $.each(list,function(i,list){
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>back_orders/update/' + list.id).html(list.id));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list.product_id).html(list.product_name));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>back_orders/update/' + list.id).html(list.part_no));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>back_orders/update/' + list.id).html(list.ean));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>back_orders/update/' + list.id).html(list.upc));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>back_orders/update/' + list.id).html(list.pi_num));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>back_orders/update/' + list.id).html(list.qty_request));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>back_orders/update/' + list.id).html(list.qty_receive));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>back_orders/update/' + list.id).html(list.status));
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>products/update/' + list.id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');

                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black').prop('href', '<?= ADMIN_URL;?>products/delete/' + list.id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');
                        tr.appendTo($("#data-list tbody"));
                    });
                    $('input[name=multibox]').prop('checked',false);
                    $('#check-all').prop('checked',false);

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>