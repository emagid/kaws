<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->back_order->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Product Id</label>
                    <select name="product_id" class="form-control">
                        <?foreach($model->products as $product){?>
                            <option value="<?=$product['id']?>" <?=$model->back_order->product_id == $product['id'] ? 'selected': ''?>><?= str_replace(["\r","\n"],'',strReplace($product['name'], 100, '...')) ?></option>
                        <?}?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Part No</label>
                    <?php echo $model->form->editorFor("part_no"); ?>
                </div>
                <div class="form-group">
                    <label>Ean</label>
                    <?php echo $model->form->editorFor("ean"); ?>
                </div>
                <div class="form-group">
                    <label>Upc</label>
                    <?php echo $model->form->editorFor("upc"); ?>
                </div>
                <div class="form-group">
                    <label>Pi Num</label>
                    <?php echo $model->form->editorFor("pi_num"); ?>
                </div>
                <div class="form-group">
                    <label>Qty Request</label>
                    <?php echo $model->form->editorFor("qty_request"); ?>
                </div>
                <div class="form-group">
                    <label>Qty Receive</label>
                    <?php echo $model->form->editorFor("qty_receive"); ?>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control">
                        <?foreach(\Model\Inventory_Status::getList(['orderBy'=>'display_order']) as $value){?>
                            <option value="<?=$value->id?>" <?=$value->id == $model->back_order->status ? 'selected': ''?>><?=$value->name?></option>
                        <?}?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'back_order/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>