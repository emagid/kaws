<div class="row">
    <div class="col-md-8">
        <div class="box search-box">
            <div class="form-group">
                <label>Search</label>
                <div class="input-group">
                    <input id="search" type="text" name="search" class="form-control" placeholder="Search"/>
                    <span class="input-group-addon">
                        <i class="icon-search"></i>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label>Range</label>
                <div class="input-group">
                    <input id="range" type="text" name="range" class="form-control" value="<?= !$model->start || !$model->end ? '': date('Y-m-d', strtotime($model->start)) . ' - ' . date('Y-m-d', strtotime($model->end)) ?>"/>
                    <span class="input-group-addon remove-date" style="cursor: pointer;">
                        <i class="icon-cancel-circled"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="box transparent_box">
            <div class="form-group">
                <label>Show on page:</label>
                <select class="how_many form-control" name="how_many">
                    <? $arr = [10, 50, 100, 500, 1000];
                    foreach ($arr as $a) {?>
                        <option value="<?= $a ?>" <?= isset($_GET['how_many']) && $_GET['how_many'] == $a ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Status Filter</label>
                <select class="status_filter form-control" name="status_filter">
                    <? $pFilter = [-1=>'Default','all'=>'All Orders','inc'=>'Incompleted'];
                    foreach ($pFilter as $k=>$a) {?>
                        <option value="<?= $k ?>" <?= isset($_GET['sf']) && $_GET['sf'] == $k ? 'selected' : '' ?>><?= $a ?></option>
                    <? } ?>
                </select>
            </div>
        </div>
    </div>
<!--    <div class="col-md-8">-->
<!--        <div class="box transparent_box" style="align-content: center">-->
<!--            <a href="--><?//= ADMIN_URL ?><!--campaigns/export" class="btn btn-warning" style="width: 49%">Export</a>-->
<!--        </div>-->
<!--    </div>-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
    $(document).ready(function () {
        function getQueryObj(str) {
            return (str || document.location.search).replace(/(^\?)/,'').split("&").map(function(n){return n = n.split("="),this[n[0]] = n[1],this}.bind({}))[0];
        }

        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            var queryString;
            if(location.search && how_many != -1){
                queryString = getQueryObj(location.search);
                queryString.how_many = how_many;
            } else if(how_many == -1){
                queryString = getQueryObj(location.search);
                delete queryString.how_many;
            } else {
                queryString = {how_many:how_many};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/campaigns'+qs);
        });

        $('body').on('change', '.status_filter', function () {
            var status_filter = $(this).val();
            var queryString;
            if(location.search && status_filter != -1){
                queryString = getQueryObj(location.search);
                queryString.sf = status_filter;
            } else if(status_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.sf;
            } else {
                queryString = {sf:status_filter};
            }
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            var qs = $.param(queryString) ? '?'+$.param(queryString): '';
            window.location.replace('/admin/campaigns'+qs);
        });

        $('body').on('change', '.sort', function () {
            var status = $(this).val();
            var multipleStatus = status.join(',').replace(/,/g, '%2c');
            if (status == "all") {
                window.location.href = '/admin/campaigns';
            }
            else {
                window.location.href = '/admin/campaigns?status_show=' + multipleStatus;
            }
        });

        $('#range').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo $model->start ? : date('Y-m-d')?>",
            endDate: "<?php echo $model->end ? : date('Y-m-d')?>"
        },function(start,end){
            end.hour(23);end.minute(59);end.second(59);
            var queryString = getQueryObj(location.search);
            queryString.t = start.unix()+','+end.unix();
            $.each(queryString,function(i,e){
                queryString[i] = decodeURIComponent(e);
            });
            window.location.replace('<?=ADMIN_URL.'campaigns'?>'+'?'+ $.param(queryString));
        });

        $('.remove-date').on('click',function(){
            var queryString = getQueryObj(location.search);
            if(queryString.t ) {
                delete queryString.t;
            }
            window.location.replace('<?=ADMIN_URL.'campaigns'?>'+'?'+ $.param(queryString));
        });

        $('body').on('change', '#range', function () {
            var pay_filter = $(this).val();
            var queryString;
            if(location.search && pay_filter != -1){
                queryString = getQueryObj(location.search);
                $.each(queryString,function(i,e){
                    queryString[i] = decodeURIComponent(e);
                });
                queryString.filter = pay_filter;
            } else if(pay_filter == -1){
                queryString = getQueryObj(location.search);
                delete queryString.filter;
            } else {
                queryString = {filter:pay_filter};
            }
            window.location.replace('/admin/campaigns?'+ $.param(queryString));
        });
    });
</script>
<?php if (count($model->campaigns) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="1%"><input type="checkbox" id="check-all"/></th>
                <th width="1%">
                    <?php
                    if (isset($_GET['sort']) && ($_GET['sort'] == 'id')) {

                        if ($_GET['sort'] == 'id' && $_GET['sort_param'] == 'asc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=id&sort_param=desc">ID</a>';
                        } elseif ($_GET['sort'] == 'id' && $_GET['sort_param'] == 'desc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=id&sort_param=asc">ID</a>';
                        }
                    } else {
                        echo '<a style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=id&sort_param=desc">ID</a>';
                    } ?>
                </th>
                <th width="3%">
                    <?php
                    if (isset($_GET['sort']) && ($_GET['sort'] == 'insert_time')) {
                        if ($_GET['sort'] == 'insert_time' && $_GET['sort_param'] == 'asc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=insert_time&sort_param=desc">Date</a>';
                        } elseif ($_GET['sort'] == 'insert_time' && $_GET['sort_param'] == 'desc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=insert_time&sort_param=asc">Date</a>';
                        }
                    } else {
                        echo '<a style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=insert_time&sort_param=desc">Date</a>';
                    } ?>
                </th>
                <th width="5%">Name</th>
                <th width="5%">
                    <?php
                    if (isset($_GET['sort']) && ($_GET['sort'] == 'status')) {
                        if ($_GET['sort'] == 'status' && $_GET['sort_param'] == 'asc') {
                            echo ' <a style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=status&sort_param=desc">Status</a>';
                        } elseif ($_GET['sort'] == 'status' && $_GET['sort_param'] == 'desc') {
                            echo ' <a  style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=status&sort_param=asc">Status</a>';
                        }
                    } else {
                        echo '<a style="color:rgba(27, 27, 32, 0.74);" href="/admin/campaigns?sort=status&sort_param=desc">Status</a>';
                    } ?>
                <th width="10%">Products</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->campaigns as $obj) {
                    $style = '';
                ?>
                <tr class="originalProducts" <?= $style ?>>
                    <td>
                        <input type="checkbox" name="multibox" value="<?=$obj->id?>"/>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>campaigns/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>campaigns/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->insert_time; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>campaigns/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->name; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>campaigns/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->status; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>campaigns/update/<?php echo $obj->id; ?>">
                            <? if (($products = $obj->getProducts())) {
                                foreach ($products as $product) {
                                    echo $product->name;
                                    echo '<br/>';
                                }
                            } ?>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } else {
    echo "<center>No matches!<br><a href='' onclick='history.go(-1)'>Go back!</a></center>";
} ?>

<?php footer(); ?>

<script>
    var site_url = '/admin/campaigns?<?if (isset($_GET)){unset($_GET['page']); echo urldecode(http_build_query($_GET).'&');}?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>campaigns/search";
            var keywords = $(this).val();
            if (keywords.length > 2) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    $.each(list,function(i,list){
                        if($.inArray(list.status,['Paid', 'Shipped', 'Complete', 'Delivered']) != -1){
                            var color = 'green';
                        } else if($.inArray(list.status,['Declined', 'Canceled', 'Incomplete PayPal']) != -1){
                            color = '#D94F74';
                            return true;
                        } else {
                            color = 'black'
                        }
                        if(list.payment_method == 2){
                            var total = '<button type="button" class="btn btn-success" style="position:relative;" disabled="disabled">$'+parseFloat(list.total).toFixed(2)+'</button>';
                        } else {
                            total = '$'+parseFloat(list.total).toFixed(2);
                        }
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html('<input type="checkbox" name="multibox" value="'+list.id+'"/>');
                        $('<td />').appendTo(tr).html(list.viewed ? '<button type="button" class="btn btn-success" disabled="disabled">New</button>' : '');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>campaigns/update/' + list.id).html(list.id));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>campaigns/update/' + list.id).html(list.insert_time));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>campaigns/update/' + list.id).html(list.tracking_number));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>campaigns/update/' + list.id).css('color',color).html(list.status+'<br>('+list.payment_status+')'));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>campaigns/update/' + list.id).html(list.bill_name));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>campaigns/update/' + list.id).html(list.products.join('<br>')));

                        $('<td />').appendTo(tr).html(total);
                        tr.appendTo($("#data-list tbody"));
                    });
                    $('input[name=multibox]').prop('checked',false);
                    $('#check-all').prop('checked',false);

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    });
    $('#check-all').on('click',function(){
        if($(this).is(':checked')){
            $('input[name=multibox]:visible').prop('checked',true);
        } else {
            $('input[name=multibox]:visible').prop('checked',false);
        }
    });
    $(function () {
        $('input[name="daterange"]').daterangepicker({
            timePicker: false,
            format: 'YYYY-MM-DD',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            startDate: "<?php echo \Carbon\Carbon::now()->subWeek();?>",
            endDate: "<?php echo \Carbon\Carbon::now();?>"
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("#search_by_mpn").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>campaigns/search_by_mpn";
            var keywords = $.trim($(this).val());
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {

                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html();
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>campaigns/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                        $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html(list[key].products);
                        $('<td />').appendTo(tr).html("$" + list[key].total);
                        /*   var editTd = $('<td />').addClass('text-center').appendTo(tr);
                         var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '
                        <?= ADMIN_URL;?>campaigns/update/' + list[key].id);
                         var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');*/
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>

