<style type="text/css">


    @media print {
        /* Стиль для печати */
        h1, h2, p {
            color: #000; /* Черный цвет текста */
        }

        .nav {
            display: none;
        }

        .btn {
            display: none;
        }

        .qqq {
            color: black;
        }

        #general-tab, #billing-info-tab, #shipping-info-tab, #products-tab {
            display: block;
            visibility: visible;
        }

        .form-group {
            margin-bottom: 3px;
        }

        a {
            border: 0;
            text-decoration: none;
        }

        input[type="text"] {
            border-color: white;
        }

        .form-control {
            border: 2px solid blue;
        }

        a img {
            border: 0
        }

        a:after {
            content: " (" attr(href) ") ";
            font-size: 90%;
        }

        a[href^="/"]:after {
            content: " ";
        }
    }
    #entrant-list .winner{
        background: #00a65a;
    }
    #entrant-list .redeemed{
        background: yellowgreen;
    }
    #entrant-list .loser{
        background: grey;
    }
</style>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->campaign->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="tab-content">
        <center style="    margin-bottom: 112px;">
            <button type="submit" style="z-index: 999" id="save_close" class="btn btn-save">Save and close</button>
            <button type="submit" style="z-index: 999" id="save" class="btn btn-save">Save</button>
            <a class="btn btn-save btn-grey" id="delete">Delete</a>
        </center>
    </div>
    <div role="tabpanel">
        <p class="qq" style=" text-align:center; color:#F9F9F9;">CAMPAIGN #<?php echo $model->campaign->id; ?></p>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?=($tab == 'general-tab') || !isset($tab)?"active":""?>"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation" class="<?=($tab == 'terms-tab')?"active":""?>"><a href="#terms-tab" aria-controls="terms" role="tab" data-toggle="tab">Terms</a></li>
            <? if($model->campaign->type == 'Lottery') {?>
                <li role="presentation" class="<?=($tab == 'lotto-tab')?"active":""?>"><a href="#lotto-tab" aria-controls="lotto" role="tab" data-toggle="tab">Lottery Entries</a></li>
            <? } ?>
        </ul>
        <div class="tab-content">

                <div role="tabpanel" class="tab-pane <?=($tab == 'general-tab') || !isset($tab)?"active":""?>" id="general-tab">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="box">
                                <h4>General</h4>
                                <?php if($model->campaign->id){ ?>
                                    <div class="form-group">
                                        <label>ID Campaign</label>
                                        <p>#<?= $model->campaign->id ?> </p>
                                    </div>
                                <?php } else { ?>
                                    <div class="form-group">
                                        <label>New Campaign</label>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label>Name</label>
                                    <?php echo $model->form->editorFor("name"); ?>
                                </div>
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="change form-control campaign_type" name="type">
                                        <?php $current_type = $model->campaign->type; ?>
                                        <? foreach (\Model\Campaign::$type as $key => $stat) { ?>
                                            <option <? if ($current_type == $stat) {
                                                echo "selected";
                                            } ?> value="<?= $stat ?>"><?= $stat ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-20">
                                        <div class="form-group">
                                            <label>Automatic start/stop time</label>
                                            <input type="text" name="daterange"
                                                   value="<?php echo ($model->campaign->end_time != 0) ? date("m/d/Y g:iA", $model->campaign->start_time) . ' - ' . date("m/d/Y g:iA", $model->campaign->end_time) : ""; ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Disable</label>
                                            <input type="checkbox" name="off_time" value="1" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="campaign_limit standard" style="<?=$model->campaign->type == 'Standard'?'':'display:none'?>">Max per user</label>
                                    <label class="campaign_limit lottery" style="<?=$model->campaign->type == 'Lottery'?'':'display:none'?>">Amount of Winners</label>
                                    <label class="campaign_limit auction" style="<?=$model->campaign->type == 'Auction'?'':'display:none'?>">Max Bid</label>
                                    <?=$model->form->editorFor('c_limit')?>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <? sort(\Model\Campaign::$status, SORT_LOCALE_STRING); ?>
                                    <select class="change form-control" name="status">
                                        <?php $current_status = $model->campaign->status; ?>
                                        <? foreach (\Model\Campaign::$status as $key => $stat) { ?>
                                            <option <? if ($current_status == $stat) {
                                                echo "selected";
                                            } ?> value="<?= $stat ?>"><?= $stat ?></option>
                                        <? } ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <?php $allProducts = \Model\Product::getList();?>
                        <div class="add-product-template" style="display: none">
                            <div class="form-group">
                                <label>Products</label>
                                <select class="change form-control product-select">
                                    <? foreach ($allProducts as $product) { ?>
                                        <option value="<?= $product->id ?>"><?= $product->name.' - '.$product->color ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input class="product-quantity" type="number">
                            </div>
                            <div class="form-group">
                                <label>Max per user</label>
                                <input class="max_per_user" type="number">
                            </div>
                            <!--<div class="form-group">
                                <label>Notify Level</label>
                                <input class="product-notify-level" type="number">
                            </div>-->
                            <button class="remove-product">Remove</button>
                        </div>
                        <div class="col-md-12">
                            <div class="box product-list">
                                <h4>Products</h4>
                                <button class='btn btn-success' id="add-more-products">Add more products</button>
                                <?php if($model->cProducts) { ?>
                                <? foreach ($model->cProducts as $cp) { $product = \Model\Product::getItem($cp->product_id);?>
                                    <div class="box product-item">
                                        <input type="hidden" name="cProductId[]" value="<?=$cp->id?>">
                                        <div class="form-group">
                                            <label>Products</label>
                                            <select class="change form-control" name="productId[]">
                                                <? foreach ($allProducts as $dbProduct) { ?>
                                                    <option <? if ($dbProduct->id == $product->id) {
                                                        echo "selected";
                                                    } ?> value="<?= $dbProduct->id ?>"><?= $dbProduct->name.' - '.$dbProduct->color ?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Quantity</label>
                                            <input name="productQuantity[]" value="<?=$cp->quantity?>" type="number">
                                        </div>
                                        <div class="form-group">
                                            <label>Max per user</label>
                                            <input name="max_per_user[]" value="<?=$cp->max_per_user?>" type="number">
                                        </div>
                                        <!--<div class="form-group">
                                            <label>Notify Level</label>
                                            <input name="productNotifyLevel[]" value="<?/*=$cp->notify_level*/?>" type="number">
                                        </div>-->
                                        <button class="remove-product">Remove</button>
                                    </div>
                                <? } ?>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane <?=($tab == 'terms-tab')?"active":""?>" id="terms-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <h4>
                                    Terms for this Campaign
                                </h4>
                                <div class="form-group">
                                    <label>Terms</label>
                                    <p style='margin: 20px 0;'>Make sure to tag the main title as an h1. Any title after that are h2s. No other level headings should be assigned ( h3, h4... ). Watch for any awkward spacing and assigning headings to blank spaces.</p>
                                    <?=$model->form->textAreaFor('terms')?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane <?=($tab == 'lotto-tab')?"active":""?>" id="lotto-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <h4>
                                    Lotto Entries
                                    <span style="float: right"><?=$model->redeemed_count?> Winning Entries Claimed </span>
                                    <span style="float: right"><?=$model->winners_count?> Winners Selected |&nbsp;</span>
                                    <span style="float: right"><?=$model->entries_count?> Entrants |&nbsp;</span>
                                </h4>
                                <div class="row">
                                    <div class="form-group col-md-10">
                                        <a class="select_winners btn" href="/admin/campaigns/select_winners/<?=$model->campaign->id?>">Select Winners</a>
                                    </div>
                                    <div class="form-group col-md-10">
                                        <a class="send_emails btn button">Send Emails</a>
                                    </div>
                                </div>
                                <div class="box box-table">
                                    <table id='entrant-list' class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <select id="entrant_filter">
                                                        <option value="">All</option>
                                                        <option value="winner">Winners</option>
                                                        <option value="redeemed">Claimed</option>
                                                        <option value="unredeemed">Unclaimed</option>
                                                        <option value="loser">Losers</option>
                                                    </select>
                                                </th>
                                                <th colspan="9">
                                                    <a class="set_status btn button">
                                                        Apply status
                                                    </a>
                                                    <select id='winStatus'>
                                                        <option value="0">Loser</option>
                                                        <option value="1">Winner</option>
                                                    </select>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th width="10%">
                                                    <input type='checkbox' id="multi-entrant" />
                                                </th>
                                                <th width="13%">
                                                    Email
                                                </th>
                                                <th width="13%">
                                                    Name
                                                </th>
                                                <th width="13%">
                                                    Address
                                                </th>
                                                <th width="13%">
                                                    Product
                                                </th>
                                                <th width="10%">
                                                    Status
                                                </th>
                                                <th width="13%">
                                                    Redemption Link
                                                </th>
                                                <th width="13%">
                                                    Entry Date
                                                </th>
                                                <th width="13%">
                                                    Product claimed date
                                                </th>
                                                <th class='last_email' width="13%">
                                                    Date of last Email sent out
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody id="entries">
                                            <? foreach ($model->entries AS $entry) { ?>
                                                <? $user = $entry->user()?>
                                                <?
                                                    $status = '';

                                                    if(($model->campaign->end_time != '' && strtotime($carbon = \Carbon\Carbon::now()) > $model->campaign->end_time ) || $model->campaign->status == 'Over' ) {
                                                        if($entry->winner){
                                                            $status = 'winner';
                                                        } else {
                                                            $status ='loser';
                                                        }
                                                    }

                                                    if($entry->email_date == ''){
                                                        $email_date = '';
                                                    } else {
                                                        $email_date = new DateTime($entry->email_date);
                                                        $email_date = $email_date->format('F d, Y h:i a');
                                                    }
                                                    if($entry->claimed_date == ''){
                                                        $order_link = '';
                                                    } else {
                                                        $order = \Model\Order::getItem(null,['where'=>"entry_id = $entry->id"]);
                                                        $claim_date = date("F d, Y g:i a",strtotime($entry->claimed_date));
                                                        $order_link = "<a href='/admin/orders/update/$order->id'>$claim_date</a>";
                                                    }
                                                    $lproduct = \Model\Product::getItem($entry->product_id);
                                                    if($lproduct == null) {
                                                        \Model\Lotto_Entry::delete($entry->id);
                                                        continue;
                                                    }
                                                    $url = '';
                                                    $img_path = "/content/uploads/products/".$lproduct->featuredImage();
                                                    if(strpos($img_path,"amazonaws") !== false) $img_path = $lproduct->featuredImage();

                                                ?>
                                                <tr class="entrant <?=$status ?> <? if($entry->winner) { echo($entry->redeemed?'redeemed':'unredeemed'); }?>">
                                                    <td><input type="checkbox" name="email[<?=$entry->id?>]" value="<?=$entry->id?>"/></td>
                                                    <td><?=$user->email?></td>
                                                    <td><?=$user->full_name()?></td>
                                                    <td>
                                                        <?
                                                           if(($addr = $user->getAddress()) && $addr) {?>
                                                            <?=$addr->formatted()?>
                                                        <? } ?>
                                                    </td>
                                                    <td><img width="50" alt="<?=$lproduct->name?>" src="<?=$img_path?>" /></td>
                                                    <td><?=ucfirst($entry->redeemed?'redeemed':$status)?></td>
                                                    <? $url = "https://".$_SERVER['SERVER_NAME'].'/redeem/'.$entry->ref_num ?>
                                                    <td><? if($entry->ref_num && $entry->ref_num != '') {?>
                                                            <a href="<?=$url?>"><?=$url?></a>
                                                        <? } ?>
                                                    </td>
                                                    <td><?=date("F d, Y g:i a",strtotime($entry->insert_time)-(4*3600))?></td>
                                                    <td><?=$order_link?></td>
                                                    <td><?=$entry->email_date?date("F d, Y g:i a",strtotime($entry->email_date)):''?></td>
                                                </tr>
                                            <? } ?>
                                        </tbody>
                                    </table>
                                    <div class="box-footer clearfix">
                                        <div class='paginationContent'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="orders/">
    <!--  <center style="    margin-bottom: 112px;"><button type="submit" id="save_close" class="btn btn-save">Save and close</button>  <button type="submit" id="save" class="btn btn-save">Save</button>  <button class="btn btn-save" id="print">Print</button> <a href="/admin/orders/print_packing_slip/-->
    <? //=$model->order->id?><!--"  class="btn btn-save">Print packing slip</a></center> -->
</form>
<?php footer(); ?>
<!--<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>-->
<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>
<script>
    var site_url = '';
    var total_pages = <?= ceil($model->entries_count/20)?>;
    var page = <?= $model->page ?>;
    $(function () {
        $('div.paginationContent').pagination({
            pages: <?= ceil($model->entries_count/20)?>,
            currentPage: page,
            cssStyle: 'light-theme',
            onPageClick: function (pageNumber, event) {
                $('tbody#entries').html('<tr><td colspan=9><h1>loading...</hi></td></tr>');

                $.post('<?=ADMIN_URL?>campaigns/queriesPage',{campaign: <?=$model->campaign->id?:0?>,page:pageNumber},function(data){
                   $('tbody#entries').html(data.html);
                });
            }
        });
    });
</script>
<script>


    $(function () {
        $('input[name="daterange"]').daterangepicker({
            timePicker: true,
            format: 'MM/DD/YYYY h:mmA',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            <?php if($model->campaign->id > 0) { ?>
            startDate: "<?php echo date("m/d/Y g:iA",$model->campaign->start_time);?>",
            endDate: "<?php echo date("m/d/Y g:iA",$model->campaign->end_time);?>",
            <?php } ?>

        });

        $("input[name=off_time]").change(function(){
           if($(this).is(':checked')){
               $('input[name=daterange]').val('').attr('readonly',true);
           } else {
               $('input[name=daterange]').attr('readonly',false);
           }
        });

        $('select.campaign_type').change(function(e){
           var ctype = this.value.toLowerCase();
           $('.campaign_limit').hide();
           $('.campaign_limit.'+ctype).show();
        }).change();

        $('#entrant_filter').change(function (e) {
           var filterClass = this.value;
           if(this.value == ''){
               $('#entrant-list tbody tr').show();
           } else {
               $('#entrant-list tbody tr').hide();
               $('#entrant-list tbody tr.'+filterClass).show();
           }
           selectAllCheck();
        });

        $('#multi-entrant').change(function (e) {
           if($(this).is(':checked')){
                $('#entrant-list tbody tr td input:visible').prop('checked',true);
            } else {
                $('#entrant-list tbody tr td input:visible').prop('checked',false);
            }
        });

        $('#entrant-list tbody tr td input').change(function(e){
           selectAllCheck();
        });

        $('.send_emails.btn').click(function(e){
            var entries = [];
            $('#entrant-list tbody tr td input:checked').each(function(i,v){
                entries.push(this.value);
            });

            $.post('/admin/campaigns/email',{emails:entries,campaign:<?=$model->campaign->id?>},function(data){
               if(data.status){
                   window.location.reload();
               }
            });
        });

        $('.set_status.btn').click(function(e){
            var entries = [];
            var status = $('select#winStatus').val();
            $('#entrant-list tbody tr td input:checked').each(function(i,v){
                entries.push(this.value);
            });

            $.post('/admin/campaigns/set_status',{entries:entries,status:status},function(data){
                if(data.status){
                    window.location.reload();
                }
            });
        });

        function selectAllCheck(){
            var all      = $('#entrant-list tbody tr td input:visible').length;
            var checked  = $('#entrant-list tbody tr td input:visible:checked').length;
            if(all == checked){
                $('#multi-entrant').prop('checked',true);
            } else {
                $('#multi-entrant').prop('checked',false);
            }
        }

        $('#add-more-products').on('click', function(e){
            e.preventDefault();
            var template = $('.add-product-template')
                .clone()
                .show()
                .removeClass('add-product-template')
                .addClass('product-item');
            template.find('.product-select').attr('name', 'productId[]');
            template.find('.product-quantity').attr('name', 'productQuantity[]');
            template.find('.product-notify-level').attr('name', 'productNotifyLevel[]');
            console.log(template);
            $('.product-list').append(template);
        });

        $(document).on('click', '.remove-product', function(e){
            e.preventDefault();
            $(this).parents('.product-item').remove();
        });
//        $('#delete').click(function () {
//            var pwd = prompt("Please enter the code");
//            if (pwd != null) {
//                $.ajax({
//                    'type': 'POST',
//                    'url': '/admin/orders/unlock',
//                    'data': {
//                        pwd: pwd,
//                        orderId: <?//=$model->campaign->id?>
//                    },
//                    success: function (data) {
//                        data = JSON.parse(data);
//
//                        if (data == "false") {
//                            alert('Incorrect code!');
//                        } else {
//                            window.location.replace(data);
//                        }
//                    }
//                })
//            }
//
//
//        });

    });
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>