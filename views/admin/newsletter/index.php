<?php if (count($model->newsletters) > 0) { ?>
<!--    <div class="box">-->
<!--        <div class="form-group">-->
<!--            <label><a href="/admin/newsletter/mass_email">Email Controller</a></label>-->
<!--        </div>-->
<!--    </div>-->
    <div class="box transparent_box">
        <div class="form-group">
            <a href="/admin/newsletter/export" class="underlineBtn underlineBtn-blue underlineBtn-blue-black">Export</a>
        </div>
    </div>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="20%">Date/Time</th>
                <th width="20%">Email</th>
                <th width="1%">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->newsletters as $newsletter): ?>
                <tr>
                    <td><?php echo date('F d,Y h:ia',strtotime($newsletter->create_date)); ?></td>
                    <td><?php echo $newsletter->email; ?></td>
                    <td class="text-center">
                    <a class="btn-actions btn-actions-delete underlineBtn underlineBtn-blue underlineBtn-blue-black"
                       href="<?php echo ADMIN_URL; ?>newsletter/delete/<?php echo $newsletter->id; ?>?token_id=<?php echo get_token(); ?>"
                       onClick="return confirm('Are You Sure?');">
                        <i class="icon-cancel-circled"></i>
                    </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php }; ?>

<?= footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL . 'newsletter';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>