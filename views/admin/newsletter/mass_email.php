<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>Send Email</h4>

                <div class="form-group">
                    <label>Date Range</label>
                    <input required type="text" name="daterange" value="<?php echo ($model->newsletter->id>0) ? date("m/d/Y g:iA",$model->newsletter->start_time).' - '.date("m/d/Y g:iA",$model->newsletter->end_time) : "";?>"/>
                </div>
                <div class="form-group">
                    <label>Email List</label>
                    <select name="email-list[]" class="multiselect" multiple required>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Send Email</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'colors/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('input[name="daterange"]').daterangepicker({
            timePicker: true,
            format: 'MM/DD/YYYY h:mmA',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true
        },function(){
            var self = $('input[name="daterange"]');
            $("select[name='email-list[]']").empty();
            $.getJSON('/admin/newsletter/gNEL', {date_range:self.val()}, function(data){
                $.each(data,function(key,value){
                    $("select[name='email-list[]']").append($('<option></option>').val(key).text(value));
                });
                $("select[name='email-list[]']").val(data);
                $("select.multiselect").multiselect("rebuild");
            });
        });
        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select.multiselect").multiselect("rebuild");
    });
</script>