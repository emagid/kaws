<?php
$gender = [1 => 'Unisex', 2 => "Men", 3 => "Women"];
$tab = isset($model->tab)? $model->tab:'general-tab';
?>
<style type="text/css">
    .legshot {
        border:1px black solid;
    }
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->product->id; ?>"/>
<!--    <input type="hidden" name="redirectTo" value="--><?//= $_SERVER['HTTP_REFERER'] ?><!--"/>-->
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?=($tab == 'general-tab')?"active":""?>"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation" class="<?=($tab == 'seo-tab')?"active":""?>"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
            <li role="presentation" class="<?=($tab == 'categories-tab')?"active":""?>"><a href="#categories-tab" aria-controls="categories" role="tab" data-toggle="tab">Categories</a></li>
<!--            <li role="presentation"><a href="#linked-tab" aria-controls="linked" role="tab" data-toggle="tab">Linked Items</a></li>-->
<!--            <li role="presentation"><a href="#related-tab" aria-controls="related" role="tab" data-toggle="tab">Related</a></li>-->
            <?php if ($model->product->id > 0) { ?>
                <li role="presentation" class="<?=($tab == 'images-tab')?"active":""?>"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
            <?}?>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?=($tab == 'general-tab')?"active":""?>" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>DELETE</label>
                                <a class="btn-actions" href="<?php echo ADMIN_URL; ?>products/delete/<?php echo $model->product->id; ?>?token_id=<?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
                                    <i class="icon-cancel-circled"></i>
                                </a>
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>SKU</label>
                                <?php echo $model->form->editorFor("sku"); ?>
                            </div>
                            <div class="form-group">
                                <label>Color</label>
                                <?php echo $model->form->editorFor("color"); ?>
                            </div>

<!--                            <div class="form-group">-->
<!--                                <label>Alias</label>-->
<!--                                --><?php //echo $model->form->editorFor("alias"); ?>
<!--                            </div>-->

                            <div class="form-group">
                                <label>Featured image</label>

                                <p>
                                    <small style="color:#abb0b3"><b>(ideal featured image dimensions are 800 x 800, zoomed out with white space surrounding product)</b></small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->product->featured_image != "") {
                                        $img_path = $model->product->featuredImage();
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'products/delete_image/' . $model->product->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->product->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                                <div class="form-group">
                                    <label>Image Alternative Description</label>
                                    <?php echo $model->form->editorFor("image_alt"); ?>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label>Banner image</label>

                                <p><input type="file" name="banner" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
/*                                    $img_path = "";
                                    if ($model->product->banner != "") {
                                        $img_path = $model->product->getBanner();
                                        */?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php /*echo $img_path; */?>" width="100"/>
                                            <br/>
                                            <a href="<?/*= ADMIN_URL . 'products/delete_image/' . $model->product->id; */?>?banner=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="banner"
                                                   value="<?/*= $model->product->banner */?>"/>
                                        </div>
                                    <?php /*} */?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Display Banner</label>
                                <?php echo $model->form->checkBoxFor("display_banner",1)?>
                            </div>-->
                            <!--<div class="form-group">
                                <label>Description</label>
                                <?php /*echo $model->form->textAreaFor("description", ['id' => 'product-description']); */?>
                            </div>-->
                            <div class="form-group">
                                <label>Details</label><p><!--<small>Separate details using | symbol</small>--></p>
                                <?php echo $model->form->textAreaFor("details", ['id' => 'product-details']); ?>
                            </div>
                            <div class="form-group">
                                <label>Quantity</label>
                                <input name="quantity" type="text" value="<?=$model->product->quantity ? : 0?>"/>
                            </div>
<!--                            <div class="form-group">-->
<!--                                <label>Heel Height</label>-->
<!--                                <input name="heel_height" type="text" value="--><?//=$model->product->heel_height ? : 0?><!--"/>-->
<!--                            </div>-->
<!--                            <div class="form-group">-->
<!--                                <label>Sizes</label>-->
<!--                                <select name="size[]" class="multiselect" multiple data-placeholder="Sizes">-->
<!--                                    --><?//$sizes = json_decode($model->product->size,true) ? : [];
//                                    foreach($model->sizes as $size){?>
<!--                                        <option value="--><?//=$size->id?><!--" --><?//=$sizes && in_array($size->id,$sizes) ? 'selected': ''?><!--><?//="US: $size->us_size | EUR: $size->eur_size"?><!--</option>-->
<!--                                    --><?//}?>
<!--                                </select>-->
<!--                            </div>-->
                            <div class="form-group">
                                <label>Availability</label>
                                <select name="availability" class="form-control">
                                    <?foreach(\Model\Product::$availability as $key=>$item){?>
                                        <option value="<?=$key?>" <?php if ($model->product->availability == $key) {echo "selected";} ?>><?=$item?></option>
                                    <?}?>
                                </select>
                            </div>


                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Dimensions</h4>
                            <p><small>In inches</small></p>
                            <div class="form-group">
                                <label>Height</label>
                                <?php echo $model->form->editorFor("height"); ?>
                            </div>
                            <div class="form-group">
                                <label>Width</label>
                                <?php echo $model->form->editorFor("width"); ?>
                            </div>
                            <div class="form-group">
                                <label>Length</label>
                                <?php echo $model->form->editorFor("length"); ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Prices</h4>

                            <div class="form-group">
                                <label>Discounted Price</label><p><small>0 for no discount</small></p>
                                <input name="msrp" type="text" class="form-control numeric" value="<?=$model->product->msrp ? : 0?>"/>
<!--                                --><?php //echo $model->form->editorFor("msrp"); ?>
                            </div>
                            <div class="form-group">
                                <label>Sell price</label>
                                <?php echo $model->form->editorFor("price"); ?>
                            </div>

                        </div>
                    </div>
                    <?php if (count($model->product_questions) > 0) { ?>
                        <div class="col-md-12">
                            <div class="box">
                                <h4>Questions</h4>
                                <table id="data-list" class="table">
                                    <thead>
                                    <tr>
                                        <th width="30%">Date</th>
                                        <th width="60%">Subject</th>
                                        <th width="10%">View</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($model->product_questions as $obj) { ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?php echo $obj->insert_time; ?></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>"><?= $obj->subject ?></a>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?php echo ADMIN_URL; ?>questions/update/<?php echo $obj->id; ?>">
                                                    <i class="icon-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane <?=($tab == 'seo-tab')?"active":""?>" id="seo-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <?php echo $model->form->editorFor("tags"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->editorFor("meta_description"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane <?=($tab == 'categories-tab')?"active":""?>" id="categories-tab">
                <select name="product_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
                    <?php foreach ($model->categories as $cat) { ?>
                        <option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                    <?php } ?>
                </select>
            </div>
            <?php if ($model->product->id > 0) { ?>
                <div role="tabpanel" class="tab-pane <?=($tab == 'images-tab')?"active":""?>" id="images-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <div class="dropzone" id="dropzoneForm"
                                     action="<?php echo ADMIN_URL . 'products/upload_images/' . $model->product->id; ?>">

                                </div>
                                <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                                <br/>
                                <p>
                                    <small style="color:#abb0b3"><b>(ideal product angle image dimensions are 1024px wide x 683px height)</b></small>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box" style="display: none">
                                <label>Set Default Color</label>
                                <select name="default_color_id" class="multiselect">
                                    <? if ($model->product->color) { ?>
                                        <? foreach (json_decode($model->product->color, true) as $color) {
                                            $c = \Model\Color::getItem($color);
                                            $selected = $model->product->default_color_id == $c->id ? 'selected' : '' ?>
                                            <option value="<?= $c->id ?>" <?= $selected ?>><?= $c->name ?></option>
                                        <? } ?>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <table id="image-container" class="table table-sortable-container">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>File Name</th>
<!--                                        <th>Variant</th>-->
                                        <th>Display Order</th>
<!--                                        <th>Leg Shot</th>-->
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($prImg = \Model\Product_Image::getList(['where' => 'product_id=' . $model->product->id, 'orderBy' => 'display_order', 'sort' => 'DESC']) as $pimg) {?>
                                            <tr data-image_id="<?php echo $pimg->id; ?>">
                                                <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100" height="100"/></td>
                                                <td><?php echo $pimg->image; ?></td>
                                                <td class="display-order-td"><?=$pimg->display_order?></td>
<!--                                                <td class="leg_shot"><input id="leg_shot" name="leg_shot" type="radio" value="--><?//=$pimg->id?><!--" --><?//=$pimg->leg_shot == 'TRUE' ? 'checked': ''?><!--/></td>-->
                                                <td class="text-center">
                                                    <a class="btn-actions delete-product-image"
                                                       href="<?php echo ADMIN_URL; ?>products/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                        <i class="icon-cancel-circled"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                    <? } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane <?=($tab == 'thresholds-tab')?"active":""?>" id="thresholds-tab">
                    <div class="row">
                        <div class="col-md-24">
                            <div class="box">
                                <h4>Set low item numbers for store locations</h4>
                                <h2>(Set to 0 for no low threshold)</h2>
                                <? foreach ($model->thresholds as $inv_id => $thresh){ ?>
                                    <div class="form-group">
                                        <label><?=$model->inventories[$inv_id]?></label>
                                        <input type="numeric" name="threshold[<?=$thresh->id?>]" value="<?=$thresh->threshold?:0?>">
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div role="tabpanel" class="tab-pane <?=($tab == 'specs-tab')?"active":""?>" id="specs-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <a id="add_desc" class="btn btn-default">Add Spec</a>
                            <table id="spec_table" class="table">
                                <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Value</th>
                                    <th>Display Order</th>
                                    <th>Used for Comparison</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody id="content_box">
                                <?php foreach ($prSp = \Model\Product_Spec::getList(['where' => 'product_id=' . $model->product->id, 'orderBy' => 'spec_id']) as $p) {?>
                                    <tr>
                                        <td><select name="spec_category[]" class="form-control">
                                                <?php foreach (\Model\Spec::getList(['where' => 'category!=0','orderBy'=>'category'])  as $spe) {
                                                    $select = ($spe->id == $p->spec_id) ? " selected='selected'" : "";
                                                    ?>
                                                    <option
                                                        value="<?php echo $spe->id; ?>" <?php echo $select; ?> ><?php echo $spe->name; ?></option>
                                                <?php } ?>
                                            </select></td>
                                        <td><input name="spec_value[]" value="<?=$p->value?>"></td>
                                        <td><input name="spec_display_order[]" value="<?=$p->display_order?>"></td>
                                        <td><select name="spec_status[]" class="form-control">
                                                <?foreach(\Model\Product_Spec::$status as $key=>$item){?>
                                                    <option value="<?=$key?>" <?php if ($p->status == $key) {echo "selected";} ?>><?=$item?></option>
                                                <?}?>
                                            </select></td>
                                        <td class="text-center">
                                            <a class="btn-actions delete_content">
                                                <i class="icon-cancel-circled"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="posttag">

    <button type="submit" class="btn btn-save" style="display:none">Save</button>
    <div class="btn btn-save saveorupdate">Save</div>
    <div id="update" class="btn btn-save saveorupdate">Update</div>
</form>

<?php echo footer(); ?>
<script type="text/javascript">
    var categories = <?php echo json_encode($model->product_categories); ?>;
    var accessories = <?php echo json_encode($model->product_accessories); ?>;
    var colors = 0;
    var sizes = <?php echo ($model->product->size?:0); ?>;
    var related = <?php echo ($model->product->related_products?:0); ?>;
    var default_color_id = <?php echo ($model->product->default_color_id?:0); ?>;
    var linked_list = <?php echo ($model->product->linked_list?:0); ?>;
//    var collections = <?php //echo json_encode($model->product_collections); ?>//;
//    var materials = <?php //echo json_encode($model->product_materials); ?>//;
    //console.log(categories);
    var site_url =<?php echo json_encode(ADMIN_URL.'products/'); ?>;
    $(document).ready(function () {
//        var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
        var video_thumbnail = new mult_image($("input[name='video_thumbnail']"), $("#preview-thumbnail-container"));

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }
        $('#update').on('click',function(){
            var tag = $(".nav li.active").find("a").attr("href");
            $("input[name='posttag']").val(tag);
        })
        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('mousewheel.disableScroll')
        });

        $('.variant-word').on('click',function(){
            $(this).parent().find('.variant-details-container').toggle();
        });

        $('#add_desc').on('click', function(){
            var content =
                '<tr><td><select name="spec_category[]" class="form-control">';
            <? foreach (\Model\Spec::getList(['where' => 'category!=0','orderBy'=>'category']) as $item) { ?>
            content += '<option value="<?= $item->id ?>"><?= $item->name ?></option>';
            <? } ?>
            content += '</select></td><td><input name="spec_value[]" value=""></td><td><input name="spec_display_order[]" value=""></td>' +
                '<td><select name="spec_status[]" class="form-control"><option value="1">Active</option><option value="2">Hidden</option></select></td>' +
                '<td class="text-center"><a class="btn-actions .delete_content"><i class="icon-cancel-circled"></i></a></td></tr>';

            $('#content_box').append(content);
        });
        $(document).on('click','.delete_content', function(){
            $(this).parent().parent().remove();
        });
        $(document).on('click','.delete_feature', function(){
            $(this).parent().remove();
        });

        $(".saveorupdate").click(function () {
            var price = $("input[name='price']").val();
            var name = $("input[name='name']").val();
            var slug = $("input[name='slug']").val();
            var id = <?=$model->product->id?>;
            var errors = new Array();
            if ($.trim(price) > 0) {
                $("input[name='price']").css({
                    "border-color": ""
                });
            } else {
                $("input[name='price']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect price");
            }
            if ($.trim(name) == "") {
                $("input[name='name']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect name");
            } else {
                $("input[name='name']").css({
                    "border-color": ""
                });
            }
            if ($.trim(slug) == "") {
                $("input[name='slug']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect slug");
            } else {

                $.ajax({
                    async: false,
                    url: '/admin/products/check_slug',
                    enctype: 'multipart/form-data',
                    method: 'POST',
                    data: {
                        slug: slug,
                        id: id
                    },
                    success: function (data) {

                        if (data == 1) {
                            errors.push("Slug must be uniq!");

                            $("input[name='slug']").css({
                                "border-color": "red"
                            });

                        } else {
                            $("input[name='slug']").css({
                                "border-color": ""
                            });
                        }
                    }
                });
            }
            var text = "";
            for (i = 0; i < errors.length; i++) {

                text += "<li>" + errors[i] + "</li>";
            }
            if (errors.length > 0) {
                $('html, body').animate({
                    scrollTop: 0
                });
                $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
                errors = [];
            } else {
                $(".form").submit();
            }
        });

        $("input.image").change(function () {
            readURL(this);
        });


        $("input[name='name'],input[name='color']").on('keyup', function (e) {
            var val = $.trim($("input[name='name']").val());
            var color = $.trim($("input[name='color']").val());
            if(color != '') {
                val = val+' '+color;
            }
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='product_category[]']").val(categories);
        $("select[name='product_accessory[]']").val(accessories);
        $("select[name='color[]']").val(colors);
        $("select[name='size[]']").val(sizes);
        $("select[name='related_products[]']").val(related);
        $("select[name='default_color_id']").val(default_color_id);
        $("select[name='linked_list[]']").val(linked_list);
        <?if(isset($prImg) && $prImg){
        foreach($prImg as $pimg){?>
            $(".color_image"+<?=$pimg->id?>).val(<?=$pimg->color_id?>);
        <?}?>
        <?}?>
//        $("select[name='product_collection[]']").val(collections);
//        $("select[name='product_material[]']").val(materials);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#spare-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        $("a.delete-product-image").on("click", function (e) {
            e.preventDefault();
            var that = $(this);
            if (confirm("are you sure?")) {

                var url = $(this).attr('href');

                $.get(url, function(){
                    that.parent().parent().fadeOut('slow');
                })
            } else {
                return false;
            }
        });
        var adjustment;
        $('#spare-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden; width: 100px; height: 100px;">.</td></tr>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#spare-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("pacc_id"));

                });
                $.post('<?=ADMIN_URL.'products/orderSpares'?>', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td style="visibility: hidden; width: 100px; height: 100px;">.</td></tr>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post('<?=ADMIN_URL.'products/sort_images'?>', {ids: ids}, function (response) {
                    var counter = 1;
                    $('#image-container >tbody>tr').each(function (i, e) {
                        $(e).find('td.display-order-td').html(counter);
                        counter++;
                    });
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
        $(".numeric").numericInput();

        $('#sync-kadro').on('click', function(e){
            e.preventDefault();
            $.post('/admin/products/sync_kadro', {id: $(this).data('id')}, function(r){
                if(r.status) alert('Updated');
                else alert(r.message);
            });
        })
    });
</script>
<script>
    $(document).ready(function(){
        $('select[name=sale_icon]').change(function(){
            var img = $('#icon_img');
            var icon =  $(this).find(':selected').text();
            if(this.value){
                img.attr('src','<?=FRONT_IMG?>'+icon+'.png');
                img.show();
            } else {
                img.hide();
            }
        });
        $('#toggle_color_display_order').on('change',function(){
            if($(this).is(':checked')){
                $('.color_display_order').sortable('enable');
            } else {
                $('.color_display_order').sortable('disable');
            }
        });
        var adjustment;
        var serial = $('.color_display_order').sortable({
            group:'sortable',
            onDrop: function ($item, container, _super) {
                var data = serial.sortable("serialize").get();
                var jsonData = JSON.stringify(data,null,'');

//                $('.serialize-result').text(jsonData);
                $.post('/admin/products/sort_colors',{data:jsonData},function(){
                    console.log('done');
                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
        $('.color_display_order').sortable('disable');
    })
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'products/upload_images/'.$model->product->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>
