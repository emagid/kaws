<?php
$discount_type = [1 => '$', 2 => '%',3 => 'Shipping'];
?>
<style type="text/css">
    .PrintOnly {
        display: none;
    }

    @media print {
        .top-bar, .site-nav, .banner.cms, footer .container, h2, .order_status, h3, .form {
            display: none;
        }

        .ordered_products img {
            width: 50px;
            height: 50px;
        }

        .PrintOnly {
            display: block;
            margin: 0 auto;
            width: 199px;
            height: 71px;
        }

        a[href^="/"]:after {
            content: " ";
        }

    }
</style>

<div class="PrintOnly">

    <br>
    <center><img src="<?= SITE_URL ?>content/frontend/img/logo.png"><br></center>


    <center>
        <label>Coupon Code</label><br><br>
        <b> <?php echo $model->coupon->code ?> </b><br><br>

        <B>Giving to you <?php if ($model->coupon->discount_type == 1) {
                echo "$";
            } else {
                echo "%";
            } ?><?php echo $model->coupon->discount_amount; ?> discount<br> WHEN?<b> <br>

                <?php echo ($model->coupon->id > 0) ? date("m/d/Y g:iA", $model->coupon->start_time) . ' - ' . date("m/d/Y g:iA", $model->coupon->end_time) : ""; ?>
                <br>
                <? if ($model->coupon->min_amount > 0) {
                    echo "Minimum amount of your purchase should be  ";
                    echo $model->coupon->min_amount;
                    if ($model->coupon->discount_type == 1) {
                        echo "$";
                    } else {
                        echo "%";
                    }
                } else {
                    echo ' ';
                } ?>
    </center>
</div>
<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->coupon->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>Display on Coupon Page?</label>
                    <?php echo $model->form->checkboxFor("display", 1); ?>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreaFor("description", ['id' => 'product-description']); ?>
                </div>
                <div class="form-group">
                    <label>Coupon Code</label>
                    <?php echo $model->form->editorFor("code"); ?>
                </div>

                <div class="form-group">
                    <label>Date Range</label>
                    <input type="text" name="daterange"
                           value="<?php echo ($model->coupon->id > 0) ? date("m/d/Y g:iA", $model->coupon->start_time) . ' - ' . date("m/d/Y g:iA", $model->coupon->end_time) : ""; ?>"/>
                </div>

                <div class="row">
                    <div class="col-sm-20">
                        <div class="form-group">
                            <label>Active Categories</label>
                            <select name="active_categories[]" class="multiselect" multiple>
                                <? $active_categories = $model->coupon->active_categories ? json_decode($model->coupon->active_categories,true) : [];
                                foreach ($model->categories as $category) { ?>
                                    <option value="<?= $category->id ?>" <?=in_array($category->id,$active_categories) ? 'selected': ''?>><?= $category->name ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Active Clearance</label>
                            <?= $model->form->checkBoxFor('active_clearance', 1) ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-20">
                        <div class="form-group">
                            <label>Active Products</label>
                            <select name="active_products[]" class="multiselect" multiple>
                                <? $active_products = $model->coupon->active_products ? json_decode($model->coupon->active_products,true) : [];
                                foreach ($model->products as $product) { ?>
                                    <option value="<?= $product->id ?>" <?=in_array($product->id,$active_products) ? 'selected': ''?>><?= $product->name ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-20">
                        <div class="form-group">
                            <label>Restrict discount to these shipping Options</label>
                            <select name="active_shipping[]" class="multiselect" multiple>
                                <? $active_shipping = $model->coupon->active_shipping ? json_decode($model->coupon->active_shipping,true) : [];
                                foreach (\Model\Shipping_Method::getList() as $shipping) { ?>
                                    <option value="<?= $shipping->id ?>" <?=in_array($shipping->id,$active_shipping) ? 'selected': ''?>><?= $shipping->country.' - '.$shipping->name ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Discount Type</label>
                    <select name="discount_type" class="form-control">
                        <?php foreach ($discount_type as $key => $val) {
                            $select = ($model->coupon->discount_type == $key) ? " selected='selected'" : "";
                            ?>
                            <option value="<?php echo $key; ?>"<?php echo $select; ?>><?php echo $val; ?></option>
                        <?php } ?>
                    </select>

                </div>
                <div class="form-group">
                    <label>Discount Amount</label>
                    <?php echo $model->form->textBoxFor("discount_amount"); ?>
                </div>
                <div class="form-group">
                    <label>Minimum Amount</label>
                    <?php echo $model->form->textBoxFor("min_amount", ['class' => "currency-us"]); ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Number maximum of uses</label>
                            <?php echo $model->form->textBoxFor("num_uses_all", ['class' => "numeric"]); ?>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Already used</label>
                            <input type="text" value="<?= $model->already_used ?>" disabled="disabled"/>
                        </div>
                    </div>
                </div>
                <!--div class="form-group">
                <label>Number of uses per user (enter 0 or leave blank)</label>
                <?php echo $model->form->textBoxFor("uses_per_user", ['class' => "numeric"]); ?>
            </div-->
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
    <button class="btn btn-save" id="print">Print</button>


    </div>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.'coupons/');?>;
    $(document).ready(function () {
        $(function () {
            $("select.multiselect").each(function (i, e) {
                var placeholder = $(e).data('placeholder');
                $(e).multiselect({
                    nonSelectedText: placeholder,
                    includeSelectAllOption: true,
                    maxHeight: 415,
                    checkboxName: '',
                    enableCaseInsensitiveFiltering: true,
                    buttonWidth: '100%'
                });
            });
            $('input[name="daterange"]').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mmA',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                showDropdowns: true,
                <?php if($model->coupon->id > 0) { ?>
                startDate: "<?php echo date("m/d/Y g:iA",$model->coupon->start_time);?>",
                endDate: "<?php echo date("m/d/Y g:iA",$model->coupon->end_time);?>",
                <?php } ?>

            });
        });

        $.validator.addMethod("validate_code", function (value, element) {
            var pass_validation = false;
            var data = {val: value, id: $("#coupon-form").data('id')};
            $.ajax({
                url: site_url + "validate_code",
                async: false,
                data: data,
                method: "GET",
                success: function (response) {
                    if (response.success) {
                        pass_validation = true;
                    }
                }
            });
            return pass_validation;
        }, 'Code is invalid or already exists');

        $.validator.addMethod("validate_daterange", function (value, element) {
            var pass_validation = false;
            var data = {val: value};
            $.ajax({
                url: site_url + "validate_daterange",
                async: false,
                data: data,
                method: "GET",
                success: function (response) {
                    if (response.success) {
                        pass_validation = true;
                    }
                }
            });
            return pass_validation;
        }, 'Invalid Date Range');

        $.validator.addMethod("validate_money", function (value, element, params) {
            if (params == 0 && value == "") {
                return true;
            }
            var pass_validation = false;
            var data = {val: value, gtzero: params};
            $.ajax({
                url: site_url + "validate_money",
                async: false,
                data: data,
                method: "GET",
                success: function (response) {
                    if (response.success) {
                        pass_validation = true;
                    }
                }
            });
            return pass_validation;
        }, 'Invalid money amount');

        $("#coupon-form").validate({
            onkeyup: false,
            onclick: false,

            rules: {
                name: {required: true},
                code: {required: true, validate_code: true},
                discount_amount: {required: true, validate_money: 1},
                min_amount: {validate_money: 0},
                daterange: {required: true, validate_daterange: true}
            }

        });

        $(".currency-us").autoNumeric('init', {aSign: '$'});
        $(".numeric").numericInput();
    });
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>