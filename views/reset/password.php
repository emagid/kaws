<div class="pageWrapper contactPageWrapper" style="margin-top: 150px;">
    <form method="post" action="<?=$this->emagid->uri?>">
        <div class="modal_formWrapper contact">
            <h4 class="gothamLight">Reset Password</h4>
            <div class="inputRow">
                <input type="password" name="password"/>
                <label>Password</label>
            </div>
            <div class="inputRow">
                <input type="password"  name="confirm-password"/>
                <label>Password Confirmation</label>
            </div>
            <button type="submit" style="margin-top: 50px">Reset</button>
        </div>
    </form>

</div>