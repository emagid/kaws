<section style="background-image:url('<?=\Model\Blog_Category::getItem($model->blog_category->id)->getBanner()?>')"> <!-- Header image-->
    <h1 class="valB big blogDetailsTitle"><?=$model->blog_category->name?></h1>
</section>
<div class="pageWrapper aboutPageWrapper">
    <div class="blogDetailsContentWidth contentWidth">
        <div class="blogDescriptionWrapper">
            <? if (!empty($model->blogs)): ?>
                <? foreach($model->blogs as $blog):?>
                <div class="row">
                    <img src="<?=$blog->getFeaturedImage()?>" width="300px" height="300px">
                    <div>
                        <a href="/blog/<?=$blog->slug?>">
                            <div class="blogDetailsContentWidth contentWidth">

                                <h2 class="valUpper uppercase gray"><?=$blog->title?></h2>
                                <h2 class="valUpper uppercase gray"><?=$blog->subtitle?></h2>

                                <div class="blogDescriptionWrapper">
                                    <p class="valB">by <?=$blog->author?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <? endforeach; ?>
            <? endif; ?>
        </div>
    </div>
    <div> <!-- sidebar, breadcrumbs -->
        <ul>
            <? foreach (\Model\Blog_Category::getOptions() as $id => $category):?>
                <li>
                    <a href="/blogs/<?=\Model\Blog_Category::getItem($id)->slug?>" ><?=$category?></a> <!-- categories -->
                </li>
                <? if(\Model\Blog::getCount(['where'=>"blog_category_id = {$id}"]) > 0): ?>
                    <li>
                        <ul>
                            <? foreach (\Model\Blog::getList(["where"=>"blog_category_id = $id","orderBy"=>"insert_time DESC"]) as $blog ):?>
                                <li>
                                    <a href="/blog/<?=$blog->slug?>" ><?=$blog->title?></a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </li>
                <? endif; ?>
            <? endforeach; ?>
        </ul>
    </div>
</div>