<section style="background-image:url('<?=\Model\Blog_Category::getItem($model->blog->blog_category_id)->getBanner()?>')"> <!-- Header image-->
    <h1 class="valB big blogDetailsTitle"><?=$model->blog->title?></h1>
    <h1 class="valB big blogDetailsTitle"><?=$model->blog->subtitle?></h1>
    <h6 class="valUpper uppercase gray">by <?=$model->blog->author?></h6>
</section>
<div class="pageWrapper aboutPageWrapper">
    <div class="blogDetailsContentWidth contentWidth">
        <div class="blogDescriptionWrapper">
            <p class="valB"><?=$model->blog->body?></p>
        </div>
    </div>
    <div> <!-- sidebar, breadcrumbs -->
        <ul>
            <? foreach (\Model\Blog_Category::getOptions() as $id => $category):?>
                <li>
                    <a href="/blogs/<?=\Model\Blog_Category::getItem($id)->slug?>" ><?=$category?></a> <!-- categories -->
                </li>
                <? if(\Model\Blog::getCount(['where'=>"blog_category_id = {$id}"]) > 0): ?>
                    <li><ul>
                        <? foreach (\Model\Blog::getList(["where"=>"blog_category_id = $id","orderBy"=>"insert_time DESC"]) as $blog ):?>
                            <li>
                                <a href="/blog/<?=$blog->slug?>" ><?=$blog->title?></a>
                            </li>
                        <? endforeach; ?>
                    </ul></li>
                <? endif; ?>
            <? endforeach; ?>
        </ul>
    </div>
</div>
