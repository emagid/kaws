<div class="pageWrapper watchVideoWrapper">
	<div class="watchHero">
		<div class="background_overlay"></div>
		<video id="banner_video" class="ux-banner-video hide-for-small" autoplay="">
            <source src="<?=FRONT_VIDEO?>djiCamriseWatch_video.mp4" type="video/mp4">
            <source src="" type="video/webm">
            <source src="<?=FRONT_VIDEO?>djiCamriseWatch_video.ogv" type="video/ogg">
        </video>
        <div class="mainContentWidth mainContentWidth_watchPage">
        	<h2 class="dji-text">
        		<icon class="djiIcon djiIconUpload" style="background-image:url('<?=FRONT_IMG?>videoUpload.png')"></icon>
        		<span>Share your Videos</span>
        	</h2>
        	<h6 class="gohamLight">Express your creativity and expertise with the DJI community.</h6>
            <div style="display: none;" class="preview-shell">
                <div class="preview-container">
                </div>
                <a class="solidBlueBtn" id="confirmUpload">Confirm</a>
                <a class="solidBlueBtn" id="cancelUpload" style="background: red;">Cancel</a>
            </div>
            <form id="watchUpload" action="/watch/upload" method="post" enctype="multipart/form-data">
                <input type="file" name="video" id="upload" style="display: none;" accept="video/*"/>
            </form>
        	<a class="solidBlueBtn" id="uploadMask">Upload Now</a>
        </div>
    </div>
    <div class="videosGrid">
    	<div class="row">


    	</div>
    </div>
</div>
<script>
    $(document).ready(function(){
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var source = $("<video />");
                reader.onload = function (e) {
                    source.attr('src', e.target.result);
                    source.attr('alt', 'Uploaded Video');
                    source.attr('autoplay', 'autoplay');
                    source.attr('controls', 'controls');
                };
                $(input).parent().parent().find('.preview-container').html(source);

                reader.readAsDataURL(input.files[0]);
            }
        }
        $('#uploadMask').on('click',function(e){
            e.preventDefault();
            $('#upload').trigger('click');
        });
        $('#upload').on('change',function(){
            readURL(this);
            $('.preview-shell').show();
            $('#uploadMask').hide();
        });
        $('#confirmUpload').on('click',function(){
            $('#watchUpload').submit();
        });
        $('#cancelUpload').on('click',function(){
            $('.preview-shell').hide();
            $('#uploadMask').show();
            $('#upload').val('');
            $('.preview-container').empty();
        })
    });
</script>