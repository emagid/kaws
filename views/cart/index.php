<div class="pageWrapper cartPageWrapper">
	<div class="row content_width_1000">
		<div class="col bagItemsCol">
			<div class="header">
				<h1 class="dji-blue dji-text">Shopping Bag</h1>
				<div class="right_float">
					<?if($model->cart->cart){?>
						<a class="solidBlueBtn" href="<?='/checkout/payment'?>">Proceed to Checkout</a>
					<?} else {?>
						<a class="underlineBtn-blue underlineBtn-blue-black underlineBtn btn" href="<?='/'?>"><p>Shop the Catalog</p></a>
					<?}?>
				</div>
			</div>
			<div class="bagItemsListWrapper">
				<ul>
					<?if(isset($model->cart->cart) && $model->cart){
					foreach($model->cart->cart as $cart){
						$product = \Model\Product::getItem($cart->product_id);
						$color = $cart->getColor();
						$size = $cart->getSize(); ?>
					<li>
						<div class="row" id="<?=$cart->id?>">
							<div class="col bagItemImg">
								<a href="<?='/products/'.$product->slug?>">
									<img src="<?=$product->featuredImage()?>">
								</a>
							</div>
							<div class="col bagItemInfo">
								<h6 class="as_l bagItemInfoName"><?=$product->name?><?=$cart->clearance ? '(Clearance)': ''?></h6>
								<?$arr = \Model\Variation::$lowerVariations;
								$variation = json_decode($cart->variation,true);
								if($variation){
								foreach($variation as $key=>$value){
									if(in_array($key,$arr) && $value){?>
									<div class="bagItemInfoItem row row_of_2">
										<div class="col">
											<h8 class="as_l"><?=$key?>:</h8>
										</div>
										<div class="col">
											<h8 class="as_l">
												<?$ucKey = ucfirst($key);
												$mode = "\Model\\$ucKey"?>
												<?=class_exists($mode) ? $mode::getItem($value)->name(): $value?>
											</h8>
										</div>
									</div>
								<?}?>
								<?}?>
								<?}?>
								<!-- Old Working Quantity -->
<!-- 								<h8 class="as_l bagItemInfoName expandQuantityOptions">

							
									<input data-cart_id="<?=$cart->id?>" class="quantityValue" type="number" name="quantity" min="1" max="5" value="<?=$cart->quantity?>"><icon class="expansion_icon"></icon></h8> -->

					<!-- New Quantity -->
					<div class="col left_float quantity numbersRow">
                        <input data-cart_id="<?=$cart->id?>" type="text" name="quantity" id="product-quantity" value="<?=$cart->quantity?>">
                        <div class="quantToggleButtons">
                            <div class="inc button">
								<span>
								</span>
								<span>
								</span>
                            </div>
                            <div class="dec button">
								<span>
								</span>
                            </div>
                        </div>
                    </div>

							</div>
							<div class="col bagItemPrice">
								<h6 class="as_l bagItemInfoPrice">$<?=$cart->clearance ? $cart->quantity*$product->getPrice($color,$size,true,$model->user) : $cart->quantity*$product->getPrice($color,$size,false,$model->user)?></h6>
								<div class="footer">
									<a class="btn solidBlueBtn updateBtn" data-cart_id="<?=$cart->id?>"><img src="<?=FRONT_IMG?>refresh-button.png"></a>
									<a class="btn solidBlueBtn" href="<?='/cart/remove_product/'.$cart->id?>">Remove</a>
								</div>
							</div>
							
						</div>
					</li>
					<?}?>
					<?}?>
				</ul>
<!-- 				<p class="as_r">For orders including at least one item not manufactured in our NYC factory, you can only choose standard or express shipping.</p> -->
			</div>
			<div class="orderTotalsWrapper">
<!-- 				<div class="promoInputWrapper inputBtnUI">
					<label class="inputLabel" for="promoCode">Promotional Code</label>
					<input type="text" id="promoCode" class="whiteTxtInput" placeholder="Promotional Code">
					<button class="btn btn_black">Apply</button>
				</div> -->
				<div class="orderTotalsData">
					<div class="row row_right">
						<div class="col orderDataLabel">
							<p>Subtotal:</p>
						</div>
						<div class="col orderDataValue">
							<p>$<?=number_format($model->cart->total,2)?></p>
						</div>
					</div>
					<div class="row row_right">
						<div class="col orderDataLabel">
							<p>Estimated Sales Tax:</p>
						</div>
						<div class="col orderDataValue">
							<p>To be Calculated</p>
						</div>
					</div>
					<div class="row row_right">
						<div class="col orderDataLabel">
							<p>Shipping:</p>
						</div>
						<div class="col orderDataValue">
							<select name="ship_speed" id="ship_speed">
                                <? foreach ($model->shipping_speeds AS $speed){?>
                                    <option value="<?=$speed?>" <?= (isset($_SESSION['speed']) && $_SESSION['speed'] == $speed ) || (!isset($_SESSION['speed']) && isset($model->default_speed) && $model->default_speed == $speed) ? 'selected':'' ?>><?=$speed?></option>
                                <? } ?>
                            </select>
                                                <span class="downArrowICon ship_arrow">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                                    <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                                    L-321,348.1z"></path>
                                </svg>
                    </span>
						</div>
					</div>
                    <?
                        $bonus_gift = floor((floatval($model->cart->total)-floatval($model->cart->discount))/1000)*50;
                    ?>
                    <? if($bonus_gift){ ?>
                    <div class="row row_right">
                        <div class="col orderDataLabel" id="special_notice">
                            <p>Your purchase has qualified you for a for $<?=number_format($bonus_gift,2)?> DJINYC.com gift card to use in 2018. <br>
                                <span>* Gift cards will be distributed by email within 72 hours of purchase.</span></p>
                        </div>
                    </div>
                    <? } ?>
					<?if($model->cart->discount){?>
						<div class="row row_right">
							<div class="col orderDataLabel">
								<p>Discount:</p>
							</div>
							<div class="col orderDataValue">
                                <p>-$<span class="discount-amt"><?=number_format($model->cart->discount,2)?></span></p>
							</div>
						</div>
					<?}?>
                    
                    
                      
                                <?if(!$model->wholesale){
                    if(!isset($_SESSION['coupon'])){?>
                    
                <div class="mobile_summary">
                            <div class="shoppingInfoCol col shoppingInfoColPayment" id="mobile_show">
                            			<div class="labelDataBox orderSummaryDataBox">


				<ul class="paymentScreenBagItems">
					<? foreach($model->cart->cart as $cart){
						if($cart->product_id) {
							$product = \Model\Product::getItem($cart->product_id);
//							$color = $cart->getColor();
//							$colorObj = \Model\Color::getItem($color);
//							$size = $cart->getSize();
//							$sizeObj = \Model\Size::getItem($size);
							$url = '/products/' . $product->slug;
							$img = $product->featuredImage();
							$name = $product->name;
//							$colorText = strlen($colorObj->name) < 20 ? $colorObj->name: substr($colorObj->name,0,20).'...';
//							$sObjUs = $sizeObj->us_size;
//							$sObjEu = $sizeObj->eur_size;
							$price = $product->getPrice(null,null,false,$model->user);
//							if($cart->clearance && ($c = \Model\Clearance::checkClearance($cart->product_id,$color,$size))){
//								$name .= '(Clearance)';
//								$price = $product->getPrice($color,$size,true);
//							}
						} else {
//							$detail = json_decode($cart->details,true);
//							$gc = \Model\Gift_Card::getItem($detail['gift_card_id']);
//
//							$url = '/gift_cards/product/'.$detail['gift_card_id'];
//							$img = UPLOAD_URL.'gift_cards/'.$gc->image;
//							$name = $gc->name;
//							$colorText = '';
//							$sObjUs = '';
//							$sObjEu = '';
//							$price = number_format($detail['amount'],2);
						}?>
						<li>
							<div class="col bagItemImg">
								<a href="<?=$url?>" class="row">
									<div class="col">
										<p><?=$name?></p>
										<!--										<p>--><?//=$colorText?><!--</p>-->
										<!--										<p>US: --><?//=$sObjUs?><!-- EU: --><?//=$sObjEu?><!--</p>-->
										<p>$<?=number_format($price,2)?></p>
									</div>
									<div class="col">
										<img src="<?=$img?>">
									</div>
								</a>
							</div>
						</li>
					<?}?>
				</ul>
				<p class="price_bag"><b>Subtotal</b>$<?=$model->cart? number_format($model->cart->total,2): 0.00?></p>
				<p class="price_bag"><b>Shipping</b>$<?=number_format($model->cart->shipping,2)?></p>
				<?if($model->cart->discount){?>
					<p><b>Discount</b>-$<?=number_format($model->cart->discount,2)?></p>
				<?}?>
				<span class="line_sep"></span>
				<div class="orderSummaryTotal"><span class="triangleBorder"></span><p><b>Total</b>$<?=$model->cart ? number_format($model->cart->total - $model->cart->discount + $model->cart->shipping,2): 0.00?></p></div>
			</div>
			<div style="display:none!important;">
				<?if(isset($model->cart->discount)&&($model->cart->discount)){?>
					<p>Discount Applied | <span class="pinkDiscount">- $<?=number_format($model->cart->discount,2)?></span> </p>
				<?} else {?>
					<label for="email"><p>Redeem a coupon</p></label>
					<input type="text" name="gift-code" id="emailAddr">
					<a id="do_coupon" class="btn btn_primary">Apply Coupon</a>
				<?}?>
			</div>
                    </div>
                </div>

                        <div class="giftCardSection" id="cartPage">

                            <input type="text" placeholder="Coupon Code" class='cpn-code' name="coupon-code" id="couponInput">
                            <input type="button" value="Apply" id="redeem-coupon" class="btn btn_black apply-cpn">
                        </div>
                    <?} else {?>
                        <h7 class="as_l">Coupon: <b><?=$_SESSION['coupon']?></b></h7>
                        <input type="button" value="Remove" style="cursor: pointer" id="remove-coupon" class="btn_black">
                    <?}?>
                <?}?>

                <?if(false){//hide gift card
                    //				<?if(!$model->wholesale){
                    if(!isset($_SESSION['gift_card'])){?>
                        <h7 class="as_l">Gift Card Code</h7>
                        <div class="giftCardSection">
                            <input type="text" name="gift-card-code" id="giftCardInput">
                            <input type="button" value="Apply" id="redeem-gift-card" class="btn btn_black">
                        </div>
                    <?} else {?>
                        <h7 class="as_l">Gift Card: <b><?=($gc = \Model\Gift_Card::validateGiftCard($_SESSION['gift_card'])) ? $gc->name: $_SESSION['gift_card']?></b></h7>
                        <input type="button" value="Remove" style="cursor: pointer" id="remove-gift-card" class="btn_black">
                    <?}?>
                <?}?>
					<div class="row row_right finalTotal">
						<span class="triangleBorder"></span>
						<div class="col orderDataLabel">
							<p>Estimated Total</p>
						</div>
						<div class="col orderDataValue">
							<p><strong class="upd_total">$<?=number_format($model->cart->total - $model->cart->discount + $model->cart->shipping,2)?></strong></p>
						</div>

					</div>





					<div class="checkout_bottom">
					<a class="solidBlueBtn" href="<?='/checkout/payment'?>">Proceed to Checkout</a>
					</div>
				</div>
			</div>
		</div>
		<div class="shoppingInfoCol col">
			<div class="labelDataBox">
				<label>Delivery and Returns</label>
				<p>You can choose from different delivery service levels:</p>
				<p><b>Standard Shipping</b>Most in-stock items should arrive on your doorstep within seven business days after receipt of your order. </p>
				<p><b>Express Shipping</b>If you use the Expedited shipping option, your products should arrive on your doorstep within three business days after receipt of your order.</p>
				<span class="line_sep"></span>
				<p><b>Easy Returns</b>Returns service: All products may be exchanged or returned within 14 days of the invoice date. Thereafter, all sales are final. </p>
			</div>
			<div class="labelDataBox">
				<label>Secure Payments</label>
				<p>To ensure the safety of your credit card data at all times, we use Secure Socket Layer (SSL) technology and the highest security standards, certified by Verisign™ and Trustwave.</p>
				<img src="<?=FRONT_IMG?>creditCardOptions.png">
			</div>
			<div class="labelDataBox">
				<label>Gift Option</label>
				<p>Gift wrap your order, removing all prices, and add a greeting card.</p>
			</div>
			<div class="labelDataBox">
				<label>Customer Care</label>
				<p>You can reach us by:</p>
				<p><b>Phone</b><a href="tel:2123333990">212 333 3990</a></p>
				<p><b>Toll-Free</b>M-F 10AM-6PM ET</p>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    $('.apply-cpn').on('click',function(){
        var code = $(this).siblings('input.cpn-code');
        if(code.val() == null || code.val() == ''){
            alert('Please input a coupon code');
        } else {
            $.post('/coupon/add',{'code':code.val()},function(data){
                var json = $.parseJSON(data);
                if(json.status == 'success'){
                    location.reload();
                } else {
                    alert(json.message);
                }
            })
        }
    });
    $('#remove-coupon').on('click',function(){
        var arr = ['card_name','cc_number','cc_type','cc_expiration_month','cc_expiration_year','cc_ccv'];
        var data = {};
        $.each(arr,function(i,e){
            data[e] = $('[name='+e+']').val();
        });
        $.post('/coupon/remove',data,function(data){
            var json = $.parseJSON(data);
            if(json.status == 'success'){
                window.location.reload();
            }
        })
    });
    $('#ship_speed').change(function(){
       $.post('/checkout/shippingPriceUpdate',{speed:this.value,cartTotal:<?=$model->cart->total?>},function(data){
           $('strong.upd_total').text('$'+data.cartTotal);
           if(data.discount[0] == null && <?=isset($_SESSION['coupon'])?'true':'false'?>){
               alert('Your Shipping coupon does not apply to this option and has been removed');
               window.location.reload();
           } else {
               $('.discount-amt').text(data.discount[1]);
           }
           console.log(data);
       })
    });
	$(".button").on("click", function() {

		var $button = $(this);
		var oldValue = $button.closest('.numbersRow').find("input").val();

		if ($button.hasClass('inc')) {
			var newVal = parseFloat(oldValue) + 1;
		} else {
			// Don't allow decrementing below zero
			if (oldValue > 0) {
				var newVal = parseFloat(oldValue) - 1;
			} else {
				newVal = 0;
			}
		}

		$button.closest('.numbersRow').find("input").val(newVal);
		if(newVal==1){
			$('.button.dec').addClass('disabled');
		}else{
			$('.button.dec').removeClass('disabled');
		}
	});
</script>

