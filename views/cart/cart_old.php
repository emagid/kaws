<div class="banner cms">
	<div class="container">
		<div class="inner">
			<h1>Cart</h1>
		</div>
	</div>
</div>
<div class="cart">
		<div class="container container_inner">
						<? if (count($model->products) > 0) { ?>

			
				<div class="table-responsive">
					<table class="table table-products">

						<thead>
							<tr>
								<th width="5%">&nbsp;</th>
								<th width="16%">Image</th>
								<th width="31%">Product</th>
								<th width="16%">Price</th>
								<th width="16%">Color</th>
								<th width="16%">Size</th>
							</tr>
						</thead>


					<?php foreach($model->products as $token=>$product) {?>


						<tbody>
							<tr>
								
								<td width="5%">
									<a href="<?=SITE_URL?>cart/remove_product/<?=$token?>" class="close" aria-label="Delete Product">&times;</a>
								</td>
								
								<!-- IMAGE -->
								<td width="16%">	
									<?php 
							            $img_path = ADMIN_IMG.'modernvice_shoe.png'; //TODO st-dev default product image
							            if(!is_null($product->featured_image) && $product->featured_image != "" && file_exists(UPLOAD_PATH.'products'.DS.$product->featured_image)){ 
							             	$img_path = UPLOAD_URL . 'products/' . $product->featured_image;
							            }
							        ?>
					        		<a href="<?=SITE_URL.'product/'.$product->slug?>"><img src="<?php echo $img_path; ?>" /></a>
								</td>

								<!-- PRODUCT -->
								<td class="product_width" width="31%">
									<h4><a href=<?=SITE_URL.'product/'.$product->slug?>><?=$product->name?></a></h4>
									<span><?php echo $product->mpn; ?></span>
								</td>

								<!-- UNIT PRICE -->
						<?if(isset($_SESSION['price'])){
				 		$code= $_SESSION['code'];
						$new_price = \Model\Price_Alert::getItem(null,['where'=>" code =  '$code'"]);
						if($product->id == $new_price->product_id){
							$price = $new_price->price;
						}else{
							$price = $product->price;
						}
						}
						else{
							$price = $product->price;
						}?>

								<td class="product_width" width="16%">$<?=number_format($price, 2)?></td>
								<td class="product_width" width="16%"><?=\Model\Color::getItem($product->details['color'])->name?></td>
								<td class="product_width" width="16%"><?=$product->details['size']?></td>
							</tr>

					<?php } ?>

							<tr>
								<td colspan="7" class="link-bottom">
									<!--<a href="<?=SITE_URL?>cart/remove_all" class="link-blue pull-left">Empty Cart</a>-->
							 		<a href="<?=SITE_URL?>" class="link-blue pull-right">Continue Shopping</a>
								</td>
							</tr>

						</tbody>
					</table>
				</div>

				<br /> <br />

				<div class="row cart-bottom">

					<div class="col-sm-12 col-md-8">
						<h3>Apply discounts</h3>
						<div class="input-group form-group">
							<input name="coupon-code" type="text" class="form-control" placeholder="Coupon Code" value="<?=(isset($_SESSION['coupon'])?$_SESSION['coupon']:'')?>" >
							<span class="input-group-btn">
								<button class="btn btn-primary apply-coupon" type="button">Apply</button>
							</span>
						</div>
<!--						<div class="input-group form-group"></div>-->
<!--						<div class="input-group form-group">-->
<!--							<input name="gift-card" type="text" class="form-control" placeholder="Gift Card">-->
<!--							<span class="input-group-btn">-->
<!--								<button class="btn btn-primary apply-btn" type="button">Apply</button>-->
<!--							</span>-->
<!--						</div>-->
					</div>

					<div class="col-sm-24 col-md-8">
						<table class="table table-cart">
							<tbody>
								<tr>
									<th>Subtotal:</th>
									<td> 
										$<span id="subtotal-value"><?= number_format($model->subtotal, 2) ?></span>
									</td>
								</tr>
								<tr>
									<th>Savings:</th>
									<td id="savings-value">$0.00</td>
								</tr>
								<tr style="display:none;">
									<th>Tax:</th>
									<td>$<span id="tax-value">0.00</span></td>
								</tr>
								<tr>
									<th><h3>Grand Total</h3></th>
									<td><h3>$<span id="grand-total-value"><?=$model->cart->total?></span></h3></td>
								</tr>
							</tbody>
						</table>
						<hr />

						<? if (is_null($model->user)) { ?>								
							<a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#login">Checkout</a>
							<!-- Guest Checkout -->
							<a href="/orders/checkout?type=guest" class="btn btn-primary btn-block">Guest Checkout</a>
						<? } else { ?>
							<a href="/orders/checkout" class="btn btn-primary btn-block">Checkout</a>
						<? } ?>

						<!--p>OR</p>
						<p><a href="#"><img src="/content/frontend/img/button_amazon.png" alt="Pay with Amazon" class="img-responsive" /></a></p>
						<p><a href="#"><img src="/content/frontend/img/button_paypal.png" alt="Pay with PayPal" class="img-responsive" /></a></p-->
					</div>
				</div>
			
					<? } else { ?>
		<h3 class="text-center">Your cart is empty, <a href="<?=SITE_URL?>" class="link-blue">Continue Shopping!</a></h3>
		<? } ?>

		</div>
	</div>

<?= footer()?>

<script src="<?=FRONT_JS?>plugins/jquery.number.min.js"></script>

<script>
	$(function(){
		$('.apply-coupon').click(function(e){
			e.preventDefault();
			var code = $(this).parent().prev().val();
			$.getJSON('<?=SITE_URL?>cart/applyCoupon?code='+code, function(data){
				var total = parseFloat($('#subtotal-value').text().replace('$','').replace(',',''));
				if (data.error.code == 0){
					$('.apply-coupon').parent().parent().next().text('');
					var savings = parseFloat(data.coupon.discount_amount);
					var shipping_and_tax = 0;
//					var shipping_and_tax = parseFloat($('#shipping-value').text().replace(/,/g,'')) + parseFloat($('#tax-value').text().replace(/,/g,''));
					if (data.coupon.discount_type == 1){ //$
						$('#grand-total-value').text(parseFloat(total - savings + shipping_and_tax).toFixed(2));
						$('#savings-value').text('$'+parseFloat(savings).toFixed(2));
					} else if (data.coupon.discount_type == 2){ //%
						$('#grand-total-value').text($.number(total * (1-savings/100) + shipping_and_tax, 2));
						$('#savings-value').text('$' + $.number(total * savings/100, 2));
					}
				} else {
					$('.apply-coupon').parent().parent().next().text(data.error.message);
					$('#grand-total-value').text($.number(parseFloat(total)+parseFloat($('#shipping-value').text().replace(/,/g,'')), 2));
					$('#savings-value').text('$0.00');
				}
			})
			return false;
		})
		if ($('.apply-coupon').parent().prev().val() != ''){
			$('.apply-coupon').trigger('click');
		}
 	})
</script>
