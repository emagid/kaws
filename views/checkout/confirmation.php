<div class="pageWrapper confirmationPageWrapper confirm_page">
            <h1>Order Success</h1>
            <p>Thank you for your KAWSONE Order</p>
        <? if (isset($model->order)) {
            $order_products = \Model\Order_Product::getList(['where' => "order_id = {$model->order->id}"]) ?>
            <h3 class="grey">Products on the Way</h3>
            <div class='order_products_flex'>
                <? foreach ($order_products as $order_product) {
                    if(!$order_product->product_id) {
                        $product = new \Model\Product();
                        $product->id    = $order_product->product_id;
                        $product->name  = $order_product->details;
                        $product->price = $order_product->unit_price;
                    } else {
                        $product = \Model\Product::getItem($order_product->product_id);
                        $color = \Model\Color::getItem(json_decode($order_product->details, true)['color']);
                        $size = \Model\Size::getItem(json_decode($order_product->details, true)['size']);
                    }
                    ?>
                        <div class='ordered_products'>
                            <img width="100px"
                                 src="<?= $product->featuredImage() ?>" alt="<?=$product->image_alt?>"/>
                            <div class="product-name"><?= $product->name ?> <?=$order_product->quantity > 1?"(x $order_product->quantity)":""?></div>
            <!--                <div class="product-price">$--><?//= number_format($order_product->unit_price,2) ?><!--</div>-->
                            <div class="product-price">$<?= number_format($order_product->unit_price*$order_product->quantity,2) ?></div>
                        </div>
<!--                <div class="product-color">--><?//= $color->name ?><!--</div>-->
                <? } ?>
            </div>
            <p class="grey">Order Details</p>
            <div class='confirm_text_holder'>
                <div>
                    <label class='confirmation_label'>Order Number </label>
                    <div class="confirmation_text order-id"><?= $model->order->id ?></div>
                </div>
                <!--<div>
                    <label class='confirmation_label'>Reference Number</label>
                    <div class="confirmation_text order-ref_num"><?/*= $model->order->ref_num */?></div>
                </div>-->
                <div>
                    <label class='confirmation_label'>Subtotal</label>
                    <div class="confirmation_text order-subtotal">$<?= number_format($model->order->subtotal,2) ?></div>
                </div>
                <div>
                    <label class='confirmation_label'>Shipping</label>
                    <div class="confirmation_text order-shipping">$<?= number_format($model->order->shipping_cost,2) ?></div>
                </div>
                <? if($model->order->ship_country=='United States'){ ?>
                <div>
                    <label class='confirmation_label'>Taxes</label>
                    <div class="confirmation_text order-tax">$<?= number_format($model->order->tax,2) ?></div>
                </div>
                <? } else { ?>
                <div>
                    <label class='confirmation_label'>Duty & Taxes</label>
                    <div class="confirmation_text order-tax">$<?= number_format($model->order->duty,2) ?></div>
                </div>
                <? }?>
                <div>
                    <label class='confirmation_label'>Total</label>
                    <div class="confirmation_text confirmation_border order-total">$<?= number_format($model->order->total,2) ?></div>
                </div>
            </div>

            <p class="grey">Your Information</p>
            <div class='confirm_text_holder'>
                <div>
                    <label class='confirmation_label'>Name</label>
                    <div class="confirmation_text order-name"><?= $model->order->shipName() ?></div>
                </div>
                <div>
                    <label class='confirmation_label'>Address</label>
                    <div class="confirmation_text order-addr"><?= $model->order->getShippingAddr() ?></div>
                </div>
                <div>
                    <label class='confirmation_label'>Country</label>
                    <div class="confirmation_text order-country"><?= $model->order->ship_country ?></div>
                </div>
                <div>
                    <label class='confirmation_label'>Email</label>
                    <div class="confirmation_text order-email"><?= $model->order->email ?></div>
                </div>
                <div>
                    <label class='confirmation_label'>Phone</label>
                    <div class="confirmation_text order-phone confirmation_border"><?= $model->order->phone ?></div>
                </div>


        <? } ?>
    </div>
</div>
<!-- Google Code for Purchase Conversion Page -->
    <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 874964450;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "NyVmCNGJkWoQ4suboQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<script type="text/javascript">
    //make a signify post
    $(document).ready(function(){
        $.post('/orders/setSignify_id/', {id:<?=$model->order->id?>}, function(data){
            if (data.status == "success" ) {
                console.log("case saved");
            }
        });
    });

</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/874964450/?label=NyVmCNGJkWoQ4suboQM&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

