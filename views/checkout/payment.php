<style>
	.row:before{
		position: relative;
	}
    input.pay_field_invalid {
        border: 2px solid red !important;
    }
    input.pay_field_valid {
        border: 2px solid #d6d6d6 !important;
    }
    input[disabled]{
        opacity: .3;
    }
</style>
<div class="pageWrapper checkoutPageWrapper cartPageWrapper shippingCheckoutPageWrapper emailCheckoutPageWrapper shippingCheckoutPageWrapper">
	<div class="row content_width_1000">
        <div class='divider'>
		
            <div class="emailFormWrapperCol cartWrapperCol col">
                    <h1>CART</h1>
                    <p class='status'>Order Summary</p>
                <section id='cart'>
                    
                    <div class="labelDataBox orderSummaryDataBox">


                        <ul class="paymentScreenBagItems">
                            <? foreach($model->cart->cart as $cart){
                                if($cart->product_id) {
                                    $product = \Model\Product::getItem($cart->product_id);
                                    $url = '/products/' . $product->slug;
                                    $img = $product->featuredImage();
                                    $name = $product->name;
                                    $price = $product->getPrice(null,null,false,$model->user);
                                } else {
                                }?>
                                <li>
                                    <div class="col bagItemImg">
                                            <div class="col item">
                                                <img src="<?=$img?>">
                                            </div>
                                            <div class="col item_info">
                                                <div class='item_name'>
                                                    <p class='name'><?=$name?> <?=$cart->quantity > 1 ? "(x $cart->quantity)" :"" ?></p>
                                                </div>
                                                <div class='item_quanity'>
                                                    <? $cProd = \Model\Campaign_Product::getItem(null,['where'=>"product_id = $cart->product_id"])?>
                                                        <? if($cart->quantity < $cProd->max_per_user) {?>
                                                            <a class="button cartUpdate-btn" data-operation="inc" data-quantity="<?=$cart->quantity?>" data-cart_id="<?=$cart->id?>" style="width:10%; margin: 0px; font-weight: bold">+</a>
                                                        <? } ?>
                                                        <? if($cart->quantity > 1) {?>
                                                            <a class="button cartUpdate-btn" data-operation="dec" data-quantity="<?=$cart->quantity?>" data-cart_id="<?=$cart->id?>" style=" margin: 0px">-</a>
                                                    <? } ?>
                                                    <a class='remove' href="/cart/remove_product/<?=$cart->id?>">Remove</a>
                                                </div>
                                            </div>
                                    </div>
                                    <p class='item_price'>$<?=number_format($cart->quantity*$price,2)?></p>
                                </li>
                            <?}?>
                        </ul>
                        <div class='final_price'>
                            
                       
                            <p class="price_bag"><b>Subtotal</b>$<?=$model->cart? number_format($model->cart->total,2): 0.00?></p>
                            <p class="price_bag"><b>Shipping</b><span class="upd_shipping_cost">$<?=number_format($model->cart->shipping,2)?></span></p>
                            <p class="price_bag duty_fees"><b class="upd_duty_name">Duty</b><span class="upd_duty_cost">$<?=number_format($model->cart->duty,2)?></span></p>
                            <?if($model->cart->discount){?>
                                <p class="price_bag"><b>Discount</b>-$<span class="discount-amt"><?=number_format($model->cart->discount,2)?></span></p>
                            <?}?>
                            <span class="line_sep"></span>
                            <div class="orderSummaryTotal"><span class="triangleBorder"></span><p><b>Total</b>$<?=$model->cart ? number_format($model->cart->total - $model->cart->discount + $model->cart->shipping,2): 0.00?></p></div>
                         </div>
                    </div>
                </section>


                 <!-- <button class='continue button'>CONTINUE</button> -->

                <div style="display:none!important;">
                    <?if(isset($model->cart->discount)&&($model->cart->discount)){?>
                        <p>Discount Applied | <span class="pinkDiscount">- $<?=number_format($model->cart->discount,2)?></span> </p>
                    <?} else {?>
                        <label for="email"><p>Redeem a coupon</p></label>
                        <input type="text"class="cpn-code">
                        <a id="do_coupon" class="btn btn_primary apply-cpn">Apply Coupon</a>
                    <?}?>
                </div>
            </div>
        </div>


        <div id='forms' class="emailFormWrapperCol shippingFormWrapperCol col">


        <div class='divider shipping'>
            <h1>Billing Address</h1>
            <h8 class="ship_statement">Your order will be shipped to your billing address</h8>
            <div class="emailFormWrapperCol shippingFormWrapperCol col">
                <form id="email_form">
                    <!-- <p style='margin-bottom: 10px'>Email</p> -->
                    <? $email = '';
                    if (isset($_SESSION['customerDetails']) && array_key_exists('email', json_decode($_SESSION['customerDetails'], true))) {
                        $email = json_decode($_SESSION['customerDetails'], true)['email'];
                    } else if ($model->user) {
                        $email = $model->user->email;
                    } ?>
                    <!-- all visible form fields will have .pay_field_valid or .pay_field_invalid after changing -->
                    <input placeholder='Email' type="email" name="email" maxlength="60" tabindex="1" class="checkOutInput" id="checkoutEmail" value="<?=$email?>" required>
                    <input type="hidden" name="onepage" value="1" />
                    <input type="password" name="password" maxlength="60" tabindex="1" class="checkOutInput" style="display:none" disabled>
                </form>
                <h8 class="form_statement">Please submit a valid email address so that we can keep you informed on the status of your order.</h8>
            </div>
            <form id="delivery_form" autocomplete="off">
                <input type="hidden" name="onepage" value="1" autocomplete="false">
                <input type="hidden" name="shipping_is_billing" value="on">
                <? if(isset($model->addresses)) {?>
                    <select name="stored_addr">
                        <option>Select stored address...</option>
                        <? foreach($model->addresses as $address) {
                            echo "<option value='$address->id' data-f_name='$address->first_name' data-l_name='$address->last_name' data-phone='$address->phone' data-address='$address->address' data-address2='$address->address2' data-city='$address->city' data-state='$address->state' data-zip='$address->zip' data-country='$address->country' >$address->label</option>";
                        }?>
                    </select>
                <?}?>
                <?$session = json_decode($_SESSION['customerDetails'],true);
                //Real bad
                $ship_first_name = isset($_SESSION['customerDetails']) && array_key_exists('ship_first_name',$session) ? $session['ship_first_name']: '';
                $ship_last_name = isset($_SESSION['customerDetails']) && array_key_exists('ship_last_name',$session) ? $session['ship_last_name']: '';
                $phone = isset($_SESSION['customerDetails']) && array_key_exists('phone',$session) ? $session['phone']: '';
                $ship_address = isset($_SESSION['customerDetails']) && array_key_exists('ship_address',$session) ? $session['ship_address']: '';
                $ship_address2 = isset($_SESSION['customerDetails']) && array_key_exists('ship_address2',$session) ? $session['ship_address2']: '';
                $ship_city = isset($_SESSION['customerDetails']) && array_key_exists('ship_city',$session) ? $session['ship_city']: '';
                $ship_country = isset($_SESSION['customerDetails']) && array_key_exists('ship_country',$session) ? $session['ship_country']: '';
                $ship_country = $ship_country?:'United States';
                $ship_state = isset($_SESSION['customerDetails']) && array_key_exists('ship_state',$session) ? $session['ship_state']: '';
                $ship_zip = isset($_SESSION['customerDetails']) && array_key_exists('ship_zip',$session) ? $session['ship_zip']: '';

                $user = $model->user ? : null;
                if($user){
                    $ship_first_name = isset($_SESSION['customerDetails']) && array_key_exists('ship_first_name',$session) ? $session['ship_first_name']: $user->first_name;
                    $ship_last_name = isset($_SESSION['customerDetails']) && array_key_exists('ship_last_name',$session) ? $session['ship_last_name']: $user->last_name;
                    $phone = isset($_SESSION['customerDetails']) && array_key_exists('phone',$session) ? $session['phone']: $user->phone;
                }
                $address = $model->user && $model->user->getAddress() ? $model->user->getAddress(): null;
                if($address){
                    $ship_address = isset($_SESSION['customerDetails']) && array_key_exists('ship_address',$session) ? $session['ship_address']: $user->address;
                    $ship_address2 = isset($_SESSION['customerDetails']) && array_key_exists('ship_address2',$session) ? $session['ship_address2']: $user->address2;
                    $ship_city = isset($_SESSION['customerDetails']) && array_key_exists('ship_city',$session) ? $session['ship_city']: $user->city;
                    $ship_country = isset($_SESSION['customerDetails']) && array_key_exists('ship_country',$session) ? $session['ship_country']: $user->country;
                    $ship_state = isset($_SESSION['customerDetails']) && array_key_exists('ship_state',$session) ? $session['ship_state']: $user->state;
                    $ship_zip = isset($_SESSION['customerDetails']) && array_key_exists('ship_zip',$session) ? $session['ship_zip']: $user->zip;
                }
                ?>
                
                <div class="form50">
                    <!-- <p for="checkoutFName">First Name</p> -->
                    <input placeholder='First Name' type="text" name="ship_first_name" maxlength="60" tabindex="1" class="checkOutInput" id="checkoutFName" value="<?=$ship_first_name?>" required>
                </div>
                
                <div class="form50">
                    <!-- <p for="checkoutLName">Last Name</p> -->
                    <input placeholder='Last Name'type="text" name="ship_last_name" maxlength="60" tabindex="2" class="checkOutInput" id="checkoutLName" value="<?=$ship_last_name?>" required>
                </div>

                <div class="form100 suggested_addr">

                </div>

                <div class="form50">
                <!-- <p for="checkoutAddress1">Address Line 1</p> -->
                <input placeholder='Address 1' type="text" name="ship_address" maxlength="60" tabindex="4" class="checkOutInput" id="checkoutAddress1" value="<?=$ship_address?>" required  autocomplete="new-password">
                </div>
                
                <div class="form50">
                <!-- <p for="checkoutAddress2">Address Line 2 <span class="optional">(optional)</span></p> -->
                <input autocomplete="new-password" placeholder='Address 2 (optional)' type="text" name="ship_address2" maxlength="60" tabindex="5" class="checkOutInput" id="checkoutAddress2" value="<?=$ship_address2?>" >
                    </div>
                
                <div class="form50">
                <!-- <p for="checkoutCity">City</p> -->
                <input placeholder='City' type="text" name="ship_city" maxlength="60" tabindex="6" class="checkOutInput" id="checkoutCity" value="<?=$ship_city?>" required autocomplete="new-password">
                <input type="hidden" id="placeId" />
                </div>
                
                <div class="form30" id="no_margin">
                <!-- <p for="checkoutZip">Country</p> -->
                <fieldset class="selectUIWrapper countrySelectWrapper">
                    <select name="ship_country">
                        <?foreach(get_countries() as $code=> $country){?>
                            <option value="<?=$country?>" data-code="<?=$code?>" <?=$ship_country == $country ? 'selected': ''?>><?=$country?></option>
                        <?}?>
                    </select>
                    <span class="downArrowICon">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                                    <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                                    L-321,348.1z"></path>
                                </svg>
                    </span>
                </fieldset>
                </div>
                
                <div class="form30 last">
                <div id="us_states" <?= $ship_country == 'United States' ? '' : 'style="display: none"' ?>>
                    <!-- <p for="checkoutRegion">County / State / Region *</p> -->
                    <fieldset class="selectUIWrapper countrySelectWrapper">
                        <select name="ship_state" id="checkoutRegionUS" required>
                            <?foreach(get_states() as $short=>$long){?>
                                <option value="<?=$short?>" <?=$ship_state == $short ? 'selected': ''?>><?=$long?></option>
                            <?}?>
                        </select>
                        <span class="downArrowICon">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                                <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                                L-321,348.1z"></path>
                            </svg>
                        </span>
                    </fieldset>
                </div>
                </div>
                
      
                <div id="foreign_states" <?=$ship_country == 'United States' ? 'style="display: none"': ''?>>
                    <p for="checkoutRegion">County / State / Region *</p>
                    <input <?=$ship_country == 'United States' ? 'disabled': ''?> type="text" name="ship_state" maxlength="60" tabindex="7" class="checkOutInput" id="checkoutRegion" value="<?=$ship_state?>" required>
                </div>
              
                <div class="form50 ">
                <!-- <p for="checkoutZip">Postcode / Zip Code *</p> -->
                <input placeholder='Postcode / Zip Code' type="text" id="shipzip" name="ship_zip" maxlength="60" tabindex="8" class="checkOutInput" id="checkoutZip" value="<?=$ship_zip?>" required autocomplete="new-password">
                </div>

                <div class="form50">
                    <!-- <p for="checkoutPhone">Phone <span class="optional"></span></p> -->
                    <input placeholder='Phone' type="text" name="phone" maxlength="60" tabindex="3" class="checkOutInput" id="checkoutPhone" value="<?=$phone?>" required>
                </div>
                <h8 class="phone_statement ship_statement">Provide a valid phone for UPS to contact you.</h8>

                <input name="email" style="display: none" value="<?=$session['email']?>"/>
                <?if($user){?>
                    <input type="hidden" name="email"  value="<?=$user ? $user->email: ''?>" >
                <?}?>
            </form>
        </div>
        <div class="emailFormWrapperCol shippingFormWrapperCol paymentFormWrapperCol col">
            <div class='divider'>
                
            <form id='bill'method="post" action="<?=$this->emagid->uri?>" novalidate>
                <input name="email" style="display: none" value="<?=$session['email']?>" />

                <!--			--><?//=dd($model->cart->products,$model->cart->cart)?>
                <h1>Billing & Payment</h1>
                <?if(false){//hide gift card
                    //				<?if(!$model->wholesale){
                    if(!isset($_SESSION['gift_card'])){?>
                        <h7 class="as_l">Gift Card Code</h7>
                        <div class="giftCardSection">
                            <input type="text" name="gift-card-code" id="giftCardInput">
                            <input type="button" value="Apply" id="redeem-gift-card" class="btn btn_black">
                        </div>
                    <?} else {?>
                        <h7 class="as_l">Gift Card: <b><?=($gc = \Model\Gift_Card::validateGiftCard($_SESSION['gift_card'])) ? $gc->name: $_SESSION['gift_card']?></b></h7>
                        <input type="button" value="Remove" style="cursor: pointer" id="remove-gift-card" class="btn_black">
                    <?}?>
                <?}?>
                <!--<div id="paypalPayment" style="display:none" class="checkboxUX optionBoxCheckboxUIWrapper highlightedCheckoutOptionBox optionBox">
                    <div class="checkboxUI">
                        <icon class="check"></icon>
                    </div>

                    <p>continue with <span class="media" style="background-image:url('<?/*=FRONT_IMG*/?>paypalLogo.png')"></span> below</p>
                    <input type="checkbox" name="paypal">
                </div>-->

                <div class="creditCardPaymentFormSection" >
                    
                <div style="width: 97%">
                    
                    <!-- <p for="cardUserName">Name on Card</p> -->
                    <input placeholder='Name on Card' type="text" name="card_name" maxlength="60" class="card_name" id="cardUserName" required>
                    </div>
                    
                <div style="width: 97%">
                    
                    <!-- <p for="ccnumber">Credit Card Number</p> -->
                    <div class="cardNumberInputWrapper">
                        <input placeholder='Credit Card Number' type="text" pattern="[0-9]*" name="cc_number" maxlength="16" class="card_num" id="ccnumber"  required>
                        <div id="creditCardIcons">
                            <div class="media"></div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="form50" id="no_margin">
                    <input type="hidden" name="cc_type" id="cc_type">
                    <!-- <p for="checkoutPhone">Expires on</p> -->
                    <fieldset class="selectUIWrapper selectExpMonth">
                        <select style='margin-top: 8px;' name="cc_expiration_month" required>
                            <?foreach(get_month() as $value=>$item){?>
                                <option value="<?=$value?>"><?=$item?></option>
                            <?}?>
                        </select>
                        <span class="downArrowICon">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                                        <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                                        L-321,348.1z"></path>
                                    </svg>
                        </span>
                    </fieldset>
                    </div>
                    
                    <div class="form50">
                    <fieldset class="selectUIWrapper selectExpYear">
                        <select style='margin-top: 8px;' name="cc_expiration_year" required>
                            <?$carbon = \Carbon\Carbon::now()->year;
                            for($i = $carbon; $i < $carbon+14; $i++){?>
                                <option value="<?=$i?>"><?=$i?></option>
                            <?}?>
                        </select>
                        <span class="downArrowICon">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                                        <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                                        L-321,348.1z"></path>
                                    </svg>
                        </span>
                    </fieldset>
                    </div>
                    <!--			<input type="text" name="card_expire" tabindex="3" class="card_expire" id="checkoutPhone">-->
                    <div style='width: 50%; position: relative; max-width: 200px;'>
                    <p class='secode' for="secCode" style='max-width: 200px; position: relative; width: 100%;'>
                        <div style="display:inline-block" class="cvv_help_box">
                            <div class="trigger">
                                <span class="as_m">?</span>
                            </div>
                            <div class="ccv_help_box">
                                <p>For security reasons, please enter your card verification number (CVV) or card identification number (CID). Find this three-digit number on the back of your Visa, MasterCard and Discover cards in the signature area, or the four-digit number on the front of your American Express card, above the credit card number.</p>
                                <span class="ccv_tip_icons">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="384.5 10.7 201.9 58.3" enable-background="new 384.5 10.7 201.9 58.3" xml:space="preserve">
                                        <g id="cvv_1_">
                                            <g>
                                                <path fill="#C3C3C3" d="M468.3,59.9c0,3.7-3,6.5-6.8,6.5h-67.9c-3.8,0-6.8-2.9-6.8-6.5V20.6c0-3.7,3-6.5,6.8-6.5h67.9
                                                    c3.8,0,6.8,2.9,6.8,6.5L468.3,59.9C468.3,59.9,468.3,59.9,468.3,59.9z"></path>
                                            </g>
                                            <rect x="386.8" y="22.3" fill="#222222" width="81.5" height="10.5"></rect>
                                            <rect x="393.2" y="38" fill="#FFFFFF" width="48.9" height="10.5"></rect>
                                            <rect x="397.1" y="41.9" fill="#222222" width="40.9" height="2.6"></rect>
                                            <rect x="449.8" y="38" fill="#FFFFFF" width="11.3" height="10.5"></rect>
                                            <rect x="451.1" y="41.9" fill="#222222" width="8.4" height="2.6"></rect>
                                            <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="455.4" cy="43.2" rx="10.6" ry="10.3"></ellipse>
                                        </g>
                                        <g id="cvv-amex_1_">
                                            <g id="cvv-amex">
                                                <g>
                                                    <g>
                                                        <path fill="#5EC1EC" d="M586.4,59.7c0,3.8-3.1,6.7-7,6.7h-69.7c-3.9,0-7-3-7-6.7V19.4c0-3.8,3.1-6.7,7-6.7h69.7c3.9,0,7,3,7,6.7
                                                            V59.7z"></path>
                                                        <g>
                                                            <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
                                                                c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
                                                            <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
                                                                v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
                                                                "></path>
                                                        </g>
                                                        <g>
                                                            <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
                                                                c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
                                                            <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
                                                                v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
                                                                "></path>
                                                        </g>
                                                    </g>
                                                    <rect x="507.5" y="43.9" display="none" fill="#FFFFFF" width="69.7" height="10.6"></rect>
                                                    <rect x="513.4" y="47.9" fill="#222222" width="56.1" height="2.7"></rect>
                                                    <rect x="565.3" y="31.4" fill="#FFFFFF" width="11.1" height="10.7"></rect>
                                                    <rect x="566.7" y="35.4" fill="#222222" width="8.4" height="2.7"></rect>
                                                </g>
                                                <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="570.8" cy="36.8" rx="10.9" ry="10.5"></ellipse>
                                            </g>
                                        </g>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </p>
                    <input style='max-width: 200px;' type="text" name="cc_ccv" placeholder="Security Code" maxlength="4" tabindex="4" class="card_cvv" id="secCode" required>

                    </div>
                    <div>
                        <p class='form_statement' for="agree_to_terms" style='max-width: 200px; position: relative; width: 100%;'>
                            You must agree to the <a onclick="$('#terms').click()">Terms of Service</a> to checkout.
                        </p>
                    <input id="agreeTerms" type="checkbox" name="agree_to_terms" />
                    </div>
                </div>
                

                <input name="payment_method" type="hidden" value="1"/>



                <?$sessionData = json_decode($_SESSION['customerDetails'],true)?>
                <?$checkBilling = $sessionData['shipping_is_billing'] == 'on' ? true: $sessionData['shipping_is_billing'] === null ? true :false;


                $shippingState=$sessionData['ship_state'];
                $shippingState = preg_replace('/\s+/', '', strtolower($shippingState));
                $shipping = $model->shipping;
                if($shippingState=='ny'||$shippingState=='newyork'){
                    $tax_rate = 8.875;
                }else if($shippingState=='nj'||$shippingState=='newjersey'){
                    $tax_rate = 6.875;
                }else{
                    $tax_rate = 0;
                }?>
                <div id="isSameAddress" style="" class="checkboxUX optionBoxCheckboxUIWrapper highlightedCheckoutOptionBox optionBox <?=$checkBilling ? 'selected': ''?>">
                    <div class="checkboxUI">
                        <icon class="check"></icon>
                    </div>
                    <p>Billing Address is the same as Shipping Address</p>
                    <input type="checkbox" readonly <?=$checkBilling ? 'checked="checked"': ''?>>
                </div>

                <br>
                <div class="billingAddressInfo <?=$checkBilling ? 'hiddenShippingInfo': ''?>" style="">
                    <div class="form50">
                        <!-- <label for="fname">First Name</label> -->
                        <input type="text" placeholder='First Name'  name="bill_first_name" maxlength="60" tabindex="1" class="checkOutInput" id="fname" required value="<?=$checkBilling ? $sessionData['ship_first_name']: ''?>">
                    </div>

                    <div class="form50">
                    <label for="lname">Last Name</label>
                    <input type="text" name="bill_last_name" maxlength="60" tabindex="2" class="checkOutInput" id="lname" required value="<?=$checkBilling ? $sessionData['ship_last_name']: ''?>">
                    </div>
                    
                    <div class="form50">
                    <label for="checkoutPhone">Phone <span class="optional"></span></label>
                    <input type="text" name="bill_phone" maxlength="60" tabindex="3" class="checkOutInput" id="checkoutPhone" value="<?=$checkBilling ? $sessionData['phone']: ''?>">
                    </div>
                    <input type="hidden" id="place_id" name="place_id"/>
                    
                    <div class="form50">
                    <label for="checkoutAddress1">Address Line 1</label>
                    <input type="text" name="bill_address" maxlength="60" tabindex="4" class="checkOutInput" id="checkoutAddress1" required value="<?=$checkBilling ? $sessionData['ship_address']: ''?>">
                    </div>
                    
                    <div class="form50">
                    <label for="checkoutAddress2">Address Line 2 <span class="optional">(optional)</span></label>
                    <input type="text" name="bill_address2" maxlength="60" tabindex="5" class="checkOutInput" id="checkoutAddress2" value="<?=$checkBilling ? $sessionData['ship_address2']: ''?>">
                    </div>
                    
                    <div class="form50">
                    <label for="checkoutCity">City</label>
                    <input type="text" name="bill_city" maxlength="60" tabindex="6" class="checkOutInput" id="checkoutCity" required value="<?=$checkBilling ? $sessionData['ship_city']: ''?>">
                    </div>
                    
                    <div class="form30" id="no_margin">
                    <label for="checkoutCountry">Country</label>
                    <fieldset class="selectUIWrapper countrySelectWrapper">
                        <select name="bill_country" id="checkoutCountry">
                            <? foreach (get_countries() as $code => $country) { ?>
                                <option value="<?= $country ?>" data-code="<?=$code?>" <?= $checkBilling && $sessionData['ship_country'] == $country ? 'selected' : '' ?>><?= $country ?></option>
                            <? } ?>
                        </select>
                        <span class="downArrowICon">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                            <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                            L-321,348.1z"></path>
                        </svg>
                    </span>
                    </fieldset>
                    </div>
                    
                    <div class="form30">
                    <div id="us_states" <?= $checkBilling && $sessionData['ship_country'] == 'United States' ? '' : 'style="display: none"' ?>>
                        <label for="checkoutRegion">County / State / Region *</label>
                        <fieldset class="selectUIWrapper countrySelectWrapper">
                            <select name="bill_state" id="checkoutRegionUS" required <?= $checkBilling && $sessionData['ship_country'] == 'United States' ? '' : 'disabled' ?>>
                                <? foreach (get_states() as $short => $long) { ?>
                                    <option value="<?= $short ?>" <?= $checkBilling && $sessionData['ship_state'] == $short ? 'selected' : '' ?>><?= $long ?></option>
                                <? } ?>
                            </select>
                            <span class="downArrowICon">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                                    <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                                    L-321,348.1z"></path>
                                </svg>
                            </span>
                        </fieldset>
                    </div>
                    <div id="foreign_states" <?=$checkBilling && $sessionData['ship_country'] == 'United States' ? 'style="display: none"': ''?>>
                        <label for="checkoutRegion">County / State / Region *</label>
                        <input <?=$checkBilling && $sessionData['ship_country'] == 'United States' ? 'disabled':''?> type="text" name="bill_state" maxlength="60" tabindex="7" class="checkOutInput" id="checkoutRegion" value="<?=$checkBilling ? $sessionData['ship_state']: ''?>" required>
                    </div>
                    </div>
<!--                    <input type="hidden" name="bill_state" maxlength="60" tabindex="7" class="checkOutInput" value="--><?//=$checkBilling ? $sessionData['ship_state']: ''?><!--" required>-->
                    
                    <div class="form30">
                    <label for="checkoutZip">Postcode / Zip Code *</label>
                    <input type="text" name="bill_zip" maxlength="60" tabindex="8" class="checkOutInput" id="checkoutZip" required value="<?=$checkBilling ? $sessionData['ship_zip']: ''?>">
                    </div>
                </div>
                <input name="tax_rate" value="<?=$tax_rate?>" hidden />
                <input type="hidden" name="noajax" value="true"/>
                <div class="row secondaryFocus">
                    <div class="right_float total_costs">
                        <input type="submit" value="Place Order" id="creditCardPayBtn" class="button" disabled>
                        <div class='total_cost orderSummaryTotal'>
                            <p style='font-size: 16px;'>$<?=$model->cart ? number_format($model->cart->total - $model->cart->discount + $model->cart->shipping,2): 0.00?></p>
                        </div>
<!--                        <input type="button" value="Proceed with Paypal" id="paypalPayBtnClicker" class="button">-->
                    </div>
                </div>

                <input name="shipping" type="hidden" value="<?=$model->cart->shipping?>"/>
                <input name="duty" type="hidden" value="<?=$model->cart->duty?>"/>
                <input name="shipping_method" type="hidden" value="0"/>
                <?if(isset($model->cart->discount)&&($model->cart->discount)){?>
                    <input type="text" name="coupon" value="<?=$model->cart->discount?>" hidden>
                <?}?>
                <input name="gift_card" type="hidden" value="<?=$model->giftCardFullCharge?>"/>
                <input name="gift_card_code" type="hidden" value="<?=isset($_SESSION['gift_card']) && $_SESSION['gift_card'] ? $_SESSION['gift_card']: '';?>"/>
                <!--			<form id="bill" method="post" action="/checkout/payment">-->
                <!--			</form>-->
            </form>
            </div>
        </div>

        <div class="shoppingInfoCol col shoppingInfoColPayment" id="theFixed">
            <div class="labelDataBox orderSummaryDataBox">
                <div class="header">
                    <h1 class="dji-text">Order Summary</h1>
                </div>
				<ul class="paymentScreenBagItems">
					<? foreach($model->cart->cart as $cart){
						if($cart->product_id) {
							$product = \Model\Product::getItem($cart->product_id);
							$url = '/products/' . $product->slug;
							$img = $product->featuredImage();
							$name = $product->name;
							$price = $product->getPrice(null,null,false,$model->user);
						} else {
//							$detail = json_decode($cart->details,true);
//							$gc = \Model\Gift_Card::getItem($detail['gift_card_id']);
//
//							$url = '/gift_cards/product/'.$detail['gift_card_id'];
//							$img = UPLOAD_URL.'gift_cards/'.$gc->image;
//							$name = $gc->name;
//							$colorText = '';
//							$sObjUs = '';
//							$sObjEu = '';
//							$price = number_format($detail['amount'],2);
						}?>
						<li>
							<div class="col bagItemImg">
								<a href="<?=$url?>" class="row">
									<div class="col">
										<p><?=$name?></p>
										<!--										<p>--><?//=$colorText?><!--</p>-->
										<!--										<p>US: --><?//=$sObjUs?><!-- EU: --><?//=$sObjEu?><!--</p>-->
										<p>$<?=number_format($cart->quantity*$price,2)?></p>
									</div>
									<div class="col">
										<img src="<?=$img?>">
									</div>
								</a>
							</div>
						</li>
					<?}?>
				</ul>
				<p class="price_bag"><b>Subtotal</b>$<?=$model->cart? number_format($model->cart->total,2): 0.00?></p>
                <p class="price_bag"><b>Shipping</b><span class="upd_shipping_cost">$<?=number_format($model->cart->shipping,2)?></span></p>
                <p class="price_bag"><b>Duty</b><span class="upd_duty_cost">$<?=number_format($model->cart->duty,2)?></span></p>
                <?if($model->cart->discount){?>
                    <p class="price_bag"><b>Discount</b>-$<span class="discount-amt"><?=number_format($model->cart->discount,2)?></span></p>
				<?}?>
				<span class="line_sep"></span>
				<div class="orderSummaryTotal"><span class="triangleBorder"></span><p><b>Total</b>$<?=$model->cart ? number_format($model->cart->total - $model->cart->discount + $model->cart->shipping,2): 0.00?></p></div>
			</div>
			<div class="redeem_coupon">
				<?if(isset($model->cart->discount)&&($model->cart->discount)){?>
					<p>Discount Applied | <span class="pinkDiscount">- $<?=number_format($model->cart->discount,2)?></span> </p>
				<?} else {?>
					<label for="email">Redeem a coupon</label>
					<input type="text" class="cpn-code">
					<a id="do_coupon" class="btn btn_primary apply-cpn">Apply</a>
				<?}?>
			</div>

			<div class="labelDataBox">
				<label>Help with your Order</label>
				<p>You can reach us at <a href="tel:2123333990">212 333 3990</a></p>			
                <p><a href="/pages/return-and-shipping-policy">Return & Shipping Policy</a></p>
                <img src="<?=FRONT_IMG?>creditCardOptions.png">
            </div>
<!--
			<div class="labelDataBox">
				<label>Returns Policy</label>
				<p><b>Easy Returns</b>Returns service: All products may be exchanged or returned within 14 days of the invoice date. Thereafter, all sales are final. </p>
			</div>
			<div class="labelDataBox">
				<label>Secure Shopping</label>
				<p>To ensure the safety of your credit card data at all times, we use Secure Socket Layer (SSL) technology and the highest security standards, certified by Verisign™ and Trustwave.</p>
				<img src="<?=FRONT_IMG?>creditCardOptions.png">
			</div>
-->
		</div>
	</div>
</div>

<script type="text/javascript"> //Address Validation
    $(document).ready(function(){
        $('form').attr('autocomplete','off');
        $(':input').live('focus',function(){
            $('form').attr('autocomplete','off');
            $(this).attr('autocomplete', 'off');
        });
    });



    var asService;
    var lsService;
    var activateSuggestions = true;
    function initService() {
        asService = new google.maps.places.AutocompleteService();
        lsService = new google.maps.Geocoder;
        console.log('sdsd');
    }
    function initMap() {
        var input = $('input#checkoutAddress1')[0];
        lsService = new google.maps.Geocoder;
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed',function () {
            var result = autocomplete.getPlace();
            activate(result);
            // $('#place_id').val(place.place_id);
        });
    }

    function displaySuggestions(predictions, status){
        $('.suggested_addr').empty();
        if (status != google.maps.places.PlacesServiceStatus.OK) {
            if(status == google.maps.places.PlacesServiceStatus.ZERO_RESULTS ){
                $('.suggested_addr').append('<div>Zero results</div>');
            } else {
                $('.suggested_addr').append('<div>Error occurred searching address</div>');
            }
            return;
        }

        predictions.forEach(function(prediction) {
            if(prediction.types && prediction.types.includes('street_address')){
                var suggestion = $('<div><a class="btn location_choices" data-place_id="'+prediction.place_id+'" >'+prediction.description+'</a></div>');
                $('.suggested_addr').append(suggestion);
            }
        });
    }
    function getSuggestions() {
        $('#creditCardPayBtn').prop('disabled',true);
        if(!activateSuggestions){ return; }
        var addr1 = $('#checkoutAddress1').val();
        var addr2 = $('#checkoutAddress2').val();
        var city = $('#checkoutCity').val();
        var ctry = $('#checkoutCountry').val();
        var zip = $('#shipzip').val();
        var state = '';
        if($('#checkoutRegion:visible').length){
            state = $('#checkoutRegion').val();
        } else if($('#checkoutRegionUS:visible').length){
            state = $('#checkoutRegionUS').val();
        }

        var address = addr1+' '+city+', '+zip+' '+ctry;
        asService.getQueryPredictions({ input: address}, displaySuggestions);
    }
    function activate(result){
        activateSuggestions = false;
        var addr1 ='';
        var street_num = '';
        var route = '';
        var city =[];
        var ctry ='';
        var zip ='';
        var state ='';
        var place_id = result.place_id;
        result.address_components.forEach(function(component){
            if(component.types.includes('street_number')){
                street_num = component.long_name;
            }
            if(component.types.includes('route')){
                route = component.long_name;
            }
            if(component.types.includes('postal_code')){
                zip = component.long_name;
            }
            if(component.types.includes('country')){
                ctry = component.long_name;
            }
            if(component.types.includes('locality') || component.types.includes('sublocality')){
                city = component.long_name;
            }
            if(component.types.includes('administrative_area_level_1')){
                state = [component.long_name,component.short_name];
            }
        });
        addr1 = street_num+' '+route;
        $('#checkoutAddress1').val(addr1).change();
        $('#checkoutCity').val(city).change();
        $('#shipzip').val(zip).change();
        $('#checkoutCountry').val(ctry).change();
        $('#checkoutRegion').val(state[1]).change();
        $('#checkoutRegionUS').val(state[1]).change();
        $('#creditCardPayBtn').prop('disabled',false);
        // $('#creditCardPayBtn').removeClass('disable_input');
        $('#place_id').val(place_id);
        activateSuggestions = true;
    }
</script>
<script type="text/javascript">
(function ($, root, undefined) {


   $(document).ready(function(){


	   $(window).bind('scroll', function() {
	   var navHeight = $( window ).height() - 600;
			 if ($(window).scrollTop() > navHeight) {
				 $('.pageWrapper.cartPageWrapper > .content_width_1000 .shoppingInfoCol.col').addClass('fixed');
			 }
			 else {
				 $('.pageWrapper.cartPageWrapper > .content_width_1000 .shoppingInfoCol.col').removeClass('fixed');
			 }
		});
	});

   })(jQuery, this);
</script>

<style>

</style>
<!--<link href="--><?//=FRONT_CSS.'jquery.datetimepicker.css'?><!--" rel="stylesheet" type="text/css"/>-->
<!--<script src="<//?=FRONT_JS.'jquery.sticky.js'?>"></script>-->
<!--<script src="<//?=FRONT_JS.'cc_type.js'?>"></script>-->
<script type="text/javascript">
	$(document).ready(function(){
        $('input[name=cc_number]').on('input',function(){
			$('#cc_type').val(creditCardType($(this).val())[0].type);
		});
		window.addEventListener('cardChange',function(c){
		    card = c.detail;
            if(card != ''){
                $('#creditCardIcons > div.media').empty().append($('<img src="<?=FRONT_ASSETS?>img/'+card+'_icon.png" />'));
            } else {
                $('#creditCardIcons > div.media').empty();
            }
        });
		function updateACart() {
			var cc_number = $('input[name=cc_number]').val();
			var cc_month = $('input[name=cc_expiration_month]').val();
			var cc_year = $('input[name=cc_expiration_year]').val();
			var cc_ccv = $('input[name=cc_ccv]').val();
			var fn = $('input[name=bill_first_name]').val();
			var ln = $('input[name=bill_last_name]').val();
			var phone = $('input[name=bill_phone]').val();
			var address1 = $('input[name=bill_address]').val();
			var address2 = $('input[name=bill_address2]').val();
			var city = $('input[name=bill_city]').val();
			var state = $('input[name=bill_state]').val();
			var zip = $('input[name=bill_zip]').val();
			var data = {
				cc_number: cc_number,
				cc_expiration_month: cc_month,
				cc_expiration_year: cc_year,
				cc_ccv: cc_ccv,
				bill_first_name: fn,
				bill_last_name: ln,
				phone: phone,
				bill_address: address1,
				bill_address2: address2,
				bill_city: city,
				bill_state: state,
				bill_zip: zip
			};
			$.post('/checkout/logCheckout', data, function () {
				console.log('123');
			});
		}


		$('[name=wholesale_term]').on('change',function(){
			var payment_method = $('[name=payment_method');
			switch(parseFloat($(this).val())){
				default:
				case 0: /** CC */
					payment_method.val(1);
					ccSection(true);
					break;
				case 1: /** Check (deposit) */
					payment_method.val(4);
					ccSection(false);
					break;
				case 2: /** Bank Wire */
					payment_method.val(6);
					ccSection(false);
					break;
				case 3: /** Cash */
					payment_method.val(3);
					ccSection(false);
					break;
			}
		}).trigger('change');

		function ccSection(show){
			var cc_section = $('.creditCardPaymentFormSection');
			if(show){
				cc_section.show(); cc_section.find('input,select').prop('disabled',false);
			} else {
				cc_section.hide(); cc_section.find('input,select').prop('disabled',true);
			}
		}

		$('#creditCardPayBtn').on('click',function(e){
			$('#bill').removeAttr('target');
            var submit = true;
			$.each(['#cardUserName','#ccnumber','#secCode'],function(k,v){
			   if($(v).val() == ''){
			       submit == false;
			       $(v).attr('style', 'border: 1px solid red !important').removeClass('pay_field_valid').addClass('pay_field_invalid');
               } else {
                   $(v).attr('style', 'border: 1px solid #d6d6d6 !important').removeClass('pay_field_invalid').addClass('pay_field_valid');
               }
            });
			var data = {
				bill_first_name:$('[name=bill_first_name]').val(),
				bill_last_name:$('[name=bill_last_name]').val(),
				bill_address:$('[name=bill_address]').val(),
				bill_city:$('[name=bill_city]').val(),
				bill_state:$('[name=bill_state]:enabled').val(),
				bill_zip:$('[name=bill_zip]').val(),
				bill_country:$('[name=bill_country]').val(),
				phone:$('[name=phone]').val(),
				ship_first_name:$('[name=ship_first_name]').val(),
				ship_last_name:$('[name=ship_last_name]').val(),
				ship_address:$('[name=ship_address]').val(),
				ship_city:$('[name=ship_city]').val(),
				ship_state:$('[name=ship_state]:enabled').val(),
				ship_zip:$('[name=ship_zip]').val(),
				ship_country:$('[name=ship_country]').val()
			};
			$.each(data,function(key,value){
			    if(!value){
                    submit = false;
					$('[name=' + key +']:enabled').attr('style', 'border: 1px solid red !important');
					$('[name=' + key +']:enabled').removeClass('pay_field_valid');
					$('[name=' + key +']:enabled').addClass('pay_field_invalid');
                } else {
                    $('[name=' + key +']:enabled').attr('style', 'border: 1px solid #d6d6d6 !important');
                    $('[name=' + key +']:enabled').removeClass('pay_field_invalid');
                    $('[name=' + key +']:enabled').addClass('pay_field_valid');
                }
            });
            if(submit && $('.pay_field_invalid').length == 0){
				$('#delivery_form :input').not(':submit').clone().hide().appendTo('#bill');
				$('#bill [name=ship_state]').val($('[name=bill_state]').val());
			} else {
                $('html, body').animate({
                    scrollTop: $('.pay_field_invalid').siblings('label').offset().top
                }, 1000);
				e.preventDefault();
            }
		});

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$('#cardUserName, #ccnumber, #secCode, #fname,#lname,#checkoutPhone,#checkoutAddress1, #checkoutAddress2, #checkoutCity, #checkoutRegionUS, #checkoutZip,#checkoutCountry').on('blur autocompletechange', function () {
				updateACart()
			});
		} else {
			$(window).on('beforeunload', function () {
				updateACart()
			});
		}
        $('#shipzip').on('keyup',function () {
            var country = $('select[name=ship_country]').find(':selected').val();
            var zip = $(this).val();
            if(country === 'United States'){
                zip = zip.replace(/[^0-9-]/g,'');
                $(this).val(zip);
                $(this).change();
            } else {
                zip = zip.replace(/[^A-z0-9-]/g,'');
                $(this).val(zip);
                $(this).change();
            }
        });
		$('#ccnumber').on('keyup',function(){
		    var ccnumber = $(this).val();
		    ccnumber = ccnumber.replace(/[^0-9-]/g,'');
		    $(this).val(ccnumber);
        });

		var opt = $('#isSameAddress');
		opt.on('click',function(){
			if(!opt.find('input').is(':checked')) {
				$('.billingAddressInfo').addClass('hiddenShippingInfo');
				$('input[name=bill_first_name').val($('input[name=ship_first_name').val());
				$('input[name=bill_last_name').val($('input[name=ship_last_name').val());
				$('input[name=bill_phone').val($('input[name=ship_phone').val());
				$('input[name=bill_address').val($('input[name=ship_address').val());
				$('input[name=bill_address2').val($('input[name=ship_address2').val());
				$('input[name=bill_city').val($('input[name=ship_city').val());
				$('input[name=bill_state').val($('input[name=ship_state').val());
				$('input[name=bill_zip').val($('input[name=ship_zip').val());
			} else {
				$('.billingAddressInfo').removeClass('hiddenShippingInfo');
				$('input[name=bill_first_name').val('');
				$('input[name=bill_last_name').val('');
				$('input[name=bill_phone').val('');
				$('input[name=bill_address').val('');
				$('input[name=bill_address2').val('');
				$('input[name=bill_city').val('');
				$('input[name=bill_state').val('');
				$('input[name=bill_zip').val('');
			}
		});
//		$("#do_coupon").on('click',function(){
//			var email = $('#emailAddr').val();
//			if(email.length>1 && email.indexOf('@')>0){
//				$.post('/checkout/addCoupon',{email:email},function(data){
//					if(data.status == 'success'){
//						window.location.replace('<?//=$this->emagid->uri?>//');
//					} else {
//						alert(data.message);
//					}
//				})
//			} else {
//				alert('Please input a coupon code');
//			}
//		});
		$('#redeem-gift-card').on('click',function(){
			var code = $('[name=gift-card-code]');
			if(code.val() == null || code.val() == ''){
				alert('Please input your gift card code');
			} else {
				$.post('/checkout/addGiftCard',{'gift-card-code':code.val()},function(data){
					var json = $.parseJSON(data);
					if(json.status == 'success'){
						window.location.replace(json.redirect);
					} else {
						alert(json.message);
					}
				})
			}
		});
		$('.apply-cpn').on('click',function(){
			var code = $(this).siblings('input.cpn-code');
			if(code.val() == null || code.val() == ''){
				alert('Please input a coupon code');
			} else {
				$.post('/coupon/add',{'code':code.val()},function(data){
					var json = $.parseJSON(data);
					if(json.status == 'success'){
						window.location.replace('/checkout/payment');
					} else {
						alert(json.message);
					}
				})
			}
		});
		$('#remove-gift-card').on('click',function(){
			$.post('/checkout/removeGiftCard',{},function(data){
				var json = $.parseJSON(data);
				if(json.status == 'success'){
					window.location.replace('/checkout/payment');
				}
			})
		});
		$('#remove-coupon').on('click',function(){
			$.post('/coupon/remove',{},function(data){
				var json = $.parseJSON(data);
				if(json.status == 'success'){
					window.location.replace('/checkout/payment');
				}
			})
		});

		//If gift card completes the purchase, disable CC and PayPal
		<?if($model->giftCardFullCharge){?>
			$('#paypalPayment').hide();
			$('.title').html('Paid with Gift Card');
			$('.creditCardPaymentFormSection').hide();
			$('[name=card_name]').prop('disabled',true);
			$('[name=cc_number]').prop('disabled',true);
			$('[name=cc_expiration_month]').prop('disabled',true);
			$('[name=cc_expiration_year]').prop('disabled',true);
			$('[name=cc_ccv]').prop('disabled',true);
			$('[name=paypal]').prop('disabled',true);
		<?}?>
		$('select[name=ship_country]').on('change',function(){
			var us = $('#us_states');
			var foreign = $('#foreign_states');
			if($(this).find(':selected').val() === 'United States'){
				us.show();
				us.find('select').prop('disabled',false);

				foreign.hide();
				foreign.find('input').prop('disabled',true);
			} else {
				us.hide();
				us.find('select').prop('disabled',true);

				foreign.val('');
				foreign.show();
				foreign.find('input').prop('disabled',false);
			}
		});
		$('select[name=bill_country]').on('change',function(){
			var us = $('#bill #us_states');
			var foreign = $('#bill #foreign_states');
			if($(this).find(':selected').val() === 'United States'){
				us.show();
				us.find('select').prop('disabled',false);

				foreign.hide();
				foreign.find('input').prop('disabled',true);
			} else {
				us.hide();
				us.find('select').prop('disabled',true);

				foreign.show();
				foreign.find('input').prop('disabled',false);
			}
		});
		$('#checkoutAddress1,' +
          '#checkoutAddress2,' +
          '#checkoutCity,' +
          '#checkoutCountry,' +
          '#shipzip,' +
          '#checkoutRegion,' +
          '#checkoutRegionUS').keyup(function(){
            $('#place_id').val('');
        });
	})
</script>
<script type="text/javascript">
   //Separate Forms
   $(document).ready(function(){
      $('#agreeTerms').change(function(){
          if($(this).is(':checked')){
              $('#creditCardPayBtn').attr('disabled',false);
          } else {
              $('#creditCardPayBtn').attr('disabled',true);
          }
      });
      $(':input:not(:visible)').attr('tabindex',-1);
      $('#email_form').find('input,select').change(function(e){
          var form = $('#email_form');
          var field = $(this);
          $('input[name=email]').val(field.val());
          $.post('/checkout/email',form.serialize(),function (data){
              if(data.status){
                  field.addClass("pay_field_valid");
                  field.removeClass("pay_field_invalid");
              } else {
                  field.addClass("pay_field_invalid");
                  field.removeClass("pay_field_valid");
              }
          })
      });
      $('#delivery_form').find('input,select').change(function(e){
          var name = $(this).attr('name');
          if($('#isSameAddress :checked').length > 0){

              $('#bill').find('input,select').filter('[name='+name.replace('ship','bill')+']').val($(this).val());
              var form = $('#delivery_form');
              $.post('/checkout/delivery',form.serialize(),function (data){
                  console.log(data);
              })
              if($(this).attr('name') == 'ship_country' || $(this).attr('name') == 'ship_state'){
                  var code = $('#delivery_form [name=ship_country]').find(':selected').data('code');
                  $.post('/checkout/shippingPriceUpdate',{country:this.value,countryCode:code,cartTotal:<?=$model->cart->total?>,state:$('#checkoutRegionUS').val()},function(data){
                    $('span.upd_shipping_cost').text('$'+data.shipping);
                    $('span.upd_duty_cost').text('$'+data.duty);
                    $('b.upd_duty_name').text(data.taxName);
                    $('[name=shipping]').val(data.shipping);
                    $('[name=shipping_method]').val(data.ship_meth);
                    if(data.taxName == 'DUTY'){
                        $('[name=duty]').val(data.duty);
                    } else {
                        $('[name=duty]').val(0);
                    }
                    $('.orderSummaryTotal p').html('<b>Total</b>$'+data.cartTotal);
                    if(code == 'US'){
                        $('.duty_name').text('Shipping');
                    } else {
                        $('.duty_name').text('Duty & Import Fees');
                    }
                    if(data.discount[0] == null && <?=isset($_SESSION['coupon'])?'true':'false'?>){
                        alert('Your Shipping coupon does not apply to this location and has been removed');
                        window.location.reload();
                    } else {
                        $('.discount-amt').text(data.discount[1]);
                    }
                    console.log(data);
                  });
              }
          }
      });
       $(document).on('click','.location_choices',function(e){
           var placeId = $(this).data('place_id');
           lsService.geocode({'placeId': placeId}, function(results, status) {
                activate(results[0]);
           });
       });
      $('select[name=ship_country]').change();
      $('select[name=bill_country]').change();
   });
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBTc_xONeYp-mEbhLrISuX28EBeZdIJwyI&libraries=places&callback=initMap"></script>
</script>
