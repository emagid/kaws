<style>
	.row:before{
		position: relative;
	}
</style>
<div class="pageWrapper checkoutPageWrapper cartPageWrapper shippingCheckoutPageWrapper">
	<div class="row content_width_1000">
		<div class="emailFormWrapperCol shippingFormWrapperCol col">
			<div class="header">
				<h1 class="dji-text">Billing/Shipping</h1>
			</div>
<!--
                        <div class="billShipNotice">
                <p><strong>*NOTE*</strong> For security reasons, your billing and shipping addresses are required to match.</p>
            </div>
-->
			<div class="sub_header">
				<h4 class="gothamLight">Billing/Shipping Address</h4>
			</div>
			<? if(isset($model->addresses)) {?>
				<select name="stored_addr">
					<option>Select stored address...</option>
					<? foreach($model->addresses as $address) {
						echo "<option value='$address->id' data-f_name='$address->first_name' data-l_name='$address->last_name' data-phone='$address->phone' data-address='$address->address' data-address2='$address->address2' data-city='$address->city' data-state='$address->state' data-zip='$address->zip' data-country='$address->country' >$address->label</option>";
					}?>
				</select>
			<?}?>
			<form method="post" action="<?=$this->emagid->uri?>" id="email_form">
				<?$session = json_decode($_SESSION['customerDetails'],true);
				//Real bad
					$ship_first_name = isset($_SESSION['customerDetails']) && array_key_exists('ship_first_name',$session) ? $session['ship_first_name']: '';
					$ship_last_name = isset($_SESSION['customerDetails']) && array_key_exists('ship_last_name',$session) ? $session['ship_last_name']: '';
					$phone = isset($_SESSION['customerDetails']) && array_key_exists('phone',$session) ? $session['phone']: '';
					$ship_address = isset($_SESSION['customerDetails']) && array_key_exists('ship_address',$session) ? $session['ship_address']: '';
					$ship_address2 = isset($_SESSION['customerDetails']) && array_key_exists('ship_address2',$session) ? $session['ship_address2']: '';
					$ship_city = isset($_SESSION['customerDetails']) && array_key_exists('ship_city',$session) ? $session['ship_city']: '';
					$ship_country = isset($_SESSION['customerDetails']) && array_key_exists('ship_country',$session) ? $session['ship_country']: '';
					$ship_state = isset($_SESSION['customerDetails']) && array_key_exists('ship_state',$session) ? $session['ship_state']: '';
					$ship_zip = isset($_SESSION['customerDetails']) && array_key_exists('ship_zip',$session) ? $session['ship_zip']: '';

					$user = $model->user ? : null;
					if($user){
						$ship_first_name = isset($_SESSION['customerDetails']) && array_key_exists('ship_first_name',$session) ? $session['ship_first_name']: $user->first_name;
						$ship_last_name = isset($_SESSION['customerDetails']) && array_key_exists('ship_last_name',$session) ? $session['ship_last_name']: $user->last_name;
						$phone = isset($_SESSION['customerDetails']) && array_key_exists('phone',$session) ? $session['phone']: $user->phone;
					}
					$address = $model->user && $model->user->getAddress() ? $model->user->getAddress(): null;
					if($address){
						$ship_address = isset($_SESSION['customerDetails']) && array_key_exists('ship_address',$session) ? $session['ship_address']: $user->address;
						$ship_address2 = isset($_SESSION['customerDetails']) && array_key_exists('ship_address2',$session) ? $session['ship_address2']: $user->address2;
						$ship_city = isset($_SESSION['customerDetails']) && array_key_exists('ship_city',$session) ? $session['ship_city']: $user->city;
						$ship_country = isset($_SESSION['customerDetails']) && array_key_exists('ship_country',$session) ? $session['ship_country']: $user->country;
						$ship_state = isset($_SESSION['customerDetails']) && array_key_exists('ship_state',$session) ? $session['ship_state']: $user->state;
						$ship_zip = isset($_SESSION['customerDetails']) && array_key_exists('ship_zip',$session) ? $session['ship_zip']: $user->zip;
					}
				?>
				<label for="checkoutFName">First Name</label>
				<input type="text" name="ship_first_name" maxlength="60" tabindex="1" class="checkOutInput" id="checkoutFName" value="<?=$ship_first_name?>" required>
				<label for="checkoutLName">Last Name</label>
				<input type="text" name="ship_last_name" maxlength="60" tabindex="2" class="checkOutInput" id="checkoutLName" value="<?=$ship_last_name?>" required>
				<label for="checkoutPhone">Phone <span class="optional">(optional)</span></label>
				<input type="text" name="phone" maxlength="60" tabindex="3" class="checkOutInput" id="checkoutPhone" value="<?=$phone?>">
				<label for="checkoutAddress1">Address Line 1</label>
				<input type="text" name="ship_address" maxlength="60" tabindex="4" class="checkOutInput" id="checkoutAddress1" value="<?=$ship_address?>" required>
				<label for="checkoutAddress2">Address Line 2 <span class="optional">(optional)</span></label>
				<input type="text" name="ship_address2" maxlength="60" tabindex="5" class="checkOutInput" id="checkoutAddress2" value="<?=$ship_address2?>">
				<label for="checkoutCity">City</label>
				<input type="text" name="ship_city" maxlength="60" tabindex="6" class="checkOutInput" id="checkoutCity" value="<?=$ship_city?>" required>
				<label for="checkoutZip">Country</label>
				<fieldset class="selectUIWrapper countrySelectWrapper">
					<select name="ship_country">
						<?foreach(get_countries() as $country){?>
							<option value="<?=$country?>" <?=$ship_country == $country ? 'selected': ''?>><?=$country?></option>
						<?}?>
					</select>
					<span class="downArrowICon">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
									<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
									L-321,348.1z"></path>
								</svg>
					</span>
				</fieldset>
				<div id="us_states">
					<label for="checkoutRegion">County / State / Region *</label>
					<fieldset class="selectUIWrapper countrySelectWrapper">
						<select name="ship_state" id="checkoutRegionUS" required>
							<?foreach(get_states() as $short=>$long){?>
								<option value="<?=$short?>" <?=$ship_state == $short ? 'selected': ''?>><?=$long?></option>
							<?}?>
						</select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"></path>
							</svg>
						</span>
					</fieldset>
				</div>
				<div id="foreign_states" style="display: none;">
					<label for="checkoutRegion">County / State / Region *</label>
					<input disabled type="text" name="ship_state" maxlength="60" tabindex="7" class="checkOutInput" id="checkoutRegion" value="<?=$ship_state?>" required>
				</div>
				<label for="checkoutZip">Postcode / Zip Code *</label>
				<input type="text" id="shipzip" name="ship_zip" maxlength="60" tabindex="8" class="checkOutInput" id="checkoutZip" value="<?=$ship_zip?>" required>
				<input name="email" style="display: none" value="<?=$session['email']?>"/>
				<?if($user){?>
					<input type="hidden" name="email"  value="<?=$user ? $user->email: ''?>" >
				<?}?>

			<div style="display: none" class="checkboxUX optionBoxCheckboxUIWrapper optionBox selected highlightedCheckoutOptionBox">
				<div class="checkboxUI">
					<icon class="check"></icon>
				</div>
				<p>This is also my billing address</p>
				<input type="checkbox" name="shipping_is_billing" checked="checked">
			</div>
			<div class="right_float checkoutNextStepFloat">
				<input type="submit" tabindex="9" value="Next Step" class="solidBlueBtn" id="delivery-submit">
				<icon>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 2464 184 282" enable-background="new 0 2464 184 282" xml:space="preserve">
						<g>
							<path fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" d="M17.8,2605.1"/>
							<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2484.5"/>
							<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2725.8"/>
						</g>
					</svg>
				</icon>
			</div>	
			<div class="row secondaryFocus">
		
				<a class="as_r goBackBtn" href="/checkout/email">
					<icon>
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 2464 184 282" enable-background="new 0 2464 184 282" xml:space="preserve">
							<g>
								<path fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" d="M17.8,2605.1"/>
								<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2484.5"/>
								<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2725.8"/>
							</g>
						</svg>
					</icon>
					<p>Previous Step</p>
				</a>
			</div>
			</form>
		</div>
		<div class="shoppingInfoCol col">
			<div class="labelDataBox orderSummaryDataBox">
				<label>Order Summary</label>
				<ul class="paymentScreenBagItems">
					<? foreach($model->cart->cart as $cart){
						if($cart->product_id) {
							$product = \Model\Product::getItem($cart->product_id);
//							$color = $cart->getColor();
//							$colorObj = \Model\Color::getItem($color);
//							$size = $cart->getSize();
//							$sizeObj = \Model\Size::getItem($size);
							$url = '/products/' . $product->slug;
							$img = $product->featuredImage();
							$name = $product->name;
//							$colorText = strlen($colorObj->name) < 20 ? $colorObj->name: substr($colorObj->name,0,20).'...';
//							$sObjUs = $sizeObj->us_size;
//							$sObjEu = $sizeObj->eur_size;
							$price = $product->getPrice(null,null,false,$model->user);
//							if($cart->clearance && ($c = \Model\Clearance::checkClearance($cart->product_id,$color,$size))){
//								$name .= '(Clearance)';
//								$price = $product->getPrice($color,$size,true);
//							}
						} else {
//							$detail = json_decode($cart->details,true);
//							$gc = \Model\Gift_Card::getItem($detail['gift_card_id']);
//
//							$url = '/gift_cards/product/'.$detail['gift_card_id'];
//							$img = UPLOAD_URL.'gift_cards/'.$gc->image;
//							$name = $gc->name;
//							$colorText = '';
//							$sObjUs = '';
//							$sObjEu = '';
//							$price = number_format($detail['amount'],2);
						}?>
						<li>
							<div class="col bagItemImg">
								<a href="<?=$url?>" class="row">
									<div class="col">
										<p><?=$name?></p>
										<!--										<p>--><?//=$colorText?><!--</p>-->
										<!--										<p>US: --><?//=$sObjUs?><!-- EU: --><?//=$sObjEu?><!--</p>-->
										<p>$<?=number_format($price,2)?></p>
									</div>
									<div class="col">
										<img src="<?=$img?>">
									</div>
								</a>
							</div>
						</li>
					<?}?>
				</ul>
				<p><b>Subtotal</b>$<?=$model->cart? number_format($model->cart->total,2): 0.00?></p>
				<p><b>Shipping</b>$<?=number_format($model->cart->shipping,2)?></p>
				<?if($model->cart->discount){?>
					<p><b>Discount</b>-$<?=number_format($model->cart->discount,2)?></p>
				<?}?>
				<span class="line_sep"></span>
				<div class="orderSummaryTotal"><span class="triangleBorder"></span><p><b>Estimated Total</b>$<?=$model->cart ? number_format($model->cart->total - $model->cart->discount + $model->cart->shipping,2): 0.00?></p></div>
			</div>
			<div class="labelDataBox">
				<label>Help with your Order</label>
				<p>You can reach us by:</p>
				<p><b>Phone</b><a href="tel:2123333990">212 333 3990</a></p>
			</div>
			<div class="labelDataBox">
				<label>Returns Policy</label>
				<p><b>Easy Returns</b>Returns service: All products may be exchanged or returned within 14 days of the invoice date. Thereafter, all sales are final. </p>
			</div>
			<div class="labelDataBox">
				<label>Secure Shopping</label>
				<p>To ensure the safety of your credit card data at all times, we use Secure Socket Layer (SSL) technology and the highest security standards, certified by Verisign™ and Trustwave.</p>
				<img src="<?=FRONT_IMG?>creditCardOptions.png">
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		function updateACart() {
			var fn = $('input[name=ship_first_name]').val();
			var ln = $('input[name=ship_last_name]').val();
			var phone = $('input[name=phone]').val();
			var address1 = $('input[name=ship_address]').val();
			var address2 = $('input[name=ship_address2]').val();
			var city = $('input[name=ship_city]').val();
			var state = $('[name=ship_state]:enabled').val();
			var zip = $('input[name=ship_zip]').val();
			var country = $('select[name=ship_country]').val();
			var data = {
				ship_first_name: fn,
				ship_last_name: ln,
				phone: phone,
				ship_address: address1,
				ship_address2: address2,
				ship_city: city,
				ship_state: state,
				ship_zip: zip,
				ship_country: country
			};
			$.post('/checkout/logCheckout', data, function () {
				console.log('123');
			});
		}

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$('#checkoutFName, #checkoutLName, #checkoutPhone, #checkoutAddress1, #checkoutAddress2, #checkoutCity, #checkoutRegionUS, #checkoutZip').on('blur autocompletechange',function(){
				updateACart();
			});
			$('#email_form').on('submit',function(){
				updateACart();
			});
		} else {
			$(window).on('beforeunload',function(){
				updateACart();
			});
		}
        $('#shipzip').on('keyup',function () {
            var country = $('select[name=ship_country]').find(':selected').val();
            var zip = $(this).val();
            if(country === 'United States'){
                zip = zip.replace(/[^0-9-]/g,'');
                $(this).val(zip);
            } else {
                zip = zip.replace(/[^A-z0-9-]/g,''); 
                $(this).val(zip);
            }
        });
		$('select[name=ship_country]').on('change',function(){
			var us = $('#us_states');
			var foreign = $('#foreign_states');
			var country = $(this).find(':selected').val();
			var shipping_cost = $('#shipping_cost');
			var cart_total = $('#cart_total');
			if(country === 'United States'){
				us.show();
				us.find('select').prop('disabled',false);

				foreign.hide();
				foreign.find('input').prop('disabled',true);

				shipping_cost.html('<b>Shipping</b>Free');
				cart_total.html('<b>Estimated Total</b>$'+'<?=number_format($model->cart->total - $model->cart->discount,2)?>');
			} else {
				$.post('/checkout/shippingPriceUpdate', {country:country,cartTotal:<?=$model->cart->total?>},function(data){
					if(data.status == 'success'){
						shipping_cost.html('<b>Shipping</b>'+data.shipping);
						cart_total.html('<b>Estimated Total</b>$'+data.cartTotal);
					}
				});
				us.hide();
				us.find('select').prop('disabled',true);

				foreign.show();
				foreign.find('input').prop('disabled',false);
			}
		});
		$('#delivery-submit').on('click',function(e){
			e.preventDefault();
			var data = {
				ship_first_name:$('[name=ship_first_name]').val(),
				ship_last_name:$('[name=ship_last_name]').val(),
				ship_address:$('[name=ship_address]').val(),
				ship_city:$('[name=ship_city]').val(),
				ship_state:$('[name=ship_state]:enabled').val(),
				ship_zip:$('[name=ship_zip]').val(),
				ship_country:$('[name=ship_country]').val()
			};
			var submit = true;
			$.each(data,function(key,value){
				if(!value){
					submit = false;
					$('[name=' + key +']:enabled').css('border', '1px solid red');
				} else {
					$('[name=' + key +']:enabled').css('border', '1px solid #d6d6d6');
				}
			});
			if(submit){
				$('#email_form').submit();
			}
		});
		$("select[name='stored_addr']").on('change',function() {
			var addr_data = $(this.options[this.selectedIndex]).data();
			if ($.isEmptyObject(addr_data)){

			} else {
				$('#checkoutFName').val(addr_data.f_name);
				$('#checkoutLName').val(addr_data.l_name);
				$('#checkoutPhone').val(addr_data.phone);
				$('#checkoutZip').val(addr_data.zip);
				$("[name='ship_country']").val(addr_data.country);
				$('#checkoutRegionUS').val(addr_data.state);
				$('#checkoutAddress1').val(addr_data.address);
				$('#checkoutAddress2').val(addr_data.address2);
				$('#checkoutCity').val(addr_data.city);
			}
		});
	})
</script>