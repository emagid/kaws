<style>
	.row:before{
		position: relative;
	}
</style>
<div class="pageWrapper checkoutPageWrapper cartPageWrapper emailCheckoutPageWrapper">
	<div class="row content_width_1000">
		<div class="emailFormWrapperCol col">
			<div class="header">
				<h1 class="dji-text">Your Email</h1>
			</div>
			<!--Why?-->
			<h8 class="as_l">Please submit a valid email address so that we can keep you informed on the status of your order.</h8>
			<form method="post" action="<?=$this->emagid->uri?>" id="email_form">
				<label for="checkoutEmail">Email Address</label>
				<? $email = '';
				if (isset($_SESSION['customerDetails']) && array_key_exists('email', json_decode($_SESSION['customerDetails'], true))) {
					$email = json_decode($_SESSION['customerDetails'], true)['email'];
				} else if ($model->user) {
					$email = $model->user->email;
				} ?>
				<input type="email" name="email" maxlength="60" tabindex="1" class="checkOutInput" id="checkoutEmail" value="<?=$email?>"  required>
				<input type="password" name="password" maxlength="60" tabindex="1" class="checkOutInput" style="display:none" disabled>
				<input type="submit" tabindex="2" value="Proceed to Shipping" class="btn btn_black btn_blue btn_full_width">
			</form>
			<div class="row secondaryFocus">
				<a class="as_r goBackBtn" href="/cart">
					<icon>
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 2464 184 282" enable-background="new 0 2464 184 282" xml:space="preserve">
							<g>
								<path fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" d="M17.8,2605.1"/>
								<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2484.5"/>
								<line fill="none" stroke="#000000" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2725.8"/>
							</g>
						</svg>
					</icon>
					<p>Previous Step</p>
				</a>
			</div>
		</div>
		<div class="shoppingInfoCol col">
			<div class="labelDataBox orderSummaryDataBox">
				<label>Order Summary</label>
				<ul class="paymentScreenBagItems">
					<? foreach($model->cart->cart as $cart){
						if($cart->product_id) {
							$product = \Model\Product::getItem($cart->product_id);
//							$color = $cart->getColor();
//							$colorObj = \Model\Color::getItem($color);
//							$size = $cart->getSize();
//							$sizeObj = \Model\Size::getItem($size);
							$url = '/products/' . $product->slug;
							$img = $product->featuredImage();
							$name = $product->name;
//							$colorText = strlen($colorObj->name) < 20 ? $colorObj->name: substr($colorObj->name,0,20).'...';
//							$sObjUs = $sizeObj->us_size;
//							$sObjEu = $sizeObj->eur_size;
							$price = $product->getPrice(null,null,false,$model->user);
//							if($cart->clearance && ($c = \Model\Clearance::checkClearance($cart->product_id,$color,$size))){
//								$name .= '(Clearance)';
//								$price = $product->getPrice($color,$size,true);
//							}
						} else {
//							$detail = json_decode($cart->details,true);
//							$gc = \Model\Gift_Card::getItem($detail['gift_card_id']);
//
//							$url = '/gift_cards/product/'.$detail['gift_card_id'];
//							$img = UPLOAD_URL.'gift_cards/'.$gc->image;
//							$name = $gc->name;
//							$colorText = '';
//							$sObjUs = '';
//							$sObjEu = '';
//							$price = number_format($detail['amount'],2);
						}?>
						<li>
							<div class="col bagItemImg">
								<a href="<?=$url?>" class="row">
									<div class="col">
										<p><?=$name?></p>
<!--										<p>--><?//=$colorText?><!--</p>-->
<!--										<p>US: --><?//=$sObjUs?><!-- EU: --><?//=$sObjEu?><!--</p>-->
										<p>$<?=number_format($price,2)?></p>
									</div>
									<div class="col">
										<img src="<?=$img?>">
									</div>
								</a>
							</div>
						</li>
					<?}?>
				</ul>				
				<p><b>Subtotal</b>$<?=$model->cart? number_format($model->cart->total,2): 0.00?></p>
				<p><b>Shipping</b>$<?=number_format($model->cart->shipping,2)?></p>
				<?if($model->cart->discount){?>
					<p><b>Discount</b>-$<?=number_format($model->cart->discount,2)?></p>
				<?}?>
				<span class="line_sep"></span>
				<div class="orderSummaryTotal"><span class="triangleBorder"></span><p><b>Estimated Total</b>$<?=$model->cart ? number_format($model->cart->total - $model->cart->discount + $model->cart->shipping,2): 0.00?></p></div>
			</div>
			<div class="labelDataBox">
				<label>Help with your Order</label>
				<p><b>Phone</b>212 777 1851</p>
				<p><b>Email and Chat</b><a>Send a Message</a></p>
			</div>
			<div class="labelDataBox">
				<label>Secure Shopping</label>
				<p>To ensure the safety of your credit card data at all times, we use Secure Socket Layer (SSL) technology and the highest security standards, certified by Verisign™ and Trustwave.</p>
				<img src="<?=FRONT_IMG?>creditCardOptions.png">
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		function updateACart(){
			var email = $('input[name=email]').val();
			var data = {email:email};
			$.post('/checkout/logCheckout',data,function(){
				console.log('123');
			});
		}

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$('input[name=email]').on('blur autocompletechange',function(){
				updateACart();
			});
			$('#email_form').on('submit',function(){
				updateACart();
			});
		} else {
			$(window).on('beforeunload', function(){
				updateACart();
			});
		}
	})
</script>