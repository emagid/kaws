<section class='product expand'>
    
    <section class='content_holder doll'>
        <div class='figures' style="background-image: url('<?=$model->product->featuredImage()?>');">

        </div>
        <div class='figure_info'>
            <h1><?=$model->product->name?></h1>
            <p class='model'><?=$model->product->color?></p>
            <p class='text'><?=$model->product->date?></p>
            <p class='text'><?=$model->product->details?></p>
            <p class='text'><?=implode(' x ',[$model->product->height,$model->product->width,$model->product->length])?> inches</p>
            <div class='thumbnails'>
                <?php foreach(\Model\Product_Image::getList(['where'=>"product_id = {$model->product->id}",'orderBy'=>'display_order ASC']) AS $i => $pi){ ?>
                    <a href=""><img class='<?= !$i?'selected':''?>' src="<?= $pi->get_image_url(); ?>"></a>
                <?php } ?>
            </div>
            <p class='price'>$<?=number_format($model->price,2)?></p>
            <? switch($model->campaign->type) {
                case 'Lottery': ?>
                    <? if($_SESSION['lottery'] == $model->campaign->id) { ?>
                    <p class='lotto_confirm'>You have already signed up for this lottery! You will be notified if you're selected as a winner.</p>
                        
                    <? } else { ?>
                    <button id='lotto_button' class='button'>SIGN UP FOR LOTTERY</button>
                    <section class='lotto popup'>
                        <div class='overlay'>
                            <div class='off_overlay'></div>
                            <div class='overlay_content'>
                                <div class='overlay_text'>
                                    
                                    <h1 class='overlay_head1'>Lottery Signup</h1>
                                    <p class='overlay_head'>Sign up here for entry to the lottery for this product!</p>
                                    <!--<form action="/lottery/signup" method="POST" id='lotto_form'>-->
                                    <form id='lotto_form'>
                                        <input type="hidden" name="campaign_id" value="<?=$model->campaign->id?>" />
                                        <input type="hidden" name="product_id" value="<?=$model->product->id?>" />
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="email" placeholder='email' required/>
                                        </div>
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" name="first_name" placeholder='first name' required/>
                                        </div>
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" name="last_name" placeholder='last name' required/>
                                        </div>
                                        <input type="submit" value="Enter" class="button" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                    <? } ?>
                    <? break; ?>
                <? case 'Standard': ?>
                    <? if($model->product->availability == 1 || $model->campaign->status == 'Sold Out') { ?>
                    <p class='status'>SOLD OUT</p>
                    <?} else if($model->cProduct->max_per_user <= $model->cProduct->current) {?>
                    <p class='status'>You can't purhase more than <?=$model->cProduct->max_per_user?> at once</p>
                    <?} else if($model->product->availability == 2) {?>
                    <a href="/checkout/payment" class="addToCart mainProd" data-product_id="<?=$model->product->id?>">
                        <button class="button">Add to Cart</button>
                    </a>
                    <?}?>
                    <? break; ?>
            <? } ?>

            <a href="/" class='keep_shopping button'>KEEP SHOPPING</a>
        </div>
    </section>
</section>
<script>
    $(document).ready(function(){
        $($('.thumbnails a')[0]).click();
        $('#lotto_form').submit(function(e) {
            e.preventDefault();
            var entrant = {
                first_name:  $('#lotto_form [name=first_name]').val(),
                last_name:   $('#lotto_form [name=last_name]').val(),
                email:       $('#lotto_form [name=email]').val(),
                campaign_id: $('#lotto_form [name=campaign_id]').val(),
                product_id:  $('#lotto_form [name=product_id]').val()
            }
            $.ajax({
                url: '<?=KAWS_API_HOST?>api/lottery-entry',
                data: entrant,
                type:"POST",
                contentType: 'json',
                success: function (data) {
                    sessionStorage.lottery      = data.campaign_id;
                    sessionStorage.notification = "You have successfully entered the lottery and will be notified if you win";
                    buildNotification(1,sessionStorage.notification);
                    location.reload();
                    $.post('<?=SITE_URL?>lottery/signup',{campaign_id:datta.campaign_id},function(data){
                    },'json');
                },
                error: function (data) {
                    console.log(data);
                    sessionStorage.notification = data.responseJSON.errorMessage;
                    buildNotification(0,sessionStorage.notification);
                },
                dataType: 'json'});
        });
    });

    if ( $('#lotto_form').is(':visible') ) {
        $('.figure_info').css('top', '-50px');
    }
</script>
