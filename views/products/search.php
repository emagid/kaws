<!-- Banner -->
<div class="banner cms">
	<div class="container">
		<div class="inner">
			<h1>Advanced Search</h1>
		</div>
	</div>
</div>

<? require_once(ROOT_DIR.'templates/'.$this->template.'/filters.php'); ?>

<?php if (isset($model->products)) { ?>
<!-- Items -->
<div class="container">
	<div class="brand_items">
		<div class="category">
			<div class="row">
				<?php foreach($model->products as $product) { ?>
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
					<div class="product brand_product">
						<?php 
				            $img_path = ADMIN_IMG.'modernvice_shoe.png'; //TODO st-dev default product image
				            if(!is_null($product->featured_image) && $product->featured_image != "" && file_exists(UPLOAD_PATH.'products'.DS.$product->featured_image)){ 
				             	$img_path = UPLOAD_URL . 'products/' . $product->featured_image;
				            }
				        ?>
				        <div class="product-img-holder">
				        	<img src="<?php echo $img_path; ?>" alt="<?=$product->image_alt?>" />
				        </div>
						<div class="inner">
							<h2>
								<a href="<?=SITE_URL.'product/'.$product->slug?>"><?=$product->name?></a>
							</h2>	
							<span itemprop="model" class="model"><?=$product->mpn?></span>
							<p itemprop="offers" itemscope itemtype="http://schema.org/Offer">
							<span itemprop="price">$<?=number_format($product->price,2)?></span>
						</p>
						</div>
						<div class="hover">
						<a class="btn" href="<?=SITE_URL.'product/'.$product->slug?>">
							View Details
						</a>
						<h2>
							<a href="product.php">
								<!-- Corum Admiral's Cup Legend 38 -->
							</a>
						</h2>
						<!-- <span class="model">A082-01272</span> -->
						<p>
							<!-- $8,850.00 -->
						</p>
					</div>
					</div>
				</div>

				<?php } ?>
				<?php } ?>	

			</div>	
		</div>	
	</div>
	<nav class="text-right">
		<ul class="pagination">
		</ul>
	</nav>
</div>

<? footer(); ?>

<script>
var params = <?php echo (isset($model->params)) ? json_encode($model->params) : json_encode((object)array()); ?>;
if(params instanceof Array) {
	params = {};
}
var site_url = '<?= SITE_URL."products/search"?>';
$(function() {
	$('ul.pagination').pagination({
		pages: <?=$model->pagination->total_pages?>,
		currentPage: <?=$model->pagination->current_page_index + 1?>,
		cssStyle: 'light-theme',
		onPageClick: function(pageNumber,event) {
			var url_params = params || {};
			url_params.page = parseInt(pageNumber);
			var full_url = site_url;
			build_url(full_url,url_params,true);			
		}
	});
});
function build_url(url,params,redirect) {
	var params_arr = [];
	$.each(params,function(i,e) {
		params_arr.push(i+"="+e);
	});
	if(redirect) {
		window.location.href = url + "?"+params_arr.join("&");
		return false;
	} else {
		return url + "?"+params_arr.join("&");
	}
}
</script>

