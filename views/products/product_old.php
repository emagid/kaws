<div class="pageWrapper productPageWrapper">

    <section class="productDetailMainHero">
<?$img = FRONT_IMG .'productHero.jpg';
if($model->product->banner != ''){
    $img = $model->product->getBanner();
}
?>
        <div class="heroMedia" style="background-image:url(<?=$img?>)">
        </div>
        <div class="absTransCenter">
            <h1 class="dji-text"><?=$model->product->name?></h1>
            <h4 class="gothamLight"><?=$model->product->description?></h4>
            <div class="productHero-videos">
                <div class="swiperContainer-productHero-videos">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide video-slide">
                            <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                                <div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample1.jpg')"></div>
                                <icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
                            </a>
                        </div>
                        <div class="swiper-slide video-slide">
                            <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                                <div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample2.jpg')"></div>
                                <icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
                            </a>
                        </div>
                        <div class="swiper-slide video-slide">
                            <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                                <div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample3.jpg')"></div>
                                <icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-button-prev" style="background-image:url('<?=FRONT_IMG?>videoSliderArrow_left.png')"></div>
                    <div class="swiper-button-next" style="background-image:url('<?=FRONT_IMG?>videoSliderArrow_right.png')"></div>
                </div>
            </div>
        </div>
    </section>
    <div class="productDetails_contentWrapper">
        <div class="stickyElementsWrapper mainContentWidth">
            <div class="productDetailsSubHeader mainLinksListWrapper">
                <ul class="productDetailsSubHeader_Links mainContentWidth">
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="1" class="activePageSection">
                            <p>Overview</p>
                        </a>
                    </li>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="2">
                            <p>Accessories</p>
                        </a>
                    </li>
                    <?if($model->product_specs){?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="3">
                            <p>Specs</p>
                        </a>
                    </li>
                    <?}?>
                    <?if($model->product->getAllProductVideos()){?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="4">
                            <p>Videos</p>
                        </a>
                    </li>
                    <?}?>
                    <?if(isset($model->feature_1)&&$model->feature_1){?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="5">
                            <p><?=$model->feature_1_name?></p>
                        </a>
                    </li>
                    <?}?>
                    <?if(isset($model->feature_2)&&$model->feature_2){
                    ?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="6">
                            <p><?=$model->feature_2_name?></p>
                        </a>
                    </li>
                    <?}?>
                    <?if(isset($model->feature_3)&&$model->feature_3){
                    ?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="7">
                            <p><?=$model->feature_3_name?></p>
                        </a>
                    </li>
                    <?}?>
                    <?if(isset($model->feature_4)&&$model->feature_4){
                    ?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="8">
                            <p><?=$model->feature_4_name?></p>
                        </a>
                    </li>
                    <?}?>
                </ul>
            </div>
            <div class="productDetailsActionBox">
                <div class="priceBar row">
                    <div class="left_float">
                        <h6 class="gothamLight">$<?=number_format($model->product->basePrice(null,$model->user),2)?></h6>
                    </div>
                    <div class="right_float">
                        <p>Tax included</p>
                        <p>Free shipping</p>
                    </div>
                </div>
                <div class="variations radioOptionsModule">
                    <div class="checkboxUX optionBoxRadio selected">
                        <input type="checkbox" checked/>
                        <div class="content">
                            <div class="left_float">
                                <img src="<?=$model->product->featuredImage()?>" alt="<?=$product->image_alt?>"/>
                            </div>
                            <div>
                                <p class="name"><?=$model->product->name?></p>
                                <p class="price">USD $<?=number_format($model->product->basePrice(null,$model->user),2)?></p>
                            </div>
                        </div>
                    </div>
                    <?if(isset($model->link_product)){
                        foreach ($model->link_product as $lProd){?>
                    <div class="checkboxUX optionBoxRadio" data-prod_slug="<?=$lProd->slug?>">
                        <input type="checkbox"/>
                        <div class="content">
                            <div class="left_float">
                                <img src="<?=$lProd->featuredImage()?>" alt="<?=$product->image_alt?>"/>
                            </div>
                            <div>
                                <p class="name"><?=$lProd->name?></p>
                                <p class="price">USD $<?=number_format($lProd->basePrice(null,$model->user),2)?></p>
                            </div>
                        </div>
                    </div>
                    <?}
                        }?>
                </div>
                <div class="productSecondaryActions row">
                    <div class="col left_float quantity numbersRow">
                        <input type="text" disabled name="product-quantity" id="product-quantity" value="1">
                        <div class="quantToggleButtons">
                            <div class="inc button">
								<span>
								</span>
								<span>
								</span>
                            </div>
                            <div class="dec button disabled">
								<span>
								</span>
                            </div>
                        </div>
                    </div>
                    <div class="col right_float mainActions">
                        <a class="solidBlueBtn trueBlue addToCart mainProd" data-product_id="<?=$model->product->id?>">
                            <p>Add to Cart</p>
                        </a>
                        <a class="solidBlueBtn trueBlue goCheckout checkout" href="/cart">
                            <p>Checkout</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="productDetailSection mainProductInfo" data-vertical_screen="1">
            <div class="mainContentWidth">
                <?if(count($model->product->getAllProductImages()) != 0){?>
                <div class="productMainImagesWrapper">
                    <div class="swiperContainer-mainProductImages">
                        <div class="swiper-wrapper">
                            <?php foreach($model->product->getAllProductImages() as $image){ ?>
                            <div class="swiper-slide image-slide">
                                <div class="media" style="background-image:url('<?="/content/uploads/products/$image"?>')"></div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="swiper-button-prev" style="background-image:url('<?=FRONT_IMG?>productDetailImg_sliderArrow_left.png')"></div>
                        <div class="swiper-button-next" style="background-image:url('<?=FRONT_IMG?>productDetailImg_sliderArrow_right.png')"></div>
                    </div>
                </div>
                <?}?>
                <div class="productMainDetailsWrapper">
<!--                    <p class="mainDetailsParagraph"><b>An uprated camera</b> is equipped with a 1-inch 20-megapixel sensor capable of shooting 4K/60fps video and Burst Mode stills at 14 fps.  The adoption of titanium alloy and magnesium alloy construction increases the rigidity of the airframe and reduces weight, making the Phantom 4 Pro similar in weight to the Phantom 4. The Flight Autonomy system adds dual rear vision sensors and infrared sensing systems for a total of 5-direction of obstacle sensing and 4-direction of obstacle avoidance.</p>-->
                    <p class="mainDetailsParagraph"><?=$model->product->details?></p>
                </div>
            </div>
        </div>
        <div class="productDetailSection suggestedAccessories" data-vertical_screen="2">
            <div class="mainContentWidth row row_of_3">
                <div class="productDetailSectionHeader">
                    <h6 class="gothamLight">Recommended Accessories</h6>
                    <div class="right_float">
                        <a class="underlineBtn underlineBtn-blue underlineBtn-blue-black" href="<?=SITE_URL.'all_accessories/'.$model->product_category->slug?>" target="_blank">
                            <p>More</p>
                        </a>
                    </div>
                </div>
                <? foreach ($model->spares as $sp){?>
                    <div class="col">
                        <a class="gridProductContent" href="<?=SITE_URL.'accessories/'.$sp->slug?>">
                            <img src="<?=$sp->featuredImage()?>" alt="<?=$product->image_alt?>"/>
                            <p class="name"><?=$model->product->name?> - <?=$sp->name?></p>
                            <p class="price">USD $<?=number_format($sp->basePrice(),2)?></p>
                        </a>
                        <div class="btnRow">
                            <input type="text" disabled hidden name="product-quantity" id="product-quantity" value="1">
                            <a class="solidBlueBtn hollowBlueBtn addToCart accessProd" data-product_id="<?=$sp->id?>" >
                                <p>Add to Cart</p>
                            </a>
                        </div>
                    </div>
                <?}?>

            </div>
        </div>
        <?if($model->product_specs){?>
        <div class="productDetailSection productDetailSpecs" data-vertical_screen="3">
            <div class="mainContentWidth">
                <h4 class="phantomLight"><span><?=$model->product->name?></span> Specs</h4>
                <div class="row row_specs">
                    <div class="col">
                        <icon class="djiIcon djiSpecsIcon djiSpecsIcon4k"></icon>
                        <p>60fps</p>
                    </div>
                    <div class="col">
                        <icon class="djiIcon djiSpecsIcon djiSpecsIcon30min"></icon>
                        <p>30-minute Flight-time</p>
                    </div>
                    <div class="col">
                        <icon class="djiIcon djiSpecsIcon djiSpecsIconImage"></icon>
                        <p>1-inch Image Sensor</p>
                    </div>
                    <div class="col">
                        <icon class="djiIcon djiSpecsIcon djiSpecsIconHd"></icon>
                        <p>4.3mi (7km) long-range control</p>
                    </div>
                    <div class="col">
                        <icon class="djiIcon djiSpecsIcon djiSpecsIconCoding"></icon>
                        <p>Dual Coding</p>
                    </div>
                    <div class="col">
                        <icon class="djiIcon djiSpecsIcon djiSpecsIconObstacle"></icon>
                        <p>5-Direction of Obstacle Sensing</p>
                    </div>
                    <div class="col">
                        <icon class="djiIcon djiSpecsIcon djiSpecsIconMechanical"></icon>
                        <p>Mechanical Shutter</p>
                    </div>
                </div>
                <div class="collapsedSection">
                    <div class="row row_of_2 row_specs_stacked">
                <? $arr = array_chunk($model->product_specs,2,true);
                foreach ($arr as $spec){?>
                    <div class="col">
                    <? foreach ($spec as $key=>$value){?>
                        <div class="specsCategory">
                            <h6 class="gothamLight"><?=$key?></h6>
                            <ul>
                                <?foreach ($value as $row){
                                    ?>
                                    <li>
                                        <label><?=$row->name?></label>
                                        <p><?=$row->value?></p>
                                    </li>
                                <?}?>
                            </ul>
                        </div>
                    <?}?>
                </div>
                <?}?>
                    </div>
                    <a class="expandParent">
                <p>View All Specs</p>
            </a>
                </div>
            </div>
        </div>
        <?}?>
        <div class="productDetailSection videosSection" data-vertical_screen="4">

        </div>
        <?$s3 = new \EmagidService\S3();
        if(isset($model->feature_1)&&$model->feature_1){?>
        <div class="productDetailSection productFeatureCenter grey_bg" data-vertical_screen="5">
            <div class="media productFeatureMedia" style="background-image:url('<?=$s3->getUrlByKey($model->product->feature_image_1)?>')"></div>
            <?foreach ($model->feature_1 as $value){?>
                <h6 class="gothamLight"><?=$value['feature_title']?></h6>
                <p><?=$value['feature_description']?></p>
            <?}?>
        </div>
        <?}?>
        <?if(isset($model->feature_2)&&$model->feature_2){?>
        <div class="productDetailSection productFeatureCenter" data-vertical_screen="6">
            <div class="media productFeatureMedia" style="background-image:url('<?=$s3->getUrlByKey($model->product->feature_image_2)?>')"></div>
            <?foreach ($model->feature_2 as $value){?>
                <h6 class="gothamLight"><?=$value['feature_title']?></h6>
                <p><?=$value['feature_description']?></p>
            <?}?>
        </div>
        <?}?>
        <?if(isset($model->feature_3)&&$model->feature_3){?>
        <div class="productDetailSection productFeatureCenter" data-vertical_screen="7">
            <?foreach ($model->feature_3 as $value){?>
                <h6 class="gothamLight"><?=$value['feature_title']?></h6>
                <p><?=$value['feature_description']?></p>
            <?}?>
        </div>
        <?}?>
        <?if(isset($model->feature_4)&&$model->feature_4){?>
        <div class="productDetailSection productFeatureCenter" data-vertical_screen="8">
            <?foreach ($model->feature_4 as $value){?>
                <h6 class="gothamLight"><?=$value['feature_title']?></h6>
                <p><?=$value['feature_description']?></p>
            <?}?>
        </div>
        <?}?>
    </div>
</div>
<script>
	$(document).ready(function(){
		$('#color-content, #size-content').on('click','.optionBox',function(){
			var url = '/product/buildImages';
			if(window.location.search != ''){
				url += window.location.search;
			}
			var product_id = <?=$model->product->id?>;
			var color_id = $('#color-content').find(':checked').val() > 0 ? $('#color-content').find(':checked').val() : 0;
			var size_id = $('#size-content').find(':checked').val() > 0 ? $('#size-content').find(':checked').val() : 0;
			var data = {product_id:product_id,color_id:color_id, size_id:size_id};
			var thumb = $('.media_thumbs');
			var large = $('.main_media');
			var leg = $('.productMediaMasonry');
			var madeToOrder = $('#madeToOrder');
			var priceText = $('.price_text');
			var defaultPrice = $('.full_price');

			var topAdd = $('#topAdd');
			var add = $('#add');
			$.post(url,data,function(item){
				thumb.children().not('.mediaThumbVideo').remove();
				large.children().not('.video_media_wrapper').remove();
				leg.empty();
				thumb.append(item.thumb);
				large.append(item.large);
				leg.html(item.legshot);
				if(item.madeToOrder){madeToOrder.show()}else{madeToOrder.hide()}
				<?if(!$model->soldOut){?>
					if (item.status) {
						topAdd.removeClass('soldOutBtn').addClass('addToBag');
						topAdd.html('Add to Bag');
						add.removeClass('soldOutBtn').addClass('addToBag');
						add.html('Add to Bag');
					} else {
						topAdd.removeClass('addToBag').addClass('soldOutBtn');
						topAdd.html('Sold Out');
						add.removeClass('addToBag').addClass('soldOutBtn');
						add.html('Sold Out');
					}
				<?}?>
				if(item.sale){
					defaultPrice.html("$"+ "<?=number_format($model->product->price,2)?>");
					defaultPrice.show();
				}else{
					defaultPrice.hide();
				}
				priceText.html("<span>$</span>"+item.price);
			})
		});
		$('#submit-review').on('click',function(){
			var rating = $('[name=rating]').val();
			var name = $('[name=name]').val();
			var email = $('[name=email]').val();
			var title = $('[name=title]').val();
			var body = $('[name=body]').val();
			var data = {product_id:<?=$model->product->id?>,rating:rating,name:name,email:email,title:title,body:body};
			$.post('/product/addReview',data,function(ret){
				var json = $.parseJSON(ret);
				if(json.status == 'success'){
					window.location.replace('<?=$this->emagid->uri?>');
				} else {
					alert(json.message);
				}
			});
		});
		$('.upvote').on('click',function(){
			var review_id = $(this).data('review');
			var self = $(this);
			$.post('/product/upvote',{review_id:review_id},function(data){
				var json = $.parseJSON(data);
				if(json.status == 'success'){
					self.parent().find('.review-helpful').html(json.helpful + ' out of ' + (json.unhelpful+json.helpful) + ' found this helpful');
				} else {
					alert(json.message);
				}
			})
		});
		$('.downvote').on('click',function(){
			var review_id = $(this).data('review');
			var self = $(this);
			$.post('/product/downvote',{review_id:review_id},function(data){
				var json = $.parseJSON(data);
				if(json.status == 'success'){
					self.parent().find('.review-helpful').html(json.helpful + ' out of ' + (json.unhelpful+json.helpful) + ' found this helpful');
				} else {
					alert(json.message);
				}
			})
		});
	})
</script>