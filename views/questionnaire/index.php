<?function strReplacer($val){return str_replace(' ', '_', $val);}?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker-standalone.min.css" />

<div class="pageWrapper questionnairePageWrapper accountPageWrapper pageLeftNavSidebar">
    <div class="content">
        <h1 class="as_t">Private Label Questionnaire</h1>
        <?$token = $_GET["tkn"];?>
        <p class="as_l"><?=\Model\Inquiry::getItem(null,['where'=>"token = '{$token}'"])->name;?></p>

        <form method="post" action="<?= $this->emagid->uri ?>" enctype="multipart/form-data">
            <? $iterator = 1;
            foreach ($model->loop as $type => $value) {
                foreach ($value as $key => $item) { ?>
                    <div class="questionnaireInputRow">
                        <label><?= is_numeric($key) ? $item : $key ?></label>
                        <? switch ($type) {
                            case 'yn':?>
                                <select name="<?= is_numeric($key) ? strReplacer($item) : strReplacer($key) ?>">
                                    <? if (is_numeric($key)) { ?>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                                    <? } else {
                                        foreach ($item as $optionText) {?>
                                            <option value="<?= strReplacer($optionText) ?>"><?= $optionText ?></option>
                                        <? } ?>
                                    <? } ?>
                                </select>
                                <span class="dropdownSelectArrow"></span>
                                <?break;
                            case 'im':?>
                                <input type="file" name="<?=is_numeric($key) ? strReplacer($item) : strReplacer($key)?>"/>
                                <?break;
                            case 'dr':?>
                                <input class="daterange" name="<?=is_numeric($key) ? strReplacer($item) : strReplacer($key)?>"/>
                                <span class="datePickerIcon"></span>
                                <?break;
                            case 'tf':?>
                                <input type="text" name="<?=is_numeric($key) ? strReplacer($item) : strReplacer($key)?>"/>
                                <?break;
                        } ?>
                    </div>
                <? } ?>
            <? } ?>
            <input type="submit" class="btn btn_full_width save_account_info_btn" value="Submit"/>
        </form>
    </div>
</div>
<script src="<?=ADMIN_JS.'plugins/daterangepicker/daterangepicker.js'?>"></script>
<script>
    $(document).ready(function(){
        $(function() {
            $('.daterange').daterangepicker({
                timePicker: true,
                format: 'MM/DD/YYYY h:mmA',
                timePickerIncrement: 30,
                timePicker12Hour: true,
                timePickerSeconds: false,
                showDropdowns: true

            });
        });
    });
</script>