<style>
    .pagination{
        padding:0;
        font-size: 15px;
    }
    .pagination li{
        display: inline;
    }
</style>

<div class="pageWrapper shopSocialPageWrapper" data-limit="<?= $model->limit ?>"
     data-offset="<?= $model->offset + $model->limit ?>">
    <div class="fixed_h1_cushion">
        <h1 class="as_m">Shop the looks on our Instagram</h1>
    </div>
    <div class="grid shopSocialGrid">
        <div class="grid-sizer">
        </div>
        <? foreach ($model->shopInstagram as $item) { ?>
            <div class="grid-item">
                <a href="<?= $item->url ?>">
                    <div class="mediaWrapper">
<!--                        <img src="--><?//= UPLOAD_URL ?><!--shop_instagrams/573612a789d3a_banner3.jpg">-->
                        <img src="<?= UPLOAD_URL ?>shop_insbtagrams/<?= $item->featured_image ?>" >

                        <div class="shopSocialCardInfo">
                            <h4><span class="abs_vert_center"><?= $item->title ?></span></h4>

                            <div>
                                <icon class="instagram"></icon>
                                <? if (strlen($item->url) > 0) { ?>
                                    <h5 class="btn">Shop the Look</h5>
                                <? } ?>
                                <span><?= date('M d', strtotime($item->display_date)) ?></span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <? } ?>

    </div>
    <div id="loading-spinner">
        <div class="hanger"></div>
        <div class="discoball">
            <img src="<?=FRONT_IMG?>disco_ball.png" alt="">
        </div>
    </div>

</div>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
<script>
    $(document).ready(function(){
        var limit = <?=$model->limit?>;
        var offset = <?=$model->offset + $model->limit?>;
        var totalProductCount = <?=$model->productsCount?>;

        var offsetScroll = true;
        $(window).scroll(function() {


            if(($(window).scrollTop() >= ($(document).height() - $(window).height()-300))&&(offsetScroll==true)) {
                offsetScroll=false;
                console.log('activated1');
                if(offset < totalProductCount){
                    console.log(offsetScroll);

                    $.getJSON('/shopinstagram/buildShopInstagram',{limit:limit, offset:offset},function(data){
                        $('.shopSocialGrid').addClass("loadingProcessActive");
                        $("#loading-spinner").addClass("loading_spinner_active").delay(800).queue(function(appendData){
                            $('.shopSocialGrid').removeClass("loadingProcessActive");
                            $("#loading-spinner").removeClass("loading_spinner_active");
                            var gridIndex = offset;
//                            $.map(data.appendHtml,function(item){
//                                var $item = $(item);
//                                $('.grid').imagesLoaded(function(){
//                                    $('.grid').append($item).masonry('appended',$('.grid-item')[gridIndex]);
//                                });
//                                gridIndex++;
//                            });
                            var el = $(data.appendHtml);
                            var map = $.map(el,function(item){
                                return $(item);
                            });
                            var $grid = $('.grid').append(map).imagesLoaded(function(){
//                                $grid.masonry('appended',map,true);
                                for(var i = offset; i < (offset+limit);i++){
                                    $('.grid').masonry('appended',$('.grid-item')[i],true);
                                }
                                offset += limit;
                            });
                            appendData();

                            if(data.status==="success"){
                                offsetScroll=true;
                                $('.productGridItemSwatchesSlider').flickity({
                                    // options
                                    cellAlign: 'left',
                                    adaptiveHeight: true,
                                    prevNextButtons: true,
                                    pageDots:false,
                                    groupCells: 3,
                                    contain: true,
                                    arrowShape: {
                                        x0: 10,
                                        x1: 65, y1: 50,
                                        x2: 70, y2: 45,
                                        x3: 20
                                    }
                                });
                            }
                        });

                    });
                }
            }
        });

    })
</script>
<!--<ul class="pagination">-->
<?// $c = 1;
//    while ($c <= $model->pageCount) {?>
<!--    <li><a href="--><?//= strtok($_SERVER["REQUEST_URI"], '?') . '?page=' . $c ?><!--">--><?//= $c ?><!--</a></li>-->
<?// $c++; } ?>
<!--</ul>-->