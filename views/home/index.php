<section class='expand'>
    <? foreach($model->campaigns as $campaign) {?>
    <section class='content_holder'>
	    <? foreach($campaign->getProducts() as $product) {?>
	        <div class='product'>
	            <?$img_path = $product->featuredImage()?>
	            <a href="/products/<?=$product->slug?>">
	                <img src="<?= $img_path ?>">
	            </a>
	        </div>
	    <? } ?>
	</section>
    <? } ?>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        <? if(isset($_GET['link'])){ ?>
            $('#<?=$_GET['link']?>').click();
        <? } ?>
    });
</script>
