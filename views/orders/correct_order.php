<link rel="stylesheet" href="<?=auto_version(FRONT_CSS."checkoutFlow.css")?>">

<style>
	.row:before{
		position: relative;
	}
    input.pay_field_invalid {
        border: 2px solid red !important;
    }
    input.pay_field_valid {
        border: 2px solid #d6d6d6 !important;
    }
    input[disabled]{
        opacity: .3;
    }

    .fix_pay {
    	padding-top: 150px !important;
    }

    .fix_pay h1 {
    	text-align: center !important;
    }

    .pageWrapper.checkoutPageWrapper .total_cost.orderSummaryTotal {
    	margin-top: 18px;
    }

    .pageWrapper.checkoutPageWrapper.shippingCheckoutPageWrapper .row.secondaryFocus {
        margin-top: 40px !important;
    }

    .order_info {
        margin-bottom: 50px;
    }

    .order_info img {
        /*height: 130px;*/
        margin: auto;
        display: block;
        width: auto;
        height: 100px;
    }

    .product {
        text-align: center;
        margin-top: 20px;
        margin: 0 40px;
    }

    .order_info > div {
        display: flex;
        justify-content: center;
    }

    #bill {
        max-width: 900px;
    }

    .products {
        padding-bottom: 50px;
        border-bottom: 1px solid #eaeaea;
    }

    .payment_info, .address_info {
        margin-top: 50px;
        margin-bottom: 50px;
        padding-bottom: 50px;
        border-bottom: 1px solid #eaeaea;
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }

    .address_info {
        justify-content: space-between;
        flex-wrap: wrap;
    }

    .payment {
        margin: 15px;
    }

    .payment p:nth-child(2), .address p:nth-child(2) {
        font-weight: bold;
    }

    .address {
        margin-top: 30px;
        width: 45%;
        margin: 2%;
        margin-top: 0;
        text-align: center;
    }

    @media ( max-width: 600px ) {
        .products {
            display: flex;
            flex-wrap: wrap;
            flex-direction: column;
        }

        #bill img {
            height: 100px;
        }
        .product {
            margin-bottom: 12px;
        }

        .address {
            width: 100%;
        }

        .address:first-child {
            margin-bottom: 30px;
        }

    }
</style>
<div class="pageWrapper checkoutPageWrapper cartPageWrapper shippingCheckoutPageWrapper emailCheckoutPageWrapper shippingCheckoutPageWrapper fix_pay">
	<div class="row content_width_1000">


        <div id='forms' class="emailFormWrapperCol shippingFormWrapperCol col">
        <div class="emailFormWrapperCol shippingFormWrapperCol paymentFormWrapperCol col">
            <div class='divider'>
                
            <form id='bill'method="post" action="<?=$this->emagid->uri?>" novalidate>

                <!--			--><?//=dd($model->cart->products,$model->cart->cart)?>
                <h1>ORDER VALIDATION</h1>

                <div class='order_info'>
                    <div class='products'>
                        <? foreach($model->products as $op){ ?>
                            <?$p = \Model\Product::getItem($op->product_id)?>
                            <div class='product'>
                                <img src="<?=$p->featuredImage(null,null)?>" style="">
                                <p><?=$p->name?> (<?=$p->color?>)
                                    <br/><span><?=$op->quantity?></span></p>
                            </div>
                        <? } ?>
                    </div>

                    <div class='payment_info'>
                        <?
                        $bd = $model->order->calculate_total();
                        $updates = $bd['updates'];
                        ?>
                        <? $order = $model->order ?>
                        <div class='payment'>
                            <p>Subtotal</p>
                            <p>
                                $<?=number_format($updates['subtotal'],2) ?></p>
                        </div>
                        <div class='payment'>
                            <p>Shipping</p>
                            <p>
                                $<?=number_format($updates['shipping_cost'],2) ?></p>
                        </div>
                        <div class='payment'>
                            <p>Duty</p>
                            <p>
                                $<?=number_format($updates['duty'],2) ?>
                            </p>
                        </div>
                        <div class='payment'>
                            <p>Tax</p>
                            <p>
                                $<?=number_format($updates['tax'],2) ?>
                            </p>
                        </div>
                        <div class='payment'>
                            <p>Correct Total</p>
                            <p>$<?=number_format(array_sum($bd['charges']),2) ?></p>
                        </div>
                        <div class='payment' style='text-align: center; width: 100%;'>
                            <p>Remaining Balance</p>
                            <p style="color: red; font-weight: bold">$<?=round($model->order->difference,2)?></p>
                        </div>
                    </div>
                    <div class='address_info'>
                        <div class='address'>
                            <p>Shipping to</p>
                            <p><?=$order->ship_address?><br>
                                <?=$order->ship_address2?$order->ship_address2.' <br />':0 ?>
                                <?=$order->ship_city.', '.$order->ship_state.' '.$order->ship_zip?><br />
                                <?=$order->ship_country?></p>
                        </div>
                        <div class='address'>
                            <p>Billing Address</p>
                            <p><?=$order->bill_address?><br>
                                <?=$order->bill_address2?$order->bill_address2.' <br />':0 ?>
                                <?=$order->bill_city.', '.$order->bill_state.' '.$order->bill_zip?><br />
                                <?=$order->bill_country?></p>
                        </div>
                    </div>
                </div>

                <div class="creditCardPaymentFormSection" >
                <p>Credit Card entered must match billing information shown above.</p>

                <div style="width: 97%">
                    

                    <input placeholder='Name on Card' type="text" name="card_name" maxlength="60" class="card_name" id="cardUserName" required value="<?=$model->order->ship_first_name.' '.$model->order->ship_last_name?>">
                    </div>
                    
                <div style="width: 97%">
                    
                    <!-- <p for="ccnumber">Credit Card Number</p> -->
                    <div class="cardNumberInputWrapper">
                        <input placeholder='Credit Card Number' type="text" pattern="[0-9]*" name="cc_number" maxlength="16" class="card_num" id="ccnumber"  required>
                        <div id="creditCardIcons">
                            <div class="media"></div>
                        </div>
                    </div>
                    </div>
                    
                    <div class="form50" id="no_margin">
                    <input type="hidden" name="cc_type" id="cc_type">
                    <input type="hidden" name="email" value="<?=$model->order->email?>">
                    <!-- <p for="checkoutPhone">Expires on</p> -->
                    <fieldset class="selectUIWrapper selectExpMonth">
                        <select style='margin-top: 8px;' name="cc_expiration_month" required>
                            <?foreach(get_month() as $value=>$item){?>
                                <option value="<?=$value?>"><?=$item?></option>
                            <?}?>
                        </select>
                        <span class="downArrowICon">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                                        <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                                        L-321,348.1z"></path>
                                    </svg>
                        </span>
                    </fieldset>
                    </div>
                    
                    <div class="form50">
                    <fieldset class="selectUIWrapper selectExpYear">
                        <select style='margin-top: 8px;' name="cc_expiration_year" required>
                            <?$carbon = \Carbon\Carbon::now()->year;
                            for($i = $carbon; $i < $carbon+14; $i++){?>
                                <option value="<?=$i?>"><?=$i?></option>
                            <?}?>
                        </select>
                        <span class="downArrowICon">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                                        <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
                                        L-321,348.1z"></path>
                                    </svg>
                        </span>
                    </fieldset>
                    </div>
                    <!--			<input type="text" name="card_expire" tabindex="3" class="card_expire" id="checkoutPhone">-->
                    <div style='width: 50%; position: relative; max-width: 200px;'>
                    <p class='secode' for="secCode" style='max-width: 200px; position: relative; width: 100%;'>
                        <div style="display:inline-block" class="cvv_help_box">
                            <div class="trigger">
                                <span class="as_m">?</span>
                            </div>
                            <div class="ccv_help_box">
                                <p>For security reasons, please enter your card verification number (CVV) or card identification number (CID). Find this three-digit number on the back of your Visa, MasterCard and Discover cards in the signature area, or the four-digit number on the front of your American Express card, above the credit card number.</p>
                                <span class="ccv_tip_icons">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="384.5 10.7 201.9 58.3" enable-background="new 384.5 10.7 201.9 58.3" xml:space="preserve">
                                        <g id="cvv_1_">
                                            <g>
                                                <path fill="#C3C3C3" d="M468.3,59.9c0,3.7-3,6.5-6.8,6.5h-67.9c-3.8,0-6.8-2.9-6.8-6.5V20.6c0-3.7,3-6.5,6.8-6.5h67.9
                                                    c3.8,0,6.8,2.9,6.8,6.5L468.3,59.9C468.3,59.9,468.3,59.9,468.3,59.9z"></path>
                                            </g>
                                            <rect x="386.8" y="22.3" fill="#222222" width="81.5" height="10.5"></rect>
                                            <rect x="393.2" y="38" fill="#FFFFFF" width="48.9" height="10.5"></rect>
                                            <rect x="397.1" y="41.9" fill="#222222" width="40.9" height="2.6"></rect>
                                            <rect x="449.8" y="38" fill="#FFFFFF" width="11.3" height="10.5"></rect>
                                            <rect x="451.1" y="41.9" fill="#222222" width="8.4" height="2.6"></rect>
                                            <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="455.4" cy="43.2" rx="10.6" ry="10.3"></ellipse>
                                        </g>
                                        <g id="cvv-amex_1_">
                                            <g id="cvv-amex">
                                                <g>
                                                    <g>
                                                        <path fill="#5EC1EC" d="M586.4,59.7c0,3.8-3.1,6.7-7,6.7h-69.7c-3.9,0-7-3-7-6.7V19.4c0-3.8,3.1-6.7,7-6.7h69.7c3.9,0,7,3,7,6.7
                                                            V59.7z"></path>
                                                        <g>
                                                            <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
                                                                c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
                                                            <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
                                                                v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
                                                                "></path>
                                                        </g>
                                                        <g>
                                                            <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
                                                                c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
                                                            <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
                                                                v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
                                                                "></path>
                                                        </g>
                                                    </g>
                                                    <rect x="507.5" y="43.9" display="none" fill="#FFFFFF" width="69.7" height="10.6"></rect>
                                                    <rect x="513.4" y="47.9" fill="#222222" width="56.1" height="2.7"></rect>
                                                    <rect x="565.3" y="31.4" fill="#FFFFFF" width="11.1" height="10.7"></rect>
                                                    <rect x="566.7" y="35.4" fill="#222222" width="8.4" height="2.7"></rect>
                                                </g>
                                                <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="570.8" cy="36.8" rx="10.9" ry="10.5"></ellipse>
                                            </g>
                                        </g>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </p>
                    <input style='max-width: 200px;' type="text" name="cc_ccv" placeholder="Security Code" maxlength="4" tabindex="4" class="card_cvv" id="secCode" required>

                    </div>
                    <div>
                        <p class='form_statement' for="agree_to_terms" style='max-width: 200px; position: relative; width: 100%;'>
                            You must agree to the <a onclick="$('#terms').click()">Terms of Service</a> to checkout.
                        </p>
                    <input id="agreeTerms" type="checkbox" name="agree_to_terms" />
                    </div>
                </div>
                


                <div class="row secondaryFocus">
                    <div class="right_float total_costs">
                        <input type="submit" value="Update Order" id="creditCardPayBtn" class="button" disabled>
                        <div class='total_cost orderSummaryTotal'>
                            <p style='font-size: 16px;'>$<?=number_format($model->order->difference,2)?></p>
                        </div>
<!--                        <input type="button" value="Proceed with Paypal" id="paypalPayBtnClicker" class="button">-->
                    </div>
                </div>

<!--                <input name="total" type="hidden" value="--><?//=round($model->order->difference)?><!--"/>-->
                <input name="total" type="hidden" value="<?=round($model->order->difference,2)?>"/>
                <input name="orderId" type="hidden" value="<?=round($model->order->id,2)?>"/>
                <input name="ref_num" type="hidden" value="<?=$model->order->ref_num?>"/>
                <!--			</form>-->
            </form>
            </div>
        </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
        $('input[name=cc_number]').on('input',function(){
			$('#cc_type').val(creditCardType($(this).val())[0].type);
		});
		$('#ccnumber').on('keyup',function(){
		    var ccnumber = $(this).val();
		    ccnumber = ccnumber.replace(/[^0-9-]/g,'');
		    $(this).val(ccnumber);
        });
	})
</script>
<script type="text/javascript">
   //Separate Forms
   $(document).ready(function(){
      $('#agreeTerms').change(function(){
          if($(this).is(':checked')){
              $('#creditCardPayBtn').attr('disabled',false);
          } else {
              $('#creditCardPayBtn').attr('disabled',true);
          }
      });
      $(':input:not(:visible)').attr('tabindex',-1);
   });
</script>
