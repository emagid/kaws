<link rel="stylesheet" href="<?=auto_version(FRONT_CSS."checkoutFlow.css")?>">

<style>

    .button {
        -webkit-appearance: none;
    }
    
	.row:before{
		position: relative;
	}
    input.pay_field_invalid {
        border: 2px solid red !important;
    }
    input.pay_field_valid {
        border: 2px solid #d6d6d6 !important;
    }
    input[disabled]{
        opacity: .3;
    }

    .fix_pay {
    	padding-top: 150px !important;
    }

    .fix_pay h1 {
    	text-align: center !important;
    }

    .pageWrapper.checkoutPageWrapper .total_cost.orderSummaryTotal {
    	margin-top: 18px;
    }

    .pageWrapper.checkoutPageWrapper.shippingCheckoutPageWrapper .row.secondaryFocus {
        margin-top: 40px !important;
    }

    .order_info {
        display: flex;
        justify-content: center;
        margin-top: 50px;
        margin-bottom: 40px;
    }

    td {
        padding: 0 10px;
    }

    td p {
        font-size: 14px;
    }

    .order_info img {
        /*height: 130px;*/
        margin: auto;
        display: block;
        width: auto;
        height: 100px;
    }

    .product {
        text-align: center;
        margin-top: 20px;
        margin: 0 40px;
    }


    #bill {
        max-width: 900px;
    }

    .products {
        width: 195px;
    }

    th {
        font-size: 14px;
        font-weight: bold;
    }

    .button {
        position: absolute;
        margin: 0;
        left: 50%;
        transform: translateX(-50%);
    }

    .payment_info, .address_info {
        margin-top: 50px;
        margin-bottom: 50px;
        padding-bottom: 50px;
        border-bottom: 1px solid #eaeaea;
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }

    .address_info {
        justify-content: space-between;
        flex-wrap: wrap;
    }

    .payment {
        margin: 15px;
    }

    tr:last-child td {
        padding-top: 40px;

    }

    td p {
        text-align: center;
    }

    .payment p:nth-child(2), .address p:nth-child(2) {
        font-weight: bold;
    }

    .address {
        margin-top: 30px;
        width: 45%;
        margin: 2%;
        margin-top: 0;
        text-align: center;
    }

    .notify {
        background-color: black;
        position: absolute;
        top: 70px;
        width: 100%;
        padding: 20px;
        color: white;
        text-align: center;
        font-size: 12px;
    }

    @media ( max-width: 1000px ) {
        .order_info {
            overflow-x: auto;
            justify-content: flex-start;
            padding-bottom: 50px;
        }

        .divider {
            margin: 0 !important;
        padding: 0 20px;
        }
    }

    @media screen and (max-width: 767px) { 
        .emailFormWrapperCol.shippingFormWrapperCol.col {
            padding-right: 0;
        }

        .emailFormWrapperCol.shippingFormWrapperCol.paymentFormWrapperCol.col {
            width: 100%;
        }

        .emailFormWrapperCol.shippingFormWrapperCol.paymentFormWrapperCol.col {
            padding-bottom: 300px;
        }
    }
</style>
<div class="pageWrapper  shippingCheckoutPageWrapper emailCheckoutPageWrapper shippingCheckoutPageWrapper fix_pay">
	<div class="row content_width_1000">


        <div id='forms' class="emailFormWrapperCol shippingFormWrapperCol col">
        <div class="emailFormWrapperCol shippingFormWrapperCol paymentFormWrapperCol col">
            <div class='divider'>
                
            <form id='verify' method="post" novalidate>

                <!--			--><?//=dd($model->cart->products,$model->cart->cart)?>
                <h1>Verify Your Orders</h1>
                <p>Please confirm your intended purchase below by clicking the box next to the order number(s) you would like to receive. The order(s) you do not select will be voided and refunded.<p>

                <p><em><i>The limit is 1 of each color per customer. You will not be able to select more than 1 of each color item.</i></em></p>

                <p>We will do our best to combine your orders into 1 order if the addresses are the same. There is no guarantee your orders will be combined. </p>

                <div class='order_info'>
                    <table>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Order #</th>
                                <th>Amount</th>
                                <th>Products</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Phone</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?foreach ($model->orders as $order) {
                            $prod_counts = [105=>'brown_count',106=>'grey_count',107=>'black_count'];?>
                            <?$order = (Object)$order?>
                            <tr>
                                <td>
                                    <input type="hidden" name="orders[<?=$order->id?>]" value="0" />
                                    <? $data_products = [];
                                        if($order->brown_count){
                                            $data_products[] = "brown";
                                        }
                                        if($order->grey_count){
                                            $data_products[] = "grey";
                                        }
                                        if($order->black_count){
                                            $data_products[] = "black";
                                        }
                                        $data_count = count($data_products);
                                        $data_products = implode(',',$data_products);
                                    ?>
                                    <input type="checkbox" class='order-select' name="orders[<?=$order->id?>]" <?=$order->brown_count?'data-brown_count="1"':''?> <?=$order->grey_count?'data-grey_count="1"':''?> <?=$order->black_count?'data-black_count="1"':''?> data-products="<?=$data_products?>" data-count="<?=$data_count?>" value="1" />
                                </td>
                                <td><p><?=$order->id?></p></td>
                                <td><p><?=$order->total?></p></td>
                                <td>
                                    <? foreach($model->products as $pi => $p){ ?>
                                        <? if($order->{$prod_counts[$pi]} > 0) {?>
                                        <div class='products'>
                                            <div class='product'>
                                                <img src="<?=$p->featuredImage(null,null)?>" style="height:25px">
                                                <p style='font-size: 10px'><?=$p->name?> (<?=$p->color?>)
                                                    <span>1</span></p>
                                            </div>
                                        </div>
                                        <? } ?>
                                    <? } ?>
                                </td>
                                <td><p><?=$order->name?></p></td>
                                <? $o = \Model\Order::getItem($order->id)?>
                                <td style='display: block; width: 200px;'><p><?=$o->getShippingAddr()?></p></td>
                                <td><p style='max-width: 200px; word-wrap: break-word;'><?=$order->email?></p></td>
                                <td><p style='max-width: 200px; word-wrap: break-word;'><?=$order->phone?></p></td>
                            </tr>
                        <? } ?>
                        </tbody>
                    </table>
                </div>

                <div style='position: relative;'>
                    <input class='button' type="submit" value="Submit" />
                    <input name="email" type="hidden" value="<?=$model->email?>" />
                    <div class='notify'>
                        <p>Note that no matter what orders are selected you can only recieve one of each color</p>
                    </div>
                </div>
            </form>
            </div>
        </div>
	</div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('#verify').submit(function(e){
            e.preventDefault();
            if($('.order-select:checked').length == 0 &&
            !confirm('Error! You have not selected an order. If you would like to select an order, please click Cancel to go back. If you would like all of your orders voided, please click OK. ')){
                return false;
            }
            var _data = $(this).serialize();
            $.post('/orders/verify_orders',_data,function(data){
                if(data.status){
                    alert("Your order has been updated! thank you");
                    window.location.href = "https://kawsone.com/"
                } else {
                    alert("Please try again");
                }
            })
        })

        $('.order-select').change(function(){
           var name = $(this).prop('name');
           var products = $(this).data('products');
           var count = $(this).data('count');
           var stayChecked = $(this);
           for(var color in stayChecked.data()){
               if(color == 'products' || color == 'count') {

               } else {
                   $('.order-select[data-products="'+products+'"]').not('[name="'+name+'"]').prop('checked', false);
                   $('.order-select[data-'+color+']').not('[data-count="'+count+'"]').prop('checked', false);

               }
           }
        });
    });
</script>
