
<section class="container order_conf">
	<h1>Order Summary</h1>

	<div class="order_details">
	<div class="PrintOnly">
   
        <br>
        <br>
        <?=$model->order->date?>
    </div>
		<div class="col-sm-24 col-md-24">
			<h3><span>#<?=$model->order->id?> - <?=$model->order->date?></span></h3>
            <br />
            <p>
                We have received your order. Please allow 5-7 business days for order processing and verification.
                <br>
                You will receive a tracking number prior to shipping, please allow an additional 7-10 business days for delivery.
            </p>
            <h2>Shipping to...</h2>
            <table>
                <tr>
                    <td><strong>Name:</strong></td>
                    <td><?= $model->order->shipName() ?></td>
                </tr>
                <tr>
                    <td><strong>Address:</strong></td>
                    <td><?= $model->order->getShippingAddr() ?></td>
                </tr>
                <tr>
                    <td><strong>Country:</strong></td>
                    <td><?= $model->order->ship_country ?></td>
                </tr>
                <tr>
                    <td><strong>Email:</strong></td>
                    <td><?= $model->order->email ?></td>
                </tr>
                <tr>
                    <td><strong>Phone:</strong></td>
                    <td><?= $model->order->phone ?></td>
                </tr>
                <tr>
                    <td><strong>Status:</strong></td>
                    <td><?= $model->order->status?></td>
                </tr>
                <? if($model->order->tracking_number != '') { ?>
                    <tr>
                        <td><strong>Tracking Number:</strong></td>
                        <td><a href="https://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=<?= $model->order->tracking_number?>" target="_blank"><?= $model->order->tracking_number?></a></td>
                    </tr>
                <? } ?>
            </table>

            <div class='return_form'>
            	<button class='button'>CONTACT US</button>
            	<form id="ticket">
                <div class="modal_formWrapper contact">
                    <h2>Send us an email about your order</h2>
                    <div class="inputRow">
                        <label>Name</label>
                        <input name="name" type="text" required/>
                    </div>
                    <div class="inputRow">
                        <label>Email Address</label>
                        <input name="email" type="email" required/>
                    </div>
                    <input name="orderID" type="hidden" value="<?=$model->order->id?>"/>
                    <div class="inputRow">
                        <label>Subject</label>
                        <input name="subject" type="text" required/>
                    </div>
                    <div class="inputRow">
                        <!--				<label>Message</label>-->
                        <label>Message</label>
                        <textarea name="message" name="message" required></textarea>
                    </div>
                    <button class='button' type="submit">Send</button>
                </div>
            </form>
            <br />
            <p>
                If you have been charged incorrectly for your order, we will contact you to confirm the corrections.
                <br />
                Thank you for your patience
            </p>
            </div>

			<? foreach($model->order_products as $order_product) { ?>
			<div class='order_products_flex'>
				
				<div class="col-md-24 ordered_products">
					<h4>
						<div class="col-sm-4 col-md-4 order_image">
						<?php 
				            $img_path = $order_product->product->featuredImage();
				        ?>
		        			<a href="<?=SITE_URL.'products/'.$order_product->product->slug?>"><img src="<?php echo $img_path; ?>" alt="<?=$product->image_alt?>"/></a>
						</div>
						<a href="<?=SITE_URL.'products/'.$order_product->product->slug?>"><?=$order_product->product->name?> (<?=$order_product->product->color?>)</a>
						<p>Quantity: <?=$order_product->quantity?></p>
					</h4>
					</div>
				<? } ?>
			</div>
		</div>
		<div class="col-md-24 order_summary table-responsive">
			<h3>Payment Summary</h3>
			<table class="table" >
				<tr>
					<td><strong>SubTotal:</strong></td>
					<td>$<?=number_format($model->order->subtotal,2)?></td>
				</tr>
				<?
					if (!is_null($model->order->coupon_code)){
						if ($model->order->coupon_type == 1){
							$savings = $model->order->coupon_amount;	
						} else if ($model->order->coupon_type == 2){
							$savings = $model->order->subtotal * $model->order->coupon_amount/100;
						}
				?>
						<tr>
							<td>Savings:</td>
							<td>$<?=number_format($savings,2)?></td>
						</tr>
				<? } ?>
				<tr>
					<td><strong>Shipping:</strong></td>
					<td>$<?=number_format($model->order->shipping_cost,2)?></td>
				</tr>
                <? if($model->order->tax > 0) {?>
				<tr>
					<td><strong>Tax:</strong></td>
					<td>$<?=number_format($model->order->tax,2)?></td>
				</tr>
				<? } ?>
                <? if($model->order->duty > 0) {?>
				<tr>
					<td><strong>Duty & Taxes:</strong></td>
					<td>$<?=number_format($model->order->duty,2)?></td>
				</tr>
				<? } ?>
				<tr>
					<td><b>Total:</b></td>
					<td>$<?=number_format($model->order->total,2)?></td>
				</tr>
			</table>
		</div>
	</div>
</section>


<style type="text/css">
 .PrintOnly {         display:none; } 
	@media print {
		.top-bar, .site-nav, .banner.cms, footer .container, h2, .order_status, h3  {
			display: none;
		}
		.ordered_products img {
			width: 50px;
			height: 50px;
		}
		.PrintOnly {     display:block; margin:0 auto; width: 199px;
			height: 71px;}
		 
a[href^="/"]:after {content: " ";}
 
	}
</style>

<script>
	$(function(){
		$('#ticket').submit(function(e){
			e.preventDefault();
		    // console.log('SSSSS-DDDD-OO__gfF');
		    var pdata = $(this).serialize();
		    $.post('/orders/ticket',pdata,function (data) {
                if(data.state){
                    buildNotification(1,'Your message has been received, we will get back to you as soon as possible');
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                    $('.return_form').hide();
                }
            });
		});
	})
</script>

    <?
        if (($_SERVER['SERVER_NAME'] == 'modernvice.com' || $_SERVER['SERVER_NAME'] == 'www.modernvice.com')
            && isset($_SESSION['purchase_conversion']) && $_SESSION['purchase_conversion']) {

            unset($_SESSION['purchase_conversion']);
    ?>
<? } ?>