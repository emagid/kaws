<?php
/**
 * Created by PhpStorm.
 * User: Foran
 * Date: 11/10/2017
 * Time: 6:30 PM
 */

?>

<div class="pageWrapper aboutPageWrapper">
<!--
	<div class="dealsHero">
		<div id="couponBanner" width="100%" style="height:100%" class="hide-mobile">

		</div>
		<div class="show-mobile coupon-page-hero-mobile" style="background-image:url('<?=FRONT_IMG?>about-hero-mobile.jpg')">
		</div>
		<div class="mainContentWidth">
			<h2 class="dji-text">Great drone deals!</h2>
			<h6 class="gothamLight">Here are the latest drone deals</h6>
		</div>
	</div>
-->
    
    	<div class="couponPageHero">
		<div class="media" style="background-image:url('<?=FRONT_IMG?>drone_deal_hero.jpg')"></div>
		<div class="mainContentWidth">
			<h1 class="dji-text">Great drone deals</h1>
		</div>
	</div>
	<div class="mainContentWidth" id="dealContent">
        <? foreach($model->coupons as $coupon) {?>
		<div class="textSectionHeader apply-coupon" data-code="<?=$coupon->code?>" id="dealHeader">
<!--			<h6 class="gothamLight"><//?=$coupon->name?></h6>-->
            <h6 class="gothamLight">Coupon Code: <?=$coupon->code?></h6>
            <div></div>
            <? $discount = $coupon->discount_amount;
            $discount = $coupon->discount_type == 1 ? '$'.$discount : $discount.'%' ?>
            <p class="gothamBook"><span class="amount-off"><?=$discount?></span> OFF ANY PURCHASE
                <?if($coupon->min_amount != 0){?>
                    OF $<?=number_format(floatval($coupon->min_amount),2) ?> OR MORE
                <?}?>
                IN QUALIFYING CATEGORIES!
            </p>
            <?
            if($coupon->active_products){
                $product = \Model\Product::getItem(null, ['where'=>'id IN ('.implode(",",json_decode($coupon->active_products,true)).')',
                                                             'orderBy'=>'type ASC, name ASC']);
            } else {
                $psql = "SELECT p.* FROM product p, product_categories pc, category c WHERE pc.product_id = p.id AND c.id = pc.category_id AND p.type = '1' AND c.id in (".implode(",",json_decode($coupon->active_categories,true)).")";
                $product = \Model\Product::getItem(null,['sql'=>$psql]);
            } ?>
            <img class="example-image" src="<?=$product->featuredImage()?>" alt="<?=$product->image_alt?>" >
            <div><?=$coupon->description?></div>
		</div>
        <? } ?>
        <div class=" textSectionHeader apply-coupon" data-code="<?=$coupon->code?>" id="dealHeader" style='pointer-events:none;'>
<!--            <h6 class="gothamLight"><//?=$coupon->name?></h6>-->
            <h6 class="gothamLight">COUPON CODE: FREE3DUS</h6>
            <div></div>
            <div><p><strong>Free 3-Day Shipping within the continental U.S. on purchases of $150.00 or more.</strong></p></div>
            <div><p>Enter coupon code at checkout and select 3-Day Shipping.</p></div>
        </div>
       <div class=" textSectionHeader apply-coupon" data-code="<?=$coupon->code?>" id="dealHeader" style='pointer-events:none;'>
<!--            <h6 class="gothamLight"><//?=$coupon->name?></h6>-->
            <h6 class="gothamLight">COUPON CODE: FREE2DUS</h6>
            <div></div>
            <div><p><strong>Free 2-Day Shipping within the continental U.S. on purchases of $150.00 or more.</strong></p></div>
            <div><p>Enter coupon code at checkout and select 2-Day Shipping.</p></div>
        </div>
        <a class='point' href="/categories/spark"><div class=" textSectionHeader apply-coupon" data-code="<?=$coupon->code?>" id="dealHeader" style='pointer-events:none;'>
<!--            <h6 class="gothamLight"><//?=$coupon->name?></h6>-->
            <h6 class="gothamLight">SPECIAL PRICE: ALL SPARK COLORS</h6>
            <div></div>
            <img class="example-image" src="<?= FRONT_IMG ?>spark.jpg" alt="<?=$product->image_alt?>" >
            <div><p>All colors in the Spark series now only $399 ($499 regularly).</p></div>
        </div></a>
        <a  class='point' href="/categories/mavic"><div class=" textSectionHeader apply-coupon" data-code="<?=$coupon->code?>" id="dealHeader" style='pointer-events:none;'>
<!--            <h6 class="gothamLight"><//?=$coupon->name?></h6>-->
            <h6 class="gothamLight">SPECIAL PRICE: $799</h6>
            <div></div>
            <img class="example-image" src="<?= FRONT_IMG ?>mavic.jpg" alt="<?=$product->image_alt?>" >
            <div><p>Your choice: Refurbished Mavic Pro or Mavic Pro w/o Controller now one low price.</p></div>
        </div></a>
        <a class='point' href="/products/mavic-pro-main"><div class="  textSectionHeader apply-coupon" data-code="<?=$coupon->code?>" id="dealHeader"  style='pointer-events:none;'>
<!--            <h6 class="gothamLight"><//?=$coupon->name?></h6>-->
            <h6 class="gothamLight">SPECIAL PRICE: MAVIC PRO</h6>
            <div></div>
            <img class="example-image" src="https://djinyc.com/content/uploads/products/58d59257a9039_1474991197000_IMG_690941.jpeg" >
            <div><p>Mavic Pro now only $899 ($999 regularly).</p></div>
        </div></a>
        <a class='point' href="/products/mavic-pro-fly-more-combo-platinum"><div class="  textSectionHeader apply-coupon" data-code="<?=$coupon->code?>" id="dealHeader"  style='pointer-events:none;'>
<!--            <h6 class="gothamLight"><//?=$coupon->name?></h6>-->
            <h6 class="gothamLight">SPECIAL PRICE: MAVIC PRO PLATINUM FLY MORE COMBO</h6>
            <div></div>
            <img class="example-image" src="https://djinyc.com/content/uploads/products/591de2da105a4_1484084740000_1285010.jpeg" >
            <div><p>$50 Off Mavic Pro Fly More Combos.</p></div>
        </div></a>
	</div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.apply-coupon').on('click',function(){
            var code = $(this).data('code');
            $.post('/coupon/add',{'code':code},function(data){
                var json = $.parseJSON(data);
                if(json.status == 'success'){
                    alert('Your coupon will be applied at checkout!');
                    window.location.reload('/checkout/payment');
                } else {
                    alert(json.message);
                }
            })
        });
    });
</script>