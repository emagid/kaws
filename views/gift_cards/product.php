<div class="pageWrapper productPageWrapper pageLeftNavSidebar">
	<div class="content">
		<div class="productHeader">
			<h1 class="as_r"><?=$model->gift_obj->name?></h1>
			<h3 class="as_r price_text"><span>$</span><?=number_format($model->gift_obj->amount,2)?></h3>
		</div>
		<div class="row productDetailSection">
			<div class="col productMedia quickSlideInactive">
				<div class="main_media">
					<div class="media_enlarged media_enlarged_1 video_media_wrapper">
						<div class="mediaWrapper scrubber">


							<div class="dragToRotate ready">
								<div class="dragToRotateContainer">
									<div class="arrowLeftContainer">
										<icon class="arrowLeft">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
												<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
												L-321,348.1z"/>
											</svg>
										</icon>
									</div>
									<div class="dragToRotateText">Drag to rotate</div>
									<div class="arrowRightContainer">
										<icon class="arrowRight">
											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
												<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
												L-321,348.1z"/>
											</svg>
										</icon>
									</div>
								</div>
							</div>
							<div id="output"></div>

						</div>

					</div>
					<div data-scale="1.4" data-image="" class="media_enlarged media_enlarged_image media_enlarged_1 media_enlarged_active">
						<div class="mediaWrapper">
							<div class="media" style="background-image:url(<?=UPLOAD_URL.'gift_cards/'.$model->gift_obj->image?>)"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col productDataActions">
				<span class="fabric_img fabric_left"></span>
					<a class="btn btn_black addGiftCard firstAddBagBtn" id="add" data-gift_card_id="<?=$model->gift_obj->id?>" data-user_id="<?=$model->user?$model->user->id:0?>">Add to Bag</a>
				<div class="add_to_bag_wrapper">
					<div class="productShareRow">
						<h6 class="as_r">Share</h6>
						<ul>
							<li>
			                    <div>
			                        <a href="https://www.facebook.com/ModernVice" class="facebook footer_social_icon" style="background-image:url(<?=FRONT_IMG?>facebookCircleBtn.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://twitter.com/modernvice" class="twitter footer_social_icon" style="background-image:url(<?=FRONT_IMG?>twitterCircleBtn.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://www.instagram.com/modernvice/" class="instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>instagramCircleBtn.png)"></a>
			                    </div>
			                </li>
			                <li>
			                    <div>
			                        <a href="https://www.instagram.com/modernvice/" class="pinterest instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>pinterestCircleBtn.png)"></a>
			                    </div>
			                </li>
						</ul>
					</div>
				</div>
				<div class="help_options_wrapper">
					<p class="as_l">For style or product advice, please contact our Shoe Design Advisors at <a class="tel black_thin_btn" href="tel:2127771851">212 777 1851</a> or start a <a id="triggerZopimChat">live chat</a> with a member of our design team right away</p>
				</div>
<!--				<div>-->
<!--					<h5 class="as_m">Reviews - --><?//=$model->averageRating?><!--</h5>-->
<!--					--><?//if($model->reviews){
//						foreach($model->reviews as $review){?>
<!--								<div>-->
<!--							<p class="as_l">--><?//=$review->rating?><!--</p>-->
<!--							<p class="as_l"><b>--><?//=$review->title?><!--</b> - <span class="review-helpful">--><?//=$review->helpful.' out of '.($review->helpful+$review->unhelpful).' found this helpful'?><!--</span></p>-->
<!--							<p class="as_l">--><?//=$review->body?><!-- - "--><?//=$review->name?><!--"</p>-->
<!--							<div class="upvote" style="cursor: pointer;" data-review="--><?//=$review->id?><!--">Upvote</div><div class="downvote" style="cursor: pointer;" data-review="--><?//=$review->id?><!--">Downvote</div>-->
<!--							<hr>-->
<!--								</div>-->
<!--					--><?//}}else{?>
<!--						<p class="as_l">There doesn't appear to be anything here. Add you own review!</p>-->
<!--					--><?//}?>
<!--					<rating>Rating</rating><select name="rating"><option value="5">5</option><option value="4">4</option><option value="3">3</option><option value="2">2</option><option value="1">1</option></select><br/>-->
<!--					<label>Name</label><input name="name"/><br/>-->
<!--					<label>Email</label><input name="email"/><br/>-->
<!--					<label>Title</label><input name="title"/><br/>-->
<!--					<label>Body</label><input name="body"/><br/>-->
<!--					<div id="submit-review">Submit a review</div>-->
<!--				</div>-->
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
	})
</script>