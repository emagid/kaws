    <div class="pageWrapper subcategoryPageWrapper pageLeftNavSidebar">
        <div class="offersSliderWrapper">
            <div class="offersSwiper">
                <div class="swiper-wrapper">
<!--                    --><?//foreach($model->sale_banners as $sale_banner){?>
<!--                        <div class="swiper-slide">-->
<!--                            <p><span>--><?//=$sale_banner->title?><!--</span><data><em>--><?//=$sale_banner->subtitle?><!--</em></data></p>-->
<!--                        </div>-->
<!--                    --><?//}?>
                </div>
                <div class="offer-swiper-button-prev swiper-button-white"></div>
                <div class="offer-swiper-button-next swiper-button-white"></div>
            </div>
        </div>
<!--        --><?//$banner_text_color = $model->category != 'all' && $model->category->banner_text_color ? 'black': 'white'?>
<!--        <div class="subCategoryHero ">-->

<!--            --><?// if(count($model->new_arrivals) > 0){?>
<!--            <a class="show_mdl" data-mdl_name="newItems">-->
<!--            --><?//}else{?>
<!--            <div>-->
<!--            --><?//}?>
<!--                <div class="image_mask">-->
<!--                    --><?//if($model->category == 'all'){?>
    <!--                    <img class="media" src="--><?//=FRONT_IMG.'banner3.jpg'?><!--">-->
<!--                        <img class="media" src="--><?//=UPLOAD_URL.'categories/'.$model->mCategory->banner?><!--">-->
<!--                    --><?//} else {?>
<!--                        <img class="media" src="">-->
<!--                    --><?//}?>
<!--                </div>-->

<!--                <div class="hero_description_wrapper ">-->
<!--                    <div class="abs_vert_center">-->
<!--                        --><?//if(!$model->category->hide_banner_text){?>
<!--                        --><?// if(count($model->new_arrivals) > 0){?>
<!--                        <h1 class="as_l"><span class="subcategory_name cortado">--><?//=$model->category == 'all'?'':$model->category->name?><!--</span></h1>-->
<!--                        <div class="btn btn_white btn_large ghost_btn">Shop New Arrivals</div>-->
<!--                        --><?//}else{?>
<!--                        <h1 class="as_l"><span class="subcategory_name cortado">--><?//='name'?><!--</span></h1>-->
<!--                        --><?//}?>
<!--                        --><?//}?>

<!--                    </div>-->
<!--                </div>-->
<!--            --><?// if(count($model->new_arrivals) > 0){?>
<!--            </a>-->
<!--            --><?//}else{?>
<!--            </div>-->
<!--            --><?//}?>
<!--        </div>-->
        <div class="noGutter_content_width product_inventory_gridui mainPageInventoryUI">
            <div class="row row_of_4 product_grid_ui">
                <?foreach($model->gift_objs as $obj){
//                    $prodAttr = [];
//                    $pa = \Model\Product_Attributes::getStatusById($product->id,$product->default_color_id);
//                    if ($product->getAvailability() != 'Active' || ($pa != null && $pa->value != 1)) {
//                        $soldOut = true;
//                    } else {
//                        $soldOut = false;
//                    }
//                    foreach(json_decode($product->color,true) as $value){
//                        $st = \Model\Product_Attributes::getStatusById($product->id,$value);
//                        if($st == null || $st->value != 3){
//                            $c = \Model\Color::getItem($value);
//                            if($c){
//                                $prodAttr[] = $c;
//                            }
//                        }
//                    }
//                    $prodAttr = \Model\Product_Attributes::getColorList($product->id,0);
//                ?>
                <div class="col productGridItem">
<!--                    <div id="showQuickView" class="show_mdl" data-id="" data-mdl_name="quickView">-->
<!--                        <h5 class="as_m"><span><i></i><i></i></span>Quick View</h5>-->
<!--                    </div>-->

<!--                    --><?// if(count($prodAttr)>=5){?>
<!--                    <div class="productGridItemSwatches productGridItemSwatchesSlider">-->
<!--                    --><?//}else{?>
<!--                    <div class="productGridItemSwatches">-->
<!--                    --><?//}?>
<!--                        --><?//foreach($prodAttr as $pa){?>
<!--                            <div class="productGridItemSwatchWrapper">-->
<!--                                <img src="--><?//=$pa->swatch()?><!--" class="productGridItemSwatch" data-color_id="--><?//=$pa->id?><!--" data-pro_id="--><?//=$product->id?><!--"/>-->
<!--                            </div>-->
<!--                        --><?//}?>
<!--                    </div>-->
                    <a href="/gift_cards/product/<?=$obj->id?>">
                        <div class="saleInfoWrapper">
                            <small class="saleIndicator">Gift Card</small>
                            <!--                            --><?//=($product->isDiscounted()||($product->price)>$product->basePrice()) ? '<small class="saleIndicator">On Sale</small>': ''?>
<!--                            --><?//if(\Model\Event::eventSale($product->id,$product->default_color_id)){
//                                $eventAttr = \Model\Product_Attributes::getEventById($product->id,$product->default_color_id);
//                                $event = \Model\Event::getItem($eventAttr->value)?>
<!--                            --><?//}?>
                        </div>
                        <div class="mediaWrapper">
<!--                            --><?//$featuredImageParam = $model->category == 'all' ? null: $model->category->id?>
                            <div class="media" style="background-image:url(<?=UPLOAD_URL.'gift_cards/'.$obj->image?>)"></div>
                        </div>
                        <div class="dataWrapper">
                            <h4 class="product_name"><?=$obj->name?></h4>

<!--                            --><?//if($product->msrp > 0.0){
//                                $percent = round(($product->basePrice() - $product->msrp()) * 100 / $product->basePrice())?>
<!--                                <h4 class="product_price">-->
<!--                                    <span class="full_price">-->
<!--                                        <span class="currency">$</span>--><?//=number_format($product->basePrice(),2)?>
<!--                                    </span>-->
<!--                                    <span class="markdown">---><?//=$percent?><!--%</span>-->
<!--                                    <span class="value">$--><?//=number_format($product->msrp(),2)?><!--</span>-->
<!--                                </h4>-->
<!--                            --><?// } else if($product->isDiscounted()){?>
<!--                                --><?//$percent = round(($product->basePrice() - $product->price()) * 100 / $product->basePrice()) ?>
<!--                                <h4 class="product_price">-->
<!--                                    <span class="full_price">-->
<!--                                        <span class="currency">$</span>--><?//= number_format($product->basePrice(),2) ?>
<!--                                    </span>-->
<!--                                    <span class="value">$--><?//= number_format($product->price(),2) ?><!--</span>-->
<!--                                </h4>-->
<!--                            --><?// } else if(($product->price)>$product->basePrice()){?>
<!--                                --><?//$percent = round(($product->price - $product->price()) * 100 / $product->price) ?>
<!--                                <h4 class="product_price">-->
<!--                                    <span class="full_price">-->
<!--                                        <span class="currency">$</span>--><?//= number_format($product->price,2) ?>
<!--                                    </span>-->
<!--                                    <span class="value">$--><?//= number_format($product->price(),2) ?><!--</span>-->
<!--                                </h4>-->
<!--                            --><?//} else {?>
                                <h4 class="product_price"><span class="value">$<?=number_format($obj->amount,2)?></span></h4>
<!--                            --><?//}?>
                        </div>
<!--                        <div class="soldOutSticker">-->
<!--                            <p>Sold out</p>-->
<!--                        </div>-->
                    </a>
                </div>
                <?}?>
            </div>

<!--            <div id="loading-spinner">-->
<!--                <div class="hanger"></div>-->
<!--                <div class="discoball">-->
<!--                    <img src="--><?//=FRONT_IMG?><!--disco_ball.png" alt="">-->
<!--                </div>-->
<!--            </div>-->
        </div>

    </div>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>subcategory.css">