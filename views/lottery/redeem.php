<link rel = "stylesheet" type = "text/css" href="<?=auto_version(FRONT_CSS."checkoutFlow.css")?>">
<? $entrant = $model->entrant ?>
<? $price = $model->product->getPrice(null,null,null,null);?>
<div class="pageWrapper checkoutPageWrapper cartPageWrapper shippingCheckoutPageWrapper emailCheckoutPageWrapper shippingCheckoutPageWrapper">
  <div class="row content_width_1000">
    <div class="divider">
      <div class="emailFormWrapperCol cartWrapperCol col">
        <h1>CART</h1>
        <p class="status">Order Summary</p>
        <section id="cart">
          <div class="labelDataBox orderSummaryDataBox">
            <ul class="paymentScreenBagItems">
              <li>
                <div class="col bagItemImg">
                  <div class="col item"><img src="<?=$model->product->featuredImage()?>"/></div>
                  <div class="col item_info">
                    <div class="item_name">
                      <div class="name"><?=$model->product->name?></div>
                    </div>
                    <div class="item_price">$<?=number_format($price,2)?></div>
                  </div>
                </div>
              </li>
            </ul>
            <div class="final_price">
              <p class="price_bag"><b>Subtotal</b>$<?=number_format($price,2)?></p>
              <p class="price_bag"><b>Shipping</b>$<?=number_format($model->shipping,2)?></p>
              <p class="price_bag"><b>Taxes</b>$<?=number_format($model->tax,2)?></p>
              <div class="orderSummaryTotal">
                  <? $total = $price+$model->tax+$model->shipping ?>
                <p><b>Total</b>$<?=number_format($total,2)?></p>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div id="forms" class="emailFormWrapperCol shippingFormWrapperCol col">
      <div class="divider shipping">
        <h1>Billing Address</h1>
        <h8 class="ship_statement">Your order will be shipped to your billing address</h8>
        <div class="emailFormWrapperCol shippingFormWrapperCol col">
          <form id="email_form">
            <input id="checkoutEmail" placeholder="Email" type="email" maxlength="60" tabindex="1" value="<?=$entrant->email?>" disabled class="checkOutInput"/>
            <input type="hidden" name="onepage" value="1"/>
          </form>
          <h8 class="form_statement">Please submit a valid email address so that we can keep you informed on the status of your order.</h8>
          <form id="delivery_form" autocomplete="off">
            <input type="hidden" name="onepage" value="1" autocomplete="false"/>
            <input type="hidden" name="shipping_is_billing" value="on"/>
            <div class="form50">
              <input id="checkoutFName" value="<?=$entrant->first_name?>" type="text" maxlength="60" tabindex="1" disabled class="checkOutInput"/>
            </div>
            <div class="form50">
              <input id="checkoutLName" value="<?=$entrant->last_name?>" type="text" maxlength="60" tabindex="2" disabled class="checkOutInput"/>
            </div>
            <div class="form50">
              <input id="checkoutAddress1" value="<?=$entrant->address?>" type="text" maxlength="60" tabindex="4" disabled class="checkOutInput"/>
            </div>
            <div class="form50">
              <input id="checkoutAddress2" value="<?=$entrant->address2?>" type="text" maxlength="60" tabindex="4" disabled class="checkOutInput"/>
            </div>
            <div class="form50">
              <input id="checkoutCity" value="<?=$entrant->city?>" type="text" maxlength="60" tabindex="6" disabled class="checkOutInput"/>
            </div>
            <div class="form50">
              <input id="checkoutCountry" value="United States" type="text" maxlength="60" tabindex="6" disabled class="checkOutInput"/>
            </div>
            <div class="form50">
              <input id="checkoutState" value="<?=$entrant->state?>" type="text" maxlength="60" tabindex="6" disabled class="checkOutInput"/>
            </div>
            <div class="form50">
              <input id="checkoutZip" value="<?=$entrant->zip?>" type="text" maxlength="60" tabindex="6" disabled class="checkOutInput"/>
            </div>
            <div class="form50">
              <input id="checkoutPhone" value="<?=$entrant->phone?>" type="text" maxlength="60" tabindex="6" disabled class="checkOutInput"/>
            </div>
            <h8 class="phone_statement ship_statement">Provide a valid phone for UPS to contact you.</h8>
          </form>
        </div>
      </div>
      <div class="emailFormWrapperCol shippingFormWrapperCol paymentFormWrapperCol col">
        <div class="divider">
          <form id="bill" method="post" novalidate=""><h1>Billing & Payment</h1>
            <div class="creditCardPaymentFormSection">
              <div style="width: 97%">
                <!-- <p for="cardUserName">Name on Card</p>-->
                <input id="cardUserName" placeholder="Name on Card" value="<?=$entrant->first_name.' '.$entrant->last_name?>" type="text" name="card_name" autocomplete="off" maxlength="60" required="" class="card_name"/>
                <input name="ref_num" type="hidden" value="<?=$entrant->ref_num?>"/>
                <input name="entry_id" type="hidden" value="<?=$entrant->id?>"/>
              </div>
              <div style="width: 97%">
                <!-- <p for="ccnumber">Credit Card Number</p>-->
                <div class="cardNumberInputWrapper">
                  <input id="ccnumber" placeholder="Credit Card Number" type="text" pattern="[0-9]*" name="cc_number" maxlength="16" required="" class="card_num"/>
                  <div id="creditCardIcons">
                    <div class="media"></div>
                  </div>
                </div>
              </div>
              <div id="no_margin" class="form50">
                <input id="cc_type" type="hidden" name="cc_type"/>
                <!-- <p for="checkoutPhone">Expires on</p>-->
                <fieldset class="selectUIWrapper selectExpMonth">
                  <select style="margin-top: 8px;" name="cc_expiration_month" required="">
                      <?foreach(get_month() as $value=>$item){?>
                          <option value="<?=$value?>"><?=$item?></option>
                      <?}?>
                  </select><span class="downArrowICon">
                    <svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                      <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4												L-321,348.1z"></path>
                    </svg></span>
                </fieldset>
              </div>
              <div class="form50">
                <fieldset class="selectUIWrapper selectExpYear">
                  <select style="margin-top: 8px;" name="cc_expiration_year" required="">
                      <?$carbon = \Carbon\Carbon::now()->year;
                      for($i = $carbon; $i < $carbon+14; $i++){?>
                          <option value="<?=$i?>"><?=$i?></option>
                      <?}?>
                  </select><span class="downArrowICon">
                    <svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
                      <path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4												L-321,348.1z"></path>
                    </svg></span>
                </fieldset>
              </div>
              <div style="width: 50%; position: relative; max-width: 200px;">
                <p for="secCode" style="max-width: 200px; position: relative; width: 100%;" class="secode">
                  <div style="display:inline-block" class="cvv_help_box">
                    <div class="trigger"><span class="as_m">?</span></div>
                    <div class="ccv_help_box">
                      <p></p>For security reasons, please enter your card verification number (CVV) or card identification number (CID). Find this three-digit number on the back of your Visa, MasterCard and Discover cards in the signature area, or the four-digit number on the front of your American Express card, above the credit card number.<span class="ccv_tip_icons">
                        <svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewbox="384.5 10.7 201.9 58.3" enable-background="new 384.5 10.7 201.9 58.3" xml:space="preserve">
                          <g id="cvv_1_">
                            <g>
                              <path fill="#C3C3C3" d="M468.3,59.9c0,3.7-3,6.5-6.8,6.5h-67.9c-3.8,0-6.8-2.9-6.8-6.5V20.6c0-3.7,3-6.5,6.8-6.5h67.9																c3.8,0,6.8,2.9,6.8,6.5L468.3,59.9C468.3,59.9,468.3,59.9,468.3,59.9z"></path>
                              <rect x="386.8" y="22.3" fill="#222222" width="81.5" height="10.5"></rect>
                              <rect x="393.2" y="38" fill="#FFFFFF" width="48.9" height="10.5"></rect>
                              <rect x="397.1" y="41.9" fill="#222222" width="40.9" height="2.6"></rect>
                              <rect x="449.8" y="38" fill="#FFFFFF" width="11.3" height="10.5"></rect>
                              <rect x="451.1" y="41.9" fill="#222222" width="8.4" height="2.6"></rect>
                              <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="455.4" cy="43.2" rx="10.6" ry="10.3"></ellipse>
                            </g>
                          </g>
                        </svg>
                        <g id="cvv-amex_1_"></g>
                        <g id="cvv-amex"></g>
                        <g></g>
                        <g></g>
                        <path fill="#5EC1EC" d="M586.4,59.7c0,3.8-3.1,6.7-7,6.7h-69.7c-3.9,0-7-3-7-6.7V19.4c0-3.8,3.1-6.7,7-6.7h69.7c3.9,0,7,3,7,6.7													V59.7z"></path>
                        <g></g>
                        <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5													c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
                        <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2													v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
                        <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
                        <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z													"></path>
                        <g></g>
                        <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5													c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
                        <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2													v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
                        <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
                        <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z													"></path>
                        <rect x="507.5" y="43.9" display="none" fill="#FFFFFF" width="69.7" height="10.6"></rect>
                        <rect x="513.4" y="47.9" fill="#222222" width="56.1" height="2.7"></rect>
                        <rect x="565.3" y="31.4" fill="#FFFFFF" width="11.1" height="10.7"></rect>
                        <rect x="566.7" y="35.4" fill="#222222" width="8.4" height="2.7"></rect>
                        <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="570.8" cy="36.8" rx="10.9" ry="10.5"></ellipse></span>
                    </div>
                  </div>
                </p>
                <input id="secCode" style="max-width: 200px;" type="text" name="cc_ccv" placeholder="Security Code" maxlength="4" tabindex="4" required="" class="card_cvv"/>
              </div>
              <div>
                <p for="agree_to_terms" style="max-width: 240px; position: relative; width: 100%;" class="form_statement">You must agree to the <a onclick="$('#terms').click()">Terms of Service</a> to checkout.</p>
                <input id="agreeTerms" type="checkbox" name="agree_to_terms" style="height: 20px; position: relative; top: 5px;"/>
              </div>
              <div class="row secondaryFocus">
                <div class="right_float total_costs">
                  <input id="creditCardPayBtn" type="submit" value="Place Order" disabled="" class="button"/>
                  <div class="total_cost orderSummaryTotal">
                    <p style="font-size: 16px;"> <b>Total</b></p>$<?=number_format($total,2)?>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    /*
     * CREDIT CARD STYLES
     */
    $('input[name=cc_number]').on('input',function(){
        $('#cc_type').val(creditCardType($(this).val())[0].type);
    });
    window.addEventListener('cardChange',function(c){
        card = c.detail;
        if(card != ''){
            $('#creditCardIcons > div.media').empty().append($('<img src="<?=FRONT_ASSETS?>img/'+card+'_icon.png" />'));
        } else {
            $('#creditCardIcons > div.media').empty();
        }
    });
    $('#ccnumber').on('keyup',function(){
        var ccnumber = $(this).val();
        ccnumber = ccnumber.replace(/[^0-9-]/g,'');
        $(this).val(ccnumber);
    });
    $('#agreeTerms').change(function(){
        if($(this).is(':checked')){
            $('#creditCardPayBtn').attr('disabled',false);
        } else {
            $('#creditCardPayBtn').attr('disabled',true);
        }
    });
});
</script>