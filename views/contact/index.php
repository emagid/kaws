<div class="pageWrapper contactPageWrapper">
	<div id="map">
		<div id="127025943" style="width: 100% ; height: 400px; -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    filter: grayscale(100%);">

		</div>
		<div class="map-info">
			<div class="row">
				<div class="right" style="float: right">
					<div class="map_inner">
						<p>1666 Broadway, 10019, Manhattan</p>
						<p>+1 212.333.3990</p>
						<p>Mon-Sun 9 a.m. – 8 p.m.</p>
                        <br>
 						<p><strong>We also have a second location at</strong><br />
							Westfield Garden State Plaza</p> 
                        <p>+1 201.605.6600</p>
                        <p>Mon-Sat 10 a.m. – 9:30 p.m.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<form method="post" action="<?=$this->emagid->uri?>">
		<div class="modal_formWrapper contact">
			<h4 class="gothamLight">Send us an email</h4>
			<div class="inputRow">
				<input name="name" type="text" required/>
				<label>Name</label>
			</div>
			<div class="inputRow">
				<input name="email" type="email" required/>
				<label>Email Address</label>
			</div>
			<div class="inputRow">
				<input name="subject" type="text"/>
				<label>Subject</label>
			</div>
			<div class="inputRow">
<!--				<label>Message</label>-->
				<textarea name="message" name="message" placeholder="Message" required></textarea>
			</div>
			<button type="submit">Send</button>
		</div>
	</div>
</form>

</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC50nXUDEDlHvPydohQUedHg6xexRYTokc&sensor=false"></script>
<script type="text/javascript">

	function initialize() {
		var styles = {
			'flatsome':  [{
				"featureType": "administrative",
				"stylers": [
					{ "visibility": "on" }
				]
			},
				{
					"featureType": "road",
					"stylers": [
						{ "visibility": "on" },
						{ "hue": "#4d4d4d" }
					]
				},
				{
					"stylers": [
						{ "visibility": "on" },
						{ "hue": "#4d4d4d" },
						{ "saturation": -30 }
					]
				}
			]};

		var myLatlng = new google.maps.LatLng(40.762253, -73.983182);
		var myOptions = {
			zoom: 17,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			mapTypeId: 'flatsome',
			draggable: true,
			zoomControl: false,
			panControl: false,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false,
			scrollwheel: false,
			disableDoubleClickZoom: true
		}
		var map = new google.maps.Map(document.getElementById("127025943"), myOptions);
		var styledMapType = new google.maps.StyledMapType(styles['flatsome'], {name: 'flatsome'});
		map.mapTypes.set('flatsome', styledMapType);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title:""
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
	google.maps.event.addDomListener(window, 'resize', initialize);

</script>