<div class="pageWrapper productPageWrapper">

    <section class="productDetailMainHero accessoryDetailMainHero">

        <div class="heroMedia accessoriesHero">
        </div>
        <div class="absTransCenter">
            <h1 class="dji-text"><?=$model->product->name?></h1>
                <h4 class="gothamLight"><?=$model->product->description?></h4>
        </div>
    </section>
    <div class="productDetails_contentWrapper">
        <div class="stickyElementsWrapper mainContentWidth">
            <div class="productDetailsSubHeader mainLinksListWrapper">
                <ul class="productDetailsSubHeader_Links mainContentWidth">
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="1" class="activePageSection">
                            <p>Overview</p>
                        </a>
                    </li>
                    <?if($model->product->product_specs){?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="2">
                            <p>Specs</p>
                        </a>
                    </li>
                    <?}?>
                    <?if($model->product->getAllProductVideos()){?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="3">
                            <p>Videos</p>
                        </a>
                    </li>
                    <?}?>
                    <?if(isset($model->feature_1)&&$model->feature_1){?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="4">
                            <p>Feature 1</p>
                        </a>
                    </li>
                    <?}
                    if(isset($model->feature_2)&&$model->feature_2){
                    ?>
                    <li>
                        <a class="scrollTrigger" data-vertical_screen_nav="5">
                            <p>Feature 2</p>
                        </a>
                    </li>
                    <?}?>
                </ul>
            </div>
            <div class="productDetailsActionBox">
                <div class="priceBar row">
                    <div class="left_float">
                        <h6 class="gothamLight">$<?=number_format($model->product->basePrice(),2)?></h6>
                    </div>
                    <div class="right_float">
                        <p>Tax included</p>
                        <p>Free shipping</p>
                    </div>
                </div>
                <div class="variations radioOptionsModule">
                    <div class="checkboxUX optionBoxRadio selected">
                        <input type="checkbox" checked/>
                        <div class="content">
                            <div class="left_float">
                                <img src="<?=$model->product->featuredImage()?>"/>
                            </div>
                            <div>
                                <p class="name"><?=$model->product->name?></p>
                                <p class="price">USD $<?=number_format($model->product->basePrice(),2)?></p>
                            </div>
                        </div>
                    </div>
<!--                    <div class="checkboxUX optionBoxRadio">-->
<!--                        <input type="checkbox"/>-->
<!--                        <div class="content">-->
<!--                            <div class="left_float">-->
<!--                                <img src="--><?//=FRONT_IMG?><!--productOption2.png"/>-->
<!--                            </div>-->
<!--                            <div>-->
<!--                                <p class="name">Accessory Variation 2</p>-->
<!--                                <p class="price">USD $149.99</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
                <div class="productSecondaryActions row">
                    <div class="col left_float quantity numbersRow">
                        <input type="text" disabled name="product-quantity" id="product-quantity" value="1">
                        <div class="quantToggleButtons">
                            <div class="inc button">
								<span>
								</span>
                                <span>
								</span>
                            </div>
                            <div class="dec button disabled">
								<span>
								</span>
                            </div>
                        </div>
                    </div>
                    <div class="col right_float mainActions">
                        <a class="solidBlueBtn trueBlue addToCart accessProd" data-product_id="<?=$model->product->id?>" >
                            <p>Add to Cart</p>
                        </a>
                    </div>

                        <a class="solidBlueBtn trueBlue mainProd addToFavorites">
                            <p>Add to Favorites</p>
                        </a>
                        <div class="twitterShareButton">

                        <a href="https://twitter.com/intent/tweet?screen_name=djiglobal" class="twitter-mention-button" data-show-count="false">Tweet to @djiglobal</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                        <div class="fb-share-button" data-href="https://www.facebook.com/DJI" data-layout="button_count" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.facebook.com%2FDJI&amp;src=sdkpreparse">Share</a></div>
                        <div class="pinterestShareButton">
                            <a data-pin-do="buttonBookmark" data-pin-save="true" href="https://www.pinterest.com/pin/create/button/"></a>
                        <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
                        </div>
                </div>
            </div>
        </div>
        <div class="productDetailSection mainProductInfo" data-vertical_screen="1">
            <div class="mainContentWidth">
                <?if(count($model->product->getAllProductImages()) != 0){?>
                <div class="productMainImagesWrapper">
                    <div class="swiperContainer-mainProductImages">
                        <div class="swiper-wrapper">
                            <?php foreach($model->product->getAllProductImages() as $image){ ?>
                                <div class="swiper-slide image-slide">
                                    <div class="media" style="background-image:url('<?="/content/uploads/products/$image"?>')"></div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="swiper-button-prev" style="background-image:url('<?=FRONT_IMG?>productDetailImg_sliderArrow_left.png')"></div>
                        <div class="swiper-button-next" style="background-image:url('<?=FRONT_IMG?>productDetailImg_sliderArrow_right.png')"></div>
                    </div>
                </div>
                <?}?>
                <div class="productMainDetailsWrapper">
                    <p class="mainDetailsParagraph"><?=$model->product->details?></p>
                </div>
            </div>
        </div>
        <?if($model->product->product_specs){?>
        <div class="productDetailSection productDetailSpecs" data-vertical_screen="2">
            <div class="mainContentWidth">
                <h4 class="phantomLight"><span><?=$model->product->name?></span> Specs</h4>

                <div class="collapsedSection">
                    <div class="row row_of_2 row_specs_stacked">
                        <? $arr = array_chunk($model->product_specs,2,true);
                        foreach ($arr as $spec){?>
                            <div class="col">
                                <? foreach ($spec as $key=>$value){
                                    $value = explode('|',$value);?>
                                    <div class="specsCategory">
                                        <h6 class="gothamLight"><?=$key?></h6>
                                        <ul>
                                            <?foreach ($value as $row){
                                                $row = explode('^',$row);
                                                ?>
                                                <li>
                                                    <label><?=$row[0]?></label>
                                                    <p><?=implode('<br>',explode(';',$row[1])) ?></p>
                                                </li>
                                            <?}?>
                                        </ul>
                                    </div>
                                <?}?>
                            </div>
                        <?}?>
                    </div>
                    <a class="expandParent">
                        <p>View All Specs</p>
                    </a>
                </div>
            </div>
        </div>
        <?}?>
        <!--<div class="productDetailSection videosSection" data-vertical_screen="3">
            <h6 class="gothamLight">All Videos</h6>
            <div class="swiperContainer-allVideos">
                <div class="swiper-wrapper">
                    <div class="swiper-slide video-slide">
                        <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                            <div class="videoThumb media" style="background-image:url('<?/*=FRONT_IMG*/?>videoExample1.jpg')"></div>
                            <icon class="djiicon djiIconPlay" style="background-image:url('<?/*=FRONT_IMG*/?>playIcon.png')"></icon>
                        </a>
                    </div>
                    <div class="swiper-slide video-slide">
                        <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                            <div class="videoThumb media" style="background-image:url('<?/*=FRONT_IMG*/?>videoExample2.jpg')"></div>
                            <icon class="djiicon djiIconPlay" style="background-image:url('<?/*=FRONT_IMG*/?>playIcon.png')"></icon>
                        </a>
                    </div>
                    <div class="swiper-slide video-slide">
                        <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                            <div class="videoThumb media" style="background-image:url('<?/*=FRONT_IMG*/?>videoExample3.jpg')"></div>
                            <icon class="djiicon djiIconPlay" style="background-image:url('<?/*=FRONT_IMG*/?>playIcon.png')"></icon>
                        </a>
                    </div>
                    <div class="swiper-slide video-slide">
                        <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                            <div class="videoThumb media" style="background-image:url('<?/*=FRONT_IMG*/?>videoExample4.jpg')"></div>
                            <icon class="djiicon djiIconPlay" style="background-image:url('<?/*=FRONT_IMG*/?>playIcon.png')"></icon>
                        </a>
                    </div>
                    <div class="swiper-slide video-slide">
                        <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                            <div class="videoThumb media" style="background-image:url('<?/*=FRONT_IMG*/?>videoExample5.jpg')"></div>
                            <icon class="djiicon djiIconPlay" style="background-image:url('<?/*=FRONT_IMG*/?>playIcon.png')"></icon>
                        </a>
                    </div>
                    <div class="swiper-slide video-slide">
                        <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                            <div class="videoThumb media" style="background-image:url('<?/*=FRONT_IMG*/?>videoExample6.jpg')"></div>
                            <icon class="djiicon djiIconPlay" style="background-image:url('<?/*=FRONT_IMG*/?>playIcon.png')"></icon>
                        </a>
                    </div>
                    <div class="swiper-slide video-slide">
                        <a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
                            <div class="videoThumb media" style="background-image:url('<?/*=FRONT_IMG*/?>videoExample7.jpg')"></div>
                            <icon class="djiicon djiIconPlay" style="background-image:url('<?/*=FRONT_IMG*/?>playIcon.png')"></icon>
                        </a>
                    </div>
                </div>
                <div class="swiper-button-prev" style="background-image:url('<?/*=FRONT_IMG*/?>allVideosSlider_left.png')"></div>
                <div class="swiper-button-next" style="background-image:url('<?/*=FRONT_IMG*/?>allVideosSlider_right.png')"></div>
            </div>
        </div>-->
        <?if(isset($model->feature_1)&&$model->feature_1){?>
        <div class="productDetailSection productFeatureCenter grey_bg" data-vertical_screen="4">
            <div class="media productFeatureMedia" style="background-image:url('<?=UPLOAD_URL.'products/'.$model->product->feature_image_1?>')"></div>
            <?foreach ($model->feature_1 as $value){?>
                <h6 class="gothamLight"><?=$value['feature_title']?></h6>
                <p><?=$value['feature_description']?></p>
            <?}?>
        </div>
        <?}
        if(isset($model->feature_2)&&$model->feature_2){?>
        <div class="productDetailSection productFeatureCenter white_bg imageTopPaddingFeature" data-vertical_screen="5">
            <div class="media productFeatureMedia" style="background-image:url('<?=UPLOAD_URL.'products/'.$model->product->feature_image_2?>')"></div>
            <?foreach ($model->feature_2 as $value){?>
                <h6 class="gothamLight"><?=$value['feature_title']?></h6>
                <p><?=$value['feature_description']?></p>
            <?}?>
        </div>
        <?}?>
        <!--<div class="secondaryProductDetailHero" style="background-image:url('<?/*=FRONT_IMG*/?>productDetailsFeatures_hero1.jpg')"></div>-->
    </div>
</div>