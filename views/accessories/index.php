<div class="pageWrapper productPageWrapper">

	<section class="productDetailMainHero accessoryDetailMainHero">

		<div class="heroMedia accessoriesHero">
		</div>
		<div class="absTransCenter">
			<h1 class="dji-text">$Accessory Name</h1>
			<h4 class="gothamLight">$Accessory Summarization lorem ipsum dolor et cetera</h4>
		</div>
	</section>
	<div class="productDetails_contentWrapper">
		<div class="stickyElementsWrapper mainContentWidth">
			<div class="productDetailsSubHeader mainLinksListWrapper">
				<ul class="productDetailsSubHeader_Links mainContentWidth">
					<li>
						<a class="scrollTrigger" data-vertical_screen_nav="1" class="activePageSection">
							<p>Overview</p>
						</a>
					</li>
					<li>
						<a class="scrollTrigger" data-vertical_screen_nav="2">
							<p>Specs</p>
						</a>
					</li>
					<li>
						<a class="scrollTrigger" data-vertical_screen_nav="3">
							<p>Videos</p>
						</a>
					</li>
					<li>
						<a class="scrollTrigger" data-vertical_screen_nav="4">
							<p>Feature 1</p>
						</a>
					</li>
					<li>
						<a class="scrollTrigger" data-vertical_screen_nav="5">
							<p>Feature 2</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="productDetailsActionBox">
				<div class="priceBar row">
					<div class="left_float">
						<h6 class="gothamLight">$129.99</h6>
					</div>
					<div class="right_float">
						<p>Tax included</p>
						<p>Free shipping</p>
					</div>
				</div>
				<div class="variations radioOptionsModule">
					<div class="checkboxUX optionBoxRadio selected">
						<input type="checkbox" checked/>
						<div class="content">
							<div class="left_float">
								<img src="<?=FRONT_IMG?>productOption1.png"/>
							</div>
							<div>
								<p class="name">Accessory Vartiation 1</p>
								<p class="price">USD $129.99</p>
							</div>
						</div>
					</div>
					<div class="checkboxUX optionBoxRadio">
						<input type="checkbox"/>
						<div class="content">
							<div class="left_float">
								<img src="<?=FRONT_IMG?>productOption2.png"/>
							</div>
							<div>
								<p class="name">Accessory Variation 2</p>
								<p class="price">USD $149.99</p>
							</div>
						</div>						
					</div>
				</div>
				<div class="productSecondaryActions row">
					<div class="col left_float quantity numbersRow">
						<input type="text" disabled name="product-quantity" id="product-quantity" value="1">
						<div class="quantToggleButtons">
							<div class="inc button">
								<span>
								</span>
								<span>
								</span>
							</div>
							<div class="dec button disabled">
								<span>
								</span>
							</div>
						</div>
					</div>
					<div class="col right_float mainActions">
						<a class="solidBlueBtn trueBlue">
							<p>Add to Cart</p>
						</a>
					</div>
				</div>
			</div>	
		</div>
		<div class="productDetailSection mainProductInfo" data-vertical_screen="1">
			<div class="mainContentWidth">
				<div class="productMainImagesWrapper">
					<div class="swiperContainer-mainProductImages">
						<div class="swiper-wrapper">
							<div class="swiper-slide image-slide">
								<div class="media" style="background-image:url('<?=FRONT_IMG?>productImage1.jpg')"></div>
							</div>
							<div class="swiper-slide image-slide">
								<div class="media" style="background-image:url('<?=FRONT_IMG?>productImage2.jpg')"></div>
							</div>
							<div class="swiper-slide image-slide">
								<div class="media" style="background-image:url('<?=FRONT_IMG?>productImage3.jpg')"></div>
							</div>
							<div class="swiper-slide image-slide">
								<div class="media" style="background-image:url('<?=FRONT_IMG?>productImage4.jpg')"></div>
							</div>
							<div class="swiper-slide image-slide">
								<div class="media" style="background-image:url('<?=FRONT_IMG?>productImage5.jpg')"></div>
							</div>
							<div class="swiper-slide image-slide">
								<div class="media" style="background-image:url('<?=FRONT_IMG?>productImage6.jpg')"></div>
							</div>
						</div>
			            <div class="swiper-button-prev" style="background-image:url('<?=FRONT_IMG?>productDetailImg_sliderArrow_left.png')"></div>
			            <div class="swiper-button-next" style="background-image:url('<?=FRONT_IMG?>productDetailImg_sliderArrow_right.png')"></div>
					</div>
				</div>
				<div class="productMainDetailsWrapper">
					<p class="mainDetailsParagraph"><b>An uprated camera</b> is equipped with a 1-inch 20-megapixel sensor capable of shooting 4K/60fps video and Burst Mode stills at 14 fps.  The adoption of titanium alloy and magnesium alloy construction increases the rigidity of the airframe and reduces weight, making the Phantom 4 Pro similar in weight to the Phantom 4. The Flight Autonomy system adds dual rear vision sensors and infrared sensing systems for a total of 5-direction of obstacle sensing and 4-direction of obstacle avoidance.</p>
				</div>
			</div>
		</div>
		<div class="productDetailSection productDetailSpecs" data-vertical_screen="2">
			<div class="mainContentWidth">
				<h4 class="phantomLight"><span>Phantom 4 Pro</span> Specs</h4>

				<div class="collapsedSection">
					<div class="row row_of_2 row_specs_stacked">
						<div class="col">
							<div class="specsCategory">
								<h6 class="gothamLight">Aircraft</h6>
								<ul>
									<li>
										<label>Weight (Battery &#x26; Propellers included)</label>
										<p>1388 g</p>
									</li>
									<li>
										<label>Diagonal Size</label>
										<p>350 mm</p>
									</li>
									<li>
										<label>Max Ascent Speed</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s</p>
									</li>
									<li>
										<label>Max Descent Speed</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>
									<li>
										<label>Max Speed</label>
										<p>350 mm</p>
									</li>
									<li>
										<label>Max Tilt Angle</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s</p>
									</li>
									<li>
										<label>Max Angular Speed</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Max Service Ceiling Above Sea Level</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Max Flight Time</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Operating Temperature Range</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>																								
									<li>
										<label>Max Service Ceiling Above Sea Level</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Max Flight Time</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Operating Temperature Range</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>									
								</ul>
							</div>
						</div>
						<div class="col">
							<div class="specsCategory">
								<h6 class="gothamLight">Gimbal</h6>
								<ul>
									<li>
										<label>Weight (Battery &#x26; Propellers included)</label>
										<p>1388 g</p>
									</li>
									<li>
										<label>Diagonal Size</label>
										<p>350 mm</p>
									</li>
									<li>
										<label>Max Ascent Speed</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s</p>
									</li>
									<li>
										<label>Max Descent Speed</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>
									<li>
										<label>Max Speed</label>
										<p>350 mm</p>
									</li>
									<li>
										<label>Max Tilt Angle</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s</p>
									</li>
									<li>
										<label>Max Angular Speed</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Max Service Ceiling Above Sea Level</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Max Flight Time</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Operating Temperature Range</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>																								
									<li>
										<label>Max Service Ceiling Above Sea Level</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Max Flight Time</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>	
									<li>
										<label>Operating Temperature Range</label>
										<p>S-mode: 6 m/s<br>P-mode: 3 m/s<br>P-mode: 31 mph</p>
									</li>									
								</ul>
							</div>						
						</div>
					</div>
					<a class="expandParent">
						<p>View All Specs</p>
					</a>
				</div>
			</div>
		</div>
		<div class="productDetailSection videosSection" data-vertical_screen="3">
			<h6 class="gothamLight">All Videos</h6>
			<div class="swiperContainer-allVideos">
				<div class="swiper-wrapper">
				 	<div class="swiper-slide video-slide">
				 		<a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
				 			<div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample1.jpg')"></div>
				 			<icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
				 		</a>
				 	</div>
				 	<div class="swiper-slide video-slide">
				 		<a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
				 			<div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample2.jpg')"></div>
				 			<icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
				 		</a>
				 	</div>
				 	<div class="swiper-slide video-slide">
				 		<a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
				 			<div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample3.jpg')"></div>
				 			<icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
				 		</a>
				 	</div>
				 	<div class="swiper-slide video-slide">
				 		<a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
				 			<div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample4.jpg')"></div>
				 			<icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
				 		</a>
				 	</div>
				 	<div class="swiper-slide video-slide">
				 		<a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
				 			<div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample5.jpg')"></div>
				 			<icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
				 		</a>
				 	</div>
				 	<div class="swiper-slide video-slide">
				 		<a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
				 			<div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample6.jpg')"></div>
				 			<icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
				 		</a>
				 	</div>
				 	<div class="swiper-slide video-slide">
				 		<a class="triggerModal" id="videoPlayerModal" data-videoUrl="">
				 			<div class="videoThumb media" style="background-image:url('<?=FRONT_IMG?>videoExample7.jpg')"></div>
				 			<icon class="djiicon djiIconPlay" style="background-image:url('<?=FRONT_IMG?>playIcon.png')"></icon>
				 		</a>
				 	</div>
				</div>
	            <div class="swiper-button-prev" style="background-image:url('<?=FRONT_IMG?>allVideosSlider_left.png')"></div>
	            <div class="swiper-button-next" style="background-image:url('<?=FRONT_IMG?>allVideosSlider_right.png')"></div>		
			</div>
		</div>	
		<div class="productDetailSection productFeatureCenter grey_bg" data-vertical_screen="4">
			<div class="media productFeatureMedia" style="background-image:url('<?=FRONT_IMG?>cameraMount.jpg')"></div>
			<h6 class="gothamLight">Camera with 1-inch 20mp sensor</h6>
			<p>The onboard camera has been redesigned to use a 1-inch 20-megapixel CMOS sensor. A custom engineered lens made up of eight elements is arranged in seven groups, it is the first DJI camera to use a mechanical shutter, eliminating rolling shutter distortion which can occur when taking images of fast moving subjects or when flying at high speed. In effect, it is as powerful as many traditional ground cameras. More powerful video processing supports H.264 4K videos at 60fps or H.265 4K at 30fps, both with a 100Mbps bitrate. Advanced sensors and processors ensure everything is captured with more image detail and the image data needed for advanced post-production.</p>
		</div>
		<div class="productDetailSection productFeatureCenter white_bg imageTopPaddingFeature" data-vertical_screen="6">
			<div class="media productFeatureMedia" style="background-image:url('<?=FRONT_IMG?>remoteControl.jpg')"></div>
			<h6 class="gothamLight">Remote Controller with Built-in Screen</h6>
			<p>A 5.5in 1080p screen integrated with the Phantom 4 Pro + offers 1000 cd/m2 of brightness, more than twice as bright as conventional smart devices. As the DJI GO app is built into the screen, hardware and software can be fully optimized, allowing you to edit and share instantly. A five-hour battery life makes the Phantom 4 Pro + a complete aerial imaging solution. The integrated upgraded Lightbridge HD video transmission system supports dual frequencies for greater interference resistance and a maximum video transmission range of 4.3mi (7km)*.</p>
		</div>
		<div class="secondaryProductDetailHero" style="background-image:url('<?=FRONT_IMG?>productDetailsFeatures_hero1.jpg')"></div>										
	</div>
</div>