<div class="pageWrapper faqPageWrapper cmsPageWrapper">
    <h1 class="dji-text dji-blue">FAQ</h1>
	<div class="questionsSeachWrapper">
		<input type="text" class="searchFaqField">
        <p class="placeholder"><icon style="background-image:url('<?=FRONT_IMG?>faq_searchIcon.png')"></icon><span>Search frequently asked questions</span></p> 
	</div>
    <div class="row row_of_3 faq_ui_wrapper">
        <h6 class="gothamBook big"><?=$model->faqs_privacy->name?></h6>
        <?foreach ($model->faqs_privacy->faqs as $faq){?>
            <div class="col">
                <div class="questionUI">
                    <h4 class="gothamLight"><?=$faq->question?></h4>
                    <p class="gothamLight"><?=$faq->answer?></p>
                </div>
            </div>
        <?}?>
    </div>
    <div class="row row_of_3 faq_ui_wrapper">
        <h6 class="gothamBook big"><?=$model->faqs_ship->name?></h6>
        <?foreach ($model->faqs_ship->faqs as $faq){?>
            <div class="col">
                <div class="questionUI">
                    <h4 class="gothamLight"><?=$faq->question?></h4>

                    <p class="gothamLight"><?=$faq->answer?></p>
                </div>
            </div>
        <?}?>
    </div>
</div>