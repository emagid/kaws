<?

function buildUrl($arr = []){
    $url = parse_url($_SERVER['REQUEST_URI'])['path'].'?'.http_build_query(array_merge($_GET,$arr));
    return $url;
}
?>
<div class="pageWrapper searchResultsPageWrapper">

	<div class="mainContentWidth">

		<div class="searchQueryFilterBar selfclear row">
			<div class="left_float">
				<h4 class="gothamLight">You searched for <span class="dji-text">"<?=$model->keywords?>"</span></h4>
			</div>
			<div class="right_float">
				<div class="dji_optionSelect_ui_wrapper">
					<select id="sortBy" >
                        <option value ="<?='/sr?keywords='.$_GET['keywords']?>" selected>Sort By</option>
                        <?foreach($this->filters->sortBy as $display=>$hidden){
                            $select = $_GET['sortBy']?>
                            <option value="<?=buildUrl(['sortBy'=>$hidden]); ?>" <?=($select == $hidden)?'selected':''?> >
                               <?=$display?>
                            </option>
                        <?}?>
					</select>
				</div>
			</div>
            <div class="right_float">
                <div class="dji_optionSelect_ui_wrapper">
                    <select id="filterBy" >
                        <?$select = $_GET['type']?>
                        <option value ="<?=buildUrl(['type'=>'main']); ?>" <?=($select == 'main')?'selected':''?>>Main</option>
                        <option value="<?=buildUrl(['type'=>'combo']); ?>" <?=($select == 'combo')?'selected':''?>>Combo</option>
                        <option value="<?=buildUrl(['type'=>'spare']); ?>" <?=($select == 'spare')?'selected':''?>>Spare</option>
                    </select>
                </div>
            </div>
		</div>
		<div class="searchResultsGrid">
            <?foreach ($model->products as $p){?>
                <div class="col">
                    <div class="comparisonToggleWrapper">
                        <a class="comparisonToggle">
                            <icon class="compareToggleIcon">
                                <span></span>
                                <span></span>
                            </icon>
                            <p>Compare</p>
                        </a>
                        <div class="comparisonLinkWrapper hidden">
                            <a>
                                <p>View Comparisons</p><span class="count">0</span>
                            </a>
                        </div>
                    </div>
                    <a class="gridProductContent" href="/products/<?=$p->slug?>">
                        <img src="<?=$p->featuredImage()?>"/>
                        <p class="name"><?=$p->name?></p>
                        <p class="price">USD $<?=number_format($p->basePrice(),2)?></p>
                    </a>
                </div>
            <?}?>

		</div>
	</div>

</div>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script>
    $(document).ready(function () {
        $('#sortBy').on('change',function(){
            var url = $(this).val();
            location.href = url;
        })
        $('#filterBy').on('change',function(){
            var url = $(this).val();
            location.href = url;
        })
    })
</script>