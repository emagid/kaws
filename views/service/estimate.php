<div class="pageWrapper servicePageWrapper">


    <div class="servicePageHero">
        <div class="media" style="background-image:url('<?=FRONT_IMG?>serviceRequestHero.jpg')"></div>
        <div class="mainContentWidth">
            <h2 class="dji-text">We received your request</h2>
            <h6 class="gothamLight"><?=$model->message?>
            </h6>
        </div>
    </div>
</div>