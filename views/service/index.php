<div class="pageWrapper servicePageWrapper">


	<div class="servicePageHero">
		<div class="media" style="background-image:url('<?=FRONT_IMG?>serviceRequestHero.jpg')"></div>
		<div class="mainContentWidth">
			<h2 class="dji-text">Welcome to <span><em>DJI</em> NYC by Camrise</span> Repair Services</h2>
			<h6 class="gothamLight">​
In DJI NYC we value good customer service and fast repairs. We provide high quality service in a given time period for your drones. This service includes a free inspection of your aircraft including a estimate being sent to your email, once you approve the repairs we will continue to repair them (firmware upgrade, calibration and test fly included). </h6>
		</div>
	</div>

	<div class="repair_process">
		<h1 class="dji-text">Repair Process</h1>

		<div class="repair_process_steps">
				<div class="solo_step">
					<div class="hexagon"><p><span class="digit">1</span></p></div>
					<div class="hexagon_big"><img src="<?=FRONT_IMG?>repair-forms.png"></div>
										<svg height="4" width="200">
					  <line x1="0" y1="0" x2="200" y2="0" style="stroke:rgb(236,236,236);stroke-width:4" />
					</svg>
					<div class="hexagon_small"></div>
					<div class="repair_step" id="indent">
						<p>​ Register as a new account. Your account will give you all the updates and information you need for your aircraft while it is in our center.</p>
					</div>
				</div>

				<div class="solo_step">
					<div class="hexagon"><p><span class="digit">2</span></p></div>
					<div class="hexagon_big"><img src="<?=FRONT_IMG?>package.png"></div>
										<svg height="4" width="200">
					  <line x1="0" y1="0" x2="200" y2="0" style="stroke:rgb(236,236,236);stroke-width:4" />
					</svg>
					<div class="hexagon_small"></div>
					<div class="repair_step">
						<p>​ Receive an email with the Service ID number and instructions how to ship your drone to us</p>
					</div>
				</div>
				<div class="solo_step">
					<div class="hexagon"><p><span class="digit">3</span></p></div>
					<div class="hexagon_big"><img src="<?=FRONT_IMG?>mail.png"></div>
										<svg height="4" width="200">
					  <line x1="0" y1="0" x2="200" y2="0" style="stroke:rgb(236,236,236);stroke-width:4" />
					</svg>
					<div class="hexagon_small"></div>
					<div class="repair_step" id="indent">
						<p>​ After receiving your drone an email will be sent stating the products we have received. </p>
					</div>
				</div>
				<div class="solo_step">
					<div class="hexagon"><p><span class="digit">4</span></p></div>
					<div class="hexagon_big"><img src="<?=FRONT_IMG?>service.png"></div>
										<svg height="4" width="200">
					  <line x1="0" y1="0" x2="200" y2="0" style="stroke:rgb(236,236,236);stroke-width:4" />
					</svg>
					<div class="hexagon_small"></div>
					<div class="repair_step">
						<p>​ Our service team will send you an estimate of cost via e-mail. You can also check service and cost information at your established online account.</p>
					</div>
				</div>
				<div class="solo_step">
					<div class="hexagon"><p><span class="digit">5</span></p></div>
					<div class="hexagon_big"><img src="<?=FRONT_IMG?>repair.png"></div>
					<svg height="4" width="200">
					  <line x1="0" y1="0" x2="200" y2="0" style="stroke:rgb(236,236,236);stroke-width:4" />
					</svg>
					<div class="hexagon_small"></div>
					<div class="repair_step" id="indent">
						<p>​ Once you approve the repair, the repair will commence (firmware upgrade, calibration and test flight  are ALWAYS preformed)</p>
					</div>
				</div>
				<div class="solo_step">
					<div class="hexagon"><p><span class="digit">6</span></p></div>
					<div class="hexagon_big"><img src="<?=FRONT_IMG?>credit-card.png"></div>
										<svg height="4" width="200">
					  <line x1="0" y1="0" x2="200" y2="0" style="stroke:rgb(236,236,236);stroke-width:4" />
					</svg>
					<div class="hexagon_small"></div>
					<div class="repair_step">
						<p>​ An email with payment link will be sent to you.</p>
					</div>
				</div>
				<div class="solo_step">
					<div class="hexagon"><p><span class="digit">7</span></p></div>
					<div class="hexagon_big"><img src="<?=FRONT_IMG?>drone.png"></div>
					<svg height="4" width="200">
					  <line x1="0" y1="0" x2="200" y2="0" style="stroke:rgb(236,236,236);stroke-width:4" />
					</svg>
					<div class="hexagon_small"></div>
					<div class="repair_step" id="indent">
						<p>​ The drone will be shipped and you will receive your aircraft in 1 - 2 business days. </p>
					</div>
				</div>

			</ul>
		</div>
	</div>


	<div class="serviceFlow" >
		<div class="mainContentWidth">
            <? if(count($model->tickets)) { ?>
                <a class="btn btn-info new_ticket_btn"><span class="new_ticket">View Open Tickets</span><span class="new_ticket" style="display: none">Submit a New Ticket</span></a>
            <? } ?>
			<form class="remodal_form" method="post" action="<?=$this->emagid->uri?>">
                <div class="serviceFlowStep new_ticket" style="display: none">
                    <h6 class="gothamLight">Tickets</h6>
                    <fieldset class="selectUIWrapper" style="width:100%">
                        <select id="ticket_select" style="width:100%">
                            <? foreach($model->tickets as $ticket){?>
                                <option value="<?=$ticket->id?>"><?=$ticket->id.' '.$ticket->product.' '.$ticket->model?></option>
                            <? } ?>
                        </select>
                    </fieldset>
                </div>
                <div class="serviceFlowStep new_ticket" style="display: none">
                    <a class="ticket_link"></a>
                </div>
			<div class="serviceFlowStep serviceFlowStep_active new_ticket">
				<?if($model->user){?>
					<h6 class="gothamLight" id="step1">Welcome, <?=$model->user->full_name()?></h6>
					<div class="modal_formWrapper">
						<div class="inputRow">
							<input class="containsText" name="firstname" type="text" value="<?=$model->user->first_name?>" required/>
							<label>First Name</label>
						</div>
						<div class="inputRow">
							<input class="containsText" name="lastname" type="text" value="<?=$model->user->last_name?>"required/>
							<label>Last Name</label>
						</div>
						<div class="inputRow">
							<input class="containsText" name="phone" type="number" value="<?=$model->user->phone?>" required/>
							<label>Phone Number</label>
						</div>
						<div class="inputRow">
							<input class="containsText" name="email" type="email" value="<?=$model->user->email?>" required/>
							<label>Email Address</label>
						</div>
					</div>
				<?}else{?>
					<h6 class="gothamLight" id="step2">Sign in to an Account</h6>
					<div>
						<a data-remodal-target="modal_login">Login</a>
						<span class="verticalSeparator"></span>
						<a data-remodal-target="modal_signup">Create Account</a>
					</div>
				<?}?>
			</div>
			<div class="serviceFlowStep new_ticket">
				<a <?=(($model->user)?'':'data-remodal-target="modal_login"')?>>
					<h6 class="gothamLight" id="step2">Select Product</h6>
						<fieldset class="selectUIWrapper">
							<select id='service_product' name="product" required>
								<option value="-1" disabled selected>Product</option>
								<?foreach($model->products as $key => $prod){?>
									<option value="<?=$key?>"><?=$key?></option>
								<?}?>
							</select>
							<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"></path>
							</svg>
							</span>
						</fieldset>
					<fieldset class="selectUIWrapper">
						<select id='service_model' name="product_model" required >
							<option value="-1" disabled selected>Model</option>
							<?foreach($model->products as $product => $models){?>
								<? foreach ($models as $model) {?>
                                    <option style='display:none' class="model" data-product="<?=$product?>" value="<?=$model?>"><?=$model?></option>
                                <?}?>
							<?}?>
						</select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"></path>
							</svg>
							</span>
					</fieldset>
					<div class="modal_formWrapper">
						<div class="inputRow">
							<input name="model" type="text" required/>
							<label>Serial Number</label>
						</div>
					</div>
				</a>
			</div>
			<div class="serviceFlowStep new_ticket">
				<h6 class="gothamLight" id="step3">Submit Service Request</h6>
				<div class="modal_formWrapper">
					<div class="inputRow textareaRow">
						<textarea name="comment"  placeholder="Description(Let us know the issue with the drone)" style="width:280px;height:120px;"></textarea>
					</div>
					<button type="submit" class="remodal-confirm">Submit</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<script>
    $(document).ready(function () {
       $('#service_product').change(function(){
           $('option.model').hide();
           $('option.model[data-product="'+$(this).val()+'"]').show();
       });
       $('.new_ticket_btn').click(function () {
           $('.new_ticket').toggle();
       });
       $('#ticket_select').change(function () {
           console.log('sadad');
           $('.ticket_link').text('Ticket #'+$('#ticket_select option:selected').text()).attr('href','/service/confirm/'+this.value);
       });
    });
</script>