<div class="pageWrapper servicePageWrapper">
    <?php
    $sender = $model->service_ticket->email;
    $inbox = new EmagidService\MailBox('gator4149.hostgator.com', 'Service@Camrise.com', 'Hello1010');
    $emails = $inbox->getMailBySender($sender);

    $logs = \Model\Service_Log::getList(['where' => ['service_ticket_id' => $model->service_ticket->id]]);

    ?>


    <div class="servicePageHero">
        <div class="media" style="background-image:url('<?=FRONT_IMG?>serviceRequestHero.jpg')"></div>
        <div class="mainContentWidth">
            <h2 class="dji-text">We received your request</h2>
            <h6 class="gothamLight">In DJI NYC we value good customer service and fast repairs. <br/>We provide high quality service in a given time period for your drones. <br>This service includes a free inspection of your aircraft including a estimate being sent to your email, once you approve the repairs we will continue to repair them.
            </h6>
        </div>
    </div>
    <div class="row">
        <div class="request_box">
            <h2>Request and Updates</h2>
            <div>
                <?=$model->service_ticket->comment?>
            </div>
        
        <div>
            <div class="request_box">
            <h2>Service History</h2>
            <? $histories = $model->service_ticket->getHistories();?>
            </div>
            <? if($histories) { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th width="15%">Date</th>
                        <th width="15%">Comment</th>
                        <th width="15%">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($histories as $obj) { ?>
                        <? if(!$obj->notify) continue; ?>
                        <tr class="originalProducts">
                            <td style="border: .5px solid black"><?=(new \Carbon\Carbon($obj->insert_time))->toDateTimeString()?></td>
                            <td style="border: .5px solid black"><?=$obj->comment?></td>
                            <td style="border: .5px solid black"><?=$obj->getStatusName();?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <? } ?>

        </div>
    </div>
    <div class="serviceFlow">
        <div class="mainContentWidth">
            <div class="serviceFlowStep serviceFlowStep_active" style="width: 25%">
                <h6 class="gothamLight" id="step1">Information</h6>
                <div class="modal_formWrapper">
                    <div class="inputRow">
                        <input class="containsText" type="text" value="<?=$model->service_ticket->product?>" disabled/>
                        <label>Product</label>
                    </div>
                    <div class="inputRow">
                        <input class="containsText" name="" type="text" value="<?=$model->service_ticket->product_model?>"disabled/>
                        <label>Model</label>
                    </div>
                    <div class="inputRow">
                        <input class="containsText" name="" type="text" value="<?=$model->service_ticket->model?>" disabled/>
                        <label>Serial Number</label>
                    </div>
                    <div class="inputRow">
                        <input class="containsText" name="" type="text" value="<?=$model->service_ticket->getStatusName()?>" disabled/>
                        <label>Status</label>
                    </div>
                </div>
            </div>
            <div class="serviceFlowStep" style="width: 40%">
                <h6 class="gothamLight" id="step2"><?=$model->service_status[$model->service_ticket->service_status_id]?></h6>

            </div>
            <div class="serviceFlowStep" style="width: 26.57%">
                <h6 class="gothamLight" id="step3">Submit a comment about this ticket</h6>
                <form method="post" >
                    <div class="modal_formWrapper">
                        <div class="inputRow">
                            <label>Comment</label>
                            <textarea style="width:150%" name="comment" class="containsText" type="text"></textarea>
                            <input type="hidden" name="id" value="<?=$model->service_ticket->id?>"/>
                            <button type="submit" class="remodal-confirm">Submit</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>