<div class="pageWrapper aboutPageWrapper">
	<div class='terms'>

		<h1 class='overlay_head1'>TERMS OF USE</h1>
        <p class='overlay_head'>Using this Site</p>
        <p>Please use a standards-compliant web browser. Javascript must be enabled to use this site. By using our website, user agree to the Terms of Use. We may change or update these terms so please check this page regularly. We do not represent or warrant that the information on our website is accurate, complete, or current. This includes pricing and availability information. We reserve the right to correct any errors or omissions, and to change or update the information at any time without prior notice.</p>
        <p class="overlay_head">Return Policy</p>
        <p>All sales are final. We do not issue refunds, accept cancellations, or allow exchanges to be made. On a case by case basis under extenuating circumstances, at our discretion, if we determine a refund is warranted, a 5% fee will be deducted from the refund.</p>
        <p class="overlay_head">Purchase for Personal Use Only</p>
        <p>Buyers may purchase products only for personal use and not for resale. By placing your order, you certify that you are purchasing products for personal use only and not for resale. We reserve the right to refuse orders for any reason without explanation.</p>
        <p class="overlay_head">Shipping</p>
        <p>We do not ship to P.O. Boxes or Freight Forwarders. Please allow up to 7 business days for order verification and processing, and an additional 10 business days for delivery.</p>
        <p>All domestic orders are shipped UPS Ground. International orders (to select countries) are shipped UPS Worldwide Expedited. Shipping fees are calculated as flat-rate per country and may include additional fees calculated on a per item basis and determined by order weight. Fees may include an additional handling charge. We are not responsible for any lost, stolen or damaged shipments. Customers are responsible for paying additional shipping and handling fees to have the order re- shipped, if available. The buyer assumes all responsibilities of claims made with the shipping carrier. Please <a href="mailto:support@kawsone.zendesk.com?subject=UPS%20Claims">contact us</a> with any questions regarding UPS claims.</p>
        <p class="overlay_head">International Duty and Tax </p>
        <p>International customers may incur additional charges at the time of checkout. International Duty and Taxes are calculated according to country and included in the order total at the time of checkout. The buyer is responsible to pay for all import Duty and Taxes prior to shipping. We will transfer the fees we collect to UPS to process your international shipment. </p>
        <p class="overlay_head">Sales Tax</p>
        <p>Sales Tax is charged to orders shipped within the states of New York and New Jersey. The applicable tax rate for orders within the state of New York is 8.875%. The applicable tax rate for orders shipped within the state of New Jersey is 6.625%. Shipping cost is also taxed. Sales tax is not charged to orders shipped to states outside of New York and New Jersey. </p>
        <p class="overlay_head">Copyright and Trademarks</p>
        <p>All content of our website including text, graphics, logos, buttons, icons, images and software is property of KAWS INC. and is protected by United States and international copyright laws. The buyer may electronically copy and print hard copies of pages from this web site solely for personal, non-commercial purposes related to placing an order or shopping with KAWSONE. Any other use of our web site, including reproduction and internet links is strictly prohibited without our prior written permission. </p>
        <p class="overlay_head">Secure Processing </p>
        <p>We use secure processing called SSL Encryption Technology, which is the industry standard. SSL (Secure Sockets Layer) is a protocol developed for the transmission of private information over the internet. SSL uses a private key to encrypt the user’s data, including credit card information, so that it cannot be read while being transferred over the internet. </p>
        <p class="overlay_head">Privacy</p>
        <p>We respect our customer’s account information as private and confidential information and will never share it with any outside affiliates or individuals. User information is secured and encrypted with the Secure Sockets Layer Software. The information we collect for orders includes name, e-mail address, billing address, phone number, credit card, internet protocol (IP) address, and geographical region. By providing this information to us, the buyer represents that they own and consent to our use of such personal data. We use this information to verify and process orders, to collect payment and bill for our services, and to contact and respond to buyer’s requests regarding sales and support. At any time, the buyer has the right to withdraw or decline consent. If the buyer does not provide the requested information, we will not be able to provide services to them.</p>
        <p>Users may unsubscribe from receiving promotional or marketing e-mail from KAWSONE at any time by using the “unsubscribe” link in the e-mail received or by <a href="mailto:support@kawsone.zendesk.com?subject=UPS%20Claims">contacting us</a></p>
      </div>
    </div>

<style type="text/css">
	.pageWrapper .terms {
	  padding: 200px 30px;	
	  text-align: left;
	  max-width: 690px;
	  margin: auto;
	}

	.pageWrapper.aboutPageWrapper {
		background-color: white;
	}

	.pageWrapper.aboutPageWrapper .terms p {
		text-align: left;
	}

	h1 {
		margin-bottom: 50px;
	}

	.pageWrapper.aboutPageWrapper .overlay_head {
	    font-weight: bold;
	    font-size: 15px;
	    margin-top: 36px;
	    margin-bottom: 7px !important;
	}

</style>