<div class="pageWrapper aboutPageWrapper">
	<div class="aboutHero">
		<video id="player" width="100% !important" height="100%" autoplay="" loop="" class="hide-mobile">
			<source src="<?=FRONT_VIDEO?>djiCamriseAbout_video.mp4" type="video/mp4">
		</video>
		<div class="show-mobile about-page-hero-mobile" style="background-image:url('<?=FRONT_IMG?>about-hero-mobile.jpg')">
		</div>
		<div class="mainContentWidth">
			<h2 class="dji-text">Here cameras rise</h2>
			<h6 class="gothamLight">Creativity is at the heart of every dream.</h6>
		</div>
	</div>
	<div class="mainContentWidth">
		<div class="textSectionHeader">
			<h6 class="gothamLight">Our Guiding Principle</h6>
			<p class="gothamBook">Every idea, every groundbreaking leap that changes our world starts with the vision of talented creators. At Camrise, we give these creators the tools they need to bring their ideas to life.</p>
			<p>Today, DJI products are redefining industries. Professionals in filmmaking, agriculture, conservation, search and rescue, energy infrastructure, and more trust DJI to bring new perspectives to their work and help them accomplish feats safer, faster, and with greater efficiency than ever before.</p>
		</div>
	</div>
</div>