


<div class="pageWrapper clearancePageWrapper subcategoryPageWrapper">
    <div class="breadcrumbs">
        <? require_once('templates/modernvice/filterBarClearance.php'); ?>
    </div>
    <div class="clearanceCopyRow">
        <div class="clearanceCopy">
            <h4 class="azoM show710 clearanceHiddenTitle">Clearance</h4>
            <p class="azoSans">We are a made-to-order brand, but the styles within this section are finished and ready to ship for various reasons. Some shoes you see here are showroom samples, while others may be one-off styles or have slight imperfections. Because these styles are already complete, we can offer them at a greater discount and guarantee shipment within two business days. 
            <div>
                <span>Sample Sale styles are sold as-is</span> <span>Promo codes can not be applied</span>
            </div>
        </div>
    </div>
    <div class="noGutter_content_width product_inventory_gridui mainPageInventoryUI">
        <div class="row row_of_4 product_grid_ui">
            <? foreach ($model->clearances as $clearance) {
                $product = \Model\Product::getItem($clearance->product_id);
                $color = isset(json_decode($clearance->color_id,true)[0]) ? \Model\Color::getItem(json_decode($clearance->color_id,true)[0]):  null;
                $prodAttr = array_map(function ($id) {
                    return \Model\Color::getItem($id);
                }, json_decode($clearance->color_id, true))
                ?>
                <div class="col productGridItem">


                    <div
                        class="productGridItemSwatches <?= count($prodAttr) >= 5 ? 'productGridItemSwatchesSlider' : '' ?>">
                        <? foreach ($prodAttr as $pa) { ?>
                            <!--<div class="media colorSwatch" style="background-image: url(<? /*=$pa->swatch()*/ ?>)"></div>-->
                            <div class="productGridItemSwatchWrapper">
                                <img src="<?= $pa->swatch() ?>" class="productGridItemSwatch" data-color_id="<?= $pa->id ?>"
                                     data-pro_id="<?= $product->id ?>"/>
                            </div>
                        <? } ?>
                    </div>
                    <a href="<?= SITE_URL . "products/$product->slug?c=$clearance->id" ?>">
                        <div class="saleInfoWrapper">
                            <small class="saleIndicator" style="display:none!important;visibility: hidden!important;">Clearance</small>
                            <? if (\Model\Event::eventSale($product->id, $product->default_color_id)) {
                                $eventAttr = \Model\Product_Attributes::getEventById($product->id, $product->default_color_id);
                                $event = \Model\Event::getItem($eventAttr->value) ?>
                                <div class="saleEventIcon event-icon icon"
                                     style="background-image:url('<?= $event->getIcon() ?>')"></div>
                            <? } ?>
                        </div>
                        <div class="mediaWrapper">
                            <div class="media"
                                 style="background-image:url(<?= UPLOAD_URL . 'products/' . $product->featuredImage(null,$color->id) ?>)"></div>
                        </div>
                        <div class="dataWrapper">
                            <h4 class="product_name"><?= $product->name ?></h4>

                            <? $percent = round(($product->basePrice() - $clearance->price) * 100 / $product->basePrice()) ?>
                            <h4 class="product_price">
                                    <span class="full_price">
                                        <span class="currency">$</span><?= number_format($product->basePrice(), 2) ?>
                                    </span>
                                <!-- <span class="markdown">-<?= $percent ?>%</span> -->
                                <span class="value">$<?= number_format($clearance->price, 2) ?></span>
                            </h4>
                        </div>
                    </a>
                </div>
            <? } ?>
        </div>

        <div id="loading-spinner">
            <div class="hanger"></div>
            <div class="discoball">
                <img src="<?= FRONT_IMG ?>disco_ball.png" alt="">
            </div>
        </div>
    </div>

</div>
<link rel="stylesheet" type="text/css" href="<?= FRONT_CSS ?>subcategory.css">
<script>
    $(document).ready(function () {
        /*Let event handler attach before triggering click*/
        <?if(isset($model->filterActive) && $model->filterActive){?>
        $('div[data-filter_optiontype=<?=$model->filterActive?>]').trigger('click');
        <?}?>
        $(document).on('mouseover', '.productGridItemSwatch', function (e) {
            var product_id = $(this).attr('data-pro_id');
            var color_id = $(this).attr('data-color_id');
            var self = $(this);
            $.post('/category/buildVariantImage', {product_id: product_id, color_id: color_id}, function (data) {
                if (data.status == 'success') {
                    self.closest('.productGridItem').find('.media').css('background-image', 'url(' + data.image + ')');
//                    if(data.soldOut){
//                        self.closest('.productGridItem').addClass('soldOutGridItem');
//                    } else {
//                        self.closest('.productGridItem').removeClass('soldOutGridItem');
//                    }
//                    if (data.isDiscounted) {
//                        var html = '<span class="full_price"> <span class="currency">$</span>' + data.basePrice + '</span><span class="value">$' + data.price + '</span>';
//                        if (self.closest('.productGridItem').find('small').length) {
//                            self.closest('.productGridItem').find('small').show();
//                        } else {
//                            self.closest('.productGridItem').find('.saleInfoWrapper').append('<small class="saleIndicator">On Sale</small>');
//                        }
//                        if (self.closest('.productGridItem').find('.product_price').length) {
//                            self.closest('.productGridItem').find('.product_price').show();
//                        } else {
//                            self.closest('.productGridItem').find('small').after('<img class="event-icon" style="width: 30px;" src="' + data.event + '">');
//                        }
//                        self.closest('.productGridItem').find('.event-icon').css('background-image', 'url(' + data.event + ')').show();
//                        self.closest('.productGridItem').find('.product_price').html(html);
//                    } else {
//                        self.closest('.productGridItem').find('small').hide();
//                        self.closest('.productGridItem').find('.event-icon').hide();
//                        self.closest('.productGridItem').find('.product_price').html('$' + data.price);
//                    }
                }
            });
        });
        var limit = <?=$model->limit?>;
        var offset = <?=$model->offset + $model->limit?>;
        var totalProductCount = <?=$model->productsCount?>;


        var offsetScroll = true;
        $(window).scroll(function () {


            if (($(window).scrollTop() >= ($(document).height() - $(window).height() - 300)) && (offsetScroll == true)) {
                offsetScroll = false;
                console.log('activated1');
                if (offset < totalProductCount) {
                    console.log(offsetScroll);

                    var mCategory = '<?=$model->mCategory ? $model->mCategory->id : ''?>';
                    var category_id = '<?=$model->category != 'all' ? $model->category->id : 'all'?>';
                    $.post('/clearance/buildClearanceItems<?=isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != '' ? '?' . $_SERVER['QUERY_STRING'] : ''?>', {
                        limit: limit,
                        offset: offset
                    }, function (data) {
                        $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').addClass("loadingProcessActive");
                        $("#loading-spinner").addClass("loading_spinner_active").delay(800).queue(function (appendData) {
                            $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').removeClass("loadingProcessActive");
                            $("#loading-spinner").removeClass("loading_spinner_active");
                            $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').append(data.itemHtml);
                            offset += limit;
                            appendData();

                            if (data.status === "success") {
                                offsetScroll = true;
                                $('.productGridItemSwatchesSlider').flickity({
                                    // options
                                    cellAlign: 'left',
                                    adaptiveHeight: true,
                                    prevNextButtons: true,
                                    pageDots: false,
                                    groupCells: 3,
                                    contain: true,
                                    arrowShape: {
                                        x0: 10,
                                        x1: 65, y1: 50,
                                        x2: 70, y2: 45,
                                        x3: 20
                                    }
                                });
                            }
                        });

                    });
                }
            }
        })
    })
</script>

