
<div class="pageWrapper pageStoreWrapper">
	<div class="cushion_h1">
        <div class="videoInitial">
            <video autoplay="" loop="" muted="" style="height:100%;width:auto;">
                <!-- <source src="<?= FRONT_ASSETS ?>video/first_presentation.webm" type="video/webm"> -->
                <source src="<?= FRONT_ASSETS ?>video/storeMarketing.mp4" type="video/mp4">
            </video>            
        </div>    
        <div class="videoOverlayWrapper">
    		<h1 class="as_m">NYC Factory Store</h1>
    		<h3 class="as_m">A Shoe Shopping Experience Like No Other</h3>
            <div class="action_row">
                <a class="btn black_thin_btn" id="slideToCustom"><i class="material-icons">location_on</i> Visit Us</a>
                <a class="btn black_thin_btn" href="tel:6468547573"><i class="material-icons">local_phone</i> Call Us</a>
                <a class="btn black_thin_btn" href="mailto:info@modernvice.com"><i class="material-icons">email</i> Email Us</a>     
            </div>
		</div>
	</div>
    
    <div class="factoryExplore">
    	<p class="as_r"><span>About the Store</span>Nestled in the heart of New York City's garment district is a one of a kind shoe buying experience. When you visit our headquarters you will have access to our showroom, leather room, studio, glass shoebox and factory. Choose your own leathers, make a custom shoe, meet the designers and see shoes being made. If you are visiting New York City and you love shoes then you MUST stop by Modern Vice.</p>
        <div class="storeSlider">
            <div class="storeSliderItem">
                <img src="<?= FRONT_ASSETS ?>img/factory1.jpg">
            </div>
            <div class="storeSliderItem">
                <img src="<?= FRONT_ASSETS ?>img/factory2.jpg">
            </div>
            <div class="storeSliderItem">
                <img src="<?= FRONT_ASSETS ?>img/factory3.jpg">
            </div>
            <div class="storeSliderItem">
                <img src="<?= FRONT_ASSETS ?>img/factory4.jpg">
            </div>
        </div>
    	<div class="contactInfo">
    		<div>
    			<h5 class="as_m">Location</h5>
    			<p class="as_r">Modern Vice</p>
    			<p class="as_r">247 West 38th Street #301</p>
    			<p class="as_r">New York, NY, 10018, USA</p>
    			<p class="as_r"><a href="tel:6468547573">646 854 7573</a></p>
    		</div>
    		<div>
    			<h5 class="as_m">Hours</h5>
    			<p class="as_r">Mon-Fri: 9am - 7pm</p>
    			<p class="as_r">Sat: 11am - 4:30pm</p>
    			<a href="mailto:info@modernvice.com">info@modernvice.com</a>
                <div>
                <a class="bookAppt" href="/nycshowroom">Book an Appointment</a>
                </div>
    		</div>
    	</div>
        <div id='maps'>
            <iframe id="googleMap" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJAYIw7axZwokR4_H_Zq9c01o&key=AIzaSyAWrwhOEEN-QMZFagR_en8rcCPwuHBmeCk" allowfullscreen></iframe>
        </div> 
    </div>
       
    <div class="customManufacture">
        <h2 class="cal_m">Design Custom Handcrafted Shoes</h2>
        <h5 class="as_m">Get Custom Fitted &#x2022; Pick your Leathers &#x2022; Create a Custom Shoe</h5>    
        <div class="launchFactoryExplore">
            <a id="launchFactoryExploreSlides">
                <h2 class="as_m">Modern Vice Shoe Factory</h2>
                <p class="cal_r">Modern Vice develops its products with a focus on quality, fast accessible fashion, and the necessity of all stylistic individuals in mind. Experience the Modern Vice brand in person, and see how our team creates the shoes that you love. We make shopping in NY as fun as possible, and think we have one of the most unique stores out there. Whether you are trying to do some serious shoe shopping, or you want to checkout a really cool business in NYC, we think you'll have a great time.</p>
            </a>
        </div>
    </div>

</div>