<div class="comparisonPageWrapper pageWrapper">
    <div class="breadcrumbsHeader">
        <div class="mainContentWidth">
            <h1 class="dji-text dji-blue">Comparisons</h1>
        </div>
    </div>
    <div class="comparisonTableWrapper">
        <div class="comparisonTable row">
            <div class="row row_of_4">
                <div class="col faux_col"></div>
                <?foreach($model->products as $product){?>
                    <div class="col">
                        <a class="comparisonToggle activeComparison">
                            <icon class="compareToggleIcon">
                                <span></span>
                                <span></span>
                            </icon>
                        </a>
                        <a class="gridProductContent">
                            <img src="<?=UPLOAD_URL.'products/'.$product->featuredImage()?>" alt="<?=$product->image_alt?>"/>
                            <p class="name"><?=$product->name?></p>
                            <p class="price">USD $<?=$product->basePrice()?></p>
                        </a>
                        <div class="btnRow">
                            <a class="solidBlueBtn hollowBlueBtn" href="<?=SITE_URL.'products/'.$product->slug?>">
                                <p>Buy Now</p>
                            </a>
                        </div>
                    </div>
                <?}?>
            </div>
            <div class="tableBody">
                <div class="specCategory">
                    <?foreach ($model->allColumn as $key=>$value){?>
                        <h4 class="dji-text dji-gray"><?=$key?></h4>
                        <? foreach ($value as $item){?>
                            <div class="row row_of_4 spec_row">
                                <div class="col spec_label">
                                    <p><?=$item->name?></p>
                                </div>
                                <?foreach($model->products as $product){?>
                                    <div class="col spec_val">
                                        <p><?=$product->getSpecValue($item->id)?></p>
                                    </div>
                                <?}?>
                            </div>
                            <?}?>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
</div>