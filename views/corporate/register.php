<div class="pageWrapper servicePageWrapper">


    <div class="servicePageHero">
        <div class="media" style="background-image:url('<?=FRONT_IMG?>serviceRequestHero.jpg')"></div>
        <div class="mainContentWidth">

            <h1 class="dji-text">Corporate</h1>
            <h2 class="dji-text">Registration</h2>

        </div>
    </div>

    <div class="serviceFlow">
        <div class="mainContentWidth">
            <div class="wholesaleStep serviceFlowStep_active wholesaleForm">

                <h6 class="gothamLight">Register for a Corporate Account</h6>
                <form action="<?= $this->emagid->uri ?>" method="post">
                    <div class="row">
                        <div class="row">
                            <input type="text" placeholder="First Name" name="first_name" id="first_name" required>
                        </div>
                        <div class="row">
                            <input type="text" placeholder="Last Name" name="last_name" id="last_name" required>
                        </div>
                        <div class="row">
                            <input type="number" placeholder="Phone Number" name="phone" id="phone">
                        </div>
                        <div class="row">
                            <input type="text" placeholder="Email" name="email" id="email" required>
                        </div>
                        <div class="row">
                            <input type="password" placeholder="Password" name="password" id="password" required>
                        </div>
                        <div class="row">
                            <input type="text" placeholder="Corporation Name" name="name" id="name">
                        </div>
                        <div class="row">
                            <input type="text" name="address" placeholder="Address" id="address" required>
                        </div>
                        <div class="row">
                            <input type="text" placeholder="Address 2" name="address2" id="address2">
                        </div>
                        <div class="row">
                            <input type="text" placeholder="City" name="city" id="city" required>
                        </div>
                        <div class="row">

                            <input type="text" placeholder="State" name="state" id="state" required>
                        </div>
                        <div class="row">
                            <input type="text" placeholder="Zip Code" name="zip" id="zip" required>
                        </div>
                        <div class="row">
                            <select name="country" placeholder="Country" required>
                                <?foreach(get_countries() as $country){?>
                                    <option value="<?=$country?>"><?=$country?></option>
                                <?}?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <input type="submit" value="Submit">
                    </div>
            </div>

    </div>
</div>