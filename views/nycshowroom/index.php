<div class="pageWrapper nycShowroomPageWrapper">
	<div class="videoContainer">
		<div class="absTransCenter bookCenteredBlock appointment-form-wrapper">
			<h4 class="warnock"><em>Want in</em> on a 1-on-1 appointment with one of our stylists?</h4>
			<p>Make an appointment at NYC's only glass shoebox, and we guarantee an experience you'll always remember.</p>
			<a class="btn primaryBtn triggerAppointmentForm">Book an Appointment</a>
			<div class="appointmentForm">
				<p class="formPreCaption">Hi there, Please fill out the following information.</p>
				<form action="<?=$this->emagid->uri?>" method="post">
					<input type="text" placeholder="When" class="appointment-dateTimePicker appointment-input" name="datetime">
					<div class="row row_of_2">
						<div class="col">
							<input type="text" placeholder="First name" class="appointment-input" name="first_name">
						</div>
						<div class="col">
							<input type="text" placeholder="Last name" class="appointment-input" name="last_name">
						</div>
					</div>
					<textarea class="appointment-input" placeholder="Which shoes are you interested in?" name="interest"></textarea>
					<input type="text" placeholder="Email address" class="appointment-input" name="email">
					<input type="text" placeholder="Phone number" class="appointment-input" name="phone">
					<input type="submit" class="btn primaryBtn">
				</form>
			</div> 
		</div>
        <video autoplay="" loop="" muted="" style="height:100%;width:auto;" id="pl_video">
            <source src="<?= FRONT_ASSETS ?>video/360_store.webm" type="video/webm">
            <source src="<?= FRONT_ASSETS ?>video/360_store.mp4" type="video/mp4">
        </video>  
    </div>
</div>
<link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>jquery.datetimepicker.css">
<script src="<?=FRONT_JS?>jquery.datetimepicker.full.min.js"></script>
<script>
	$(document).ready(function(){
		$('.datetimepicker').datetimepicker({
			format:'M d, Y h:ia'
		});
		$('.toggle').on('click',function(){
			$('.togglee').toggle();
		});
	})
</script>