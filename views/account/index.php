<div class="pageWrapper faqPageWrapper cmsPageWrapper">
    <h1 class="dji-text dji-blue">My Account</h1>
        <ul class="nav nav-pills myaccount_nav" role="tablist">
            <li role="presentation" class="<?=($_GET['tab'] == "general-tab" ? "active" : "")?>"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">Customer Profile</a></li>
            <li role="presentation" class="<?=($_GET['tab'] == "orders" ? "active" : "")?>"><a href="#orders" aria-controls="orders" role="tab" data-toggle="tab">Order History</a></li>
            <li role="presentation" class="<?=($_GET['tab'] == "payments" ? "active" : "")?>"><a href="#payments" aria-controls="payments" role="tab" data-toggle="tab">Payment Information</a></li>
            <li role="presentation" class="<?=($_GET['tab'] == "addresses" ? "active" : "")?>"><a href="#addresses" aria-controls="addresses" role="tab" data-toggle="tab">Stored Addresses</a></li>
            <li role="presentation" class="<?=($_GET['tab'] == "wish-list" ? "active" : "")?>"><a href="#wish-list" aria-controls="wish-list" role="tab" data-toggle="tab">Wish List</a></li>
        </ul>


    <div class="content account-info" role="tabpanel">

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?=($_GET['tab'] == "general-tab" ? "active" : "")?>" id="general-tab">
                            <div class="serviceFlow">
        <div class="mainContentWidth">
            <div class="accountInfo">
                <h6 class="gothamLight">Customer Profile</h6>
                 <form method="post" action="<?= $this->emagid->uri ?>">
                            <input hidden name="id" value="<?= $model->user->id ?>">

                            <div class="form-group">
                                <input name="first_name" placeholder="First Name" type="text" value="<?= $model->user->first_name ?>"/>
                            </div>
                            <div class="form-group">
                                <input name="last_name" placeholder="Last Name" type="text" value="<?= $model->user->last_name ?>"/>
                            </div>
                            <div class="form-group">
                                <input name="email" type="text" placeholder="Email Address" value="<?= $model->user->email ?>"/>
                            </div>
                            <div class="form-group">
                                <input name="phone" type="text" placeholder="Phone" value="<?= $model->user->phone ?>"/>
                            </div>
                            <div class="form-group">
                                <input name="password" placeholder="Change Password" type="password"/>
                            </div>
                            <button name="submit" class="btn btn-info btn-lg">Save Account Info</button>
                        </form>
                    </div>
            </div>
        </div>
            </div>
        </div>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?=($_GET['tab'] == "orders" ? "active" : "")?>" id="orders">
                <div class="serviceFlow">
                    <div class="mainContentWidth">
                        <div class="accountInfo">
                            <h6 class="gothamLight">Order History</h6>
                                <?if($model->orderHistory){ ?>
                                    <table>
                                        <thead>
                                        <tr>
                                            <th><label>Id</label></th>
                                            <th><label>Purchased</label></th>
                                            <th><label>Total</label></th>
                                            <th><label>Products</label></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?foreach($model->orderHistory as $order){
                                            $orderProduct = \Model\Order_Product::getList(['where'=>"order_id = $order->id"])?>
                                             <tr>
                                                <td><?=$order->id?></td>
                                                <td><?=date('F d, Y h:i a',strtotime($order->insert_time))?></td>
                                                <td>$<?=number_format($order->total,2)?></td>
                                                <td>
                                                    <?foreach($orderProduct as $op){
                                                        $product = \Model\Product::getItem($op->product_id)?>
                                                        <img src="<?=$product->featuredImage()?>" alt="<?=$product->image_alt?>" width="100px"/>
                                                    <?}?>
                                                </td>
                                            </tr>
                                            <tr colspan="4">
                                                <hr>
                                            </tr>
                                        <?}?>
                                        </tbody>
                                    </table>
                                <?} else {?>
                                    <div>No active history</div>
                                <?}?>
                        </div>
                    </div>
                </div>
            </div>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?=($_GET['tab'] == "payments" ? "active" : "")?>" id="payments">
                <div class="serviceFlow">
                    <div class="mainContentWidth">
                        <div class="accountInfo">
                            <h6 class="gothamLight">Payments</h6>
                            <p>No Active Payments</p>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?=($_GET['tab'] == "addresses" ? "active" : "")?>" id="addresses">
                <div class="serviceFlow">
                    <div class="mainContentWidth">
                        <div class="accountInfo">
                            <h6 class="gothamLight">Addresses</h6>
                            <? if(!sizeof($model->addresses)) {?>
                                <p>No Active Address</p>
                            <? } ?>
                            <div class="row">
                                <select name="view_addr">
                                    <? foreach($model->addresses as $address) {
                                        echo "<option value='$address->id'>$address->label</option>";
                                    }?>
                                    <option value=0>Add New</option>
                                </select><input type="button" name="delete_addr" value="Remove">
                            </div>
                            <? $first = 0;
                            foreach($model->addresses as $address) { ?>
                            <div class="row address_info" name="<?=$address->id?>" <? if($first > 0) {echo 'hidden';}  $first++;?>>
                                <div><b><?=$address->label?></b></div>
                                <div><b>First Name:</b>     <?=$address->first_name?> </div>
                                <div><b>Last Name:</b>      <?=$address->last_name?>  </div>
                                <div><b>Phone Number:</b>   <?=$address->phone?>      </div>
                                <div><b>Address:</b>        <?=$address->address?>    </div>
                                <div><b>Address line 2:</b> <?=$address->address2?>   </div>
                                <div><b>City:</b>           <?=$address->city?>       </div>
                                <div><b>State:</b>          <?=$address->state?>      </div>
                                <div><b>Zipcode:</b>        <?=$address->zip?>        </div>
                                <div><b>Country:</b>        <?=$address->country?>    </div>
                            </div>
                            <? }?>
                            <div class="row address_info new_addr" <?=($first>0 ? 'hidden':'')?>
                                <form id="new_addr" method="post" action="javascript:void(0);">
                                    <input type="hidden" name="user_id" value="<?=$model->user->id?>">
                                    <div class="row">
                                        <input type="text" placeholder="Address Name" name="label" id="label" required>
                                    </div>
                                    <div class="row">
                                        <input type="text" placeholder="First Name" name="first_name" id="first_name" required>
                                    </div>
                                    <div class="row">
                                        <input type="text" placeholder="Last Name" name="last_name" id="last_name" required>
                                    </div>
                                    <div class="row">
                                        <input type="number" placeholder="Phone Number" name="phone" id="phone">
                                    </div>
                                    <div class="row">
                                        <input type="text" name="address" placeholder="Address" id="address" required>
                                    </div>
                                    <div class="row">
                                        <input type="text" placeholder="Address 2" name="address2" id="address2">
                                    </div>
                                    <div class="row">
                                        <input type="text" placeholder="City" name="city" id="city" required>
                                    </div>
                                    <div class="row">

                                        <input type="text" placeholder="State" name="state" id="state" required>
                                    </div>
                                    <div class="row">
                                        <input type="text" placeholder="Zip Code" name="zip" id="zip" required>
                                    </div>
                                    <div class="row">
                                        <select name="country" placeholder="Country" required>
                                            <?foreach(get_countries() as $country){?>
                                                <option value="<?=$country?>"><?=$country?></option>
                                            <?}?>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <input type="submit" value="Add">
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?=($_GET['tab'] == "wish-list" ? "active" : "")?>" id="wish-list">
                                <div class="serviceFlow">
                    <div class="mainContentWidth">
                        <div class="accountInfo">
                            <h6 class="gothamLight">Favorites</h6>

                    <? if ($model->favorites) {
                        foreach ($model->favorites as $favorite) {
                            $prod = \Model\Product::getItem($favorite->product_id)?>
                            <div><a href="<?=SITE_URL.'products/'.$prod->slug?>"><?=$prod->name?></a></div>
                            <div><a href="<?=SITE_URL.'products/'.$prod->slug?>">$<?=number_format($prod->price,2)?></a></div>
                            <div><a href="<?=SITE_URL.'products/'.$prod->slug?>"><img src="<?=$prod->featuredImage()?>" alt="<?=$product->image_alt?>" width="100px"/></a></div>
                        <? } ?>
                    <? } else { ?>
                    <p>No active Favorites</p>
                    <?php } ?>
                </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--                             <form method="post" action="/login/logout">
                            <input class="btn btn_oreo signout_btn" type="submit" value="Sign out"/>
                        </form> -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
    $(function(){
        $('.myaccount_nav a').click(function (e) {
            e.preventDefault();
            $('.tab-content').find('.active').each(function(){
               $(this).removeClass('active');
            });
            $(this).tab('show');
        });
    });
    $('#new_addr').submit(function(event) {
        event.preventDefault();
        var url = "<?php echo SITE_URL; ?>account/new_address";
        $.post(url, $(this).serialize(),function(data){
            if(data.status == 'success'){
                $("select[name='view_addr'] > option:last").before('<option value="'+data.address.id+'">'+data.address.label+'</option>');
                $(".address_info.new_addr").before('<div class="row address_info" name="'+data.address.id+'" hidden>'
                                                    +'<div><b>'+data.address.label+'</b></div>'
                                                    +'<div><b>First Name:</b>'+data.address.first_name+'</div>'
                                                    +'<div><b>Last Name:</b>'+data.address.last_name+'</div>'
                                                    +'<div><b>Phone Number:</b>'+data.address.phone+'</div>'
                                                    +'<div><b>Address:</b>'+data.address.address+'</div>'
                                                    +'<div><b>Address line 2:</b>'+data.address.address2+'</div>'
                                                    +'<div><b>City:</b>'+data.address.city+'</div>'
                                                    +'<div><b>State:</b>'+data.address.state+'</div>'
                                                    +'<div><b>Zipcode:</b>'+data.address.zip+'</div>'
                                                    +'<div><b>Country:</b>'+data.address.country+'</div>'
                                                    +'</div>');
            }
        });

    });
    $("select[name='view_addr']").on('change',function(){
        $('.address_info').attr('hidden','');
        if($(this).val() == '0'){
            $('.address_info.new_addr').removeAttr('hidden');
        } else {
            $('.address_info[name="'+$(this).val()+'"]').removeAttr('hidden');
        }
    });

</script>