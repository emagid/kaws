<div class="pageWrapper loginPageWrapper">
	<div class="row loginMainRow">
		<div class="col">
			<icon class="circleLogoOreo">
			</icon>
			<h5 class="as_r">Slip on In</h5>
			<h8 class="as_l">Made in NYC</h8>
			<div class="separator">
			</div>
			<div class="reasons">
				<h6 class="as_l">Complete checkout faster</h6>
				<h6 class="as_l">We can send you personalized Modern Vice reccomendations</h6>
				<h6 class="as_l">Get cccess to exclusive deals and early releases</h6>
				<h6 class="as_l">Create and share personal favorites</h6>
			</div>
		</div>
		<div class="col">
			<div class="formWrapper accountFormWrapper">
				<div class="row">
					<div class="row">
						<label for="fname">First Name</label>
						<input type="text" id="fname">
					</div>
					<div class="row">
						<label for="lname">Last Name</label>
						<input id="lname" type="text">
					</div>
					<div class="row">
						<label for="email">Email <span>*</span></label>
						<input type="email" id="email">
					</div>
					<div class="row">
						<label for="phone">Phone <span>*</span></label>
						<input type="number" id="phone">
					</div>
					<div class="row">
						<label for="password">Password</label>
						<input type="password" id="password">
					</div>
					<div class="row">
						<label for="confirmPassword">Confirm Password</label>
						<input type="password" id="confirmPassword">
					</div>
				</div>
				<div class="row">
					<input type="submit" class="btn btn_black btn_full_width" value="Create">
				</div>
				<div class="footer_links">
					<a>Return to Store</a>
				</div>
			</div>
		</div>
	</div>
</div>