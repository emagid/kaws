<?$header = $model->header;?>
<div class="pageWrapper pageNoRightSidebar privateLabelPageWrapper">
	<div class="floatingNav">
		<div id="privateLabelNav" class="expandCollapseModule expandCollapseModule_expanded">
			<ul>
				<li>
					<a data-vertical_screen_nav="1" class="activePageSection">Intro to Private Label</a>
				</li>
				<li>
					<a data-vertical_screen_nav="2">Case Studies</a>
				</li>
				<li>
					<a data-vertical_screen_nav="3">Services</a>
				</li>
				<li>
					<a data-vertical_screen_nav="4">Additional Capabilities</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="show_mdl floatingActionBtn" data-mdl_name="plApplication">
		<p>Submit Inquiry</p>
	</div>
	<div class="introView cushion_h1 privateLabelSection" data-vertical_screen="1">
		<div class="videoInitial">

		

            <video autoplay="" loop="" muted="" style="height:100%;width:auto;" id="pl_video">
                <!-- <source src="<?= FRONT_ASSETS ?>video/first_presentation.webm" type="video/webm"> -->
                <source src="<?= FRONT_ASSETS ?>video/shoeManufacture.mp4" type="video/mp4">
            </video>   
            <script>
				var vid = document.getElementById("pl_video");
				vid.playbackRate = 0.35;
			</script>         
        </div> 
		<div class="content_width_1000">
			<div>
				<?if($header && $header->featured_image && $header->getFeaturedImage()){?>
					<style type="text/css">
						.circleLogoOreo {
							background-image: url(<?=$header->getFeaturedImage()?>);
						}
					</style>
				<?}?>
				<icon class="circleLogoOreo">
				</icon>
			</div>
			<div>
				<?
				$title = $header && $header->title ? : 'MV Private Label';
				$titlecaption = 'Shoe Design and Manufacturing in NYC';
				$subtitle = $header && $header->subtitle ? : 'Modern Vice has a depth of experience in designing and manufacturing private label shoes for companies of any size, from small startup shoe companies to very large brands. Our unique capabilities allow us to provide a custom and seamless solution to anyone looking for a top-quality shoe manufacturer.';
				?>
				<h4 class="as_r">Private Label</h4>
				<h6 class="cortado">Footwear development &#x26; production services <br>Based in New York City</h6>
				<p class="as_l" style="display:none;visibility: hidden;pointer-events: none;opacity:0;" ><?=$subtitle?></p>
			</div>
		</div>
	</div>
	<div class="privateLabelSection content_width_1000 plTestimonials" style="display:none;visibility: hidden;pointer-events: none;opacity:0;" data-vertical_screen="2">
		<div class="cushion_h3">
			<h3 class="as_r">Private Label Case Studies</h3>
		</div>	
		<div class="tabbedView caseStudiesTabbedView">
			<div class="tab_contents_wrapper">
				<?$active = true;
				foreach($model->caseStudy as $caseStudy){
					$caseTitle = $caseStudy->case_study;
					$caseTitle = str_replace('\'','',$caseTitle);
					?>
					<div class="tab_content <?=$active ? 'tab_content_active' : '';?> tab_content_<?=$caseTitle?> row row_of_2">
						<div class="mediaWrapper col">
							<img src="<?=$caseStudy->getFeaturedImage()?>" class="example">
						</div>
						<div class="textWrapper col">
							<div class="abs_trans_center">
								<h4 class="as_r"><?=$caseStudy->title?></h4>
								<p class="lyon_r"><?=$caseStudy->subtitle?></p>
								<a class="btn btn_oreo show_mdl" data-mdl_name="plApplication">Submit Inquiry</a>
							</div>
						</div>
					</div>
				<?$active = false;}?>
			</div>
			<div class="tabController row row_of_6">
				<?$active = true;
				foreach($model->caseStudy as $caseStudy){
				$caseTitle = $caseStudy->case_study;
				$caseTitle = str_replace('\'','',$caseTitle)?>
				<div class="tab <?=$active?'active':''?> col" data-tab_title="<?=$caseTitle?>">
					<p class="as_r"><?=$caseStudy->case_study?></p>
				</div>
				<?$active = false;}?>
			</div>
		</div>
	</div>	
	<div class="textCenteredRow privateLabelIntro">

		<p>Welcome to Modern Vice’s Footwear Development and Private Label division! By far the best place to be for any and all your shoe needs, developing a shoe has never been so easy! Our state of the art, multi-million dollar facility combines modern day technology with traditional Italian shoemaking and craftsmanship that is only available to the biggest luxury brands in the world. Located at the heart of New York City's garment district, Modern Vice caters to any and all of your needs. New designers will get a “shoe in”, whereas existing brands, both big and small, will be able to get a boost!<br><br>
			Made-in-Midtown is one of just a few places in the world and the only place in New York City where shoe design, development, production, digital content and web services, for any size brand, meet under one roof. The few remaining footwear manufacturers in the United States focus in on one style, whether it is workwear, athletic or casual. M-I-M is the only factory that develops a full range of styles from A to Z.<br><br>
			Over the past 50 years, as fashion production left the USA, the relationship between design and production became distant. Today, it's normal for designers to visit their factories overseas once or twice a year if at all. Conveniently located in New York City, Made in Midtown cuts out the middle man and makes the process more efficient.<br><br>
			Made in Midtown is fluent in design, development, and production; with designers being able to visit the factory for follow-ups on sampling and production runs, which is not possible anywhere else in the fashion footwear business. Having used our services, our brand partnerships have yielded award-winning styles extensively covered in the press.<br><br>
			Made in Midtown lends its production and shoe structure expertise to advise designers on creating products that are most efficiently made.<br><br>
			Made in Midtown is the only shoe production facility that operates at any significant scale in New York City. Once home to a myriad of shoe factories, most designers are forced to work with production at far-flung locations and with a significant language barrier. USA-based designers can get samples and do production runs in days rather than weeks with Made in Midtown.<br><br>
			At our factory, we have the capacity for significant growth in handmade shoe production, while maintaining quality.<br><br>
			The craftsmen in our factory are all masters in the art of shoemaking that have worked a minimum of 30 years making shoes. Since the decline of the fashion production industry throughout North America, qualified shoemakers have flocked to MIM to get reliable work. Thus, we have a congregation of some of the best shoemaking talent in the world.<br><br>
			In conclusion, the reason why we are able to cover a wide variety of needs and services is because of our strategic relationships and partnerships with our factories and suppliers in Italy, Brazil, China, and Taiwan. In addition, we visit our factories up to six times a year, as well as our partners frequently visiting us, in order to maintain our supplies and always be on the same page.</p>
	</div>
	<div class="scrollPowerView privateLabelFeatures content_width_1000 privateLabelSection" data-vertical_screen="3">
		<div class="cushion_h3">
			<h3 class="as_r">Our Services</h3>
		</div>
		<?$swap = true;
		foreach($model->services as $service){?>
		<div class="row row_of_2 mediaTextColsUI mediaTextColsUI <?=$swap ? 'mediaTextColsUI_imgRight' : 'mediaTextColsUI_imgLeft'?>">
			<div class="col mediaWrapper">
				<div class="media" style="background-image:url(<?=$service->getFeaturedImage()?>)">
				</div>
			</div>
			<div class="col">
				<div class="abs_trans_center iconTextDesc">
					<icon style="background-image:url(<?=$service->getFeaturedImage()?>)"></icon>
					<h4 class="as_r"><?=$service->title?></h4>
					<p class="lyon_r"><?=$service->subtitle?></p>
				</div>
			</div>
		</div>
		<?$swap = $swap ? false : true;}?>
	</div>
	<div class="row_of_2 row plExtraInfo privateLabelSection" data-vertical_screen="4">
		<?foreach($model->additional as $additional){?>
		<div class="col privateLabelSection content_width_1000 confidentiality">
			<div class="abs_trans_center">
				<div class="cushion_h3">
					<h4 class="as_r"><?=$additional->title?></h4>
				</div>
				<p class="lyon_r"><?=$additional->subtitle?></p>
			</div>
		</div>
		<?}?>
	</div>
</div>