<style>
    .row:before{
        position: relative;
    }
</style>
<div class="pageWrapper checkoutPageWrapper cartPageWrapper shippingCheckoutPageWrapper">
    <div class="row content_width_1000">
        <div class="emailFormWrapperCol shippingFormWrapperCol paymentFormWrapperCol col">
            <h1 class="as_l">Secure Payment</h1>
            <form method="post" action="/repair_invoice" id="myContainer">
                <h7 class="as_l title">Pay with your Credit Card</h7>
                <div class="creditCardPaymentFormSection">
                    <input name="email" type="hidden" value="<?=$this->viewData->ticket->email?>"/>
                    <input name="ticket_id" type="hidden" value="<?=$this->viewData->ticket->id?>"/>
                    <input name="payment_method" type="hidden" value="1"/>
                    <label for="cardUserName">Name on Card</label>
                    <input type="text" name="card_name" maxlength="60" class="card_name" id="cardUserName" required>
                    <label for="ccnumber">Credit Card Number</label>
                    <div class="cardNumberInputWrapper">
                        <input type="text" pattern="[0-9]*" name="cc_number" maxlength="16" class="card_num" id="ccnumber"  required>
                        <div id="creditCardIcons">
                            <div class="media"></div>
                        </div>
                    </div>
                    <input type="hidden" name="cc_type" id="cc_type">
                    <label for="checkoutPhone">Expires on</label>
                    <fieldset class="selectUIWrapper selectExpMonth">
                        <select name="cc_expiration_month" required>
                            <?foreach(get_month() as $value=>$item){?>
                                <option value="<?=$value?>"><?=$item?></option>
                            <?}?>
                        </select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
										<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
										L-321,348.1z"></path>
									</svg>
						</span>
                    </fieldset>
                    <fieldset class="selectUIWrapper selectExpYear">
                        <select name="cc_expiration_year" required>
                            <?$carbon = \Carbon\Carbon::now()->year;
                            for($i = $carbon; $i < $carbon+14; $i++){?>
                                <option value="<?=$i?>"><?=$i?></option>
                            <?}?>
                        </select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
										<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
										L-321,348.1z"></path>
									</svg>
						</span>
                    </fieldset>
                    <!--			<input type="text" name="card_expire" tabindex="3" class="card_expire" id="checkoutPhone">-->
                    <label for="secCode">Security Code
                        <div style="display:inline-block" class="cvv_help_box">
                            <div class="trigger">
                                <span class="as_m">?</span>
                            </div>
                            <div class="ccv_help_box">
                                <p>For security reasons, please enter your card verification number (CVV) or card identification number (CID). Find this three-digit number on the back of your Visa, MasterCard and Discover cards in the signature area, or the four-digit number on the front of your American Express card, above the credit card number.</p>
		                        <span class="ccv_tip_icons">
		                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="384.5 10.7 201.9 58.3" enable-background="new 384.5 10.7 201.9 58.3" xml:space="preserve">
		                                <g id="cvv_1_">
                                            <g>
                                                <path fill="#C3C3C3" d="M468.3,59.9c0,3.7-3,6.5-6.8,6.5h-67.9c-3.8,0-6.8-2.9-6.8-6.5V20.6c0-3.7,3-6.5,6.8-6.5h67.9
		                                            c3.8,0,6.8,2.9,6.8,6.5L468.3,59.9C468.3,59.9,468.3,59.9,468.3,59.9z"></path>
                                            </g>
                                            <rect x="386.8" y="22.3" fill="#222222" width="81.5" height="10.5"></rect>
                                            <rect x="393.2" y="38" fill="#FFFFFF" width="48.9" height="10.5"></rect>
                                            <rect x="397.1" y="41.9" fill="#222222" width="40.9" height="2.6"></rect>
                                            <rect x="449.8" y="38" fill="#FFFFFF" width="11.3" height="10.5"></rect>
                                            <rect x="451.1" y="41.9" fill="#222222" width="8.4" height="2.6"></rect>
                                            <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="455.4" cy="43.2" rx="10.6" ry="10.3"></ellipse>
                                        </g>
                                        <g id="cvv-amex_1_">
                                            <g id="cvv-amex">
                                                <g>
                                                    <g>
                                                        <path fill="#5EC1EC" d="M586.4,59.7c0,3.8-3.1,6.7-7,6.7h-69.7c-3.9,0-7-3-7-6.7V19.4c0-3.8,3.1-6.7,7-6.7h69.7c3.9,0,7,3,7,6.7
		                                                    V59.7z"></path>
                                                        <g>
                                                            <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
		                                                        c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
                                                            <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
		                                                        v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
		                                                        "></path>
                                                        </g>
                                                        <g>
                                                            <path fill="#FFFFFF" d="M514.1,28.6l-0.4-1.6h-2.9l-0.4,1.6h-2.8l2.9-8.9h3.3l3.1,8.9H514.1z M513.1,25l-0.4-1.5
		                                                        c-0.1-0.3-0.1-0.7-0.3-1.2c-0.1-0.5-0.3-0.9-0.3-1.1c0,0.3-0.1,0.5-0.3,1.1c-0.1,0.5-0.4,1.3-0.7,2.8L513.1,25L513.1,25z"></path>
                                                            <path fill="#FFFFFF" d="M521.7,28.6l-1.8-6.3l0,0c0.1,1.1,0.1,1.9,0.1,2.6v3.8h-2.2v-8.7h3.3l2,6.3l0,0l1.8-6.3h3.3v8.7h-2.2
		                                                        v-3.8c0-0.1,0-0.4,0-0.7c0-0.3,0-0.8,0.1-1.7l0,0l-1.8,6.2H521.7L521.7,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M535.4,28.6H530v-8.9h5.4v1.9h-2.9v1.3h2.8v1.9h-2.8v1.6h2.9L535.4,28.6L535.4,28.6z"></path>
                                                            <path fill="#FFFFFF" d="M545.3,28.6h-2.9l-1.8-2.8l-1.8,2.8H536L539,24l-2.9-4.3h2.8l1.7,2.7l1.5-2.7h2.8l-2.9,4.6L545.3,28.6z
		                                                        "></path>
                                                        </g>
                                                    </g>
                                                    <rect x="507.5" y="43.9" display="none" fill="#FFFFFF" width="69.7" height="10.6"></rect>
                                                    <rect x="513.4" y="47.9" fill="#222222" width="56.1" height="2.7"></rect>
                                                    <rect x="565.3" y="31.4" fill="#FFFFFF" width="11.1" height="10.7"></rect>
                                                    <rect x="566.7" y="35.4" fill="#222222" width="8.4" height="2.7"></rect>
                                                </g>
                                                <ellipse fill="none" stroke="#FF0000" stroke-miterlimit="10" cx="570.8" cy="36.8" rx="10.9" ry="10.5"></ellipse>
                                            </g>
                                        </g>
		                            </svg>
		                        </span>
                            </div>
                        </div>
                    </label>
                    <input type="text" name="cc_ccv" maxlength="4" tabindex="4" class="card_cvv" id="secCode" required>
                </div>

                <?if(!isset($_SESSION['gift_card'])){?>
                    <h7 class="as_l">Gift Card Code</h7>
                    <div class="giftCardSection">
                        <input type="text" name="gift-card-code" id="giftCardInput">
                        <input type="button" value="Apply" id="redeem-gift-card" class="btn btn_black">
                    </div>
                <?} else {?>
                    <h7 class="as_l">Gift Card: <b><?=($gc = \Model\Gift_Card::validateGiftCard($_SESSION['gift_card'])) ? $gc->name: $_SESSION['gift_card']?></b></h7>
                    <input type="button" value="Remove" style="cursor: pointer" id="remove-gift-card" class="btn_black">
                <?}?>
                <h7 class="as_l">Shipping Address</h7>
                <div>
                    <label for="checkoutFName">First Name</label>
                    <input type="text" name="ship_first_name" maxlength="60" tabindex="1" class="checkOutInput" id="checkoutFName" value="<?=$ship_first_name?>" required>
                    <label for="checkoutLName">Last Name</label>
                    <input type="text" name="ship_last_name" maxlength="60" tabindex="2" class="checkOutInput" id="checkoutLName" value="<?=$ship_last_name?>" required>
                    <label for="checkoutPhone">Phone <span class="optional">(optional)</span></label>
                    <input type="text" name="phone" maxlength="60" tabindex="3" class="checkOutInput" id="checkoutPhone" value="<?=$phone?>">
                    <label for="checkoutAddress1">Address Line 1</label>
                    <input type="text" name="ship_address" maxlength="60" tabindex="4" class="checkOutInput" id="checkoutAddress1" value="<?=$ship_address?>" required>
                    <label for="checkoutAddress2">Address Line 2 <span class="optional">(optional)</span></label>
                    <input type="text" name="ship_address2" maxlength="60" tabindex="5" class="checkOutInput" id="checkoutAddress2" value="<?=$ship_address2?>">
                    <label for="checkoutCity">City</label>
                    <input type="text" name="ship_city" maxlength="60" tabindex="6" class="checkOutInput" id="checkoutCity" value="<?=$ship_city?>" required>
                    <label for="checkoutZip">Country</label>
                    <fieldset class="selectUIWrapper countrySelectWrapper">
                        <select name="ship_country">
                            <?foreach(get_countries() as $country){?>
                                <option value="<?=$country?>" <?=$ship_country == $country ? 'selected': ''?>><?=$country?></option>
                            <?}?>
                        </select>
					<span class="downArrowICon">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
									<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
									L-321,348.1z"></path>
								</svg>
					</span>
                    </fieldset>
                    <div id="us_states">
                        <label for="checkoutRegion">County / State / Region *</label>
                        <fieldset class="selectUIWrapper countrySelectWrapper">
                            <select name="ship_state" id="checkoutRegionUS" required>
                                <?foreach(get_states() as $short=>$long){?>
                                    <option value="<?=$short?>" <?=$ship_state == $short ? 'selected': ''?>><?=$long?></option>
                                <?}?>
                            </select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"></path>
							</svg>
						</span>
                        </fieldset>
                    </div>
                    <div id="foreign_states" style="display: none;">
                        <label for="checkoutRegion">County / State / Region *</label>
                        <input disabled type="text" name="ship_state" maxlength="60" tabindex="7" class="checkOutInput" id="checkoutRegion" value="<?=$ship_state?>" required>
                    </div>
                    <label for="checkoutZip">Postcode / Zip Code *</label>
                    <input type="text" name="ship_zip" maxlength="60" tabindex="8" class="checkOutInput" id="checkoutZip" value="<?=$ship_zip?>" required>
                </div>
                <h7 class="as_l">Billing Address</h7>
                <?$sessionData = json_decode($_SESSION['customerDetails'],true)?>
                <?$checkBilling = $sessionData['shipping_is_billing'] == 'on' ? true: false;
                $shippingState=$sessionData['ship_state'];
                $shippingState = preg_replace('/\s+/', '', strtolower($shippingState));
                $shipping = $model->shipping;
                if($shippingState=='ny'||$shippingState=='newyork'){
                    $tax_rate = 8.875;
                }else if($shippingState=='nj'||$shippingState=='newjersey'){
                    $tax_rate = 6.875;
                }else {
                    $tax_rate = 0;
                }?>
                <div class="billingAddressInfo <?=$checkBilling ? 'hiddenShippingInfo': ''?>">
                    <label for="fname">First Name</label>
                    <input type="text" name="bill_first_name" maxlength="60" tabindex="1" class="checkOutInput" id="fname" required value="<?=$checkBilling ? $sessionData['ship_first_name']: ''?>">
                    <label for="lname">Last Name</label>
                    <input type="text" name="bill_last_name" maxlength="60" tabindex="2" class="checkOutInput" id="lname" required value="<?=$checkBilling ? $sessionData['ship_last_name']: ''?>">
                    <label for="checkoutPhone">Phone <span class="optional">(optional)</span></label>
                    <input type="text" name="bill_phone" maxlength="60" tabindex="3" class="checkOutInput" id="checkoutPhone" value="<?=$checkBilling ? $sessionData['phone']: ''?>">
                    <label for="checkoutAddress1">Address Line 1</label>
                    <input type="text" name="bill_address" maxlength="60" tabindex="4" class="checkOutInput" id="checkoutAddress1" required value="<?=$checkBilling ? $sessionData['ship_address']: ''?>">
                    <label for="checkoutAddress2">Address Line 2 <span class="optional">(optional)</span></label>
                    <input type="text" name="bill_address2" maxlength="60" tabindex="5" class="checkOutInput" id="checkoutAddress2" value="<?=$checkBilling ? $sessionData['ship_address2']: ''?>">
                    <label for="checkoutCity">City</label>
                    <input type="text" name="bill_city" maxlength="60" tabindex="6" class="checkOutInput" id="checkoutCity" required value="<?=$checkBilling ? $sessionData['ship_city']: ''?>">
                    <label for="checkoutCountry">Country</label>
                    <fieldset class="selectUIWrapper countrySelectWrapper">
                        <select name="bill_country" id="checkoutCountry">
                            <? foreach (get_countries() as $country) { ?>
                                <option value="<?= $country ?>" <?= $checkBilling && $sessionData['ship_country'] == $country ? 'selected' : '' ?>><?= $country ?></option>
                            <? } ?>
                        </select>
				<span class="downArrowICon">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
						<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
						L-321,348.1z"></path>
					</svg>
				</span>
                    </fieldset>
                    <div id="us_states" <?= $checkBilling && $sessionData['ship_country'] == 'United States' ? '' : 'style="display: none"' ?>>
                        <label for="checkoutRegion">County / State / Region *</label>
                        <fieldset class="selectUIWrapper countrySelectWrapper">
                            <select name="bill_state" id="checkoutRegionUS" required <?= $checkBilling && $sessionData['ship_country'] == 'United States' ? '' : 'disabled' ?>>
                                <? foreach (get_states() as $short => $long) { ?>
                                    <option value="<?= $short ?>" <?= $checkBilling && $sessionData['ship_state'] == $short ? 'selected' : '' ?>><?= $long ?></option>
                                <? } ?>
                            </select>
						<span class="downArrowICon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-334 336 26.4 16.1" enable-background="new -334 336 26.4 16.1" xml:space="preserve">
								<path stroke="#000000" stroke-width="0.85" stroke-miterlimit="10" d="M-321,348.1l-10.6-11.1l-1.4,1.4l12,12.6l12-12.6l-1.4-1.4
								L-321,348.1z"></path>
							</svg>
						</span>
                        </fieldset>
                    </div>
                    <div id="foreign_states" <?=$checkBilling && $sessionData['ship_country'] == 'United States' ? 'style="display: none"': ''?>>
                        <label for="checkoutRegion">County / State / Region *</label>
                        <input <?=$checkBilling && $sessionData['ship_country'] == 'United States' ? 'disabled':''?> type="text" name="bill_state" maxlength="60" tabindex="7" class="checkOutInput" id="checkoutRegion" value="<?=$checkBilling ? $sessionData['ship_state']: ''?>" required>
                    </div>
                    <label for="checkoutZip">Postcode / Zip Code *</label>
                    <input type="text" name="bill_zip" maxlength="60" tabindex="8" class="checkOutInput" id="checkoutZip" required value="<?=$checkBilling ? $sessionData['ship_zip']: ''?>">
                </div>
                <input name="tax_rate" value="<?=$tax_rate?>" hidden />
                <input type="hidden" name="noajax" value="true"/>
                <div class="row secondaryFocus">
                    <div class="right_float">
                        <input type="submit" value="Place Order" id="creditCardPayBtn" class="btn btn_black btn_blue btn_full_width">
                        <input type="submit" style="display:none;" value="Proceed with Paypal" id="paypalPayBtnClicker" class="btn btn_black btn_blue btn_full_width">
                    </div>
                </div>
                <input name="shipping" type="hidden" value="<?=$shipping?>"/>
                <?if(isset($model->cart->discount)&&($model->cart->discount)){?>
                    <input type="text" name="coupon" value="<?=$model->cart->discount?>" hidden>
                <?}?>
                <input name="gift_card" type="hidden" value="<?=$model->giftCardFullCharge?>"/>
                <input name="gift_card_code" type="hidden" value="<?=isset($_SESSION['gift_card']) && $_SESSION['gift_card'] ? $_SESSION['gift_card']: '';?>"/>
            </form>
            <!--			<form id="myContainer" method="post" action="/checkout/payment">-->
            <!--			</form>-->
        </div>
        <div class="shoppingInfoCol col shoppingInfoColPayment">
<!--            <div style="display:none!important;">-->
<!--                --><?//if(isset($model->cart->discount)&&($model->cart->discount)){?>
<!--                    <p>Discount Applied | <span class="pinkDiscount">- $--><?//=number_format($model->cart->discount,2)?><!--</span> </p>-->
<!--                --><?//} else {?>
<!--                    <label for="email"><p>Redeem a coupon</p></label>-->
<!--                    <input type="text" name="gift-code" id="emailAddr">-->
<!--                    <a id="do_coupon" class="btn btn_primary">Apply Coupon</a>-->
<!--                --><?//}?>
<!--            </div>-->
            <div class="labelDataBox orderSummaryDataBox">
                <label>Order Summary</label>
                <ul class="paymentScreenBagItems">
                    <?php $discount = floatval($this->viewData->ticket->discount); $total = 0; foreach($this->viewData->estimateData as $data){ $total += floatval($data['price']);?>
                        <li>
                            <div class="col bagItemImg">
                                <a href="#" class="row">
                                    <div class="col">
                                        <p><?=$data['name']?></p>
                                        <!--										<p>--><?//=$colorText?><!--</p>-->
                                        <!--										<p>US: --><?//=$sObjUs?><!-- EU: --><?//=$sObjEu?><!--</p>-->
                                        <p>$<?=number_format($data['price'],2)?></p>
                                    </div>
                                    <div class="col">
                                        <img src="<?=$img?>">
                                    </div>
                                </a>
                            </div>
                        </li>
                    <?}?>
                </ul>
                <p><b>Subtotal</b>$<?=number_format($total,2)?></p>
                <p><b>Shipping</b><?=$shipping?'$'.number_format($shipping,2):' Free'?></p>
                <?php if($discount) { $total -= $discount;?>
                <p><b>Discount</b>-$<?=number_format($discount,2)?></p>
                <?php } ?>
                <span class="line_sep"></span>
                <div class="orderSummaryTotal"><span class="triangleBorder"></span><p><b>Estimated Total</b>$<?=number_format($total += $shipping,2)?></p></div>
            </div>
            <div class="labelDataBox">
                <label>Help with your Order</label>
                <p>You can reach us by:</p>
                <p><b>Phone</b><a href="tel:2123333990">212 333 3990</a></p>
            
            </div>
            <div class="labelDataBox">
                <label>Returns Policy</label>
                <p><b>Easy Returns</b>Returns service: All products may be exchanged or returned within 14 days of the invoice date. Thereafter, all sales are final. </p>
            </div>
            <div class="labelDataBox">
                <label>Secure Shopping</label>
                <p>To ensure the safety of your credit card data at all times, we use Secure Socket Layer (SSL) technology and the highest security standards, certified by Verisign™ and Trustwave.</p>
                <img src="<?=FRONT_IMG?>creditCardOptions.png">
            </div>
        </div>
    </div>
</div>
<!--<link href="--><?//=FRONT_CSS.'jquery.datetimepicker.css'?><!--" rel="stylesheet" type="text/css"/>-->
<!--<script src="--><?//=FRONT_JS.'jquery.datetimepicker.full.min.js'?><!--"></script>-->
<!--<script src="--><?//=FRONT_JS.'cc_type.js'?><!--"></script>-->
<script type="text/javascript">
    $(document).ready(function(){
        $('input[name=cc_number]').on('input',function(){
            $('#cc_type').val(creditCardType($(this).val())[0].type);
        });
        function updateACart() {
            var cc_number = $('input[name=cc_number]').val();
            var cc_month = $('input[name=cc_expiration_month]').val();
            var cc_year = $('input[name=cc_expiration_year]').val();
            var cc_ccv = $('input[name=cc_ccv]').val();
            var fn = $('input[name=bill_first_name]').val();
            var ln = $('input[name=bill_last_name]').val();
            var phone = $('input[name=bill_phone]').val();
            var address1 = $('input[name=bill_address]').val();
            var address2 = $('input[name=bill_address2]').val();
            var city = $('input[name=bill_city]').val();
            var state = $('input[name=bill_state]').val();
            var zip = $('input[name=bill_zip]').val();
            var data = {
                cc_number: cc_number,
                cc_expiration_month: cc_month,
                cc_expiration_year: cc_year,
                cc_ccv: cc_ccv,
                bill_first_name: fn,
                bill_last_name: ln,
                phone: phone,
                bill_address: address1,
                bill_address2: address2,
                bill_city: city,
                bill_state: state,
                bill_zip: zip
            };
            $.post('/checkout/logCheckout', data, function () {
                console.log('123');
            });
        }

        $('#creditCardPayBtn').on('click',function(e){
          $(this).css('pointer-events','none');
          setTimeout(function(){
              $('#creditCardPayBtn').css('pointer-events','auto');
          }, 5000);
        });
        $('#creditCardPayBtn').on('click',function(e){
            $('#myContainer').removeAttr('target');
            var data = {
                bill_first_name:$('[name=bill_first_name]').val(),
                bill_last_name:$('[name=bill_last_name]').val(),
                bill_address:$('[name=bill_address]').val(),
                bill_city:$('[name=bill_city]').val(),
                bill_state:$('[name=bill_state]:enabled').val(),
                bill_zip:$('[name=bill_zip]').val(),
                bill_country:$('[name=bill_country]').val()
            };
            var submit = true;
            $.each(data,function(key,value){
                if(!value){
                    submit = false;
                    $('[name=' + key +']:enabled').css('border', '1px solid red');
                } else {
                    $('[name=' + key +']:enabled').css('border', '1px solid #d6d6d6');
                }
            });
            if(submit){
                $('#email_form').submit();
            } else {
                e.preventDefault();
            }
        });

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            $('#cardUserName, #ccnumber, #secCode, #fname,#lname,#checkoutPhone,#checkoutAddress1, #checkoutAddress2, #checkoutCity, #checkoutRegionUS, #checkoutZip,#checkoutCountry').on('blur autocompletechange', function () {
                updateACart()
            });
        } else {
            $(window).on('beforeunload', function () {
                updateACart()
            });
        }

        var opt = $('#isSameAddress');
        opt.on('click',function(){
            if(!opt.find('input').is(':checked')) {
                $('.billingAddressInfo').addClass('hiddenShippingInfo');
                $('input[name=bill_first_name').val('<?=$sessionData['ship_first_name']?>');
                $('input[name=bill_last_name').val('<?=$sessionData['ship_last_name']?>');
                $('input[name=bill_phone').val('<?=$sessionData['phone']?>');
                $('input[name=bill_address').val('<?=$sessionData['ship_address']?>');
                $('input[name=bill_address2').val('<?=$sessionData['ship_address2']?>');
                $('input[name=bill_city').val('<?=$sessionData['ship_city']?>');
                $('input[name=bill_state').val('<?=$sessionData['ship_state']?>');
                $('input[name=bill_zip').val('<?=$sessionData['ship_zip']?>');
            } else {
                $('.billingAddressInfo').removeClass('hiddenShippingInfo');
                $('input[name=bill_first_name').val('');
                $('input[name=bill_last_name').val('');
                $('input[name=bill_phone').val('');
                $('input[name=bill_address').val('');
                $('input[name=bill_address2').val('');
                $('input[name=bill_city').val('');
                $('input[name=bill_state').val('');
                $('input[name=bill_zip').val('');
            }
        });
        $("#do_coupon").on('click',function(){
            var email = $('#emailAddr').val();
            if(email.length>1 && email.indexOf('@')>0){
                $.post('/checkout/addCoupon',{email:email},function(data){
                    if(data.status == 'success'){
                        window.location.replace('<?=$this->emagid->uri?>');
                    } else {
                        alert(data.message);
                    }
                })
            } else {
                alert('Please input a coupon code');
            }
        });
        $('#redeem-gift-card').on('click',function(){
            var code = $('[name=gift-card-code]');
            if(code.val() == null || code.val() == ''){
                alert('Please input your gift card code');
            } else {
                $.post('/checkout/addGiftCard',{'gift-card-code':code.val()},function(data){
                    var json = $.parseJSON(data);
                    if(json.status == 'success'){
                        window.location.replace(json.redirect);
                    } else {
                        alert(json.message);
                    }
                })
            }
        });
        $('#redeem-coupon').on('click',function(){
            var code = $('[name=coupon-code]');
            if(code.val() == null || code.val() == ''){
                alert('Please input a coupon code');
            } else {
                $.post('/coupon/add',{'code':code.val()},function(data){
                    var json = $.parseJSON(data);
                    if(json.status == 'success'){
                        window.location.replace('/checkout/payment');
                    } else {
                        alert(json.message);
                    }
                })
            }
        });
        $('#remove-gift-card').on('click',function(){
            $.post('/checkout/removeGiftCard',{},function(data){
                var json = $.parseJSON(data);
                if(json.status == 'success'){
                    window.location.replace('/checkout/payment');
                }
            })
        });
        $('#remove-coupon').on('click',function(){
            $.post('/coupon/remove',{},function(data){
                var json = $.parseJSON(data);
                if(json.status == 'success'){
                    window.location.replace('/checkout/payment');
                }
            })
        });

        //If gift card completes the purchase, disable CC and PayPal
        <?if($model->giftCardFullCharge){?>
        $('#paypalPayment').hide();
        $('.title').html('Paid with Gift Card');
        $('.creditCardPaymentFormSection').hide();
        $('[name=card_name]').prop('disabled',true);
        $('[name=cc_number]').prop('disabled',true);
        $('[name=cc_expiration_month]').prop('disabled',true);
        $('[name=cc_expiration_year]').prop('disabled',true);
        $('[name=cc_ccv]').prop('disabled',true);
        $('[name=paypal]').prop('disabled',true);
        <?}?>
        $('select[name=bill_country]').on('change',function(){
            var us = $('#us_states');
            var foreign = $('#foreign_states');
            if($(this).find(':selected').val() === 'United States'){
                us.show();
                us.find('select').prop('disabled',false);

                foreign.hide();
                foreign.find('input').prop('disabled',true);
            } else {
                us.hide();
                us.find('select').prop('disabled',true);

                foreign.show();
                foreign.find('input').prop('disabled',false);
            }
        });
    })
</script>
<script>
    //	$("#paypalPayBtnClicker").on('click', function(e){
    //		$('#myContainer').submit();
    //	});

    window.paypalCheckoutReady = function() {
        paypal.checkout.setup('<?=PAYPAL_CLIENT_ID?>', {
            environment: 'live',
            button: 'paypalPayBtnClicker',
            click: function(){
                paypal.checkout.initXO();
                var action = $.post('/set-express-checkout');
                action.done (function(data){
                    paypal.checkout.startFlow(data.token);
                });
                action.fail(function(){
                    paypal.checkout.closeFlow();
                })
            },
            condition: function(){
                var data = {
                    bill_first_name:$('[name=bill_first_name]').val(),
                    bill_last_name:$('[name=bill_last_name]').val(),
                    bill_address:$('[name=bill_address]').val(),
                    bill_city:$('[name=bill_city]').val(),
                    bill_state:$('[name=bill_state]:enabled').val(),
                    bill_zip:$('[name=bill_zip]').val(),
                    bill_country:$('[name=bill_country]').val()
                };
                var submit = true;
                $.each(data,function(key,value){
                    if(!value){
                        submit = false;
                        $('[name=' + key +']:enabled').css('border', '1px solid red');
                    } else {
                        $('[name=' + key +']:enabled').css('border', '1px solid #d6d6d6');
                    }
                });
                return submit;
            }
        });
    };
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>