<div class="pageWrapper subcategoryPageWrapper pageLeftNavSidebar searchResultsPageWrapper">
        <div class="breadcrumbs">
            <div class="row">
                <div class="otherSubCats row" style="display:none!important;visibility:hidden!important;opacity:0;pointer-events: none!important;">
                    <?php $sql = "select * from subnav where active = 1 and category_id = {$model->mCategory->id} order by column_num ASC, display_order ASC;";
                    $subCat = \Model\Subnav::getList(['sql'=>"$sql"]); $list = [];?>
                    <?foreach($subCat as $category){?>
                    <?$category = \Model\Category::getItem($category->sub_category_id);
                    if(($category->id)!=($model->category->id)){?>
                    <div class="col">
                        
                        <a href="<?=SITE_URL."collections/{$model->mCategory->slug}/{$category->slug}"?>">
                            <p class="as_l"><?=$category->name?></p>
                        </a>
                    </div>
                    <?}}?>
                </div>
                <div class="right_float inventoryFilterBar row">
                    <div class="filtersBarSection col">
                        <? require_once('templates/modernvice/filterBar.php'); ?>
                    </div>  
                    <div class="col">              
                        <label class="lyon_r dg"><em>Sort by</em></label>
                        <div class="dropdown_ui">
                            <?$arr = ['Newest','Best Sellers','Price: High to Low','Price: Low to High']?>
                            <div class="visible"><p class="as_r sort_by_main" data-value="<?=isset($_GET['sort_by']) && $_GET['sort_by'] ? $_GET['sort_by']: 0?>"><?=isset($model->sortBy) && $model->sortBy ? $arr[$model->sortBy]: 'Newest'?></p><icon></icon></div>
                            <div class="hidden">
                                <?foreach($arr as $i=>$a){
                                    $active = isset($_GET['sort_by']) && $_GET['sort_by'] == $i? 'selected': '';
                                    ?>
                                    <div class="option <?=$active?>"><p class="as_l sort_by_group" data-value="<?=$i?>"><?=$a?></p></div>
                                <?}?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
        <div class="noGutter_content_width product_inventory_gridui mainPageInventoryUI">
        <div class="row row_of_4 product_grid_ui">
            <?foreach($model->products as $product){
                $prodAttr = [];
                foreach(json_decode($product->color,true) as $value){
                    $st = \Model\Product_Attributes::getStatusById($product->id,$value);
                    if($st == null || $st->value != 3){
                        $prodAttr[] = \Model\Color::getItem($value);
                    }
                }
//                $prodAttr = \Model\Product_Attributes::getColorList($product->id,0);
            ?>
            <div class="col productGridItem">
                <div id="showQuickView" class="show_mdl" data-id="<?=$product->id?>" data-mdl_name="quickView">
                    <h5 class="as_m"><span><i></i><i></i></span>Quick View</h5>
                </div>

                <? if(count($prodAttr)>=5){?>
                <div class="productGridItemSwatches productGridItemSwatchesSlider">
                <?}else{?>
                <div class="productGridItemSwatches">
                <?}?>
                    <?$default_color = null;
                    $colorList = array_map(function($item){return $item->id;},$model->searchColors);
                    foreach($prodAttr as $pa){
                        if(in_array($pa->id,$colorList)){
                            $default_color = $pa->id;
                        }?>
                        <!--<div class="media colorSwatch" style="background-image: url(<?/*=$pa->swatch()*/?>)"></div>-->
                        <div class="productGridItemSwatchWrapper">
                            <img src="<?=$pa->swatch()?>" class="productGridItemSwatch" data-color_id="<?=$pa->id?>" data-pro_id="<?=$product->id?>"/>
                        </div>
                    <?}?>
                </div>
                <a href="<?=SITE_URL."products/$product->slug"?><?=$default_color?'?col='.$default_color:''?>">
                    <div class="mediaWrapper">
                        <?$featuredImageParam = $model->category == 'all' ? null: $model->category->id?>
                        <div class="media" style="background-image:url(<?=UPLOAD_URL.'products/'.$product->featuredImage($featuredImageParam,$default_color)?>)"></div>
                    </div>
                    <div class="dataWrapper">
                        <h4 class="product_name"><?=$product->name?></h4><?=$product->isDiscounted() ? '<small>On Sale</small>': ''?>
                        <?if($product->msrp > 0.0){
                            $percent = round(($product->basePrice() - $product->msrp()) * 100 / $product->basePrice())?>
                            <h4 class="product_price">
                                <span class="full_price">
                                    <span class="currency">$</span><?=number_format($product->basePrice(),2)?>
                                </span>
                                <span class="value">$<?=number_format($product->msrp(),2)?></span>
                            </h4>
                        <? } else if($product->isDiscounted()){?>
                            <?$percent = round(($product->basePrice() - $product->price()) * 100 / $product->basePrice()) ?>
                            <h4 class="product_price">
                                <span class="full_price">
                                    <span class="currency">$</span><?= number_format($product->basePrice(),2) ?>
                                </span>
                                <span class="markdown">-<?= $percent ?>%</span>
                                <span class="value">$<?= number_format($product->price(),2) ?></span>
                            </h4>
                        <?} else {?>
                            <h4 class="product_price"><span class="value">$<?=number_format($product->price(),2)?></span></h4>
                        <?}?>
                    </div>
                </a>
            </div>
            <?}?>
        </div>

        <div id="loading-spinner">
            <div class="hanger"></div>
            <div class="discoball">
                <img src="<?=FRONT_IMG?>disco_ball.png" alt="">
            </div>
        </div>
    </div>
    
</div>
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>subcategory.css">
<script>
    $(document).ready(function(){
        /*Let event handler attach before triggering click*/
        <?if(isset($model->filterActive) && $model->filterActive){?>
            $('div[data-filter_optiontype=<?=$model->filterActive?>]').trigger('click');
        <?}?>
        $(document).on('mouseover', '.productGridItemSwatch', function (e) {
            var product_id = $(this).attr('data-pro_id');
            var color_id = $(this).attr('data-color_id');
            var self = $(this);
            $.post('/category/buildVariantImage', {product_id: product_id, color_id: color_id}, function (data) {
                var pgi = self.closest('.productGridItem');
                pgi.find('a').attr('href',data.url);
                if (data.status == 'success') {
                    pgi.find('.media').css('background-image', 'url(' + data.image + ')');
                    if(data.soldOut){
                        pgi.addClass('soldOutGridItem');
                    } else {
                        pgi.removeClass('soldOutGridItem');
                    }
                    if(data.isDiscounted){
                        var html = '<span class="full_price"> <span class="currency">$</span>'+data.basePrice+'</span><span class="markdown">-'+data.percent
                            + '%</span><span class="value">$'+data.price+'</span>';
                        pgi.find('small').show();
                        pgi.find('.product_price').html(html);
                    }else{
                        pgi.find('small').hide();
                        pgi.find('.product_price').html('$' + data.price);
                    }
                }
            });
        });
        var limit = <?=$model->limit?>;
        var offset = <?=$model->offset + $model->limit?>;
        var totalProductCount = <?=$model->productsCount?>;


        var offsetScroll = true;
        $(window).scroll(function() {


            if (($(window).scrollTop() >= ($(document).height() - $(window).height() - 300)) && (offsetScroll == true)) {
                offsetScroll = false;
                console.log('activated1');
                if (offset < totalProductCount) {
                    console.log(offsetScroll);

                    var mCategory = '<?=$model->mCategory ? $model->mCategory->id : ''?>';
                    var category_id = '<?=$model->category != 'all' ? $model->category->id : 'all'?>';
                    $.post('/category/buildCategoryItems<?=isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != '' ? '?' . $_SERVER['QUERY_STRING'] : ''?>', {
                        mCategory: mCategory,
                        category_id: category_id,
                        limit: limit,
                        offset: offset
                    }, function (data) {
                        $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').addClass("loadingProcessActive");
                        $("#loading-spinner").addClass("loading_spinner_active").delay(800).queue(function (appendData) {
                            $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').removeClass("loadingProcessActive");
                            $("#loading-spinner").removeClass("loading_spinner_active");
                            $('.mainPageInventoryUI.product_inventory_gridui .product_grid_ui').append(data.itemHtml);
                            offset += limit;
                            appendData();

                            if (data.status === "success") {
                                offsetScroll = true;
                                $('.productGridItemSwatchesSlider').flickity({
                                    // options
                                    cellAlign: 'left',
                                    adaptiveHeight: true,
                                    prevNextButtons: true,
                                    pageDots: false,
                                    groupCells: 3,
                                    contain: true,
                                    arrowShape: {
                                        x0: 10,
                                        x1: 65, y1: 50,
                                        x2: 70, y2: 45,
                                        x3: 20
                                    }
                                });
                            }
                        });

                    });
                }
            }
        })
    })
</script>
