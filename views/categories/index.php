<?php 
$categories = $model->display_categories;
if($model->isPreorder){
    $categories = [$model->main_category];
}
?>
<div class="pageWrapper categoriesPageWrapper">

    <div class="breadcrumbsHeader">
        <? if ($model->category->banner != "") {
//            $img_path = UPLOAD_URL . 'categories/' . $model->category->banner;
            $img_path = 'https://dji-emagid.s3.amazonaws.com/' . $model->category->banner; ?>
            <section class='category_banner' style="background-image:url(<?=$img_path?>)"></section>
        <? } ?>
<!--        <section class='category_banner'></section>-->

        <div class="mainContentWidth" id="productScrollerNew">
            <h1 class="dji-text dji-blue"><?=$model->category->name?></h1>
            <h4 class="gothamLight">Let Your Creativity Fly</h4>
        </div>

    <div class="imageTiling categoryProductsTiling">

        <?foreach ($categories as $key => $category){
            $product = \Model\Category::getDefaultProduct($category->id);?>
            <div class="row row_of_1">
                <div class="col contentCol">
                    <div class="comparisonToggleWrapper" hidden>
                        <a class="comparisonToggle">
                            <icon class="compareToggleIcon">
                                <span></span>
                                <span></span>
                            </icon>
                            <p>Compare</p>
                        </a>
                        <div class="comparisonLinkWrapper hidden">
                            <a href="/comparisons">
                                <p>View Comparisons</p><span class="count">0</span>
                            </a>
                        </div>
                    </div>
                    <div class="">
                        <a class="productImgTxtLink">
                            <div class="showcaseImage">
                                <img src="<?=($product)?$product->featuredImage():UPLOAD_URL . 'categories/' . $category->banner?>"/>
                            </div>
                            <h6 class="dji-text" id="<?=$category->slug?>"><?=$category->title?: $category->name.' Series'?></h6>
                        </a>
                    </div>
                    <div class="productModel">
                        <? if(stristr($category->name,'accessories') !== false || stristr($category->title,'accessories') !== false ) {
                            $cType = 0;
                        } else {
                            $cType = 1;
                        }?>
                        <?foreach ($category->getProductsByType($model->user,$cType) as $product){ ?>
                            <div class="productModelSolo">
                                <a href="<?='/products/'.$product->slug?>">
                                    <img src="<?=$product->featuredImage()?>" alt="<?=$product->image_alt?>"/>
                                    <h6 class="gothamLight linkColor"><?=$product->name?></h6>                                
                                </a>
                                <?php $defaultPrice = $product->basePrice(null,$model->user);?>
                                <? if($model->wholesale) {?>
                                    <h6 class="gothamLight" style='font-size: 17px;'>$<?=$product->basePrice(null,$model->user)?></h6>
                                <? } else {?>
                                    <? if($product->getPrice() != $defaultPrice) {?>
                                        <h6 style="text-decoration: line-through; text-align: center; font-size: 14px;">$<?=$product->price?></h6>
                                        <h6 class="gothamLight" style='font-size: 17px;'>$<?=$product->msrp?></h6>
                                    <? } else {?>
                                        <h6 class="gothamLight" style='font-size: 17px;'>$<?=$product->price?></h6>
                                    <? } ?>
                                <? } ?>
                                <!-- If there is a message show this -->
                                <h6 class="gothamLight"><span class="red_font"><?=$product->deal_msg?></span></h6>
                                
                            
                        
                                        <div class="productSecondaryActions row">
                    <div class="col left_float quantity numbersRow" style="display:none;">
                        <input type="text" disabled name="product-quantity" id="product-quantity" value="1">
                        <div class="quantToggleButtons">
                            <div class="inc button">
                                <span>
                                </span>
                                <span>
                                </span>
                            </div>
                            <div class="dec button disabled">
                                <span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col right_float mainActions">
                        <?php if($product->preorder){ ?>
                        <a href="/checkout/preorder" class="solidBlueBtn trueBlue addToCart mainProd" data-product_id="<?=$product->id?>">
                            <p>Preorder</p>
                        </a>
                        <a class="solidBlueBtn trueBlue goCheckout checkout" href="/checkout/preorder">
                            <p>Preorder</p>
                        </a>
                        <a class="view_more" href="<?='/products/'.$product->slug?>">
                            <p><span>View Details</span></p>
                        </a>
                        <?php } else { ?>
                        <a href="/cart" class="solidBlueBtn trueBlue addToCart mainProd" data-product_id="<?=$product->id?>">
                            <p>Add to Cart</p>
                        </a>
                        <a class="solidBlueBtn trueBlue goCheckout checkout" href="/cart">
                            <p>Add to Cart</p>
                        </a>
                        <a class="view_more" href="<?='/products/'.$product->slug?>">
                            <p><span>View Details</span></p>
                        </a>
                        <?php } ?>

                        <!-- SALES ICONS -->
                        <? if($product->sale_icon) {?>
                            <img class='sale_icon' src="<?=FRONT_IMG?><?=\Model\Product::$sale_icons[$product->sale_icon]?>.png">
                        <? } ?>

                    </div>
                </div>
                        
                </div>
                        <?}?>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
    </div>


</div>

<style type="text/css">
    .hidden.shopHiddenDropdown {
        opacity: 1;
        max-height: 113px;
        -webkit-transform: translateY(-10px);
        transform: translateY(0px);
        width: 100% !important;
    }
    .hidden.shopHiddenDropdown > div.exploreProducts {
        margin-left: auto !important;
        margin-right: auto !important;
    }
    h6.dji-text {
        text-transform: uppercase;
        font-size: 36px;
        color:#8e8e8e;
    }
</style>

<script type="text/javascript">
  $(document).ready(function(){
    $("#sticker").sticky({topSpacing:0});
  });
</script>



