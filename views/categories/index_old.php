<? $categories = $model->display_categories;?>
<div class="pageWrapper categoriesPageWrapper">

    <div class="breadcrumbsHeader">
        <div class="mainContentWidth">
            <h1 class="dji-text dji-blue"><?=$model->category->name?></h1>
            <div class="categoryBreadcrumbs">
                <ul class="horizontalList">
                    <li>
                        <a><p class="text gothamBook">Home</p></a>
                    </li>
                    <li>
                        <a><p class="text gothamBook">All Products</p></a>
                    </li>
                    <li>
                        <p><?=$model->category->name?></p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="imageTiling categoryProductsTiling">
        <?foreach ($categories as $key => $category){
            $product = \Model\Category::getDefaultProduct($category->id);?>
            <div class="row row_of_2">
                <div class="col mediaCol">
                    <div class="media" style="background-image:url('<?=UPLOAD_URL . 'categories/' . $category->banner?>')">
                    </div>
                    <p class="caption">Shot on <span><?=$category->name?></span></p>
                </div>
                <div class="col contentCol">
                    <div class="comparisonToggleWrapper" hidden>
                        <a class="comparisonToggle">
                            <icon class="compareToggleIcon">
                                <span></span>
                                <span></span>
                            </icon>
                            <p>Compare</p>
                        </a>
                        <div class="comparisonLinkWrapper hidden">
                            <a href="/comparisons">
                                <p>View Comparisons</p><span class="count">0</span>
                            </a>
                        </div>
                    </div>
                    <div class="absTransCenter">
                        <a class="productImgTxtLink">
                            <img src="<?=($product)?$product->featuredImage():UPLOAD_URL . 'categories/' . $category->banner?>" alt="<?=$product->image_alt?>"/>
                            <h6 class="gothamLight linkColor"><?=$category->name?> Series</h6>
                        </a>
                        <div class="categoryProductDescription">
                            <p><?=$category->description?></p>
                        </div>
                        <div class="btnRow">
                            <a href="<?='/categories/'.$model->category->slug.'/'.$category->slug?>" class="underlineBtn underlineBtn-blue underlineBtn-blue-black">
                                <p>Learn More</p>
                            </a>
<!--                            <a class="solidBlueBtn" href="--><?//='/categories/'.$model->category->slug.'/'.$category->slug?><!--">Buy Now</a>-->
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</div>
