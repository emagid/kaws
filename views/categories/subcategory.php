<div class="pageWrapper categoriesPageWrapper">

    <div class="breadcrumbsHeader">
        <div class="mainContentWidth">
            <h1 class="dji-text dji-blue">Phantom</h1>
            <div class="categoryBreadcrumbs">
                <ul class="horizontalList">
                    <li>
                        <a><p class="text gothamBook">Home</p></a>
                    </li>
                    <li>
                        <a><p class="text gothamBook">All Products</p></a>
                    </li>
                    <li>
                        <a><p class="text gothamBook">Drones</p></a>
                    </li>
                    <li>
                        <p>Phantom</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="imageTiling categoryProductsTiling">
        <div class="row row_of_2">
            <div class="col mediaCol">
                <div class="media" style="background-image:url('<?=FRONT_IMG?>category1Hero.jpg')">
                </div>
                <p class="caption">Shot on <span>Phantom 4 Pro</span></p>
            </div>
            <div class="col contentCol">
                <div class="absTransCenter">
                    <a class="productImgTxtLink">
                       <img src="<?=FRONT_IMG?>productsCategory1.png"/>
                       <h6 class="gothamLight linkColor">Phantom 4 Pro</h6>
                    </a>
                    <div class="categoryProductDescription">
                        <p>Equipped with a 1-inch sensor, the Phantom 4 Pro camera shoots video at up to 4K 60fps and 20-megapixel stills. It has 4 directions of obstacle avoidance, a 30-minute flight time, and 4.1mi (7km) transmission range.</p>
                    </div>
                    <div class="btnRow">
                        <a class="underlineBtn underlineBtn-blue underlineBtn-blue-black">
                            <p>Learn More</p>
                        </a>  
                        <a class="solidBlueBtn">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row_of_2">
            <div class="col mediaCol">
                <div class="media" style="background-image:url('<?=FRONT_IMG?>category2Hero.jpg')">
                </div>
                <p class="caption">Shot on <span>Phantom 3 Standard</span></p>
            </div>
            <div class="col contentCol">
                <div class="absTransCenter">
                    <a class="productImgTxtLink">
                       <img src="<?=FRONT_IMG?>productsCategory2.png"/>
                       <h6 class="gothamLight linkColor">Phantom 3 Standard</h6>
                    </a>
                    <div class="categoryProductDescription">
                        <p>Safe, easy, and fun to fly, the Phantom 3 Standard makes it possible for everyone to reach for the sky.</p>
                    </div>
                    <div class="btnRow">
                        <a class="underlineBtn underlineBtn-blue underlineBtn-blue-black">
                            <p>Learn More</p>
                        </a>  
                        <a class="solidBlueBtn">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row_of_2">
            <div class="col mediaCol">
                <div class="media" style="background-image:url('<?=FRONT_IMG?>category3Hero.jpg')">
                </div>
                <p class="caption">Shot on <span>Phantom 3 Advanced</span></p>
            </div>
            <div class="col contentCol">
                <div class="absTransCenter">
                    <a class="productImgTxtLink">
                       <img src="<?=FRONT_IMG?>productsCategory3.png"/>
                       <h6 class="gothamLight linkColor">Phantom 3 Advanced</h6>
                    </a>
                    <div class="categoryProductDescription">
                        <p>Take to the sky and capture your world in beautiful 2.7K HD. The Phantom 3 is an easy to use, fully integrated package with an intelligent system that helps you fly.</p>
                    </div>
                    <div class="btnRow">
                        <a class="underlineBtn underlineBtn-blue underlineBtn-blue-black">
                            <p>Learn More</p>
                        </a>  
                        <a class="solidBlueBtn">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row_of_2">
            <div class="col mediaCol">
                <div class="media" style="background-image:url('<?=FRONT_IMG?>category4Hero.jpg')">
                </div>
                <p class="caption">Shot on <span>Phantom FC40</span></p>
            </div>
            <div class="col contentCol">
                <div class="absTransCenter">
                    <a class="productImgTxtLink">
                       <img src="<?=FRONT_IMG?>productsCategory4.png"/>
                       <h6 class="gothamLight linkColor">Phantom FC40</h6>
                    </a>
                    <div class="categoryProductDescription">
                        <p>The same ease of use as the Phantom, with its own smart FC40 camera.</p>
                    </div>
                    <div class="btnRow">
                        <a class="underlineBtn underlineBtn-blue underlineBtn-blue-black">
                            <p>Learn More</p>
                        </a>  
                        <a class="solidBlueBtn">Buy Now</a>
                    </div>
                </div>
            </div>
        </div>                        
    </div>

</div>