<div class="pageWrapper categoriesPageWrapper">

    <div class="breadcrumbsHeader">
        <div class="mainContentWidth">
            <h1 class="dji-text dji-blue"><?=$model->category->name?></h1>
            <div class="categoryBreadcrumbs">
                <ul class="horizontalList">
                    <li>
                        <a><p class="text gothamBook">Home</p></a>
                    </li>
                    <li>
                        <a><p class="text gothamBook">All Products</p></a>
                    </li>
                    <li>
                        <a><p class="text gothamBook"><?=$model->parentCat->name?> Series</p></a>
                    </li>
                    <li>
                        <a><p><?=$model->category->name?> Series</p></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="imageTiling categoryProductsTiling">
        <?foreach($model->products as $product){?>
            <div class="row row_of_2">
                <div class="col mediaCol">
                    <div class="media" style="background-image:url('<?=UPLOAD_URL.'products/'.$product->featuredImage()?>')">
                    </div>
                    <p class="caption">Shot on <span><?=$product->name?></span></p>
                </div>
                <div class="col contentCol">
                    <div class="comparisonToggleWrapper">
                        <a class="comparisonToggle <?=in_array($product->id,$_SESSION['comparison']) ? 'activeComparison': ''?>" data-id="<?=$product->id?>">
                            <icon class="compareToggleIcon">
                                <span></span>
                                <span></span>
                            </icon>
                            <p>Compare</p>
                        </a>
                        <div class="comparisonLinkWrapper <?=in_array($product->id,$_SESSION['comparison']) ? '': 'hidden'?>">
                            <a href="/comparisons">
                                <p>View Comparisons</p><span class="count">0</span>
                            </a>
                        </div>
                    </div>
                    <div class="absTransCenter">
                        <a class="productImgTxtLink">
                            <img src="<?=$product->featuredImage()?>"/>
                            <h6 class="gothamLight linkColor"><?=$product->name?></h6>
                        </a>
                        <div class="categoryProductDescription">
                            <p><?=$product->description?></p>
                        </div>
                        <div class="btnRow">
                            <a href="<?='/products/'.$product->slug?>" class="underlineBtn underlineBtn-blue underlineBtn-blue-black">
                                <p>Learn More</p>
                            </a>
                            <a class="solidBlueBtn" href="/products/<?=$product->slug?>">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
        <?foreach($model->relative_products as $product){?>
            <div class="row row_of_2">
                <div class="col mediaCol">
                    <div class="media" style="background-image:url('<?=UPLOAD_URL.'products/'.$product->featuredImage()?>')">
                    </div>
                    <p class="caption">Shot on <span><?=$product->name?></span></p>
                </div>
                <div class="col contentCol">
                    <div class="absTransCenter">
                        <a class="productImgTxtLink">
                            <img src="<?=$product->featuredImage()?>" alt="<?=$product->image_alt?>"/>
                            <h6 class="gothamLight linkColor"><?=$product->name?></h6>
                        </a>
                        <div class="categoryProductDescription">
                            <p><?=$product->description?></p>
                        </div>
                        <div class="btnRow">
                            <a href="<?='/accessories/'.$product->slug?>" class="underlineBtn underlineBtn-blue underlineBtn-blue-black">
                                <p>Learn More</p>
                            </a>
                            <a class="solidBlueBtn" href="/accessories/<?=$product->slug?>">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</div>