<div class="pageWrapper categoriesPageWrapper">

    <div class="breadcrumbsHeader">
        <div class="mainContentWidth">
            <h1 class="dji-text dji-blue">Parts</h1>
            <div class="categoryBreadcrumbs">
                <ul class="horizontalList">
                    <li>
                        <a><p class="text gothamBook">Home</p></a>
                    </li>
                    <li>
                        <a><p class="text gothamBook">All Products</p></a>
                    </li>
                    <li>
                        <a><p class="text gothamBook">Series</p></a>
                    </li>
                    <li>
                        <a><p>Series</p></a>
                    </li>
                </ul>
            </div>
            <div class="parts_search">
                <input type="text" name="partSearch" placeholder="Type to search..">
            </div>
            
        </div>
        
    </div>

    <div class="imageTiling categoryProductsTiling">
        <ul>
        <?
        foreach($model->products as $product){?>
            <div class="row row_of_2 part" data-name="<?=$product->name?>">
<!--                 <div class="col mediaCol">
                    <div class="media" style="background-image:url('<//?=UPLOAD_URL.'products/'.$product->featuredImage()?>')">
                    </div>
                    <p class="caption">Shot on <span><//?=$product->name?></span></p>
                </div> -->
                <div class="col contentCol">
<!--                     <div class="comparisonToggleWrapper">
                        <a class="comparisonToggle <//?=in_array($product->product_id,$_SESSION['comparison']) ? 'activeComparison': ''?>" data-id="<?=$product->product_id?>">
                            <icon class="compareToggleIcon">
                                <span></span>
                                <span></span>
                            </icon>
                            <p>Compare</p>
                        </a>
                        <div class="comparisonLinkWrapper <//?=in_array($product->product_id,$_SESSION['comparison']) ? '': 'hidden'?>">
                            <a href="/comparisons">
                                <p>View Comparisons</p><span class="count">0</span>
                            </a>
                        </div>
                    </div> -->
                    <div class="absTransCenter">
                        <div>
                            <a class="productImgTxtLink">
                                <!-- <img src="<//?=$product->featuredImage()?>"/> -->
                                <h6 class="gothamLight linkColor"><?=$product->name?></h6>
                                <!-- <a href="<//?='/products/'.$product->slug?>" class="underlineBtn underlineBtn-blue underlineBtn-blue-black">
                                    <p>Learn More</p>
                                </a> -->
                                                    
                            <?$img_path = $product->featuredImage()?>
                            <img src="<?php echo $img_path; ?>" width="50"/>
                            </a>
                            
                        </div>
                        <div class="btnRow">
                            <p class="price">USD $<?=number_format($product->price,2)?></p>
                                                <div class="col left_float quantity numbersRow">
                        <input type="text" disabled name="product-quantity" id="product-quantity" value="1">
                        <div class="quantToggleButtons">
                            <div class="inc button">
                                <span>
                                </span>
                                <span>
                                </span>
                            </div>
                            <div class="dec button disabled">
                                <span>
                                </span>
                            </div>
                        </div>
                    </div>


                    <input type="text" disabled hidden name="product-quantity" id="product-quantity" value="1">
                    <?php if($model->product->preorder){ ?>
                        <a class="solidBlueBtn hollowBlueBtn addToCart accessProd" data-product_id="<?=$product->id?>" >
                            <p>Preorder</p>
                        </a>
                    <?php } else { ?>
                        <a class="solidBlueBtn hollowBlueBtn addToCart accessProd" data-product_id="<?=$product->id?>" >
                            <p>Add to Cart</p>
                        </a>
                    <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        <?}?>
        </ul>
    </div>
</div>
<script>
    $(".button").on("click", function() {

        var $button = $(this);
        var oldValue = $button.closest('.numbersRow').find("input").val();

        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        $button.closest('.numbersRow').find("input").val(newVal);
        if(newVal==1){
            $('.button.dec').addClass('disabled');
        }else{
            $('.button.dec').removeClass('disabled');
        }
    });
    $("input[type='text'][name='partSearch']").on('input',function(){
        if($(this).val() == '') {
            $('.row_of_2.part[hidden]').removeAttr('hidden');
        } else {
            var queryString = this.value.toLowerCase();
            $('.row_of_2.part').each(function(){
               if( this.getAttribute('data-name').toLowerCase().indexOf(queryString) >= 0 ){
                   this.removeAttribute('hidden');
               } else {
                   this.setAttribute('hidden','');
               }
            });
        }
    });
</script>