<?php
$page = $model->page;
?>
<div class="pageWrapper faqPageWrapper cmsPageWrapper">
    <h1 class="dji-text dji-blue"><?= $page->title; ?></h1>
    <div class="row row_of_3 page_ui_wrapper">
        <?= $page->description; ?>
    </div>
</div>

