<script>
    $('.mainHeaderWrapper').hide();
</script>
<?php
set_time_limit(0);

$filePaths = [
    'orders' => __DIR__.'/data.csv'
];

$displace_id = [];
$displace_hist = [];

$arrResult = [];
$head = false;
foreach($filePaths as $key => $filePath) {
    $handle = fopen($filePath, 'r');
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            $arrResult[$key][] = $data;
        }
        fclose($handle);
        $head = false;
    }
}
global $emagid;
$db = $emagid->getDb();

$headers = $arrResult['orders'][0];

$myfile = fopen("output.txt", "w") or die("Unable to open file!");
for($i = 1; $i < count($arrResult['orders']); $i++) {
//    if($i > 5) continue;
    $data = array_combine($headers,$arrResult['orders'][$i]);
    $user =  \Model\User::getItem(null,['where'=>"lower(email) LIKE '".strtolower(trim($data['email']))."'"]);
    $name = explode(' ',$data['name'],2);
    if(!$user){
        $user = new \Model\User(['email'=>$data['email'],
                'first_name'=>$name[0],
                'last_name'=>$name[1],
                'phone'=>$data['phone'],
                'password'=>'********']);
    } else {
        $user->first_name = $name[0];
        $user->last_name = $name[1];
    }
    //$user->save();
    $_address = explode(',',$data['address']);
    if(count($_address) == 6) {
        $address = (object)[
            'name'    => trim($_address[0]),
            'address' => trim($_address[1]),
            'address2'=> NULL,
            'city'    => trim($_address[2]),
            'state'   => trim($_address[3]),
            'zip'     => trim($_address[4]),
            'country' => trim($_address[5]),
        ];
    } else {
        $address = (object)[
            'name'    => trim($_address[0]),
            'address' => trim($_address[1]),
            'address2'=> trim($_address[2]),
            'city'    => trim($_address[3]),
            'state'   => trim($_address[4]),
            'zip'     => trim($_address[5]),
            'country' => trim($_address[6]),
        ];

    }
    //var_dump($address);
    $order = new \Model\Order();

    $order->id                = str_replace(',','',str_replace('$','',trim($data['order'])));
    $order->subtotal          = str_replace(',','',str_replace('$','',trim($data['subtotal'])));
    $order->shipping_charge   = str_replace(',','',str_replace('$','',trim($data['shipping'])));
    $order->tax               = str_replace(',','',str_replace('$','',trim($data['tax'])));
    $order->total             = str_replace(',','',str_replace('$','',trim($data['total'])));
    $order->phone             = trim($user->phone);
    $order->user_id           = $user->id;


    $order->bill_first_name = $order->ship_first_name = $user->first_name;
    $order->bill_last_name  = $order->ship_last_name  = $user->last_name;
    $order->bill_address    = $order->ship_address    = $address->address;
    $order->bill_address2   = $order->ship_address2   = $address->address2;
    $order->bill_city       = $order->ship_city       = $address->city;
    $order->bill_state      = $order->ship_state      = $address->state;
    $order->bill_country    = $order->ship_country    = $address->country;
    $order->bill_zip        = $order->ship_zip        = $address->zip;
    $order->email = $data['email'];
    $sql = "UPDATE public.\"order\" SET (bill_first_name, bill_last_name, bill_address, bill_address2, bill_city, bill_state, bill_country, bill_zip, ship_first_name, ship_last_name, ship_address, ship_address2, ship_city, ship_state, ship_country, ship_zip, subtotal, shipping_cost, tax, total, phone, user_id) ".
	"= ('$name[0]', '$name[1]', '$address->address', '$address->address2', '$address->city', '$address->state', '$address->country', '$address->zip', '$name[0]', '$name[1]', '$address->address', '$address->address2', '$address->city', '$address->state', '$address->country', '$address->zip', $order->subtotal,$order->shipping_charge,$order->tax,$order->total,'$order->phone',$order->user_id) ".
	"WHERE id = $order->id;";
    echo ($sql);
    echo ("\n");
    fwrite($myfile, $sql);

//    $results = $db->execute($sql);
//    $order = \Model\Order::getItem(null,['orderBy'=>'id DESC']);
    continue;
    foreach (json_decode($data['product'],true) as $deets){
        $_product = $deets['product'];
        $product = \Model\Product::getItem(null,['where'=>"name LIKE '{$_product['name']}'"]);
        if(!$product){
            $product = new \Model\Product();
            $product->name  = $_product['name'];
            $product->slug = $product->name;
            $product->slug =  preg_replace ( "/[^\w-]/" , '-' , $product->slug);
            $product->slug =  preg_replace ( "/[-]+/" , '-' , $product->slug);
            $product->slug =  strtolower($product->slug);
            $product->slug =  uniqueSlug('\Model\Product',$product->slug);
            $product->msrp  = str_replace('$','',$deets['price']);
            $product->price  = str_replace('$','',$deets['price']);
            if($product->save()){
                //var_dump('Product Saved');
            } else {
                var_dump($product->errors);
            }
        }
        $order_product = new \Model\Order_Product();
        $order_product->product_id = $product->id;
        $order_product->order_id   = $order->id;
        $order_product->details    = json_encode(['color'=>strtoupper($_product['color'])]);
        $order_product->unit_price = str_replace('$','',$deets['price']);
        if($order_product->save()){
            //var_dump('order_product saved');
        } else {
            var_dump($order_product->errors);
        }
    }


}

fclose($myfile);
function uniqueSlug($model, $slug, $count = 0){
    $list = $model::getList(['where'=>"slug = '".$slug."' "]);
    if (isset($list) && count($list) > 0){
        return uniqueSlug($model, $slug.'-'.++$count, $count);
    } else {
        return $slug;
    }
}