<?php
require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class checkoutController extends siteController {
	
    public function index(Array $params = [])
    {

//        $this->configs['Meta Title'] = "Modern Vice - Checkout";
//        $this->loadView($this->viewData);
        redirect('/');
    }

    public function email(Array $params = [])
    {
        $this->emptyCartValidation();
        $this->configs['Meta Title'] = "Dji - Checkout - Email";
        if($this->viewData->user){
            $_SESSION['customerDetails'] = json_encode(['email'=>$this->viewData->user->email]);
            redirect('/checkout/payment');
        }
        $this->loadView($this->viewData);
    }

    function email_post($params = []){
        if(isset($_SESSION['lotto_email']) && $_SESSION['lotto_email'] != $_POST['email']){
            $this->toJson(['status'=>false,'msg'=>'This email does not match the one used to sign up for the lottery']);
        }
        if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) == false){
            $n = new \Notification\ErrorHandler("Please enter a valid email address");
            $_SESSION['notification'] = serialize($n);
            if(isset($_POST['onepage'])){
                $this->toJson(['status'=>false]);
            } else {
                redirect('/checkout/email');
            }
        }
        if(isset($_SESSION['customerDetails']) && $_SESSION['customerDetails']){
            $json = json_decode($_SESSION['customerDetails'],true);
            foreach($_POST as $id=>$value){
                $json[$id] = $value;
            }
        } else {
            $json = ['email'=>$_POST['email']];
        }
        $_SESSION['customerDetails'] = json_encode($json);
        $_SESSION['aCart']->details = json_encode(['email'=>$_POST['email']]);
        if(isset($_POST['onepage'])){
            $this->toJson(['status'=>true]);
        } else {
            redirect('/checkout/delivery');
        }
    }

    public function delivery(Array $params = [])
    {
        if($this->viewData->user != null){
            $this->viewData->addresses = \Model\Address::getList(['where' => 'user_id = ' . $this->viewData->user->id]);
        }
        $this->emptyCartValidation();
        $this->customerCheckoutValidation();
        $this->configs['Meta Title'] = "Dji - Checkout - Shipping";
        $this->loadView($this->viewData);
    }

    function delivery_post($params = []){
        $_SESSION['customerDetails'] = json_decode($_SESSION['customerDetails'],true);
        isset($_POST['shipping_is_billing']) ? : $_POST['shipping_is_billing'] = 'off';
        foreach($_POST as $key=>$item){
            $_SESSION['customerDetails'][$key] = $item;
        }
//        $email = $_SESSION['customerDetails']['email'];
        $_SESSION['customerDetails'] = json_encode($_SESSION['customerDetails']);
//        $newsletter = \Model\Newsletter::getItem(null,['where'=>"active = 1 and is_expired = 1 and email = '{$email}'"]);
//        if($newsletter){
//            $_SESSION['coupon'] = $newsletter->email;
//        }elseif(isset($_SESSION['coupon'])){
//            unset($_SESSION['coupon']);
//        }
        if(isset($_POST['onepage'])){
            $this->toJson(['status'=>true]);
        } else {
            redirect('/checkout/payment');
        }
    }

    public function preorder(Array $params = [])
    {
        $this->emptyCartValidation();
        $this->configs['Meta Title'] = "Dji - Checkout - Email";
        if($this->viewData->user){
            $_SESSION['customerDetails'] = json_encode(['email'=>$this->viewData->user->email]);
        }
        $this->loadView($this->viewData);
    }

    public function preorder_post()
    {
//        if(!isset($_SESSION['customerDetails'])){
//            $n = new \Notification\ErrorHandler('Your session expired.');
//            $_SESSION["notification"] = serialize($n);
//            redirect('/checkout/preorder');
//        }

//        $session = json_decode($_SESSION['customerDetails'],true);
//        $_POST = array_merge($session,$_POST);
        $order = \Model\Order::loadFromPost();
        $order->is_preorder = 1;
        $date = new DateTime();
        $order->insert_time = $date->format('Y-m-d H:i:s');
        if($this->viewData->user){
            $order->user_id = $this->viewData->user->id;
            if($this->viewData->user->wholesale_id && ($ws = \Model\Wholesale::getItem(null,['where'=>"id = {$this->viewData->user->wholesale_id} and status = 1"]))){
                $order->wholesale_id = $ws->id;
                $order->wholesale_payment_type = 1;
            }
        } else {
            $order->user_id = 0;
        }
        $order->guest_id = session_id();
        $subtotal = $this->viewData->cart->total;
        $calculatedTotal = $this->viewData->cart->total;
        $taxable = $this->viewData->cart->taxable;

        if(isset($_POST['shipping']) && $_POST['shipping']){
            $calculatedTotal += $_POST['shipping'];
            $taxable += $_POST['shipping'];
            $order->shipping_cost = $_POST['shipping'];
        }
        if(isset($_POST['coupon']) && $_POST['coupon']){
            if($coupon = \Model\Coupon::checkDiscount()){
                $order->coupon_code = $coupon->code;
                $order->coupon_type = $coupon->discount_type;
            }
            $order->coupon_amount = round($_POST['coupon'],2);
            $calculatedTotal -= round($_POST['coupon'],2);
            $taxable -= round($_POST['coupon'],2);
        }
        if(isset($_POST['tax_rate']) && $_POST['tax_rate']){
            $order->tax = round($taxable * .08875,2);
            $calculatedTotal += $order->tax;
            $order->tax_rate = $_POST['tax_rate'];
        }
        //If gift card amount exceeds transactional total
        if(isset($_POST['gift_card']) && $_POST['gift_card']){
//            $calculatedTotal -= $_POST['gift_card'];
            $order->gift_card_amount = $calculatedTotal;
            $calculatedTotal = 0;
            $order->payment_method = 5;
        }
//        if(($newsletter = \Model\Newsletter::checkCoupon($order->email))){
//            $order->newsletter_discount = $newsletter->getDiscountAmount($subtotal);
//            $calculatedTotal -= number_format($newsletter->getDiscountAmount($subtotal),2);
//        }
        //If gift card amount does not exceed transactional total, subtract amount from total, continue with payment
        else if(($gift_card = \Model\Gift_Card::validateGiftCard($_POST['gift_card_code']))){
            if($gift_card->getRemainingAmount() > $calculatedTotal){
            } else {
                $order->gift_card_amount = round($gift_card->getRemainingAmount(),2);
                $calculatedTotal -= round($gift_card->getRemainingAmount(),2);
            }
        }

        $order->gift_card = $_POST['gift_card_code'];

        if(isset($_POST['paypal']) && $_POST['paypal'] == 'on'){
            $order->payment_method = 2;
        }

        $order->status = \Model\Order::$status[0];

        $order->subtotal = round($subtotal,2);
        $order->total = round($calculatedTotal,2);
        $order->ref_num = generateToken();

        if($_POST['same_address'] == 'on'){
            $billing = new stdClass();
            $order->bill_first_name = $order->ship_first_name;
            $order->bill_last_name = $order->ship_last_name;
            $order->bill_address = $order->ship_address;
            $order->bill_address2 = $order->ship_address2;
            $order->bill_phone = $order->ship_phone;
            $order->bill_city = $order->ship_city;
            $order->bill_country = $order->ship_country;
            $order->bill_state = $order->ship_state;
            $order->bill_zip = $order->ship_zip;
        }
//        dd($_POST,$_SESSION,$this->viewData->cart->cart,$order);
        if($order->save()) {
            \Model\User::checkoutUser($order);
//            \Model\User::addToMailChimp($order->email);
            $html = '';

            $shipping = new stdClass();
            $shipping->name = $order->ship_first_name . ' ' . $order->ship_last_name;
            $shipping->company = null;
            $shipping->street1 = $order->ship_address;
            $shipping->street2 = $order->ship_address2;
            $shipping->city = $order->ship_city;
            $shipping->state = $order->ship_state;
            $shipping->postalCode = $order->ship_zip;
            $shipping->phone = $order->phone;
            $shipping->residential = true;

            if($_POST['same_address'] == 'on'){
                $billing = new stdClass();
                $billing->name = $shipping->name;
                $billing->company = $shipping->company;
                $billing->street1 = $shipping->street1;
                $billing->street2 = $shipping->street2;
                $billing->city = $shipping->city;
                $billing->state = $shipping->state;
                $billing->postalCode = $shipping->postalCode;
                $billing->phone = $shipping->phone;
                $billing->residential = true;
            }



            $shipItems = [];

            //Start appending items to PayPal API
            $pPItems = new \PayPal\Api\ItemList();
            foreach ($this->viewData->cart->cart as $array) {
                $order_prod = new \Model\Order_Product();
                $order_prod->order_id = $order->id;
                $order_prod->quantity = $array->quantity;
                if($array->product_id) {
                    $product = \Model\Product::getItem($array->product_id);
                    $color = json_decode($array->variation, true)['color'];
                    $size = json_decode($array->variation, true)['size'];

                    $order_prod->product_id = $array->product_id;
                    $order_prod->unit_price = $product->getPrice($color, $size, false, $this->viewData->user);
                    $order_prod->details = $array->variation;
                    $order_prod->clearance = $array->clearance;
//                    $colorObj = \Model\Color::getItem($color);
//                    $sizeObj = \Model\Size::getItem(json_decode($array->variation, true)['size']);

                    $full = 'https://kaws.mymagid.net/products/' . $product->slug;
                    $img = UPLOAD_URL . 'products/' . $product->featuredImage();
                    $name = $product->name;
                    $colorText = '';
                    $sObjUs = '';
                    $sObjEu = '';
//                    $colorText = $colorObj->name;
//                    $sObjUs = $sizeObj->us_size;
//                    $sObjEu = $sizeObj->eur_size;
                    $price = $array->clearance ? $product->getPrice($color,$size,true,$this->viewData->user) : $product->getPrice($color,$size, false, $this->viewData->user);

                    $appendColumn = '';

                    /**
                     * Total sales for product +1
                     */
                    $product->total_sales += 1;
                    $product->save();
                }
                $order_prod->save();

                $html .=
                    '<tr style="height:75.0pt">
						<td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal"><img width="100px!important;" src="https://kaws.mymagid.net'.$img.'"></p>
						</td>
						<td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
						</td>
                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$array->quantity.'</span></p>
						</td>
                        <td width="140" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $array->quantity,2).'</span></p>
						</td>
                        '.$appendColumn.'
					</tr>';

            }

            $mailMaster = new \Email\MailMaster();
            $mergeFields = [
                'ORDER_NUMBER' => "" . $order->id,
                'DATE' => date('Y-m-d h:i:s'),
                'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                'SHIPPING' => $order->getShippingAddr(),
                'BILLING' => $order->getBillingAddr(),
                'ITEMS' => $html,
                'SUBTOTAL' => number_format($order->subtotal, 2),
                'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                'TAXFEE' => number_format($order->tax, 2),
                'TOTAL' => number_format($order->total, 2)
            ];

            unset($_SESSION['customerDetails']);
            unset($_SESSION['ship_id']);
            unset($_SESSION['speed']);
            unset($_COOKIE['cart']);

            $mailMaster->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation')->send();

            redirect('/checkout/confirmation/'.$order->ref_num);
        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION["notification"] = serialize($n);
            redirect('/checkout/preorder');
        }
    }

    public function payment(Array $params = [])
    {
        if($this->viewData->cart->campaign->type == 'Lottery'){
            if(!isset($_SESSION['lotto_entrant']) || $_SESSION['lotto_entrant'] == ''){
                $n = new \Notification\ErrorHandler('This item can only be purchased through your redemption link');
                $_SESSION["notification"] = serialize($n);
                redirect('/');
            }
        }

        if(isset($_GET['token']) && $_GET['token']){
            //Person refreshed or accessed url manually
            if($_GET['token'] == $_COOKIE['paypalCancel']){
                //do nothing
            } else if(isset($_COOKIE['paypalOrder']) && $_COOKIE['paypalOrder']){
                $order = \Model\Order::getItem($_COOKIE['paypalOrder']);
                $email = new \Email\MailMaster();
                $mergeTags = [
                    'ORDER_NUM'=>$order->id,
                    'CUSTOMER_NAME'=>$order->shipName()
                ];
//                $email->setTo(['email'=>'eitan@emagid.com','name'=>'Eitan Magid','type'=>'to'])->setMergeTags($mergeTags)->setTemplate('')->noBCC()->send();

            } else {
                //do nothing
            }
            setcookie("paypalCancel",$_GET['token'],time() + 60 * 60 * 24 * 100, "/");
        }

        $this->emptyCartValidation();
        $this->customerCheckoutValidation();
        $this->configs['Meta Title'] = "Dji - Checkout - Secure Payment";
//        $products = $this->viewData->cart->products;
//        $carts = $this->viewData->cart->cart;
//        if(isset($_SESSION['coupon']) && ($newsletter = \Model\Newsletter::getItem(null,['where'=>"active = 1 and is_expired = 1 and email = '".$_SESSION['coupon']."'"]))){
//            $this->viewData->coupon = $newsletter;
//            if(!$newsletter->product_id){
//                $min = $this->viewData->cart->total;
//            }else{
//                $prod = \Model\Product::getItem($newsletter->product_id);
//                if(in_array($prod,$products)){
//                    foreach ($carts as $cart){
//                        if($cart->product_id == $prod->id){
//                            $this->viewData->discountProd = $prod->id;
//                            $color = json_decode($cart->variation,true)['color'];
//                            $min = $prod->getPrice($color);
//                        }
//                    }
//                }else{
//                    $min = $this->viewData->cart->total;
//                }
//            }
//            $this->viewData->cart->discount = $min * 10 / 100;
//        }
        $shipping_meth = \Model\Shipping_Method::getItem($_SESSION['ship_id']);
        if(!$shipping_meth){
            $shipping_meth = \Model\Shipping_Method::getItem(null,['where'=>"is_default = {$_SESSION['ship_id']}"]);
        }
        $this->viewData->shipping = $shipping = json_decode($shipping_meth->cost,true)[0];


        //set tax rate
        $sessionData = json_decode($_SESSION['customerDetails'],true);
        if($sessionData['ship_state'] == 'NY'){
            $tax_rate = 8.875;
        } else if($sessionData['ship_state'] == 'NJ'){
            $tax_rate = 6.875;
        } else {
            $tax_rate = 0;
        }

        //set subtotal for all items (w/o taxes, discount, shipping)
        $this->viewData->subtotal = $subtotal = $this->viewData->cart ? $this->viewData->cart->total : 0;
        $taxable = $this->viewData->cart ? $this->viewData->cart->taxable: 0;
        $newsletterDisc = 0;
//        if(($n = \Model\Newsletter::checkCoupon(json_decode($_SESSION['customerDetails'],true)['email']))){
//            $newsletterDisc = $n->getDiscountAmount($subtotal);
//        }

        //check discount amount
        $discount = isset($this->viewData->cart->discount) && $this->viewData->cart->discount ? $this->viewData->cart->discount: 0;
        $discount += $newsletterDisc;
        $this->viewData->discount = $discount;

        //check gift card
        $giftCard = isset($this->viewData->cart->giftCard) && $this->viewData->cart->giftCard ? $this->viewData->cart->giftCard: 0;

        //apply discount
        $discountTotal = $taxable - $discount;

        //apply taxes
        $this->viewData->tax = $tax = $discountTotal * $tax_rate / 100;

        //set total
        $total = $subtotal + $tax + $shipping - $discount;

        //if gift card covers full cost
        if($giftCard > $total){
            $giftCardFullCharge = true;
            $giftCard = $total;
            $total = 0;
        } else {
            $giftCardFullCharge = false;
            $total -= $giftCard;
        }
        $this->viewData->giftCardFullCharge = $giftCardFullCharge;
        $this->viewData->giftCard = $giftCard;
        $this->viewData->total = $total;

        $this->loadView($this->viewData);
    }

    function payment_post(){
        if(!isset($_POST['agree_to_terms'])){
            $n = new \Notification\ErrorHandler("You must agree to the <a href='#agreeTerms' onclick=\"$('#terms').click()\">Terms of service<a/>");
            $_SESSION['notification'] = serialize($n);
            redirect('/checkout/payment');
        }
        if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) == false){
            $n = new \Notification\ErrorHandler("Please enter a valid email address");
            $_SESSION['notification'] = serialize($n);
            redirect('/checkout/payment');
        }
        if(!isset($_SESSION['customerDetails'])){
            $n = new \Notification\ErrorHandler('Your session expired.');
            $_SESSION["notification"] = serialize($n);
            redirect('/checkout/payment');
        }


//        $session = json_decode($_SESSION['customerDetails'],true);
//        $_POST = array_merge($session,$_POST);
        $order = \Model\Order::loadFromPost();
        $order->user_ip = $_SERVER['REMOTE_ADDR'];
        $order->campaign_id = $this->viewData->cart->campaign->id;
        if($this->viewData->cart->campaign->type == 'Lottery' && isset($_SESSION['lotto_entrant']) && $_SESSION['lotto_entrant'] != ''){
            $order->entry_id = $_SESSION['lotto_entrant'];
        }

        $shipping_meth = \Model\Shipping_Method::getItem($_SESSION['ship_id']);
        $order->shipping_cost = json_decode($shipping_meth->cost,true)[0];
        $date = new DateTime();
        $order->insert_time = $date->format('Y-m-d H:i:s');

        $user = \Model\User::getItem(null,['where'=>"lower(email) = lower('$order->email') AND lower(first_name) = lower('$order->ship_first_name') AND lower(last_name) = lower('$order->ship_last_name') AND lower(phone) = lower('$order->phone')"]);
        if(isset($_SESSION['lotto_email']) ) {
            if ($_SESSION['lotto_email'] != $order->email) {
                $n = new \Notification\ErrorHandler('This email does not match the one used to sign up for the lottery.');
                $_SESSION["notification"] = serialize($n);
                redirect('/');
            } else {
                $user = \Model\Lotto_Entry::getItem($_SESSION['lotto_entrant'])->user();
            }
        }
        if($user) {
            $order->user_id = $user->id;
        } else {
//            $user = new \Model\User();
//            $user->email = $order->email;
//            $user->first_name = $order->ship_first_name;
//            $user->last_name = $order->ship_last_name;
//            $user->phone = $order->phone;
            $order->user_id = 0;
        }
        $order->guest_id = session_id();

        $subtotal = $this->viewData->cart->total;
        $calculatedTotal = $this->viewData->cart->total;
        $taxable = $this->viewData->cart->taxable;

        if(isset($_POST['shipping']) && $_POST['shipping']){
            $calculatedTotal += $_POST['shipping'];
            $taxable += $_POST['shipping'];
            $order->shipping_cost = $_POST['shipping'];
        }
        if(isset($_POST['duty']) && $_POST['duty']){
            $order->duty = $_POST['duty'];
            $calculatedTotal += $order->duty;
        }
        if(isset($_POST['coupon']) && $_POST['coupon']){
            if($coupon = \Model\Coupon::checkDiscount()){
                $order->coupon_code = $coupon->code;
                $order->coupon_type = $coupon->discount_type;
            }
            $order->coupon_amount = round($_POST['coupon'],2);
            $calculatedTotal -= round($_POST['coupon'],2);
            $taxable -= round($_POST['coupon'],2);
        }
        if($order->ship_country != $order->bill_country){
            $order->ship_country = $order->bill_country;
        }
        if($order->ship_country=='United States'){
            switch($order->ship_state){
                case 'NY':
                    $order->tax_rate = $_POST['tax_rate'] = 8.875;
                    break;
                case 'NJ':
                    $order->tax_rate = $_POST['tax_rate'] = 6.875;
                    break;
            }
        }
        if(isset($_POST['tax_rate']) && $_POST['tax_rate']){
            $order->tax = round($taxable * ($_POST['tax_rate']/100),2);
            $calculatedTotal += $order->tax;
            $order->tax_rate = $_POST['tax_rate'];
        }
        //If gift card amount exceeds transactional total
        if(isset($_POST['gift_card']) && $_POST['gift_card']){
//            $calculatedTotal -= $_POST['gift_card'];
            $order->gift_card_amount = $calculatedTotal;
            $calculatedTotal = 0;
            $order->payment_method = 5;
        }
//        if(($newsletter = \Model\Newsletter::checkCoupon($order->email))){
//            $order->newsletter_discount = $newsletter->getDiscountAmount($subtotal);
//            $calculatedTotal -= number_format($newsletter->getDiscountAmount($subtotal),2);
//        }
        //If gift card amount does not exceed transactional total, subtract amount from total, continue with payment
        else if(($gift_card = \Model\Gift_Card::validateGiftCard($_POST['gift_card_code']))){
            if($gift_card->getRemainingAmount() > $calculatedTotal){
            } else {
                $order->gift_card_amount = round($gift_card->getRemainingAmount(),2);
                $calculatedTotal -= round($gift_card->getRemainingAmount(),2);
            }
        }

        $order->gift_card = $_POST['gift_card_code'];

        if(isset($_POST['paypal']) && $_POST['paypal'] == 'on'){
            $order->payment_method = 2;
        }

        $order->status = \Model\Order::$status[0];

        $order->subtotal = round($subtotal,2);
        $order->total = round($calculatedTotal,2);
        $order->ref_num = generateToken();

//        dd($_POST,$_SESSION,$this->viewData->cart->cart,$order);

        if($order->save()) {
            \Model\User::checkoutUser($order);
//            \Model\User::addToMailChimp($order->email);
            $html = '';

            $billing = new stdClass();
            $billing->name = $order->bill_first_name . ' ' . $order->bill_last_name;
            $billing->company = null;
            $billing->street1 = $order->bill_address;
            $billing->street2 = $order->bill_address2;
            $billing->city = $order->bill_city;
            $billing->state = $order->state;
            $billing->postalCode = $order->bill_zip;
            $billing->phone = $order->phone;
            $billing->residential = true;

            $shipping = new stdClass();
            $shipping->name = $order->ship_first_name . ' ' . $order->ship_last_name;
            $shipping->company = null;
            $shipping->street1 = $order->ship_address;
            $shipping->street2 = $order->ship_address2;
            $shipping->city = $order->ship_city;
            $shipping->state = $order->ship_state;
            $shipping->postalCode = $order->ship_zip;
            $shipping->phone = $order->phone;
            $shipping->residential = true;

            $shipItems = [];

            //Start appending items to PayPal API
            $pPItems = new \PayPal\Api\ItemList();
            foreach ($this->viewData->cart->cart as $array) {
                $order_prod = new \Model\Order_Product();
                $order_prod->order_id = $order->id;
                $order_prod->quantity = $array->quantity;
                if($array->product_id) {
                    $product = \Model\Product::getItem($array->product_id);
                    $color = json_decode($array->variation, true)['color'];
                    $size = json_decode($array->variation, true)['size'];

                    $order_prod->product_id = $array->product_id;
                    $order_prod->unit_price = $product->getPrice($color, $size, false, $this->viewData->user);
                    $order_prod->details = $array->variation;
                    $order_prod->clearance = $array->clearance;
//                    $colorObj = \Model\Color::getItem($color);
//                    $sizeObj = \Model\Size::getItem(json_decode($array->variation, true)['size']);

                    $full = 'https://kaws.mymagid.net/products/' . $product->slug;
                    $img = "https://kaws.mymagid.net" . UPLOAD_URL . 'products/' . $product->featuredImage();
                    if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
                    $name = $product->name;
                    $colorText = '';
                    $sObjUs = '';
                    $sObjEu = '';
//                    $colorText = $colorObj->name;
//                    $sObjUs = $sizeObj->us_size;
//                    $sObjEu = $sizeObj->eur_size;
                    $price = $array->clearance ? $product->getPrice($color,$size,true,$this->viewData->user) : $product->getPrice($color,$size, false, $this->viewData->user);

                    $appendColumn = '';

                    $shipItem = new stdClass();
                    $shipItem->lineItemKey = uniqid();
                    $shipItem->productId = $product->id;
                    $shipItem->sku = $product->part_number;
                    $shipItem->name = $product->name;
                    $shipItem->imageUrl = SITE_DOMAIN . '/content/uploads/products/' . $product->featuredImage();
                    $shipItem->quantity = $array->quantity;
                    $shipItem->unitPrice = $product->getPrice(null,null, false, $this->viewData->user);
                    $shipItems[] = $shipItem;

                    /**
                     * Total sales for product +1
                     */
                    $product->total_sales += 1;
                    $product->save();
                } else {
                    $detail = json_decode($array->details,true);
                    $gc = \Model\Gift_Card::getItem($detail['gift_card_id']);

                    $gift = \Model\Gift_Card::generateNewGiftCard($gc->id,$order->id);
                    $detail['code'] = $gift->code;
                    $detail['new_gift_card'] = $gift->id;

                    $order_prod->product_id = 0;
                    $order_prod->unit_price = $detail['amount'];
                    $order_prod->details = json_encode($detail);
                    $order_prod->clearance = 0;

                    $full = 'https://kaws.mymagid.net/gift_cards/product/'.$detail['gift_card_id'];
                    $img = UPLOAD_URL.'gift_cards/'.$gc->image;
                    $name = $gc->name;
                    $colorText = '';
                    $sObjUs = '';
                    $sObjEu = '';
                    $price = number_format($detail['amount'],2);

                    $appendColumn = '
                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">Gift Card Code: '.$gift->code.'</span></p>
                        </td>';
                }
                $order_prod->save();
                $pPItem = new \PayPal\Api\Item(); $pPItem->setQuantity($order_prod->quantity)->setName($name)->setPrice($price)->setUrl($full)->setCurrency('USD');
                $pPItems->addItem($pPItem);

                $html .=
                    '<tr style="height:75.0pt">
						<td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
						</td>
						<td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$array->quantity.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $array->quantity,2).'</span></p>
						</td>
                        '.$appendColumn.'
					</tr>';

            }

            if(!$order->validate_order()){
                redirect('/checkout/payment');
            }

            if($order->coupon_amount > 0){
                $pPItem = new \PayPal\Api\Item(); $pPItem->setQuantity(1)->setName('Discount')->setPrice(-$order->coupon_amount)->setCurrency('USD');
                $pPItems->addItem($pPItem);
            }
            if($order->gift_card_amount > 0){
                $pPItem = new \PayPal\Api\Item(); $pPItem->setQuantity(1)->setName('Gift Card')->setPrice(-$order->gift_card_amount)->setCurrency('USD');
                $pPItems->addItem($pPItem);
            }
            if($order->newsletter_discount > 0){
                $pPItem = new \PayPal\Api\Item(); $pPItem->setQuantity(1)->setName('Newsletter Discount')->setPrice(-$order->newsletter_discount)->setCurrency('USD');
                $pPItems->addItem($pPItem);
            }
            $email = new \Email\MailMaster();
            $mergeFields = [
                'ORDER_NUMBER' => $order->id,
                'DATE' => date('Y-m-d h:i:s'),
                'DATED' => date('l, F d, Y'),
                'DATET' => date('h:i A'),
                'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                'SHIPPING' => $order->getShippingAddr(),
                'BILLING' => $order->getBillingAddr(),
                'ITEMS' => $html,
                'SUBTOTAL' => number_format($order->subtotal, 2),
                'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                'SHIPPINGNAME' => 'Shipping',
                'TAXNAME' => $order->ship_country=='United States'?'Tax':'Duty',
                'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                'TAXFEE' => number_format($order->ship_country=='United States'?$order->tax:$order->duty, 2),
                'TOTAL' => number_format($order->total, 2)
            ];
            //Credit card payment - Payment_method = 1
            if($order->payment_method == 1) {
                $transaction = new AuthorizeNetAIM();
                $transaction->setFields([
                    'amount' => $order->total,
                    'card_num' => $order->cc_number,
                    'exp_date' => $order->cc_expiration_month . '/' . $order->cc_expiration_year,
                    'first_name' => $order->bill_first_name,
                    'last_name' => $order->bill_last_name,
                    'address' => $order->bill_address,
                    'city' => $order->bill_city,
                    'state' => $order->bill_state,
                    'country' => $order->bill_country,
                    'zip' => $order->bill_zip,
                    'email' => $order->email,
                    'card_code' => $order->cc_ccv,
                    'ship_to_address' => $order->ship_address,
                    'ship_to_city' => $order->ship_city,
                    'ship_to_country' => $order->ship_country,
                    'ship_to_first_name' => $order->ship_first_name,
                    'ship_to_last_name' => $order->ship_last_name,
                    'ship_to_state' => $order->ship_state,
                    'ship_to_zip' => $order->ship_zip,
                    'phone' => $order->phone
                ]);
                $transaction->setCustomFields(['x_invoice_num' => $order->id]);
//            $transaction->setCustomFields(['x_test_request'=>true]); //TEST MODE
                $shipOrderStatus = 'awaiting_shipment';
//                if($this->viewData->wholesale && $this->viewData->wholesale->payment_type == 0){
//                    $response = $transaction->authorizeOnly();
//                    $order->wholesale_payment_type = 0;
//                    $shipOrderStatus = 'awaiting_payment';
//                } else {
//                    $response = $transaction->authorizeAndCapture();
//                }
                $localTransaction = new \Model\Transaction();
                $localTransaction->order_id = $order->id;
//                $localTransaction->ref_tran_id = $response->transaction_id;
//                $localTransaction->amount = $response->amount;
                $localTransaction->avs_result_code = $response->avs_response;
                $localTransaction->cvv_result_code = $response->cavv_response;
                $localTransaction->type = "AUTH_CAPTURE";
                $localTransaction->save();
//                if (true) {
                if (false) {
                    if(isset($_SESSION['lotto_entrant']) && $_SESSION['lotto_entrant'] != ''){
                        $entrant = \Model\Lotto_Entry::getItem($_SESSION['lotto_entrant']);
                        $entrant->redeemed = 1;
                        $now = Carbon\Carbon::now();
                        $entrant->claimed_date = $now;
                        $entrant->ref_num = '';
                        $entrant->save();
                        $order->entry_id = $entrant->id;
                    }
                    foreach(\Model\Order_Product::getList(['where'=>"order_id = $order->id"]) AS $oProd ){
                        $prodUpdate = \Model\Product::getItem($oProd->product_id);
                        $campProdUpdate = \Model\Campaign_Product::getItem(null,['where'=>"product_id = $prodUpdate->id AND campaign_id = $order->campaign_id"]);
                        $prodUpdate->quantity -= $oProd->quantity;
                        $campProdUpdate->quantity -= $oProd->quantity;
                        if($prodUpdate->quantity <= 0){
                            $prodUpdate->quantity = 0;
                            $prodUpdate->availability = 1;
                        }
                        if($prodUpdate->quantity < $campProdUpdate->quantity ){
                            $campProdUpdate->quantity = $prodUpdate->quantity;
                            $campProdUpdate->max_per_user = $prodUpdate->quantity;
                        }
                        if($campProdUpdate->quantity <= 0){
                            $campProdUpdate->quantity = 0;
                            $campProdUpdate->max_per_user = 0;
                        }
                        if($prodUpdate->quantity < $campProdUpdate->max_per_user){
                            $campProdUpdate->max_per_user = $prodUpdate->quantity;
                        }
                        $prodUpdate->save();
                        $campProdUpdate->save();
                    }
                    $order->auth_number = $response->authorization_code;

                    $order->status = \Model\Order::$status[0];
                    $order->payment_status = 'AuthorizeNet Success';
//                    $order->payment_status = 'Test Order';
                    $order->save();
                    $bonus_gift = floor(($this->viewData->cart->total - $this->viewData->cart->discount)/1000)*50;
                    $gc = Model\Gift_Card::generateNewGiftCard(null,$order->id,$bonus_gift);

                    $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation');
                    try {
                        $email->send();
                    } catch(Mandrill_Error $e){
                        $n = new \Notification\ErrorHandler('We could not send you an email at this time. We will contact you to confirm your order');
                        $_SESSION['notification'] = serialize($n);
                    }

                    if($order->gift_card && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card)) && $order->gift_card_amount){
                        $giftCard->removeAmount($order->gift_card_amount);
                        unset($_SESSION['gift_card']);
                    }

                    foreach ($this->viewData->cart->cart as $array) {
                        $color = json_decode($array->variation,true)['color'];
                        $size = json_decode($array->variation,true)['size'];
                        $c = \Model\Clearance::checkClearance($array->product_id,$color,$size);
                        if($c){
                            $c->current_quantity--;
                            $c->save();
                        }
                        \Model\Cart::delete($array->id);
                        if (isset($_SESSION['aCart'])) {
                            \Model\Abandoned_Cart::delete($_SESSION['aCart']->id);
                        }
                    }

                    if (isset($_SESSION['coupon'])) {
                        unset($_SESSION['coupon']);
                    }
                    unset($_SESSION['customerDetails']);
                    unset($_SESSION['ship_id']);
                    unset($_SESSION['speed']);
                    unset($_COOKIE['cart']);
                    redirect('/checkout/confirmation/' . $order->ref_num);
                } else {
                    $order->status = \Model\Order::$status[2];
                    $order->error_message = $response->response_reason_text;
                    $order->payment_status = 'AuthorizeNet Failed';
                    $order->save();
                    $n = new \Notification\ErrorHandler('Item is no longer available');
                    $_SESSION["notification"] = serialize($n);
                    redirect('/checkout/payment');
                }
            }
            //Paypal charge - Payment_method = 2
            else if($order->payment_method == 2) {

                $order->ship_first_name =   $order->bill_first_name;
                $order->ship_last_name =    $order->bill_last_name;
                $order->ship_address =      $order->bill_address;
                $order->ship_address2 =     $order->bill_address2;
                $order->ship_phone =        $order->bill_phone;
                $order->ship_city =         $order->bill_city;
                $order->ship_country =      $order->bill_country;
                $order->ship_state =        $order->bill_state;
                $order->ship_zip =          $order->bill_zip;

                $order->status = \Model\Order::$status[11];
                $order->payment_status = 'PayPal Initiated';
                $order->save();

                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                $apiContext->setConfig(['mode'=>'LIVE']);

                $payer = new \PayPal\Api\Payer(); $payer->setPaymentMethod('paypal');

                $pPDetails = new \PayPal\Api\Details(); $pPDetails->setSubtotal($order->subtotal - $order->coupon_amount - $order->gift_card_amount - $order->newsletter_discount)->setShipping($order->shipping_cost)->setTax($order->tax);
                $pPAmount = new \PayPal\Api\Amount(); $pPAmount->setTotal(round($order->total,2))->setCurrency('USD')->setDetails($pPDetails);
                $pPTransaction = new \PayPal\Api\Transaction(); $pPTransaction->setAmount($pPAmount)->setItemList($pPItems);
                $pPTransArr[] = $pPTransaction;

                //Live
                $pPRedirectUrl = new \PayPal\Api\RedirectUrls(); $pPRedirectUrl->setReturnUrl("https://kaws.mymagid.net/checkout/confirmation/$order->ref_num")->setCancelUrl('https://kaws.mymagid.net/checkout/payment');
                //Local
//                $pPRedirectUrl = new \PayPal\Api\RedirectUrls(); $pPRedirectUrl->setReturnUrl("http://mv.app/checkout/confirmation/$order->ref_num")->setCancelUrl('http://mv.app/checkout/payment');

                setcookie('paypalOrder',$order->id, time()+60*60*24*100, "/");

                $payment = new \PayPal\Api\Payment();
                $payment->setIntent('sale')->setPayer($payer)->setTransactions($pPTransArr)->setRedirectUrls($pPRedirectUrl);
                try{
                    $payment = $payment->create($apiContext);
                    if(strtolower($payment->getState()) == 'created'){
                        $order->payment_status = 'PayPal Login and Confirmation';$order->save();
                        $redirect = $payment->getLink('approval_url');
                        redirect($redirect);
                    } else {
                        $order->error_message = $payment->getFailureReason();
                        $order->status = \Model\Order::$status[2];
                        $order->payment_status = 'PayPal Canceled';
                        $order->save();
                        $n = new \Notification\ErrorHandler($payment->getFailureReason());
                        $_SESSION['notification'] = serialize($n);
                        redirect('/checkout/payment');
                    }
                } catch (\PayPal\Exception\PayPalConnectionException $ex){
                    $order->error_message = $ex->getData();
                    $order->status = \Model\Order::$status[2];
                    $order->payment_status = 'PayPal Error';
                    $order->save();
                    $n = new \Notification\ErrorHandler('It looks like we had an issue processing your PayPal payment. If the problem persists, please try again using your credit card.');
                    $_SESSION['notification'] = serialize($n);
                    redirect('/checkout/payment');
                }
            }
            //Cash Payment - Payment_method = 3 - no validation - WHOLESALE ONLY
            else if($order->payment_method == 3){
                $shipOrderStatus = 'awaiting_payment';
                $shipOrder = new stdClass();
                $shipOrder->orderNumber = $order->id;
                $shipOrder->orderDate = date(DateTime::ATOM);
                $shipOrder->orderStatus = $shipOrderStatus;
                $shipOrder->amountPaid = $order->total;
                $shipOrder->taxAmount = $order->tax;
//                    $shipOrder->carrierCode = 'ups';
//                    $shipOrder->serviceCode = $serviceCode; //shipping service
                $shipOrder->paymentMethod = 'Cash';

                $advanced = new stdClass();
                $advanced->storeId = 186177; //using available store id to test

                $shipOrder->billTo = $billing;
                $shipOrder->shipTo = $shipping;
                $shipOrder->advancedOptions = $advanced;
                $shipOrder->items = $shipItems;
                if($this->viewData->wholesale){
                    $shipOrder->orderStatus = 'awaiting_payment';
                }
                $shipstation = new ShipStation();
                $shipstation->setSsApiKey(SHIPSTATION_KEY);
                $shipstation->setSsApiSecret(SHIPSTATION_SECRET);
                $result = $shipstation->addOrder($shipOrder);
                if ($result) {
                    $order->ship_id = $result->orderId;
                } else {
                    $error = $shipstation->getLastError();
                    $msg = json_decode($error->message, true)['ExceptionMessage'];
                    $order->ship_id = 0;
                    $order->ship_status = $error->code;
                    $order->ship_message = $msg;
                }

                $order->status = \Model\Order::$status[0];
                $order->payment_status = 'Cash Checkout';
                $order->save();

                $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation-template');
                try {
                    $email->send();
                } catch(Mandrill_Error $e){
                    $n = new \Notification\ErrorHandler('We could not send you an email at this time. We will contact you to confirm your order');
                    $_SESSION['notification'] = serialize($n);
                }

                /** Unset Block */
                foreach ($this->viewData->cart->cart as $array) {
                    $color = json_decode($array->variation,true)['color'];
                    $size = json_decode($array->variation,true)['size'];
                    $c = \Model\Clearance::checkClearance($array->product_id,$color,$size);
                    if($c){
                        $c->current_quantity--;
                        $c->save();
                    }
                    \Model\Cart::delete($array->id);
                    if (isset($_SESSION['aCart'])) {
                        \Model\Abandoned_Cart::delete($_SESSION['aCart']->id);
                    }
                }

                if (isset($_SESSION['coupon'])) {
                    unset($_SESSION['coupon']);
                }
                unset($_SESSION['customerDetails']);
                unset($_SESSION['ship_id']);
                unset($_SESSION['speed']);
                unset($_COOKIE['cart']);
                redirect('/checkout/confirmation/' . $order->ref_num);
            }
            //Deposit Payment - Payment_method = 4 - Check - WHOLESALE ONLY
            else if($order->payment_method == 4){
                $shipOrderStatus = 'awaiting_payment';
                $shipOrder = new stdClass();
                $shipOrder->orderNumber = $order->id;
                $shipOrder->orderDate = date(DateTime::ATOM);
                $shipOrder->orderStatus = $shipOrderStatus;
                $shipOrder->amountPaid = $order->total;
                $shipOrder->taxAmount = $order->tax;
//                    $shipOrder->carrierCode = 'ups';
//                    $shipOrder->serviceCode = $serviceCode; //shipping service
                $shipOrder->paymentMethod = 'Check';

                $advanced = new stdClass();
                $advanced->storeId = 186177; //using available store id to test

                $shipOrder->billTo = $billing;
                $shipOrder->shipTo = $shipping;
                $shipOrder->advancedOptions = $advanced;
                $shipOrder->items = $shipItems;
                if($this->viewData->wholesale){
                    $shipOrder->orderStatus = 'awaiting_payment';
                }
                $shipstation = new ShipStation();
                $shipstation->setSsApiKey(SHIPSTATION_KEY);
                $shipstation->setSsApiSecret(SHIPSTATION_SECRET);
                $result = $shipstation->addOrder($shipOrder);
                if ($result) {
                    $order->ship_id = $result->orderId;
                } else {
                    $error = $shipstation->getLastError();
                    $msg = json_decode($error->message, true)['ExceptionMessage'];
                    $order->ship_id = 0;
                    $order->ship_status = $error->code;
                    $order->ship_message = $msg;
                }

                $order->status = \Model\Order::$status[0];
                $order->payment_status = 'Deposit Checkout';
                $order->save();

                $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation');
                try {
                    $email->send();
                } catch(Mandrill_Error $e){
                    $n = new \Notification\ErrorHandler('We could not send you an email at this time. We will contact you to confirm your order');
                    $_SESSION['notification'] = serialize($n);
                }

                /** Unset Block */
                foreach ($this->viewData->cart->cart as $array) {
                    $color = json_decode($array->variation,true)['color'];
                    $size = json_decode($array->variation,true)['size'];
                    $c = \Model\Clearance::checkClearance($array->product_id,$color,$size);
                    if($c){
                        $c->current_quantity--;
                        $c->save();
                    }
                    \Model\Cart::delete($array->id);
                    if (isset($_SESSION['aCart'])) {
                        \Model\Abandoned_Cart::delete($_SESSION['aCart']->id);
                    }
                }

                if (isset($_SESSION['coupon'])) {
                    unset($_SESSION['coupon']);
                }
                unset($_SESSION['customerDetails']);
                unset($_SESSION['ship_id']);
                unset($_SESSION['speed']);
                unset($_COOKIE['cart']);
                redirect('/checkout/confirmation/' . $order->ref_num);
            }
            //Gift Code - Payment_method = 5
            else if($order->payment_method == 5){
                $order->status = \Model\Order::$status[0];
                $order->save();

                $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation');
                try{
                    $email->send();
                } catch (Mandrill_Error $e){
                    $n = new \Notification\ErrorHandler('We could not send you an email at this time. We will contact you to confirm your order');
                    $_SESSION['notification'] = serialize($n);
                    redirect('/checkout/confirmation/'.$order->ref_num);
                }

                if($order->gift_card && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card)) && $order->gift_card_amount){
                    $giftCard->removeAmount($order->gift_card_amount);
                    unset($_SESSION['gift_card']);
                }

                foreach ($this->viewData->cart->cart as $array) {
                    \Model\Cart::delete($array->id);
                    if (isset($_SESSION['aCart'])) {
                        \Model\Abandoned_Cart::delete($_SESSION['aCart']->id);
                    }
                }

                if (isset($_SESSION['coupon'])) {
                    unset($_SESSION['coupon']);
                }
                unset($_SESSION['customerDetails']);
                unset($_SESSION['ship_id']);
                unset($_SESSION['speed']);
                unset($_COOKIE['cart']);
                redirect('/checkout/confirmation/'.$order->ref_num);
            }
        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION["notification"] = serialize($n);
            redirect('/checkout/payment');
        }
    }

    function confirmation(Array $params = []){
        if(($order = \Model\Order::getItem(null,['where'=>"ref_num = '".$params['id']."'"]))){
            $this->viewData->order = $order;
            if($this->viewData->user){
                $this->viewData->user->insert_time = \Model\User::getItem($order->user_id)->insert_time;
                $this->viewData->user->order_count = \Model\Order::getCount(["where"=>"user_id = $order->user_id"]);
            }
            $this->viewData->order->authorizeData = \Model\Transaction::getItem(null,['where'=>'order_id = '.$order->id]);
            $order->invoicePaid();
            if(isset($_GET['paymentId']) && $_GET['paymentId'] && $order->paypal_id != $_GET['paymentId']){
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                $apiContext->setConfig(['mode'=>'LIVE']);
                $payment = \PayPal\Api\Payment::get($_GET['paymentId'],$apiContext);
                $paymentExecution = new \PayPal\Api\PaymentExecution();
                $paymentExecution->setPayerId($_GET['PayerID']);
                $order->payment_status = 'PayPal Executing Payment'; $order->save();
                try{
                    $payment = $payment->execute($paymentExecution,$apiContext);
                    if(strtolower($payment->getState()) == 'approved') {
                        //unset cookie
                        setcookie('paypalOrder','',time()-3600,'/');

                        $order->paypal_id = $_GET['paymentId'];
                        $order->status = \Model\Order::$status[0];
                        $order->payment_status = 'PayPal Payment Complete';
                        $order->save();
                        $bonus_gift = floor(($order->subtotal - $order->discount)/1000)*50;
                        $gc = Model\Gift_Card::generateNewGiftCard(null,$order->id,$bonus_gift);

                        $html = '';

                        $shipItems = [];
                        foreach ($this->viewData->cart->cart as $array) {
                            $product = \Model\Product::getItem($array->product_id);
                            $color = json_decode($array->variation, true)['color'];

                            $colorObj = \Model\Color::getItem($color);
                            $sizeObj = \Model\Size::getItem(json_decode($array->variation, true)['size']);

                            $shipItem = new stdClass();
                            $shipItem->lineItemKey = uniqid();
                            $shipItem->productId = $product->id;
                            $shipItem->sku = $product->part_number;
                            $shipItem->name = $product->name;
                            $shipItem->imageUrl = SITE_DOMAIN . '/content/uploads/products/' . $product->featuredImage();
                            $shipItem->quantity = $array->quantity;
                            $shipItem->unitPrice = $product->getPrice(null,null, false, $this->viewData->user);
                            $shipItems[] = $shipItem;

                            $html .=
                                '<tr style="height:75.0pt">
						<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal"><img width="100px!important;" src="https://modernvice.com' . UPLOAD_URL . 'products/' . $product->featuredImage(null, $color) . '"></p>
						</td>
						<td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $product->name . '</span></p>
						</td>
                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $array->quantity . '</span></p>
						</td>
                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$' . number_format($product->getPrice(null,null, false, $this->viewData->user) * $array->quantity, 2) . '</span></p>
						</td>
					</tr>';

                            $size = json_decode($array->variation,true)['size'];
                            $c = \Model\Clearance::checkClearance($array->product_id,$color,$size);
                            if($c){
                                $c->current_quantity--;
                                $c->save();
                            }
                            /**
                             * Total sales for product +1
                             */
                            \Model\Cart::delete($array->id);
                            if (isset($_SESSION['aCart'])) {
                                \Model\Abandoned_Cart::delete($_SESSION['aCart']->id);
                            }
                        }
                        $email = new \Email\MailMaster();
                        $mergeFields = [
                            'ORDER_NUMBER' => $order->id,
                            'DATE' => date('Y-m-d h:i:s'),
                            'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                            'SHIPPING' => $order->getShippingAddr(),
                            'BILLING' => $order->getBillingAddr(),
                            'ITEMS' => $html,
                            'SUBTOTAL' => number_format($order->subtotal, 2),
                            'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                            'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                            'TAXFEE' => number_format($order->tax, 2),
                            'TOTAL' => number_format($order->total, 2)
                        ];
                        $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation');

                        try{
                            $email->send();
                        } catch (Mandrill_Error $e){
                            $n = new \Notification\ErrorHandler('We could not send you an email at this time. Please contact us at our toll free number to let our representatives know.');
                            $_SESSION['notification'] = serialize($n);
                            redirect('/checkout/confirmation/'.$order->ref_num);
                        }

                        //ShipStation - addOrder function accepts stdClass
                        $billing = new stdClass();
                        $billing->name = $order->bill_first_name . ' ' . $order->bill_last_name;
                        $billing->company = null;
                        $billing->street1 = $order->bill_address;
                        $billing->street2 = $order->bill_address2;
                        $billing->city = $order->bill_city;
                        $billing->state = $order->state;
                        $billing->postalCode = $order->bill_zip;
                        $billing->phone = $order->phone;
                        $billing->residential = true;

                        $shipping = new stdClass();
                        $shipping->name = $order->ship_first_name . ' ' . $order->ship_last_name;
                        $shipping->company = null;
                        $shipping->street1 = $order->ship_address;
                        $shipping->street2 = $order->ship_address2;
                        $shipping->city = $order->ship_city;
                        $shipping->state = $order->ship_state;
                        $shipping->postalCode = $order->ship_zip;
                        $shipping->phone = $order->phone;
                        $shipping->residential = true;

                        $shipping_meth = \Model\Shipping_Method::getItem($_SESSION['ship_id']);
                        $shipOrder = new stdClass();
                        $shipOrder->requestedShippingService = $shipping_meth->name;
                        $shipOrder->orderNumber = $order->id;
                        $shipOrder->orderDate = date(DateTime::ATOM);
                        $shipOrder->orderStatus = 'awaiting_shipment';
                        $shipOrder->amountPaid = $order->total;
                        $shipOrder->taxAmount = $order->tax;
                        $shipOrder->customerEmail = $order->email;
                        $shipOrder->paymentMethod = 'PayPal';

                        $advanced = new stdClass();
                        $advanced->storeId = 186177; //using available store id to test

                        $shipOrder->billTo = $billing;
                        $shipOrder->shipTo = $shipping;
                        $shipOrder->advancedOptions = $advanced;
                        $shipOrder->items = $shipItems;
                        if($this->viewData->wholesale){
                            $shipOrder->orderStatus = 'awaiting_payment';
                        }
                        $shipstation = new ShipStation();
                        $shipstation->setSsApiKey(SHIPSTATION_KEY);
                        $shipstation->setSsApiSecret(SHIPSTATION_SECRET);
                        $result = $shipstation->addOrder($shipOrder);
                        if ($result) {
                            $order->ship_id = $result->orderId;
                            $order->save();
                        } else {
                            $error = $shipstation->getLastError();
                            $msg = json_decode($error->message, true)['ExceptionMessage'];
                            $order->ship_id = 0;
                            $order->ship_status = $error->code;
                            $order->ship_message = $msg;
                        }

                        if($order->gift_card && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card)) && $order->gift_card_amount){
                            $giftCard->removeAmount($order->gift_card_amount);
                            unset($_SESSION['gift_card']);
                        }

                        if (isset($_SESSION['coupon'])) {
                            unset($_SESSION['coupon']);
                        }
                        unset($_SESSION['customerDetails']);
                        unset($_SESSION['ship_id']);
                        unset($_SESSION['speed']);
                        unset($_COOKIE['cart']);
                    } else {
                        $order->error_message = $payment->getFailureReason();
                        $order->status = \Model\Order::$status[2];
                        $order->save();;
                        $n = new \Notification\ErrorHandler('It looks like we had an issue processing your PayPal payment. If the problem persists, please try again using your credit card.');
                        $_SESSION['notification'] = serialize($n);
                        redirect('/checkout/payment');
                    }
                } catch(\PayPal\Exception\PayPalConnectionException $ex){
                    $order->error_message = $ex->getData();
                    $order->status = \Model\Order::$status[2];
                    $order->save();;
                    $n = new \Notification\ErrorHandler('It looks like we had an issue processing your PayPal payment. If the problem persists, please try again using your credit card.');
                    $_SESSION['notification'] = serialize($n);
                    redirect('/checkout/payment');
                }
//                if(strtolower($payment->getState()) == 'approved') {
//                    //unset cookie
//                    setcookie('paypalOrder','',time()-3600,'/');
//
//                    $order->paypal_id = $_GET['paymentId'];
//                    $order->status = \Model\Order::$status[0];
//                    $order->payment_status = 'PayPal Payment Complete';
//                    $order->save();
//                    $html = '';
//                    foreach ($this->viewData->cart->cart as $array) {
//                        $product = \Model\Product::getItem($array->product_id);
//                        $color = json_decode($array->variation, true)['color'];
//
//                        $colorObj = \Model\Color::getItem($color);
//                        $sizeObj = \Model\Size::getItem(json_decode($array->variation, true)['size']);
//
//                        $html .=
//                            '<tr style="height:75.0pt">
//						<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
//                            <p class="MsoNormal"><img width="100px!important;" src="https://modernvice.com' . UPLOAD_URL . 'products/' . $product->featuredImage(null, $color) . '"></p>
//						</td>
//						<td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
//                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $product->name . '</span></p>
//						</td>
//                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
//                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $array->quantity . '</span></p>
//						</td>
//                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
//                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$' . number_format($product->getPrice($color,$sizeObj->id) * $array->quantity, 2) . '</span></p>
//						</td>
//                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
//                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $colorObj->name . '</span></p>
//                        </td>
//                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
//                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">US: ' . $sizeObj->us_size . '<br>EU: ' . $sizeObj->eur_size . '</span></p>
//                        </td>
//					</tr>';
//
//                        $size = json_decode($array->variation,true)['size'];
//                        $c = \Model\Clearance::checkClearance($array->product_id,$color,$size);
//                        if($c){
//                            $c->current_quantity--;
//                            $c->save();
//                        }
//                        /**
//                         * Total sales for product +1
//                         */
//                        \Model\Cart::delete($array->id);
//                        if (isset($_SESSION['aCart'])) {
//                            \Model\Abandoned_Cart::delete($_SESSION['aCart']->id);
//                        }
//                    }
//                    $email = new \Email\MailMaster();
//                    $mergeFields = [
//                        'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
//                        'DATE' => date('Y-m-d h:i:s'),
//                        'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
//                        'SHIPPING' => $order->getShippingAddr(),
//                        'BILLING' => $order->getBillingAddr(),
//                        'ITEMS' => $html,
//                        'SUBTOTAL' => number_format($order->subtotal, 2),
//                        'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
//                        'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
//                        'TAXFEE' => number_format($order->tax, 2),
//                        'TOTAL' => number_format($order->total, 2)
//                    ];
//                    $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template');
//
//                    try{
//                        $email->send();
//                    } catch (Mandrill_Error $e){
//                        $n = new \Notification\ErrorHandler('We could not send you an email at this time. We will contact you to confirm your order');
//                        $_SESSION['notification'] = serialize($n);
//                        redirect('/checkout/confirmation/'.$order->ref_num);
//                    }
//
//                    if($order->gift_card && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card)) && $order->gift_card_amount){
//                        $giftCard->removeAmount($order->gift_card_amount);
//                        unset($_SESSION['gift_card']);
//                    }
//
//                    if (isset($_SESSION['coupon'])) {
//                        unset($_SESSION['coupon']);
//                    }
//                    unset($_SESSION['customerDetails']);
//                    unset($_SESSION['ship_id']);
//                    unset($_SESSION['speed']);
//                } else {
//                    $order->error_message = $payment->getFailureReason();
//                    $order->status = \Model\Order::$status[2];
//                    $order->save();
//                    $n = new \Notification\ErrorHandler($payment->getFailureReason());
//                    $_SESSION['notification'] = serialize($n);
//                    redirect('/cart');
//                }
            }
        }

        $this->loadView($this->viewData);
    }

    function invoice(Array $params = []){
        if(isset($_GET['ref']) && $_GET['ref'] && ($invoice = \Model\Invoice::getItem(null,['where'=>"ref_num = '{$_GET['ref']}'"])) && $invoice->status == 'New') {
            $cart = new stdClass();
            $cartItems = [];
            $total = 0;
            foreach(json_decode($invoice->details,true) as $item){
                $cartItem = new stdClass();
                if(isset($item['product_id']) && $item['product_id']){
                    $product = \Model\Product::getItem($item['product_id']);
                    $product_id = $product->id;
                    $name = $product->name;
                    $price = isset($item['custom_price']) && $item['custom_price'] ? $item['custom_price']: $product->getPrice($item['color']);
                    $variation = ['color'=>$item['color'], 'size'=>$item['size']];
                } else {
                    $product_id = 0;
                    $name = $item['misc_name'];
                    $price = $item['custom_price'];
                    $variation = [];
                }
                $total += $price;
                $cartItem->product_id = $product_id;
                $cartItem->name = $name;
                $cartItem->price = $price;
                $cartItem->variation = $variation;
                $cartItems[] = $cartItem;
            }
            $cart->subtotal = $total;
            //no taxes
            if($invoice->tax_free){
                $cart->tax_rate = $tax_rate = 0;
            } else {
                $cart->tax_rate = $tax_rate = 8.875;
            }
            $total += ($total * $tax_rate / 100);
            $cart->tax = $total * $tax_rate / 100;

            $cart->cart = $cartItems;
            $cart->discount = 0;
            $cart->total = $total;
            $this->viewData->shipping = $invoice->shipping;
            $this->viewData->cart = $cart;
            $this->loadView($this->viewData);
        } else {
            $n = new \Notification\ErrorHandler("The invoice reference number appears to be invalid. Please call ModernVice for support at 212-777-1851.");
            $_SESSION['notification'] = serialize($n);
            redirect('/cart');
        }
    }

    function invoice_post(){
        $order = \Model\Order::loadFromPost();
        $order->insert_time = date('Y-m-d H:i:s');
        if($this->viewData->user){
            $order->user_id = $this->viewData->user->id;
            if($this->viewData->user->wholesale_id && ($ws = \Model\Wholesale::getItem(null,['where'=>"id = {$this->viewData->user->wholesale_id} and status = 1"]))){
                $order->wholesale_id = $ws->id;
            }
        } else {
            $order->user_id = 0;
        }
        $order->guest_id = session_id();

        $calculateTotal = $order->subtotal;

        //Set shipping cost
        if(isset($_POST['shipping']) && $_POST['shipping']){
            $order->shipping_cost = $_POST['shipping'];
            $calculateTotal += $_POST['shipping'];
        }
        //Set tax
        if(isset($_POST['tax_rate']) && $_POST['tax_rate']){
            $order->tax = $tax = round($order->subtotal * $_POST['tax_rate'] / 100,2);
            $calculateTotal += round($tax,2);
        }
        //Set discount - not applicable
        if(isset($_POST['coupon']) && $_POST['coupon']){
            $coupon = \Model\Coupon::getItem(null,['where'=>"code = '{$_POST['coupon']}'"]);
            $order->coupon_code = $_POST['coupon'];
            $order->coupon_type = $coupon->discount_type;
            $order->coupon_amount = $coupon->discount_amount;
            $amount = $coupon->getDiscountAmount($order->subtotal);
            $calculateTotal -= $amount;
        }

        $order->status = \Model\Order::$status[0];
        $order->ref_num = generateToken();


        if(isset($_POST['paypal']) && $_POST['paypal'] == 'on'){
            $order->payment_method = 2;
        }

        $order->total = $calculateTotal;
        if($order->save()){
            if($invoice = \Model\Invoice::getByRef($_GET['ref'])){
                \Model\User::addToMailChimp($order->email);
                $invoice->order_id = $order->id;
                $invoice->save();

                $order->note = $invoice->notes;
                $order->save();

                $pPItems = new \PayPal\Api\ItemList();
                $items = json_decode($invoice->details,true);
                $html = '';
                foreach($items as $item){
                    $orderProduct = new \Model\Order_Product();
                    $orderProduct->order_id = $order->id;
                    $orderProduct->product_id = isset($item['product_id']) && $item['product_id'] ? $item['product_id']: 0;
                    $orderProduct->quantity = 1;
                    if(isset($item['product_id']) && $item['product_id']){
                        $product = \Model\Product::getItem($item['product_id']);
                        $product->total_sales += 1;
                        $product->save();

                        $unitPrice = isset($item['custom_price']) && $item['custom_price'] ? $item['custom_price']: $product->getPrice($item['color']);
                        $productImage = $product->featuredImage(null,$item['color']);
                        $productName = $product->name;
                        $productColor = \Model\Color::getItem($item['color'])->name;
                        $productSize = \Model\Size::getItem($item['size']);
                        $productPrice = $unitPrice * $orderProduct->quantity;
                        $url = 'https://modernvice.com/products/'.$product->slug;

                        $orderProduct->unit_price = $unitPrice;
                        $orderProduct->details = json_encode(['color'=>$item['color'],'size'=>$item['size']]);
                    } else {
                        $unitPrice = $item['custom_price'];
                        $productImage = '';
                        $productName = $item['misc_name'];
                        $productColor = '';
                        $productSize = new stdClass(); $productSize->us_size = ''; $productSize->eur_size = '';
                        $productPrice = $unitPrice * $orderProduct->quantity;
                        $url = '';

                        $orderProduct->unit_price = $unitPrice;
                        $orderProduct->details = json_encode(['misc_name'=>$productName]);
                    }
                    $orderProduct->save();
                    $pPItem = new \PayPal\Api\Item(); $pPItem->setQuantity($orderProduct->quantity)->setName($productName)->setPrice($productPrice)->setCurrency('USD');
                    if($url){
                        $pPItem->setUrl($url);
                    }
                    $pPItems->addItem($pPItem);

                    $html .= '<tr style="height:75.0pt">
                            <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p class="MsoNormal"><img width="100px!important;" src="https://kaws.mymagid.net'.UPLOAD_URL.'products/'.$productImage.'"></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$productName.'</span></p>
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e; margin-top: 10px;">'.$orderProduct->quantity.'</span></p>
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e; margin-top 10px;">$'.number_format($productPrice,2).'</span></p>
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e; margin-top: 10px;">'.$productColor.'</span></p>
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e; margin-top: 10px;">US: '.$productSize->us_size.'<br>EU: '.$productSize->eur_size.'</span></p>
                            </td>
                        </tr>';
                }

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => $order->id,
                    'DATE' => date('Y-m-d h:i:s'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format($order->tax, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];
                //Credit Card
                if($order->payment_method == 1){
                    $expiry = str_pad($order->cc_expiration_month, 2, 0) . substr($order->cc_expiration_year, -2);
                    $firstData = new Payment\FirstData(PAYEEZY_ID, PAYEEZY_PASSWORD, PAYEEZY_SANDBOX);
                    $firstData->setAmount($order->total);
                    $firstData->setCreditCardNumber($order->cc_number);
                    $firstData->setCreditCardName($order->card_name);
                    $firstData->setCreditCardType($_POST['cc_type']);
                    $firstData->setCreditCardVerification($order->ccv);
                    $firstData->setCreditCardExpiration($expiry);
                    $firstData->setCurrency('USD');
                    $firstData->setReferenceNumber($order->id);
                    if($order->user_id){
                        $firstData->setCustomerReferenceNumber($order->user_id);
                    }
                    $firstData->process();
                    $order->payment_status = 'FirstData Processing';
                    if ($firstData->isSuccess()) {
                        $order->status = \Model\Order::$status[0];
                        $order->payment_status = 'FirstData Success';
                        $order->save();
                        $invoice->status = \Model\Invoice::$status[1];
                        $invoice->save();

                        $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation');

                        try{
                            $email->send();
                        } catch (Mandrill_Error $e){
                            $n = new \Notification\ErrorHandler('We could not send you an email at this time. We will contact you to confirm your order');
                            $_SESSION['notification'] = serialize($n);
                            redirect('/checkout/confirmation/'.$order->ref_num);
                        }

                        foreach ($this->viewData->cart->cart as $array) {
                            \Model\Cart::delete($array->id);
                            if (isset($_SESSION['aCart'])) {
                                \Model\Abandoned_Cart::delete($_SESSION['aCart']->id);
                            }
                        }

                        if (isset($_SESSION['coupon'])) {
                            unset($_SESSION['coupon']);
                        }
                        unset($_SESSION['customerDetails']);
                        unset($_SESSION['ship_id']);
                        unset($_SESSION['speed']);
                        redirect('/checkout/confirmation/' . $order->ref_num);
                    } else {
                        $order->status = \Model\Order::$status[2];
                        $order->error_message = $firstData->getErrorMessage();
                        $order->payment_status = 'FirstData Failed';
                        $order->save();
                        $n = new \Notification\ErrorHandler($firstData->getErrorMessage());
                        $_SESSION["notification"] = serialize($n);
                        redirect('/checkout/invoice?ref='.$_GET['ref']);
                    }
                }
                //Paypal charge - Payment_method = 2
                else if($order->payment_method == 2) {
                    $order->status = \Model\Order::$status[11];
                    $order->payment_status = 'PayPal Initiated';
                    $order->save();
                    $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                    $apiContext->setConfig(['mode'=>'LIVE']);

                    $payer = new \PayPal\Api\Payer(); $payer->setPaymentMethod('paypal');

                    $pPDetails = new \PayPal\Api\Details(); $pPDetails->setSubtotal($order->subtotal)->setShipping($order->shipping_cost)->setTax($order->tax);
                    $pPAmount = new \PayPal\Api\Amount(); $pPAmount->setTotal(round($order->total,2))->setCurrency('USD')->setDetails($pPDetails);
                    $pPTransaction = new \PayPal\Api\Transaction(); $pPTransaction->setAmount($pPAmount)->setItemList($pPItems);
                    $pPTransArr[] = $pPTransaction;

                    //Live
                    $pPRedirectUrl = new \PayPal\Api\RedirectUrls(); $pPRedirectUrl->setReturnUrl("https://modernvice.com/checkout/confirmation/$order->ref_num")->setCancelUrl('https://modernvice.com/checkout/invoice?ref='.$invoice->ref_num);
                    //Local
//                $pPRedirectUrl = new \PayPal\Api\RedirectUrls(); $pPRedirectUrl->setReturnUrl("http://modernvice.dev/checkout/confirmation/$order->ref_num")->setCancelUrl('http://modernvice.dev/checkout/payment');

                    $payment = new \PayPal\Api\Payment();
                    $payment = $payment->setIntent('sale')->setPayer($payer)->setTransactions($pPTransArr)->setRedirectUrls($pPRedirectUrl)->create($apiContext);
                    if(strtolower($payment->getState()) == 'created'){
                        $order->payment_status = 'PayPal Login and Confirmation';$order->save();
                        $redirect = $payment->getLink('approval_url');
                        redirect($redirect);
                    } else {
                        $order->error_message = $payment->getFailureReason();
                        $order->status = \Model\Order::$status[2];
                        $order->payment_status = 'PayPal Canceled';
                        $order->save();
                        $n = new \Notification\ErrorHandler($payment->getFailureReason());
                        $_SESSION['notification'] = serialize($n);
                        redirect('/checkout/invoice?ref='.$_GET['ref']);
                    }
                }
            } else {
                $order->status = \Model\Order::$status[2];
                $n = new \Notification\ErrorHandler("Cannot find your invoice. Please call for customer support.");
                $_SESSION['notification'] = serialize($n);
                redirect('/checkout/invoice?ref='.$_GET['ref']);
            }
        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION['notification'] = serialize($n);
            redirect('/checkout/invoice?ref='.$_GET['ref']);
        }
    }

    function logCheckout(){
        if(isset($_POST) && isset($_SESSION['aCart'])){
            $ac = $_SESSION['aCart'];
            $checkout_fields = $ac->checkout_fields ? json_decode($ac->checkout_fields,true): [];
            foreach($_POST as $key=>$value){
                $checkout_fields[$key] = $value;
            }
            $ac->update_time = date('Y-m-d H:i:s.u');
            $ac->checkout_fields = json_encode($checkout_fields);
            $ac->save();
            echo json_encode(['status'=>"success"]);
        }
    }

    function shippingPriceUpdate(){
        if(isset($_POST['countryCode'])){
           $country = $_POST['countryCode'];
        } else if(isset($_SESSION['customerDetails']) && ($country = json_decode($_SESSION['customerDetails'],true)['ship_country'])){
        } else {
            $country = \Model\Shipping_Method::getItem(null,['where'=>"is_default = true"])->country;
        }
        if(isset($_POST['speed'])){
            $speed = $_POST['speed'];
            $_SESSION['speed'] = $speed;
        } else if(isset($_SESSION['speed'])){
            $speed = $_SESSION['speed'];
        } else {
            $speed = \Model\Shipping_Method::getItem(null,['where'=>"is_default = true"])->name;
        }
        $cartTotal = $_POST['cartTotal'];
        $shipping_method = \Model\Shipping_Method::getItem(null,['where'=>"country = '$country'"]);
        if(!$shipping_method) $shipping_method = \Model\Shipping_Method::getItem(null,['where'=>"is_default = true"]);
        $_SESSION['ship_id'] = $shipping_method->id;
        $discount = \Model\Coupon::checkDiscount();
        if(!$discount){
            unset($_SESSION['coupon']);
        }

        $discountAmt = $discount ? $discount->getDiscountAmount($cartTotal,$_SESSION['ship_id']) : 0;
        $cost = 0;
        $duty = 0;
        if(isset ($_POST['countryCode'])){
            $duty = get_duty($_POST['countryCode'],$_POST['cartTotal']);
        }
        $tax_name = 'DUTY';
        if($shipping_method) {
            $costs = json_decode($shipping_method->cost, true);
            $cost = $costs[0];
            if(count($costs) > 1){
                $cost += $costs[1]*($this->viewData->cart->count-1);
            }
            if($_POST['countryCode'] == 'US'){
                $tax_name = 'TAX';
                switch ($_POST['state']){
                    case "NY":
                        $duty = number_format(.08875*($cartTotal+$cost),2);
                        break;
                    case "NJ":
                        $duty = number_format(.06625*($cartTotal+$cost),2);
                        break;
                }
            }
        }
        $_SESSION['shipping_cost'] = $cost;
        if($cost){
            $this->toJson(['status'=>'success','ship_meth'=>$shipping_method->id,'discount'=>[$discount,number_format($discountAmt,2)] ,'duty'=>$duty, 'shipping'=>number_format($cost,2), 'cartTotal'=>number_format($cartTotal+$duty+$cost-$discountAmt,2),'taxName'=>$tax_name]);
        } else {
            $this->toJson(['status'=>'success','ship_meth'=>0,'discount'=>[$discount,number_format($discountAmt,2)] ,'duty'=>$duty, 'shipping'=>0, 'cartTotal'=>number_format($cartTotal+$duty+0-$discountAmt,2),'taxName'=>$tax_name]);
        }
    }

    private function emptyCartValidation(){
        if(!$this->viewData->cart->cart){
            redirect('/');
        }
    }

    private function customerCheckoutValidation(){
        switch($this->emagid->route['action']){
            case 'delivery':
                if(!isset($_SESSION['customerDetails']) || !array_key_exists('email',json_decode($_SESSION['customerDetails'],true))){
                    $n = new \Notification\MessageHandler('Please submit your email address to proceed!');
                    $_SESSION['notification'] = serialize($n);
                    redirect('/checkout/email');
                }
                break;
            case 'payment':
//                $deliveryParams = ['ship_first_name','ship_last_name','phone','ship_address','ship_city','ship_state','ship_zip'];
//                $arr = array_map(function($value){return array_key_exists($value,json_decode($_SESSION['customerDetails'],true));}, $deliveryParams);
//                if(!isset($_SESSION['customerDetails']) || !array_key_exists('email',json_decode($_SESSION['customerDetails'],true))){
//                    $n = new \Notification\MessageHandler('Please submit your email address to proceed!');
//                    $_SESSION['notification'] = serialize($n);
//                    redirect('/checkout/email');
//                } else if(!(count(array_unique($arr)) === 1 && end($arr) === true)){
//                    $n = new Notification\MessageHandler('Please fill out necessary shipment info to proceed!');
//                    $_SESSION['notification'] = serialize($n);
//                    redirect('/checkout/delivery');
//                }
                break;
        }
    }

    function addGiftCard(){
        $code = trim($_POST['gift-card-code']);
        if(\Model\Gift_Card::validateGiftCard($code)){
            $_SESSION['gift_card'] = $code;
            echo json_encode(['status'=>'success', 'message'=>'success', 'redirect'=>'/checkout/payment']);
        } else {
            echo json_encode(['status'=>'fail', 'message'=>'Invalid gift card code']);
        }
    }

    function removeGiftCard(){
        if(isset($_SESSION['gift_card']) && $_SESSION['gift_card']){
            unset($_SESSION['gift_card']);
            echo json_encode(['status'=>'success', 'message'=>'success', 'redirect'=>'/checkout/payment']);
        } else {
            echo json_encode(['status'=>'fail', 'message'=>'Gift card not present']);
        }
    }

}