<?php

class aboutController extends siteController {
    
    public function index(Array $params = [])
    {
        redirect('/');
        $this->configs['Meta Keywords'] = 'dji nyc, dji, drone, drone camera, drone accessories, drone equipment, dji product nyc, dji store nyc';
        $this->configs['Meta Description'] = 'DJI NYC Drone equipment & accessories, professional DJI products in New York City that are redefining industries by bringing new perspectives & technologies.';
        $this->configs['Meta Title'] = 'About DJI NYC Drones, Equipment, Accessories, Official DJI Store NYC';
        $this->loadView($this->viewData);
    }


    function terms(Array $params = []){
        $this->loadView($this->viewData);
    }
            
}