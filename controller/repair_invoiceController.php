<?php

require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class repair_invoiceController extends siteController
{

    public function index(Array $params = [])
    {

        redirect('/');
        $id = $params['invoice_number'];
        if(!$id){
            $n = new \Notification\ErrorHandler('Invalid service ticket invoice number');
            $_SESSION["notification"] = serialize($n);
            redirect('/');
        }

        $ticket = \Model\Service_Ticket::getItem(null, ['where' => ['invoice_number' => $id]]);

        if(!$ticket){
            $n = new \Notification\ErrorHandler('Invalid service ticket invoice number');
            $_SESSION["notification"] = serialize($n);
            redirect('/');
        }

        $this->viewData->shipping = $ticket->shipping;
        $this->viewData->ticket = $ticket;
        $estimateData = $ticket->getEstimateData();
        $this->viewData->estimateData = array_merge($estimateData['parts'], isset($estimateData['charges'])?$estimateData['charges']:[]);

        $this->loadView($this->viewData);
    }

    public function index_post()
    {
        $_SESSION['post'] = $_POST;
        if (isset($this->viewData->user) && $this->viewData->user) {
            $guest = false;
        } else {
            $guest = true;
        }

//        if (!isset($_POST['terms']) || is_null($_POST['terms']) || !$_POST['terms']) {
//            $n = new \Notification\ErrorHandler('You must accept the Terms and Conditions to proceed with checkout.');
//            $_SESSION["notification"] = serialize($n);
//            if ($guest) {
//                redirect(SITE_URL . 'orders/checkout?type=guest');
//            } else {
//                redirect(SITE_URL . 'orders/checkout');
//            }
//        }

        $ticket = \Model\Service_Ticket::getItem($_POST['ticket_id']);

        $estimateData = $ticket->getEstimateData();

        if(!$ticket || (count($estimateData['parts']) == 0 && count($estimateData['charges']) == 0)) {
            $n = new \Notification\ErrorHandler('Invalid service ticket invoice number');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'orders/checkout');
        }

        if($_SESSION['invoicePaid'] == $ticket->invoice_number ) {
            $n = new \Notification\ErrorHandler('Payment Processing Please wait');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL.'repair_invoice/'.$ticket->invoice_number);
        }

        if($ticket->service_status_id == \Model\Service_Status::getPaidStatus()->id ) {
            $n = new \Notification\ErrorHandler('Invoice Already Paid');
            $_SESSION["notification"] = serialize($n);
            redirect('/');
        }

//        if (count($this->viewData->cart->products) > 0 && (!is_null($this->viewData->user) || $guest)) {
        $order = \Model\Order::loadFromPost();

        if (!$guest) {
            $order->user_id = $this->viewData->user->id;
        } else {
            $order->user_id = 0;
        }
        $order->discount = $ticket->discount;
        $order->guest_id = session_id();
        $order->status = $order::$status[0];
        $order->ref_num = generateToken();
//        $order->shipping_method = 1;
//
//        if (is_null(\Model\Shipping_Method::getItem($order->shipping_method))) {
//            $n = new \Notification\ErrorHandler('You must choose a valid shipping method.');
//            $_SESSION["notification"] = serialize($n);
//            redirect(SITE_URL . 'cart');
//        }

        $order->insert_time = new DateTime();
        $order->insert_time = date_format($order->insert_time, 'm/d/Y H:i:s');

        if (!is_null($order->coupon_code)) {
            $coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $order->coupon_code . "'"]);
            $order->coupon_type = $coupon->discount_type;
            $order->coupon_amount = $coupon->discount_amount;
            if (is_null($coupon)
                || (!is_null($coupon)
                    && (time() < $coupon->start_time || time() >= $coupon->end_time
                        || \Model\Product::getPriceSum(implode(',', $this->viewData->cart->products)) < $coupon->min_amount
                        || \Model\Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]) >= $coupon->num_uses_all)
                )
            ) {
                $n = new \Notification\ErrorHandler('Invalid coupon.');
                $_SESSION['coupon'] = null;
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'cart');
            }
        } else {
            $order->coupon_code = null;
        }


        if ($order->save()) {


            if($estimateData['parts']){
                foreach ($estimateData['parts'] as $data) {
                    $product = \Model\Product::getItem($data['id']);
                    if(!$data['id']) {
                        $product = new stdClass();
                        $product->id    = $data['id'];
                        $product->name  = $data['name'];
                        $product->price = $data['price'];
                    }

                    $order_product = new \Model\Order_Product();
                    $order_product->details     = $product->name;
                    $order_product->order_id    = $order->id;
                    $order_product->product_id  = $product->id;
                    $order_product->quantity    =    1;
                    $order_product->unit_price  = $product->price;
                    $order->subtotal            += floatval($product->price);

                    $order_product->save();
                }
            }


            if(isset($estimateData['charges']) && $estimateData['charges']){
                $additionalCharge = 0;
                foreach ($estimateData['charges'] as $data) {
                    $additionalCharge += floatval($data['price']);
                }

                $order->additional_charge = $additionalCharge;
                $order->subtotal += $additionalCharge;
            }

            if($ticket->discount){
                $order->coupon_amount = floatval($ticket->discount);
                $order->discount = $ticket->discount;
            }




            if ($order->payment_method == 2) {
                $order->subtotal = $order->subtotal * 0.98;
            }


            $order->total = $order->subtotal;

            if (!is_null($order->coupon_code)) {
                $_SESSION['coupon'] = null;
                if ($coupon->discount_type == 1) {//$
                    $order->total = $order->total - $coupon->discount_amount;
                } else if ($coupon->discount_type == 2) {//%
                    $order->total = $order->total * (1 - $coupon->discount_amount / 100);
                }
            }
            if ($order->discount){
                $order->total -= $order->discount;
            }


            if ($order->ship_state == 'NY') {
                $order->tax_rate = 8.875;
                $order->tax = $order->total * ($order->tax_rate / 100);
                $order->total += $order->tax;
            } else if ($order->ship_state == 'NJ') {
                $order->tax_rate = 6.875;
                $order->tax = $order->total * ($order->tax_rate / 100);
                $order->total += $order->tax;
            } else {
                $order->tax_rate = null;
                $order->tax = null;
            }


            $order->comment = $_POST['comment'];

            $order->shipping_method = \Model\Shipping_Method::getItem(null, ['where' => ['is_default' => true]]);
            $costs = json_decode($order->shipping_method->cost);
            $order->shipping_method->cost = 0;
            $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
            foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                if (is_null($range) || trim($range) == '') {
                    $range = 0;
                }
                if ($order->total >= $range && isset($costs[$key])) {
                    $order->shipping_cost = $costs[$key];
                }
            }
            if($ticket->shipping){
                $order->shipping_cost = $ticket->shipping;
            }
            $order->shipping_method = $order->shipping_method->id;
            $order->total += $order->shipping_cost;


            if ($order->save()) {

                if (!$guest) {
                    if (isset($_POST['payment_profile']) && $_POST['payment_profile'] != "" && is_numeric($_POST['payment_profile'])) {
                        $payment_profile = \Model\Payment_Profile::getItem($_POST['payment_profile']);
                    } else {
                        $payment_profile = new \Model\Payment_Profile();
                    }

                    $payment_profile->user_id = $order->user_id;
                    $payment_profile->country = get_countries()[$order->bill_country];
                    $payment_profile->first_name = $order->bill_first_name;
                    $payment_profile->last_name = $order->bill_last_name;
                    $payment_profile->address = $order->bill_address;
                    $payment_profile->address2 = $order->bill_address2;
                    $payment_profile->city = $order->bill_city;
                    $payment_profile->state = $order->bill_state;
                    $payment_profile->zip = $order->bill_zip;
                    $payment_profile->cc_number = $order->cc_number;
                    $payment_profile->cc_expiration_month = $order->cc_expiration_month;
                    $payment_profile->cc_expiration_year = $order->cc_expiration_year;
                    $payment_profile->cc_ccv = $order->cc_ccv;
                    $payment_profile->save();

                    if (isset($_POST['shipping_address']) && $_POST['shipping_address'] != "" && is_numeric($_POST['shipping_address'])) {
                        $address = \Model\Address::getItem($_POST['shipping_address']);
                    } else {
                        $address = new \Model\Address();
                    }

                    $address->user_id = $order->user_id;
                    $address->country = get_countries()[$order->ship_country];
                    $address->first_name = $order->ship_first_name;
                    $address->last_name = $order->ship_last_name;
                    $address->address = $order->ship_address;
                    $address->address2 = $order->ship_address2;
                    $address->city = $order->ship_city;
                    $address->state = $order->ship_state;
                    $address->zip = $order->ship_zip;
                    $address->save();
                }

                if ($order->payment_method == 1) {

                    $transaction = new AuthorizeNetAIM;
                    $transaction->setFields(
                        array(
                            'amount' => $order->total,
                            'card_num' => $order->cc_number,
                            'exp_date' => $order->cc_expiration_month . '/' . $order->cc_expiration_year,
                            'first_name' => $order->bill_first_name,
                            'last_name' => $order->bill_last_name,
                            'address' => $order->bill_address,
                            'city' => $order->bill_city,
                            'state' => $order->bill_state,
                            'country' => get_countries()[$order->bill_country],
                            'zip' => $order->bill_zip,
                            'email' => ($guest) ? $order->email : $this->viewData->user->email,
                            'card_code' => $order->cc_ccv,
                            'ship_to_address' => $order->ship_address,
                            'ship_to_city' => $order->ship_city,
                            'ship_to_country' => get_countries()[$order->ship_country],
                            'ship_to_first_name' => $order->ship_first_name,
                            'ship_to_last_name' => $order->ship_last_name,
                            'ship_to_state' => $order->ship_state,
                            'ship_to_zip' => $order->ship_zip,
                            'phone' => $order->phone
                        )
                    );
                    $transaction->setCustomFields(['x_invoice_num' => $order->id]);
                    //$transaction->setCustomFields(['x_test_request'=>true]); //TEST MODE
                    $_SESSION['invoicePaid'] = $ticket->invoice_number;
                    $response = $transaction->authorizeAndCapture();
                    unset( $_SESSION['invoicePaid']);


                    $localTransaction = new \Model\Transaction();
                    $localTransaction->order_id = $order->id;
                    $localTransaction->authorize_net_data = serialize($response);
                    $localTransaction->ref_trans_id = $response->transaction_id;
                    $localTransaction->save();

                    if ($response->approved || $response->held) {
                        $this->sendOrderPlacedEmail($order, $guest);
                        $n = new \Notification\MessageHandler('Order successfully placed!', $guest);
                        $_SESSION["notification"] = serialize($n);



                        $_SESSION['purchase_conversion'] = true;
//                        if (isset($_SESSION['code']) || isset($_SESSION['price']) || isset($_SESSION['id_product'])) {
//                            $alert = \Model\Price_Alert::getItem(null, ['where' => " code =  '$code'"]);
//                            $alert->code = "";
//                            $alert->save();
//                            unset($_SESSION['code']);
//                            unset($_SESSION['price']);
//                            unset($_SESSION['id_product']);
//                        }


                        $a = 1;
                        if($estimateData['parts']){
                            foreach ($estimateData['parts'] as $data) {
                                $product = \Model\Product::getItem($data['id']);
                                if(!$data['id']) {
                                    $product = new stdClass();
                                    $product->id    = $data['id'];
                                    $product->name  = $data['name'];
                                    $product->description  = $data['name'];
                                    $product->price = $data['price'];
                                }
                                $desc = $product->description;
                                if (empty($product->description)) {
                                    $desc = "Replacement part";
                                } else {
                                    $desc = $product->description;
                                }

                                if (isset($_SESSION['price'])) {
                                    $code = $_SESSION['code'];
                                    $new_price = \Model\Price_Alert::getItem(null, ['where' => " code =  '$code'"]);
                                    if ($product->id == $new_price->product_id) {
                                        $fraud_price = $new_price->price;
                                    } else {
                                        $fraud_price = $product->price;
                                    }
                                } else {
                                    $fraud_price = $product->price;
                                }
                                
                                $quantity = 1;

                                $arr[$a++] = array(
                                    'ProductName' => $product->name,
                                    'ProductDescription' => $desc,
                                    'ProductSellingPrice' => number_format($fraud_price * $quantity, 2, '.', ''),
                                    'ProductQty' => $quantity,
                                    'ProductCostPrice' => number_format($fraud_price,2, '.', '')
                                );
                            }
                        }

                        if ($order->cc_number[0] == 4) {
                            $credit_card = "VISA";
                        } elseif ($order->cc_number[0] == 5) {
                            $credit_card = "MC";
                        } elseif ($order->cc_number[0] == 3) {
                            $credit_card = "AMEX";
                        } elseif ($order->cc_number[0] == 6) {
                            $credit_card = "DISC";
                        } else {
                            $credit_card = "OTHER";
                        }

                        $post_array = array(
                            //////// Required fields //////////////
                            'ApiLogin' => 'YwZzFUKX66lx',
                            'ApiKey' => '5rhsxiIkxDVvgiULV1lTxltcfeamlwUr',
                            'TransactionId' => time() . mt_rand(),
                            'OrderDate' => $order->insert_time,
                            'OrderNumber' => $order->id,
                            'BillingFirstName' => $order->bill_first_name,
                            'BillingMiddleName' => '',
                            'BillingLastName' => $order->bill_last_name,
                            'BillingCompany' => '',
                            'BillingAddress1' => $order->bill_address,
                            'BillingAddress2' => $order->bill_address2,
                            'BillingCity' => $order->bill_city,
                            'BillingState' => $order->bill_state,
                            'BillingZip' => $order->bill_zip,
                            'BillingCountry' => get_countries()[$order->bill_country],
                            'BillingEveningPhone' => $order->phone,
                            'BillingEmail' => ($guest) ? $order->email : $this->viewData->user->email,
                            'IPAddress' => $_SERVER["REMOTE_ADDR"],
                            'ShippingFirstName' => $order->ship_first_name,
                            'ShippingMiddleName' => '',
                            'ShippingLastName' => $order->ship_last_name,
                            'ShippingCompany' => '',
                            'ShippingAddress1' => $order->ship_address,
                            'ShippingAddress2' => $order->ship_address2,
                            'ShippingCity' => $order->ship_city,
                            'ShippingState' => $order->ship_state,
                            'ShippingZip' => $order->ship_zip,
                            'ShippingCountry' => get_countries()[$order->ship_country],
                            'ShippingEveningPhone' => $order->phone,
                            'ShippingEmail' => ($guest) ? $order->email : $this->viewData->user->email,
                            'ShippingCost' => number_format($order->shipping_cost,2),
                            'GrandTotal' => number_format($order->total, 2, '.', ''),
                            'CCType' => $credit_card,
                            'CCFirst6' => substr($order->cc_number, 0, 6),
                            'CCLast4' => substr($order->cc_number, -4),
                            'CIDResponse' => 'M',
                            'AVSCode' => 'Y',
                            'LineItems' => $arr,
                            /////////// Optional fields /////////////
                            'SiteName' => 'Kenjo' //TODO st-dev determine use
                        );

// Convert post array into a query string
                        $post_query = http_build_query($post_array);

// Do the POST
                        $ch = curl_init('https://www.eye4fraud.com/api/');
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec($ch);
                        curl_close($ch);

// Show response


                     /*   $post_array = array(
                            'ApiLogin' => 'YwZzFUKX66lx',
                            'ApiKey' => '5rhsxiIkxDVvgiULV1lTxltcfeamlwUr',
                            'Action' => 'getOrderStatus',
                            'OrderNumber' => $order->id
                        );
*/
// Convert post array into a query string
                        //$post_query = http_build_query($post_array);

// Do the POST
                       /* $ch = curl_init('https://www.eye4fraud.com/api/');
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec($ch);
                        curl_close($ch);*/

// Show response
                        //echo $response;
                        $order->status = \Model\Order::$status[0];
                        $order->save();

                        $ticketPaidStatus = \Model\Service_Status::getPaidStatus();
                        $ticket->service_status_id = $ticketPaidStatus->id;
                        $ticket->save();

                        $ticket->createLog('PAID');
                        //preg_match_all('~<value>(.*)</value>~', $response, $out_status);
                       /* @$fraud_status = $out_status[0][1];
                        if ($fraud_status == "<value>I</value>" || $fraud_status == "<value>A</value>" || $fraud_status == "<value>ALW</value>" || $fraud_status == "<value>U</value>") {

                            @$order->fraud_status =  $out_status[0][2];

                        }else{
                            $order->status = \Model\Order::$status[2];
                            @$order->fraud_status =  $out_status[0][2];
                            $order->save();
                        }*/
//                        $this->viewData->cart->products = [];
//                        $this->viewData->cart->productDetails = [];
//                        setcookie('cartProducts', implode(',', $this->viewData->cart->products), time() + 60 * 60 * 24 * 100, "/");
//                        setcookie('cartProductDetails',json_encode($this->viewData->cart->productDetails), time()+60*60*24*100, "/");
//echo '<script>window.location.replace("/order/' . $order->id.'");</script>';
                        redirect('/checkout/confirmation/' . $order->ref_num);
                    } else {

                        $ticketPaidFailedStatus = \Model\Service_Status::getPaidFailedStatus();
                        $ticket->service_status_id = $ticketPaidFailedStatus->id;
                        $ticket->save();
                        $ticket->createLog('PAID FAILED');
                        $order->status = \Model\Order::$status[2];
                        $order->save();

                        $n = new \Notification\ErrorHandler($response->response_reason_text);
                        $_SESSION["notification"] = serialize($n);

                        redirect('/repair_invoice/'.$ticket->invoice_number);
                    }/*
                    } else {
                        $order->status = \Model\Order::$status[2];
                        $order->save();
                        $n = new \Notification\ErrorHandler("Check your credit card information!");
                        $_SESSION["notification"] = serialize($n);
                        if ($guest) {
                            redirect(SITE_URL . 'orders/checkout?type=guest');
                        } else {
                            redirect(SITE_URL . 'orders/checkout');
                        }

                    } */
                } else {
                    $this->sendOrderPlacedEmail($order, $guest);

                    $n = new \Notification\MessageHandler('Order successfully placed!', $guest);
                    $_SESSION["notification"] = serialize($n);

                    $this->viewData->cart->products = [];
                    setcookie('cartProducts', implode(',', $this->viewData->cart->products), time() + 60 * 60 * 24 * 100, "/");

                    $_SESSION['purchase_conversion'] = true;
                    if (isset($_SESSION['code']) || isset($_SESSION['price']) || isset($_SESSION['id_product'])) {
                        $alert = \Model\Price_Alert::getItem(null, ['where' => " code =  '$code'"]);
                        $alert->code = "";
                        $alert->save();
                        unset($_SESSION['code']);
                        unset($_SESSION['price']);
                        unset($_SESSION['id_product']);
                    }
                    redirect('/checkout/confirmation/' . $order->ref_num);
                }
            } else {
                $n = new \Notification\ErrorHandler($order->errors);
                $_SESSION["notification"] = serialize($n);
                if ($guest) {
                    redirect(SITE_URL . '/');
                } else {
                    redirect(SITE_URL . '/');
                }
            }
        } else {
            $n = new \Notification\ErrorHandler($order->errors);
            $_SESSION["notification"] = serialize($n);
            if ($guest) {
                redirect(SITE_URL . '/');
            } else {
                redirect(SITE_URL . '/');
            }
        }
//        } else {
//            if (is_null($this->viewData->user) && !$guest) {
//                $n = new \Notification\ErrorHandler('You must be logged in to create an order, or checkout as a guest.');
//            }
//            if (count($this->viewData->cart->products) <= 0) {
//                $n = new \Notification\ErrorHandler('Your cart is empty.');
//            }
//            $_SESSION["notification"] = serialize($n);
//            redirect(SITE_URL . 'cart');
//        }
    }

    public function order(Array $params = [])
    {
        $this->viewData->order = \Model\Order::getItem($params['order_id']);
        if (is_null($this->viewData->order) || (!is_null($this->viewData->order->user_id) && (is_null($this->viewData->user) || $this->viewData->order->user_id != $this->viewData->user->id))) {
            redirect(SITE_URL);
        } else {
            $this->viewData->order->date = new DateTime($this->viewData->order->insert_time);
            $this->viewData->order->date = $this->viewData->order->date->format('M d Y');
            $this->viewData->order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $this->viewData->order->id]);
            foreach ($this->viewData->order_products as $order_product) {
                $order_product->product = \Model\Product::getItem($order_product->product_id);
            }
            $this->loadView($this->viewData);
        }
    }
   
    private function sendOrderPlacedEmail($order, $guest)
    {
        /*global $emagid;
        $emagid->email->from->email = 'orders-dji@emagid.com'; //TODO st-dev get modern vice email

        $email = new \Emagid\Email();
        if ($guest) {
            $email->addTo($order->email);
        } else {
            $email->addTo($this->viewData->user->email);
        }*/

        $ticket = Model\Service_Ticket::getItem($order->ticket_id);

        $html = '';
        foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
            $html .=
                '<tr style="height:75.0pt">
						<td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$order_product->details.'</span></p>
						</td>
                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$order_product->quantity.'</span></p>
						</td>
                        <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($order_product->quantity*$order_product->unit_price, 2).'</span></p>
						</td>
                        '.''.'
					</tr>';
        }
        
        $mailMaster = new \Email\MailMaster();
        $mergeFields = [
            'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
            'DATE' => date('Y-m-d h:i:s'),
            'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
            'SHIPPING' => $order->getShippingAddr(),
            'BILLING' => $order->getBillingAddr(),
            'SUBTOTAL' => number_format($order->subtotal, 2),
            'TAXFEE' => number_format($order->tax, 2),
            'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
            'TOTAL' => number_format($order->total, 2),
            'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
            'ITEMS' => $html
        ];

        if ($guest) {
            $to = $order->email;
        } else {
            $to = $this->viewData->user->email;
        }

        $mailMaster->setTo(['email' => $to, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('dji-repair-service')->send();

        /*$email->addBcc('info@djinyc.com'); //TODO st-dev get MV info email
        $email->addBcc('orders@djinyc.com'); //TODO st-dev get MV orders email
		$email->addTo('eitan@emagid.com');*/
    }

}