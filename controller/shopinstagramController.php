<?php

class shopInstagramController extends siteController {
	
    public function index(Array $params = [])
    {

        redirect('/');
        $limit = 12;
        $offset = 0;
        if(isset($_GET['page'])){
            $offset = ($_GET['page']-1) * $limit;
        }
        $query['limit'] = $limit;
        $query['offset'] = $offset;
        $this->viewData->shopInstagram = \Model\Shop_Instagram::getList($query);
//        $count =  \Model\Shop_Instagram::getCount($query);
//        $count = intval($count / $limit);
//        if(\Model\Shop_Instagram::getCount(['where'=>"active = 1"]) % $limit > 0){
//            $count++;
//        }

//        dd($_GET['page'], $offset,$limit,$this->viewData->shopInstagram, $query);
//        $this->viewData->pageCount = $count;
        $this->viewData->productsCount = \Model\Shop_Instagram::getCount(['where'=>"active = 1"]);
        $this->viewData->limit = $limit;
        $this->viewData->offset = $offset;
       // $this->viewData->InstaCount = \Model\Shop_Instagram::getCount($query);
        $this->configs['Meta Title'] = "Modern Vice - Shop Instagram";
        $this->loadView($this->viewData);
    }

    public function buildShopInstagram(){
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        $query = ['limit'=>$limit,'offset'=>$offset];

//        <img src="'.UPLOAD_URL.'shop_instagrams/'.$item->featured_image.'">
//        <img src="'.UPLOAD_URL.'shop_instagrams/573612a789d3a_banner3.jpg">
        $htmlArr = [];
        if($si = \Model\Shop_Instagram::getList($query)){
            foreach($si as $item){
                $returnData[] = ['url'=>$item->url,'image'=>UPLOAD_URL.'shop_instagrams/'.$item->featured_image,'title'=>$item->title,'display_date'=>$item->display_date];


                $html = '';
                $html .=
                    '<div class="grid-item">
                        <a href="'.$item->url.'">
                            <div class="mediaWrapper">
                                <img src="'.UPLOAD_URL.'shop_instagrams/'.$item->featured_image.'">

                                <div class="shopSocialCardInfo">
                                    <h4><span class="abs_vert_center">'.$item->title.'</span></h4>

                                    <div>
                                        <icon class="instagram"></icon>';
                                        if (strlen($item->url) > 0) {
                                            $html .= '<h5 class="btn">Shop the Look</h5>';
                                        }
                            $html .= '<span>'.date('M d', strtotime($item->display_date)).'</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>';
                $htmlArr[] = $html;
            }
            $this->toJson(['status'=>"success", 'appendHtml'=>$htmlArr]);
        } else {
            $this->toJson(['status'=>'failed']);
        }

    }

}