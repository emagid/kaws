<?php

class couponController extends siteController{

    function add(){
        $code = isset($_POST['code']) ? $_POST['code']: null;
        //check valid code & coupon exists
        $carbon = \Carbon\Carbon::now();
        $userId = $this->viewData->user ? $this->viewData->user->id: null;
        $activeCart = \Model\Cart::getActiveCart($userId,session_id());
        $couponObj = \Model\Coupon::getItem(null,['where'=>'lower(code) = lower(\''.$code.'\')']);
        $clearance = [];
        $categoryCouponBlock = [];
        $totals = [];
        $coup = [];
        if($couponObj) {
            foreach ($activeCart as $ac) {
                if ($ac->product_id) {
                    $prod = \Model\Product::getItem($ac->product_id);
                    $color = json_decode($ac->variation, true)['color'];
                    $size = json_decode($ac->variation, true)['size'];
                    $categoryIds = array_map(function ($i) {
                        return $i->id;
                    }, $prod->getAllCategories(true));
                    if ($ac->clearance && \Model\Clearance::checkClearance($prod->id, $color, $size)) {
                        if ($couponObj->active_clearance == 1) {
                            $totals[] = $prod->getPrice($color, $size, true);
                        } else {
                            $clearance[] = $prod->getPrice($color, $size, true);
                        }
//                } else if ($prod->isCouponBlocked()) {
                    } else if ($couponObj
                            &&(
                                ($couponObj->active_categories == '' || $couponObj->active_categories == null || json_decode($couponObj->active_categories,true) == [])
                                &&
                                ($couponObj->active_products == '' || $couponObj->active_products == null || json_decode($couponObj->active_products,true) == [])
                                &&
                                ($couponObj->active_shipping == '' || $couponObj->active_shipping == null || json_decode($couponObj->active_shipping,true) == [])
                        )) {
                        $categoryCouponBlock[] = $prod->getPrice($color, $size);
                    } else if(count(array_intersect(json_decode($couponObj->active_categories,true),$categoryIds)) != count($categoryIds) && count(json_decode($couponObj->active_categories,true)) > 0) {
                        $categoryCouponBlock[] = $prod->getPrice($color, $size);
                    } else if(in_array($prod->id,json_decode($couponObj->active_products,true)) === false && count(json_decode($couponObj->active_products,true)) > 0 ) {
                        $categoryCouponBlock[] = $prod->getPrice($color, $size);
                    } else if($couponObj->discount_type == 3 && (
                        (isset($_SESSION['ship_id']) && in_array($_SESSION['ship_id'],json_decode($couponObj->active_shipping,true)) === false)
                        ||
                        !isset($_SESSION['ship_id']))){
                        $categoryCouponBlock[] = $prod->getPrice($color, $size);
                    } else {
                        $totals[] = $prod->getPrice($color, $size);
                    }
                } else if ($ac->details) {
                    $detail = json_decode($ac->details, true);
                    $coup[] = $detail['amount'];
                }
            }
        } else {
            echo json_encode(['status'=>'failed','message'=>"Unable to apply this coupon. \nPlease confirm the spelling and minimum required amount to activate this coupon. \n(Coupons cannot be applied to clearance items or sale categories)"]);
            return;
        }
//        $totals = array_map(function($item) use ($clearance){
//            $prod = \Model\Product::getItem($item->product_id);
//            $color = json_decode($item->variation,true)['color'];
//            $size = json_decode($item->variation,true)['size'];
//            if(\Model\Clearance::checkClearance($prod->id,$color,$size)){
//                $clearance[] = $prod->getPrice($color,$size);
//                return 0;
//            } else {
//                return $prod->getPrice($color, $size);
//            }
//        },$activeCart);
        $subtotal = array_sum($totals);
        $clearanceAmt = array_sum($clearance);
        $ccbAmt = array_sum($categoryCouponBlock);
        $couponTotal = array_sum($coup);
        if($code && ($coupon = \Model\Coupon::getItem(null,['where'=>"lower(code) = lower('$code') and (min_amount = 0 or min_amount <= $subtotal)"])) && $subtotal != 0){
            if(($coupon->num_uses_all == 0 || \Model\Order::getCount(['where'=>['coupon_code'=>$coupon->code]]) < $coupon->num_uses_all) && $coupon->start_time <= strtotime($carbon) && $coupon->end_time > strtotime($carbon)){
                $_SESSION['coupon'] = $code;
                $discount = $coupon->getDiscountAmount($subtotal);
                $type = $coupon->discount_type;
                $amt = $coupon->discount_amount;
                $discountText = $type == 1 ? ' OFF': $amt.'% OFF';
                //TODO Sale tier Christmas 2016, remove when unnecessary
                $discountTotal = $coupon->applyCoupon($subtotal) - \Model\Sale::tiers($coupon->applyCoupon($subtotal)) + $clearanceAmt + $ccbAmt + $couponTotal;
                echo json_encode(['status'=>'success', 'discount'=>$discount, 'discountText'=>$discountText, 'discountTotal'=>'$'.number_format($discountTotal,2)]);
            } else {
                echo json_encode(['status'=>'failed','message'=>'Coupon Expired']);
            }
        } else {
            echo json_encode(['status'=>'failed','message'=>"Unable to apply this coupon. \nPlease confirm the spelling and minimum required amount to activate this coupon. \n(Coupons cannot be applied to clearance items or sale categories)"]);
        }
    }

    function remove(){
        if(isset($_SESSION['coupon']) && $_SESSION['coupon']){
            unset($_SESSION['coupon']);
            echo json_encode(['status'=>'success']);
        } else {
            echo json_encode(['status'=>'failed']);
        }
    }
}