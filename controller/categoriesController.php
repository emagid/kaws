<?php

class categoriesController extends siteController {

    public function index(Array $params = [])
    {
        redirect('/');
		$this->configs['Meta Title'] = 'DJI Products, Phantom Drones, Mavic, Spark, Inspire, Osmo, Ronin';
		$this->configs['Meta Description'] = 'DJI drones cameras & equipment, Phantom, Mavic, Spark, Inspire, Osmo, Ronin, DJI goggles & Industrial parts, gimbals, controllers, propulsion systems | DJI NYC';
		$this->configs['Meta Keywords'] = 'dji nyc, dji nyc product, dji nyc drone, dji nyc camera, dji nyc store';
		$this->loadView($this->viewData);
    }

    public function subcategory(Array $params = [])
    {
        redirect('/');
        $this->configs['Meta Title'] = "Subcategory";
        $this->loadView($this->viewData);
    }

	public function category(Array $params = []){
        redirect('/');
		$slug = $params['category_slug'];
		$mCategory = \Model\Category::getItem(null,['where'=>"slug = '{$slug}'"]);
        $isMain = \Model\Category::getCount(['where'=>"parent_category = {$mCategory->id}"]);
		$featured_categories = \Model\Category::getDisplayCategories($mCategory->id,$isMain);
		$this->viewData->display_categories = $featured_categories;
		$this->viewData->isPreorder = false;
		if(isset($params['category_slug']) && $params['category_slug'] != '' && !isset($params['subcategory'])){
			if($mCategory->meta_title){
				$this->configs['Meta Title'] = $mCategory->meta_title;
			}
			if($mCategory->meta_keywords){
				$this->configs['Meta Keywords'] = $mCategory->meta_keywords;
			}
			if($mCategory->meta_description){
				$this->configs['Meta Description'] = $mCategory->meta_description;
			}
			if(strtolower($params['category_slug']) == 'preorder'){
				$this->viewData->isPreorder = true;
			}
			$this->viewData->category = $mCategory;
			$this->viewData->featured_category = \Model\Category::getItem(null,['where'=>"parent_category = {$mCategory->id} and featured_category = 1"]);
			$activeSubcategories = array_map(function($item){return \Model\Category::getItem($item->sub_category_id)->new_arrivals;},$mCategory->getSubNav());
			$newIds = [];
			if(!empty($activeSubcategories)){
				foreach($activeSubcategories as $activeSubcategory){
					if(!empty($activeSubcategory)) {
						foreach (json_decode($activeSubcategory, true) as $value) {
							$newIds[] = $value;
						}
					}
				}
			}
			$newArrayIds = array_unique($newIds);
			$newArrivals = \Model\Product::getList(['where'=>"id in (".implode(',',$newArrayIds).")"]);
			$this->viewData->new_arrivals = $newArrivals;
			$this->view = 'index';
		}

		$mainCategory = \Model\Category::getItem(null,['where'=>"slug = '{$slug}'"]);
		$this->viewData->main_category = $mainCategory;

		$products = [];
		if(isset($params['subcategory'])) {
			$subCategory = \Model\Category::getItem(null, ['where' => "slug = '{$params['subcategory']}'"]);
			$this->viewData->sub_category = $subCategory;

			if ($this->viewData->user && $this->viewData->user->wholesale_id && ($ws = \Model\Wholesale::getItem(null,['where'=>"id = {$this->viewData->user->wholesale_id} and status = 1"]))) {
				$wsItems = $ws->getItemList();
				$wsIds = array_map(function ($item) {
					return $item->product_id;
				}, $wsItems);
				$imp = implode(',', $wsIds);
				$products = $imp ? \Model\Product::getList(['where' => "id in (select product_id from product_categories where category_id = $subCategory->id) and id in ($imp) and type = '1'"]) : [];
			} else {
				$products = \Model\Product::getList(['where' => "id in (select product_id from product_categories where category_id = $subCategory->id) and type = '1'"]);
				/** Get main products */
			}
		}
		$this->viewData->products = $products;

		$this->loadView($this->viewData);
	}
    public function all(Array $params = []){
        redirect('/');
        $slug = $params['category_slug'];
        $mCategory = \Model\Category::getItem(null,['where'=>"slug = '{$slug}'"]);
        $parentCat = $mCategory->getParent();
        if($parentCat == ''){
            $parentCat = $mCategory;
        }
        $this->viewData->parentCat = $parentCat;
        $this->viewData->category= $mCategory;
        $products = \Model\Product::getList(['where'=>"id in (select product_id from product_categories where category_id = $mCategory->id) and type = '1'"]); /** Get main products */
        $this->viewData->products = $products;
        $relative_products = \Model\Product::getList(['where'=>"id in (select product_id from product_categories where category_id = $mCategory->id) and type != '1'",'orderBy'=>'type']); /** Get main products */
        $this->viewData->relative_products = $relative_products;
        $this->loadView($this->viewData);
    }
	public function category_old(Array $params = []){
        redirect('/');
		$slug = $params['category_slug'];
		if($slug == 'gift-cards'){
			redirect('/gift_cards');
		}
		$mCategory = \Model\Category::getItem(null,['where'=>"slug = '{$slug}'"]);
        $featured_categories = \Model\Category::getDisplayCategories($mCategory->id);
        $this->viewData->display_categories = $featured_categories;
		// /collections/{main category}
		if(isset($params['category_slug']) && $params['category_slug'] != '' && !isset($params['subcategory'])){
			if($mCategory->meta_title){
				$this->configs['Meta Title'] = $mCategory->meta_title;
			}
			if($mCategory->meta_keywords){
				$this->configs['Meta Keywords'] = $mCategory->meta_keywords;
			}
			if($mCategory->meta_description){
				$this->configs['Meta Description'] = $mCategory->meta_description;
			}
			$this->viewData->category = $mCategory;
			$this->viewData->featured_category = \Model\Category::getItem(null,['where'=>"parent_category = {$mCategory->id} and featured_category = 1"]);
			$activeSubcategories = array_map(function($item){return \Model\Category::getItem($item->sub_category_id)->new_arrivals;},$mCategory->getSubNav());
			$newIds = [];
			foreach($activeSubcategories as $activeSubcategory){
				foreach(json_decode($activeSubcategory,true) as $value){
					$newIds[] = $value;
				}
			}
			$newArrayIds = array_unique($newIds);
			$newArrivals = \Model\Product::getList(['where'=>"id in (".implode(',',$newArrayIds).")"]);
			$this->viewData->new_arrivals = $newArrivals;
			$this->view = 'index';
		}

		// /collections/{main category}/{subcategory}
		if(isset($params['subcategory']) && $params['subcategory'] != '' && $mCategory != ''){
			$slug = $params['subcategory'];
			$category = \Model\Category::getItem(null,['where'=>"slug = '{$slug}' and parent_category = {$mCategory->id}"])?:"all";
			if($category != 'all' && $category->meta_title){
				$this->configs['Meta Title'] = $category->meta_title;
			}
			if($category->meta_keywords){
				$this->configs['Meta Keywords'] = $category->meta_keywords;
			}
			if($category->meta_description){
				$this->configs['Meta Description'] = $category->meta_description;
			}
			if($params['subcategory'] == 'all' || strpos(strtolower($params['subcategory']),'shop-by') !== false){
				//Apply filter on grabbed products
				$query = ['where'=>["id in (select product_id from product_categories where category_id = $mCategory->id and product_categories.active = 1)"]];
				$orderByCategory = ' and category_id='.$mCategory->id;
//				$query = ['where'=>"product_categories.category_id = $mCategory->id and product.active = 1 and product_categories.active = 1"];

				if(strpos(strtolower($params['subcategory']),'shop-by') == 0){
					$filterActive = str_replace('shop-by-','',$params['subcategory']);
					$this->viewData->filterActive = $filterActive;
				}
			} else {
				//Apply filter on grabbed products
				$query = ['where'=>["id in (select product_id from product_categories where category_id = $category->id and product_categories.active = 1)"]];
				$orderByCategory = ' and category_id='.$category->id;
//				$query = ['where'=>"product_categories.category_id = $category->id and product.active = 1 and product_categories.active = 1"];
			}

			$sortBy = isset($_GET['sort_by'])?\Model\Product::$SORT_MAP[intval($_GET['sort_by'])]:null;
			$sortNum = isset($_GET['sort_by']) ? $_GET['sort_by']: null;

			$limit = 8;
			$offset = 0;

			//Filter functionality (build query)
			$query['orderBy'][] = "(select min(display_order) from product_categories where product.id = product_categories.product_id and product_categories.active=1 $orderByCategory),id";
			if($sortBy){
				$query['orderBy'] = [$sortBy];
//				unset($_GET['sort_by']);
			}
			if(isset($_GET)){
				foreach($_GET as $key=>$value) {
					if ($key == 'heel_height') {
						$val = explode(',', urldecode($value));
						$query['where'][] = "product.$key in (".implode(',',$val).")";
					} else if($key == 'color' || $key == 'size'){
						$val = explode(',', urldecode($value));
						$query['where'][] = "product.$key similar to '%\"(" . implode('|', $val) . ")\"%'";
					}
				}
			}

//			$query['where'][] = '(id not in (select product_id from clearance where now() between start_date and end_date and active=1) or id in (select product_id from clearance where current_quantity = 0 and active=1))';
			$query['where'][] = 'product.active=1';
			$sqlCount = \Model\Category::filterSql($query);
			$query['limit'] = $limit;
			$query['offset'] = $offset;

			$sql = \Model\Category::filterSql($query);
			$productPreLimit = \Model\Product::getList(['sql'=>$sqlCount]);
			$this->viewData->productsCount = count($productPreLimit);
			$products = \Model\Product::getList(['sql'=>$sql]);
			$colorList = [];
			$heel_height = [];
			foreach($productPreLimit as $product){
				$color = \Model\Product_Attributes::getColorList($product->id,0);
				if($color){
					$arrMap = array_map(function($item){return $item->id;},$color);
					$colorList = array_unique(array_merge($colorList,$arrMap));
				}
				$heel_height[$product->heel_height] = $product->heel_height.'MM';
			}
			if($colorList){
				$colorList = array_map(function($item){return \Model\Color::getItem($item);},$colorList);
			}
			if($category == null){
				$n = new Notification\ErrorHandler("Invalid category");
				$_SESSION['notification'] = serialize($n);
				redirect(SITE_URL.'collections/'.$params['category_slug']);
			}
			ksort($heel_height);
			$newArrivals = $category != 'all' && $category->new_arrivals ? array_map(function($item){return \Model\Product::getItem($item);},json_decode($category->new_arrivals,true)): null;
			$this->viewData->filters->colors = $colorList;
			$this->viewData->filters->heel_height = $heel_height;
			$this->viewData->parent = $params['category_slug'];
			$this->viewData->category = $category;
			$this->viewData->mCategory = $mCategory;
			$this->viewData->mainCategory = \Model\Category::getList(['where'=>"parent_category = 0"]);
			$this->viewData->products = $products;
			$this->viewData->new_arrivals = $newArrivals;
			$this->viewData->limit = $limit;
			$this->viewData->offset = $offset;
			$this->viewData->sortBy = $sortNum;
		}
		$this->loadView($this->viewData);
	}

	public function search(Array $params = []){
        redirect('/');
		$query = [];
		$limit = 8;
		$offset = 0;
		if(!isset($_GET['term']) || !$_GET['term']){
			redirect('/');
		}
		$keywords = $_GET['term'];
		$query['where'] = [];
		$sortBy = isset($_GET['sort_by'])?\Model\Product::$SORT_MAP[intval($_GET['sort_by'])]:null;
		$sortNum = isset($_GET['sort_by']) ? $_GET['sort_by']: null;
		if($sortBy){
			unset($_GET['sort_by']);
		}
		if (is_numeric($keywords)) {
			$query['where'][] = 'CAST (id as TEXT) like \'%'.$keywords.'%\'';
		}
		//Apply filters
		foreach($_GET as $key=>$value) {
			$colorSql = \Model\Color::getList(['where'=>"lower(name) like '%".strtolower($keywords)."%'"]);
			$colorStr = "'%\"".implode("%\"','\"%",array_map(function($item){return $item->id;},$colorSql))."\"%'";
			$explodeTags = explode(' ', strtolower(urldecode($keywords)));
			$tags = "'%" . implode("%','%", $explodeTags) . "%'";
			switch($key){
				case 'term':
					//old search logic
//					$query['where'][] = 'lower(name) like \'%' . strtolower(urldecode($keywords)) . '%\'';
//					$query['where'][] = 'lower(tags) like any (array[' . $tags . '])';

					$query['where'][] = 'lower(name) like all (array['.$tags.'])';
					$query['where'][] = 'lower(tags) like any (array[' . $tags . '])';
					if($colorSql) {
						$query['where'][] = 'color like any (array[' . $colorStr . '])';
					}

					$query['where'] = ["(" . implode(' OR ', $query['where']) . ")"];
					break;
				case 'heel_height':
					$val = explode(',', urldecode($value));
					$query['where'][] = "product.$key in (".implode(',',$val).")";
					break;
				case 'size':
				case 'color':
					$val = explode(',', urldecode($value));
					$query['where'][] = "product.$key similar to '%\"(" . implode('|', $val) . ")\"%'";
					break;
			}
		}
		$query['where'][] = 'product.active=1';
		$query['where'] = [implode(' AND ', $query['where'])];
		$query['orderBy'] = $sortBy ? [$sortBy]:'';

		$productsNoLimit = \Model\Product::getList(['sql'=>\Model\Category::filterSql($query)]);
		$productsCount = count($productsNoLimit);
		$query['limit'] = $limit;
		$query['offset'] = $offset;

		$products = \Model\Product::getList(['sql'=>\Model\Category::filterSql($query)]);
		$colorList = [];
		$heel_height = [];
		foreach($productsNoLimit as $product){
			$color = \Model\Product_Attributes::getColorList($product->id,0);
			if($color){
				$arrMap = array_map(function($item){return $item->id;},$color);
				$colorList = array_unique(array_merge($colorList,$arrMap));
				$heel_height[$product->heel_height] = $product->heel_height.'MM';
			}
		}
		if($colorList){
			$colorList = array_map(function($item){return \Model\Color::getItem($item);},$colorList);
		}
		$impColor = '\'%'.implode('%\',\'%',explode(' ',strtolower($keywords))).'%\'';
		$searchedColors = ['where'=>"lower(name) like all (array[$impColor])"];
		$searchColors = \Model\Color::getList($searchedColors);
		ksort($heel_height);
//		dd($productsCount,$products,\Model\Category::filterSql($query));
		$this->viewData->mCategory = '';
		$this->viewData->filters->heel_height = $heel_height;
		$this->viewData->products = $products;
		$this->viewData->filters->colors = $colorList;
		$this->viewData->productsCount = $productsCount;
		$this->viewData->category = $this->viewData->parent = 'all';
		$this->viewData->limit = $limit;
		$this->viewData->offset = $offset;
		$this->viewData->sortBy = $sortNum;
		$this->viewData->searchColors = $searchColors;
		parent::index($params);
	}
}