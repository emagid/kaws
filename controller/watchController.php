<?php

class watchController extends siteController {
	
    public function index(Array $params = [])
    {

        redirect('/');
        $this->configs['Meta Title'] = 'DJI NYC Community, DJI Drone Video, DJI Camera | Camrise DJI NYC';
        $this->configs['Meta Description'] = 'DJI NYC community allows you to express your creativity & expertise, share your DJI drone videos & become an honored number of our community | Camrise DJI NYC';
        $this->configs['Meta Keywords'] = 'dji nyc, dji nyc community, dji drone video, dji camera video';
        $this->loadView($this->viewData);
    }

    public function upload(){
        $obj = new \Model\Watch();
        foreach($_FILES as $fileType=>$file){
            if ($file['error'] == 0){
                $s3 = new EmagidService\S3();
                $name = $s3->upload($_FILES[$fileType]);
                $obj->$fileType = $name;
                $obj->save();
            }
        }
        redirect('/watch');
    }
}