<?php

class wholesaleController extends siteController{

    public function register(){

        redirect('/');
        $this->configs['Meta Title'] = 'Register For a Wholesale Account | Official DJI Store NYC, Camrise';
        $this->configs['Meta Description'] = 'Register for a wholesale account, complete wholesale registration for our official DJI store in New York City, brought to you by Camrise. Drones & accessories.';
        $this->configs['Meta Keywords'] = 'dji nyc, dji nyc wholesale, dji nyc drone, dji nyc drone camera, dji nyc drone accessories, dji nyc drone equipment';
        $this->loadView();
    }
    public function register_post(){
        $user = new \Model\User($_POST);
        $wholesale = new \Model\Wholesale($_POST);
        $hash = \Emagid\Core\Membership::hash($user->password);
        $user->password = $hash['password'];
        $user->hash = $hash['salt'];
        if(isset($_POST['email']) && filter_var($_POST['email'],FILTER_SANITIZE_EMAIL) && $wholesale->save()){
            $user->wholesale_id = $wholesale->id;
            $user->save();
            $customerRole = new \Model\User_Roles();
            $customerRole->role_id = 2;
            $customerRole->user_id = $user->id;
            $customerRole->save();
            $_SESSION['notification'] = serialize(new \Notification\MessageHandler('Thank you for submitting to be a wholesaler. You will receive a notification when you are approved.'));
            redirect('/');
        } else {
            $_SESSION['notification'] = serialize(new \Notification\ErrorHandler('Your email is invalid or is already in the system. Please verify your email address.'));
            redirect('/wholesale/register');
        }
    }

    public function parts(){

        redirect('/');
        if($this->viewData->wholesale){
            global $emagid; $db = $emagid->getDb();
            //print_r(json_encode($this->viewData->wholesale));
            $wsID = $this->viewData->wholesale->id;
            $sql = "SELECT wsi.*, p.name FROM wholesale_items wsi JOIN product_categories pc ON wsi.product_id = pc.product_id JOIN product p ON p.id = wsi.product_id WHERE pc.category_id = 63 AND wsi.wholesale_id = $wsID";

            $wholesale_items = $db->execute($sql);
            $products = [];
            foreach($wholesale_items as $wsi){
                $products[] = \Model\Product::getItem($wsi['product_id']);
            }
            $this->viewData->products = $products;
            $this->viewData->relative_products = $relative_products;
            $this->loadView($this->viewData);
        }
    }
}