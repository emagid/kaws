<?php

use Zendesk\API\HttpClient as ZendeskAPI;
require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class ordersController extends siteController
{

    public function index(Array $params = [])
    {
        if (!is_null($this->viewData->user)) {
            $this->viewData->orders = \Model\Order::getList(['where' => 'user_id = ' . $this->viewData->user->id]);
            foreach ($this->viewData->orders as $order) {
                $order->date = new DateTime($order->insert_time);
                $order->date = date_format($order->date, 'm/d/Y H:i:s');
            }
            $this->loadView($this->viewData);
        } else {
            redirect(SITE_URL);
        }
    }

    public function checkout(Array $params = [])
    {

        if (isset($_GET['type']) && $_GET['type'] == 'guest') {
            $guest = true;
        } else {
            $guest = false;
        }
        if (count($this->viewData->cart->products) > 0 && (!is_null($this->viewData->user) || $guest)
            && isset($_COOKIE['shippingMethod']) && !is_null(\Model\Shipping_Method::getItem($_COOKIE['shippingMethod']))
        ) {
            $order = new \Model\Order();

            $order_products = [];
            $order->subtotal = 0;

            foreach ($this->viewData->cart->products as $productId) {
                $product = \Model\Product::getItem($productId);
                $order_product = new \Model\Order_Product();
                $order_product->product_id = $productId;
                $order_product->quantity = array_count_values($this->viewData->cart->products)[$product->id];
                $order_product->unit_price = $product->price;
                $order_product->product = $product;
                $order->subtotal += $product->price;
                if (!in_array($order_product, $order_products)) {
                    $order_products[] = $order_product;
                }
            }
            $order->total = $order->subtotal;

            $this->viewData->shipping_methods = [];
            $shipping_methods = \Model\Shipping_Method::getList(['orderBy'=>'id asc']);
            foreach ($shipping_methods as $shipping_method){
                $costs = json_decode($shipping_method->cost);
                $shipping_method->cost = 0;
                $shipping_method->cart_subtotal_range_min = json_decode($shipping_method->cart_subtotal_range_min);
                foreach($shipping_method->cart_subtotal_range_min as $key=>$range){
                    if (is_null($range) || trim($range) == ''){
                        $range = 0;
                    }
                    if ($order->subtotal >= $range && isset($costs[$key])){
                        $shipping_method->cost = $costs[$key];
                    }
                }
                $this->viewData->shipping_methods[] = $shipping_method;
            }

            if (isset($_COOKIE['shippingMethod'])){
                $this->viewData->shipping_method = $_COOKIE['shippingMethod'];
            } else {
                $this->viewData->shipping_method = \Model\Shipping_Method::getItem(null, ['orderBy'=>'is_default desc']);
                $this->viewData->shipping_method = $this->viewData->shipping_method->id;
                setcookie('shippingMethod', $this->viewData->shipping_method, time()+60*60*24*100, "/");
            }
            $this->viewData->savings = '0.00';
            if (isset($_SESSION['coupon']) && !is_null($_SESSION['coupon'])) {
                $coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $_SESSION['coupon'] . "'"]);
                if (!is_null($coupon)
                    && time() >= $coupon->start_time && time() < $coupon->end_time
                    && $order->subtotal >= $coupon->min_amount
                    && \Model\Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]) < $coupon->num_uses_all
                ) {
                    $this->viewData->coupon = $coupon;
                    if ($this->viewData->coupon->discount_type == 1) {//$
                        $this->viewData->savings = number_format($this->viewData->coupon->discount_amount, 2);
                        $order->total = $order->total - $this->viewData->coupon->discount_amount;
                    } else if ($this->viewData->coupon->discount_type == 2) {//%
                        $this->viewData->savings = number_format($order->total * $this->viewData->coupon->discount_amount / 100, 2);
                        $order->total = $order->total * (1 - $this->viewData->coupon->discount_amount / 100);
                    }
                } else {
                    $this->viewData->coupon = null;
                }
            } else {
                $this->viewData->coupon = null;
            }

            $order->shipping_method = \Model\Shipping_Method::getItem($_COOKIE['shippingMethod']);
            $costs = json_decode($order->shipping_method->cost);
            $order->shipping_method->cost = 0;
            $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
            foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                if (is_null($range) || trim($range) == '') {
                    $range = 0;
                }
                if ($order->total >= $range && isset($costs[$key])) {
                    $order->shipping_cost = $costs[$key];
                }
            }
            $order->shipping_method = $order->shipping_method->id;
            $order->total += $order->shipping_cost;

            $this->viewData->guest_checkout = $guest;
            $this->viewData->order = $order;
            $this->viewData->order_products = $order_products;

            if (!is_null($this->viewData->user)) {
                $this->viewData->addresses = \Model\Address::getList(['where' => 'user_id = ' . $this->viewData->user->id]);
                $this->viewData->payment_profiles = \Model\Payment_Profile::getList(['where' => 'user_id = ' . $this->viewData->user->id]);
                foreach ($this->viewData->payment_profiles as $profile) {
                    $profile->cc_number = '';
                    $profile->cc_expiration_month = '';
                    $profile->cc_expiration_year = '';
                    $profile->cc_ccv = '';
                }
            } else {
                $this->viewData->addresses = null;
                $this->viewData->payment_profiles = null;
            }

            $this->loadView($this->viewData);
        } else {
            if (is_null($this->viewData->user) && !$guest) {
                $n = new \Notification\ErrorHandler('You must be logged in to create an order, or checkout as a guest.');
            }
            if (count($this->viewData->cart->products) <= 0) {
                $n = new \Notification\ErrorHandler('Your cart is empty.');
            }
            if (!isset($_COOKIE['shippingMethod']) || is_null(\Model\Shipping_Method::getItem($_COOKIE['shippingMethod']))) {
                $n = new \Notification\ErrorHandler('Please select a valid shipping method.');
            }
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'cart');
        }
    }

    public function checkout_post()
    {
        $_SESSION['post'] = $_POST;
        if (isset($_GET['type']) && $_GET['type'] == 'guest') {
            $guest = true;
            if (!isset($_POST['email']) || is_null($_POST['email']) || trim($_POST['email']) == '') {
                $n = new \Notification\ErrorHandler('You must enter a valid email to checkout as a guest.');
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'orders/checkout?type=guest');
            }
        } else {
            $guest = false;
        }

        if (!isset($_POST['terms']) || is_null($_POST['terms']) || !$_POST['terms']) {
            $n = new \Notification\ErrorHandler('You must accept the Terms and Conditions to proceed with checkout.');
            $_SESSION["notification"] = serialize($n);
            if ($guest) {
                redirect(SITE_URL . 'orders/checkout?type=guest');
            } else {
                redirect(SITE_URL . 'orders/checkout');
            }
        }

        if (count($this->viewData->cart->products) > 0 && (!is_null($this->viewData->user) || $guest)) {
            $order = \Model\Order::loadFromPost();

            if (!$guest) {
                $order->user_id = $this->viewData->user->id;
            } else {
                $order->user_id = 0;
            }
            $order->guest_id = session_id();
            $order->status = $order::$status[0];

            if (is_null(\Model\Shipping_Method::getItem($order->shipping_method))) {
                $n = new \Notification\ErrorHandler('You must choose a valid shipping method.');
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'cart');
            }

            $order->insert_time = new DateTime();
            $order->insert_time = date_format($order->insert_time, 'm/d/Y H:i:s');

            if (!is_null($order->coupon_code)) {
                $coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $order->coupon_code . "'"]);
                $order->coupon_type = $coupon->discount_type;
                $order->coupon_amount = $coupon->discount_amount;
                if (is_null($coupon)
                    || (!is_null($coupon)
                        && (time() < $coupon->start_time || time() >= $coupon->end_time
                            || \Model\Product::getPriceSum(implode(',', $this->viewData->cart->products)) < $coupon->min_amount
                            || \Model\Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]) >= $coupon->num_uses_all)
                    )
                ) {
                    $n = new \Notification\ErrorHandler('Invalid coupon.');
                    $_SESSION['coupon'] = null;
                    $_SESSION["notification"] = serialize($n);
                    redirect(SITE_URL . 'cart');
                }
            } else {
                $order->coupon_code = null;
            }


            if ($order->save()) {


                $order_products = [];
                //                $order->subtotal = 0;
                foreach ($this->viewData->cart->products as $token=>$productId) {
                    $product = \Model\Product::getItem($productId);
                    $order_product = new \Model\Order_Product();
                    $order_product->order_id = $order->id;
                    $order_product->product_id = $productId;
                    $order_product->quantity = 1;
                    $order_product->unit_price = $product->price;
                    $order_product->details = json_encode($this->viewData->cart->productDetails[$token]);

                    //                    if (!in_array($product->id, $order_products)) {
                    //                        $order_products[] = $product->id;
                        $order_product->save();

                        if (isset($_SESSION['price'])) {
                            $code = $_SESSION['code'];


                            $new_price = \Model\Price_Alert::getItem(null, ['where' => " code =  '$code'"]);
                            if ($product->id == $new_price->product_id) {
                                $order->subtotal += $new_price->price * $order_product->quantity;

                            } else {
                                $order->subtotal += $order_product->unit_price * $order_product->quantity;

                            }


                        } else {
                            $order->subtotal += $order_product->unit_price * $order_product->quantity;
                        }


                    //                    }
                }

                if ($order->payment_method == 2) {
                    $order->subtotal = $order->subtotal * 0.98;
                }

                $order->total = $order->subtotal;

                if (!is_null($order->coupon_code)) {
                    $_SESSION['coupon'] = null;
                    if ($coupon->discount_type == 1) {//$
                        $order->total = $order->total - $coupon->discount_amount;
                    } else if ($coupon->discount_type == 2) {//%
                        $order->total = $order->total * (1 - $coupon->discount_amount / 100);
                    }
                }

                if ($order->ship_state == 'NY') {
                    $order->tax_rate = 8.875;
                    $order->tax = $order->total * ($order->tax_rate / 100);
                    $order->total += $order->tax;
                } else {
                    $order->tax_rate = null;
                    $order->tax = null;
                }


                $order->comment = $_POST['comment'];

                $order->shipping_method = \Model\Shipping_Method::getItem($_COOKIE['shippingMethod']);
                $costs = json_decode($order->shipping_method->cost);
                $order->shipping_method->cost = 0;
                $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
                foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                    if (is_null($range) || trim($range) == '') {
                        $range = 0;
                    }
                    if ($order->total >= $range && isset($costs[$key])) {
                        $order->shipping_cost = $costs[$key];
                    }
                }
                $order->shipping_method = $order->shipping_method->id;
                $order->total += $order->shipping_cost;


                if ($order->save()) {

                    if (!$guest) {
                        if (isset($_POST['payment_profile']) && $_POST['payment_profile'] != "" && is_numeric($_POST['payment_profile'])) {
                            $payment_profile = \Model\Payment_Profile::getItem($_POST['payment_profile']);
                        } else {
                            $payment_profile = new \Model\Payment_Profile();
                        }

                        $payment_profile->user_id = $order->user_id;
                        $payment_profile->country = get_countries()[$order->bill_country];
                        $payment_profile->first_name = $order->bill_first_name;
                        $payment_profile->last_name = $order->bill_last_name;
                        $payment_profile->address = $order->bill_address;
                        $payment_profile->address2 = $order->bill_address2;
                        $payment_profile->city = $order->bill_city;
                        $payment_profile->state = $order->bill_state;
                        $payment_profile->zip = $order->bill_zip;
                        $payment_profile->cc_number = $order->cc_number;
                        $payment_profile->cc_expiration_month = $order->cc_expiration_month;
                        $payment_profile->cc_expiration_year = $order->cc_expiration_year;
                        $payment_profile->cc_ccv = $order->cc_ccv;
                        $payment_profile->save();

                        if (isset($_POST['shipping_address']) && $_POST['shipping_address'] != "" && is_numeric($_POST['shipping_address'])) {
                            $address = \Model\Address::getItem($_POST['shipping_address']);
                        } else {
                            $address = new \Model\Address();
                        }

                        $address->user_id = $order->user_id;
                        $address->country = get_countries()[$order->ship_country];
                        $address->first_name = $order->ship_first_name;
                        $address->last_name = $order->ship_last_name;
                        $address->address = $order->ship_address;
                        $address->address2 = $order->ship_address2;
                        $address->city = $order->ship_city;
                        $address->state = $order->ship_state;
                        $address->zip = $order->ship_zip;
                        $address->save();
                    }

                    if ($order->payment_method == 1) {

                        $transaction = new AuthorizeNetAIM;
                        $transaction->setFields(
                            array(
                                'amount' => $order->total,
                                'card_num' => $order->cc_number,
                                'exp_date' => $order->cc_expiration_month . '/' . $order->cc_expiration_year,
                                'first_name' => $order->bill_first_name,
                                'last_name' => $order->bill_last_name,
                                'address' => $order->bill_address,
                                'city' => $order->bill_city,
                                'state' => $order->bill_state,
                                'country' => get_countries()[$order->bill_country],
                                'zip' => $order->bill_zip,
                                'email' => ($guest) ? $order->email : $this->viewData->user->email,
                                'card_code' => $order->cc_ccv,
                                'ship_to_address' => $order->ship_address,
                                'ship_to_city' => $order->ship_city,
                                'ship_to_country' => get_countries()[$order->ship_country],
                                'ship_to_first_name' => $order->ship_first_name,
                                'ship_to_last_name' => $order->ship_last_name,
                                'ship_to_state' => $order->ship_state,
                                'ship_to_zip' => $order->ship_zip,
                                'phone' => $order->phone
                            )
                        );
                        $transaction->setCustomFields(['x_invoice_num' => $order->id]);
                        //$transaction->setCustomFields(['x_test_request'=>true]); //TEST MODE
                        $response = $transaction->authorizeOnly();

                        $localTransaction = new \Model\Transaction();
                        $localTransaction->order_id = $order->id;
                        $localTransaction->authorize_net_data = serialize($response);
                        $localTransaction->ref_trans_id = $response->transaction_id;
                        $localTransaction->save();

                        if ($response->approved || $response->held) {
                            $this->sendOrderPlacedEmail($order, $guest);
                            $n = new \Notification\MessageHandler('Order successfully placed!', $guest);
                            $_SESSION["notification"] = serialize($n);

                           

                            $_SESSION['purchase_conversion'] = true;
                            if (isset($_SESSION['code']) || isset($_SESSION['price']) || isset($_SESSION['id_product'])) {
                                $alert = \Model\Price_Alert::getItem(null, ['where' => " code =  '$code'"]);
                                $alert->code = "";
                                $alert->save();
                                unset($_SESSION['code']);
                                unset($_SESSION['price']);
                                unset($_SESSION['id_product']);
                            }


                            $a = 1;

                            foreach ($this->viewData->cart->products as $productId) {

                                $product = \Model\Product::getItem($productId);
                                $desc = $product->description;
                                if (empty($product->description)) {
                                    $desc = "Watches";
                                } else {
                                    $desc = $product->description;
                                }

                                if (isset($_SESSION['price'])) {
                                    $code = $_SESSION['code'];
                                    $new_price = \Model\Price_Alert::getItem(null, ['where' => " code =  '$code'"]);
                                    if ($product->id == $new_price->product_id) {
                                        $fraud_price = $new_price->price;
                                    } else {
                                        $fraud_price = $product->price;
                                    }
                                } else {
                                    $fraud_price = $product->price;
                                }
                                $quantity = array_count_values($this->viewData->cart->products)[$product->id];

                                $arr[$a++] = array(
                                    'ProductName' => $product->name,
                                    'ProductDescription' => $desc,
                                    'ProductSellingPrice' => number_format($fraud_price * $quantity, 2, '.', ''),
                                    'ProductQty' => $quantity,
                                    'ProductCostPrice' => number_format($fraud_price,2, '.', '')
                                );
                            }

                            if ($order->cc_number[0] == 4) {
                                $credit_card = "VISA";
                            } elseif ($order->cc_number[0] == 5) {
                                $credit_card = "MC";
                            } elseif ($order->cc_number[0] == 3) {
                                $credit_card = "AMEX";
                            } elseif ($order->cc_number[0] == 6) {
                                $credit_card = "DISC";
                            } else {
                                $credit_card = "OTHER";
                            }

                            $post_array = array(
                                //////// Required fields //////////////
                                'ApiLogin' => 'YwZzFUKX66lx',
                                'ApiKey' => '5rhsxiIkxDVvgiULV1lTxltcfeamlwUr',
                                'TransactionId' => time() . mt_rand(),
                                'OrderDate' => $order->insert_time,
                                'OrderNumber' => $order->id,
                                'BillingFirstName' => $order->bill_first_name,
                                'BillingMiddleName' => '',
                                'BillingLastName' => $order->bill_last_name,
                                'BillingCompany' => '',
                                'BillingAddress1' => $order->bill_address,
                                'BillingAddress2' => $order->bill_address2,
                                'BillingCity' => $order->bill_city,
                                'BillingState' => $order->bill_state,
                                'BillingZip' => $order->bill_zip,
                                'BillingCountry' => get_countries()[$order->bill_country],
                                'BillingEveningPhone' => $order->phone,
                                'BillingEmail' => ($guest) ? $order->email : $this->viewData->user->email,
                                'IPAddress' => $_SERVER["REMOTE_ADDR"],
                                'ShippingFirstName' => $order->ship_first_name,
                                'ShippingMiddleName' => '',
                                'ShippingLastName' => $order->ship_last_name,
                                'ShippingCompany' => '',
                                'ShippingAddress1' => $order->ship_address,
                                'ShippingAddress2' => $order->ship_address2,
                                'ShippingCity' => $order->ship_city,
                                'ShippingState' => $order->ship_state,
                                'ShippingZip' => $order->ship_zip,
                                'ShippingCountry' => get_countries()[$order->ship_country],
                                'ShippingEveningPhone' => $order->phone,
                                'ShippingEmail' => ($guest) ? $order->email : $this->viewData->user->email,
                                'ShippingCost' => number_format($order->shipping_cost,2),
                                'GrandTotal' => number_format($order->total, 2, '.', ''),
                                'CCType' => $credit_card,
                                'CCFirst6' => substr($order->cc_number, 0, 6),
                                'CCLast4' => substr($order->cc_number, -4),
                                'CIDResponse' => 'M',
                                'AVSCode' => 'Y',
                                'LineItems' => $arr,
                                /////////// Optional fields /////////////
                                'SiteName' => 'Kenjo' //TODO st-dev determine use
                            );

// Convert post array into a query string
                            $post_query = http_build_query($post_array);

// Do the POST
                            $ch = curl_init('https://www.eye4fraud.com/api/');
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec($ch);
                            curl_close($ch);

// Show response


                         /*   $post_array = array(
                                'ApiLogin' => 'YwZzFUKX66lx',
                                'ApiKey' => '5rhsxiIkxDVvgiULV1lTxltcfeamlwUr',
                                'Action' => 'getOrderStatus',
                                'OrderNumber' => $order->id
                            );
*/
// Convert post array into a query string
                            //$post_query = http_build_query($post_array);

// Do the POST
                           /* $ch = curl_init('https://www.eye4fraud.com/api/');
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec($ch);
                            curl_close($ch);*/

// Show response
                            //echo $response;
                            $order->status = \Model\Order::$status[0];
                            $order->save();
                            //preg_match_all('~<value>(.*)</value>~', $response, $out_status);
                           /* @$fraud_status = $out_status[0][1];
                            if ($fraud_status == "<value>I</value>" || $fraud_status == "<value>A</value>" || $fraud_status == "<value>ALW</value>" || $fraud_status == "<value>U</value>") {
                                
                                @$order->fraud_status =  $out_status[0][2];
                                
                            }else{
								$order->status = \Model\Order::$status[2];
                                @$order->fraud_status =  $out_status[0][2];
                                $order->save();
							}*/
							$this->viewData->cart->products = [];
							$this->viewData->cart->productDetails = [];
                            setcookie('cartProducts', implode(',', $this->viewData->cart->products), time() + 60 * 60 * 24 * 100, "/");
                            setcookie('cartProductDetails',json_encode($this->viewData->cart->productDetails), time()+60*60*24*100, "/");
//echo '<script>window.location.replace("/order/' . $order->id.'");</script>';
                             redirect(SITE_URL . 'order/' . $order->id);
                        } else {
                             
                            $order->status = \Model\Order::$status[2];
                            $order->save();

                            $n = new \Notification\ErrorHandler($response->response_reason_text);
                            $_SESSION["notification"] = serialize($n);
                            if ($guest) {
                                redirect(SITE_URL . 'orders/checkout?type=guest');
                            } else {
                                redirect(SITE_URL . 'orders/checkout');
                            }
                        }/* 
                        } else {
							$order->status = \Model\Order::$status[2];
                            $order->save();
                            $n = new \Notification\ErrorHandler("Check your credit card information!");
                            $_SESSION["notification"] = serialize($n);
                            if ($guest) {
                                redirect(SITE_URL . 'orders/checkout?type=guest');
                            } else {
                                redirect(SITE_URL . 'orders/checkout');
                            }
                           
                        } */
                    } else {
                        $this->sendOrderPlacedEmail($order, $guest);

                        $n = new \Notification\MessageHandler('Order successfully placed!', $guest);
                        $_SESSION["notification"] = serialize($n);

                        $this->viewData->cart->products = [];
                        setcookie('cartProducts', implode(',', $this->viewData->cart->products), time() + 60 * 60 * 24 * 100, "/");

                        $_SESSION['purchase_conversion'] = true;
                        if (isset($_SESSION['code']) || isset($_SESSION['price']) || isset($_SESSION['id_product'])) {
                            $alert = \Model\Price_Alert::getItem(null, ['where' => " code =  '$code'"]);
                            $alert->code = "";
                            $alert->save();
                            unset($_SESSION['code']);
                            unset($_SESSION['price']);
                            unset($_SESSION['id_product']);
                        }
                        redirect(SITE_URL . 'order/' . $order->id);
                    }
                } else {
                    $n = new \Notification\ErrorHandler($order->errors);
                    $_SESSION["notification"] = serialize($n);
                    if ($guest) {
                        redirect(SITE_URL . 'orders/checkout?type=guest');
                    } else {
                        redirect(SITE_URL . 'orders/checkout');
                    }
                }
            } else {
                $n = new \Notification\ErrorHandler($order->errors);
                $_SESSION["notification"] = serialize($n);
                if ($guest) {
                    redirect(SITE_URL . 'orders/checkout?type=guest');
                } else {
                    redirect(SITE_URL . 'orders/checkout');
                }
            }
        } else {
            if (is_null($this->viewData->user) && !$guest) {
                $n = new \Notification\ErrorHandler('You must be logged in to create an order, or checkout as a guest.');
            }
            if (count($this->viewData->cart->products) <= 0) {
                $n = new \Notification\ErrorHandler('Your cart is empty.');
            }
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'cart');
        }
    }

    public function order_post(Array $params = [])
    {
        if ($this->viewData->order = $this->retrieve_order()) {
            $this->viewData->order->date = new DateTime($this->viewData->order->insert_time);
            $this->viewData->order->date = $this->viewData->order->date->format('M d Y');
            $this->viewData->order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $this->viewData->order->id]);
            foreach ($this->viewData->order_products as $order_product) {
                $order_product->product = \Model\Product::getItem($order_product->product_id);
            }
            $this->loadView($this->viewData);
        } else {
            redirect(SITE_URL);
        }
    }

    function retrieve_order(){
        $fields = ['email','ref_num'];
        $_POST  = array_filter($_POST);
        $missing = array_diff($fields,array_keys($_POST));
        if(count($missing)){
            $n = new \Notification\ErrorHandler('Order Info Missing: <br/>'.implode('<br/>',$missing));
            $_SESSION['notification'] = serialize($n);
            redirect(SITE_URL);
        } else {
            $id1Sql = '';
            if( $_POST['ref_num'][0] == '1'){
               $id1Sql = " OR id = ".substr($_POST['ref_num'],1);
            }
            $order = \Model\Order::getItem(null,['where'=>
                "(ref_num LIKE '{$_POST['ref_num']}'OR ".
                "id = {$_POST['ref_num']} $id1Sql) AND ".
                "LOWER(email) LIKE LOWER('{$_POST['email']}')"
            ]);
            if(!$order) {
                $n = new \Notification\ErrorHandler('No Matching order found');
                $_SESSION['notification'] = serialize($n);
                redirect(SITE_URL);
            } else {
                return $order;
            }
        }
    }

    public function ticket_post(){
        $obj = \Model\Contact::loadFromPost();
        $obj->message = "Order ID: {$_POST['orderID']}\n\n".$obj->message;
        $obj->admin_reply = 1;

        $client = new ZendeskAPI(ZENDESK_SUBDOMAIN);
        $client->setAuth('basic',['username'=>ZENDESK_USERNAME,'token'=>ZENDESK_API_TOKEN]);

        $locale = $client->locales()->findCurrent();

        $newTicket = $client->tickets()->create([
            'subject'  => $obj->subject,
            'comment'  => [
                'body' => $obj->message
            ],
            'priority' => 'normal',
            "requester"=> [
                'locale_id'=> $locale->id,
                'name'=> $obj->name,
                'email'=> $obj->email ]
        ]);

        if($obj->save()){
            $n = new \Notification\MessageHandler('We received your message.');
            $this->toJson(['state'=>true,'newTicket'=>$newTicket]);
        }
    }

    public function setSignify_id(){

        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $order = \Model\Order::getItem($id);
        $this->toJson(['status'=>'success','data'=>json_encode($order)]);
        exit;
        $settings = new \Signifyd\Core\SignifydSettings();
        $settings->apiKey = SIGNIFY_API_KEY;

        $apiInstance = new \Signifyd\Core\SignifydAPI($settings);
        
        $purchase = new \Signifyd\Models\Purchase();
        $purchase->browserIpAddress = $_SERVER['REMOTE_ADDR'];
        $purchase->orderId = $order->id;
        $purchase->createdAt = (new \Carbon\Carbon($order->insert_time))->toIso8601String();
        $purchase->paymentGateway = "authorize.net";
        $purchase->orderChannel = "WEB";
        $purchase->currency = "USD";
        $purchase->avsResponseCode = $order->authorizeData->avs;
        $purchase->cvvResponseCode = $order->authorizeData->cvv;
        $purchase->totalPrice = floatval($order->total);

        $shippingAddress = new \Signifyd\Models\Address();
        $shippingAddress->streetAddress = $order->ship_address;
        $shippingAddress->unit = $order->ship_address2;
        $shippingAddress->city = $order->ship_city;
        $shippingAddress->provinceCode = $order->ship_state;
        $shippingAddress->postalCode = $order->ship_zip;
        $shippingAddress->countryCode = 'US';

        $recipient = new \Signifyd\Models\Recipient();
        $recipient->fullName = $order->ship_first_name." ".$order->ship_last_name;
        $recipient->confirmationEmail = $order->email;
        $recipient->confirmationPhone = $order->phone;
        $recipient->deliveryAddress = $shippingAddress;

        $card = new \Signifyd\Models\Card();

        $billingAddress = new \Signifyd\Models\Address();
        $billingAddress->streetAddress = $order->bill_address;
        $billingAddress->unit = $order->bill_address2;
        $billingAddress->city = $order->bill_city;
        $billingAddress->provinceCode = $order->bill_state;
        $billingAddress->postalCode = $order->bill_zip;
        $billingAddress->countryCode = 'US';

        $card->cardHolderName = $order->bill_first_name." ".$order->bill_last_name;
        $card->last4 = substr($order->cc_number, -4);
        $card->expiryMonth = '0'.substr($order->cc_expiration_month, -2);
        $card->expiryYear = substr($order->cc_expiration_year, -2);
        $card->billingAddress = $billingAddress;

        $userAccount = new \Signifyd\Models\UserAccount();
        if($this->viewData->user){
            $userAccount->email = $this->viewData->user->email;
            $userAccount->username = $this->viewData->user->email;
            $userAccount->phone = $this->viewData->user->phone;

            $u_insert_time = new DateTime($this->viewData->user->insert_time);
            $u_insert_time = $u_insert_time->format('m-d-Y H:i:s');
            $userAccount->createdDate = $u_insert_time;
            $userAccount->accountNumber = $this->viewData->user->id;
            $userAccount->lastOrderId = $this->viewData->order->id;
            $userAccount->aggregateOrderCount = $this->viewData->user->order_count;
            $userAccount->aggregateOrderDollars = $this->viewData->user->total_spend;
        } else {
            $userAccount->email = "";
            $userAccount->username = "";
            $userAccount->phone = "";
            $userAccount->createdDate = $order->insert_time;
            $userAccount->accountNumber = "";
            $userAccount->lastOrderId = "";
            $userAccount->aggregateOrderCount = 1;
            $userAccount->aggregateOrderDollars = $order->total;
        }

        $seller = new \Signifyd\Models\Seller();
        $case = new \Signifyd\Models\CaseModel();

        $case->purchase = $purchase;
        $case->recipient = $recipient;
        $case->card = $card;
        $case->userAccount = $userAccount;
        $case->seller = $seller;

        $case_id = $apiInstance->createCase($case);
        $order->case_id = $case_id;
        if($order->save()){
            $this->toJson(['status'=>'success','data'=>json_encode($order)]);
        } else {
            $this->toJson(['status'=>'failure','data'=>json_encode($_POST)]);
        }
    }

    private function sendOrderPlacedEmail($order, $guest)
    {
        global $emagid;
        $emagid->email->from->email = 'orders@djinyc.com'; //TODO st-dev get modern vice email

        $email = new \Emagid\Email();
        if ($guest) {
            $email->addTo($order->email);
        } else {
            $email->addTo($this->viewData->user->email);
        }

        $subject_array = Array();
        foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
            $product = \Model\Product::getItem($order_product->product_id);
            $brand = \Model\Brand::getItem($product->brand);
            $subject_array[] = $brand->name;
            $subject_array[] = $product->mpn;

            //$subject_array[]=$order_product->id;
        }

        $email->subject('Thank you for your DJI Order! ' . implode(" ", $subject_array) . ". Order #" . $order->id);
        $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="http://djinyc.com/content/frontend/img/logo.png" alt="Luggage"></center>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
	<tbody>
		<tr>
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal" ><a href="http://djinyc.com/brands" target="_blank"><span  style="text-decoration:none;color:black;">BRANDS </span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://djinyc.com/category/new-arrivals" target="_blank"><span style="text-decoration:none;color:black;">NEW ARRIVALS</span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://djinyc.com/category/men-s" target="_blank"><span style="text-decoration:none;color:black;">MEN`S</span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://djinyc.com/category/women-s" target="_blank"><span style="text-decoration:none;color:black;">WOMEN`S</span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://djinyc.com/category/accessories" target="_blank"><span style="text-decoration:none;color:black;">ACCESSORIES</span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://djinyc.com/category/pre-owned-clearance" target="_blank"><span style="text-decoration:none;color:black;">PRE-OWNED &amp; CLEARANCE</span></a>&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
	<tr style="height:.75pt">
		<td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
	</tr>
	<tr style="height:.75pt">
		<td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
			<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
				<tbody>
					<tr style="height:22.5pt">
						<td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
							<p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Order Confirmation</span></b> </p>
						</td>
						<td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
							<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Number:</span></b> </p>
						</td>
						<td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
							<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</span> </p>
						</td>
					</tr>
					<tr style="height:22.5pt">
						<td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
							<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Date:</span></b> </p>
						</td>
						<td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
							<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->insert_time . '</span> </p>
						</td>
					</tr>
				</tbody>
			</table>';

        /* if ($order->payment_method == 1) {
            $email->body .= 'We have received your order and we are currently processing it.<br>';
            $email->body .= 'Your credit card will not be charged until the item’s availability has been established, at which time you will receive a confirmation email.';
            $email->body .= '<p>Then, as soon as your item ships you will receive another email with the tracking information.</p>';
            $email->body .= '<p>If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p>';
        } else if ($order->payment_method == 2) {
            $email->body .= '<p>We will contact you shortly for more information. If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p>';
        } */


        $shp = array($order->ship_first_name, $order->ship_last_name, $order->ship_address, $order->ship_address2,
            get_countries()[$order->ship_country], $order->ship_city, $order->ship_state, $order->ship_zip);


        $blng = array($order->bill_first_name, $order->bill_last_name, $order->bill_address, $order->bill_address2,
            get_countries()[$order->bill_country], $order->bill_city, $order->bill_state, $order->bill_zip);


        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
			<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
				<tbody>
					<tr style="height:9.75pt">
						<td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:9.75pt"></td>
					</tr>
					<tr style="height:41.25pt">
						<td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 15.0pt 15.0pt;height:41.25pt">
							<p><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Dear ' . $order->bill_first_name . ' ' . $order->bill_last_name . ',</span></b></p>
							<p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Thank you for your order. Please retain this email as confirmation of your order.</span></p>
						</td>
					</tr>
					<tr style="height:20.25pt">
						<td width="50%" style="width:50.0%;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
							<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping to:</span></b> </p>
						</td>
						<td width="50%" style="width:50.0%;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
							<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Billing to:</span></b> </p>
						</td>
					</tr>
					<tr style="height:5.25pt">
						<td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
					</tr>
					<tr>
						<td style="background:white;padding:0in 0in 7.5pt 15.0pt">
							<p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">
							' . $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
							' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
							' . $order->ship_city . ' ' . $order->ship_state . ' <br>
							' . $order->ship_zip . ' ' . get_countries()[$order->ship_country] . '						
							</span></p>
						</td>
						<td style="background:white;padding:0in 0in 7.5pt 15.0pt">
							<p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">';
        if (count(array_diff($shp, $blng)) > 0) {
            $email->body .= $order->bill_first_name . ' ' . $order->bill_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
							' . $order->bill_address . ' ' . $order->bill_address2 . '<br>
							' . $order->bill_city . ' ' . $order->bill_state . '<br>
							' . $order->bill_zip . ' ' . get_countries()[$order->bill_country];
        } else {
            $email->body .= $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
							' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
							' . $order->ship_city . ' ' . $order->ship_state . '<br>
							' . $order->ship_zip . ' ' . get_countries()[$order->ship_country];
        }
        $email->body .= '</p>
						</td>
					</tr>
				</tbody>
			</table>';


        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
			<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
				<tbody>
					<tr style="height:20.25pt">
						<td width="255" colspan="2" style="width:191.25pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
							<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Items</span></b> </p>
						</td>
						<td width="146" style="width:109.5pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:20.25pt">
							<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping Method</span></b> </p>
						</td>
						<td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
							<p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Qty</span></b> </p>
						</td>
						<td width="100" style="width:75.0pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
							<p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Price</span></b> </p>
						</td>
						<td width="100" style="width:75.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
							<p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Total</span></b> </p>
						</td>
					</tr>
					<tr style="height:5.25pt">
						<td width="650" colspan="6" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
					</tr>';

        foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {

            $product_inventory = \Model\Actual_Inventory::getItem(null,["where"=>"product_id = $order_product->product_id"]);
            $product_inventory->website = intval($product_inventory->website) - $order_product->quantity;
            $product_inventory->save();

            if (isset($_SESSION['price'])) {
                $code = $_SESSION['code'];
                $new_price = \Model\Price_Alert::getItem(null, ['where' => " code =  '$code'"]);
                if ($order_product->product_id == $new_price->product_id) {
                    $price = $new_price->price;

                } else {
                    $price = $order_product->unit_price;

                }
            } else {
                $price = $order_product->unit_price;
            }

            $final_sale = $order_product->final_sale;
            If ($final_sale > 0) {
                $fin = 'This is final sale!';
            } else {
                $fin = '';
            }
            $email->body .= '<tr style="height:75.0pt">'
                . '<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
							<p class="MsoNormal"><img  width="100px!important;" src="https://djinyc.com/content/uploads/products/' . \Model\Product::getItem($order_product->product_id)->featured_image . '"></p></td>'
                . '<td width="170" valign="top" style="width:127.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 3.75pt 0in;height:75.0pt">
							<p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042"> <a href="https://djinyc.com/product/' . \Model\Product::getItem($order_product->product_id)->slug . '">' . \Model\Product::getItem($order_product->product_id)->name . ' ' . $fin . ' ' . \Model\Product::getItem($order_product->product_id)->mpn . '</a></span></p>
						</td>'
                . '<td width="140" valign="top" style="width:105.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 3.75pt 7.5pt;height:75.0pt">
							<p><strong><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . \Model\Shipping_Method::getItem($order->shipping_method)->name . '</span></strong><span style="font-size:8.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042"></span></p></td>'

                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
							<p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order_product->quantity . '</span></b></p>
						</td>'
                . '<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
							<p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($price, 2) . '</span></b></p>
						</td>'
                . '<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
							<p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($price * $order_product->quantity, 2) . '</span></b></p>
						</td>'
                . '</tr> 	
				';
        }
        $email->body .= '</tbody>
			</table>
			<p class="MsoNormal">&nbsp;</p>';
        if (!is_null($order->coupon_code)) {
            $savings = ($order->coupon_type == 1) ? '$' : '';
            $savings .= number_format($order->coupon_amount, 2);
            $savings .= ($order->coupon_type == 2) ? '%' : '';
        } else {
            $savings = 0;
        }

        $show_saving = 'display:none;';

        $email->body .= '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt;padding-left: 604px;">
				<tbody>
					<tr>
						 
						<td style="padding:0in 0in 0in 0in">
							<table border="0" cellspacing="0" cellpadding="0" width="200" style="width:150.0pt">
								<tbody>
									<tr style="height:20.25pt">
										<td width="200" colspan="2" style="width:150.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
											<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Order Summary</span></b></p>
										</td>
									</tr><tr style="height:20.25pt">
										<td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
											<p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sub Total:</span></b></p>
										</td>
										<td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
											<p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->subtotal, 2) . '</span></p>
										</td>
									</tr>
								 
									<tr style="height:20.25pt">
										<td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
											<p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping:</span></b></p>
										</td>
										<td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
											<p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->shipping_cost, 2) . '</span></p>
										</td>
									</tr>
									<tr style="height:20.25pt">
										<td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
											<p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sales Tax:</span></b> </p>
										</td>
										<td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
											<p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->tax, 2) . '</span></p>
										</td>
									</tr>
									<tr style="height:20.25pt">
										<td width="125" style="width:93.75pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 3.75pt 0in 0in;height:20.25pt">
											<p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Total:</span></b></p>
										</td>
										<td width="75" style="width:56.25pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 15.0pt 0in 0in;height:20.25pt">
											<p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->total, 2) . '</span></b></p>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>';


        $email->send(); //sends to the customer

        $email->to = [];
        $email->addBcc('info@djinyc.com'); //TODO st-dev get MV info email
        $email->addBcc('orders@djinyc.com'); //TODO st-dev get MV orders email
		$email->addTo('eitan@emagid.com');

        if ($guest) {
            $to = $order->email;
        } else {
            $to = $this->viewData->user->email;
        }


        $email->send();
    }

    public function returnLabel(Array $params = []){
        $order = \Model\Order::getItem(null,['where'=>"ref_num = '{$params['id']}'"]);
        if(!$order){
            redirect("https://kawsone.com/orders/validate/none");
        } else {
            $label = \Model\Labels::getItem(null,['where'=>"order_id = {$order->id} and type = 2",'orderBy'=>"id DESC"]);
            $newestLabel = \Model\Labels::getItem(null,['where'=>"order_id = {$order->id}",'orderBy'=>"id DESC"]);
            if($label && $label->id == $newestLabel->id){
                if($label->claimed == 0){
                    $order->tracking_number = $label->tracking_number;
                    $order->save();
                }
                $label->claimed = 1;
                $label->save();
                redirect($label->image());
            } else {
                redirect("https://kawsone.com/orders/validate/none");
            }
        }

    }

    public function correct_order(Array $params = [])
    {
        $ref_num = $params['id'];
        $order = \Model\Order::getItem(null, ['where' => "ref_num = '$ref_num'"]);
        if (false) {
        // if ($order && $order->difference && $order->campaign_id == 9) {
            $this->viewData->order = $order;
            $this->viewData->products = $order->getOrderProducts();
            $this->loadView($this->viewData);   
        } else { 
            redirect("https://kawsone.com/");
        }
    }

    public function verify_orders(Array $params = [])
    {
        $this->viewData->email = $email = $params['id'];
        $ov = \Model\Order_Verification::getItem("user_identifier = '$email'");
        if(true){
            redirect("https://kawsone.com/orders/validate/none");
        }

        $orderSql = "SELECT distinct o.id, o.total, o.ship_first_name || ' ' || o.ship_last_name AS name, o.status, o.user_ip, o.email, o.phone,
         count(brc.*) AS brown_count, count(grc.*) AS grey_count, count(blc.*) AS black_count
         FROM public.order o
         LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.product_id = 105
         LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.product_id = 106
         LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.product_id = 107
         INNER JOIN public._order_matching_cleaner om ON o.id = om.id
         WHERE  om.groupid in (SELECT MAX(groupid) FROM public._order_matching_cleaner WHERE trim(lower(email)) = trim(lower('$email')))
         AND o.status in ('Active','Updated')
         GROUP BY o.id, o.total, o.ship_first_name, o.ship_last_name, o.status, o.user_ip, o.email, o.phone";



        global $emagid;
        $db = $emagid->getDb();
        $results = $db->getResults($orderSql);

        if(count($results) < 2 ){
            redirect("https://kawsone.com/orders/validate/none");
        }

        $this->viewData->orders = $results;
        $brown = \Model\Product::getItem(105);
        $grey = \Model\Product::getItem(106);
        $black = \Model\Product::getItem(107);

        $this->viewData->products = [105=>$brown, 106=>$grey, 107=>$black];
        $this->loadView($this->viewData);
    }

    public function correct_order_post(){
        if(true){
            redirect("https://kawsone.com/");
        }

        if(
            (!isset($_POST['card_name'])             || $_POST['card_name'] == '') ||
            (!isset($_POST['cc_number'])             || $_POST['cc_number'] == '') ||
            (!isset($_POST['email'])                 || $_POST['email'] == '') ||
            (!isset($_POST['cc_expiration_month'])   || $_POST['cc_expiration_month'] == '') ||
            (!isset($_POST['cc_expiration_year'])    || $_POST['cc_expiration_year'] == '') ||
            (!isset($_POST['cc_ccv'])                || $_POST['cc_ccv'] == '') ||
            (!isset($_POST['total'])                 || $_POST['total'] == '') ||
            (!isset($_POST['orderId'])               || $_POST['orderId'] == '') ||
            (!isset($_POST['ref_num'])               || $_POST['ref_num'] == '')
        ){
            $n = new \Notification\ErrorHandler('There is information missing from your order');
            $_SESSION['notification'] = serialize($n);
            redirect("/orders/correct_order/$order->ref_num");
        }
        
        $order = \Model\Order::getItem($_POST['orderId']);

        if($order == null){
            $n = new \Notification\ErrorHandler('Order Not found');
            $_SESSION['notification'] = serialize($n);
            redirect("/orders/correct_order/$order->ref_num");
        }

        if(!isset($_POST['agree_to_terms']) ){
            $n = new \Notification\ErrorHandler('You must agree to the terms of the sale');
            $_SESSION['notification'] = serialize($n);
            redirect("/orders/correct_order/$order->ref_num");
        }


        $transaction = new AuthorizeNetAIM();
        $transaction->setFields([
            'amount' => $_POST['total'],
            'card_num' => $_POST['cc_number'],
            'exp_date' => $_POST['cc_expiration_month'] . '/' . $_POST['cc_expiration_year'],
            'card_code' => $_POST['cc_ccv'],
            'first_name' => $order->bill_first_name,
            'last_name' => $order->bill_last_name,
            'address' => $order->bill_address,
            'city' => $order->bill_city,
            'state' => $order->bill_state,
            'country' => $order->bill_country,
            'zip' => $order->bill_zip,
            'email' => $_POST['email'],
            'ship_to_address' => $order->ship_address,
            'ship_to_city' => $order->ship_city,
            'ship_to_country' => $order->ship_country,
            'ship_to_first_name' => $order->ship_first_name,
            'ship_to_last_name' => $order->ship_last_name,
            'ship_to_state' => $order->ship_state,
            'ship_to_zip' => $order->ship_zip,
            'phone' => $order->phone
        ]);
        $transaction->setCustomFields(['x_invoice_num' => $order->id]);

        $response = $transaction->authorizeAndCapture();

        $localTransaction = new \Model\Transaction();
        $localTransaction->order_id = $order->id;
        $localTransaction->ref_tran_id = $response->transaction_id;
        $localTransaction->amount = $response->amount;
        $localTransaction->avs_result_code = $response->avs_response;
        $localTransaction->cvv_result_code = $response->cavv_response;
        $localTransaction->type = "AUTH_CAPTURE";
        $localTransaction->save();
        if ($response->approved || $response->held) {
            $order->total += $_POST['total'];
            $order->difference = $order->difference - $_POST['total'];
            $order->charge_added = 1;
            $order->status = 'Active';
            $order->fulfillment_status = 'Ready';
            $order->save();
            $now = Carbon\Carbon::now();


            $order->save();

//            $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation');
//            try {
//                $email->send();
//            } catch(Mandrill_Error $e){
//            }
            $this->send_confirmation_email($order);

            $n = new \Notification\MessageHandler('Your Order has been updated');
            $_SESSION['notification'] = serialize($n);

            redirect('/checkout/confirmation/' . $order->ref_num);
        } else {
            $n = new \Notification\ErrorHandler($response->response_reason_text);
            $_SESSION["notification"] = serialize($n);
            redirect("/orders/correct_order/{$_POST['ref_num']}");
        }
        var_dump($_POST);
    }
    public function verify_orders_post(){
        $identifier = $_POST['email'];
        $wanted_orders = [];
        $unwanted_orders = [];

        foreach ($_POST['orders'] as $id => $answer){
            if($answer == 1){
                $wanted_orders[] = $id;
            } else {
                $unwanted_orders[] = $id;
            }
        }

        $ov = \Model\Order_Verification::getItem(null,['where'=>"user_identifier = '$identifier'"]);
        if($ov == null){
            $ov = new \Model\Order_Verification();
        } else {
//            var_dump($ov->getWanted());
        }
        $ov->user_identifier = $identifier;
        $ov->wanted_orders = "{".implode(',',$wanted_orders)."}";
        $ov->unwanted_orders = "{".implode(',',$unwanted_orders)."}";
        $resp = ['status'=>false];
        if($ov->save()){
            $resp = ['status'=>true];
        }
        $this->toJson($resp);
    }
    private function send_confirmation_email($order){
        foreach ($order->getOrderProducts() as $orderProduct){
            $product = \Model\Product::getItem($orderProduct->product_id);
            $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
            if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
            $name = $product->name;
            $price = $product->price;
            $html .=
                '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
                                </td>
                                <td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
                                
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                                
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
                                </td>
                            </tr>';
        }
        $email = new \Email\MailMaster();
        $mergeFields = [
            'ORDER_NUMBER' => $order->id,
            'DATE' => date('Y-m-d h:i:s',strtotime($order->insert_time)),
            'DATED' => date('l, F d, Y',strtotime($order->insert_time)),
            'DATET' => date('h:i A',strtotime($order->insert_time)),
            'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
            'SHIPPING' => $order->getShippingAddr(),
            'BILLING' => $order->getBillingAddr(),
            'ITEMS' => $html,
            'SUBTOTAL' => number_format($order->subtotal, 2),
            'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
            'SHIPPINGNAME' => 'Shipping',
            'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
            'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
            'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
            'TOTAL' => number_format($order->total, 2)
        ];

        if(isset($_POST['send_confirm'])){
            $template = 'kaws-order-confirmation';
        } else if(isset($_POST['send_update'])) {
            $template = 'kaws-confirmation-update';
        }

        $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])
            ->setMergeTags($mergeFields)
            ->setTemplate('kaws-order-confirmation');
        try {
            $emailResp = $email->send();
            $n = new \Notification\MessageHandler("Your order has been updated. Confirmation Email has been Sent");
            $_SESSION["notification"] = serialize($n);
        } catch(Mandrill_Error $e){
            $n = new \Notification\ErrorHandler("Your order has been successfully updated, however, an email could not be sent to you at this time.");
            $_SESSION['notification'] = serialize($n);
        }
    }


}