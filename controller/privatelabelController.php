<?php

class privatelabelController extends siteController {
	
    public function index(Array $params = [])
    {
        redirect('/');
        $this->viewData->header = \Model\Private_Label::getItem(null,['where'=>"type = 1"]);
        $this->viewData->caseStudy = \Model\Private_Label::getList(['where'=>"type = 2"]);
        $this->viewData->services = \Model\Private_Label::getList(['where'=>"type = 3", 'orderBy'=>"display_order"]);
        $this->viewData->additional = \Model\Private_Label::getList(['where'=>"type = 4"]);
        $this->configs['Meta Title'] = "Modern Vice - Private Label";
        $this->loadView($this->viewData);
    }

    public function submitInquiry(){
        $inquiry = \Model\Inquiry::loadFromPost();
        $inquiry->token = generateToken();
        $result = json_encode([]);
        if($_POST['g-recaptcha-response']){
            $recaptchaUrl = 'https://www.google.com/recaptcha/api/siteverify';
            $params = ['secret'=>RECAPTCHA_SECRET_KEY,'response'=>$_POST['g-recaptcha-response']];
            $ch = curl_init($recaptchaUrl);
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

            $result = curl_exec($ch);
            curl_close($ch);
        }
        $response = json_decode($result,true);
        if(isset($response['success']) && $response['success'] == true && $inquiry->save()) {
            foreach ($_FILES as $fileType => $file) {
                if ($file['error'] == 0) {
                    $image = new \Emagid\Image();
                    $image->upload($_FILES[$fileType], UPLOAD_PATH . 'inquiry' . DS);
                $inquiry->featured_image = $image->fileName;
                $inquiry->save();
                }
            }
            $email = new \Email\MailMaster();
            $mergeFields = [
                'QUESTIONNAIRE'=>'https://modernvice.com/questionnaire?tkn='.$inquiry->token
            ];
            $email->setTo(['email'=>$inquiry->email,'name'=>$inquiry->name,'type'=>'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-pl-questionnaire')->send();
            $n = new \Notification\MessageHandler("Thank you for your submission. We'll be in contact.");
            $_SESSION['notification'] = serialize($n);
            redirect('/privatelabel');
        } else {
            $n = new \Notification\ErrorHandler("Failed to send inquiry");
            $_SESSION['notification'] = serialize($n);
            redirect('/privatelabel');
        }
    }
}