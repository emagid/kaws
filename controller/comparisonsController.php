<?php

class comparisonsController extends siteController {
	
    public function index(Array $params = [])
    {

        redirect('/');
        $products = array_map(function($item){return \Model\Product::getItem($item);},$_SESSION['comparison']);
        $prodSpecList = array_map(function($item){
            return \Model\Product_Spec::getSpecs($item->id,true);
        },$products);
        $allSpecCategory = [] ;
        $specArr = [];
        foreach ($prodSpecList as $sp){
            $allSpecCategory = array_unique(array_merge($allSpecCategory,array_keys($sp)),SORT_STRING);
        }
        foreach ($allSpecCategory as $category){
            $temp = [];
            foreach ($prodSpecList as $sp){
                if($sp[$category]) {
                    $temp += $sp[$category];
                }
            }
            if($temp) {
                $specArr[$category] = $temp;
            }
        }
        $this->viewData->allColumn = $specArr;
        $this->viewData->products = $products;
        $this->configs['Meta Title'] = "";
        $this->loadView($this->viewData);
    }

    /** Update session to store comparison item */
    public function up_comp(){
        $prodId = $_POST['product_id'];

        if(in_array($prodId,$_SESSION['comparison'])){
            unset($_SESSION['comparison'][array_search($prodId,$_SESSION['comparison'])]);
        } else {
            $_SESSION['comparison'][] = $prodId;
        }
    }
//    public function getUnionCategory()
}