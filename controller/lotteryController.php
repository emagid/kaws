<?php

require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class lotteryController extends siteController {

    public function redeem2(array $arr = []){
        $ref_num = $arr['id'];
        $entry = \Model\Lotto_Entry::getItem(null,['where'=>"ref_num = '$ref_num'"]);
        $_SESSION['lotto_email'] = $entry->user()->email;
        $_SESSION['lotto_entrant'] = $entry->id;
        $product = \Model\Product::getItem($entry->product_id);

        $cartIds = isset($_COOKIE['cart']) && $_COOKIE['cart'] ? json_decode($_COOKIE['cart'],true) : [];
        $carts = $this->viewData->cart->cart;
        $cart = new \Model\Cart();
        $cart->user_id = $entry->user_id;
        $cart->guest_id = session_id();
        $cart->product_id = $entry->product_id;
        $cart->quantity = 1;
        $cart->save();
        redirect('/checkout/payment');
    }

    public function signup_post(){
        $_SESSION['lottery'] = $_POST['campaign_ids'];
        $this->toJson(['status'=>true]);
    }

    function redeem(Array $params = []){
        if(!isset($params['redemption_slug']) || $params['redemption_slug'] == '' ){
            redirect('https://kawsone.com');
        }
        $entrant = \Model\Lotto_Entry::getItem(null,['where'=>"ref_num = '{$params['redemption_slug']}'"]);
        if($entrant == null ){
            redirect('https://kawsone.com');
        }
        $user = $entrant->user();
        $address = $user->getAddress();
        $this->viewData->product = $product = \Model\Product::getItem($entrant->product_id);
        $entrant->email = $user->email;
        $entrant->first_name = $address->first_name;
        $entrant->last_name = $address->last_name;
        $entrant->phone = $address->phone;
        $entrant->address = $address->address;
        $entrant->address2 = $address->address2;
        $entrant->city = $address->city;
        $entrant->state = $address->state;
        $entrant->country = $address->country;
        $entrant->zip = $address->zip;

        $price = $product->getPrice(null,null,null,null);
        $tax_rate = 0;
        if($entrant->state == 'NY' || $entrant->state == 'New York'){
            $tax_rate = 8.875;
        } else if($entrant->state == 'NJ' || $entrant->state == 'New Jersey') {
            $tax_rate = 6.625;
        }

        $shippingPrices = [
            90013=>311.90,
            10013=>93.95,
            91201=>311.90,
            33543=>279.95,
            22079=>127.98
        ];

        $this->viewData->tax = $price*($tax_rate/100);
        $this->viewData->shipping = isset($shippingPrices[$entrant->zip])?$shippingPrices[$entrant->zip]:0;
        $this->viewData->entrant = $entrant;
        $this->loadView($this->viewData);
    }

    function redeem_post(){
//        var_dump($_POST);
        if(!isset($_POST['agree_to_terms'])){
            $n = new \Notification\ErrorHandler("You must first agree to the terms to continue");
            $_SESSION["notification"] = serialize($n);
            redirect("/redeem/{$_POST['ref_num']}");
        }
        $entrant = \Model\Lotto_Entry::getItem(null,['where'=>"ref_num = '{$_POST['ref_num']}' AND id = {$_POST['entry_id']}"]);
        $product = \Model\Product::getItem($entrant->product_id);
        $user = $entrant->user();

        $address = $user->getAddress();
        if($entrant == null || $product == null || $user == null || $address == null){
            $n = new \Notification\ErrorHandler("There was an issue processing your order");
            $_SESSION["notification"] = serialize($n);
            redirect("/redeem/{$_POST['ref_num']}");
        }

        $order = \Model\Order::loadFromPost();

        $order->user_ip = $_SERVER['REMOTE_ADDR'];
        $order->campaign_id = $entrant->campaign_id;
        $order->email = $user->email;
        $order->phone = $address->phone;
        $order->ship_first_name = $order->bill_first_name   = $address->first_name;
        $order->ship_last_name  = $order->bill_last_name    = $address->last_name;
        $order->ship_address    = $order->bill_address      = $address->address;
        $order->ship_address2   = $order->bill_address2     = $address->address2;
        $order->ship_city       = $order->bill_city         = $address->city;
        $order->ship_state      = $order->bill_state        = $address->state;
        $order->ship_country    = $order->bill_country      = $address->country;
        $order->ship_zip        = $order->bill_zip          = $address->zip;

        $shippingPrices = [
            90013=>311.90,
            10013=>93.95,
            91201=>311.90,
            33543=>279.95,
            22079=>127.98
        ];

        $order->shipping_cost = $shipping = isset($shippingPrices[$order->ship_zip])?$shippingPrices[$order->ship_zip]:0;
        $date = new DateTime();
        $order->insert_time = $date->format('Y-m-d H:i:s');
        $user = \Model\User::getItem($entrant->user_id);
        $order->user_id = $entrant->user_id;
        $order->guest_id = session_id();

        $order->ship_country = 'United States';
        switch($order->ship_state){
            case 'NY':
                $order->tax_rate = $_POST['tax_rate'] = 8.875;
                break;
            case 'NJ':
                $order->tax_rate = $_POST['tax_rate'] = 6.625;
                break;
        }
        $order->status = \Model\Order::$status[0];
        $subtotal = $product->getPrice(null,null,null,null);
        $order->tax = $tax = round($subtotal * ($order->tax_rate/100),2);
        $order->payment_method = 1;

        $order->subtotal = round($subtotal,2);
        $order->total = $order->orig_total = round($subtotal+$shipping+$tax,2);
        $order->ref_num = generateToken();
        if($order->save()){

            $html = '';

            $order_prod = new \Model\Order_Product();
            $order_prod->order_id = $order->id;
            $order_prod->quantity = 1;
            $order_prod->product_id = $product->id;
            $order_prod->unit_price = $product->getPrice(null, null, null, null);
            $order_prod->save();

            $full = '';
            $img = $product->featuredImage();
            $html .=
                '<tr style="height:75.0pt">
						<td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
						</td>
						<td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product->name.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">1</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($order_prod->unit_price,2).'</span></p>
						</td>
					</tr>';

            if(false){
                redirect("/redeem/{$_POST['ref_num']}");
            }
            $email = new \Email\MailMaster();
            $mergeFields = [
                'ORDER_NUMBER' => $order->id,
                'DATE' => date('Y-m-d h:i:s'),
                'DATED' => date('l, F d, Y'),
                'DATET' => date('h:i A'),
                'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                'SHIPPING' => $order->getShippingAddr(),
                'BILLING' => $order->getBillingAddr(),
                'ITEMS' => $html,
                'SUBTOTAL' => number_format($order->subtotal, 2),
                'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                'SHIPPINGNAME' => 'Shipping',
                'TAXNAME' => 'Tax',
                'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                'TAXFEE' => number_format($order->tax, 2),
                'TOTAL' => number_format($order->total, 2)
            ];

            //Credit card payment - Payment_method = 1
            if(true) {
                $transaction = new AuthorizeNetAIM();
                $transaction->setFields([
                    'amount' => $order->total,
                    'card_num' => $order->cc_number,
                    'exp_date' => $order->cc_expiration_month . '/' . $order->cc_expiration_year,
                    'first_name' => $order->bill_first_name,
                    'last_name' => $order->bill_last_name,
                    'address' => $order->bill_address,
                    'city' => $order->bill_city,
                    'state' => $order->bill_state,
                    'country' => $order->bill_country,
                    'zip' => $order->bill_zip,
                    'email' => $order->email,
                    'card_code' => $order->cc_ccv,
                    'ship_to_address' => $order->ship_address,
                    'ship_to_city' => $order->ship_city,
                    'ship_to_country' => $order->ship_country,
                    'ship_to_first_name' => $order->ship_first_name,
                    'ship_to_last_name' => $order->ship_last_name,
                    'ship_to_state' => $order->ship_state,
                    'ship_to_zip' => $order->ship_zip,
                    'phone' => $order->phone
                ]);
                $transaction->setCustomFields(['x_invoice_num' => $order->id]);

                $response = $transaction->authorizeAndCapture();

                $localTransaction = new \Model\Transaction();
                $localTransaction->order_id = $order->id;
                $localTransaction->ref_tran_id = $response->transaction_id;
                $localTransaction->amount = $response->amount;
                $localTransaction->avs_result_code = $response->avs_response;
                $localTransaction->cvv_result_code = $response->cavv_response;
                $localTransaction->type = "AUTH_CAPTURE";
                $localTransaction->save();
                if ($response->approved || $response->held) {
//                if (true) {
                    $entrant->redeemed = 1;
                    $now = Carbon\Carbon::now();
                    $entrant->claimed_date = $now;
                    $entrant->ref_num = '';
                    $entrant->redeemed = 1;
                    $entrant->save();

                    $order->auth_number = $response->authorization_code;

                    $order->status = \Model\Order::$status[0];
                    $order->payment_status = 'AuthorizeNet Success';
//                    $order->payment_status = 'Test Order';
                    $order->save();

                    $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-order-confirmation');
                    try {
                        $email->send();
                    } catch(Mandrill_Error $e){
                        $n = new \Notification\ErrorHandler('We could not send you an email at this time. We will contact you to confirm your order');
                        $_SESSION['notification'] = serialize($n);
                    }

                    redirect('/checkout/confirmation/' . $order->ref_num);
                } else {
                    $order->status = \Model\Order::$status[2];
                    $order->error_message = $response->response_reason_text;
                    $order->payment_status = 'AuthorizeNet Failed';
                    $order->save();
                    $n = new \Notification\ErrorHandler($response->response_reason_text);
                    $_SESSION["notification"] = serialize($n);
                    redirect("/redeem/{$_POST['ref_num']}");
                }
            }


        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION["notification"] = serialize($n);
            redirect("/redeem/{$_POST['ref_num']}");
        }

    }

    /*public function signup_post()
    {
        $campaign = \Model\Campaign::getItem(null,['where'=>"id = {$_POST['campaign_id']} AND status = 'Active'"]);
        $product = \Model\Product::getItem($_POST['product_id']);
        $user = \Model\User::getItem(null,['where'=>"email = '{$_POST['$email']}'"]);
        if(!$user){
            $user = new \Model\User();
            $user->first_name = $_POST['first_name'];
            $user->last_name = $_POST['last_name'];
            $user->email = $_POST['email'];
            $user->password = "**************";
            $user->save();
        } //Todo: Add Blacklist, partial user match validation
        $entries = \Model\Lotto_Entry::getList(['where'=>"campaign_id = $campaign->id AND user_id = $user->id"]);
        if(count($entries) == 0){
            $entry = new \Model\Lotto_Entry();
            $entry->user_id = $user->id;
            $entry->product_id = $product->id;
            $entry->campaign_id = $campaign->id;
            if($entry->save()){
                $n = new \Notification\MessageHandler('You have successfully entered the lottery and will be notified if you win');
                $_SESSION['lottery'] = $campaign->id;
            } else {
                $n = new \Notification\ErrorHandler('You could not be entered into this lottery');
            }
        } else {
            $n = new \Notification\ErrorHandler('You have already entered this drawing');
            $_SESSION['lottery'] = $campaign->id;
        }
        $_SESSION["notification"] = serialize($n);
        redirect('/products/' . $product->slug);
    }*/
            
}