<?php

class pressController extends siteController {
	
    public function index(Array $params = [])
    {

        redirect('/');
        $limit = 12;
        $offset = 0;
        if(isset($_GET['page'])){
            $offset = ($_GET['page']-1) * $limit;
        }
        $query['orderBy'] = 'display_order';
        $query['limit'] = $limit;
        $query['offset'] = $offset;
        $this->viewData->shopInstagram = \Model\Press::getList($query);
//        $count =  \Model\Press::getCount($query);
//        $count = intval($count / $limit);
//        if(\Model\Press::getCount(['where'=>"active = 1"]) % $limit > 0){
//            $count++;
//        }

//        dd($_GET['page'], $offset,$limit,$this->viewData->shopInstagram, $query);
//        $this->viewData->pageCount = $count;
        $this->viewData->productsCount = \Model\Press::getCount(['where'=>"active = 1"]);
        $this->viewData->limit = $limit;
        $this->viewData->offset = $offset;
       // $this->viewData->InstaCount = \Model\Press::getCount($query);
        $this->configs['Meta Title'] = "Modern Vice - Shop Instagram";
        $this->loadView($this->viewData);
    }

    public function buildPress(){
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        $query = ['limit'=>$limit,'offset'=>$offset];
        $query['orderBy'] = 'display_order';

//        <img src="'.UPLOAD_URL.'shop_instagrams/'.$item->featured_image.'">
//        <img src="'.UPLOAD_URL.'shop_instagrams/573612a789d3a_banner3.jpg">
        $htmlArr = [];
        if($si = \Model\Press::getList($query)){
            foreach($si as $item){
                $url = $item->url ? : UPLOAD_URL.'press/'.$item->featured_image;
                $html = '';
                $html .=
                    '<div class="grid-item">
                        <a href="'.$url.'">
                            <div class="mediaWrapper">
                                <img src="'.UPLOAD_URL.'press/'.$item->featured_image.'">

                                <div class="shopSocialCardInfo">
                                    <h4><span class="abs_vert_center">'.$item->title.'</span></h4>

                                    <div>
                                        <icon class="instagram"></icon>';
                                        if (strlen($item->url) > 0) {
                                            $html .= '<h5 class="btn">View</h5>';
                                        }
                            $html .= '<span>'.date('M d', strtotime($item->display_date)).'</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>';
                $htmlArr[] = $html;
            }
            $this->toJson(['status'=>"success", 'appendHtml'=>$htmlArr]);
        } else {
            $this->toJson(['status'=>'failed']);
        }

    }

}