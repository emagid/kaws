<?php

class gift_cardsController extends siteController {

    function __construct()
    {
        redirect('/');
    }

    public function index(Array $params = []) {

		$this->viewData->gift_objs = \Model\Gift_Card::getGiftObj();
		$this->loadView($this->viewData);
	}

	public function product(Array $params = []){
		if(isset($params['id']) && ($gc = \Model\Gift_Card::getItem($params['id']))){
			$this->viewData->gift_obj = $gc;
			$this->loadView($this->viewData);
		}
	}

	function addToCart(){
		if(($gc = \Model\Gift_Card::getItem($_POST['id'])) && $gc->gift_card_type == 2){
			$cart = new \Model\Cart();
			if (!isset($_POST['user_id']) || $_POST['user_id'] == 0) {
				$cart->user_id = 0;
				$cart->guest_id = session_id();
			} else {
				$cart->user_id = $_POST['user_id'];
				$cart->guest_id = session_id();
			}
			$cart->product_id = 0;
			$cart->quantity = 1;
			$cart->details = json_encode(['gift_card_id'=>$_POST['id'],'amount'=>$gc->amount]);
			if($cart->save()){
				$this->toJson($this->buildCart());
			} else {
				$this->toJson(['status'=>'failed','message'=>'Could not add gift card. Please try again later.']);
			}
		} else {
			$this->toJson(['status'=>'failed','message'=>'Could not add gift card. Please try again later.']);
		}
	}
	public function buildCart()
	{
		$sesh_id = session_id();
		$user_id = $this->viewData->user ? $this->viewData->user->id: 0;
		$where = 'active = 1';
		if($user_id){
			$where .= " and user_id = {$user_id}";
		} else {
			$where .= " and guest_id = '{$sesh_id}'";
		}
		$cart = \Model\Cart::getList(['where'=>$where]);
		$html = '';
		$total = 0;
		$clearance = $categoryCouponBlock = $detailAmount = 0;
		foreach ($cart as $c) {
			if($c->product_id){
				$product = \Model\Product::getItem($c->product_id);
				$color_id = json_decode($c->variation,true)['color'];
				$colorObj = \Model\Color::getItem($color_id);
				$sizeId = json_decode($c->variation,true)['size'];
				$sizeObj = \Model\Size::getItem($sizeId);
				if (\Model\Clearance::checkClearance($c->product_id, $color_id, $sizeId)) {
					$clearance += $product->getPrice($color_id, $sizeId);
				} else if ($product->isCouponBlocked()) {
					$categoryCouponBlock += $product->getPrice($color_id, $sizeId);
				} else {
					$total += $product->getPrice($color_id, $sizeId);
				}
				$url = '/products/' . $product->slug;
				$img = UPLOAD_URL . 'products/' . $product->featuredImage(null,$color_id);
				$name = $product->name;
				$colorText = strlen($colorObj->name) < 20 ? $colorObj->name: substr($colorObj->name,0,20).'...';
				$sObjUs = $sizeObj->us_size;
				$sObjEu = $sizeObj->eur_size;
				$price = $product->getPrice($color_id,$sizeId);
			} else {
				$detail = json_decode($c->details,true);
				$detailAmount = $detail['amount'];
				$gc = \Model\Gift_Card::getItem($detail['gift_card_id']);

				$url = '/gift_cards/product/'.$detail['gift_card_id'];
				$img = UPLOAD_URL.'gift_cards/'.$gc->image;
				$name = $gc->name;
				$colorText = '';
				$sObjUs = '';
				$sObjEu = '';
				$price = number_format($detail['amount'],2);
			}

			$html .= '<li>
					<div class="item_container">
						<div class="bagEditItem">
							<div class="editClose circle_x">
								<circle>
									<span></span>
									<span></span>
								</circle>
							</div>
							<div class="mediaWrapper item_img">
								<a class="cartImg" href="'.$url.'">
									<img src="'.$img.'">
								</a>
								<div class="remove_item_cart btn btn_black" data-cart_id="'.$c->id.'">
									<p>Remove</p>
								</div>
							</div>
							<div class="item_info">
								<div class="itemName">
									<h4 class="as_l">' . $name . '</h4>
								</div>
								<div class="itemName">
									<h4 class="as_l">' . $colorText . '</h4>
								</div>
								<div class="itemName">
									<h4 class="as_l">US: ' . $sObjUs . ' | EU: '. $sObjEu .'</h4>
								</div>
								<div class="product_price">
									<h4 class="as_l">$' . $price . '</h4>
								</div>
							</div>
						</div>
					</div>
				</li>';
		}
		$discount = 0;
		$discountText = '';
		if(($coupon = \Model\Coupon::checkDiscount())){
			$discount = $coupon->getDiscountAmount($total);
			$discountText = $coupon->discount_type == 1 ? "OFF": "$coupon->discount_amount% OFF";
			$total -= $discount;
		}
		$total += $clearance;
		$total += $categoryCouponBlock;
		$total += $detailAmount;
		if ($html) {
			return ['status'=>"success",'sum' => '$'.number_format($total,2), 'cart' => $html, 'discount', 'discount'=>$discount,'discountText'=>$discountText];
		} else {
			return ['status'=>"failed",'message'=>"Failed to load"];
		}
	}

}