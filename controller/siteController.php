<?php

class siteController extends \Emagid\Mvc\Controller {	
	
	protected $viewData;
	protected $filters;

	function __construct(){
		parent::__construct();
//dd($this,\Model\Product::getIdBySlug($this->emagid->route['product_slug']),http_build_query(['term'=>'hello_world']));
		//Begin redirecting before content loading
		$this->legacyRedirection();

		$this->configs = \Model\Config::getItems();

		$this->viewData = (object)[
			'pages' => \Model\Page::getList(),
			'categories' => \Model\Category::getNested(0),
			'brands' => \Model\Brand::getList(['orderBy'=>'name']),
			'cart' => new stdClass(),
			'params' => [],
			'user' => null,
			'sale_banners'=>\Model\Sale_Banner::getList(['orderBy'=>'display_order']),
			'popup_ad'=>\Model\Popup_Ad::getList()
		];

		if(\Emagid\Core\Membership::isAuthenticated() && \Emagid\Core\Membership::isInRoles(['customer'])){
			$this->viewData->user = \Model\User::getItem(\Emagid\Core\Membership::userId());
		}
		$this->viewData->wholesale = $this->viewData->user && $this->viewData->user->wholesale_id && ($ws = \Model\Wholesale::getItem(null,['where'=>"id = {$this->viewData->user->wholesale_id} and status = 1"])) ? $ws: null;
		if($this->viewData->user){
			if(($cart = \Model\Cart::getList(['where'=>"guest_id = '".session_id()."' and user_id = 0"])) && !$this->viewData->wholesale) {
				foreach ($cart as $c) {
				    $campaign = \Model\Campaign::getItem(null,['SQL'=>"SELECT c.* FROM campaign c, campaign_product cp WHERE c.id = cp.campaign_id and cp.product_id = $cart->product_id  AND c.active = 1 AND c.status = 'Active' AND cp.active = 1"]);
				    $this->viewData->cart->campaign = $campaign;
                    $c->user_id = $this->viewData->user->id;
					$c->save();
				}
			}
			$cart = \Model\Cart::getActiveCart($this->viewData->user->id,session_id());
		} else {
			$cart = \Model\Cart::getActiveCart(null,session_id());
		}
		if($cart){
			$this->viewData->cart->products = array_map(function($item){
                $campaign = \Model\Campaign::getItem(null,['sql'=>"SELECT c.* FROM campaign c, campaign_product cp WHERE c.id = cp.campaign_id and cp.product_id = $item->product_id  AND c.active = 1 AND c.status LIKE 'Active' AND cp.active = 1"]);
                $this->viewData->cart->campaign = $campaign;
                if ($this->viewData->user && $this->viewData->user->wholesale_id && ($ws = \Model\Wholesale::getItem(null,['where'=>"id = {$this->viewData->user->wholesale_id} and status = 1"]))) {
                    $wsItems = $ws->getItemList();
                    $wsIds = array_map(function ($item) {
                        return $item->product_id;
                    }, $wsItems);
                    if(in_array($item->product_id,$wsIds) === false){
                        \Model\Cart::delete($this->viewData->cart->id);
                    } else {
                        $wsProd = \Model\Product::getItem($item->product_id);
                        return $wsProd;
                    }
                } else {
                    $wsProd = \Model\Product::getItem($item->product_id);
                    return $wsProd;
                }
			},$cart);
			$this->viewData->cart->cart = $cart;
			$cart_count = 0;
			$clearance = [];
			$categoryCouponBlock = [];
			$totals = [];
			$untaxable = [];
			foreach($cart as $ac){
                $cart_count += $ac->quantity;
				if($ac->product_id) {
					$prod = \Model\Product::getItem($ac->product_id);
					$color = json_decode($ac->variation, true)['color'];
					$size = json_decode($ac->variation, true)['size'];
					$couponObj = \Model\Coupon::checkDiscount();
					$categoryIds = array_map(function($i){return $i->id;},$prod->getAllCategories(true));
					if ($ac->clearance && \Model\Clearance::checkClearance($prod->id, $color, $size)) {
						if($couponObj && $couponObj->active_clearance == 1){
							$totals[] = $ac->quantity*$prod->getPrice($color, $size,true,$this->viewData->user);
						} else {
							$clearance[] = $ac->quantity*$prod->getPrice($color, $size,true,$this->viewData->user);
						}
//                } else if ($prod->isCouponBlocked()) {
                    } else if(!$couponObj){
                        $categoryCouponBlock[] = $ac->quantity*$prod->getPrice($color, $size,true,$this->viewData->user );
                    } else if ($couponObj
                        &&(
                            ($couponObj->active_categories == '' || $couponObj->active_categories == null || json_decode($couponObj->active_categories,true) == [])
                            &&
                            ($couponObj->active_products == '' || $couponObj->active_products == null || json_decode($couponObj->active_products,true) == [])
                            &&
                            ($couponObj->active_shipping == '' || $couponObj->active_shipping == null || json_decode($couponObj->active_shipping,true) == [])
                        )) {
                        $categoryCouponBlock[] = $ac->quantity*$prod->getPrice($color, $size,true,$this->viewData->user);
                    } else if(count(array_intersect(json_decode($couponObj->active_categories,true),$categoryIds)) != count($categoryIds)
                        && count(json_decode($couponObj->active_categories,true)) > 0) {
                        $categoryCouponBlock[] = $ac->quantity*$prod->getPrice($color, $size,true,$this->viewData->user);
                    } else if(in_array($prod->id,json_decode($couponObj->active_products,true)) === false
                        && count(json_decode($couponObj->active_products,true)) > 0 ) {
                        $categoryCouponBlock[] = $ac->quantity*$prod->getPrice($color, $size,true,$this->viewData->user);
                    } else if($couponObj->discount_type == 3 && (
                            (isset($_SESSION['ship_id']) && in_array($_SESSION['ship_id'],json_decode($couponObj->active_shipping,true)) === false)
                            ||
                            !isset($_SESSION['ship_id']))){

                        $categoryCouponBlock[] = $ac->quantity*$prod->getPrice($color, $size,true,$this->viewData->user);
                    } else {
                        $totals[] = $ac->quantity*$prod->getPrice($color, $size,true,$this->viewData->user);
					}
				} else if($ac->details){
					$detail = json_decode($ac->details,true);
//					$totals[] = $detail['amount'];
					$untaxable[] = $detail['amount'];
				}
			}
            $this->viewData->cart->count = $cart_count;
//			$totals = array_map(function($item){
//				$prod = \Model\Product::getItem($item->product_id);
//				$color = json_decode($item->variation,true)['color'];
//				$size = json_decode($item->variation,true)['size'];
//				return $prod->getPrice($color,$size);
//			},$cart);
			$this->viewData->cart->discount = isset($_SESSION['coupon']) && \Model\Coupon::checkDiscount()? \Model\Coupon::checkDiscount()->getDiscountAmount(array_sum($totals),$_SESSION['ship_id']): 0;
            $this->viewData->cart->giftCard = isset($_SESSION['gift_card']) && ($giftCard = \Model\Gift_Card::validateGiftCard($_SESSION['gift_card'])) ? $giftCard->getRemainingAmount(): 0;
			$this->viewData->cart->taxable = $taxable = array_sum($totals) + array_sum($clearance) + array_sum($categoryCouponBlock);
			$this->viewData->cart->total = $taxable + array_sum($untaxable);
			if(isset($_SESSION['ship_id'])){
			    $shipping = \Model\Shipping_Method::getItem($_SESSION['ship_id']);
			} else {
                if(isset($_SESSION['customerDetails']) && ($country = json_decode($_SESSION['customerDetails'],true)['ship_country'])){
                } else {
                    $country = \Model\Shipping_Method::getItem(null,['where'=>"is_default = true"])->country;
                }
                $shipping = \Model\Shipping_Method::getItem(null,['where'=>"country = '".$country."'"]);
            }
            if($shipping){
                $costs =  json_decode($shipping->cost,true);
                $_shipping = $costs[0];
                if(count($costs)>1){
                    $_shipping += $costs[1]*($cart_count-1);
                }
            } else {
			    $_shipping = 0;
            }
            $this->viewData->cart->shipping = $_shipping;
			if($this->viewData->cart->shipping == '' && ($country = json_decode($_SESSION['customerDetails'],true)['ship_country'])){
			    $countries = get_countries();
			    $code = array_search($country,$countries);
			    if($code != false){
			        $this->viewData->cart->shipping = get_duty($code,$this->viewData->cart->total);
                }
            }
//			$this->viewData->cart->minPrice = min($totals);

			$carbon = \Carbon\Carbon::now();
			if(($sale = \Model\Sale::getItem(null,['where'=>"sale_type = 3 and '$carbon' between start_date::timestamp and end_date::timestamp"]))){
				$sale_options = json_decode($sale->sale_options,true);
				$this->viewData->cart->sale = $sale->id;
				if($sale_options['buy_type'] == 1){
					if(array_sum($totals) >= $sale_options['buy']){
						asort($totals);
						$this->viewData->cart->discount = $sale->discount_type == 1 ? array_sum(array_slice($totals,0,$sale_options['get']))*$sale->discount_amount/100: $sale->discount_amount;
					}
				} else if($sale_options['buy_type'] == 2) {
					if (count($this->viewData->cart->cart) >= $sale_options['buy'] + $sale_options['get']) {
						sort($totals);
						$arr = array_slice($totals, 0, $sale_options['get']);
						$arr = array_map(function ($value) use ($sale) {
							return $sale->discount_type == 1 ? $value * $sale->discount_amount / 100 : $sale->discount_amount;
						}, $arr);
						$this->viewData->cart->discount += array_sum($arr);
					}
				}
			}
		} else {
			$this->viewData->cart->products = $this->viewData->cart->cart = [];
			$this->viewData->cart->discount = 0;
			$this->viewData->cart->total = 0;
//			$this->viewData->cart->minPrice = 0;
		}
		//TODO inelegant solution to abandoned cart
		if(!isset($_SESSION['aCart']) && $this->emagid->route['controller'] == 'checkout'){
			$cartIds = [];
			if(isset($this->viewData->cart->cart) && $this->viewData->cart->cart != null) {
				$cartIds = array_map(function ($cart) {
					return $cart->id;
				}, $this->viewData->cart->cart);
			}
			$ac = new \Model\Abandoned_Cart();
			$ac->user_id = $this->viewData->user ? $this->viewData->user->id: 0;
			$ac->guest_id = session_id();
			$ac->cart = json_encode($cartIds);
			$ac->save();
			$_SESSION['aCart'] = $ac;
		} else if(isset($_SESSION['aCart']) && $this->emagid->route['controller'] != 'checkout'){
			unset($_SESSION['aCart']);
		}

		//Send email 1 hour after abandoned cart is not set - workaround for actual cronjob
		$start = \Carbon\Carbon::now(); //set start and end to avoid overloading system on release
		$end = \Carbon\Carbon::now()->addHour();
		$query = ['where'=>"update_time + interval '2h' between '$start' and '$end' and checkout_fields like '%email%' and checkout_fields not like all (array['%\"email\":\"\"%', '%@test%']) and (send_email != 1 or send_email is null)"];
		$aCart = \Model\Abandoned_Cart::getList($query);
		foreach($aCart as $item){
			$item->sendEmail();
		}

		if(!isset($_SESSION['comparison'])){
			$_SESSION['comparison'] = [];
		}
		$this->setFilters();
	}
	
	public function index(Array $params = []){
		$this->loadView($this->viewData);
	}

	public function setFilters(){
		$this->filters = (object)[
			'where' => [],
			'type' => ['main'=>1,'combo'=>2,'spare'=>3],
//			'materials' => \Model\Material::getList(),
//			'colors' => \Model\Color::getList(),
//			'size'=>\Model\Size::getList(['orderBy'=>"us_size"]),
			'price' => ['1000', '5000', '10000', '15000'],
			'sortBy' =>['Newest'=>'new','Price: Low to High'=>'lth','Price: High to Low'=>'htl']
		];
		if (isset($_GET) && count($_GET) > 0){
			foreach($_GET as $filter=>$keyword){
				switch($filter){
					case 'price':
						$this->filters->where['price'] = 'product.price < '.$keyword;
						$this->filters->$filter = $keyword;
						$this->viewData->params['price'] = $keyword;
						break;
					case 'type':
						$this->filters->where['type'] = "product.type = '".$this->filters->type[$keyword]."'";
						$this->filters->$filter = $keyword;
						$this->viewData->params['type'] = $keyword;
						break;
					case 'material':
						$this->filters->where['material'] = 'product.id in ('.implode(',', \Model\Product_Material::get_material_products($keyword)).')';
						$this->filters->$filter = $keyword;
						$this->viewData->params['material'] = $keyword;
						break;
					case 'collection':
						$this->filters->where['collection'] = 'product.id in ('.implode(',', \Model\Product_Collection::get_collection_products($keyword)).')';
						$this->filters->$filter = $keyword;
						$this->viewData->params['collection'] = $keyword;
						break;
					case 'color':
						$this->filters->where['color'] = 'product.color = '.\Model\Color::getIdBySlug($keyword);
						$this->filters->$filter = $keyword;
						$this->viewData->params['color'] = $keyword;
						break;
				}
			}
			if(isset($_GET['filter'])){
				$this->filters->priceFilter = $_GET['filter'];
			}
		}
		$this->viewData->filters = $this->filters;
	}

	public function toJson($array)
	{
		header('Content-Type: application/json');
		echo json_encode($array);
		exit();
	}
	/**
	 * Intercept previously existing URL and redirect (directly|algorithmically)
	 **/
	public function legacyRedirection(){
		/**
		 * URI => Redirection
		 **/
		$hardLinkRedirection = ['/pages/shoe-store'=>'/nycstore'];
		if(array_key_exists($this->emagid->uri,$hardLinkRedirection)){
			redirect($hardLinkRedirection[$this->emagid->uri]);
		}

		/**
		 * /products/ redirection to search page
		 **/
		if(isset($this->emagid->route['product_slug']) && \Model\Product::getIdBySlug($this->emagid->route['product_slug']) == 0){
			$slugReplace = str_replace('-',' ',$this->emagid->route['product_slug']);
			$search = http_build_query(['term'=>$slugReplace]);
			redirect('/categories/search?'.$search);
		}
	}
}