<?php

//This controller was made to handle product ajax calls
class productController extends siteController
{
    public function buildQuickView(){
        $product_id = $_POST['id'];
        $product = \Model\Product::getItem($product_id);
        $defaultColor = $product->default_color_id ? \Model\Color::getItem($product->default_color_id) : null;
        $colorHtml = '';
        $sizeHtml = '';
        //image html
        $thumbHtml = '';
        $largeHtml = '';
        if($this->viewData->user){
            $favorite = \Model\User_Favorite::getItem(null,['where'=>"user_id = {$this->viewData->user->id} and product_id = $product->id"]);
        } else if(isset($_COOKIE['favorite']) && ($cookie = json_decode($_COOKIE['favorite'],true)) && isset($cookie['product']) && ($key = array_search($product->id,$cookie['product'])) !== false){
            $favorite = true;
        } else {
            $favorite = null;
        }
        if($product->color){
            $colors = array_map(function($item){return \Model\Color::getItem($item);},json_decode($product->color,true));
            foreach($colors as $color){
                $sta = \Model\Product_Attributes::getStatusById($product_id,$color->id);
                if($color && ($sta == null || $sta->value != 3)){
                    $active = $color->id == $product->default_color_id;
                    $colorSelect = $active ? "selected": "";
                    $colorCheckbox = $active ? 'checked="checked"': "";
                    $colorHtml .=
                        '<div class="col optionBox optionBoxRadio '.$colorSelect.'" data-id="'.$color->id.'" data-product_id="'.$product->id.'">
							<div class="media colorSwatch" style="background-image:url('.$color->swatch().')"></div>
							<p>'.$color->name.'</p>
							<input type="checkbox" value="'.$color->id.'" '.$colorCheckbox.'>
						</div>';
                }
            }
        }
        $status = \Model\Product_Attributes::getStatusById($product_id,$product->default_color_id);
        $activity = $product->getAvailability();
        if($activity != 'Active'){
            $stat = false;
        } else if($status == null || $status->value == 1 || $status->value == ''){
            $stat = true;
        } else {
            $stat = false;
        }
        if($product->size){
            $sizes = array_map(function($item){return \Model\Size::getItem($item);},json_decode($product->size,true));
            foreach($sizes as $size) {
                $sizeHtml .=
                    '<div class="col optionBox optionBoxRadio">
                        <h3 class="as_r">'.$size->us_size.'</h3>
                        <input type="checkbox" value="'.$size->id.'">
                    </div>';
            }
        }
        if($prodImg = $product->getAllProductImages()){
            $increment = 1;
            $active = true;
            foreach($prodImg as $pi) {
                $thumbActive = $active ? 'mediaThumbActive': '';
                $largeActive = $active ? 'media_enlarged_active': '';
                $thumbHtml .=
                    '<a class="mediaThumb '.$thumbActive.'" data-mediaThumb="'.$increment.'">
                        <div class="mediaWrapper">
                            <div class="media" style="background-image:url('.UPLOAD_URL.'products/'.$pi.')"></div>
                        </div>
                    </a>';
                $largeHtml .=
                    '<div class="media_enlarged media_enlarged_image '.$largeActive.' media_enlarged_'.$increment.'">
	    				<div class="mediaWrapper">
		    				<div class="media" style="background-image:url('.UPLOAD_URL.'products/'.$pi.')"></div>
		    			</div>
	    			</div>';
                $increment++;
                $active = false;
            }
        }
        $data = [
            'id'=>$product->id,
            'name'=>$product->name,
            'slug'=>$product->slug,
            'price'=>"$".number_format($product->getPrice(),2),
            'made_to_order'=>$defaultColor ? $defaultColor->made_to_order: 0,
            'status'=>$stat,
            'colors'=>$colorHtml,
            'default_color_id_name'=>$defaultColor ? $defaultColor->name : '',
            'sizes'=>$sizeHtml,
            'thumbHtml'=>$thumbHtml,
            'largeHtml'=>$largeHtml,
            'favorite'=>$favorite
        ];
        $this->toJson($data);
    }

    public function buildImages(){
        $product_id = $_POST['product_id'];
        $color_id = $_POST['color_id'];
        $size_id = isset($_POST['size_id']) ? $_POST['size_id']: null;
        $product = \Model\Product::getItem($product_id);
        $color = \Model\Color::getItem($color_id);
        $product_image = \Model\Product_Image::getList(['where'=>"product_id = $product_id and color_id like '%\"$color_id\"%' and (legshot = 0 or legshot is null)", 'orderBy'=>'display_order']);
        $legShot = \Model\Product_Image::getItem(null,['where'=>"product_id = $product_id and color_id like '%\"$color_id\"%' and legshot = 1", 'orderBy'=>'display_order']);
        $madeToOrder = \Model\Product_Attributes::getItem(null,['where'=>"product_id = $product_id and color_id = $color_id and name = 'mto'"]);
        $status = \Model\Product_Attributes::getStatusById($product_id,$color_id);
        $activity = $product->getAvailability();
//        if($color_id == $product->default_color_id && $product->featured_image != ''){
//            $featured = new stdClass();
//            $featured->image = $product->featured_image;
//            array_unshift($product_image,$featured);
//        }
        if($legShot){
            $legImg = $legShot;
        } else {
            $legImg = '';
        }
        //changes inline display css
        if($madeToOrder && $madeToOrder->value){
            $mTO = true;
        } else {
            $mTO = false;
        }
        $clear = null;
        if(isset($_GET['c']) && $_GET['c'] && ($clear = \Model\Clearance::checkClearance($product_id,$color_id,$size_id))){
            $price = number_format($product->getPrice($color_id,$size_id,true),2);
        } else {
            $price = number_format($product->getPrice($color_id,$size_id),2);
        }
        //change status 'active' or 'sold out'
        if($activity != 'Active' && $clear == null){
            $stat = false;
        } else if($status == null || $status->value == 1 || $status->value == '' || $clear){
            $stat = true;
        } else {
            $stat = false;
        }
        $thumb = '';
        $large = '';
        $increment = 2;
        if($product_image) {
            foreach ($product_image as $image) {
                if ($increment == 2) {
                    $thumb_active = 'mediaThumbActive';
                    $large_active = 'media_enlarged_active';
                } else {
                    $thumb_active = '';
                    $large_active = '';
                }
                $thumb .=
                    '<a class="mediaThumb ' . $thumb_active . '" data-mediaThumb="' . $increment . '">
                    <div class="mediaWrapper">
                        <div class="media" style="background-image:url(\'' . UPLOAD_URL . 'products/' . $image->image . '\')"></div>
                    </div>
                </a>';
                $large .=
                    '<div data-scale="1.4" data-image="' . UPLOAD_URL . 'products/' . $image->image . '" class="media_enlarged media_enlarged_image media_enlarged_' . $increment . ' ' . $large_active . '">
                    <div class="mediaWrapper">
                        <div class="media" style="background-image:url(\'' . UPLOAD_URL . 'products/' . $image->image . '\')"></div>
                    </div>
                </div>';
                $increment++;
            }
        } else {
            $thumb = [];
            $large = [];
        }
        if($legImg) {
            $legshot = '<div class="gradientOverlay"></div><div class="grid-item">
                        <div class="mediaWrapper media_wrapper_legshot">
                            <div class="media" style="background-image:url(' . UPLOAD_URL . 'products/' . $legImg->image . ')"></div>
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="mediaWrapper media_wrapper_legshot media_wrapper_legshot_flipped">
                            <div class="media" style="background-image:url(' . UPLOAD_URL . 'products/' . $legImg->image . ')"></div>
                        </div>
                    </div>';
        } else {
            $legshot = '';
        }
        if($price<$product->price){
            $sale = true;
        }else{
            $sale = false;
        }
        $arr = ['thumb'=>$thumb,'large'=>$large,'legshot'=>$legshot,'madeToOrder'=>$mTO,'status'=>$stat,'price'=>$price,'sale'=>$sale];
        $this->toJson($arr);
    }

    public function changeFavorite(){
        //guest favoring, add to cookies
        if($_POST['user_id'] == 0){
            if(isset($_COOKIE['favorite']) && $_COOKIE['favorite'] != ''){
                $product = json_decode($_COOKIE['favorite'],true)['product'];
                if(($key = array_search($_POST['product_id'],$product)) !== false) {
                    unset($product[$key]);
                } else {
                    array_push($product, $_POST['product_id']);
                }
                $prodArray = ['product'=>$product];
            } else {
                $prodArray = ['product'=>[$_POST['product_id']]];
            }
            setcookie('favorite',json_encode($prodArray),time()+60*60*24*100, "/");
            $_COOKIE['favorite'] = json_encode($prodArray);
            $this->toJson(['status'=>"success"]);
//            $this->toJson($this->buildFavorite());

            return;
        }
        //user favoring
        if(\Model\User_Favorite::changeFavorite($_POST['user_id'],$_POST['product_id'])){
            $this->toJson(['status'=>"success"]);
//            $this->toJson($this->buildFavorite());
        } else {
            $this->toJson(['status'=>"fail", 'message'=>"Problems occurred"]);
        }
    }
    public function buildFavorite()
    {

        $user_id = $this->viewData->user ? $this->viewData->user->id : 0;
        if (($user_id) && ($fav = \Model\User_Favorite::getList(['where' => "user_id = $user_id"]))) {
            $uFavorite = array_map(function ($item) {
                return $item->product_id;
            }, $fav);
        } else if (isset($_COOKIE['favorite']) && $uFavorite = json_decode($_COOKIE['favorite'], true)['product']) {
        } else {
            $uFavorite = [];
        }
        $html = '';
        foreach ($uFavorite as $uf) {
            $prod = \Model\Product::getItem($uf);
            $html .= '	
                    <li>
                        <div class="item_container">
                        <div class="bagEditItem">
                        <div class="editClose circle_x">
                        <circle>
                        <span></span>
                        <span></span>
                    </circle>
                </div>
                <div class="mediaWrapper item_img">
                    <a class="cartImg" href="/products/' . $prod->slug . '"> 
                        <img src="' . UPLOAD_URL . 'products/' . $prod->featuredImage() . '">
                    </a>

                    <div class="remove_item_favorite btn btn_black" data-product_id="'.$uf.'" data-user_id="'.$user_id.'">
                        <p>Remove</p>
                    </div>
                </div>
                <div class="item_info">
                    <div class="itemName">
                        <h4 class="as_l">' . $prod->name . '</h4>
                    </div>
                    <div class="product_price">
                        <h4 class="as_l">$' . number_format($prod->price, 2).'</h4>
                    </div>
                </div>
                <div class="item_edit_options">
                    <a class="btn btn_oreo">Add to Bag</a>
                </div>
            </div>
        </div>
    </li>';
        }
        return ['status'=>"success",'favorites' => $html];
    }

    public function addReview(){
        $ar = ['rating','name','email','title','body','product_id'];
        $review = new \Model\Review();
        foreach($ar as $a){
            $review->$a = $_POST[$a];
        }
        $review->helpful = $review->unhelpful = 0;
        echo $review->save() ? json_encode(['status'=>'success']): json_encode(['status'=>'fail','message'=>'Failed to add review. Please try again later.']);
    }

    public function upvote(){
        $review = \Model\Review::getItem($_POST['review_id']);
        $review->helpful++;
        echo $review->save() ? json_encode(['status'=>'success', 'helpful'=>$review->helpful, 'unhelpful'=>$review->unhelpful]): json_encode(['status'=>'fail', 'message'=>'Error. Please try again']);
    }
    public function downvote(){
        $review = \Model\Review::getItem($_POST['review_id']);
        $review->unhelpful++;
        echo $review->save() ? json_encode(['status'=>'success', 'helpful'=>$review->helpful, 'unhelpful'=>$review->unhelpful]): json_encode(['status'=>'fail', 'message'=>'Error. Please try again']);
    }
}