<?php

class contactController extends siteController
{

    public function index(Array $params = [])
    {
        redirect('/');
        $this->configs['Meta Keywords'] = 'contact dji nyc, dji nyc drone, dji drone camera, dji nyc drone service, dji drone accessories';
        $this->configs['Meta Description'] = 'Contact DJI NYC, drone equipment & accessories, NYC DJI products & service, cameras, gimbals, packs, propulsion systems, controllers - Official DJI Store in NYC';
        $this->configs['Meta Title'] = 'Contact Official DJI Store in NYC, Drone Accessories & Equipment';

        $this->loadView($this->viewData);
    }
    public function index_post(Array $params = [])
    {

        $obj = \Model\Contact::loadFromPost();
        $obj->admin_reply = 1;



        if($obj->save()){
            $n = new \Notification\MessageHandler('We received your message.');
            $_SESSION["notification"] = serialize($n);
        }else{
            $n = new \Notification\ErrorHandler($obj->errors);
            $_SESSION['notification'] = serialize($n);
        }
        redirect(SITE_URL);
    }


}