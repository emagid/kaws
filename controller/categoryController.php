<?php

//For categoriesController AJAX calls
class categoryController extends siteController{

    public function buildCategoryItems(){
        $mCategory = $_POST['mCategory'];
        $categoryId = $_POST['category_id'];
        $limit = $_POST['limit'];
        $offset = $_POST['offset'];
        $query['where'] = [];
        $sortBy = isset($_GET['sort_by'])?\Model\Product::$SORT_MAP[intval($_GET['sort_by'])]:null;

//        if($sortBy){
//            unset($_GET['sort_by']);
//        }
        if(is_numeric($categoryId)){
            $category = \Model\Category::getItem($categoryId);
        } else {
            $category = new stdClass();
            $category->slug = $categoryId;
        }
        $orderByCategory = '';
        if($mCategory && $categoryId) {
            if ($category->slug == 'all' || strpos(strtolower($category->slug), 'shop-by') !== false) {
                $mCatId = is_numeric($mCategory) ? $mCategory: $mCategory->id;
                $query = ['where' => ["id in (select product_id from product_categories where category_id = $mCatId and product_categories.active = 1)"]];
                $orderByCategory = ' and category_id=' . $mCatId;
            } else {
                $query = ['where' => ["id in (select product_id from product_categories where category_id = $category->id and product_categories.active = 1)"]];
                $orderByCategory = ' and category_id=' . $category->id;
            }
        }
        $colorList = [];
        if(isset($_GET)){
            foreach($_GET as $key=>$value){
                $colorSql = \Model\Color::getList(['where'=>"lower(name) like '%".strtolower($value)."%'"]);
                $colorStr = "'%\"".implode("%\"','\"%",array_map(function($item){return $item->id;},$colorSql))."\"%'";
                $explodeTags = explode(' ', strtolower(urldecode($value)));
                $tags = "'%" . implode("%','%", $explodeTags) . "%'";
                if($key == 'term'){

                    //old search logic
//                    $query['where'][] = 'lower(name) like \'%' . strtolower(urldecode($value)) . '%\'';
//                    $query['where'][] = 'lower(tags) like any (array[' . $tags . '])';

                    $query['where'][] = 'lower(name) like all (array['.$tags.'])';
                    $query['where'][] = 'lower(tags) like any (array[' . $tags . '])';
                    if($colorSql) {
                        $query['where'][] = 'color like any (array[' . $colorStr . '])';
                    }

                    $query['where'] = ["(" . implode(' OR ', $query['where']) . ")"];
                } else if ($key == 'heel_height') {
                    $val = explode(',', urldecode($value));
                    $query['where'][] = "$key in (".implode(',',$val).")";
                } else if($key == 'color' || $key == 'size'){
                    $val = explode(',',urldecode($value));
                    $query['where'][] = "$key similar to '%\"(".implode('|',$val).")\"%'";
                }

                $colorList[] = array_map(function($item){return $item->id;},$colorSql);
//                switch($key){
//                    case 'term':
//                        $query['where'][] = 'lower(name) like all (array['.$tags.'])';
//                        $query['where'] = ["(" . implode(' OR ', $query['where']) . ")"];
//                        break;
//                    case 'heel_height':
//                        $val = explode(',', urldecode($value));
//                        $query['where'][] = "$key in (".implode(',',$val).")";
//                        break;
//                    case 'color':
//                    case 'size':
//                        $val = explode(',',urldecode($value));
//                        $query['where'][] = "$key similar to '%\"(".implode('|',$val).")\"%'";
//                        break;
//                }
            }
        }
        $colorList = flatten($colorList);
//        $query['where'][] = '(id not in (select product_id from clearance where now() between start_date and end_date and active=1) or id in (select product_id from clearance where current_quantity = 0 and active=1))';
        $query['where'][] = 'product.active=1';
        $query['orderBy'] = $sortBy ? [$sortBy]: ["(select min(display_order) from product_categories where product.id = product_categories.product_id and product_categories.active=1 $orderByCategory),id"];
        $query['limit'] = $limit;
        $query['offset'] = $offset;
        $products = \Model\Product::getList(['sql'=>\Model\Category::filterSql($query)]);

        if(isset($_GET['term'])) {
            $impColor = '\'%' . implode('%\',\'%', explode(' ', strtolower($_GET['term']))) . '%\'';
            $searchedColors = ['where' => "lower(name) like all (array[$impColor])"];
            $searchColors = \Model\Color::getList($searchedColors);
            $colorList = array_map(function($item){return $item->id;},$searchColors);
        }
        $html = '';
        if($products) {
            foreach ($products as $product) {
                $prodAttr = [];
                $default_color = null;
                foreach(json_decode($product->color,true) as $value){
                    $st = \Model\Product_Attributes::getStatusById($product->id,$value);
                    if($st == null || $st->value != 3){
                        $c = \Model\Color::getItem($value);
                        if($c){
                            $prodAttr[] = $c;
                        }
                    }
                    if(in_array($value,$colorList)){
                        $default_color = $value;
                    }
                }
                $pa = \Model\Product_Attributes::getStatusById($product->id,$default_color ? : $product->default_color_id);
                if ($product->getAvailability() != 'Active' || ($pa != null && $pa->value != 1)) {
                    $soldOut = 'soldOutGridItem';
                } else {
                    $soldOut = '';
                }
//                $prodAttr = \Model\Product_Attributes::getColorList($product->id, 0);
                $featuredImageParam = $category->slug == 'all' ? null: $category->id;
                if((isset($_GET['view']) && $_GET['view'] == 'grid') || (isset($_GET['term']) && $_GET['term'])) {
                    $html .=
                        '<div class="col productGridItem ' . $soldOut . '">
                        <div id="showQuickView" class="show_mdl" data-id="' . $product->id . '" data-mdl_name="quickView">
                            <h5 class="as_m"><span><i></i><i></i></span>Quick View</h5>
                        </div>';
                    if (count($prodAttr) >= 5) {
                        $html .= '<div class="productGridItemSwatches productGridItemSwatchesSlider">';
                    } else {
                        $html .= '<div class="productGridItemSwatches">';
                    }
                    foreach ($prodAttr as $pa) {
                        $html .= '<div class="productGridItemSwatchWrapper"><img src="' . $pa->swatch() . '" class="productGridItemSwatch" data-color_id="' . $pa->id . '" data-pro_id="' . $product->id . '"/></div>';
                    }
                    $def = $default_color ? '?col='.$default_color: '';
                    $html .=
                        '</div>
                        <a href="' . SITE_URL . "products/$product->slug" .$def. '">
                            <div class="saleInfoWrapper">';
                    $product->isDiscounted() || ($product->price) > $product->basePrice() ? $html .= '<small class="saleIndicator">On Sale</small>' : $html .= '';
                    if (\Model\Event::eventSale($product->id, $product->default_color_id)) {
                        $eventAttr = \Model\Product_Attributes::getEventById($product->id, $product->default_color_id);
                        $event = \Model\Event::getItem($eventAttr->value);
                        $html .= '<div class="saleEventIcon event-icon icon" style="background-image:url(\'' . $event->getIcon() . '\')"></div>';
                    }
                    $html .= '</div>
                <div class="mediaWrapper">
                    <div class="media" style="background-image:url(' . UPLOAD_URL . 'products/' . $product->featuredImage($featuredImageParam,$default_color) . ')"></div>
                </div>
                <div class="dataWrapper">
                <h4 class="product_name">' . $product->name . '</h4>';
                    $product->isDiscounted() ? $html .= "<small>On Sale</small>" : $html .= "";
                    if ($product->msrp > 0.0) {
                        $percent = round(($product->basePrice() - $product->msrp()) * 100 / $product->basePrice());
                        $html .= '
                        <h4 class="product_price">
                            <span class="full_price">
                                <span class="currency">$</span>' . number_format($product->basePrice(), 2) . '
                            </span>
                            <span class="markdown">-' . $percent . '%</span>
                            <span class="value">$' . number_format($product->msrp(), 2) . '</span>
                        </h4>';
                    } else if ($product->isDiscounted()) {
                        $percent = round(($product->basePrice() - $product->price()) * 100 / $product->basePrice());
                        $html .=
                            '<h4 class="product_price">
                            <span class="full_price">
                                <span class="currency">$</span>' . number_format($product->basePrice(), 2) . '
                            </span>
                            <span class="markdown">-' . $percent . '%</span>
                            <span class="value">$' . number_format($product->price(), 2) . '</span>
                        </h4>';
                    } else if (($product->price) > $product->basePrice()) {
                        $percent = round(($product->price - $product->price()) * 100 / $product->price);
                        $html .=
                            '<h4 class="product_price">
                            <span class="full_price">
                                <span class="currency">$</span>' . number_format($product->price, 2) . '
                            </span>
                            <span class="markdown">-' . $percent . '%</span>
                            <span class="value">$' . number_format($product->price(), 2) . '</span>
                        </h4>';
                    } else {
                        $html .= '<h4 class="product_price">$' . number_format($product->price(), 2) . '</h4>';
                    }
                    $html .=
                        '</div>
                    <div class="soldOutSticker">
                        <p>Sold out</p>
                    </div>
                    </a>
                </div>';
                } else {
                    $priceRange = ($min = $product->getPriceRange()["min"]) == ($max = $product->getPriceRange()["max"]) ? floatval($max) :floatval($min)."-".floatval($max);
                    $html .= '<div class="collectionRow">
                <div class="collectionRowWrapper">
                    <div class="primaryProduct">
                        <a href="/products/'.$product->slug.'">
                            <div class="media">
                                <img src="'.UPLOAD_URL . 'products/' . $product->featuredImage($featuredImageParam).'">
                            </div>
                            <p class="caption">Handcrafted in Midtown Manhattan</p>
                        </a>
                    </div>
                    <div class="productExplainer">
                        <div class="box">
                            <h4 class="productName">'.$product->name.'</h4>

                            <p class="collectionStyleTag">Our Classic Colorways</p>

                            <p class="desc">'.$product->description.'</p>
                            <a class="btn invBtn"
                               href="/products/'.$product->slug.'">Shop Now <span
                                    class="middot"></span>$'.$priceRange.'</a>
                        </div>
                    </div>
                    <div class="productSliderWrapper">
                        <a class="arrowTrigger">
                            <p>More Colors</p>
                            <icon class="sliderArrowCollections"
                                  style="background-image:url(\''.FRONT_IMG.'collectionSliderArrow.png\')"></icon>
                        </a>

                        <div class="collectionProductsSlider">';
                             foreach ($product->getColors() as $col) {
                                $html .= '<div class="slideItem">
                                    <a href="/products/'.$product->slug.'?col='.$col->id.'">
                                        <div class="media">
                                            <img
                                                src="'.UPLOAD_URL . 'products/' . $product->featuredImage(null, $col->id).'">
                                        </div>
                                        <div class="info">
                                            <p><span class="variationName">'.$col->name .'</span><span
                                                    class="middot"></span>
                                            </p>
                                        </div>
                                    </a>
                                </div>';

                            }
                        $html .= '</div>
                    </div>
                </div>
            </div>';
                }
            }
        }
        $status = "success";
        $this->toJson(['itemHtml'=>$html,'status'=>$status]);
    }
  
    public function category_new(Array $params = [])
    {
        redirect('/');
        $this->configs['Meta Title'] = "Modern Vice NYC Showroom &#x2014; Book your Appointment";
        $this->loadView($this->viewData);
    }
            
    public function buildVariantImage(){
        $product_id = $_POST['product_id'];
        $color_id = $_POST['color_id'];
        $product_image = \Model\Product_Image::getItem(null,['where'=>"product_id = $product_id and color_id like '%\"$color_id\"%' and (legshot = 0 or legshot is null)", 'orderBy'=>'display_order']);
        $image = is_null($product_image)? '': UPLOAD_URL.'products/'.$product_image->image;

        $product = \Model\Product::getItem($product_id);
        $isDiscounted = $product->isDiscounted($color_id);
        $defaultPrice = $product->price;
        $base = $product->basePrice($color_id);
        $icon = '';
        if(\Model\Event::eventSale($product_id,$color_id)){
            $event = \Model\Event::getItem(\Model\Product_Attributes::getEventById($product_id,$color_id)->value);
            $isDiscounted = true;
            $icon = $event->getIcon();
            $price = $product->price($color_id);
            $percent = round(($base - $price) * 100 / $base);
        } else if(($productAttribute = \Model\Product_Attributes::getItem(null,['where'=>"product_id = $product_id and color_id = $color_id and name = 'price'"])) && $productAttribute->value > 0){
            $price = $product->price($color_id);
            if($defaultPrice>$base){
                $isDiscounted = true;
                $base = $defaultPrice;
                $percent = round(($base - $price) * 100 / $base);
            }elseif($price == $base){
                $isDiscounted = false;
                $percent = 0;
            }else{
                $isDiscounted = true;
                $percent = round(($base - $price) * 100 / $base);
            }
        }elseif ($product->msrp>0){
            $price = $product->msrp($color_id);
            $percent = round(($base - $price) * 100 / $base);
        }else{
            $price = $product->price($color_id);
            if($isDiscounted){
                $percent = round(($base - $price) * 100 / $base);
            }else{
                $percent = 0;
            }
        }
        $pa = \Model\Product_Attributes::getStatusById($product->id,$color_id);
        if ($product->getAvailability() != 'Active' || ($pa != null && $pa->value != 1)) {
            $soldOut = true;
        } else {
            $soldOut = false;
        }
        $base = number_format($base,2);
        $price = number_format($price,2);
        $url = '/products/'.$product->slug.'?col='.$color_id;
        $this->toJson(['status'=>'success','image'=>$image,'price'=>$price,'isDiscounted'=>$isDiscounted,'basePrice'=>$base,'percent'=>$percent, 'event'=>$icon,'soldOut'=>$soldOut, 'url'=>$url]);

    }
}