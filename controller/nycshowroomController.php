<?php

class nycshowroomController extends siteController {
    
    public function index(Array $params = [])
    {

        redirect('/');
        $this->configs['Meta Title'] = "Modern Vice NYC Showroom &#x2014; Book your Appointment";
        $this->loadView($this->viewData);
    }
    public function index_post(){

        redirect('/');
        $showroom = \Model\Showroom::loadFromPost();
        $showroom->save();
        $_SESSION['notification'] = serialize(new \Notification\MessageHandler('Your appointment is scheduled!'));
        redirect('/nycshowroom');
    }
            
}