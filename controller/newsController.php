<?php

class newsController extends siteController {
	
	public function index(Array $params = []){

        redirect('/');
		$where = '';

		if (count($this->filters->where) > 0){
			$where = array_merge(explode('AND', $where), $this->filters->where);
			$where = implode(' AND ', $where);
		}
			
		$this->viewData->pagination = new \Emagid\Pagination('\Model\Article',[
			'where'=> $where,
			'orderBy'=> 'insert_time desc',
			'page_size'=>'4'
		]);

		$this->viewData->articles = $this->viewData->pagination->getList();
		foreach($this->viewData->articles as $article){
			$article->insert_time = new DateTime($article->insert_time);
			$article->insert_time = $article->insert_time->format('F d, Y');
		}

		$this->loadView($this->viewData);
	}

	public function article(Array $params = []){
		$slug = (isset($params['article_slug'])) ? $params['article_slug'] : '';
		$this->viewData->article  = \Model\Article::getItem(null,['where'=>'article.slug like \''.$slug.'\'']);
		$this->viewData->article->insert_time = new DateTime($this->viewData->article->insert_time);
		$this->viewData->article->insert_time = $this->viewData->article->insert_time->format('F d, Y');

		if (!is_null($this->viewData->article->meta_keywords) && $this->viewData->article->meta_keywords != ''){
			$this->configs['Meta Keywords'] = $this->viewData->article->meta_keywords;
		}
		if (!is_null($this->viewData->article->meta_description) && $this->viewData->article->meta_description != ''){
			$this->configs['Meta Description'] = $this->viewData->article->meta_description;
		}
		if (!is_null($this->viewData->article->meta_title) && $this->viewData->article->meta_title != ''){
			$this->configs['Meta Title'] = $this->viewData->article->meta_title;
		}
		
		$this->loadView($this->viewData);
	}

}