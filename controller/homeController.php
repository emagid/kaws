<?php

class homeController extends siteController {
	
	public function index(Array $params = []){
	    if(isset($_SESSION['lotto_email']) || $_SESSION['lotto_entrant']){
            unset($_SESSION['lotto_email']);
            unset($_SESSION['lotto_entrant']);
        }
	    $this->viewData->campaigns = \Model\Campaign::getList(['where'=>"status = 'Active'"]);
	    redirect("https://kawsone.com");
//		$this->viewData->mainCategories = \Model\Category::getList(['where'=>"parent_category = 0",'orderBy'=>"display_order asc"]);
//		$this->viewData->banners = \Model\Banner::getList(['where'=>"banner_type = 1"]);
//		$this->viewData->featuredBanners = \Model\Banner::getItem(null,['where'=>"banner_type = 2"]);
//		$this->viewData->selectionBanners = \Model\Banner::getItem(null,['where'=>"banner_type = 3"]);
//		$this->viewData->alternateBanners = \Model\Banner::getItem(null,['where'=>"banner_type = 4"]);
//		$this->viewData->specialityBanners = \Model\Banner::getList(['where'=>"banner_type = 5"]);

//		$this->viewData->featured_category = \Model\Category::getItem(null,['where'=>'featured_category = 1']);
//		$this->viewData->instagram = \Model\Shop_Instagram::getList(['orderBy'=>'display_order,id desc','limit'=>4]);

		$this->loadView($this->viewData);
	}

	public function query()
	{
		$query = $_POST['q'];

		$products = \Model\Product::search($query, 20);

		$res = [];
		foreach($products as $product){
//			$category = $product->getMainCategory();
			$res[] = ['label' => $product->name, 'value' => "/products/{$product->slug}", 'image' => $product->featuredImage()];
		}

		$this->toJson($res);
	}

}