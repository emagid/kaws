<?php

class accessoriesController extends siteController {
	
    public function index(Array $params = [])
    {
        redirect('/');
        $this->configs['Meta Title'] = "";
        $this->loadView($this->viewData);
    }
    public function accessory(Array $params = []){
        redirect('/');
        $slug = (isset($params['product_slug'])) ? $params['product_slug'] : '';
        $this->viewData->product = $product = \Model\Product::getItem(null, ['where' => 'product.active = 1 AND product.slug LIKE \'' . $slug . '\'']);
        $feature_1 = json_decode($this->viewData->product->feature_1,true);
        $feature_2 = json_decode($this->viewData->product->feature_2,true);
        if($feature_1!=null){
            $this->viewData->feature_1 = $feature_1;
            if($this->viewData->product->feature_name_1!=''){
                $this->viewData->feature_1_name = $this->viewData->product->feature_name_1;
            }else{
                $this->viewData->feature_1_name = "Feature 1";
            }
        }
        if($feature_2!=null){
            $this->viewData->feature_2 = $feature_2;
            if($this->viewData->product->feature_name_2!=''){
                $this->viewData->feature_2_name = $this->viewData->product->feature_name_2;
            }else{
                $this->viewData->feature_2_name = "Feature 2";
            }
        }
        if (!is_null($this->viewData->product->meta_keywords) && $this->viewData->product->meta_keywords != '') {
            $this->configs['Meta Keywords'] = $this->viewData->product->meta_keywords;
        }
        if (!is_null($this->viewData->product->meta_description) && $this->viewData->product->meta_description != '') {
            $this->configs['Meta Description'] = $this->viewData->product->meta_description;
        }
        if (!is_null($this->viewData->product->meta_title) && $this->viewData->product->meta_title != '') {
            $this->configs['Meta Title'] = $this->viewData->product->meta_title;
        }
        $this->loadView($this->viewData);
    }

}