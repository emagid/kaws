<?php

class serviceController extends siteController {
	
    public function index(Array $params = [])
    {

        redirect('/');
        $tickets=[];
        if($this->viewData->user != null){
            foreach (Model\Service_Ticket::getList(['where'=>"user_id = {$this->viewData->user->id} OR (user_id = 0 AND email LIKE '{$this->viewData->user->email}')"]) as $ticket){
                $tickets[] = $ticket;
            }
        }
        $this->viewData->tickets = $tickets;
        $this->configs['Meta Keywords'] = 'dji nyc, drone service nyc, drone repair nyc, dji store nyc';
        $this->configs['Meta Description'] = 'Professional drone repair service center New York City official DJI store by Camrise. We provide high quality service in a given time period for your drones.';
        $this->configs['Meta Title'] = 'Drone Repair Service Center NYC, DJI NYC Store by Camrise';
        $this->viewData->products = [
            'Spark'         =>['Standard'],
            'Mavic Pro'     =>['Mavic Pro','Mavic Pro Platinum' ],
            'Phantom 4'     =>['Phantom 4','Phantom 4 Pro','Phantom 4 Pro +','Phantom 4 Advanced','Phantom 4 Advanced + '],
            'Phantom 3'     =>['Phantom 3 Standard','Phantom 3 4K','Phantom 3 Advanced','Phantom 3 Professional ','Phantom 3 SE'],
            'Phantom 2'     =>['Phantom 2 ','Phantom 2 V','Phantom 2 V+','Phantom 2 H3-3D'],
            'Inspire'       =>['Inspire 1','Inspire 1 V.2','Inspire 1 Pro','Inspire 1 Raw','Inspire 2'],
            'Osmo'          =>['Osmo','Osmo +','Osmo Pro','Osmo Raw','Osmo Mobile'],
            'Zenmuse'       =>['Zenmuse X3','Zenmuse X4s','Zenmuse X5','Zenmuse X5R','Zenmuse X5s','Zenmuse X7s','Zenmuse XT','Zenmuse Z30'],
            'Matrice 600'   =>['Matrice 600','Matrice 600 Pro'],
            'Ronin'         =>['Ronin ','Ronin M','Ronin MX','Ronin 2'],
            'Other'         =>['Matrice 100','Matrice 200','Matrice 210']];
        //$this->viewData->product_model = ['Standard','Advanced','Professional'];
        $this->loadView($this->viewData);
    }

    public function index_post(){
        $obj = \Model\Service_Ticket::loadFromPost();
        $obj->service_action_id = 1;
        $obj->user_id = $this->viewData->user->id;
        $obj->service_status_id = \Model\Service_Status::getEstimatePendingStatus()->id;
        $obj->createLog('created');
        if($obj->save()){
            $email = $obj->email;
            $fname = $obj->firstname;
            (new EmagidService\MailMaster())
                ->setTemplate('dji-service-received')
                ->addTo(['email' => $email, 'name' => 'test', 'type' => 'to'])
                ->addMergeTags([$email => [
                    'FIRST_NAME' => $fname
                ]])
                ->send();
            $n = new \Notification\MessageHandler('We received your message.');
            $_SESSION["notification"] = serialize($n);
            redirect('/service/confirm/'.$obj->id);
        } else {
            $n = new \Notification\ErrorHandler($obj->errors);
            $_SESSION['notification'] = serialize($n);
            redirect('/service');
        }
    }
    public function confirm(Array $params = []){
        $obj = \Model\Service_Ticket::getItem($params['id']);
        if($this->viewData->user){
            $this->viewData->service_ticket = $obj;
            $this->viewData->service_status = [1=>'Receive an email with the Service ID number and instructions how to ship your drone to us',
                2=>'After receiving your drone an email will be sent stating the products we have received',
                3=>'Our service team will send you an estimate of cost via e-mail. You can also check service and cost information at your established online account.',
                4=>'Once you approve the repair, the repair will commence',
                5=>'An email with payment link will be sent to you',
                6=>'The drone will be shipped and you will receive your aircraft in 1 - 2 business days'];
            $this->loadView($this->viewData);
        }else{
            $n = new \Notification\MessageHandler('Please Login to you account first.');
            $_SESSION["notification"] = serialize($n);
            redirect('/service');
        }
    }
    public function confirm_post(){
        $ticket = Model\Service_Ticket::getItem($_POST['id']);
        if(!$ticket){
            $n = new \Notification\ErrorHandler('Ticket Not found.');
            $_SESSION["notification"] = serialize($n);
            redirect('/service');
        } else {
            $ticket->comment = '<b>New Comment: </b><br /><br />'.(new \Carbon\Carbon($ticket->insert_time))->toDateTimeString().'<br /><br />'.$_POST['comment'].'<hr/>'.$ticket->comment;
            $ticket->viewed = 0;
            if($ticket->save()){
                $n = new \Notification\MessageHandler('Message submitted!');
                $_SESSION["notification"] = serialize($n);
                redirect('/service/confirm/'.$ticket->id);
            } else {
                $n = new \Notification\ErrorHandler('Comment not saved.');
                $_SESSION["notification"] = serialize($n);
                redirect('/service/confirm/'.$ticket->id);
            }
        }
    }

    public function estimate(Array $params = []){
        if(isset($params['id']) && isset($params['response'])){
            $ticket = \Model\Service_Ticket::getItem($params['id']);

            $response = $params['response'];

            $ticket->service_status_id = $response;

            if($ticket->save()){
                $sh = new \Model\Service_History();
                $sh->service_ticket_id = $ticket->id;
                $sh->service_status_id = $response;
                $sh->comment = "User Responded To estimate";
                $sh->notify = 1;
                $sh->save();

                $html = \Model\Email_Type::getItem($response == 21? 5:4)->message;
                $html = str_replace('%custName%',"{$ticket->firstname} {$ticket->lastname}",$html);
                $html = str_replace('%customerName%',"{$ticket->firstname} {$ticket->lastname}",$html); // TODO synch dbs
                $html = str_replace('%productId%',"{$ticket->product} {$ticket->product_model}",$html);
                $html = str_replace('%serviceId%',$ticket->id,$html);


                $email = $ticket->email;
                $fname = $ticket->firstname;
                $sent = new EmagidService\MailMaster();
                $sent->setSubject(\Model\Email_Type::getItem($response == 21? 5:4)->subject);


                $sent->setTemplate('dji-service-update')
                    ->addTo(['email' => $email, 'name' => 'test', 'type' => 'to'])
                    ->addMergeTags([$email => [
                        'FIRST_NAME' => $fname,
                        'COMMENT' => $html
                    ]]);
                $sent->send();

            }
            $this->viewData->ticket = $ticket;
            $this->viewData->message = $ticket->getStatusName();
            $this->loadView($this->viewData);
        } else {
            print_r($params);
            $this->viewData->message = "{$params['id']} | {$params['response']}";
            $this->loadView($this->viewData);
        }

    }
}