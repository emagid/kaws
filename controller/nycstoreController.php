<?php

class nycstoreController extends siteController {
	
    public function index(Array $params = [])
    {

        redirect('/');
        $this->configs['Meta Title'] = "NYC Store | Modern Vice";
        $this->loadView($this->viewData);
    }
}