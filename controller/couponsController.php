<?php

class couponsController extends siteController{

    function index(Array $params = []){

        redirect('/');
        $this->configs['Meta Keywords'] = 'dji nyc coupons, dji nyc deals, dji nyc, dji nyc drone, dji drone camera, dji nyc drone service, dji drone accessories';
        $this->configs['Meta Description'] = 'DJI NYC Coupons';
        $this->configs['Meta Title'] = 'DJI Drone Deals';
        $carbon = time();
        $this->viewData->coupons = \Model\Coupon::getList(['where'=>"start_time < '$carbon' AND end_time > '$carbon' AND display = 1","orderBy"=>'end_time ASC']);
        $this->loadView($this->viewData);
    }
}