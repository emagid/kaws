<?php

class faqController extends siteController {
	
    public function index(Array $params = [])
    {

        redirect('/');
        $this->viewData->faqs_privacy = new \stdClass();
        $this->viewData->faqs_privacy->faqs = new \stdClass();
        $this->viewData->faqs_ship = new \stdClass();
        $this->viewData->faqs_ship->faqs = new \stdClass();

        $this->configs['Meta Title'] = 'DJI NYC FAQ, Privacy Policy, Return, Shipping Policy | NYC DJI Store';
        $this->configs['Meta Description'] = 'Frequently asked questions concerning privacy policy, return and shipping policy. Everything you need to know to shop quickly, safely and efficiently | DJI NYC';
        $this->configs['Meta Keywords'] = 'dji nyc, dji nyc faq, dji nyc drone, dji nyc store, dji nyc service';
        $this->viewData->faqs_privacy->faqs = \Model\Faq::getList(['where'=>['category'=>'1'],'orderBy'=>'display_order']);
        $this->viewData->faqs_privacy->name = \Model\Faq::$category[1];
        $this->viewData->faqs_ship->faqs = \Model\Faq::getList(['where'=>['category'=>'2'],'orderBy'=>'display_order']);
        $this->viewData->faqs_ship->name = \Model\Faq::$category[2];

        $this->loadView($this->viewData);
    }
}