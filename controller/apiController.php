<?php


class apiController extends siteController {

    protected $response = ['errors'=>[]];
    protected $user;
    protected $campaign_id = 9;
    function __construct() {
        parent::__construct();
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: *");
        header("Access-Control-Allow-Headers: Authorization");
        if(isset($_SERVER['HTTP_APIKEY'])){
            if($_SERVER['HTTP_APIKEY'] == SKU2U_MATCHKEY){
            } else {
                $this->response['errors'][] = "Invalid api token";
                $this->response['status'] = false;
            }
        } else {
            $this->response['errors'][] = "Missing api token";
            $this->response['status'] = false;
        }
        if(isset($this->response['status']) && !$this->response['status']){
            $this->toJson($this->response);
            exit();
        }

    }

    public function get_count(){
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $time='';
            $statuses = '';
            if(!isset($_GET['orderStatus']) || $_GET['orderStatus'] == '' ) {
                $_GET['orderStatus'] == 'Ready';
            }

            if ($_GET["orderStatus"] == 'Ready') {
                $statuses = " AND (status = 'Active' AND fulfillment_status IN ('Ready'))";
            }
            else if ($_GET["orderStatus"] == 'Processed') {
                $statuses = " AND (fulfillment_status = 'Processed' AND (status = 'Active'))";
            }
            else if ($_GET["orderStatus"] == 'Shipped') {
                $statuses = " AND ((fulfillment_status = 'Processed' AND status = 'Shipped') OR fulfillment_status = 'Shipped')";
            }
            else if($_GET["orderStatus"] == 'Returned'){
                $statuses = " AND ('Returned' IN (fulfillment_status, status))";
            }

            $orders = [];
            $sql = "active = 1 AND campaign_id = $this->campaign_id $statuses";

            GLOBAL $emagid;
            $count = \Model\Order::getCount(['where'=>$sql]);
            var_dump($count);
//            $count = false;
            if($count === false){
                $this->toJson(['count'=>0,'SQL'=>$sql]);
                $this->toJson(['count'=>0]);
            }
//            $this->toJson(['count'=>0,'SQL'=>$sql]);
            $this->toJson(['count'=>$count]);
        }
    }

    public function get_orders(){
        ini_set('max_execution_time',500);
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $time='';
            $statuses = '';
            if(!isset($_GET['orderStatus']) || $_GET['orderStatus'] == '' ) {
                $_GET['orderStatus'] == 'Ready';
            }

            if ($_GET["orderStatus"] == 'Ready') {
                $statuses = " AND (status = 'Active' AND fulfillment_status IN ('Ready'))";
            }
            else if ($_GET["orderStatus"] == 'Processed') {
                $statuses = " AND (fulfillment_status = 'Processed' AND (status = 'Active'))";
            }
            else if ($_GET["orderStatus"] == 'Shipped') {
                $statuses = " AND ((fulfillment_status = 'Processed' AND status = 'Shipped') OR fulfillment_status = 'Shipped')";
            }
            else if($_GET["orderStatus"] == 'Returned'){
                $statuses = " AND ('Returned' IN (fulfillment_status, status))";
            }
            if(!isset($_GET['limit']) || !is_numeric($_GET['limit']) || $_GET['limit'] < 1 ){
                $_GET['limit'] = 500;
            }
            $orders = [];
            $sql = "active = 1 AND campaign_id = $this->campaign_id $statuses";
            if(isset($_GET['orderId']) && $_GET['orderId'] != ''){
                $sql = "id = {$_GET['orderId']}".$statuses;
            }
            foreach(\Model\Order::getList(['where'=>$sql,'limit' => $_GET['limit']]) AS $order){
                // $order->insert_time = new DateTime($order->insert_time);
                $insert_time = date(DATE_ISO8601, strtotime($order->insert_time));
                $order->fulfillment_status = 'Processed';
                $order->save();
                $items = [];
                $order_products = \Model\Order_Product::getList(['where' => "order_id = " . $order->id]);
                foreach ($order_products as $order_product) {
                    if($order_product->product_id > 0){
                        $product = \Model\Product::getItem($order_product->product_id);
                        $items[] = [
                            'itemID' => $order_product->id,
                            'productID'=> $product->id,
                            'code' => $product->sku,
                            'sku' => $product->sku,
                            'name' => $product->name,
                            //                        'upc' => $product->name,
                            //                        'weight' => $product->weight,
                            'quantity' => intval($order_product->quantity),
                            'unitPrice' => floatval($order_product->unit_price)/intval($order_product->quantity),
                            'totalPrice' => $order_product->unit_price
                        ];
                    }
                }
                $orderID = $order->id;
                // if($order->pull_attempts){
                //     $orderID .= "R$order->pull_attempts"; 
                // }
                $rOrder = [
                    'orderNumber' => $order->id,
                    'orderId' => $orderID,
                    'orderStatus' => $order->fulfillment_status,
                    'orderPaymentStatus' => $order->status,
                    'customerId' => $order->user_id,
                    'orderDate' => $insert_time,
                    'requestedCarrier' => 'UPS',
                    'requestedShipping' => in_array($order->ship_country,['US','United States'])?'Ground':'Expedited Worldwide',
                    'trackingNumber' => $order->tracking_number,
                    'discount'=>floatval($order->discount),
                    'shipping'=>floatval($order->shipping_cost),
                    'tax'=>floatval($order->tax),
                    'duty'=>floatval($order->duty),
                    'subtotal'=>floatval($order->subtotal),
    //                    'shippingMethod' => ''
                    'notes' => [],
                    'shippingAddress' => [
                        'fullName'      => $order->ship_first_name.' '.$order->ship_last_name,
                        'firstName'     => $order->ship_first_name,
                        'lastName'      => $order->ship_last_name,
                        'company'       => '',
                        'street1'       =>$order->ship_address,
                        'street2'       =>$order->ship_address2,
                        'city'          =>$order->ship_city,
                        'state'         =>$order->ship_state,
                        'country'       =>$order->ship_country,
                        'postalCode'    =>$order->ship_zip,
                        'email'         =>$order->email,
                        'phone'         =>$order->phone,
                    ],
                    'billingAddress' => [
                        'fullName'      => $order->bill_first_name.' '.$order->bill_last_name,
                        'firstName'     => $order->bill_first_name,
                        'lastName'      => $order->bill_last_name,
                        'company'       => '',
                        'street1'       =>$order->bill_address,
                        'street2'       =>$order->bill_address2,
                        'city'          =>$order->bill_city,
                        'state'         =>$order->bill_state,
                        'country'       =>$order->bill_country,
                        'postalCode'    =>$order->bill_zip,
                        'email'         =>$order->email,
                        'phone'         =>$order->phone,
                    ],
                    'items' => $items
                ];
                $orders[] = $rOrder;
            }
            $this->toJson($orders);
        }
    }

    public function update_shipment_post(){
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $order = \Model\Order::getItem($_POST['orderId']);
            if($order->campaign_id != 9 ){
                $order = false;
            }
            $label = '';
            
            if($order){
                $oorder = null;
                if($order->original_order_id){
                    $oorder = \Model\Order::getItem($order->original_order_id);
                }
                $order->fulfillment_status = $_POST['orderStatus'];
                switch($order->fulfillment_status) {
                    case 'Ready':
                        $order->status = '';
                        break;
                    case 'Processed':
                        $order->status = '';
                        break;
                    case 'Shipped':
                        if(!isset($_POST['trackingNumber']) || $_POST['trackingNumber'] == ''){
                            $this->toJson(['status'=>false,'message'=>"Parameter missing: trackingNumber"]);
                        }
                        $order->pull_attempts++;
                        $order->status = '';
                        $order->tracking_number = $_POST['trackingNumber'];
                        $label = new \Model\Labels();
                        $label->order_id = $order->id;
                        $label->tracking_number = $_POST['trackingNumber'];
                        $label->type = 1;
                        $label->save();

                        if($oorder){
                            $oorder->pull_attempts++;
                            $oorder->status = '';
                            $oorder->tracking_number = $_POST['trackingNumber'];

                            $label = new \Model\Labels();
                            $label->order_id = $oorder->id;
                            $label->tracking_number = $_POST['trackingNumber'];
                            $label->type = 1;
                            $label->save();
                        }
                        break;
                    case 'Returned':
                        $order->status = 'Returned';
                        $order->fulfillment_status = 'Returned';
                        break;
                    case 'Rejected':
                        $order->status = 'Rejected';
                        $order->fulfillment_status = 'Rejected';
                        break;
                    default:
                        $this->toJson(['status'=>false,'message'=>"Invalid status"]);
                        break;

                }
                if($order->save()){
                    if($oorder){
                        $oorder->status = $order->status;
                        $oorder->fulfillment_status = $order->fulfillment_status;
                        $oorder->save();
                    }
                    if($order->fulfillment_status == 'Shipped'){
                        if($oorder){
                            $this->sendShippedEmail($oorder);
                        } else {
                            $this->sendShippedEmail($order);
                        }
                    }
                    $this->toJson(['status'=>true,'Order'=>['orderId'=>$order->id,'originalOrderId'=>$oorder->id]]);
                } else {
                    $this->toJson(['status'=>false,'message'=>"order failed to update"]);
                }
            } else {
                $this->toJson(['status'=>false,'message'=>"order does not exist"]);
            }
        }
    }

    public function getStatus(){
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $response = [
                [
                    'code'=>'Ready',
                    'description'=>'Order Information not yet taken, can be set with update_shipment()'
                ],
                [
                    'code'=>'Processed',
                    'description'=>'Order information taken, label being created, status automatically set when get_orders is called'
                ],
                [
                    'code'=>'Shipped',
                    'description'=>'Label Created, can be set with update_shipment()'
                ],
                [
                    'code'=>'Returned',
                    'description'=>'Returned to return center, can be set with update_shipment()'
                ],
            ];
            $this->toJson($response);
        }
    }

    public function toJson($array)
    {
        header('Content-Type: application/json');
        echo json_encode($array);
        exit();
    }


    private function sendShippedEmail($order){
        if($order->tracking_number != ''){
            $html = '';
            if(count($order->getOrderProducts()) < 1){
                return false;
            }
            foreach ($order->getOrderProducts() as $orderProduct){
                $product = \Model\Product::getItem($orderProduct->product_id);
                $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
                if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
                $name = $product->name;
                $price = $product->price;
                $html .=
                    '<tr style="height:75.0pt">
                        <td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
                        </td>
                        <td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
                        
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                        
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
                        </td>
                    </tr>';
            }
            $email = new \Email\MailMaster();
            $mergeFields = [
                'ORDER_NUMBER' => $order->id,
                'TRACKING_NUMBER' => $order->tracking_number,
                'DATE' => date('Y-m-d h:i:s'),
                'DATED' => date('l, F d, Y'),
                'DATET' => date('h:i A'),
                'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                'SHIPPING' => $order->getShippingAddr(),
                'BILLING' => $order->getBillingAddr(),
                'ITEMS' => $html,
                'SUBTOTAL' => number_format($order->subtotal, 2),
                'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                'SHIPPINGNAME' => 'Shipping',
                'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
                'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
                'TOTAL' => number_format($order->total, 2)
            ];
            $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-shipping');
            try {
                $emailResp = $email->send();
                return true;
            } catch(Mandrill_Error $e){
                $n = new \Notification\ErrorHandler('email was not sent');
                $_SESSION['notification'] = serialize($n);
                return false;
            }
        } else {
            $n = new \Notification\ErrorHandler('Order does not have a tracking number, no email sent');
            $_SESSION['notification'] = serialize($n);
            return false;
        }
    }
}