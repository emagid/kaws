<?php

class abandoned_cartController extends adminController{
    function __construct()
    {
        parent::__construct('Abandoned_Cart');
    }

    function export(){
        $this->template = false;
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=abandoned-cart-'.\Carbon\Carbon::now()->format('Y-m-d').'.csv');
        $output = fopen('php://output', 'w');
        $t = ['Id','DateTime','Full Name','Email','Product'];
        fputcsv($output, $t);
        $aCart = \Model\Abandoned_Cart::getAllByField();
        foreach($aCart as $ac) {
            if($ac->getEmail()){
                foreach($ac->getProductNames() as $name){
                    $row = [$ac->id,date('M j, Y h:i:sa',strtotime($ac->update_time)),$ac->getFullName(),$ac->getEmail(),$name];
                    fputcsv($output, $row);
                }
            }
        }
    }

    function sendEmail(Array $params = []){
        if(isset($params['id']) && $params['id']){
            $ac = \Model\Abandoned_Cart::getItem($params['id']);
            if ($ac->getEmail()) {
                $html = '';
                $cart = $ac->getCartProducts();
                $chunk = array_chunk($cart, 3);
                foreach ($chunk as $cart) {
                    $html .= '<tr>';
                    foreach ($cart as $array) {
                        $product = \Model\Product::getItem($array->product_id);
                        $color = json_decode($array->variation, true)['color'];
                        $size = json_decode($array->variation, true)['size'];
                        $html .= '
                        <td width="192" style="width: 192px;" valign="top">
                            <table width="192" border="0" cellpadding="0" cellspacing="0" align="center" class="force-row" style="width: 192px;"><tr>
                                    <td class="col" valign="top" style="padding-left:12px;padding-right:12px;padding-top:18px;padding-bottom:12px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="img-wrapper">
                                            <tr>
                                                <td style="padding-bottom:18px"><a href="https://djinyc.com/products/'.$product->slug.'"><img src="' . $product->featuredImage() . '" border="0" alt="The Albatross flew across the ocean" width="168" height="110" hspace="0" vspace="0" style="max-width:100%; " class="image"></a></td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0"><tr>
                                                <td class="subtitle" style="font-family:Helvetica, Arial, sans-serif;font-size:16px;line-height:22px;font-weight:600;color:#24262b;padding-bottom:6px">
                                                    <a href="https://djinyc.com/products/'.$product->slug.'">' . $product->name . '</a>
                                                </td></tr>
                                        </table>
                                        <div class="col-copy" style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;text-align:left;color:#24262b">';
                        if ($product->basePrice() != $product->getPrice($color, $size)) {
                            $html .= '<a href="https://djinyc.com/products/'.$product->slug.'"><span style="text-decoration: line-through;color:rgba(36, 38, 43, 0.5);">$' . number_format($product->basePrice() * $array->quantity, 2) . '</span></a> ';
                        }
                        $html .= '<a href="https://djinyc.com/products/'.$product->slug.'"><span style="color:#24262b;">$' . number_format($product->getPrice($color, $size) * $array->quantity, 2) . '</span></a>
                                            </div>
                                            <br>
                                        </td>
                                    </tr>
                                </table>
                            </td>';
                    }
                    $html .= '</tr>';
                }
                $email = new \Email\MailMaster();
                $mergeTags = [
                    'NAME'=>'Valued Customer',
                    'ITEMS' => $html
                ];
                if($ac->getFullName()){
                    $mergeTags['NAME'] = $ac->getFullName();
                }
                $email->setTo(['email' => $ac->getEmail(), 'name' => ucwords($ac->getFullName()), 'type' => 'to'])->setTemplate('dji-abandoned-cart')->setMergeTags($mergeTags);
//                $email->send();
                $ac->send_email = 1;
                $ac->save();

                $n = new \Notification\MessageHandler("Email sent");
            } else {
                $n = new \Notification\ErrorHandler("This person's email is not in the system");
            }
        } else {
            $n = new \Notification\ErrorHandler("Invalid ID");
        }
        $buildQueryString = isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING']: '';
        $_SESSION['notification'] = serialize($n);
        redirect(ADMIN_URL.'abandoned_cart'.$buildQueryString);
    }
}