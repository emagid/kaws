<?php

use Emagid\Html\Form;

class newproductsController extends adminController {
	
	function __construct(){
		parent::__construct("Newproduct");
	}

	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;
		parent::index($params);
	}

	public function update(Array $arr = []) {
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
		/*$this->_viewData->product_categories = \Model\Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_collections = \Model\Product_Collection::get_product_collections($pro->id);
		$this->_viewData->product_materials = \Model\Product_Material::get_product_materials($pro->id);
		$this->_viewData->product_notes = \Model\Note::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);*/
	/*	foreach($this->_viewData->product_notes as $note){
			$note->admin = \Model\Admin::getItem($note->admin_id);
			$note->insert_time = new DateTime($note->insert_time);
			$note->insert_time = $note->insert_time->format('m-d-Y H:i:s');
		}*/
		/*$this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}*/
		$this->_viewData->product_images = [];
		$this->_viewData->brands = \Model\Brand::getList();
		$this->_viewData->categories = \Model\Category::getList();
		$this->_viewData->colors = \Model\Color::getList();
		$this->_viewData->collections = \Model\Collection::getList();
		$this->_viewData->materials = \Model\Material::getList();

		parent::update($arr);
	}
	
	public function multi_update(Array $arr = []) {
		$id_array = $_GET;
		
		$q = implode("," , $id_array);

		if (count($_GET) <= 1){
			$n = new \Notification\ErrorHandler('You must select at least one product to Multi Edit.');
           	$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'products');
		}

		$this->_viewData->q=$q;
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->product_categories = \Model\Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_collections = \Model\Product_Collection::get_product_collections($pro->id);
		$this->_viewData->product_materials = \Model\Product_Material::get_product_materials($pro->id);
		$this->_viewData->product_notes = \Model\Note::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_notes as $note){
			$note->admin = \Model\Admin::getItem($note->admin_id);
			$note->insert_time = new DateTime($note->insert_time);
			$note->insert_time = $note->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_images = [];
		$this->_viewData->brands = \Model\Brand::getList();
		$this->_viewData->categories = \Model\Category::getList();
		$this->_viewData->colors = \Model\Color::getList();
		$this->_viewData->collections = \Model\Collection::getList();
		$this->_viewData->materials = \Model\Material::getList();

		parent::update($arr);
	}
	
	public function multi_update_post(){

		$q = explode(",", $_POST['id']);
		$a = $_POST;
		
		foreach($q as $productId){
			if (is_numeric($productId)){
				$product = Model\Product::getItem($productId);
				
				if ((isset($a['product_category'])) && (count($a['product_category'])>0)  )  {
					for ($x = 0; $x < count($a['product_category']); $x++){
						$category = \Model\Product_Category::getItem(null, ['where' => "product_id = '" . $productId . "' and category_id = ".$a['product_category'][$x]]);
						if (!isset($category))	{
							$category = new Model\Product_Category();
							$category->product_id = $productId;
							$category->category_id = $a['product_category'][$x];
							$category->save();
						}
					}
				}

				if ((isset($a['product_collection'])) && (count($a['product_collection'])>0)  )  {
					for ($x = 0; $x < count($a['product_collection']); $x++){
						$collection = \Model\Product_Collection::getItem(null, ['where' => "product_id = '" . $productId . "' and collection_id = ".$a['product_collection'][$x]]);
						if (!isset($collection))	{
							$collection = new Model\Product_Collection();
							$collection->product_id = $productId;
							$collection->collection_id = $a['product_collection'][$x];
							$collection->save();
						}
					}
				} 

				if ((isset($a['product_material'])) && (count($a['product_material'])>0)  )  {
					for ($x = 0; $x < count($a['product_material']); $x++){
						$material = \Model\Product_Material::getItem(null, ['where' => "product_id = '" . $productId . "' and material_id = ".$a['product_material'][$x]]);
						if (!isset($material))	{
							$material = new Model\Product_Material();
							$material->product_id = $productId;
							$material->material_id = $a['product_material'][$x];
							$material->save();
						}
					}
				}

				foreach($a as $key1 => $value1)	{
					if (!empty($value1) && $key1 != "id"){
						$product->$key1 = $value1;
						$product->save();
					}
				}
			}
		}
		redirect(ADMIN_URL.'products');
	}

	 

	protected function afterObjSave($obj){
		if (isset($_POST['note'])){
			$note = new \Model\Note();
			$note->name = $_POST['note']['name'];
			$note->description = $_POST['note']['description'];
			$note->admin_id = \Emagid\Core\Membership::userId();
			$note->product_id = $obj->id;
			$note->save();
		}
	}
	
	function upload_images($params) {
		header("Content-type:application/json");
		$data = [];
		$data['success'] = false;
		$data['redirect'] = false;
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
		if((int)$id>0) {
			$product = Product::getItem($id);
			if($product==null) {$data['redirect']=true;}
			$this->save_upload_images($_FILES['file'], $product->id, UPLOAD_PATH.'products'.DS,[]);
			$data['success'] = true;
		}
	
		//print_r($_FILES);
		echo json_encode($data);exit();
	}
	
	private function save_upload_images($files,$obj_id,$upload_path,$sizes=[]) {
		$counter = 1;
		// get highest display order counter if any images exist for specified listing
		$image_high = Product_Image::getItem(null,['where'=>'product_id='.$obj_id,'orderBy'=>'display_order','sort'=>'DESC']);
		if($image_high!=null) {
			$counter = $image_high->display_order+1;
		}
		foreach($files['name'] as $key=>$val) {
			$product_image = new Product_Image();
			$temp_file_name = $files['tmp_name'][$key];
			$file_name = uniqid() . "_" . basename($files['name'][$key]);
			$file_name = str_replace(' ', '_', $file_name);
			$file_name = str_replace('-', '_', $file_name);
	
			$allow_format = array('jpg', 'png', 'gif','JPG');
			$filetype = explode("/", $files['type'][$key]);
			$filetype = strtolower(array_pop($filetype));
			//$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
			//$filetype = array_pop($filetype);
			if($filetype == 'jpeg' || $filetype=="JPG"){$filetype = 'jpg';}
	
			if(in_array($filetype, $allow_format)){
	
				$img_path = compress_image($temp_file_name, $upload_path . $file_name, 60,$files['size'][$key]);
				if($img_path===false) {
					move_uploaded_file($temp_file_name, $upload_path . $file_name);
				}
				//move_uploaded_file($temp_file_name, $upload_path . $file_name);
				 
				if(count($sizes)>0) {
					foreach($sizes as $key=>$val) {
						if(is_array($val) && count($val)==2) {
							resize_image_by_minlen($upload_path,
							$file_name, min($val), $filetype);
	
							$path = images_thumb($file_name,min($val),
									$val[0],$val[1], file_format($file_name));
							$image_size_str = implode('_',$val);
							copy($path, $upload_path.$image_size_str.$file_name);
						}
					}
				}
	
				$product_image->image = $file_name;
				$product_image->product_id = $obj_id;
				$product_image->display_order = $counter;
				$product_image->save();
			}
			$counter++;
		}
	}

	public function search(){
		$products = \Model\Product::search($_GET['keywords']);

		echo '[';
		foreach($products as $key=>$product){
			echo '{ "id": "'.$product->id.'", "name": "'.$product->name.'", "featured_image": "'.$product->featured_image.'", "price":"'.$product->price.'", "mpn":"'.$product->mpn.'" }';
			if ($key < (count($products)-1)){
				echo ",";
			}
		}
		echo ']';
	}   

	public function google_product_feed(){
        if(isset($_SESSION["google_feed_generated"]) && $_SESSION["google_feed_generated"] == 1){
            $this->_viewData->generated = true;
            unset($_SESSION["google_feed_generated"]);
        }
        $this->loadView($this->_viewData);
    }

    public function google_product_feed_post(){

    	$genders = [ 1 => 'unisex', 2 => 'male', 3 => 'female' ];
    	$type = "Apparel & Accessories > Jewelry > Watches";
    	$condition = "new";
        $availability = "in stock"; // "out of stock"
        $gpc = 201;
        $identifier_exists = "TRUE";
        $age_group = "adult";

        $f = fopen(UPLOAD_PATH."/google_product_feed.txt","w"); fclose($f); //clear the file
        $f = fopen(UPLOAD_PATH."/google_product_feed.txt","a"); //open with append write mode

        $header = "id\ttitle\tdescription\tgoogle product category\tlink\timage link\tcondition\tavailability\tprice\tbrand\tmpn\tidentifier exists\tcolor\tage group\tgender\tproduct type";
        fwrite($f, $header);

        $sql = "SELECT product.id as id, product.name as name, product.description as description, product.slug as slug, ";
        $sql .= "product.featured_image as featured_image, product.price as price, product.mpn as mpn, brand.name as brand_name, ";
        $sql .= "color.name as color_name, product.gender as gender ";
        $sql .= "FROM product ";
        $sql .= "INNER JOIN brand ON (product.brand = brand.id) ";
        $sql .= "INNER JOIN color ON (product.color = color.id) ";
        $sql .= "WHERE product.active = 1 ";

        $products = $this->emagid->db->getResults($sql);
        $line_items = "";
        foreach($products as $product){
            $id = $product['id'];
            $title = $product['name'];
            $description = str_replace('\\', '\\\\', $product['description']);
            $description = str_replace('<p>', ' ', $description);
            $description = str_replace('</p>', ' ', $description);
            $description = str_replace('<br>', ' ', $description);
            $description = str_replace('<br />', ' ', $description);
            $description = str_replace('<br/>', ' ', $description);
            $description = str_replace("\n", " ", $description);
            $description = str_replace('\n', ' ', $description);
            $description = str_replace("\r", " ", $description);
            $description = str_replace('\r', ' ', $description);
            $description = str_replace("\t", " ", $description);
            $description = str_replace('\t', ' ', $description);
            $description = str_replace(PHP_EOL,' ',$description);

            $link = "http://www.modernvice.com/product/".$product['slug']; //TODO st-dev check if valid url

			$image_link = $product['featured_image'];
			if (is_null($image_link) || trim($image_link) == '' || !file_exists(UPLOAD_PATH.'products'.DS.$image_link)){
				$image_link = "http://www.modernvice.com".ADMIN_IMG.'modernvice_shoe.png';
			} else {
				$image_link = "http://www.modernvice.com".UPLOAD_URL . 'products/' . $image_link;
			}

            $price = $product['price'] . " USD";
            $brand = $product['brand_name'];
            $mpn = $product['mpn'];
            $color = $product['color_name'];
            $gender = $genders[$product['gender']];

            $line_item = "\n".$id."\t".$title."\t".$description."\t".$gpc."\t".$link."\t".$image_link."\t".$condition."\t".$availability."\t".$price."\t".$brand."\t".$mpn."\t".$identifier_exists."\t".$color."\t".$age_group."\t".$gender."\t".$type;
            fwrite($f, $line_item);
        }
        fclose($f); 
        $_SESSION["google_feed_generated"] = 1;
        redirect(ADMIN_URL.'products/google_product_feed');
    }
    
	
}






















