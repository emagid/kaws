<?php

class brandsController extends adminController {
	
	function __construct(){
		parent::__construct("Brand");
	}
  	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		
		$this->_pageSize = 20;
		$params['queryOptions'] = ['orderBy'=>'brand.name'];
		parent::index($params);
	}

	function beforeLoadUpdateView(){
		$this->_viewData->products = \Model\Product::getList(['where'=>' product.brand = '.$this->_viewData->brand->id]);
	}

	function afterObjSave($obj){
		if (!is_null($obj->banner) && $obj->banner != ""){
			$image = new \Emagid\Image();
			$image->fileName = $obj->banner;
			$image->fileType = explode(".", $image->fileName);
			$image->fileType = strtolower(array_pop($image->fileType));
			$image->fileDir = UPLOAD_PATH.'brands'.DS;
			$image->resize('1200_344'.$image->fileName, false, 1200, 344);
		}
	}

	function update_relationships($obj){}

	public function search(){
		$brands = \Model\Brand::search($_GET['keywords'], 5);

		echo '[';
		foreach($brands as $key=>$brand){
			echo '{ "id": "'.$brand->id.'", "name": "'.$brand->name.'", "image": "'.$brand->image.'" }';
			if ($key < (count($brands)-1)){
				echo ",";
			}
		}
		echo ']';
	}
  	
}