<?php

class price_alertsController extends adminController {
	
	function __construct(){
		parent::__construct("Price_alert");
	}

    
    	function index(Array $params = []){
    		global $emagid ;
		  	
		 $this->_viewData->alerts = \Model\Price_Alert::getList();
	 

$this->emagid->getDb()->execute("UPDATE price_alert set viewed='TRUE' where viewed='FALSE'");

		 parent::index($params);
	}

 function respond(Array $params = []){
    		 $id = $params['id'];
		 $this->_viewData->alert = \Model\Price_Alert::getItem($id);
		
		 parent::index($params);
	}
/*TODO st-dev price alert removed, restore when used
	 function do_respond(Array $params = []){
	 	$this->template = FALSE;
    		 $id = $_POST['id'];
    		 $respond = $_POST['respond'];
		 	 $alert = \Model\Price_Alert::getItem($id);
		 	 $alert->code = md5($alert->insert_time.$alert->name.$alert->email.mt_rand(100,9999));
		 	 $alert->save();
		 	 $product = \Model\Product::getItem($alert->product_id);
		 	 if(!empty($_POST['respond'])){
	    	$email = new \Emagid\Email(); 
	    	global $emagid;
		$emagid->email->from->email = 'PriceAlert@itmustbetime.com';
	    	$email->addTo($alert->email);
	    	 
	    	$email->addBcc("PriceAlert@itmustbetime.com");
			$email->subject('Price Alert! '.$product->name.' '.$product->mpn);
			$email->body =  '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="http://itmustbetime.com/content/frontend/img/logo.png" alt="Luggage"></center>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
	<tbody>
		<tr>
			<td style="padding:0in 0in 0in 0in">
				<p class=mm "MsoNormal" ><a href="http://itmustbetime.com/brands" target="_blank"><span  style="text-decoration:none;color:black;">BRANDS </span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://itmustbetime.com/category/new-arrivals" target="_blank"><span style="text-decoration:none;color:black;">NEW ARRIVALS</span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://itmustbetime.com/category/men-s" target="_blank"><span style="text-decoration:none;color:black;">MEN`S</span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://itmustbetime.com/category/women-s" target="_blank"><span style="text-decoration:none;color:black;">WOMEN`S</span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://itmustbetime.com/category/accessories" target="_blank"><span style="text-decoration:none;color:black;">ACCESSORIES</span></a>&nbsp;</p>
			</td> 
			<td style="padding:0in 0in 0in 0in">
				<p class="MsoNormal"><a href="http://itmustbetime.com/category/pre-owned-clearance" target="_blank"><span style="text-decoration:none;color:black;">PRE-OWNED &amp; CLEARANCE</span></a>&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
	<tr style="height:.75pt">
		<td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
	</tr>
	<tr style="height:.75pt">
		<td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
			<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
				<tbody>
					<tr style="height:22.5pt">
						<td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
							<p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Price Alert</span></b> </p>
						</td>
						<td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
							<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Product:</span></b> </p>
						</td>
						<td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
							<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042"><a href="https://itmustbetime.com/product/'.$product->slug.'" style="color:black"> '.$product->name.' '.$product->mpn.'</a></span> </p>
						</td>
					</tr>
					<tr style="height:22.5pt">
						<td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
							<p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Special price:</span></b> </p>
						</td>
						<td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
							<p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:green">$'.$alert->price.'</span> </p>
						</td>
					</tr>
				</tbody>
			</table>';


	 $email->body .='<p class="MsoNormal"><span>&nbsp;</span></p>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%">
				<tbody>
					<center>'.$_POST['respond'].'</center>
				</tbody>
				</table>';
 
	 	$email->body .='<p class="MsoNormal"><span>&nbsp;</span></p>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%">
				<tbody>
					<center>Buy <a href="https://itmustbetime.com/product/'.$product->slug.'?alert='.$alert->code.'" style="color:black"> '.$product->name.' </a></center>
				</tbody>
				</table>';
			  $email->send();
					
			$n = new \Notification\MessageHandler('Sent!');
	         $_SESSION["notification"] = serialize($n);
			}else{
				$n = new \Notification\ErrorHandler("No respond text!");
	           	$_SESSION["notification"] = serialize($n);
			}
		 redirect(ADMIN_URL.'price_alerts/');
		// parent::index($params);
	}*/


}