<?php

use Emagid\Html\Form;
use Emagid\Core\Membership;

require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class ordersController extends adminController
{

    function __construct()
    {
        parent::__construct("Order");
    }

    public function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->page_title = "Manage Web Orders";
       /* //$order_by=$_GET['order'];
        //$limit=$_GET['limit'];
        if (Empty($_GET['sort']) || Empty($_GET['sort_param'])) {
            $params['queryOptions']['orderBy'] = "insert_time DESC";
        } else {
            $sort_by = $_GET['sort'];
            $sort_param = $_GET['sort_param'];
            $params['queryOptions']['orderBy'] = $sort_by . ' ' . $sort_param;
        }
        if (!Empty($_GET['status_show'])) {
            $st = explode('%2c', $_GET['status_show']);
            $where = "status in (";
            $it = 0;
            foreach($st as $queryString) {
                $where .= "'".$queryString."'";
                if(++$it<count($st)){
                    $where .= ',';
                }
            }
            $where .= ")";
            $params['queryOptions']['where'][] = $where;
        }

        $params['queryOptions']['limit'] = 10;
        if (!Empty($_GET['how_many']))
        {
            $params['queryOptions']['page_size'] = $_GET['how_many'];
        }
        else{
            $params['queryOptions']['page_size'] = 10;
        }
        if(isset($_GET['filter'])){
            switch($_GET['filter']){
                case 'sto':
                    $params['queryOptions']['where'][] = 'in_store = 1';break;
                case 'inv':
                    $params['queryOptions']['where'][] = 'id in (select order_id from invoice)';break;
                case 'onl':
                    $params['queryOptions']['where'][] = '(in_store != 1 or in_store is null) and id not in (select order_id from invoice)';break;
            }
        }
        $params['queryOptions']['where'][] = "insert_time > '2017-12-15'";
//        $params['queryOptions']['where'][] = "(status not in ('Declined', 'Canceled', 'Incomplete PayPal'))";
        if(isset($_GET['sf'])){
            switch($_GET['sf']){
                case 'processed':
                    $params['queryOptions']['where'][] = "(status NOT IN ('Declined','Canceled','Refunded','Incomplete PayPal','Incomplete Amazon','Banned','Limit Reached') AND fulfillment_status = 'Processed')";break;
                case 'fill':
                    $params['queryOptions']['where'][] = "(fulfillment_status = 'Ready')";break;
                case 'filled':
                    $params['queryOptions']['where'][] = "(status NOT IN ('Declined','Canceled','Refunded','Incomplete PayPal','Incomplete Amazon','Banned','Limit Reached'))";break;
                case 'inc':
                    $params['queryOptions']['where'][] = "(status IN ('Declined','Canceled','Refunded','Incomplete PayPal','Incomplete Amazon','Banned','Limit Reached'))";break;
                case 'all':default:break;
            }
        } else {
            $params['queryOptions']['where'][] = "(status NOT IN ('Declined','Canceled','Refunded','Incomplete PayPal','Incomplete Amazon','Banned','Limit Reached') AND (fulfillment_status NOT IN('Ready','Processed','Fulfilled') OR fulfillment_status IS NULL))";
        }
        if(isset($_GET['cf'])){
            $this->_viewData->campaign = $campaign1 = \Model\Campaign::getItem($_GET['cf']);
            $params['queryOptions']['where'][] = "(campaign_id = {$_GET['cf']})";
        } else {
            $this->_viewData->campaign = $campaign1 = \Model\Campaign::getItem(null);
            $params['queryOptions']['where'][] = "(campaign_id = $campaign1->id)";
        }
        $params['queryOptions']['where'][] = "insert_time > '2017-12-15'";
        $this->_viewData->start = '';
        $this->_viewData->end = '';
        if(isset($_GET['t'])){
            $get = explode(',',urldecode($_GET['t']));
            $start = date('Y-m-d H:i:s',$get[0]); $end = date('Y-m-d H:i:s',$get[1]);
            $this->_viewData->start = $start;
            $this->_viewData->end = $end;
            if(isset($params['queryOptions']['where'])){
                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
            } else {
                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
            }
        }
        if(isset($params['queryOptions']['where'])){
            $params['queryOptions']['where'] = implode(' and ',$params['queryOptions']['where']);
        }
        if(count(array_intersect(array_keys($_GET),['t','sf','filter','sort','sort_param','status_show'])) > 0 && !isset($_GET['how_many'])){
            $params['queryOptions']['page_size'] = 10;
        }*/
        $this->_viewData->product_counts = \Model\Campaign::getAllProductsCount();
        $params['queryOptions']['where'][] = "active = 0";
        parent::index($params);
    }

    public function overview(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->page_title = "Manage Web Orders";
        //$order_by=$_GET['order'];
        //$limit=$_GET['limit'];
//        if (Empty($_GET['sort']) || Empty($_GET['sort_param'])) {
//            $params['queryOptions']['orderBy'] = "insert_time DESC";
//        } else {
//            $sort_by = $_GET['sort'];
//            $sort_param = $_GET['sort_param'];
//            $params['queryOptions']['orderBy'] = $sort_by . ' ' . $sort_param;
//        }
//        if (!Empty($_GET['status_show'])) {
//            $st = explode('%2c', $_GET['status_show']);
//            $where = "status in (";
//            $it = 0;
//            foreach($st as $queryString) {
//                $where .= "'".$queryString."'";
//                if(++$it<count($st)){
//                    $where .= ',';
//                }
//            }
//            $where .= ")";
//            $params['queryOptions']['where'][] = $where;
//        }

        $params['queryOptions']['limit'] = 20;
        if (!Empty($_GET['how_many']))
        {
            $params['queryOptions']['page_size'] = $_GET['how_many'];
        }
        else {
            $params['queryOptions']['page_size'] = 20;
        }
//        $params['queryOptions']['where'][] = "insert_time > '2017-12-15'";
//        $params['queryOptions']['where'][] = "(status not in ('Declined', 'Canceled', 'Incomplete PayPal'))";
        if(isset($_GET['sf'])){
            switch($_GET['sf']){
                case 'rejected':
                    $params['queryOptions']['where'][] = "('Rejected' IN (status,fulfillment_status) )";break;
                    break;
                case 'returned':
                    $params['queryOptions']['where'][] = "('Returned' IN (status,fulfillment_status) )";break;
                    break;
                default:
                    $params['queryOptions']['where'][] = "(coalesce(status,'Active') = ('Active',''))";break;
                    break;    
            }
        } else {
            $params['queryOptions']['where'][] = "(coalesce(status,'Active') IN ('Active',''))";
        }
       if(isset($_GET['cf'])){
           $this->_viewData->campaign = $campaign1 = \Model\Campaign::getItem($_GET['cf']);
           $params['queryOptions']['where'][] = "(campaign_id = {$_GET['cf']})";
       } else {
           $this->_viewData->campaign = $campaign1 = \Model\Campaign::getItem(null);
           $params['queryOptions']['where'][] = "(campaign_id = $campaign1->id)";
       }
        $this->_viewData->start = '';
        $this->_viewData->end = '';
//        if(isset($_GET['t'])){
//            $get = explode(',',urldecode($_GET['t']));
//            $start = date('Y-m-d H:i:s',$get[0]); $end = date('Y-m-d H:i:s',$get[1]);
//            $this->_viewData->start = $start;
//            $this->_viewData->end = $end;
//            if(isset($params['queryOptions']['where'])){
//                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
//            } else {
//                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
//            }
//        }
       if(isset($params['queryOptions']['where'])){
           $params['queryOptions']['where'] = implode(' and ',$params['queryOptions']['where']);
       }
        if(count(array_intersect(array_keys($_GET),['t','sf','filter','sort','sort_param','status_show'])) > 0 && !isset($_GET['how_many'])){
            $params['queryOptions']['page_size'] = 10;
        }
        $this->_viewData->product_counts = \Model\Campaign::getAllProductsCount(6);
        parent::index($params);
    }

    public function beforeLoadIndexView()
    {
        $this->_viewData->products = [];
        foreach ($this->_viewData->orders as $order) {
            $order->insert_time = new DateTime($order->insert_time);
            $order->insert_time = $order->insert_time->format('m-d-Y H:i:s');
            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);
            $this->_viewData->products[$order->id] = [];
            foreach ($order_products as $order_product) {
                $product = \Model\Product::getItem($order_product->product_id);
                $product->brand_name = \Model\Brand::getItem($product->brand)->name;
                $this->_viewData->products[$order->id][] = $product;
            }
        }
    }

    public function beforeLoadUpdateView()
    {
        if ($this->_viewData->order->id != 0 && (is_null($this->_viewData->order->viewed) || !$this->_viewData->order->viewed)) {
            $this->_viewData->order->viewed = true;
            $this->_viewData->order->save();
        }
        $shippings = [];
        if($this->_viewData->order->getOrderProducts() && count($this->_viewData->order->getOrderProducts()) > 0){
            foreach(\Model\Shipping_Method::getList() as $ship){
                $costs = json_decode($ship->cost,true);
                $cost = $costs[0];
                $totalProducts = -1;
                foreach ($this->_viewData->order->getOrderProducts() as $i => $orderProduct){
                    $totalProducts += $orderProduct->quantity;
                    if($i > 0){
                        $cost += $costs[1]*$totalProducts;
                    }
                }
                $shippings[] = [$ship->country .' '.$ship->name,$cost];
            }
        }
        if($this->_viewData->order->original_order_id){
            $o_order = \Model\Order::getItem($this->_viewData->order->original_order_id);
            if($o_order) {
                $this->_viewData->original_order = $o_order;
            }
        }
        $this->_viewData->shippings = $shippings;
        $this->_viewData->order_status = \Model\Order::$status;

        $shipping_methods = \Model\Shipping_Method::getList(['orderBy' => 'shipping_method.id']);
        $this->_viewData->shipping_methods = [];
        foreach ($shipping_methods as $shipping_method) {
            $this->_viewData->shipping_methods[$shipping_method->id] = $shipping_method->name;
        }

        $this->_viewData->order_products = \Model\Order_Product::getList(['where' => "order_id=" . $this->_viewData->order->id]);
        foreach ($this->_viewData->order_products as $order_product) {
            if($order_product->product_id > 0){
                $order_product->product = \Model\Product::getItem($order_product->product_id);
                $order_product->type = 'product';
            } else {
                $misc = json_decode($order_product->details,true);
                if(isset($misc['misc_name'])){
                    $miscProduct = new stdClass();
                    $miscProduct->id = '';
                    $miscProduct->name = $misc['misc_name'];
                    $miscProduct->featured_image = '';
                    $miscProduct->slug = '';
                    $order_product->product = $miscProduct;
                    $order_product->type = 'misc';
                } else if(isset($misc['gift_card_id'])){
                    $giftCard = \Model\Gift_Card::getItem($misc['gift_card_id']);
                    $giftCard->featured_image = $giftCard->image;
                    $order_product->product = $giftCard;
                    $order_product->type = 'gift card';
                }
            }
        }

        if (is_null($this->_viewData->order->user_id)||!($this->_viewData->order->user_id)) {
            $this->_viewData->order->user = null;
        } else {
            $this->_viewData->order->user = \Model\User::getItem($this->_viewData->order->user_id);
        }

        if (is_null($this->_viewData->order->coupon_code)) {
            $this->_viewData->coupon = null;
            $this->_viewData->savings = '0';
        } else {
            $this->_viewData->coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $this->_viewData->order->coupon_code . "'"]);
            if ($this->_viewData->order->coupon_type == 1) {//$
                $this->_viewData->savings = $this->_viewData->order->coupon_amount;
            } else if ($this->_viewData->order->coupon_type == 2) {//%
                $this->_viewData->savings = $this->_viewData->order->subtotal * $this->_viewData->order->coupon_amount / 100;
            }
        }

        if (!is_null($this->_viewData->order->cc_number)) {
            $this->_viewData->order->cc_number = '****' . substr($this->_viewData->order->cc_number, -4);
        }

        $this->_viewData->campaign = $campaign = \Model\Campaign::getItem($this->_viewData->order->campaign_id);
        if($campaign != null){
            $campaignProducts = [];
            foreach($campaign->getProducts() as $campaignProd){
                $product = \Model\Product::getItem($campaignProd->id);
                $campaignProducts[] = $product;
            }
            $this->_viewData->campaign->products = $campaignProducts;
        }
    }

    public function mail(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

        $mail = \Model\Mail_Log::getItem($params['id']);
        echo $mail->text;
        $this->template = FALSE;


    }

    public function print_packing_slip(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

        $this->slip_pack($id);
        $this->template = FALSE;


    }

    public function apply_tracking_numbers(Array $params = []){
        require(__DIR__.'/../../bin/applyTrackingNumbers.php');
        global $emagid;
        $db = $emagid->getDb();
        $resultes = $db->getResults($trackSql);
    }

     public function upd_fraud(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

         $post_array = array(
                                'ApiLogin' => 'YwZzFUKX66lx',
                                'ApiKey' => '5rhsxiIkxDVvgiULV1lTxltcfeamlwUr',
                                'Action' => 'getOrderStatus',
                                'OrderNumber' => $id
                            );

// Convert post array into a query string
                            $post_query = http_build_query($post_array);

// Do the POST
                            $ch = curl_init('https://www.eye4fraud.com/api/');
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec($ch);
                            curl_close($ch);
                            preg_match_all('~<value>(.*)</value>~', $response, $out_status);

                                $order = \Model\Order::getItem($id);
                                $order->fraud_status =  $out_status[0][2];
                                $order->save();
                                redirect($_SERVER["HTTP_REFERER"]);
        $this->template = FALSE;


    }

    public function getVariations(){
        $product_id = isset($_GET['product_id']) ? $_GET['product_id']: null;
        if($product_id){
            $product = \Model\Product::getItem($product_id);
            $colors = $product->getColors();
            $sizes = $product->getSizes();
            $colorHtml = $sizeHtml ='<option value="0">None</option>';
            $price = $product->getPrice($colors[0]->id);
            foreach($colors as $color){
                $colorHtml .= '<option value="'.$color->id.'">'.$color->name.'</option>';
            }
            foreach($sizes as $size){
                $sizeHtml .= '<option value="'.$size->id.'">'.$size->us_size.'</option>';
            }
            echo json_encode(['status'=>"success", 'colorHtml'=>$colorHtml, 'sizeHtml'=>$sizeHtml, 'subtotal'=>$price]);
        }
    }

    public function setColorVariant(){
        $product_id = isset($_GET['product_id']) ? $_GET['product_id']: null;
        $color_id = isset($_GET['color_id']) ? $_GET['color_id']: null;
        if($product_id && $color_id){
            $product = \Model\Product::getItem($product_id);
            echo json_encode(['status'=>"success", 'price'=>$product->getPrice($color_id)]);
        } else {
            echo json_encode(['status'=>"failed"]);
        }
    }

    function export(Array $params = []){
        $this->template = false;
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=orders-'.\Carbon\Carbon::now()->format('Y-m-d').'.csv');
        $output = fopen('php://output', 'w');
        $t = ['Id','DateTime','Bill Name','Ship Name','Bill Zip','Ship Zip','Products','Email','Status','Payment Status','Total','Coupon','Purchase Type'];
        fputcsv($output, $t);
        $orders = \Model\Order::getList(['where'=>'status in (\'New\',\'Paid\',\'Shipped\',\'Complete\',\'Delivered\',\'Processed\')']);
        foreach($orders as $o) {
            $order_products = $o->getOrderProducts();
            if($order_products) {
                foreach ($order_products as $op) {
                    if ($o->getInvoice()) {
                        $payType = 'Invoice';
                    } else if ($o->in_store) {
                        $payType = 'In Store';
                    } else {
                        $payType = 'Online';
                    }
                    $row = [$o->id, date('M j, Y h:i:sa', strtotime($o->insert_time)), $o->billName(), $o->shipName(), $o->bill_zip, $o->ship_zip, $op->getProductName(), $o->email, $o->status, $o->payment_status, '$' . number_format($o->total, 2), $o->coupon_code, $payType];
                    fputcsv($output, $row);
                }
            }
        }
    }

    public function getShippingCost(){
        $country = $_GET['country'];
        $shipping = \Model\Shipping_Method::getItem(null,['where'=>"country = '{$country}'"]);
        if($shipping){
            echo json_encode(['status'=>'success', 'shipping'=>json_decode($shipping->cost,true)[0]]);
        } else {
            echo json_encode(['status'=>'failed', 'shipping'=>80]);
        }
    }


    public function update(Array $arr = []){
        if(isset($arr['id'])){
            $authNetDetails = new AuthorizeNetTD();
            $transactions = \Model\Transaction::getList(['where'=>"order_id = {$arr['id']} AND ref_tran_id IS NOT NULL AND ref_tran_id != 0"]);
            foreach($transactions AS &$localTransaction){
                $transaction = $authNetDetails->getTransactionDetails($localTransaction->ref_tran_id);
                $localTransaction->transaction = $trans = $transaction->xml;
            }
            $this->_viewData->transactions = $transactions;

        }
        parent::update($arr);
    }

    public function create_order(Array $arr = []){
        update($arr);
    }

    public function charge_post(){
        $order = \Model\Order::getItem($_POST['orderID']);
        $charge = $_POST['charge'];
        if($order && (isset($charge) && is_numeric($charge) && $charge != 0)){
            $transaction = new AuthorizeNetAIM();
            $transaction->setFields([
                'amount' => $charge,
                'card_num' => $order->cc_number,
                'exp_date' => $order->cc_expiration_month . '/' . $order->cc_expiration_year,
                'first_name' => $order->bill_first_name,
                'last_name' => $order->bill_last_name,
                'address' => $order->bill_address,
                'city' => $order->bill_city,
                'state' => $order->bill_state,
                'country' => $order->bill_country,
                'zip' => $order->bill_zip,
                'email' => $order->email,
                'card_code' => $order->cc_ccv,
                'ship_to_address' => $order->ship_address,
                'ship_to_city' => $order->ship_city,
                'ship_to_country' => $order->ship_country,
                'ship_to_first_name' => $order->ship_first_name,
                'ship_to_last_name' => $order->ship_last_name,
                'ship_to_state' => $order->ship_state,
                'ship_to_zip' => $order->ship_zip,
                'phone' => $order->phone
            ]);
            $transaction->setCustomFields(['x_invoice_num' => $order->id]);
            $response = $transaction->authorizeAndCapture();
            if($response->held || $response->approved){
                foreach($order->calculate_total()['updates'] as $field => $value){
                    $order->$field = $value;
                }
                $order->total = $order->total + $_POST['charge'];
                $order->save();
                $html = '';
                foreach ($order->getOrderProducts() as $orderProduct){
                    $product = \Model\Product::getItem($orderProduct->product_id);
                    $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
                    if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
                    $name = $product->name;
                    $price = $product->price;
                    $html .=
                        '<tr style="height:75.0pt">
						<td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
						</td>
						<td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
						</td>
					</tr>';
                }
                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => $order->id,
                    'DATE' => date('Y-m-d h:i:s'),
                    'DATED' => date('l, F d, Y'),
                    'DATET' => date('h:i A'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGNAME' => 'Shipping',
                    'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];
                $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-confirmation-update');
                try {
                    $emailResp = $email->send();
                } catch(Mandrill_Error $e){
                    $n = new \Notification\ErrorHandler('email was not sent');
                    $_SESSION['notification'] = serialize($n);
                }

                $authTransaction = new AuthorizeNetTD();
                $localTransaction = new \Model\Transaction();
                $localTransaction->order_id = $order->id;
                $localTransaction->ref_tran_id = $response->transaction_id;
                $localTransaction->amount = $response->amount;
                $localTransaction->avs_result_code = $response->avs_response;
                $localTransaction->cvv_result_code = $response->cavv_response;
                $localTransaction->type = "AUTH_CAPTURE";
                $localTransaction->save();
                $n = new \Notification\MessageHandler('Successful Charge');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($response->response_reason_text);
                $_SESSION["notification"] = serialize($n);
            }
            $this->toJson(['status'=>true,'transaction'=>$response,'email'=>isset($emailResp)?$emailResp:'']);
        }
    }

    public function void(){
        ini_set('memory_limit','-1');
        $authTransaction = new AuthorizeNetAIM();
        $response = $authTransaction->void($_POST['authId']);
        $resp = ['status'=>false];
        if($response->approved){
            $transaction = \Model\Transaction::getItem(null,['where'=>"ref_tran_id = {$_POST['authId']}"]);
            $order = \Model\Order::getItem($transaction->order_id);
            if($transaction->type == 'REFUND'){
//                $order->total += $transaction->amount;
            } else if($transaction->type == 'AUTH_CAPTURE'){
//                $order->total -= $transaction->amount;
            }
            $order->save();
            $resp['status'] = true;
            $n = new \Notification\MessageHandler('Transaction void/cancel Success');
            $_SESSION["notification"] = serialize($n);
        } else {
            $n = new \Notification\ErrorHandler($response->response_reason_text);
            $_SESSION["notification"] = serialize($n);
        }
        $this->toJson($resp);
    }

    public function mark_ready_post(){
        $response = ['status'=>false];
        $order = \Model\Order::getItem($_POST['orderId']);
        if($order){
            $order->fulfillment_status = \Model\Order::$fulfillment_status[3];
            if($order->save()){
                $response['status'] = true;
            }
        }
        $this->toJson($response);
    }

    public function notifyMissingNumbers(Array $arr = []){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        $emailSent = 0;
        $emailFailed = [];
        foreach (\Model\Order::getList(['where'=>"campaign_id = 4
        AND ship_country NOT IN ('US','United States')
        AND (phone = '' OR phone IS NULL)
        AND status NOT IN ('Declined', 'Canceled', 'Refunded',
                             'Incomplete PayPal', 'Archived',
                             'Incomplete Amazon', 'Banned', 'Limit Reached') "]) AS $order){

                $html = '';
                foreach ($order->getOrderProducts() as $orderProduct){
                    $product = \Model\Product::getItem($orderProduct->product_id);
                    $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
                    if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
                    $name = $product->name;
                    $price = $product->price;
                    $html .=
                        '<tr style="height:75.0pt">
						<td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
						</td>
						<td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
						</td>
					</tr>';
                }
                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => $order->id,
                    'DATE' => date('Y-m-d h:i:s'),
                    'DATED' => date('l, F d, Y'),
                    'DATET' => date('h:i A'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGNAME' => 'Shipping',
                    'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];
                $email->setSubject('')->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-numbers');
                try {
                    $emailResp = $email->send();
                    $emailSent ++;
                } catch(Mandrill_Error $e){
                    $emailFailed [] = $order->id.' : '.$order->email;
                }

//                break;
            }
            var_dump('Emails Sent Successfully: '.$emailSent);
            var_dump(count($emailFailed).' Emails failed to send');
            echo(implode('<br />',$emailFailed));
    }

    /*public function fixMisCharge(Array $arr = []){
        exit;
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        foreach (\Model\Order::getList(['where'=>"campaign_id = 4 ".
        "AND id IN(215303,214247,214773,215610,216312,218188,216961,217894,215778,217290,215693,217251,214660,215686,215289,216274,216514,214640,214195,218149,218458,218401,215344,215323,216707,214882,216444,216904,216172,216934,218519,216752,215584,216973,216297,215609,216290,217185,215996,214514,216322,215527,215222,216338,215254,217148,216390,216758,216646,216382,215819,216880,216879,215769,215864,216731,215589,215291,216448,214581,215313,214447,217243,217714,216697,216920,217965,217085,214568,217048,216724,216720,214343,216830,216115,218347,214687,217952,214511,214457,217023,218284,216176,215958,218131,218400,217024,216970,214230,218419,214241,215264,214947,214667,216240,215924,214419,216945,215721,215875,214573,216953,214870,214351,214654,215613,214699,214732,217677,216355,214499,218270,215406,217821,216755,215325,214172,216866,215228,215766,216942,215373,216629,214747,218457,217851,214314,214123,214234,217180,214250,214487,216849,218105,214174,217289,214137,214306,218465,217224,217006,218162,216941,216306,215432,215739,216301,216028,217113,215998,214185,214532,215975,216374,216573,216821,214198,218440,218007,217178,214888,217938,216743,216048,218360,214245,216396,218100,216730,217022,217115,215116,215703,216852,216300,214740,217144,216793,215709,216699,214430,214322,214525,215279,218526,217221,218049,216957,215672,215729,216788,214365,215873,216160,214729,217059,217942,218448,216475,218356,217910,214409,214491,214128,218319,215335,216356,216762,216681,215705,216549,217228,217742,215586,215369,215624,218366,216818,215402,218321,216347,215869,218389,216068,215692,218213,216075,214188,216789,214283,214951,218396,216716,218378,218354,216700,214799,214480,216001,214572,215771,216352,216714,214371,214183,215870,216749,214769,218462,215490,216997,218517,218417,218088,216344,217072,214493,218450,215925,214571,218403,215370,214200) ".
        "AND status NOT IN ('Declined', 'Canceled', 'Refunded',
                             'Incomplete PayPal', 'Archived',
                             'Incomplete Amazon', 'Banned', 'Limit Reached') "]) AS $order){
//        foreach (\Model\Order::getList(['where'=>"id = 11"]) AS $order){
            $bd = $order->calculate_total();
            $difference = array_sum($bd['charges']) - $order->total;
            if($difference > 0){
                $_order= ['charged'=>false,'emailed'=>false];
                $transaction = new AuthorizeNetAIM();
                $transaction->setFields([
                    'amount' => $difference,
                    'card_num' => $order->cc_number,
                    'exp_date' => $order->cc_expiration_month . '/' . $order->cc_expiration_year,
                    'first_name' => $order->bill_first_name,
                    'last_name' => $order->bill_last_name,
                    'address' => $order->bill_address,
                    'city' => $order->bill_city,
                    'state' => $order->bill_state,
                    'country' => $order->bill_country,
                    'zip' => $order->bill_zip,
                    'email' => $order->email,
                    'card_code' => $order->cc_ccv,
                    'ship_to_address' => $order->ship_address,
                    'ship_to_city' => $order->ship_city,
                    'ship_to_country' => $order->ship_country,
                    'ship_to_first_name' => $order->ship_first_name,
                    'ship_to_last_name' => $order->ship_last_name,
                    'ship_to_state' => $order->ship_state,
                    'ship_to_zip' => $order->ship_zip,
                    'phone' => $order->phone
                ]);
//            $transaction->setCustomFields(['x_invoice_num' => $order->id]);
                $response = $transaction->authorizeAndCapture();
                if($response->held || $response->approved){
                    $charges++;
                    $_order['charged']=true;
                    foreach($bd['updates'] as $field => $value){
                        $order->$field = $value;
                    }
                    $order->total = $order->total + $difference;
                    $order->fulfillment_status = 'Ready';
                    $order->save();
                    $html = '';
                    foreach ($order->getOrderProducts() as $orderProduct){
                        $product = \Model\Product::getItem($orderProduct->product_id);
                        $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
                        if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
                        $name = $product->name;
                        $price = $product->price;
                        $html .=
                            '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
                                </td>
                                <td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
                                
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                                
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
                                </td>
                            </tr>';
                    }
                    $email = new \Email\MailMaster();
                    $mergeFields = [
                        'ORDER_NUMBER' => $order->id,
                        'DATE' => date('Y-m-d h:i:s'),
                        'DATED' => date('l, F d, Y'),
                        'DATET' => date('h:i A'),
                        'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                        'SHIPPING' => $order->getShippingAddr(),
                        'BILLING' => $order->getBillingAddr(),
                        'ITEMS' => $html,
                        'SUBTOTAL' => number_format($order->subtotal, 2),
                        'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                        'SHIPPINGNAME' => 'Shipping',
                        'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
                        'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                        'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
                        'TOTAL' => number_format($order->total, 2)
                    ];
                    $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-confirmation-update');
                    try {
                        $emailResp = $email->send();
                        $_order['emailed'] = true;
                        $emails++;
                    } catch(Mandrill_Error $e){

                    }

                    $localTransaction = new \Model\Transaction();
                    $localTransaction->order_id = $order->id;
                    $localTransaction->ref_tran_id = $response->transaction_id;
                    $localTransaction->amount = $response->amount;
                    $localTransaction->avs_result_code = $response->avs_response;
                    $localTransaction->cvv_result_code = $response->cavv_response;
                    $localTransaction->type = "AUTH_CAPTURE";
                    $localTransaction->save();
                }
            } else if($difference < 0) {
                $_order= ['refunded'=>false,'emailed'=>false];
                $refAmt = abs($difference);
                $refTransaction = \Model\Transaction::getItem(null,['where'=>"order_id = $order->id AND amount::FLOAT > $refAmt AND type = 'AUTH_CAPTURE'",'orderBy'=>"id ASC"]);
                $authTransaction = new AuthorizeNetAIM();
                $response = $authTransaction->credit($refTransaction->ref_tran_id,$refAmt,substr($order->cc_number,-4));
                if($response->approved){
                    $_order['refunded']=true;
                    $refunds++;
                    foreach($order->calculate_total()['updates'] as $field => $value){
                        $order->$field = $value;
                    }

                    if( in_array($order->ship_country,['US','United States']) === false && $order->phone != '' && $order->phone != null ){
                        $order->fulfillment_status = 'Ready';
                    } else if(in_array($order->ship_country,['US','United States']) ){
                        $order->fulfillment_status = 'Ready';
                    }
                    $order->total = $order->total - $refAmt;
                    $order->save();
                    $html = '';
                    foreach ($order->getOrderProducts() as $orderProduct){
                        $product = \Model\Product::getItem($orderProduct->product_id);
                        $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
                        if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
                        $name = $product->name;
                        $price = $product->price;
                        $html .=
                            '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
                                </td>
                                <td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
                                
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                                
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
                                </td>
                            </tr>';
                    }
                    $email = new \Email\MailMaster();
                    $mergeFields = [
                        'ORDER_NUMBER' => $order->id,
                        'DATE' => date('Y-m-d h:i:s'),
                        'DATED' => date('l, F d, Y'),
                        'DATET' => date('h:i A'),
                        'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                        'SHIPPING' => $order->getShippingAddr(),
                        'BILLING' => $order->getBillingAddr(),
                        'ITEMS' => $html,
                        'SUBTOTAL' => number_format($order->subtotal, 2),
                        'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                        'SHIPPINGNAME' => 'Shipping',
                        'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
                        'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                        'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
                        'TOTAL' => number_format($order->total, 2)
                    ];
                    $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-confirmation-update');
                    try {
//                $emailResp = $email->send();
                    } catch(Mandrill_Error $e){
                        $n = new \Notification\ErrorHandler('email was not sent');
                        $_SESSION['notification'] = serialize($n);
                    }
                    $localTransaction = new \Model\Transaction();
                    $localTransaction->order_id = $order->id;
                    $localTransaction->ref_tran_id = $response->transaction_id;
                    $localTransaction->amount = $response->amount;
                    $localTransaction->avs_result_code = $response->avs_response;
                    $localTransaction->cvv_result_code = $response->cavv_response;
                    $localTransaction->type = "REFUND";
                    $localTransaction->save();
                }
                $orders[$order->id]=$_order;
            } else {
                echo "<br/>";
                var_dump($order->id.' '.$difference);
                echo "<br/>";
            }
        }
        echo "Successful Charges: $charges/".count($orders);
        echo "<br />";
        echo "Successful Refunds: $refunds/".count($orders);
        echo "<br />";
        echo "Successful Emails:  $emails/".count($orders);
        var_dump($orders);
    }*/

    public function send_dupe_emails(Array $arr = []){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        $emails = [];
        foreach (\Model\Order::getList(['where'=>"campaign_id = 4 ".
            "AND id IN(217317,217365,217375,215803,216176,217350,217326,217343,217362,215262,217323,217366,216059,216344,217616,217309,216156,214534,216190,217418,217554,217649,216867,217505,217484,217574,217414,217451,217532,217539,217463,218398,215774,217559,217570,217619,217444,217618,217470,218068,217492,217512,217526,217465,217367,217355,217511,217537,217552,217627,217336,214481,217494,217487,217595,214552,214916,216203,217431,217499,217535,217612,217473,217617,217583,217528,214630) ".
            "AND status NOT IN ('Declined', 'Canceled', 'Refunded',
                             'Incomplete PayPal', 'Archived',
                             'Incomplete Amazon', 'Banned', 'Limit Reached') "]) AS $order){
            $emails[strtolower($order->email)] = $order;
        }
        foreach ($emails as $email => $_order){
            if($this->sendDupeEmail($_order)){
                echo($email.' Sent '."<br />");
            }
        }
    }

    public function send_balance_emails(Array $arr = []){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        foreach (\Model\Order::getList(['where'=>"campaign_id = 9 ".
            "AND id IN(370346) "]) AS $order){
            if($this->sendBalanceEmail($order)){
                echo($order->email.' Sent '."<br />");
            }
        }
    }

    public function report(Array $arr = []){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=skusToUpdate.csv");

        function outputCSV($data) {
            $output = fopen("php://output", "w");
            foreach ($data as $row)
                fputcsv($output, $row); // here you can change delimiter/enclosure
            fclose($output);
        }

        $output = [['Order Number','Fulfillment Status','Status','Ship Country','Bill Country','Phone Number','Price Difference']];

        foreach (\Model\Order::getList(['where'=>"(campaign_id = 4 OR campaign_id IS NULL)".
            "AND status NOT IN ('Declined', 'Canceled', 'Refunded',
                             'Incomplete PayPal', 'Archived',
                             'Incomplete Amazon', 'Banned', 'Limit Reached')",
            'orderBy'=>"fulfillment_status ASC, phone ASC, id ASC"]) AS $order) {

            $bd = $order->calculate_total();
            $difference = array_sum($bd['charges']) - $order->total;
            $output[] = [$order->id,$order->fulfillment_status,$order->status,$order->ship_country,$order->bill_country,$order->phone,'$'.number_format($difference,2)];

        }

        outputCSV($output);

    }

    public function refund(){
        $authTransaction = new AuthorizeNetAIM();
//        $transaction = \Model\Transaction::getItem(null,['where'=>"ref_tran_id = '{$_POST['authId']}'"]);
        $order = \Model\Order::getItem($_POST['orderId']);
        $amount = $_POST['amount'];
        var_dump($_POST['authId']);
        if($order->cc_number == 0){
            $authNetDetails = new AuthorizeNetTD();
            $transaction = $authNetDetails->getTransactionDetails($_POST['authId']);
            $trans = $transaction->xml;
            $order->cc_number = $trans->transaction->payment->creditCard->cardNumber;
        }

        $response = $authTransaction->credit($_POST['authId'],$amount,substr($order->cc_number,-4));
        $resp = ['status'=>false];
        if($response->approved){
//            foreach($order->calculate_total()['updates'] as $field => $value){
//                $order->$field = $value;
//            }
//            $order->total = $order->total - $_POST['amount'];
            if($order->total - $_POST['amount'] == 0){
                $order->refunded = 1;
            }
            $order->save();
            $html = '';
//            foreach ($order->getOrderProducts() as $orderProduct){
//                $product = \Model\Product::getItem($orderProduct->product_id);
//                $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
//                if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
//                $name = $product->name;
//                $price = $product->price;
//                $html .=
//                    '<tr style="height:75.0pt">
//						<td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
//                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
//						</td>
//						<td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
//                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
//
//                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
//
//                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
//						</td>
//					</tr>';
//            }
//            $email = new \Email\MailMaster();
//            $mergeFields = [
//                'ORDER_NUMBER' => $order->id,
//                'DATE' => date('Y-m-d h:i:s'),
//                'DATED' => date('l, F d, Y'),
//                'DATET' => date('h:i A'),
//                'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
//                'SHIPPING' => $order->getShippingAddr(),
//                'BILLING' => $order->getBillingAddr(),
//                'ITEMS' => $html,
//                'SUBTOTAL' => number_format($order->subtotal, 2),
//                'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
//                'SHIPPINGNAME' => 'Shipping',
//                'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
//                'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
//                'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
//                'TOTAL' => number_format($order->total, 2)
//            ];
//            $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-confirmation-update');
//            try {
////                $emailResp = $email->send();
//            } catch(Mandrill_Error $e){
//                $n = new \Notification\ErrorHandler('email was not sent');
//                $_SESSION['notification'] = serialize($n);
//            }

            $localTransaction = new \Model\Transaction();
            $localTransaction->order_id = $order->id;
            $localTransaction->ref_tran_id = $response->transaction_id;
            $localTransaction->amount = $response->amount;
            $localTransaction->avs_result_code = $response->avs_response;
            $localTransaction->cvv_result_code = $response->cavv_response;
            $localTransaction->type = "REFUND";
            $localTransaction->save();
            $resp['status'] = true;
            $n = new \Notification\MessageHandler('Successful Refund');
            $_SESSION["notification"] = serialize($n);
        } else {
            $n = new \Notification\ErrorHandler($response->response_reason_text);
            $_SESSION["notification"] = serialize($n);
        }
        $this->toJson($resp);
    }

    public function addTransaction_post(){
        $adj_price = $_POST['adj_price'];
        $tran_id = $_POST['tran_id'];
        $order_id = $_POST['order_id'];
        $order = \Model\Order::getItem($order_id);
        $bd = $order->calculate_total();
//        foreach($bd['updates'] as $field => $value){
//            $order->$field = $value;
//        }
        $order->save();
        $resp = ['status'=>false];
        if($order != null){
            $orig = $order->total;
            $authNetDetails = new AuthorizeNetTD();
            $authTransaction = $authNetDetails->getTransactionDetails($tran_id);
            $message = (array)$authTransaction->xml->messages->resultCode;
            if($message[0] == 'Ok'){
                $resp = ['status'=>true];
                $localTransaction = new \Model\Transaction();
                $localTransaction->order_id = $order->id;
                $localTransaction->import = 1;
                $notes = [];
                $notes['Card Num'] = $authTransaction->xml->transaction->payment->creditCard->cardNumber;
                $notes['Card Num'] = (array)$notes['Card Num'];
                $notes['Card Num'] = $notes['Card Num'][0];
                $notes['Card Type'] = $authTransaction->xml->transaction->payment->creditCard->cardType;
                $notes['Card Type'] = (array)$notes['Card Type'];
                $notes['Card Type'] = $notes['Card Type'][0];
                $localTransaction->notes = json_encode($notes);
                $localTransaction->ref_tran_id = $tran_id;
                $localTransaction->amount = $authTransaction->xml->transaction->authAmount;
                $localTransaction->avs_result_code = $authTransaction->xml->transaction->AVSResponse;
                switch($authTransaction->xml->transaction->transactionType){
                    case 'authCaptureTransaction':
                        $localTransaction->type = "AUTH_CAPTURE";
                        if($adj_price){
                            $order->total = floatval($order->total) + floatval($localTransaction->amount);
                        }
                        break;
                    case 'refundTransaction':
                        $localTransaction->type = "REFUND";
                        if($adj_price){
                            $order->total = floatval($order->total) - floatval($localTransaction->amount);
                        }
                        break;
                }
                $order->transid = $order->trans_id = $_POST['tran_id'];
                $order->status = 'Updated';
                if($order->save()){
                    if($localTransaction->save()){
                        global $emagid;
                        $db = $emagid->getDb();
                        $sql = "UPDATE _tmp_authnet SET kaws_trans_id = {$localTransaction->id} WHERE transid = {$_POST['tran_id']}";
                        $db->execute($sql);
                        $n = new \Notification\MessageHandler('Transaction Added <br /> Order Updated');
                        $_SESSION["notification"] = serialize($n);
                    } else {
                        $n = new \Notification\ErrorHandler($localTransaction->errors);
                        $order->total = $orig;
                        $order->save();
                        $_SESSION["notification"] = serialize($n);
                    }
                } else {
                    $n = new \Notification\ErrorHandler($order->errors);
                    $_SESSION["notification"] = serialize($n);
                }
            } else {
                $message = (array)$authTransaction->xml->messages->message->text;
                $n = new \Notification\ErrorHandler($message[0]);
                $_SESSION["notification"] = serialize($n);
            }
        } else {
            $n = new \Notification\ErrorHandler('Invalid Order');
            $_SESSION["notification"] = serialize($n);
        }
        $this->toJson($resp);


    }

    public function clearTransaction_post(){
        $resp = ['status'=>false];
        $order_id = $_POST['order_id'];
        $order = \Model\Order::getItem($order_id);
        $order->trans_id = NULL;
        $order->status = 'Canceled';
        if($order->save()){
            $sql = "UPDATE transaction SET active = 0 WHERE order_id = $order_id";
            global $emagid;
            $db = $emagid->getDb();
            $db->execute($sql);
            $resp['status'] = true;
        }
        $this->toJson($resp);
    }

    public function deleteTransaction_post(){
        if(isset($_POST['tran_id']) && $_POST['tran_id'] != '') {
            $transaction = \Model\Transaction::getItem($_POST['tran_id']);
            $orderId = $transaction->order_id;
            if($_POST['adj_price']){
                $order = \Model\Order::getItem($orderId);
                if($transaction->type == 'AUTH_CAPTURE'){
                    $order->total = $order->total - $transaction->amount;
                }
                else {
                    $order->total = $order->total + $transaction->amount;
                }
                $order->save();
            }
            \Model\Transaction::delete($transaction->id);
            $n = new \Notification\MessageHandler('Transaction Removed <br /> Order Updated');
            $_SESSION["notification"] = serialize($n);
        }
        $this->toJson(['status'=>true]);
    }

    //Admin checkout flow -- because we're crazy
    public function create_order_post(){
        //Set Subtotal
        $subtotal = 0;
        for($i = 0; $i < count($_POST['product_id']); $i++){
            if($_POST['product_id'][$i] > 0 && $_POST['custom_price'][$i] == ''){
                $product = \Model\Product::getItem($_POST['product_id'][$i]);
                $subtotal += $product->getPrice($_POST['color'][$i]);
            } else if($_POST['custom_price'] > 0){
                $subtotal += $_POST['custom_price'][$i];
            }
        }
        $_POST['subtotal'] = $subtotal;
        $calculatedTotal = $subtotal;

        if(isset($_POST['gift_card']) && ($gc = \Model\Gift_Card::validateGiftCard($_POST['gift_card']))){
            if($gc->getRemainingAmount() > $subtotal){
                $_POST['gift_card_amount'] = $gift_card_amount = $subtotal;
                $_POST['payment_method'] = 5;
                $calculatedTotal = 0;
            } else {
                $_POST['gift_card_amount'] = $gift_card_amount = round($gc->getRemainingAmount(),2);
                $calculatedTotal -= $gift_card_amount;
            }
        }
        //Set Taxes
//        if((isset($_POST['tax-free']) && $_POST['tax-free']) || $_POST['ship_state'] != 'NY'){
        if((isset($_POST['tax-free']) && $_POST['tax-free'])){
            $_POST['tax_rate'] = 0;
            $_POST['tax'] = $tax = 0;
        } else {
            $_POST['tax_rate'] = 8.875;
            $_POST['tax'] = $tax = round($calculatedTotal * .08875,2);
        }
        $calculatedTotal += $_POST['tax'];

        //Set shipping cost
        if($_POST['ship_country'] == 'United States'){
            $shipping_cost = 0;
        } else if($shipping = \Model\Shipping_Method::getItem(null,['where'=>"country = '{$_POST['ship_country']}'"])){
            $shipping_cost = json_decode($shipping->cost,true)[0];
        } else {
            $shipping_cost = 80;
        }
        $calculatedTotal += $shipping_cost;
        $_POST['shipping_cost'] = round($shipping_cost,2);

        //Set discount -- N/A
        $discount = 0;
        $calculatedTotal -= $discount;
//        $coupon = \Model\Coupon::getItem(null,['where'=>"code = '{$_POST['coupon_code']}'"]);
//        $_POST['coupon_type'] = $coupon->discount_type;
//        $_POST['coupon_amount'] = $coupon->discount_amount;
//        $discount = $coupon->applyCoupon($subtotal);

        //Set guest_id
        $_POST['guest_id'] = session_id();

        $_POST['status'] = 'New';
        $_POST['ref_num'] = generateToken();

        //Set total
        $_POST['total'] = $calculatedTotal;

        $_POST['insert_time'] = date('Y-m-d H:i:s.u');
        $order = \Model\Order::loadFromPost();
        $order->product_id=$order->color=$order->size=$order->misc_name=$order->custom_price='';

        if($order->save()){
            if(isset($_GET['inv']) && $_GET['inv'] && ($invoice = \Model\Invoice::getByRef($_GET['inv']))){
                $invoice->order_id = $order->id;
                $invoice->save();
            }
            $html = '';
            for($i = 0; $i < count($_POST['product_id']); $i++){
                $orderProduct = new \Model\Order_Product();
                $orderProduct->order_id = $order->id;
                $orderProduct->product_id = $_POST['product_id'][$i];
                $orderProduct->quantity = 1; //add quantity
                if($_POST['product_id'][$i] > 0){ //id is product
                    $product = \Model\Product::getItem($_POST['product_id'][$i]);
                    $product->total_sales += 1;
                    $product->save();
                    $unit_price = $_POST['custom_price'][$i] ? : $product->price($_POST['color'][$i]);
                    $product_image = $product->featuredImage(null,$_POST['color'][$i]);
                    $product_name = $product->name;
                    $product_price = $unit_price * $orderProduct->quantity;
                    $product_color = \Model\Color::getItem($_POST['color'][$i])->name;
                    $product_size = \Model\Size::getItem($_POST['size'][$i]);

                    $orderProduct->unit_price = $unit_price;
                    $orderProduct->details = json_encode(['color'=>$_POST['color'][$i], 'size'=>$_POST['size'][$i]]);
                    $orderProduct->save();
                } else { //id is miscellaneous charge
                    $unit_price = $_POST['custom_price'][$i];
                    $product_image = '';
                    $product_name = $_POST['misc_name'][$i];
                    $product_price = $unit_price * $orderProduct->quantity;
                    $product_color = '';
                    $product_size = new stdClass(); $product_size->us_size = ''; $product_size->eur_size = '';

                    $orderProduct->unit_price = $unit_price;
                    $orderProduct->details = json_encode(['misc_name'=>$_POST['misc_name'][$i]]);
                }
                $orderProduct->save();

                $html .=
                    '<tr style="height:75.0pt">
                            <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p class="MsoNormal"><img width="100px!important;" src="https://djinyc.com'.UPLOAD_URL.'products/'.$product_image.'"></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product_name.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($product_price,2).'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product_color.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">US: '.$product_size->us_size.'<br>EU: '.$product_size->eur_size.'</span></p>
                            </td>
                        </tr>';
            }

            if($order->payment_method == 1) {
                $expiry = str_pad($order->cc_expiration_month, 2, 0) . substr($order->cc_expiration_year, -2);
                $firstData = new Payment\FirstData(PAYEEZY_ID, PAYEEZY_PASSWORD);
                $firstData->setAmount($order->total);
                $firstData->setCreditCardNumber($order->cc_number);
                $firstData->setCreditCardName($order->card_name);
                $firstData->setCreditCardType($_POST['cc_type']);
                $firstData->setCreditCardVerification($order->ccv);
                $firstData->setCreditCardExpiration($expiry);
                $firstData->setCurrency('USD');
                $firstData->setReferenceNumber($order->id);
                if ($order->user_id) {
                    $firstData->setCustomerReferenceNumber($order->user_id);
                }
                $firstData->process();
                if ($firstData->isSuccess()) {
                    $order->status = \Model\Order::$status[1];
                    $order->save();

                    $email = new \Email\MailMaster();
                    $mergeFields = [
                        'ORDER_NUMBER' => $order->id,
                        'DATE' => date('Y-m-d h:i:s'),
                        'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                        'SHIPPING' => $order->getShippingAddr(),
                        'BILLING' => $order->getBillingAddr(),
                        'ITEMS' => $html,
                        'SUBTOTAL' => number_format($order->subtotal, 2),
                        'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                        'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                        'TAXFEE' => number_format($order->tax, 2),
                        'TOTAL' => number_format($order->total, 2)
                    ];
                    $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();

                    if($order->gift_card && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card)) && $order->gift_card_amount){
                        $giftCard->removeAmount($order->gift_card_amount);
                    }
                    redirect('/admin/orders/update/' . $order->id);
                } else {
                    $order->status = \Model\Order::$status[2];
                    $order->error_message = $firstData->getErrorMessage();
                    $order->save();
                    $n = new \Notification\ErrorHandler($firstData->getErrorMessage());
                    $_SESSION["notification"] = serialize($n);
                    redirect('/admin/orders/create_order');
                }
            }
            else if($order->payment_method == 3){
                $order->status = \Model\Order::$status[1];
                $order->save();

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => $order->id,
                    'DATE' => date('Y-m-d h:i:s'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format($order->tax, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];

                if($order->gift_card && $order->gift_card_amount && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card))){
                    $giftCard->removeAmount($order->gift_card_amount);
                }
                
                $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();
                redirect('/admin/orders/update/' . $order->id);
            }
            else if($order->payment_method == 5){
                $order->status = \Model\Order::$status[1];
                $order->save();

                if($order->gift_card && $order->gift_card_amount && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card))){
                    $giftCard->removeAmount($order->gift_card_amount);
                }

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => $order->id,
                    'DATE' => date('Y-m-d h:i:s'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format($order->tax, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];
                $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();
                redirect('/admin/orders/update/' . $order->id);
            }
            else {
                $order->status = \Model\Order::$status[2];
                $order->save();
                $n = new \Notification\ErrorHandler('Invalid payment method ('.\Model\Order::$payment_method[$order->payment_method].'). Please try again.');
                $_SESSION["notification"] = serialize($n);
                redirect('/admin/orders/create_order');
            }
        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION["notification"] = serialize($n);
            redirect('/admin/orders/create_orders');
        }
    }

    public function sync()
    {
        $updated = false;
        $ids = ['ai'=>[],'o'=>[],'p'=>[]];
        foreach(\Model\Order::getList(['where'=>"ship_id IS NOT NULL AND ship_id != 0 AND (tracking_number = '' OR tracking_number IS NULL) AND status NOT IN ('Declined', 'Canceled', 'Incomplete PayPal')"]) as $order){
            $ss = new ShipStation();
            $ss->setSsApiKey(SHIPSTATION_KEY);
            $ss->setSsApiSecret(SHIPSTATION_SECRET);
            $ss_order = $ss->getShipments(['orderId'=>$order->ship_id]);
            if(count($ss_order->shipments)){
                $order->tracking_number = $ss_order->shipments[0]->trackingNumber;
                $order->shipping_charge = $ss_order->shipments[0]->shipmentCost;
                $serial_search = $ss->getOrder($ss_order->shipments[0]->orderId);
                $serial_search = $serial_search->internalNotes;
                $serial_search = trim($serial_search);
                $order->status = 'shipped';
                if($order->save()){
                    $receive_item = \Model\Receive_Item::getItem(null,['where'=>"lower(sku) LIKE lower('$serial_search')"]);
                    if($receive_item){
                        $receive_item->status = \Model\Inventory_Status::getSoldStatus();
                        $receive_item->save();
                    }
                    $ids['o'][] = $order->id;
                    foreach (\Model\Order_Product::getItem(null,['where'=>"order_id = $order->id"]) as $op){
                        if($op->product_id == $receive_item->product_item && !$updated){
                            $updated = true;
                            $receive_item->order_product_id = $op->id;
                            $receive_item->save();
                        }
                        \Model\Actual_Inventory::recalculate($op->product_id);
                    }
                }
            }
        }
        $this->toJson(['status'=>true,'count'=>$updated,'ids'=>$ids,'ssorder'=>$ss_order]);
    }

    public function update_post()
    {
        ini_set('display_errors',1);
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {

            $order = \Model\Order::getItem($_POST['id']);

            if(isset($_POST['difference']) && $_POST['difference'] != '') {
                $order->difference = $_POST['difference'];
            }

            if(isset($_POST['email']) && $_POST['email'] != ''){
                $order->email = $_POST['email'];
                $order->save();
                if($order->user_id != 0){
                    $user = \Model\User::getItem($order->user_id);
                    if($user != null){
                        $user->email = $_POST['email'];
                        $user->save();
                    }
                }
            }

            /*if ($order->status != $_POST['status'] && $_POST['status'] == \Model\Order::$status[3] && trim($_POST['tracking_number']) != ""){
                $this->sendShippedEmail($order, $_POST['tracking_number']);
            }*/

            if (isset($_POST['status'])) {
                $order->status = $_POST['status'];
                if ($_POST['status'] !== $_POST['old_status']) {
                    $log_status = new \Model\Log_Status();
                    $log_status->order_id = $_POST['id'];
                    $log_status->status = $_POST['status'];
                    $log_status->admin_id = \Emagid\Core\Membership::userId();
                    $log_status->save();
                }
            }

            if (isset($_POST['fulfillment_status'])) {
                $order->fulfillment_status = $_POST['fulfillment_status'];
                if ($_POST['status'] !== $_POST['old_status']) {
                    $log_status = new \Model\Log_Status();
                    $log_status->order_id = $_POST['id'];
                    $log_status->status = $_POST['fulfillment_status'];
                    $log_status->admin_id = \Emagid\Core\Membership::userId();
                    $log_status->save();
                }
            }

            if(isset($_POST['bill_state']) && isset($_POST['ship_state']) ){
                if($_POST['bill_state'] == '' && $_POST['ship_state'] != ''){
                    $_POST['bill_state'] = $_POST['ship_state'];
                } else if($_POST['ship_state'] == '' && $_POST['bill_state'] != ''){
                    $_POST['ship_state'] = $_POST['bill_state'];
                }
            }

            if(isset($_POST['fulfilment'])){
                foreach ($_POST['fulfilment'] as $orderProductId=>$receiveItemId){
                    $orderProduct = \Model\Order_Product::getItem($orderProductId);
                    $orderProduct->receive_item_id = $receiveItemId;
                    $orderProduct->save();
                }
            }

            $order->tracking_number = $_POST['tracking_number'];
            if (isset($_POST['cc_number'])) {
                if ('****' . substr($order->cc_number, -4) != $_POST['cc_number']) {
                    $order->cc_number = $_POST['cc_number'];
                }
            }
            if (isset($_POST['cc_expiration_month'])) {
                $order->cc_expiration_month = $_POST['cc_expiration_month'];
            }
            if (isset($_POST['cc_expiration_year'])) {
                $order->cc_expiration_year = $_POST['cc_expiration_year'];
            }

            if (isset($_POST['cc_ccv'])) {
                $order->cc_ccv = $_POST['cc_ccv'];
            }


            if (isset($_POST['payment_method'])) {
                $order->payment_method = $_POST['payment_method'];
            }

            $order->bill_first_name  = $_POST['bill_first_name'];
            $order->bill_last_name  = $_POST['bill_last_name'];
            $order->bill_address  = $_POST['bill_address'];
            $order->bill_address2  = $_POST['bill_address2'];
            if (isset($_POST['bill_state'])) {
                $order->bill_state  = $_POST['bill_state'];
            }
            if (isset($_POST['bill_country'])) {
                $order->bill_country  = $_POST['bill_country'];
            }
            $order->bill_city  = $_POST['bill_city'];
            $order->bill_zip  = $_POST['bill_zip'];

            $order->ship_first_name = $_POST['ship_first_name'];
            $order->ship_last_name = $_POST['ship_last_name'];
            $order->ship_address = $_POST['ship_address'];
            $order->ship_address2 = $_POST['ship_address2'];
            if (isset($_POST['ship_state'])) {
                $order->ship_state = $_POST['ship_state'];
            }
            if (isset($_POST['ship_country'])) {
                $order->ship_country = $_POST['ship_country'];
            }
            $order->ship_city = $_POST['ship_city'];
            $order->ship_zip = $_POST['ship_zip'];

            //$order->note = $_POST['note'];
            if(isset($_POST['note']) && $_POST['note'] != ''){
                $note = new \Model\Note_Log();
                $note->order_id = $order->id;
                $note->note = $_POST['note'];
                $note->admin_id = $this->_viewData->logged_admin->id;
                $note->save();
            }


            $order->phone = $_POST['phone'];
            $order->allow_returns = $_POST['allow_returns']?:0;
            $label = \Model\Labels::getItem(null,['where'=>"order_id = $order->id",'orderBy'=>"id DESC"]);
            if(isset($_POST['send_return']) && $_POST['send_return'] == 'on'){
                if($label == null || $label->type != 2){
                    $label = $this->generateLabel_post(['weight'=>2,'update'=>true,'id'=>$order->id,'type'=>2]);
                }
                if($label != null){
                    $email = new \Email\MailMaster();
                    $link = 'https://aws.kawsone.com/orders/returnLabel/'.$order->ref_num;
                    $mergeFields = [
                        'NAME'=>$order->billName(),
                        'ORDER_NUMBER'=>$order->id,
                        'LINK'=>$link
                    ];
                    try{
                        $email->setTo(['email'=>$order->email,'name'=>$order->billName(), 'type'=>'to'])
                            ->setMergeTags($mergeFields)
                            ->setSubject('KAWSONE Return Shipping Label')
                            ->setTemplate('kaws-return')
                            ->send();
                    } catch (Mandrill_Error $e){

                    }
                }
            }

            $subtotal = $order->subtotal;
//            if (!is_null($order->coupon_code)) {
//                if ($order->coupon_type == 1) {
//                    $subtotal = $subtotal - $order->coupon_amount;
//                } else if ($order->coupon_type == 2) {
//                    $subtotal = $subtotal * (1 - ($order->coupon_amount / 100));
//                }
//            }

            /*$logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());  
            $orderchange = new \Model\Orderchange();  
            $orderchange->order_id = $_POST['id'];
            $orderchange->user_id =  $logged_admin->id;
            $orderchange->save() ; */


            if (!empty($_POST['mail']) && false) {
                $email = new \Emagid\Email();
                global $emagid;
                $emagid->email->from->email = 'orders@djinyc.com'; //TODO st-dev get email
                $mail_log = new \Model\Mail_Log();
                $mail_log->from_ = "orders@djinyc.com"; //TODO st-dev get email

                if (is_null($order->user_id)) {
                    $email->addTo($order->email);
                    $mail_log->to_ = $order->email;
                } else {
                    $email->addTo(\Model\User::getItem($order->user_id)->email);
                    $mail_log->to_ = \Model\User::getItem($order->user_id)->email;
                }
                $email->addBcc("orders@djinyc.com"); //TODO st-dev get email
                if ($_POST['status'] !== $_POST['old_status']) {
                    $subject_array = Array();
                    foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
                        $product = \Model\Product::getItem($order_product->product_id);
                        $brand = \Model\Brand::getItem($product->brand);
                        $subject_array[] = $brand->name;
                        $subject_array[] = $product->mpn;

                        //$subject_array[] = $order_product->id;
                    }
                    if ($_POST['status'] == "Shipped") {

                        $email->subject('Your order has shipped! ' . implode(" ", $subject_array) . " kawsone Order #" . $order->id);
                        $mail_log->subject = 'Your order has shipped! ' . implode(" ", $subject_array) . "kawsone Order #" . $order->id;
                    } else {
                        $email->subject('Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " kawsone Order #" . $order->id);
                        $mail_log->subject = 'Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " kawsone Order #" . $order->id;
                    }
                }
                $subject = $_POST['subject'];
                $mailtitle = $_POST['mailtitle'];
                $status = $_POST['status'];
                $text = $_POST['mail'];
                $email->subject($subject);
                $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="https://djinyc.com/content/frontend/img/logo.png" alt="DJI"></center>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">' . $mailtitle . '</span></b> </p>
                        </td>
                        <td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</a></span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Client:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:green">' . $order->ship_first_name . ' ' . $order->ship_last_name . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';


                $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%">
                <tbody>
                     <p align="justify">' . $text . '</p>
                </tbody>
                </table>';

                $mail_log->text = $email->body;

                $mail_log->order_id = $order->id;
                $mail_log->save();
                // echo $email->body;
                // echo $email->subject;
//                $email->send();

            }
            if(isset($_POST['tracking_email']) && $_POST['tracking_email'] == 'on' && false){
                $html = '';
                $html .= '<tr>';
                foreach ($order->getOrderProducts() as $array) {
                    $product = \Model\Product::getItem($array->product_id);
                    $color = json_decode($array->variation, true)['color'];
                    $size = json_decode($array->variation, true)['size'];
                    $html .= '
                        <td width="192" style="width: 192px;" valign="top">
                            <table width="192" border="0" cellpadding="0" cellspacing="0" align="center" class="force-row" style="width: 192px;"><tr>
                                    <td class="col" valign="top" style="padding-left:12px;padding-right:12px;padding-top:18px;padding-bottom:12px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="img-wrapper">
                                            <tr>
                                                <td style="padding-bottom:18px"><a href="https://djinyc.com/products/'.$product->slug.'"><img src="'.$product->featuredImage() . '" border="0" alt="The Albatross flew across the ocean" width="168" height="110" hspace="0" vspace="0" style="max-width:100%; " class="image"></a></td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0"><tr>
                                                <td class="subtitle" style="font-family:Helvetica, Arial, sans-serif;font-size:16px;line-height:22px;font-weight:600;color:#24262b;padding-bottom:6px">
                                                    <a href="https://djinyc.com/products/'.$product->slug.'">' . $product->name . '</a>
                                                </td></tr>
                                        </table>
                                        <div class="col-copy" style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;text-align:left;color:#24262b">';
                    if ($product->basePrice() != $product->getPrice($color, $size)) {
                        $html .= '<a href="https://djinyc.com/products/'.$product->slug.'"><span style="text-decoration: line-through;color:rgba(36, 38, 43, 0.5);">$' . number_format($product->basePrice() * $array->quantity, 2) . '</span></a> ';
                    }
                    $html .= '<a href="https://djinyc.com/products/'.$product->slug.'"><span style="color:#24262b;">$' . number_format($product->getPrice($color, $size) * $array->quantity, 2) . '</span></a>
                                            </div>
                                            <br>
                                        </td>
                                    </tr>
                                </table>
                            </td>';
                }
                $html .= '</tr>';
                $email = new \Email\MailMaster();
                $mergeFields = [
                    'NAME'=>$order->billName(),
                    'TRACKING_NUMBER'=>$order->tracking_number,
                    'ITEMS'=>$html
                ];
                $email->setTo(['email'=>$order->email,'name'=>$order->shipName(), 'type'=>'to'])->setMergeTags($mergeFields)->setTemplate('dji-shipping')->send();
            }

           /* if (isset($_POST['shipping_method'])) {
                if ($order->shipping_method != $_POST['shipping_method']) {
                    $order->shipping_method = \Model\Shipping_Method::getItem($_POST['shipping_method']);
                    $costs = json_decode($order->shipping_method->cost);
                    $order->shipping_method->cost = 0;
                    $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
                    foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                        if (is_null($range) || trim($range) == '') {
                            $range = 0;
                        }
                        if ($subtotal >= $range && isset($costs[$key])) {
                            $order->shipping_cost = $costs[$key];
                        }
                    }
                    $order->shipping_method = $order->shipping_method->id;
                }
            }*/
           if(isset($_POST['shipping_cost'])){
               if($order->shipping_cost != $_POST['shipping_cost']){
                   $order->shipping_cost = $_POST['shipping_cost'];
               }
           }

//            if (isset($_POST['ship_state'])) {
//                $shippingState = preg_replace('/\s+/', '', strtolower($_POST['ship_state']));
//                if ($shippingState == 'ny' ||$shippingState=='newyork') {
//                    $order->tax_rate = 8.875;
//                    $order->tax = $subtotal * ($order->tax_rate / 100);
//                } else {
//                    $order->tax_rate = null;
//                    $order->tax = null;
//                }
//            }

//            $order->total = $subtotal + $order->shipping_cost + $order->tax - $order->coupon_amount;

//            $bd = $order->calculate_total();
//            foreach($bd['updates'] as $field => $value){
//                $order->$field = $value;
//            }
            if ($order->save()) {

                if(isset($_POST['send_confirm']) || isset($_POST['send_update'])){
                    foreach ($order->getOrderProducts() as $orderProduct){
                        $product = \Model\Product::getItem($orderProduct->product_id);
                        $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
                        if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
                        $name = $product->name;
                        $price = $product->price;
                        $html .=
                            '<tr style="height:75.0pt">
                                <td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
                                </td>
                                <td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                    <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
                                
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                                
                                    <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
                                </td>
                            </tr>';
                    }
                    $email = new \Email\MailMaster();
                    $mergeFields = [
                        'ORDER_NUMBER' => $order->id,
                        'DATE' => date('Y-m-d h:i:s',strtotime($order->insert_time)),
                        'DATED' => date('l, F d, Y',strtotime($order->insert_time)),
                        'DATET' => date('h:i A',strtotime($order->insert_time)),
                        'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                        'SHIPPING' => $order->getShippingAddr(),
                        'BILLING' => $order->getBillingAddr(),
                        'ITEMS' => $html,
                        'SUBTOTAL' => number_format($order->subtotal, 2),
                        'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                        'SHIPPINGNAME' => 'Shipping',
                        'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
                        'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                        'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
                        'TOTAL' => number_format($order->total, 2)
                    ];

                    if(isset($_POST['send_confirm'])){
                        $template = 'kaws-order-confirmation';
                    } else if(isset($_POST['send_update'])) {
                        $template = 'kaws-confirmation-update';
                    }

                    $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])
                        ->setMergeTags($mergeFields)
                        ->setTemplate($template);
                    try {
                        $emailResp = $email->send();
                        $n = new \Notification\MessageHandler("Order #{$order->id} saved. <br /> Email Sent");
                        $_SESSION["notification"] = serialize($n);
                    } catch(Mandrill_Error $e){
                        $n = new \Notification\ErrorHandler("Order #{$order->id} Saved <br /> Email failed to Send");
                        $_SESSION['notification'] = serialize($n);
                    }
                }
                else if(isset($_POST['send_shipping'])){
                    if(!$this->sendShippedEmail($order)){
                        $_POST['redirectTo'] = $this->_content . '/update/' . $order->id;
                    } else {
                        $n = new \Notification\MessageHandler("Order #{$order->id} saved. <br /> Email Sent");
                        $_SESSION["notification"] = serialize($n);
                    }
                }
                else if(isset($_POST['send_balance'])){
                    if(!$this->sendBalanceEmail($order)){
                        $_POST['redirectTo'] = $this->_content . '/update/' . $order->id;
                    } else {
                        $n = new \Notification\MessageHandler("Order #{$order->id} saved. <br /> Email Sent");
                        $_SESSION["notification"] = serialize($n);
                    }}
                else if(isset($_POST['send_dupe'])){
                    if(!$this->sendDupeEmail($order)){
                        $_POST['redirectTo'] = $this->_content . '/update/' . $order->id;
                    } else {
                        $n = new \Notification\MessageHandler("Order #{$order->id} saved. <br /> Email Sent");
                        $_SESSION["notification"] = serialize($n);
                    }}
                else {
                    $n = new \Notification\MessageHandler("Order #{$order->id} saved.");
                    $_SESSION["notification"] = serialize($n);
                }
            } else {
                $n = new \Notification\ErrorHandler($order->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . $this->_content . '/update/' . $order->id);
            }

        }
        redirect(ADMIN_URL . $_POST['redirectTo']);
    }

    public function apply_breakdown(Array $arr = []){
        $order = \Model\Order::getItem($arr['id']);
        $bd = $order->calculate_total();
        foreach($bd['updates'] as $field => $value){
            $order->$field = $value;
        }
        $order->save();
        $n = new \Notification\MessageHandler("Order #$order->id updated");
        $_SESSION["notification"] = serialize($n);
        redirect(ADMIN_URL . $this->_content . '/update/' . $order->id);
    }

    public function add_product_post(){
        $resp = ['status'=>false];
        if($_POST['quantity'] > 0) {
            $order = \Model\Order::getItem($_POST['order_id']);
            $product = \Model\Product::getItem($_POST['prod_id']);
            $order_product = new \Model\Order_Product();
            $order_product->product_id = $product->id;
            $order_product->quantity = $_POST['quantity'];
            $order_product->unit_price = $_POST['price'];
            $order_product->order_id = $order->id;
            if ($order_product->save()) {
                $resp['status'] = true;
//                foreach ($order->calculate_total()['updates'] AS $field => $value){
//                    $order->$field = $value;
//                }
//                $order->save();
            }
        }
        $this->toJson($resp);
    }

    public function update_orderProd_post(){
        $resp = ['status'=>false];

        $order_product = \Model\Order_Product::getItem($_POST['op_id']);
        $order = \Model\Order::getItem($order_product->order_id);
        if($_POST['quantity'] == 0) {
            \Model\Order_Product::delete($_POST['op_id']);
            $resp['status'] = true;
        } else {
            $order_product->quantity = $_POST['quantity'];
            $order_product->unit_price = $_POST['price'];
            $order_product->save();
        }
        $resp['status'] = true;
//        foreach ($order->calculate_total()['updates'] AS $field => $value){
//            $order->$field = $value;
//        }
//        $order->save();
        $this->toJson($resp);
    }

    public function fulfill_items(Array $arr = []){
        $this->_viewData->page_title = 'Manage Fulfillment';
        $this->_viewData->order_producuts = \Model\Order_Product::getList(['where'=>"order_id = {$arr['id']}"]);
        parent::update($arr);
    }

    public function fulfill_items_post(){
        if(isset($_POST['fulfillment'])){
            foreach ($_POST['fulfillment'] as $op_key=>$value){
                foreach ($value as $val){
                    if($val > 0){
                        $ri = \Model\Receive_Item::getItem($val);
                        $ri->order_product_id = $op_key;
                        $ri->save();
                    }
                }
            }
        }
        redirect('/admin/orders/update/'.$_POST['id']);
    }

    public function mass_email(Array $arr = []){
        $this->_viewData->orders = isset($_GET['orders']) && $_GET['orders'] ? \Model\Order::getList(['where'=>'id in ('.urldecode($_GET['orders']).')','orderBy'=>'id desc']): [];
        parent::update($arr);
    }

    public function mass_email_post(){
        $orders = \Model\Order::getList(['where'=>'id in ('.urldecode($_GET['orders']).')']);
        foreach($orders as $order){
            $body = sprintf(\Model\Order::$mass_email_template,$order->shipName(),\Model\Order::$mass_email[$_POST['email_type']]);
            $email = new \Email\MailMaster();
            $mergeTags = ['BODY'=>$body];
            $email->setTo(['email'=>$order->email,'name'=>$order->shipName(),'type'=>'to'])->setMergeTags($mergeTags)->setTemplate('modern-vice-mass-email');
            $email->send();

            if($order->email_log){
                $log = json_decode($order->email_log,true);
            } else {
                $log = [];
            }
            $log[] = ['type'=>$_POST['email_type'],'datetime'=>date('Y-m-d H:i:s'),'email_text'=>\Model\Order::$mass_email[$_POST['email_type']]];
            $order->email_log = json_encode($log);
            $order->save();
        }
        redirect(ADMIN_URL.$_POST['redirectTo']);
    }

    public function send_progress(){
        $order = \Model\Order::getItem($_POST['order_id']);
        $orderProduct = \Model\Order_Product::getItem($_POST['order_product_id']);
        if($order && $order->email && $orderProduct && $_POST['status'] != 0){
            $productName = $orderProduct->getProductName();
            $steps = [
                1=>"Hi there! We’ve received the material components for your $productName. At this stage, we will begin cutting the material to design your shoe for the stitching process. Stay tuned for an update once your shoes have been fully stitched.",
                2=>"Hello! The upper material of your $productName has been fully stitched. Next step is to shape your shoes with our foot molds. Once your shoes have been placed on the foot mold, we’ll send you an update. Thanks for your patience as we get closer to finishing your shoes!",
                3=>"Hi there! Your $productName are on the foot mold to fully take shape. We’re adjusting the soles and placing them on the shoes so your order can be completed. Please allow 3-5 days for us to ship your shoes. Upon shipping your order, you’ll receive an email with the tracking number."];
            $html = '<tr class="tc" style="text-align: center" align="center">
                            <td class="f18" style="font-size: 18px">
                                    <p style="margin-bottom: 35px;color:#000000;">'.$steps[$_POST['status']].'</p>
                            </td>
                        </tr>';

            $email = new \Email\MailMaster();
            $mergeTags = [
                'STATUS'=>$html
            ];
            $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeTags)->setTemplate('modern-vice-wip')->send();

//            $email = new \Emagid\Email();
//            $email->addTo($order->email)
//                ->addBcc('info@modernvice.com')
//                ->addBcc('keenon@emagid.com')
//                ->addBcc('jordan@modernvice.com')
//                ->addBcc('eitan@emagid.com');
//            $email->subject('Modern Vice Order Status');
//            $email->body = $html;
//            $email->send();

            $orderProduct->status = $_POST['status'];
            $orderProduct->save();
            echo json_encode(['status'=>'success','message'=>'Email Sent']);
        } else {
            echo json_encode(['status'=>'failed','message'=>'Email failed to send. Speak with administrator.']);
        }
    }

    public function add_to_blacklist(Array $arr = [])
    {
        redirect(ADMIN_URL.'blacklists');
        foreach (\Model\Order::getList(['where' => "id IN (56233,50781,54228,52131,50054,52078,48764,51447,53683,51218,51492,52234,51329,49741,52443,59369,54640,56832,51104,57543,51639,50236,56987,57017,57448,59151,52440,49614,48764,60926,59200,49335,28859,49551,50548,56147,54682,37703,60023,53514,54299,57638,52131,56693,50398,54395,52728,59361,57046,52728,58567,53725,48842,49973,54725,55425,54571,57334,58631,60513,52644,53979,56925,59952,57201,60922,53005,52280,51981,52105,49953,51218,53969,52876,53802,51445,52761,55168,49741,55613,54522,56177,51104,51757,56742,55436,54795,54311,49298,53186,57554,49551,51618,52025,52135,48842,48764,50916,51410,48767,52728,48786,52131,50019,51010,49614,52440,48896,54144,51639,55427,48947,50548,49373,49581,48831,50619,49061,50407,49138,49933,54733,49343,54835,55137,52728,53712,52022,53593,52143,52448,52234,53179,49700,50531,53503,54119,51547,60091,57374,51402,60740,57445,51883,53409,68692,203167,208717,199410,1200655,204691,201524,1204893,53532,48973,54223,56189,48654,52876,49373,72681,72682,72683)"]) AS $order) {
            if ($order->phone != '') {
                $blacklist = new \Model\Blacklist();
                $blacklist->field = 6;
                $blacklist->value = $order->phone;
                $blacklist->save();
            }
            if ($order->email != '') {
                $blacklist = new \Model\Blacklist();
                $blacklist->field = 1;
                $blacklist->value = $order->email;
                $blacklist->save();
            }
            $a = [];
            foreach (['address','address2','city','state','country','zip'] as $field){
                $aval = 'ship_'.$field;
                if($order->$aval != ''){
                    $a[$field] = $order->$aval;
                }
            }
            if(count($a) > 0){
                $blacklist = new \Model\Blacklist();
                $blacklist->field = 5;
                $blacklist->value = json_encode($a);
                $blacklist->save();
            }
        }
    }

    /*  public function update_post_status() {   
            $id = $_POST['id'];
            $status = $_POST['status'];
            $order = \Model\Order::getItem($_POST['id']);
            $order->status = $status;
            $order->save();
        }*/

//    private function sendBalanceEmail($order){
//        return true;
//        $bd = $order->calculate_total();
//        $difference = array_sum($bd['charges']) - $order->total;
//        if(number_format($difference,2) > 0){
//             $items = [];
//             foreach ($order->getOrderProducts() as $orderProduct){
//                 $product = \Model\Product::getItem($orderProduct->product_id);
//                 $items[] = $orderProduct->quantity.' '.$product->name.' ('.$product->color.')';
//             }
//             $html = implode(' and ',$items);
//             $email = new \Email\MailMaster();
//             $mergeFields = [
//                 'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
//                 'AMOUNT' => number_format($order->total,2),
//                 'CORRECT_CHARGE' => number_format($difference+$order->total,2),
//                 'ITEMS' => $html,
//                 'BALANCE' => number_format($difference,2)
//             ];
//             $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-remaining-balance-due');
//             try {
//                 $emailResp = $email->send();
//                 return true;
//             } catch(Mandrill_Error $e){
//                 $n = new \Notification\ErrorHandler('email was not sent');
//                 $_SESSION['notification'] = serialize($n);
//                 return false;
//             }
//         } else {
//             $n = new \Notification\ErrorHandler('no balance');
//             $_SESSION['notification'] = serialize($n);
//             return false;
//         }
//    }
    private function sendBalanceEmail($order){
        $items = [];
        // $bd = $order->calculate_total();
        // $order->difference = array_sum($bd['charges']) - $order->total;
        if(round($order->difference,2) > 0){
            $order->save();
            foreach ($order->getOrderProducts() as $orderProduct){
                $product = \Model\Product::getItem($orderProduct->product_id);
                $items[] = $product->name.' ('.$product->color.')';
            }


            $email = new \Email\MailMaster();
            $mergeFields = [
                'ORDER_NUMBER' => $order->id,
                'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                // 'CORRECT_ORDER_TOTAL' => round(array_sum($bd['charges']),2),
                'CORRECT_ORDER_TOTAL' => number_format(floatval($order->total)+floatval($order->difference),2),
                'ITEMS_IN_ORDER' => implode(',',$items),
                'REMAINING_TOTAL' => number_format($order->difference,2),
                'LINK' => "https://aws.kawsone.com/orders/correct_order/$order->ref_num",
                'ORDER_TOTAL' => number_format($order->total, 2)
            ];
            $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-correct-charge');
            try {
                $emailResp = $email->send();
                    return true;
            } catch(Mandrill_Error $e){
//                $n = new \Notification\ErrorHandler('email was not sent');
//                $_SESSION['notification'] = serialize($n);
                    return false;
            }
         } else {
             $n = new \Notification\ErrorHandler('no balance');
             $_SESSION['notification'] = serialize($n);
             return false;
         }
    }
    private function sendDupeEmail($order){
        $email = new \Email\MailMaster();
        $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setTemplate('kaws-confirm-your-orders');
        try {
            $emailResp = $email->send();
            return true;
        } catch(Mandrill_Error $e){
            $n = new \Notification\ErrorHandler('email was not sent');
            $_SESSION['notification'] = serialize($n);
            return false;
        }
    }
    private function sendShippedEmail($order){
         if($order->tracking_number != ''){
             $html = '';
             foreach ($order->getOrderProducts() as $orderProduct){
                 $product = \Model\Product::getItem($orderProduct->product_id);
                 $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
                 if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
                 $name = $product->name;
                 $price = $product->price;
                 $html .=
                     '<tr style="height:75.0pt">
                        <td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
                        </td>
                        <td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
                        
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                        
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
                        </td>
                    </tr>';
             }
             $email = new \Email\MailMaster();
             $mergeFields = [
                 'ORDER_NUMBER' => $order->id,
                 'TRACKING_NUMBER' => $order->tracking_number,
                 'DATE' => date('Y-m-d h:i:s'),
                 'DATED' => date('l, F d, Y'),
                 'DATET' => date('h:i A'),
                 'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                 'SHIPPING' => $order->getShippingAddr(),
                 'BILLING' => $order->getBillingAddr(),
                 'ITEMS' => $html,
                 'SUBTOTAL' => number_format($order->subtotal, 2),
                 'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                 'SHIPPINGNAME' => 'Shipping',
                 'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
                 'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                 'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
                 'TOTAL' => number_format($order->total, 2)
             ];
             $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-shipping');
             try {
                 $emailResp = $email->send();
                 return true;
             } catch(Mandrill_Error $e){
                 $n = new \Notification\ErrorHandler('email was not sent');
                 $_SESSION['notification'] = serialize($n);
                 return false;
             }
         } else {
             $n = new \Notification\ErrorHandler('Order does not have a tracking number, no email sent');
             $_SESSION['notification'] = serialize($n);
             return false;
         }
    }

    public function sendTrackingEmails(Array $arr = []){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        foreach(\Model\Order::getList(['where'=>"tracking_number != '' AND tracking_number IS NOT NULL AND status NOT IN( 'Declined', 'Canceled', 'Refunded', 
                             'Incomplete PayPal', 'Archived',
                             'Incomplete Amazon', 'Banned', 'Limit Reached') AND id IN(218434,218044,214751,214329,216818)"]) AS $i => $order){
//            if($i > 0) break;
            if(!$this->sendShippedEmail($order)){
              display_notification();
            } else {
                echo "$order->id $order->ship_first_name $order->ship_last_name $order->email <br />";
            }
        }
    }

    public function blacklist(Array $params = []){
        $order = \Model\Order::getItem($params['id']);
        if($order){
            $lists = ["Order {$params['id']} info blacklisted"];
            foreach (\Model\Blacklist::$field_types AS $i => $field){
                if($i != 4 && $i != 5){
                    if($order->$field != ''){
                        $blacklist = \Model\Blacklist::getItem(null,['where'=>"field = $i AND (LOWER(TRIM(value)) = LOWER(TRIM('{$order->$field}')))"]);
                        if(!$blacklist){
                            $blacklist = new \Model\Blacklist(['field'=>$i,'value'=>$order->$field]);
                            if($blacklist->save()){
                                if($i == 2){
                                    $order->$field = '****' . substr($order->cc_number, -4);
                                }
                                $lists[] = \Model\Blacklist::display_types()[$i].": {$order->$field},  Added to Blacklist";
                            }
                        }
                    }
                } else if ($i == 5){
                    $sAddress = [];
                    $sAddress['address'] = $order->ship_address;
                    if($order->ship_address){
                        $sAddress['address2'] = $order->ship_address2;
                    }
                    $sAddress['city'] = $order->ship_city;
                    $sAddress['zip'] = $order->ship_zip;
                    $sAddress['state'] = $order->ship_state;
                    $sAddress['country'] = $order->ship_country;
                    $bAddress = [];
                    $bAddress['address'] = $order->bill_address;
                    if($order->bill_address){
                        $bAddress['address2'] = $order->bill_address2;
                    }
                    $bAddress['city'] = $order->bill_city;
                    $bAddress['zip'] = $order->bill_zip;
                    $bAddress['state'] = $order->bill_state;
                    $bAddress['country'] = $order->bill_country;
                    $blacklist = \Model\Blacklist::getItem(null,['where'=>"field = $i AND value = '".json_encode($sAddress)."'"]);
                    if(!$blacklist){

                        $blacklist = new \Model\Blacklist(['field'=>$i,'value'=>json_encode($sAddress)]);
                        if($blacklist->save()){
                            $lists[] = \Model\Blacklist::display_types()[$i].": ".$order->getShippingAddr().",  Added to Blacklist";
                        }
                    }
                    $blacklist = \Model\Blacklist::getItem(null,['where'=>"field = $i AND value = '".json_encode($bAddress)."'"]);
                    if(!$blacklist){
                        $blacklist = new \Model\Blacklist(['field'=>$i,'value'=>json_encode($bAddress)]);
                        if($blacklist->save()){
                            $lists[] = \Model\Blacklist::display_types()[$i].": ".$order->getBillingAddr().",  Added to Blacklist";
                        }
                    }
                }
            }
            $message = '';
            foreach ($lists as $list) {
                $message .= "<p>$list</p>";
            }
            $n = new \Notification\MessageHandler($message);
            $_SESSION["notification"] = serialize($n);
        }
        redirect("/admin/orders/");
    }

    public function getGiftCardValidation(){
        $arr = ['status'=>'failed','message'=>'Invalid gift card','value'=>0];
        if(isset($_POST['gift_card']) && $_POST['gift_card'] && ($gc = \Model\Gift_Card::validateGiftCard($_POST['gift_card']))){
            $arr = ['status'=>'success','message'=>'Valid gift card','value'=>$gc->amount];
        }
        echo json_encode($arr);
    }

    public function getCcNumber()
    {
        if (isset($_POST['password']) && $_POST['password'] == 'test5343') {
            echo json_encode(\Model\Order::getItem($_POST['order_id'])->cc_number);
        } else {
            echo json_encode("false");
        }
    }

    public function unlock()
    {
        if (isset($_POST['pwd']) && $_POST['pwd'] == 'test5343') {
            echo json_encode("/admin/orders/delete/{$_POST['orderId']}");
        } else {
            echo json_encode("false");
        }
    }

    public function search()
    {
        global $emagid;
        $db = $emagid->getDb();
        $sql = "
select o.id,c.name as campaign,o.fulfillment_status,o.viewed,o.insert_time,o.tracking_number,o.bill_first_name||' '||o.bill_last_name as bill_name,o.status,o.payment_status,o.total,o.payment_method,p.name || ' ( ' || p.color || ' )' as products,op.details
from public.order o
left join order_products op on o.id = op.order_id and o.active = 1
left join product p on op.product_id = p.id
left join campaign c ON o.campaign_id = c.id";
        $parsed = trim(preg_replace('/\s+/',' ',strtolower(urldecode($_GET['keywords']))));
        $expl = explode(' ',$parsed);
        $impl = "'%".implode("%','%",$expl)."%'";
        $where = [];
        if(is_numeric($_GET['keywords'])){
            $where[] = "o.id = {$_GET['keywords']}";
        }
        $where[] = "lower(email) like '%$parsed%'";
        $where[] = "lower(ship_first_name)||' '||lower(ship_last_name) like all(array[$impl])";
        $where[] = "lower(bill_first_name)||' '||lower(bill_last_name) like all(array[$impl])";
        $where[] = "lower(p.name) like all(array[$impl])";
        $limit = ' limit 20';
        $orderBy = ' order by COALESCE(campaign_id,0) desc, id desc';

        $sql .= " where o.active = 1  AND o.insert_time > '2017-12-15' AND (".implode(' or ',$where).')';
        $sql .= $orderBy;
//        $sql .= $limit;
//        echo($sql);
        $result = $db->execute($sql);
//        echo($sql);

        $arr = [];
        $id = '';
        $productList = [];
        foreach($result as $r){
            if($id != $r['id']){
                $id = $r['id'];
                $productList = [];
            }
            if($r['products']){
                $productList[] = $r['products'];
            } else if($r['details']){
                $json = json_decode($r['details'], true);
                $productList[] = $json['misc_name'] . '(Custom)';
            }
//            $obj = \Model\Order::getItem($r['id']);
//            if ($obj && $obj->getInvoice()) {
//                $style = 'C6DDFF';
//            } elseif ($obj && $obj->in_store) {
//                $style = 'ffff9e';
//            } else {
//                $style = '';
//            }
            $r['products'] = $productList;
//            $r['style'] = $style;
            $arr[$r['id']] = $r;

        }
        rsort($arr);
        echo json_encode($arr);
    }

    public function search_by_mpn()
    {
        $mpn = trim($_GET['keywords']);
        $mpn = str_replace('%20', ' ', $mpn);
        $mpn = str_replace('+', ' ', $mpn);
        $mpn = str_replace('%2F', '/', $mpn);
        $mpn = urldecode($mpn);

        $product = \Model\Product::getList(['where' => "mpn = '$mpn'"]);
        $a = Array();

        echo '[';
        foreach ($product as $pr) {
            $order_product = \Model\Order_Product::getList(['where' => 'product_id = ' . $pr->id]);


            foreach ($order_product as $op) {
                $orders = \Model\Order::getList(['where' => 'id = ' . $op->order_id]);


                foreach ($orders as $order) {

                    $a[] = '{ "id": "' . $order->id . '", 
            "insert_time": "' . $order->insert_time . '", 
            "tracking_number": "' . $order->tracking_number . '",
            "status": "' . $order->status . '",
            "bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
            "products": "' . $pr->name . '",
            "total": "' . $order->total . '"}';


                }
            }
        }

        echo implode(',', $a);
        echo "]";

    }

    public function duplicate (Array $params = []){
        $order_id = $params['id'];
        $order = \Model\Order::getItem($order_id);
        if($order){
            $new_order = $order->duplicate();
            if($new_order){
                $new_order->tracking_number = '';
                $new_order->status = 'Active';
                $new_order->fulfillment_status = 'Ready';
                $new_order->save();

                $note = new \Model\Note_Log();
                $note->order_id = $order_id;
                $note->note = "Order duplicate #$new_order->id created";
                $note->admin_id = $this->_viewData->logged_admin->id;
                $note->save();

                $note->id = 0;
                $note->order_id = $new_order->id;
                $note->note = "Order created as duplicate of order #$order_id";
                $note->save();

                redirect(ADMIN_URL."orders/update/$new_order->id");
            } else {
                redirect(ADMIN_URL."orders/update/$order->id");
            }
        }
    }

    public function generateLabel_post(Array $params = []){

        $order = \Model\Order::getItem($_POST['id']?:$params['id']);
        $weight = $_POST['weight']?:2;
        $type = $_POST['type'];
        if(isset($params['type']) && $params['type']){
            $type = $params['type'];
        }
        if(isset($params['weight'])){
            $weight = $params['weight'];
        }
        $postDimensions = [
            'length' => $_POST['length']?:0,
            'width' => $_POST['width']?:0,
            'height' => $_POST['height']?:0,
        ];
        foreach ($postDimensions as $i => $v){
            if(isset($params[$i]) && $params[$i]){
                $postDimensions[$i] = $params[$i];
            }
        }

        $shipment = new Ups\Entity\Shipment();


        $shipper = $shipment->getShipper();
        $shipper->setShipperNumber('9WA616');
        $shipper->setName('KAWSONE');
        $shipper->setAttentionName('KAWSONE');
        $shipperAddress = $shipper->getAddress();
        $shipperAddress->setAddressLine1('259 Banker st');
        $shipperAddress->setPostalCode('11222');
        $shipperAddress->setCity('Brooklyn');
        $shipperAddress->setStateProvinceCode('NY'); // required in US
        $shipperAddress->setCountryCode('US');
        $shipper->setAddress($shipperAddress);
        $shipper->setEmailAddress('noreply@kawsone.com'); //TODO Get KAWs email
        $shipper->setPhoneNumber('3479874113'); //TODO get KAWS number
        $shipment->setShipper($shipper);

        // Kaws address
        $kawsAddress = new \Ups\Entity\Address();
        $kawsAddress->setAddressLine1('259 Banker st');
        $kawsAddress->setPostalCode('11222');
        $kawsAddress->setCity('Brooklyn');
        $kawsAddress->setStateProvinceCode('NY');  // Required in US
        $kawsAddress->setCountryCode('US');


        // Order address
        $orderAddress = new \Ups\Entity\Address();
        $orderAddress->setAddressLine1($order->ship_address);
        if($order->ship_address2){
            $orderAddress->setAddressLine2($order->ship_address2);
        }
        $orderAddress->setPostalCode($order->ship_zip);
        $orderAddress->setCity($order->ship_city);
        if(trim($order->ship_country) == 'United States' || trim($order->ship_country) == 'US'){
            $orderAddress->setCountryCode('US');
        } else {
            if(strlen(trim($order->ship_country)) != 2){
                $countries = get_countries();
                $countries = array_flip($countries);
                $code = $countries[trim($order->ship_country)];
            } else {
                $code = $order->ship_country;
            }
            $orderAddress->setCountryCode($code);
        }
        if(in_array($orderAddress->getCountryCode(),['CA','US'])){
            if(strlen(trim($order->ship_state)) != 2){
                $states = get_states($orderAddress->getCountryCode() == 'CA');
                $states = array_flip($states);
                $code = $states[trim($order->ship_state)];
            } else {
                $code = $order->ship_state;
            }
            $orderAddress->setStateProvinceCode($code);
        }

        $return = true;
        if($type == 2) {
            $shipTo = new \Ups\Entity\ShipTo();
            $shipTo->setAddress($kawsAddress);
            $shipTo->setCompanyName('KAWSONE');
            $shipTo->setAttentionName('KAWSONE Return Service');
            $shipTo->setEmailAddress('noreply@kawsone.com'); //TODO Get KAWs email
            $shipTo->setPhoneNumber('3479874113'); //TODO get KAWS number
            $shipment->setShipTo($shipTo);

            $shipFrom = new \Ups\Entity\ShipFrom();
            $shipFrom->setAddress($orderAddress);
            $shipFrom->setName($order->shipName());
            $shipFrom->setCompanyName($order->ship_first_name);
            $shipFrom->setEmailAddress($order->email);
            $shipFrom->setPhoneNumber($order->phone);
            $shipment->setShipFrom($shipFrom);
        } else {
            $return = false;
            $shipFrom = new \Ups\Entity\ShipFrom();
            $shipFrom->setAddress($kawsAddress);
            $shipFrom->setCompanyName('KAWSONE');
            $shipFrom->setEmailAddress('noreply@kawsone.com'); //TODO Get KAWs email
            $shipFrom->setPhoneNumber('3479874113'); //TODO get KAWS number
            $shipment->setShipFrom($shipFrom);

            $shipTo = new \Ups\Entity\ShipTo();
            $shipTo->setAddress($orderAddress);
            $shipTo->setAttentionName($order->shipName());
            $shipTo->setCompanyName($order->shipName());
            $shipTo->setEmailAddress($order->email);
            $shipTo->setPhoneNumber($order->phone);
            $shipment->setShipTo($shipTo);
        }



        // Set service
        $service = new \Ups\Entity\Service;
        if(trim($order->ship_country) == 'United States' || trim($order->ship_country) == 'US'){
            $service->setCode(\Ups\Entity\Service::S_GROUND);
        } else {
            $service->setCode(\Ups\Entity\Service::S_WW_EXPEDITED);
        }


        $service->setDescription($service->getName());
        $shipment->setService($service);

        // Mark as a return (if return)

        if ( $return ) {
            $returnService = new \Ups\Entity\ReturnService;
            $returnService->setCode(\Ups\Entity\ReturnService::PRINT_RETURN_LABEL_PRL);
            $shipment->setReturnService($returnService);
        }

        // Set description
        $shipment->setDescription("Order #{$order->id}, ".$return?'Return':'');

        // Add Package
        $package = new \Ups\Entity\Package();
        $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
        $package->getPackageWeight()->setWeight($weight);
        $unit = new \Ups\Entity\UnitOfMeasurement;
        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_LBS);
        $package->getPackageWeight()->setUnitOfMeasurement($unit);

        // Set dimensions
        $dimensions = new \Ups\Entity\Dimensions();

        if($postDimensions['height']){
            $dimensions->setHeight($postDimensions['height']?:10);
        }
        if($postDimensions['width']){
            $dimensions->setWidth($postDimensions['width']?:13);
        }
        if($postDimensions['length']){
            $dimensions->setLength($postDimensions['length']?:4);
        }

        $unit = new \Ups\Entity\UnitOfMeasurement;
        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);
        $dimensions->setUnitOfMeasurement($unit);
//        $package->setDimensions($dimensions);

        // Add descriptions because it is a package
        $package->setDescription('Order #'.$order->id);

        // Add this package
        $shipment->addPackage($package);

        // Set Reference Number
//        $referenceNumber = new \Ups\Entity\ReferenceNumber;
//        if ($return) {
//            $referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_RETURN_AUTHORIZATION_NUMBER);
//            $referenceNumber->setValue(23);
////            $referenceNumber->setValue($order_id);
//        } else {
//            $referenceNumber->setCode(\Ups\Entity\ReferenceNumber::CODE_INVOICE_NUMBER);
//            $referenceNumber->setValue(13);
////            $referenceNumber->setValue($order_id);
//        }
//        $shipment->setReferenceNumber($referenceNumber);

        // Set payment information
        $shipment->setPaymentInformation(new \Ups\Entity\PaymentInformation('prepaid', (object)array('AccountNumber' => '9WA616')));

        // Ask for negotiated rates (optional)
        $rateInformation = new \Ups\Entity\RateInformation;
        $rateInformation->setNegotiatedRatesIndicator(1);
        $shipment->setRateInformation($rateInformation);

        // Get shipment info
        try {
            $api = new Ups\Shipping(UPS_ACCESS_KEY, UPS_USER_ID, UPS_SECRET);

            $confirm = $api->confirm(\Ups\Shipping::REQ_NONVALIDATE, $shipment);
            //var_dump($confirm); // Confirm holds the digest you need to accept the result

            if ($confirm) {


                $accept = $api->accept($confirm->ShipmentDigest);
                $label = new \Model\Labels();
                $label->order_id = $order->id;
                $label->type = $type;
                $label->tracking_number  = $accept->PackageResults->TrackingNumber;
                if($label->type == 1){
                    $order->tracking_number = $label->tracking_number;
                }
                $order->save();

                $base64_string = $accept->PackageResults->LabelImage->GraphicImage;
//                $ifp = fopen($label_file, 'wb');
                $gif = $base64_string;
                $data = base64_decode($gif);
                $file = tmpfile();
                fwrite($file, $data);
                $metaDatas = stream_get_meta_data($file);
                $fileName = $metaDatas['uri'];
                $s3 = new EmagidService\S3();
                $name = $s3->upload($fileName,'order_'.\Model\Labels::$types[$type]."label_".uniqid().'.png');
                fclose($file);
                $label->image = $name;
                $label->save();
                $label = \Model\Labels::getItem($label->id);

                $label->insert_time = strtotime($label->insert_time);
                $label->insert_time = date('M d, Y  g:i A',$label->insert_time-14400);
                $label->shipped_time = strtotime($label->shipped_time);
                $label->shipped_time = date('M d, Y  g:i A',$label->shipped_time-14400);
                $_label = [
                  'id'=>$label->id,
                  'img'=>$label->image(),
                  'tracking'=>$label->tracking_number,
                  'insert_time'=>$label->insert_time,
                  'shipped_time'=>$label->shipped_time,
                  'type'=>\Model\Labels::$types[$label->type]
                ];
                $n = new \Notification\MessageHandler('New label generated');
                $_SESSION["notification"] = serialize($n);

                if(isset($params['update'])){
                    return $label;
                } else {
                    $this->toJson(['status'=>true,'label'=>$_label]);
                }
            } else {
                if(isset($params['update'])) {
                    return false;
                } else {
                    $this->toJson(['status' => false, 'message' => 'Label failed to be created']);
                }
            }
        } catch (\Exception $e) {
            if(isset($params['update'])) {
                $n = new \Notification\ErrorHandler($e->getMessage());
                $_SESSION["notification"] = serialize($n);
                return false;
            } else {
                $this->toJson(['status' => false, 'message' => $e->getMessage()]);
            }
        }
    }

    public function pay(Array $params = [])
    {
        if (isset($params['id']) && is_numeric($params['id'])) {

            $order = \Model\Order::getItem($params['id']);

            if (!is_null($order)) {

                $localTransaction = $this->emagid->db->execute('SELECT ref_trans_id FROM transaction WHERE order_id = ' . $order->id);

                if (isset($localTransaction[0]) && isset($localTransaction[0]['ref_trans_id'])) {
                    $refTransId = $localTransaction[0]['ref_trans_id'];

                    $transaction = new AuthorizeNetAIM;
                    $response = $transaction->priorAuthCapture($refTransId);

                    $n = new \Notification\MessageHandler($response->response_reason_text);
                    $_SESSION["notification"] = serialize($n);
                }

                redirect(ADMIN_URL . 'orders/update/' . $order->id);

            }

        }

        redirect(ADMIN_URL . 'orders');
    }

    private function slip_pack($order)
    {
        global $emagid;
        $emagid->email->from->email = 'orders@djinyc.com';

        $email = new \Emagid\Email();

        $order = \Model\Order::getItem($order);


        $email->subject('Order packing slip!');///wtf?
        $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="http://djinyc.com/content/frontend/img/logo.png" alt="Luggage"></center>
</td>
</tr>
</tbody>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:100.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Packing slip</span></b> </p>
                        </td>
                        <td width="108" style="width:1.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:15.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Number:</span></b> </p>
                        </td>
                        <td width="118" style="width:8.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:15.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:1.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:15.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Date:</span></b> </p>
                        </td>
                        <td width="118" style="width:8.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:15.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . date('M d, Y h:i:sa',strtotime($order->insert_time)) . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';

        /* if ($order->payment_method == 1) {
            $email->body .= 'We have received your order and we are currently processing it.<br>';
            $email->body .= 'Your credit card will not be charged until the item’s availability has been established, at which time you will receive a confirmation email.';
            $email->body .= '<p>Then, as soon as your item ships you will receive another email with the tracking information.</p>';
            $email->body .= '<p>If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p>';
        } else if ($order->payment_method == 2) {
            $email->body .= '<p>We will contact you shortly for more information. If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p>';
        } */


        $shp = array($order->ship_first_name, $order->ship_last_name, $order->ship_address, $order->ship_address2,
            $order->ship_country, $order->ship_city, $order->ship_state, $order->ship_zip);


        $blng = array($order->bill_first_name, $order->bill_last_name, $order->bill_address, $order->bill_address2,
            $order->bill_country, $order->bill_city, $order->bill_state, $order->bill_zip);


        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:20.25pt">
                        <td width="50%" style="width:50.0%;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping to:</span></b> </p>
                        </td>
                        <td width="50%" style="width:50.0%;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Billing to:</span></b> </p>
                        </td>
                    </tr>
                    <tr style="height:5.25pt">
                        <td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
                    </tr>
                    <tr>
                        <td style="background:white;padding:0in 0in 7.5pt 15.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">
                            ' . $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
                            ' . $order->ship_city . ' ' . $order->ship_state . ' <br>
                            ' . $order->ship_zip . ' ' . $order->ship_country . '                       
                            </span></p>
                        </td>
                        <td style="background:white;padding:0in 0in 7.5pt 15.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">';
        if (count(array_diff($shp, $blng)) > 0) {
            $email->body .= $order->bill_first_name . ' ' . $order->bill_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->bill_address . ' ' . $order->bill_address2 . '<br>
                            ' . $order->bill_city . ' ' . $order->bill_state . '<br>
                            ' . $order->bill_zip . ' ' . $order->bill_country;
        } else {
            $email->body .= $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
                            ' . $order->ship_city . ' ' . $order->ship_state . '<br>
                            ' . $order->ship_zip . ' ' . $order->ship_country;
        }
        $email->body .= '</p>
                        </td>
                    </tr>
                </tbody>
            </table>';

        if($order->note) {
            $email->body .= '<h4>Note: ' . $order->note . '</h4>';
        }
        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:20.25pt">
                        <td width="255" colspan="2" style="width:191.25pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Items</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Qty</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Color</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Size</span></b> </p>
                        </td>
                         
                    </tr>
                    <tr style="height:5.25pt">
                        <td width="650" colspan="6" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
                    </tr>';

        foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
            $variant = json_decode($order_product->details,true);
            $color = isset($variant['color']) && $variant['color'] ? \Model\Color::getItem($variant['color']) : null;
            $size = isset($variant['size']) && $variant['size'] ? \Model\Size::getItem($variant['size']) : null;

            $product = $order_product->product_id ? \Model\Product::getItem($order_product->product_id) : null;
            $productName = $product ? $product->name: (isset($variant['misc_name']) ? $variant['misc_name']: (isset($variant['gift_card_id']) ? \Model\Gift_Card::getItem($variant['gift_card_id'])->name: ''));
            $productImage = $product ? $product->featuredImage(null,$color->id) : '';
            $custom = !$product && isset($variant['misc_name']) ? '(Custom Item)': '';

            $colorName = $color ? $color->name: '';
            $sizeHtml = $size ? 'US: '.$size->us_size.' EU: '.$size->eur_size : '';

            $style = $order_product->clearance ? 'color:green': '';
            $name = $order_product->clearance ? '(CLEARANCE)': '';

            $email->body .= '<tr style="height:75.0pt">'
                . '<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal"><img  width="400px!important;" src="https://djinyc.com/content/uploads/products/' . $productImage . '"></p></td>'
                . '<td width="100" valign="top" style="width:127.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 3.75pt 0in;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042;'.$style.'"> ' . $productName . ' ' . $custom . '<br>' .$name . '</span></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order_product->quantity . '</span></b></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $colorName . '</span></b></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">'.$sizeHtml.'</span></b></p>
                        </td> </tr>
                ';
        }
        $email->body .= '</tbody>
            </table>
            <p class="MsoNormal">&nbsp;</p>';
        if (!is_null($order->coupon_code)) {
            $savings = ($order->coupon_type == 1) ? '$' : '';
            $savings .= number_format($order->coupon_amount, 2);
            $savings .= ($order->coupon_type == 2) ? '%' : '';
        } else {
            $savings = 0;
        }

        $show_saving = 'display:none;';

        $a = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt;padding-left: 604px;">
                <tbody>
                    <tr>
                         
                        <td style="padding:0in 0in 0in 0in">
                            <table border="0" cellspacing="0" cellpadding="0" width="200" style="width:150.0pt">
                                <tbody>
                                    <tr style="height:20.25pt">
                                        <td width="200" colspan="2" style="width:150.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Order Summary</span></b></p>
                                        </td>
                                    </tr><tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sub Total:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->subtotal, 2) . '</span></p>
                                        </td>
                                    </tr>
                                     
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->shipping_cost, 2) . '</span></p>
                                        </td>
                                    </tr>
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sales Tax:</span></b> </p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->tax, 2) . '</span></p>
                                        </td>
                                    </tr>
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Total:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->total, 2) . '</span></b></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>';


        /*  $email->addTo($order->email);
          $email->addBcc('info@modernvice.com');
          $email->addBcc('orders@modernvice.com');
          $mail_log = new \Model\Mail_Log();
          $mail_log->to_ = $order->email;
          $mail_log->from_ = "orders@modernvice.com";
          $mail_log->subject = "Order packing slip";
          $mail_log->order_id = $order->id;
          $mail_log->text = $email->body;
          $mail_log->save();*/
        echo $email->body; //sends to the customer


    }

    function emailHead(){
        return '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang="en">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
            <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
            <title>Three columns with images</title>
        
            <style type="text/css">
                body {
                    margin: 0;
                    padding: 0;
                    -ms-text-size-adjust: 100%;
                    -webkit-text-size-adjust: 100%;
                }
        
                table {
                    border-spacing: 0;
                }
        
                table td {
                    border-collapse: collapse;
                }
        
                .ExternalClass {
                    width: 100%;
                }
        
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                }
        
                .ReadMsgBody {
                    width: 100%;
                    background-color: #ebebeb;
                }
        
                table {
                    mso-table-lspace: 0pt;
                    mso-table-rspace: 0pt;
                }
        
                img {
                    -ms-interpolation-mode: bicubic;
                }
        
                .yshortcuts a {
                    border-bottom: none !important;
                }
        
                @media screen and (max-width: 599px) {
                    .force-row,
                    .container {
                        width: 100% !important;
                        max-width: 100% !important;
                    }
                }
        
                @media screen and (max-width: 400px) {
                    .container-padding {
                        padding-left: 12px !important;
                        padding-right: 12px !important;
                    }
                }
        
                .ios-footer a {
                    color: #aaaaaa !important;
                    text-decoration: underline;
                }
        
                @media screen and (max-width: 599px) {
                    .col {
                        width: 100% !important;
                        border-top: 1px solid #eee;
                        padding-bottom: 0 !important;
                    }
        
                    .cols-wrapper {
                        padding-top: 18px;
                    }
        
                    .img-wrapper {
                        float: right;
                        max-width: 40% !important;
                        height: auto !important;
                        margin-left: 12px;
                    }
        
                    .subtitle {
                        margin-top: 0 !important;
                    }
                }
        
                @media screen and (max-width: 400px) {
                    .cols-wrapper {
                        padding-left: 0 !important;
                        padding-right: 0 !important;
                    }
        
                    .content-wrapper {
                        padding-left: 12px !important;
                        padding-right: 12px !important;
                    }
                }
            </style>
        </head>
        
        <body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        
        <!-- 100% background wrapper (grey background) -->
        <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
            <tr>
                <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
                    <br>
        
                    <!-- 600px container (white background) -->
                    <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
        
                        <tr>
                            <td class="content" align="left" style="background-color:#ffffff">
        
                                <table class="tc" style="background: #000000; margin-left: auto; margin-right: auto; max-width: 768px; text-align: center; width: 100%;border-radius: 0px 0px 0 0;" bgcolor="#000000">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <a href="https://djinyc.com/">
                                                <img style="width:192px;border: 0; display: inline-block; margin: 20px 0; outline: none; text-decoration: none" alt="DJI Logo" src="http://djinyc.com/content/frontend/assets/img/logoTextWhite.png">
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table style="background: #fff; border: 1px solid #ccc; border-collapse: separate; border-radius: 0px; margin-left: auto; margin-right: auto; max-width: 768px; padding: 60px 40px; width: 100%;border:1px solid #e1e1e1;" bgcolor="#fff">
                                    <tbody>
        
                                    <tr>
                                        <td>
                                            <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;">
                                                <tr>
                                                    <td class="cols-wrapper" style="padding-left:12px;padding-right:12px">
                                                        <table border="0" width="576" cellpadding="0" cellspacing="0" style="width: 576px;">
                                                               ';
    }

    function emailFoot(){
        return '</table>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="tc" style="text-align: center" align="center">
                                <td class="f18" style="font-size: 18px">
                                    <p style="margin-bottom: 35px;color:#000000;">Questions?<br> Email us at
                                        info@djinyc.com or give us a call at (212) 777-1851 to learn more! </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="tc" style="color:#9c9c9c;background: #f4f4f4; margin: 0 auto 0; max-width: 768px; text-align: center; width: 100%" bgcolor="#f4f4f4">
                            <tbody>
                            <tr class="tc" style="text-align: center" align="center">
                                <td class="f24 lh-title" style="border-collapse: separate; color: #9c9c9c; font-size: 24px; line-height: 1.2; padding: 12px 0 15px">Keep in Touch</td>
                            </tr>
                            <tr class="tc" style="text-align: center" align="center">
                                <td class="f18 lh-copy" style="font-size: 18px; line-height: 1.4;color:#9c9c9c;">If you have any questions, concerns, or suggestions,
                                    <br>please email us:
                                    <a href="mailto:info@djinyc.com" style="color:#000000;text-decoration: none;">info@djinyc.com</a>
                                </td>
                            </tr>
                            <tr class="tc" style="text-align: center" align="center">
                                <td style="padding-top: 30px">
                                    <a style="display: inline-block" href="https://twitter.com/dji">
                                        <img style="border: 0; display: inline-block; outline: none; width:36px;height:36px;text-decoration: none" alt="Twitter logo" src="http://djinyc.com/content/frontend/assets/img/twitterCircleBtn.png">
                                    </a>
                                    <a style="display: inline-block; padding: 0 25px" href="https://www.facebook.com/dji">
                                        <img style="border: 0; display: inline-block; outline: none; text-decoration: none;width:36px;height:36px;" alt="Facebook logo" src="http://djinyc.com/content/frontend/assets/img/facebookCircleBtn.png">
                                    </a>
                                    <a style="display: inline-block" href="https://www.instagram.com/dji/">
                                        <img style="border: 0; display: inline-block; outline: none; text-decoration: none;width:36px;height:36px;" alt="Instagram logo" src="http://djinyc.com/content/frontend/assets/img/instaCircleBtn.png">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="f14" style="font-size: 14px;color:#9c9c9c;">©2016 <span style="font-weight: 600;color:#9c9c9c;">dji</span> Adoni Group Inc., All rights reserved.
                                </td>
                            </tr>
                            <tr>
                                <td class="f14" style="color: #9c9c9c; font-size: 14px">247 West 38th Street, 301</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="https://djinyc.com/">
                                        <img style="border: 0; width:60px;display: inline-block; margin-top: 30px; outline: none; text-decoration: none" alt="DJI Logo" src="http://djinyc.com/content/frontend/assets/img/header_logo.png">
                                    </a>
                                </td>
                            </tr>
                            </tbody></table>

                    </td>
                </tr>

            </table>
            <!--/600px container -->


        </td>
    </tr>
</table>
<!--/100% background wrapper-->

</body>
</html>';
    }

    function fixReady(Array $params = []){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        $sql = "SELECT o.id, o.total FROM public.order o WHERE campaign_id = 4 AND fulfillment_status = 'Ready' AND status NOT IN ( 'Declined', 'Canceled', 'Refunded', 
                                 'Incomplete PayPal', 'Archived',
                             'Incomplete Amazon', 'Banned', 'Limit Reached' ) ORDER BY id";
        $changes = [];
        global $emagid;
        $db = $emagid->getDb();
        $results = $db->getResults($sql);
        foreach ($results AS $result){
            if($result['total'] < 310){
                $changes[] = $result['id'];
            } else {
                $order = \Model\Order::getItem($result['id']);
                $cTotal = array_sum($order->calculate_total()['charges']);
                if($order->total != $cTotal){
                    $changes[] = $order->id;
                    var_dump("$order->id : ".($order->total-$cTotal));
                }
            }
        }
        $ids = implode(',',$changes);
        $editSql = "UPDATE public.order SET fulfillment_status = '' WHERE id IN($ids)";
        echo "<br />";
        echo "<br />";
        var_dump($editSql);
    }

    function setRefNums(){

        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        foreach (\Model\Order::getList(['where'=>"(ref_num = '' OR ref_num IS NULL) AND campaign_id = 6"]) AS $order){
            $order->ref_num = generateToken().$order->id.'tat';
            $order->save();
            var_dump($order->id.' '.$order->ref_num);
        }
    }

    function setDifferences(){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        foreach ( \Model\Order::getList(['where'=>"status = 'Declined' and id not in (select distinct order_id from \"transaction\" where raw is not null and raw->>'errors' is not null) and id in (select distinct order_id from \"transaction\" where raw is not null and raw->>'errors' is null) and difference IS null"]) AS $order ){
            $bd = $order->calculate_total();
            $order->difference = round(array_sum($bd['charges'])- $order->total,2);
            if($order->save()){
                var_dump($order->id);
            } else {
                var_dump($order->id." Failed");
                var_dump($order->errors);
            }
            echo "<hr />";
        }
    }

    function sendBalEmails(){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
//        foreach ( \Model\Order::getList(['where'=>"campaign_id = 6 and active = 1 and status = 'Attn Needed'"]) AS $order ){
            foreach ( \Model\Order::getList(['where'=>"difference > 0 and campaign_id = 6 and refunded = 0 and status not in('Refunded','Possible Duplicate','Canceled','Declined','') and email not in ('14333733@QQ.COM','acc01@cfkl.com.my','acc01@cfkl.com.my','alexandre-b.dubois@cetelem.fr','anna.taranto@umusic.com','anthonychoywm@gmail.com','barbieyik@hotmail.com','beaneyruth@gmail.com','bi9foot@hotmail.co.uk','boomthitinan@yahoo.com','bunyaminvest@gmail.com','castric.maxime@gmail.com','christopher_kross@icloud.com','cyril.laurier@gmail.com','dave@mariopatrick.nl','david.cigan@gmail.com','deanwatkiss@protonmail.com','dickyeung@worldnamelogistics.com','doughunter511@btinternet.com','douglas@deltabiscreative.com','dzachmann@gmail.com','eileenyu2@yahoo.com.tw','georgiamontour1@hotmail.com','hoiwai65@hotmail.com','hotkickss@yahoo.com.hk','incrediblekeung@gmail.com','info@thedongallery.com','jia_jun20@hotmail.com','jonathan2796@gmail.com','josh@eurobroid.com','kaijie_242@hotmail.com','kawaii_hiyo@hotmail.com','kayuleun2015@gmail.com','kellypoonnn@gmail.com','kennychan0124@hotmail.com','kevin@sierra.ca','kjg3171@gmail.com','laikaman0418@yahoo.com.hk','lfankhauser@stu.acs-schools.com','linniyat@gmail.com','loicmonde@gmail.com','love_joys@hotmail.com','maartenheinkoops@gmail.com','mariocicciarella13@hotmail.com','michael.mccann@connaught.net','michael@michaelmalapert.com','mr.glimmer@hotmail.com','nathanliu00@gmail.com','nicholas.yip@multiimage.com.sg','oliver@lieff.info','oneill9@gmail.com','oriolangosto@gmail.com','patrick_ebert88@hotmail.com','pinacoladaplease@gmail.com','pritesh_pnc@outlook.com','ralphwinkelmolen@hotmail.com','rjvase@gmail.com','robert.chisholm@tradition.com','ryan_hoa@yahoo.com','steven_oconnell@live.co.uk','tea-carpentry@live.co.uk','tkcecd@jcom.zaq.ne.jp','tom.fiander@ecauk.com','wilhelm@henri.dk','wilsona@amvbbdo.com','winnie@studiokaleido.net','winterhakone@hotmail.com','yuan.zai1122@gmail.com','zuqingteng@gmail.com','zyj90442@gmail.com','1020199@qq.com','119344239@qq.com','35752278@qq.com','358992053@qq.com','394402686@qq.com','540497617@qq.com','547785215@qq.com','895553484@qq.com','949369069@qq.com','a.g_saita@hotmail.it','a_wai_84@yahoo.com.hk','a107lio@gmail.com','a7322730@hotmail.com','aaronhai@gmail.com','adamcapaldi@gmail.com','admin@tec-know.com.au','adrn.chai@gmail.com','ahshean88@hotmail.com','akosart.es@gmail.com','alan8428alan8428@yahoo.com.tw','alexhurter@me.com','alexwong19832003@yahoo.com.hk','aliceduggan77@icloud.com','alvin.tang@hotmail.co.uk','amorphous.object@yahoo.com','anakin888888@gmail.com','andrew@abowler.com','andytran.ad@gmail.com','ANGUS.HO@GMAIL.COM','anitazeng1014@gmail.com','ankiem16@hotmail.com','anton.anhammer@gmail.com','ashraf.khabazian@gmail.com','atse330@hotmail.com','atsuo-w@jk2.so-net.ne.jp','b.angelov95@gmail.com','bao19930825@hotmail.com','basjanproper@gmail.com','beckycpk@gmail.com','bekhai.s75@outlook.com','boris_blanc@hotmail.fr','boydchan@gmail.com','brianchu128@hotmail.com.hk','bricks.g@gmail.com','bryanfan2008@hotmail.com','bungmaeying@gmail.com','busyleung@gmail.com','careanna51311@yahoo.com.hk','carmen.carmenho53@gmail.com','catherine.hamilton@opus.co.nz','cf_ha@hotmail.com','cfs@sc-th.com','ch.bergmayr@gmail.com','charlesnahmany@gmail.com','che.ki.chan@hotmail.com','cheeyeong.chor@gmail.com','chen19910210@hotmail.com','chengyuanfeng1127@gmail.com','chewei26@gmail.COM','chiweibrandon@gmail.com','chowsang419@hotmail.com','chrisj1002001@yahoo.co.uk','christian@gallerimda.com','chunt@costco.co.uk','cindyloli@hotmail.com','claudiasin124@gmail.com','cowperwang@gmail.com','craig@craighunter.ca','craige13@gmail.com','creesevesterdal@gmail.com','crystaljin.c@gmail.com','ctwbusiness@gmail.com','dan.herscovici@gmail.com','daniel@eventcircle.ca','danniparker@hotmail.co.uk','dantgtfe@hotmail.com','darron@copped.io','dd_deniseyu@hotmail.com','dennis.cw.au@gmail.com','dennistsui623@gmail.com','denny@hello-air.com','derubeis.marc@gmail.com','dhrhoang@gmail.com','donaldszeto3031@yahoo.com.hk','dotskl@yahoo.com.hk','duncangarritt@me.com','eddieluzihao@163.com','edwardchui@gmail.com','egomezc@yahoo.com','ekkiko@gmail.com','elias@awada.se','elixr416@gmail.com','elyess@protonmail.com','erling@omegamedia.no','esp.nico@gmail.com','ettisberger@gmail.com','extramorgan2002@gmail.com','f.rosenpick@gmail.com','facetime.joszz@gmail.com','fanzhenvampire@gmail.com','fatjob69@yahoo.com.hk','fatman5jp@gmail.com','fg6366@hotmail.com','fg6366@yahoo.com','flipmode_250@msn.com','francesco@tencom-group.com','franco@volare-via.com','gchow87@hotmail.com','gencan.lam@gmail.com','gilbert88qianwei@gmail.com','giocadag@gmail.com','gorski.maciej.pl@gmail.com','greatharryhui@yahoo.com','greencutter3366@msn.com','hachihachi88@mac.com','halil.bolukbashi@gmail.com','han914512@gmail.com','hani.elkhoury@hotmail.com','hayes1@gmail.com','hcislinchunhung@gmail.com','hermannpuschel@hotmail.com','hezron_chen@hotmail.com','hi_jpkchan@hotmail.com','hs@siegeldesign.com','huguesyox@gmail.com','iameleven@hotmail.com','ict_517@hotmail.com','idebruc@hotmail.com','info@shikimenya.ca','intheknow322@gmail.com','istarmimi7@gmail.com','j_fan@doramail.com','j855234@hotmail.com','jackwilliamdong@gmail.com','jackylamck@gmail.com','jacob_wkc@yahoo.com.hk','Jafette.Madrigal@gmail.com','jamesa2289@gmail.com','jas19yeung@gmail.com','jasper.vercruysse@gmail.com','jay_lsl@yahoo.com.hk','jaynygen@me.com','jchow0215@gmail.com','jchow76@gmail.com','jennyman121@gmail.com','jere.ngg@gmail.com','jeremymgonzalez@hotmail.com','JERMAINSAW@GMAIL.COM','jeryuan.itb@gmail.com','jhershon93@gmail.com','jipjip_mobile@hotmail.com','jme7315@gmail.com','joe.moore@ideagen.com','joelnixon1@gmail.com','john.poon@waterltd.com.hk','john.teng1119@gmail.com','johnnyjuwelz@gmail.com','joiew@hotmail.com','jona.herzig@netcologne.de','jonathanj1967@me.com','jordanalang.77@gmail.com','jorgendewit82@hotmail.com','jumpmatudo@yahoo.co.jp','jurkoprotega@gmail.com','kai@teicomposite.com','kamankh@hotmail.com','kapilsuri@live.ca','karnlpp@gmail.com','katerina.reznitskaya@gmail.com','kawing.leung@hotmail.com','kbabylook@gmail.com','kcck0703@gmail.com','kcng@me.com','keita@havenshop.com','keltkk@outlook.com','ken.chan@live.com','kenji216@hotmail.com','keremilicak@yahoo.com','kevin142@hotmail.com','kevin-706@msn.com','kevwong928@gmail.com','kingytan@outlook.com','kinka2011@yahoo.com.hk','kiss5656a@yahoo.com.tw','kittipat80-@hotmail.com','kkmtsoi@gmail.com','kodchapong@hotmail.com','krystlefu@gmail.com','kwcheng@hotmail.com','kyleleebow2016@gmail.com','larhonto@hotmail.com','larsbernt@hotmail.com','laurent.vassilian@wanadoo.fr','lawgee@icloud.com','little.matthewjames@gmail.com','liu.daiqiang123@gmail.com','liza726@gmail.com','lollipopppv@hotmail.com','loyiukee@hotmail.com','lukas1307@yahoo.de','lyj120@hotmail.com','m.janicki@gmx.net','m.studiodokadoka@gmail.com','m.weber@koeln.de','machiewei@gmail.com','maggiema1209@gmail.com','magisystem.synchro@gmail.com','mahjunjie90@gmail.com','mallun@me.com','mamomimemoxxx@i.softbank.jp','mandair_10@hotmail.com','mansoor.rahaman@me.com','manu20@sunrise.ch','marc.deleze@gmail.com','marcuswngls@gmail.com','mari.schrumpf@gmail.com','mark@lordofthefries.com.au','mark_beylin@hotmail.com','markkevingarcia@hotmail.com','markmancool@hotmail.com','mauritsvandriel@hotmail.com','maxang90@gmail.com','maxime.merran@gmail.com','mdiskul@gmail.com','mdsalter@gmail.com','michael951218@gmail.com','michaelalfarela@gmail.com','michaelsorg@hotmail.com','micheleborgna@tiscali.it','mikedobson8@gmail.com','mike-parker@sky.com','misschristabelle@gmail.com','mona0703@hotmail.com','mpedregalballesteros@gmail.com','mr8bit@me.com','mycyb@qq.com','n.bicchierai@yahoo.fr','n.virk.07@gmail.com','nanakino@naver.com','nathanielrendell@gmail.com','ndse@nextstage.com','neverbowdownhua@gmail.com','neverbowdownhua@gmail.com','newydewy@hotmail.com','nikeshing@msn.com','noahzhao2000.5.2@outlook.com','nxxxxxxxp@gmail.com','obambipaul@gmail.com','okusigbolahan@gmail.com','olutos@gmail.com','onatans@yahoo.com','one0334@163.com','ong2345@hotmail.com','onizuka00918@gmail.com','ossy3104@naver.com','pakkin.lee@hotmail.com','panhsin@gmail.com','paolopazzia@gmail.com','pass10jqka@gmail.com','passthecrayons@icloud.com','patricktan1990@hotmail.com','pcstoltz@gmail.com','prnormansell@gmail.com','pro_mx@hotmail.com','pspaolo@gmail.com','qoo123830@gmail.com','rachelngai_0603@hotmail.com','raylio0715@hotmail.com','rickie0530@gmail.com','rincinning1190@gmail.com','rob@concreteamsterdam.nl','robinson.kernevez@gmail.com','roccowu125@gmail.com','rockyshiu@gmail.com','romainleb@hotmail.fr','ronyu98@hotmail.com','roweiyang@gmail.com','roy.yong@rocketmail.com','roychang008@yahoo.com.hk','rudysillam@hotmail.com','russell.williamson@u-dox.com','ruthbennett35@hotmail.co.uk','s.wyclif@gmail.com','s_ang@me.com','saintn@hotmail.com','sarasola.d.miguel@gmail.com','scott_jason@me.com','seancyhung@gmail.com','sendmenomails@yahoo.de','sengkeong467@hotmail.com','Seto_raymond@hotmail.com','shanelong@gmail.com','shiyi041@gmail.com','shoji@thehappy.jp','simon@lead-distribution.co.uk','sixmel73@gmail.com','sng.1983@gmail.com','solebossy@gmail.com','south_south@hotmail.com','staffan.vilhelm@gmail.com','stashed777@gmail.com','steeve_cute@hotmail.com','stitcha@gmail.com','sundaysmash@hotmail.com','sunnylam228@gmail.com','sylviabbq@gmail.com','t.molyneux@live.ca','tangelven@gmail.com','tarek.a.oueidat@gmail.com','teijepauline@gmail.com','theo2602@gmail.com','tiffany.mtng@gmail.com','tipsuda.suji@gmail.com','tlumpi@arcor.de','tomdavidb@icloud.com','tomhealan84@hotmail.com','tommasolagala94@gmail.com','tsai.kate@gmail.com','tsoikintang@yahoo.com.hk','tut@complexonline.com','twiltman@gmail.com','ugotjin@gmail.com','urmysunshinegy@gmail.com','vernal@188.com','viktorgudnidadason@hotmail.com','vincemak@gmail.com','vincentclli@gmail.com','vivianmai@gmail.com','vladpac@gmail.com','vpokari@hotmail.com','washida.ozone@gmail.com','wcl19958428@gmail.com','weichengx7@gmail.com','weihanli@gmail.com','wengcandy@hotmail.com','wh.matthewlaw@gmail.com','willielim79@gmail.com','winsonmiao@me.com','ycphilippot@gmail.com','yewtgoh@gmail.com','ymanhin@hotmail.com','ymanwai@gmail.com','ymwfx409@ybb.ne.jp','yoongkuan@gmail.com','yuyuq925@gmail.com','yuyuyu318@hotmail.com','yvonnechua.yc@hotmail.com','zackawilcox@gmail.com','zeta@we-say-so.com','zhengfeng2424@gmail.com','zocobain@gmail.com','0787788@gmail.com','237527989@qq.com','2502008107@qq.com','312586998@qq.com','455181464@qq.com','464808539@qq.com','46724516@qq.com','562530782@qq.com','623921260@qq.com','739420201@qq.com','764276626@qq.com','907370528@qq.com','a.leitch78@googlemail.com','aceentsldn@gmail.com','adetroy@hotmail.com','adrienmagnin@gmail.com','ahmedelmeer@gmail.com','ali.isitir@gmail.com','ammar_johri@yahoo.co.uk','andrew830515@gmail.com','angelscriedwhenstarscollide@gmail.com','antonio.costagliola@gmail.com','anuphannote@gmail.com','ariozfashion@gmail.com','arnaud.robichon@me.com','artualgallery@gmail.com','asl@brighteyevc.com','aubreyrosenhek@gmail.com','awgchan@gmail.com','axel.mertens@btinternet.com','azn_djx5000@hotmail.com','bape_kc1028@hotmail.com','baptista-garcia@brainclash.de','BELLETANCL@YAHOO.COM','benavigdori@gmail.com','bennyford@gmail.com','biaoco.paul@gmail.com','blindfaith3by8@gmail.com','blufine@singnet.com.sg','blythetak813@hotmail.com','bossatron04@gmail.com','bowenzhang117@gmail.com','bprice@rennie.com','brentkore@gmail.com','brettfrankle@hotmail.com','captainbrick2012@gmail.com','carlovery45@gmail.com','carolchui12@gmail.com','caroline.asmar@gmail.com','Caroline.scrivens@hotmail.co.uk','cchue0410@gmail.com','ccordes73@gmx.de','chanhoichinggg@hotmail.com','chansun_lou@hotmail.com','charlie.brown163@yahoo.com.hk','charlie.gu@hotmail.com','CHEMICALWESKER@GMAIL.COM','chiulamwong@gmail.com','chl981216@gmail.com','chloe.cml.cl@gmail.com','chonks78@gmail.com','chris.benoit36@gmail.com','christian.fieth@gmx.de','chukaitat@gmail.com','ckc872000@yahoo.com.hk','clarakulala@outlook.com','codyliu91@gmail.com','contact@basilios.fr','cor73s@gmail.com','cultdevision@gmail.com','dadouchaaron@gmail.com','dahae19970524@gmail.com','dalton.kaun@gmail.com','dangermousehero@hotmail.com','danknowles1990@hotmail.com','daradjavaheri@gmail.com','david.chiuchunchieh@gmail.com','deadman8703@hotmail.com','debbiebui@gmail.com','diestel@gmail.com','dinahteston@gmail.com','discograffy@ybb.ne.jp','donaldszeto3031@gmail.com','drskin718@gmail.com','eddie118424@gmail.com','edmond.ng@pennpaper.hk','edwinwcw@hotmail.com','eguarnieri75@gmail.com','emailminhdat@gmail.com','Etsang86@gmail.com','eva@evamena.com','ewan333@hotmail.com','eyefunny@nifty.com','farani.federico@gmail.com','fatdavid_hk@yahoo.com','fatjob69@yahoo.com.hk','filu_22@hotmail.it','findtom@gmail.com','fletchzer@hotmail.com','flo@amongstfew.com','flysora@gmail.com','fqazqaz@gmail.com','frankteixeirahkg@gmail.com','fujiwara.sheep@gmail.com','Garrett@emagid.com','garrett@emagid.com','gene777@gmail.com','george_boey@hotmail.com','gohyewkeng@gmail.com','gongxiaochang@163.com','good0818@hotmail.com','gr76@btinternet.com','guanzhenhui@hotmail.com','gutBee@gmail.com','h4l_al@hotmail.com','hani.elkhoury@hotmail.com','happy_house06@yahoo.com.hk','harryxkaws@yahoo.com.hk','hello@ethanchang.ca','hello@ginneynoa.com','henryyamar@gmail.com','hilmi.wong@gmail.com','iama48@yahoo.com','iangarraway@hotmail.com','idohit@yahoo.com','ie9394@hotmail.com','imelda.christ@gmail.com','inlovewithjean@gmail.com','insane8911@gmail.com','j.klugic@gmail.com','jamesma19871013@gmail.com','jason.xu.cx@gmail.com','jeanpascalfournier@gmail.com','jeff.lee808@gmail.com','jeslyn329@gmail.com','jiewei.koh@gmail.com','jimmy@superdon.co','jm_Boi_hbk@hotmail.com','jmok80@gmail.com','joelkadish@gmail.com','johnsharpebromley@gmail.com','jonathan.weise@live.de','jpbach@gmail.com','jprazowski@bluewin.ch','jranger@web.de','JSivananthan@me.com','julianleung2@gmail.com','jwong.hkg@gmail.com','kawaii_hiyo@hotmail.com','kefzmy@yahoo.com','kennmak@hotmail.com','kfir.kachlon@gmail.com','kinho510@yahoo.com.hk','koon-sureen@hotmail.com','lajfuller@gmail.com','lara2695@hotmail.com','laurent.pouyet@yahoo.fr','Lee17173@hotmail.co.uk','lesleyleung12@hotmail.com','liangliangishere@outlook.com','lihua814@gmail.com','littlelittlepin@yahoo.com','liuyaogang66@gmail.com','lllpfhp@gmail.com','louiseisonline@gmail.com','luke23803012@gmail.com','lulusbakeryplaka@gmail.com','luukvanpuffelen@gmail.com','madevaney1977@gmail.com','mahakutay@aol.com','man_man1984@msn.com','matthias.rohrer9@icloud.com','matthias_hecht@hotmail.com','max@mxdvs.co','mayuko.wong@protonmail.com','me@kimhoong.com','michaelthomson@gmail.com','mike.a.pang@gmail.com','mingochiuapple@gmail.com','minimalist_spaces@hotmail.com','miyabichanszenga@gmail.com','mmorelli@firstavenue.ca','mwessb@gmx.ch','nenonean@gmail.com','nick.endean@btinternet.com','nick325583@btinternet.com','nicoleathana@gmail.com','nkh_@hotmail.com','nleungcw@gmail.com','npudjiadi@gmail.com','ola@maddoxgallery.co.uk','oliver@coinjournal.net','ononoff@gmail.com','owen_622@yahoo.com.hk','palmi.kormakur@icloud.com','Passakorn8032@gmail.com','paul_008@hotmail.com','phil@bam-berlin.com','philipp.striegel@googlemail.com','philippe.winnen@me.com','phoebelry@gmail.com','piggyin76@hotmail.com','pingkoo@gmail.com','pistol@pistolart.com','poonlo418@gmail.com','pris.wss@gmail.com','pritumdhillon027@gmail.com','ramybehman@gmail.com','raul151@yahoo.com','raymondlau_no23@hotmail.com','rewrit3@gmail.com','rexinroom@yahoo.com','rexlin0304@gmail.com','riccardoterzolo@hotmail.com','rick.leong@ymail.com','ridleyk2011@me.com','rogiermulder@me.com','rsellouk@gmail.com','s.benoukaci@yahoo.fr','sam@sfmhomes.com.au','sam@ware.hk','scaglia23@yahoo.it','schneidercheung@yahoo.com.hk','sebastiandp@hotmail.no','seven_kevin777@yahoo.com.hk','shannadunfield@gmail.com','shmr526@gmail.com','siuhim628@gmail.com','sonynex5d@yahoo.com.tw','sopasalo@gmail.com','stevencamp_@hotmail.com','stu94608@hotmail.com','sweetjapan418@gmail.com','syau86@gmail.com','szeki.chan@ralphlauren.com','t.barbey@gmail.com','t.giorgetti@orange.fr','tedvaneldik@hotmail.com','thatanos@gmail.com','theinfamousmickpickles@gmail.com','thejulianjordan@gmail.com','thoang3254@gmail.com','thomas.hirst@me.com','thomasiver@gmail.com','timothylove2002@hotmail.com','titinono@hotmail.com','tjc_carol@yahoo.com.sg','tomgao1204@gmail.com','tonytam1218@gmail.com','unboxes@gmail.com','urbanart.biz@gmail.com','va.henryyu@gmail.com','villainyang01@gmail.com','vincent_yung@hotmail.com','visethsinghsachathet@yahoo.com','vito1008@hotmail.com','w.henseler@sensory-minds.com','wallymud@hotmail.com','wilsonkoh062@gmail.com','wingkkun@gmail.com','wong-sunny@hotmail.com','wordyz@virgilio.it','wuziluoaaron@gmail.com','xavier.gandon@bnpparibas.com','yamantaka2000@hotmail.com','yansencsl@gmail.com','ycwong9634@gmail.com','yeapchoonlin@gmail.com','yehzihyu@gmail.com','ygxmxx@gmail.com','Z3nden@gmail.com','z710922@gmail.com','zhanphillip@hotmail.com','zhenfeng0310@gmail.com','ziadrabbat@gmail.com','zkinghei@gmail.com','1318506088@qq.com','4trendsinfo@gmail.com','aka_kiddo@naver.com','alecborger@shaw.ca','alex.siuhei.cheng@gmail.com','annep@neuf.fr','ape23ape@gmail.com','artmoes@gmail.com','athena_shiu@hotmail.com','bawsjacky@gmail.com','bay.saru@gmail.com','benjaminjohnson552@gmail.com','beruz@naver.com','brendaliu333@hotmail.com','c_jimbo@hotmail.com','cesarf@uvet.net','cheeboon@buffalo.edu','cherrie_house@yahoo.com.hk','chishang2001@gmail.com','chitongshum@gmail.com','chnaccarato@gmail.com','chriskllaw@yahoo.com.hk','christy225@hotmail.com','cryonyoursmile@gmail.com','crystallai329@yahoo.com.hk','cskbilly@hotmail.com','cwhc88@gmail.com','danielohay@gmail.com','dara_angha14@yahoo.co.uk','daydaygood2404@gmail.com','dieter@brauntown.com','dj_cooper_1972@yahoo.co.uk','doi11_2000@yahoo.com.hk','dominicstas@icloud.com','drine.malik@gmail.com','dymondamian@gmail.com','egon@okerman.com','emmanuel.ascoli@gmail.com','ert0922@hotmail.com','feyenoch@yahoo.com','fh.barreiro@gmail.com','fraserku@gmail.com','fstork@mac.com','galliano4649@yahoo.co.jp','glibby1@gmail.com','gng@muho.tv','greybear_01@yahoo.com','guldlossen@gmail.com','haverinenhki@gmail.com','hugo_rassion@hotmail.com','info@inkquisitive.com','ives1980103@gmail.com','izzo20012001@yahoo.co.jp','j_i_n_e@hotmail.com','jackpuis23@GMAIL.COM','jamesyeowdw@gmail.com','jamie.leepl@gmail.com','jayviongjl@hotmail.com','joan_cylam@yahoo.com.hk','joechan819@yahoo.com','jorgensene@gmail.com','joyang219@gmail.com','juanredon@telefonica.net','jun_wei_9@hotmail.com','kachan908@gmail.com','karrchan@yahoo.com.hk','kathoria.m@gmail.com','katrina.pagey@gmail.com','kevwong79@hotmail.com','kitty820310@yahoo.com.hk','kuranyi222000@yahoo.com.hk','kvncelestin@gmail.com','kwkryan@yahoo.com','l.p.celestin@gmail.com','leceayeo@gmail.com','leofu88@gmail.com','leon@leonmartyn.com','lieberummr@gmail.com','lion2lysh@sina.com','loesvandelft@gmail.com','luciqua@alice.it','manchunwong@outlook.com','manuel.raisman@stardusttrading.com','marctanphoto@gmail.com','marilyncolette@gmail.com','mark_warner@hotmail.co.uk','martinchiucss@hotmail.com','maximinberko2@gmail.com','michelcohen01@mac.com','mike_mr@live.cn','mkc19942012@gmail.com','monicaferng@gmail.com','mugeustunberk@yahoo.com','mvdw5722@gmail.com','mymttturingma@163.com','naoduo@gmail.com','naoimgk@gmail.com','nathaniel.aryehmemoune@gmail.com','nkoo1234@gmail.com','numbernineno9@gmail.com','nwjckcity@live.com','oberontoronto@gmail.com','ocar907@yahoo.com.hk','odyahsu@gmail.com','olaf@sb-1.net','oliverkaikai@hotmail.com','pai_nova@hotmail.com','parrot_ng@hotmail.com','peiqian.is@gmail.com','pengngee@yahoo.com','pericolly@gmail.com','polite119@hotmail.com','poonpune@hotmail.com','q_kakashi@hotmail.com','q352884012@126.com','r4lau@yahoo.ca','ross_lyndon_davies@hotmail.com','ryan@dripglass.com','s_fish575@yahoo.com.hk','schermack@lfb.fr','sendkent@gmail.com','smithcole361@gmail.com','starchoice5001@yahoo.co.jp','stevenlin1994@gmail.com','stickitplakt@hotmail.com','studio391@mts.net','sugiball@yahoo.com.hk','supreme-kid@live.nl','szotto26@hotmail.com','t_chauveau@hotmail.co.uk','taazzy@gmail.com','thomas@kitschclub.be','timbosi0625@gmail.com','tk@sixsixonesix.com','troubl515@gmail.com','v.lee-6395@hotmail.com','vickie@avgbrand.com','vinccihuang@gmail.com','voj87@hotmail.com','weendy.n@hotmail.com','wonghualun@gmail.com','wongw23@hotmail.com','xinn.shi@gmail.com','xsching@netvigator.com','yaelnahar@gmail.com','yi410@yahoo.com','yikkali@icloud.com','yoyo.bel@free.fr','zaki@3paradis.com','zhanpx@hotmail.com','zzerone@yahoo.com.hk','1412888222@qq.com','393430204@qq.com','457414247@QQ.COM','aa_fung@hotmail.com','achirathivat25@gmail.com','alan.hsin@sunlive.com.tw','bernd.rathjen@corporate-candy.com','Brendalum1997@gmail.com','casey_chu@hotmail.com','chiachengchu@hotmail.com','clcleye@gmail.com','comingsoonshop@gmail.com','connorforster332@gmail.com','criseone@me.com','cruzk777@gmail.com','cyledwin@yahoo.com.hk','dariushdjav@yahoo.com','david.rahikka@gmail.com','delphineguerin73@gmail.com','devinlan@hotmail.com','dickey1236@yahoo.com.hk','dickyy0921@gmail.com','edward.nwy@gmail.com','emilqqq@gmail.com','gianmariocosta@hotmail.it','gocchigocchi@i.softbank.jp','guillaume.despois@gmail.com','guy.mccusker@me.com','hcs158@yahoo.com','hjo1979@hotmail.com','hougongb@gmail.com','jason_nks@live.com','jerry_ya@hotmail.com','jonchalermwong@yahoo.com','justina.bailey@me.com','kateguo1210@gmail.com','kelvin90613@hotmail.com','kskee01@gmail.com','lgiuriato@hotmail.com','lihsijessie@gmail.com','liv.tsai@gmail.com','louly.90@gmail.com','lovetlo@outlook.com','mcsweeneylee@googlemail.com','me0718girl@gmail.com','micahgcw@yahoo.com.hk','mn@appear.dk','moonpak918@gmail.com','nadya_gunawan@hotmail.com','nczky@hotmail.com','nick.hyounku.lim@gmail.com','ninedicks@hotmail.com','nnweicy97@gmail.com','philynch92@gmail.com','raphyki@hotmail.com','recon0077@yahoo.com.sg','saint104@r5.dion.ne.jp','saranbhatia@outlook.com','sebastiano.fazioli@gmail.com','sinwai303@hotmail.com','slv.chin66@gmail.com','sng_1983@yahoo.com','stopdon@hotmail.com','sudeep@sudeepgohil.com','test4ment@msn.com','tiendong92@gmail.com','tks1114@hotmail.com','tr@nothing-special.nl','tracy.th.vo@gmail.com','twinkle.koay@gmail.com','w.sauman@gmail.com','webber322@hotmail.com','wich.piyamongkol@gmail.com','wwcheukin@gmail.com','yi_sandra@hotmail.com','1294533014@qq.com','alexrealty@gmail.com','ali.saab@autoplexglobal.com','andrea@carandell.com','andrewperth@hotmail.com','bacoboy02@hotmail.com','boey.michel@skynet.be','brendanobrien85@gmail.com','chantallynch_@hotmail.com','cherryfaan@gmail.com','chiyaungwu@yahoo.com.hk','edward.bull5@gmail.com','elixat.design@gmail.com','elmalhouf.samir@gmail.com','fillon.arnaud@gmail.com','fung_shi@hotmail.com','halliegeller@gmail.com','harout@equipbureau.com','hellobonnet@gmail.com','herrling-christian@gmx.de','hha_010@hotmail.com','hobby1942@yahoo.com.hk','jingjing.nancy@gmail.com','jjbrosnan@me.com','joanna_han07@yahoo.com.sg','jonaslangmo@gmail.com','jonathanoua96@icloud.com','joonhyungkim0127@gmail.com','justin.cheung24@gmail.com','k@rarityuk.com','kate@katehalfpenny.com','kelvin_1114@hotmail.com','lkf-tyson0830@live.hk','lokwaii@gmail.com','lookingforterry@gmail.com','lotakhei@gmail.com','lvn.prf@gmail.com','lwfss2009@yahoo.com','marcuschangjr@gmail.com','martinruck90@gmail.com','mikepiot@outlook.com','naoimgk@gmail.com','nicoleblack_g@hotmail.com','odesilets@dwpv.com','polite119@gmail.com','prilatay@gmail.com','rich002@kimo.com','robertmichaeljackson@gmail.com','RobSawyerUK@yahoo.co.uk','schraepenolivier@yahoo.fr','surugiuana@gmail.com','tobywagner@me.com','tonyon84@hotmail.com','tristanbanning@me.com','vincent99999@icloud.com','Wataru_nino@hotmail.com','water21988@hotmail.com','wonuseoki@naver.com','yastupid123@gmail.com','454021714@qq.com','893685911@qq.com','aisugal@hotmail.com','aniawurster@gmail.com','aperucho@kenna.ca','benady@gmail.com','bird88848@yahoo.com.tw','chow_wangcheong16@yahoo.com.hk','cobbysiu305@gmail.com','el_dwarf420@hotmail.com','evaliskas@gmail.com','hahahabrenda@hotmail.com','henri.jaanimagi@gmail.com','iambeamboy@yahoo.com','ibi.mustafi@hotmail.com','id4shelly@hotmail.com','kazuma._.0302@i.softbank.jp','kiwisiu22@gmail.com','lingyuanzhen@1diantang.com','linweichensam0629@gmail.com','lipo811@hotmail.com','louisvvong0802@gmail.com','lzwkevin@live.com','max@gescon.ca','melino-88@hotmail.com','mtc993@yahoo.com','nigeljohanns@gmail.com','peterpoon3@hotmail.com','plainyo@naver.com','puneater_3@hotmail.com','ryan_hargett@hotmail.com','sing6819@gmail.com','tigerlaumh@yahoo.com','william19840804@hotmail.com','desmond_1205@hotmail.com','heyman120@gmail.com','info@gxf.nl','isxxxx@yahoo.com','john_su@live.com','john_su@live.com','ken700406@gmail.com','leroy.philippe@wanadoo.fr','lewis1225@hotmail.com','lmfroggie724@yahoo.com.hk','naphat_teanaj32@hotmail.com','nelson_419@hotmail.com','pfundluca@gmx.ch','sendaivirus@gmail.com','stefano.dallavilla@gmail.com','stepheniesitu@gmail.com','symonvogel@gmx.de','tchchan3@hotmail.com','wellanderphan_@hotmail.com','wrx_kev@hotmail.com','adamradi88@gmail.com','andreas@kandidat.no','cutieelainewong@hotmail.com','goldenchild0330@hotmail.com','hamid.sarhdaoui@hotmail.fr','huyyamy@gmail.com','ilsoopark0519@gmail.com','kakaimay@yahoo.com.hk','kris.paul.r@gmail.com','lamlamlamlamchl@gmail.com','lisalin0411@hotmail.com','lovesyloveny@163.com','pine_khemmanat@me.com','tkbrad@hotmail.com','tw.zoee@gmail.com','unik_studio@hotmail.com','vanenckevortluuk@gmail.com','andrewleekk88@gmail.com','chioner@icloud.com','ff26wyh@gmail.com','katherine.gundeck@gmail.com','katherine.gundeck@gmail.com','kennee.chan@gmail.com','kiskeooo@yahoo.com.hk','kultur@mcbc.de','madamon@gmail.com','made-in-hideo@i.softbank.jp','mohdhag@gmail.com','nelson_419@hotmail.com','paradise41@me.com','pwangchiu@gmail.com','rosichin09@hotmail.com','sgcarq@gmail.com','somewireless@gmail.com','yiu9110@163.com','afebby525@gmail.com','boygirlin@gmail.com','fysique@yahoo.com','gerardmabe07@gmail.com','hypo1983@hotmail.com','jcl@macway.com','kevin@weewungwung.com','m_tidswell@sky.com','mallun@me.com','minhtrong.tran0@gmail.com','yklam0225@gmail.com','jason8887@gmail.com','kgwlai@yahoo.com','lorixcai1112@gmail.com','michael.veres@gmx.de','moxx.tiago@gmail.com','sebmuratyan@gmail.com','brandondiperna@yahoo.com','christophe.demoulin@mac.com','debisiu@gmail.com','Peeranatbright_5@hotmail.com','smeaxcrime@hotmail.fr','tier58101130@gmail.com','d11235813@web.de','hrgig113@yahoo.com.hk','keevin_poon@hotmail.com','andre70783@yahoo.ca','huang_rob@hotmail.com','karen.lim1993@hotmail.com','mradrian.dixon@gmail.com','nourchoucair@gmail.com','richardho_10@hotmail.com','umbro182@hotmail.com','fabulousmonkey2@hotmail.com','aaron.dixon21@gmail.com','cassidy_graham@hotmail.com','pa.hippeau@hotmail.fr','bobby_shannon@hotmail.com','guillaume@artoyz.com','lisette@farinex.ca','yves1976@me.com','megabassist@gmail.com','gautier.berard@gmail.com','love_ccai@hotmail.com','sevenknives@hotmail.com','digard@me.com','n-starko@hotmail.com','rickrick110@gmail.com','rosenberg.ben.j@gmail.com','takevinta.kevin@gmail.com','thorsten@friendlyproducts.eu')"]) AS $order ){
            if($order->charge_added == 1 || $order->charge_email_sent == 1 || $order->difference <= 0 || $order->difference == null){
                continue;
            }

            $order->charge_email_sent = 1;
            $order->save();
            $items = [];
            $bd = $order->calculate_total();

            foreach ($order->getOrderProducts() as $orderProduct){
                $product = \Model\Product::getItem($orderProduct->product_id);
                $items[] = $product->name.' ('.$product->color.')';
            }


            $email = new \Email\MailMaster();
            $mergeFields = [
                'ORDER_NUMBER' => $order->id,
                'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                'CORRECT_ORDER_TOTAL' => number_format(array_sum($bd['charges']),2),
                'ITEMS_IN_ORDER' => implode(',',$items),
                'REMAINING_TOTAL' => number_format($order->difference,2),
                'LINK' => "https://aws.kawsone.com/orders/correct_order/$order->ref_num",
                'ORDER_TOTAL' => number_format($order->total, 2)
            ];
            $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-correct-charge');
            try {
                $emailResp = $email->send();
                echo("Order #{$order->id}  ".$order->email.' email sent. <br/>');
            } catch(Mandrill_Error $e){
                echo("Order #{$order->id}  ".$order->email.' email was not sent. <br/>');
//                return false;
            }
            var_dump($emailResp);
            echo "<hr />";
        }
    }

    function sendLine2Emails(){
        $line2Sql = "SELECT email, max(id) as id FROM public.order WHERE campaign_id = 6 AND add_update_sent = 1 AND add_updated = 0 GROUP BY email";
        global $emagid;
        $db = $emagid->getDb();
        foreach ($db->getResults($line2Sql) as $result){
            $order = \Model\Order::getItem($result['id']);

            $email = new \Email\MailMaster();
            $mergeFields = [
                'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                'ORDER_LINK' => "https://kawsone.com/orders/validate/".$order->ref_num
            ];
            $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('kaws-missing-info');
            try {
                $emailResp = $email->send();
                $order->add_update_sent = 2;
                $order->save();
                var_dump($order->email);
//                return true;
            } catch(Mandrill_Error $e){
                $n = new \Notification\ErrorHandler('email was not sent');
                $_SESSION['notification'] = serialize($n);
//                return false;
            }
        }
    }

    function voidDupeOrders(){
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        $sql = "SELECT ref_tran_id, order_id, amount from public.transaction WHERE order_id in (SELECT id FROM public._dupe_test WHERE keep = 0) and order_id not in(select id from \"order\" where status = 'Refunded') and ((raw IS NOT NULL and raw->>'errors' IS NULL) OR (raw is null and active = 1 and import = 0))";
        global $emagid;
        $db = $emagid->getDb();
        $results = $db->getResults($sql);
        foreach($results as $row){
            var_dump($row);
            $orderid = $row['order_id'];
            $amount = $row['amount'];
            $tranid = $row['ref_tran_id'];
            $order = \Model\Order::getItem($orderid);
            if($order->refunded || $order->status == 'Refunded'){
                continue;
            }

            $authTransaction = new AuthorizeNetAIM();
//        $transaction = \Model\Transaction::getItem(null,['where'=>"ref_tran_id = '{$_POST['authId']}'"]);
            $authNetDetails = new AuthorizeNetTD();
            $transaction = $authNetDetails->getTransactionDetails($tranid);
            $trans = $transaction->xml;
            $cc_number = $trans->transaction->payment->creditCard->cardNumber;
            echo("Refunding ${$amount} on Order #{$orderid}. transaction: {$tranid} <br/>");


            $response = $authTransaction->credit($tranid,$amount,substr($cc_number,-4));
            if($response->approved){
                echo "Refund successful <br />";
                $order->refunded = 1;
                $order->status = 'Refunded';
                $order->save();
                $html = '';

//                var_dump($response);
                $localTransaction = new \Model\Transaction();
                $localTransaction->order_id = $order->id;
                $localTransaction->ref_tran_id = $response->transaction_id;
                $localTransaction->amount = $response->amount;
                $localTransaction->avs_result_code = $response->avs_response;
                $localTransaction->cvv_result_code = $response->cavv_response;
                $localTransaction->type = "REFUND";
                $localTransaction->save();
            } else {
                echo "Refund unsuccessful: {$response->response_reason_text}<br />";
            }

            echo"<hr />";
        }
    }

    function voidRefundedNewCharges(){
        $sql = "SELECT ref_tran_id, order_id from public.transaction WHERE order_id in (SELECT id from public.order where charge_added = 1 and refunded = 1 and status = 'Refunded') and type = 'REFUND' and import = 0";
        global  $emagid;
        $db = $emagid->getDb();
        $results = $db->getResults($sql);
        foreach ($results as $row){
            $order = \Model\Order::getItem($row['order_id']);
            $transaction = \Model\Transaction::getItem(null,['where'=>"ref_tran_id = '{$row['ref_tran_id']}'"]);
            $authTransaction = new AuthorizeNetAIM();

            echo "Voiding refund: {$row['ref_tran_id']} on Order: {$row['order_id']} <br />";
            $response = $authTransaction->void($row['ref_tran_id']);
            if($response->approved) {
                echo "Void successful";
                $transaction->type = 'REFUND_VOIDED';
                $transaction->save();

                $order->refunded = 0;
                $order->status = 'Active';
                $order->save();
            } else {
                echo "Void unsuccessful: $response->response_reason_text";
            }
            echo "<hr />";
        }
    }

    function getTransactions(){
        ini_set('xdebug.var_display_max_depth', -1);
        ini_set('xdebug.var_display_max_children', 256);
        ini_set('xdebug.var_display_max_data', 1024);
        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=transactionMatching.csv");

        function outputCSV($data) {
            $output = fopen("php://output", "w");
            foreach ($data as $row)
                fputcsv($output, $row); // here you can change delimiter/enclosure
            fclose($output);
        }

        $output = [['id',
                    'trans_id',
                    'o_name',
                    'a_name',
                    'addr_matches',
                    'prod_matches',
                    'bill_address',
                    'bill_city',
                    'bill_state',
                    'bill_country',
                    'bill_zip',
                    'a_address',
                    'a_city',
                    'a_state',
                    'a_zip',
                    'a_country',
                    'o_brown_count',
                    'o_grey_count',
                    'o_black_count',
                    'a_brown_count',
                    'a_grey_count',
                    'a_black_count']];

        $sql = "SELECT o.id, t.ref_tran_id, o.bill_first_name || ' ' || o.bill_last_name AS o_name,
                NULL as a_name,
                NULL AS addr_matches, NULL as prod_matches,
                LOWER(o.bill_address) as bill_address, LOWER(o.bill_city) as bill_city, LOWER(o.bill_state) as bill_state, LOWER(o.bill_country) as bill_country, LOWER(o.bill_zip) as bill_zip,
                NULL as a_address, NULL as a_city, NULL as a_state, NULL as a_zip, NULL as a_country,
                count(brc.*) AS o_brown_count, count(grc.*) AS o_grey_count, count(blc.*) AS o_black_count,
                0 AS a_brown_count, 0 AS a_grey_count, 0 AS a_black_count 
                 FROM public.order o 
                  LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.product_id = 105
                  LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.product_id = 106
                  LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.product_id = 107
                 INNER JOIN public.transaction t ON t.order_id = o.id 
                WHERE t.active = 1 AND
                      o.active = 1 AND  
                      t.ref_tran_id IN ('61137909320','61137774628','61137769373','61137769392','61137881244','61137778043','61137772352','61137842726','61137875163','61137775771','61137865571','61137863856','61137889012','61137771040','61137774354','61137770928','61137775820','61137769594','61137777490','61137895093','61137769389','61137894329','61137851227','61137770970','61137770994','61137882753','61137851029','61137774205','61137777362','61137777831','61137889765','61137878018','61137896570','61137777833','61137771012','61137913527','61137842463','61137769575','61137777520','61137774244','61137771039','61137777532','61137774237','61137769502','61137865315','61137775736','61137869823','61137769452','61137774473','61137774132','61137774401','61137770874','61137770776','61137772151','61137777450','61137774480','61137771042','61137770852','61137774296','61137848737','61137851000','61137851220','61137899600','61137774148','61137908832','61137772177','61137716330','61137774386','61137769610','61137774471','61137771001','61137770908','61137769546','61137769397','61137774444','61137882830','61137774610','61137770824','61137770872','61137864487','61137881208','61137777487','61137770918','61137769312','61137775878','61137881221','61137775766','61138031107','61137774563','61137775708','61137865576','61137771007','61137854984','61137860172','61137888941','61137891463','61137890867','61137770979','61137774227','61137851406','61137770818','61137772144','61137774419','61137775720','61137854563','61137895515','61137774385','61137769471','61137770876','61137777563','61137850842','61137769534','61137769320','61137850836','61137769529','61137881227','61137777517','61137774531','61137774459','61137772329','61137846742','61137774340','61137895498','61137879301','61137895090','61137843297','61137884148','61137770877','61137769509','61137846521','61137772268','61137774536','61137955634','61137769567','61137769492','61137777502','61137774171','61137772119','61137778055','61137778205','61137848758','61137775711','61137864881','61137891816','61137877479','61137875435','61137769466','61137895101','61137864486','61137851274','61137897151','61137774128','61137898585','61137770992','61137897520','61137909319','61137909292','61137884170','61137851014','61137934752','61137928326','61137769537','61137771051','61137846545','61137892795','61137777528','61137885212','61137872944','61137775883','61137772274','61137894289','61137890463','61137970275','61138017472','61137774596','61137851248','61137889804','61137777807','61137774213','61137769311','61137774159','61137769408','61137860155','61137716094','61137771077','61137770937','61137889114','61137774396','61137777350','61137769528','61137777438','61137774358','61137769357','61137769525','61137846546','61137772267','61137772328','61137716093','61137774185','61137769589','61137774289','61137774315','61137774370','61137769510','61137774332','61137770963','61137774097','61137777553','61137774150','61137769484','61137772277','61137777845','61137774475','61137774270','61137772339','61137774145','61137770778','61137770995','61137774629','61137774239','61137774276','61137774483','61137774135','61137774548','61137777858','61137778202','61137778201','61137846361','61137850643','61137850833','61137850661','61137854983','61137770894','61137864886','61137774585','61137865590','61137774537','61137769396','61137873193','61137873032','61137774067','61137882728','61137881269','61137869909','61137882703','61137873036','61137884124','61137884153','61137881139','61137882725','61137884128','61137884159','61137873065','61137885233','61137846745','61137889809','61137846733','61137889805','61137896545','61137896558','61137897504','61137769629','61137897163','61137774371','61137898560','61137769383','61137902116','61137879295','61137892797','61137895496','61137904877','61137864893','61137908780','61137912965','61137899010','61137855035','61137777797','61137916247','61137891480','61137769479','61137920571','61137912983','61137923364','61137778190','61137923373','61137923378','61137924247','61137898590','61137932608','61137927580','61137898576','61137916216','61137949222','61137929543','61137771015','61137959318','61137872940','61137769488','61137774170','61137891462','61137908828','61137916207','61137879310','61137855013','61137842307','61137772322','61137774547','61137958642','61137777474','61137842468','61137851235','61138031109','61138031101','61137769451','61137772164','61137772138','61137864448','61137777540','61138083158','61137778215','61137875453','61137994656','61137769514','61137772158','61137772275','61137854791','61137772139','61137774373','61137904297','61137716338','61137771086','61137775818','61137769547','61137769410','61137777506','61137769542','61137775719','61137769388','61137865544','61137769386','61137890829','61137842882','61137847521','61137772276','61137774411','61137774248','61137770964','61137769371','61137769333','61137777491','61137882748','61137881246','61137890859','61137851026','61137775826','61137850827','61137845415','61137774165','61137769322','61137769355','61137769366','61137769375','61137769381','61137769465','61137769519','61137769605','61137774173','61137854796','61137771052','61137771009','61137771031','61137915360','61137854585','61137769390','61137772108','61137772186','61137772216','61137772285','61137769597','61137769402','61137771062','61137772150','61137772183','61137774156','61137774196','61137774210','61137771024','61137774206','61137774274','61137774258','61137769305','61137774264','61137774268','61137774316','61137774324','61137774487','61137774562','61137772141','61137769426','61137770758','61137769524','61137774257','61137772099','61137778199','61137775853','61137774454','61137775747','61137769507','61137775811','61137775831','61137775836','61137770839','61137770955','61137775881','61137774511','61137774634','61137777554','61137844525','61137774577','61137774224','61137772234','61137770904','61137774192','61137774195','61137774134','61137774209','61137770982','61137777345','61137777365','61137777435','61137769517','61137777445','61137777463','61137777482','61137777518','61137775739','61137777530','61137777545','61137777561','61137774382','61137777577','61137777580','61137777583','61137769596','61137772273','61137775859','61137775669','61137770880','61137774222','61137775662','61137774575','61137769477','61137769595','61137775800','61137772140','61137774300','61137772172','61137769454','61137770873','61137772310','61137774149','61137774400','61137775730','61137774355','61137842890','61137774321','61137774501','61137774288','61137775673','61137774280','61137774378','61137774481','61137774551','61137775726','61137775707','61137774261','61137769353','61137777823','61137777843','61137777864','61137777882','61137777853','61137777849','61137777883','61137774479','61137778035','61137774197','61137778053','61137777581','61137778050','61137777850','61137778195','61137778170','61137778176','61137778177','61137774160','61137778184','61137778194','61137778197','61137778216','61137774273','61137769615','61137771049','61137777312','61137855038','61137842589','61137842318','61137846724','61137846531','61137842301','61137847501','61137846741','61137851025','61137846722','61137850996','61137851027','61137851030','61137851015','61137851258','61137851236','61137851438','61137778221','61137846540','61137851443','61137846380','61137854997','61137855042','61137775783','61137769315','61137855334','61137860145','61137860160','61137860154','61137771017','61137842313','61137774186','61137863422','61137863510','61137775779','61137866324','61137866335','61137774491','61137866716','61137875468','61137866339','61137775907','61137864478','61137873147','61137873173','61137873157','61137873194','61137884175','61137867172','61137875163','61137875243','61137875467','61137882756','61137879440','61137869902','61137881120','61137881221','61137881230','61137881387','61137882705','61137882830','61137774486','61137882736','61137873141','61137884129','61137881194','61137884112','61137884131','61137884150','61137884161','61137871647','61137881213','61137770773','61137875180','61137885222','61137777513','61137886976','61137769476','61137889113','61137889009','61137889037','61137889045','61137889038','61137889057','61137889790','61137769487','61137884165','61137777860','61137895488','61137890469','61137774594','61137889112','61137891834','61137891837','61137891817','61137891827','61137892183','61137895481','61137884178','61137881283','61137869964','61137894308','61137884147','61137894788','61137895470','61137895507','61137895502','61137851243','61137895516','61137894331','61137891475','61137869887','61137897507','61137897487','61137897483','61137894781','61137897140','61137902281','61137774522','61137898999','61137843062','61137895100','61137774601','61137897159','61137897484','61137899610','61137899007','61137774193','61137899008','61137889130','61137884121','61137896840','61137902106','61137903855','61137898302','61137896830','61137904887','61137902108','61137769471','61137905110','61137903530','61137772221','61137903121','61137897128','61137907713','61137898335','61137907694','61137908778','61137908798','61137909320','61137777813','61137910459','61137872946','61137774204','61137911964','61137911972','61137908755','61137911959','61137897156','61137913525','61137909342','61137912995','61137909298','61137896834','61137881036','61137911944','61137910556','61137916237','61137774124','61137915335','61137919969','61137915387','61137770827','61137890872','61137875452','61137919978','61137920589','61137908182','61137923571','61137923775','61137929471','61137929487','61137923377','61137932543','61137932536','61137933713','61137927008','61137769384','61137933662','61137774158','61137769348','61137889101','61137884183','61137949216','61137949245','61137952824','61137952789','61137777591','61137892196','61137958054','61137957710','61137960002','61137879240','61137960723','61137769328','61137772145','61137774125','61137970469','61137970288','61137970441','61137934686','61137974111','61137774389','61137881132','61137976955','61137770991','61137976950','61137980131','61137985332','61137863857','61137777524','61137770938','61137989773','61137992347','61137985312','61137774394','61137994634','61137997131','61137884132','61137770905','61138006880','61138008217','61138033727','61138017060','61137770907','61137774234','61137775674','61137774579','61138030903','61137990315','61137955630','61137769411','61138041093','61137774290','61137775892','61137869888','61137882666','61137910559','61137774376','61138083524','61138083910','61137851001','61138089882','61137842735','61137842881','61137772335','61137774304','61137869924','61137771067','61137772209','61137775890','61137844507','61137775830','61137775818','61137882835','61137770828','61137770893','61137777579','61137774609','61137772323','61137957457','61137774263','61137769323','61137770993','61137769423','61137855021','61137775723','61137770825','61137842173','61137769544','61137770829','61137771004','61137769594','61137771034','61137770885','61137771054','61137778192','61137772179','61137769324','61137777446','61137854591','61137772158','61137774524','61137850845','61137774228','61137770912','61137775713','61137772313','61137843284','61137774438','61137769552','61137772333','61137772176','61137889007','61137777570','61137774147','61137851217','61137770892','61137775731','61137777414','61137774626','61137769401','61137774302','61137770942','61137909323','61137777497','61137770971','61137769568','61137881393','61137774390','61137890858','61137777335','61137769429','61137771021','61137769316','61137772213','61137770782','61137774218','61137772278','61137774617','61137774388','61137769394','61137774584','61137774624','61137774169','61137771025','61137777501','61137770889','61137879212','61137770832','61137775721','61137774620','61137772308','61137842180','61137875335','61137770783','61137770878','61137777855','61137770837','61137772324','61137851231','61137889792','61137771036','61137771026','61137850997','61137777834','61137855353','61137769358','61137769489','61137769453','61137769504','61137769536','61137769531','61137769549','61137774463','61137774353','61137771075','61137775861','61137770841','61137770895','61137772286','61137770969','61137770951','61137771011','61137770967','61137772208','61137769606','61137884168','61137772217','61137772351','61137777856','61137774292','61137774236','61137774217','61137774311','61137774297','61137774327','61137848736','61137774458','61137774484','61137777370','61137777508','61137851229','61137777407','61137769418','61137771005','61137952817','61137848906','61137843303','61137843081','61137843080','61137843289','61137898565','61137846534','61137848909','61137848927','61137848910','61137851036','61137851241','61137851211','61137851237','61137851246','61137851255','61137851421','61137875201','61137854565','61137854764','61137854781','61137854777','61137854808','61137859963','61137855004','61137855014','61137855009','61137855000','61137855320','61137855358','61137857350','61137860165','61137860157','61137863499','61137863501','61137864904','61137865353','61137863860','61137865352','61137865580','61137866771','61137869833','61137884177','61137872469','61137875402','61137881210','61137885211','61137876612','61137875309','61137879322','61137889798','61137885200','61137881195','61137881217','61137884193','61137889779','61137889006','61137884173','61137884191','61137884149','61137884158','61137885205','61137885218','61137886934','61137889053','61137889019','61137889124','61137889041','61137889106','61137892787','61137891836','61137902286','61137889754','61137890496','61137889802','61137889812','61137890473','61137891819','61137891490','61137890825','61137891829','61137891823','61137892169','61137892785','61137882598','61137889770','61137896828','61137884160','61137770902','61137904869','61137903498','61137907672','61137902284','61137903503','61137908163','61137907682','61137918121','61137924248','61137927547','61137927017','61137934672','61137940667','61137769356','61137775749','61138041063')
                GROUP BY o.id, t.ref_tran_id, o.bill_first_name, o.bill_last_name, o.bill_address, o.bill_city";
        global $emagid;
        $db = $emagid->getDb();
        $oresults = $db->getResults($sql);
        foreach ($oresults as $i => $row){
            $authNetDetails = new AuthorizeNetTD();
            $transaction = $authNetDetails->getTransactionDetails($row['ref_tran_id']);
            $tran = $transaction->xml->transaction;
            $billTo = $tran->billTo;
            $row['a_name'] = strtolower($billTo->firstName . ' ' . $billTo->lastName);
            $row['a_address'] = strtolower($billTo->address);
            $row['a_city'] = strtolower($billTo->city);
            $row['a_state'] = strtolower($billTo->state);
            $row['a_zip'] = strtolower($billTo->zip);
            $row['a_country'] = strtolower($billTo->country);
            $row['addr_matches'] = 0;
            $countries = get_countries();
            if(trim(strtolower($billTo->address))    == trim(strtolower($row['bill_address']))) $row['addr_matches']++;
            if(trim(strtolower($billTo->city))       == trim(strtolower($row['bill_city']))) $row['addr_matches']++;
            if(trim(strtolower($billTo->state))      == trim(strtolower($row['bill_state']))) $row['addr_matches']++;
            if(trim(strtolower($billTo->zip))        == trim(strtolower($row['bill_zip']))) $row['addr_matches']++;
            if(
                (trim(strtolower($billTo->country))    == trim(strtolower($row['bill_country']))) ||
                (strtolower($countries[trim(strtoupper($billTo->country))]) == trim(strtolower($row['bill_country'])))
            ) $row['addr_matches']++;


            $line_items = $tran->lineItems->lineItem;
            foreach($line_items as $li){
                if($li->itemId == '105'){
                    $row['a_brown_count'] = 1;
                } else if($li->itemId == '106'){
                    $row['a_grey_count'] = 1;
                } else if($li->itemId == '107'){
                    $row['a_black_count'] = 1;
                }
            }
            $row['prod_matches'] = 0;
            if($row['a_brown_count'] == $row['o_brown_count']){ $row['prod_matches']++; }
            if($row['a_grey_count'] == $row['o_grey_count']){ $row['prod_matches']++; }
            if($row['a_black_count'] == $row['o_black_count']){ $row['prod_matches']++; }
            if(($row['prod_matches'] == 3 && $row['addr_matches'] == 5) || (
                (trim(strtolower($billTo->zip))  == trim(strtolower($row['bill_zip'])) &&
                trim(strtolower( $row['a_name'])) == trim(strtolower($row['o_name'])) &&
                $row['prod_matches'] == 3)
        )){
                $id = $row['id'];
                $updatesql = "UPDATE public.order SET status = 'Active' WHERE id = $id";
                $db->execute($updatesql);
                $note = new \Model\Note_Log();
                $note->order_id = $id;
                $note->note = "Transaction verified";
                $note->admin_id = 1;
                $note->save();
                continue;
            }

            $output[] = $row;
//            var_dump($row);
//            exit;
        }
        outputCSV($output);
    }

    function userMergeResponses(Array $params = []){
        global $emagid;
        $db = $emagid->getDb();
        $pageSize = 20;

        $this->_viewData->page_title = "Manage Merge Requests";

        $sf = '';
        switch($_GET['sf']){
            case 'wanted':
                $sf = " AND unwanted_orders = '{}' ";
                break;
            case 'unwanted':
                $sf = " AND wanted_orders = '{}' ";
                break;
            default:
                break;
        }

        $this->_viewData->current_page_index = $current_page = $_GET['page'] = $_GET['page']?:1;
        $countSQL = "SELECT count(*) as responses FROM order_verifications WHERE active = 1 ".$sf;
        $results = $db->getResults($countSQL);
        $this->_viewData->total_pages = $total_pages = ceil($results[0]['responses']/20);
        if($total_pages < $current_page){
            $this->_viewData->current_page_index = $current_page = 1;
        }
        $offset = ($current_page-1)*20;

        $sql = "SELECT * FROM order_verifications WHERE active = 1 $sf ORDER BY id ASC LIMIT $pageSize OFFSET $offset";
        $results = $db->getResults($sql);


        $this->_viewData->records = $results;
        $this->loadView($this->_viewData);
    }

    function userResponse(Array $params = []){
        $id = $params['id'];
        global $emagid;
        $db = $emagid->getDb();
        $response = \Model\Order_Verification::getItem($id);
        if($response == null){
            $n = new \Notification\ErrorHandler('Record does not exist');
            $_SESSION['notification'] = serialize($n);
            redirect(ADMIN_URL.'orders/userMergeResponses');
        }
        $this->_viewData->page_title = "Customer: $response->user_identifier";

        $goodSQL = "SELECT o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name || ' ' || o.ship_last_name AS name, count(brc.*) AS brown_count, count(grc.*) AS grey_count, count(blc.*) AS black_count FROM public.order o
                    INNER JOIN order_verifications v ON v.id = $id and o.id::text = any(v.wanted_orders)
                    LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.active = 1 AND brc.product_id = 105
                    LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.active = 1 AND grc.product_id = 106
                    LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.active = 1 AND blc.product_id = 107
                    WHERE o.active = 1
                    GROUP BY o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name, o.ship_last_name";
        $badSQL  = "SELECT o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name || ' ' || o.ship_last_name AS name, count(brc.*) AS brown_count, count(grc.*) AS grey_count, count(blc.*) AS black_count FROM public.order o
                    INNER JOIN order_verifications v ON v.id = $id and o.id::text = any(v.unwanted_orders)
                    LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.active = 1 AND brc.product_id = 105
                    LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.active = 1 AND grc.product_id = 106
                    LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.active = 1 AND blc.product_id = 107
                    WHERE o.active = 1
                    GROUP BY o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name, o.ship_last_name";


        $good_orders = [];
        $bad_orders  = [];
        $this->_viewData->generated_order = [];
        if($response->generated_order){
            $genSQL = "SELECT o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name || ' ' || o.ship_last_name AS name, count(brc.*) AS brown_count, count(grc.*) AS grey_count, count(blc.*) AS black_count FROM public.order o
                    INNER JOIN order_verifications v ON v.id = $id and o.id = v.generated_order
                    LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.active = 1 AND brc.product_id = 105
                    LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.active = 1 AND grc.product_id = 106
                    LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.active = 1 AND blc.product_id = 107
                    WHERE o.active = 1
                    GROUP BY o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name, o.ship_last_name";
            $this->_viewData->generated_order = $db->getResults($genSQL)[0];
        }

        $good_totals = 0;
        $good_results = $db->getResults($goodSQL);
        foreach ($good_results as $row){
            $oid = $row['id'];
            $total = 0;
            $transactions = [];
            $authNetDetails = new AuthorizeNetTD();
            $_transactions = \Model\Transaction::getList(['where'=>"order_id = $oid AND ref_tran_id IS NOT NULL AND ref_tran_id != 0"]);
            foreach($_transactions AS $localTransaction){
                $transaction = $authNetDetails->getTransactionDetails($localTransaction->ref_tran_id);
                $trans = $transaction->xml;
                $trow = ['id'=>$localTransaction->ref_tran_id,'order_id'=>$row['id'],'type'=>$localTransaction->type, 'amount'=>$localTransaction->amount, 'status'=>$trans->transaction->transactionStatus];
                $toadd = $localTransaction->amount;
                if($trans->transactionType == 'refundTransaction' ){
                    $toadd *= -1;
                }
                if(in_array($trans->transaction->transactionStatus,['authorizedPendingCapture','capturedPendingSettlement','refundPendingSettlement','settledSuccessfully']) === false ){
                    $toadd = 0;
                }
                $total = $total + $toadd;
                $transactions[] = $trow;
            }
            $good_totals = $good_totals + $total;
            $row['transactions'] = $transactions;
            $good_orders[$row['id']] = $row;
        }

        $bad_totals = 0;
        $bad_results = $db->getResults($badSQL);
        foreach ($bad_results as $row){
            $oid = $row['id'];
            $total = 0;
            $transactions = [];
            $authNetDetails = new AuthorizeNetTD();
            $_transactions = \Model\Transaction::getList(['where'=>"order_id = $oid AND ref_tran_id IS NOT NULL AND ref_tran_id != 0"]);
            foreach($_transactions AS $localTransaction){
                $transaction = $authNetDetails->getTransactionDetails($localTransaction->ref_tran_id);
                $trans = $transaction->xml;
                $trow = ['id'=>$localTransaction->ref_tran_id,'order_id'=>$row['id'],'type'=>$localTransaction->type, 'amount'=>$localTransaction->amount, 'status'=>$trans->transaction->transactionStatus];
                $toadd = $localTransaction->amount;
                if($trans->transactionType == 'refundTransaction' ){
                    $toadd *= -1;
                }
                if(in_array($trans->transaction->transactionStatus,['authorizedPendingCapture','capturedPendingSettlement','refundPendingSettlement','settledSuccessfully']) === false ){
                    $toadd = 0;
                }
                $total = $total + $toadd;
                $transactions[] = $trow;
            }
            $bad_totals = $bad_totals + $total;
            $row['transactions'] = $transactions;
            $bad_orders[$row['id']] = $row;
        }

        $response->good_totals = $good_totals;
        $response->good_orders = $good_orders;
        $response->bad_totals = $bad_totals;
        $response->bad_orders = $bad_orders;
        $this->_viewData->response = $response;

        $this->loadView($this->_viewData);

    }
}
