<?php

class wholesale_paymentsController extends adminController{

	function __construct(){
        parent::__construct('Wholesale_Payment');
    }
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }
}