<?php

class pressController extends adminController{
    public function __construct()
    {
        parent::__construct('Press','press');
    }

    public function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        parent::index($params); // TODO: Change the autogenerated stub
    }

    public function sorter(Array $params = []){
        parent::index($params);
    }

    public function sorter_post(){
        $data = json_decode($_POST['data'])[0];
        $display = 1;
        foreach($data as $da){
            $press = \Model\Press::getItem($da->id);
            $press->display_order = $display;
            $press->save();

            $display++;
        }
    }

}