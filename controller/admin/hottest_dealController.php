<?php

class hottest_dealController extends adminController{

    public function __construct()
    {
        parent::__construct('Hottest_Deal', 'hottest_deal');
    }

    public function index(Array $params = [])
    {
        parent::index($params);
    }

    public function update(Array $arr = [])
    {
        $this->_viewData->hottest_deal = \Model\Hottest_Deal::getItem($arr['id']);

        $this->loadView($this->_viewData);
//        parent::update($arr);
    }

    public function update_post()
    {
        $hottest_deal = \Model\Hottest_Deal::getItem($_POST['id']);
        $hottest_deal->title = $_POST['title'];
        $hottest_deal->subtitle = $_POST['subtitle'];
        $hottest_deal->status = $_POST['status'];
        $hottest_deal->discount = $_POST['discount'] == ""? 0:$_POST['discount'];
        $hottest_deal->start_date = $_POST['start_date'] == ""? NULL:$_POST['start_date'];
        $hottest_deal->end_date = $_POST['end_date'] == ""? NULL:$_POST['end_date'];
        $hottest_deal->save();

        $product = \Model\Product::getItem($hottest_deal->product_id);
        if($hottest_deal->status == true) {
            $product->price = number_format((1 - ($hottest_deal->discount/100)) * $product->price, 2, '.', '');
        } else {
            $product->price = $hottest_deal->original_price;
        }
        $product->save();

        redirect(ADMIN_URL.'hottest_deal');
//        parent::update_post();
    }

    public function addHottestDeal(){
        $productId = $_POST['product_id'];

        if(\Model\Hottest_Deal::getItem(null,['where'=>"product_id = $productId"])) {
            echo \Model\Hottest_Deal::getItem(null,['where'=>"product_id = $productId"])->id;
        } else {
            $hottestDeal = new \Model\Hottest_Deal();
            $hottestDeal->product_id = $productId;
            $hottestDeal->original_price = \Model\Product::getItem($productId)->price;
            $hottestDeal->save();
            echo json_encode($hottestDeal->id);
        }
    }
}