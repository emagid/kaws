<?php

class wholesalesController extends adminController{

	function __construct(){
        parent::__construct('Wholesale');
    }
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }

    public function update(Array $arr = [])
    {
        global $emagid; $db = $emagid->getDb();
        $productSql = "SELECT p.id,p.name,p.type,p.price,CASE WHEN pc.category_id = 63 THEN 1 ELSE null END AS part from product p LEFT JOIN product_categories pc ON p.id = pc.product_id GROUP BY p.id,p.name,p.type,p.price,part order by type,name";
        $products = $db->execute($productSql);
        $productGrid = [];
        foreach($products as $product){
            $type = $product['type'] && \Model\Product::$type[$product['type']] ? \Model\Product::$type[$product['type']]: 'Etc';
            $productGrid[$type][] = $product;
        }
        $this->_viewData->productGrid = $productGrid;

        $categories = \Model\Category::getList(['orderBy'=>'name']);
        foreach($categories as $category){
            $sql = 'select product_id from product_categories pc inner join product p on p.id = pc.product_id where p.active = 1 and category_id = '.$category->id;
            $res = implode(',',flatten($db->execute($sql)));
            $category->product_id = $res;
        }
        $this->_viewData->categoryGrid = $categories;

        $this->_viewData->wholesale_items = $this->_viewData->wholesale_ids = $this->_viewData->orders = [];
        if(isset($arr['id']) && $arr['id']){
            $wholesale_items = \Model\Wholesale_Item::getList(['where'=>"wholesale_id = {$arr['id']}"]);
            foreach($wholesale_items as $item){
                $this->_viewData->wholesale_ids[] = $item->product_id;
            }
            $this->_viewData->wholesale_items = $wholesale_items;
            $this->_viewData->wholesale_ids = json_encode($this->_viewData->wholesale_ids);

            $orderHistory = \Model\Order::getList(['where'=>"wholesale_id = {$arr['id']}"]);
            $this->_viewData->orders = $orderHistory;

        }
        parent::update($arr);
    }

    public function update_post()
    {
        $_POST['terms'] = json_encode($_POST['terms']);
        $wholesale = \Model\Wholesale::getItem($_POST['id']);
        if($_POST['status'] == '1'){
            (new EmagidService\MailMaster())
                ->setTemplate('dji-wholesale-approved')
                ->addTo(['email' => $wholesale->email, 'name' => 'test', 'type' => 'to'])
                ->addMergeTags([$wholesale->email => [
                    'NAME' => $wholesale->name,
                ]])
                ->send();
        }
        parent::update_post();
    }

    public function save_items(){
        $id = $_POST['id'];
        $items = $_POST['items'];

        /** Get active */
        $map = \Model\Wholesale_Item::getList(['where'=>"wholesale_id = $id"]);
        $prodList = [];
        foreach($map as $key=>$item){
            $prodList[$key] = $item->product_id;
        }

        /** Find item in map, else create new item */
        foreach($items as $item){
            if(in_array($item['id'],$prodList)){
                $key = array_search($item['id'],$prodList);
                $ws = $map[$key];
                $ws->price = $item['price'];
                $ws->save();

                /** Unset key value */
                unset($map[$key]);
            } else {
                $ws = new \Model\Wholesale_Item();
                $ws->wholesale_id = $id;
                $ws->product_id = $item['id'];
                $ws->price = $item['price'];
                $ws->save();
            }
        }
        foreach($map as $unsetItems){
            \Model\Wholesale_Item::delete($unsetItems->id);
        }
        echo json_encode(['status'=>'success','message'=>'Success']);
    }
}