<?php

class fulfillmentController extends adminController {

    function __construct(){
        parent::__construct('Fulfillment');
    }

    public function index(Array $params = []){

        $this->_viewData->campaigns = \Model\Campaign::getList();
        foreach($this->_viewData->campaigns as $campaign){
            $campaign->available = 0;
            foreach (\Model\Campaign_Product::getList(['where'=>"campaign_id = $campaign->id"]) as $cp){
                $campaign->available += $cp->quantity;
            }
            $totalSql     = "SELECT COUNT(*) FROM public.order o WHERE o.campaign_id = $campaign->id AND o.status NOT IN ( 'Declined', 'Canceled', 'Returned','Refunded', 'Incomplete PayPal', 'Archived', 'Incomplete Amazon', 'Banned', 'Limit Reached' )";
            $fulfilledSql = "SELECT COUNT(*) FROM public.order o WHERE o.campaign_id = $campaign->id AND o.status NOT IN ( 'Declined', 'Canceled', 'Returned','Refunded', 'Incomplete PayPal', 'Archived', 'Incomplete Amazon', 'Banned', 'Limit Reached' ) AND o.fulfillment_status = 'Processed' AND o.status != 'Shipped'";
            $pendingSql   = "SELECT COUNT(*) FROM public.order o WHERE o.campaign_id = $campaign->id AND o.status NOT IN ( 'Declined', 'Canceled', 'Returned','Refunded', 'Incomplete PayPal', 'Archived', 'Incomplete Amazon', 'Banned', 'Limit Reached' ) AND o.fulfillment_status = 'Ready'";
            $returnedSql  = "SELECT COUNT(*) FROM public.order o WHERE o.campaign_id = $campaign->id AND o.status NOT IN ( 'Declined', 'Canceled', 'Incomplete PayPal', 'Archived', 'Incomplete Amazon', 'Banned', 'Limit Reached' ) AND 'Returned' IN (o.fulfillment_status, o.status)";
            $shippedSql   = "SELECT COUNT(*) FROM public.order o WHERE o.campaign_id = $campaign->id AND o.status NOT IN ( 'Declined', 'Canceled', 'Returned','Refunded', 'Incomplete PayPal', 'Archived', 'Incomplete Amazon', 'Banned', 'Limit Reached' ) AND ((o.fulfillment_status = 'Processed' AND o.status = 'Shipped') OR  (o.fulfillment_status = 'Shipped'))";
            global $emagid;
            $db = $emagid->getDb();
            $total = $db->getResults($totalSql);
            $campaign->total = $total[0]['count'];
            $fulfilled = $db->getResults($fulfilledSql);
            $campaign->fulfilled = $fulfilled[0]['count'];
            $pending = $db->getResults($pendingSql);
            $campaign->pending = $pending[0]['count'];
            $returned = $db->getResults($returnedSql);
            $campaign->returned = $returned[0]['count'];
            $shipped = $db->getResults($shippedSql);
            $campaign->shipped = $shipped[0]['count'];
        }
//        $this->_viewData->orderProduct = \Model\Order_Product::getList(['where'=>"active = '1'"]);
//        $this->_viewData->orderProduct1 = \Model\Order_Product::getCount(['where'=>"product_id = '1'"]);
//        $this->_viewData->orderProduct2 = \Model\Order_Product::getCount(['where'=>"product_id = '2'"]);
        

        // var_dump($this->_viewData->orderProducts);
        // exit;
        $this->_viewData->page_title = 'Fulfillment';
        $this->loadView($this->_viewData);
    }

    public function campaign(Array $arr = []){
        $campaign_id = isset($arr['id'])?$arr['id']:1;
        $this->_viewData->orders = \Model\Order::getList(['sql'=>"SELECT * FROM public.order WHERE id IN (SELECT op.order_id FROM order_products op, campaign_product cp WHERE op.product_id = cp.product_id AND cp.campaign_id = $campaign_id) ORDER BY id DESC LIMIT 20"]);
        $this->loadView($this->_viewData);
    }

    public function export(Array $arr = []){

        function outputCSV($data) {
            $output = fopen("php://output", "w");
            foreach ($data as $row)
                fputcsv($output, $row); // here you can change delimiter/enclosure
            fclose($output);
        }

        ini_set('display_errors',1);
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=OrdersToFulfill.csv');

        $sql = "SELECT 
        o.id AS \"OrderNumber\", 
        o.insert_time - interval '4 hours' AS \"OrderDate\",
        CASE WHEN ship_country IN ('US','United States') THEN 'UPS Ground' ELSE 'UPS Expedited Worldwide' END AS \"RequestedShipping\",
        NULL AS \"ShippingAmount\",
        NULL AS \"DiscountAmount\",
        NULL AS \"OrderTotalAmount\",
        p.sku AS \"ItemSKU\",
        p.name || ' (' || p.color || ')' AS \"ItemName\",
        op.quantity AS \"ItemQty\",
        NULL AS \"ItemPrice\",
        o.ship_first_name || ' ' || o.ship_last_name AS \"ShipAddressName\",
        o.ship_address AS \"ShipAddress1\",
        o.ship_address2 AS \"ShipAddress2\",
        o.ship_city AS \"ShipAddressCity\",
        o.ship_state AS \"ShipAddressState\",
        o.ship_country AS \"ShipAddressCountry\",
        o.ship_zip AS \"ShipAddressPostal\",
        o.phone AS \"ShipAddressPhone\",
        o.email AS \"ShipAddressEmail\",
        NULL AS \"BillAddressName\",
        NULL AS \"BillAddress1\",
        NULL AS \"BillAddress2\",
        NULL AS \"BillAddressCity\",
        NULL AS \"BillAddressState\",
        NULL AS \"BillAddressCountry\",
        NULL AS \"BillAddressPostal\",
        NULL AS \"BillAddressPhone\"
        FROM public.order o, order_products op, product p 
        WHERE
        o.id IN (266499,250245,251517,271283,259240,275391,282038,258490,300424,269343,292487,294477,249884,302095,260532,308147,350814,244315,278646,255924,274533,284635,360421,341208,354300,242485,353149,303971,276112,275483,261484,321018,253108,288470,293026,274683,260809,262665,261005,295765,304361,274451,296340,257795,290912,337633,242969,241380,294188,247952,298700,289079,353480,247905,298967,250503,269959,347942,297050,286288,288291,234694,310942,252338,312629) 
        AND o.campaign_id = 6 
--        AND o.fulfillment_status = 'Ready'
        AND op.active = 1
        AND o.active = 1
        AND p.active = 1
        AND o.id = op.order_id
        AND p.id = op.product_id 
--        AND ((o.ship_country NOT IN ('US','United States') AND (TRIM(o.phone) != '' OR o.phone IS NOT NULL)) OR ship_country IN ('US','United States'))";

        $header = [ "OrderNumber",
                "OrderDate",
                "RequestedShipping",
                "ShippingAmount",
                "DiscountAmount",
                "OrderTotalAmount",
                "ItemSKU",
                "ItemName",
                "ItemQty",
                "ItemPrice",
                "ShipAddressName",
                "ShipAddress1",
                "ShipAddress2",
                "ShipAddressCity",
                "ShipAddressState",
                "ShipAddressCountry",
                "ShipAddressPostal",
                "ShipAddressPhone",
                "ShipAddressEmail",
                "BillAddressName",
                "BillAddress1",
                "BillAddress2",
                "BillAddressCity",
                "BillAddressState",
                "BillAddressCountry",
                "BillAddressPostal",
                "BillAddressPhone"];
        $list = [$header];
        global $emagid;
        $db = $emagid->getDb();
        $results = $db->getResults($sql);
        $list = array_merge($list,$results);
        outputCSV($list);
    }
}