<?php

class email_logsController extends adminController {

    function __construct(){
        parent::__construct("Mail_Log");
    }

    function index(Array $params = []){
        $mailBag = new \Email\MailBag();
        $mailBag->setQuery(['sender'=>'kaws@emagid.com']);
            $mailBag->retrieveEmails();
        $this->_viewData->emails = $mailBag->getResponseEmails();

        parent::index($params);
    }

    function update(Array $params = []){
        $mail_id = $params['id'];
        $mailBag = new \Email\MailBag();
        $this->_viewData->mail_log = \Model\Mail_Log::wrap($mailBag->retrieveEmailStats($mail_id));
        $this->loadView($this->_viewData);
    }

    function sync(Array $params = []){
        $mailBag = new \Email\MailBag();
        $mailBag->setQuery(['sender'=>'kaws@emagid.com']);
        $mailBag->retrieveEmails();
        var_dump($mailBag->getResponseEmails());
    }
}