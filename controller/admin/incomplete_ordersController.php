<?php

use Emagid\Html\Form;
use Emagid\Core\Membership;

//require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class incomplete_ordersController extends adminController
{

    function __construct()
    {
        parent::__construct("Order");
    }

    public function index(Array $params = [])
    {

        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->page_title = "Manage Incomplete Orders";
        $params['queryOptions']['where'][] = '(wholesale_id is null or wholesale_id = 0)';
        //$order_by=$_GET['order'];
        //$limit=$_GET['limit'];
        if (Empty($_GET['sort']) || Empty($_GET['sort_param'])) {
            $params['queryOptions']['orderBy'] = "insert_time DESC";
        } else {
            $sort_by = $_GET['sort'];
            $sort_param = $_GET['sort_param'];
            $params['queryOptions']['orderBy'] = $sort_by . ' ' . $sort_param;
        }
        if (!Empty($_GET['status_show'])) {
            $st = explode('%2c', $_GET['status_show']);
            $where = "status in (";
            $it = 0;
            foreach($st as $queryString) {
                $where .= "'".$queryString."'";
                if(++$it<count($st)){
                    $where .= ',';
                }
            }
            $where .= ")";
            $params['queryOptions']['where'][] = $where;
        }

        $params['queryOptions']['limit'] = 10;
        if (!Empty($_GET['how_many']))
        {
            $params['queryOptions']['page_size'] = $_GET['how_many'];
        }
        else{
            $params['queryOptions']['page_size'] = 10;
        }
        if(isset($_GET['filter'])){
            switch($_GET['filter']){
                case 'sto':
                    $params['queryOptions']['where'][] = 'in_store = 1';break;
                case 'inv':
                    $params['queryOptions']['where'][] = 'id in (select order_id from invoice)';break;
                case 'onl':
                    $params['queryOptions']['where'][] = '(in_store != 1 or in_store is null) and id not in (select order_id from invoice)';break;
            }
        }
        $params['queryOptions']['where'][] = "(status in ('Declined', 'Canceled', 'Incomplete PayPal'))";
        if(isset($_GET['sf'])){
            switch($_GET['sf']){
                case 'inc':
                    $params['queryOptions']['where'][] = "(status in ('Declined','Canceled','Refunded','Incomplete PayPal'))";break;
                case 'all':default:break;
            }
        } else {
//            $params['queryOptions']['where'][] = "(status in ('New','Paid','Shipped','Complete','Delivered','Processed'))";
        }
        $this->_viewData->start = '';
        $this->_viewData->end = '';
        if(isset($_GET['t'])){
            $get = explode(',',urldecode($_GET['t']));
            $start = date('Y-m-d H:i:s',$get[0]); $end = date('Y-m-d H:i:s',$get[1]);
            $this->_viewData->start = $start;
            $this->_viewData->end = $end;
            if(isset($params['queryOptions']['where'])){
                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
            } else {
                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
            }
        }
        if(isset($params['queryOptions']['where'])){
            $params['queryOptions']['where'] = implode(' and ',$params['queryOptions']['where']);
        }
        parent::index($params);
    }

    public function beforeLoadIndexView()
    {
        $this->_viewData->products = [];
        foreach ($this->_viewData->orders as $order) {
            $order->insert_time = new DateTime($order->insert_time);
            $order->insert_time = $order->insert_time->format('m-d-Y H:i:s');
            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);
            $this->_viewData->products[$order->id] = [];
            foreach ($order_products as $order_product) {
                $product = \Model\Product::getItem($order_product->product_id);
                $product->brand_name = \Model\Brand::getItem($product->brand)->name;
                $this->_viewData->products[$order->id][] = $product;
            }
        }
    }

    public function beforeLoadUpdateView()
    {
        if (is_null($this->_viewData->order->viewed) || !$this->_viewData->order->viewed) {
            $this->_viewData->order->viewed = true;
            $this->_viewData->order->save();
        }

        $this->_viewData->order_status = \Model\Order::$status;

        $shipping_methods = \Model\Shipping_Method::getList(['orderBy' => 'shipping_method.id']);
        $this->_viewData->shipping_methods = [];
        foreach ($shipping_methods as $shipping_method) {
            $this->_viewData->shipping_methods[$shipping_method->id] = $shipping_method->name;
        }

        $this->_viewData->order_products = \Model\Order_Product::getList(['where' => "order_id=" . $this->_viewData->order->id]);
        foreach ($this->_viewData->order_products as $order_product) {
            if($order_product->product_id > 0){
                $order_product->product = \Model\Product::getItem($order_product->product_id);
                $order_product->type = 'product';
            } else {
                $misc = json_decode($order_product->details,true);
                if(isset($misc['misc_name'])){
                    $miscProduct = new stdClass();
                    $miscProduct->id = '';
                    $miscProduct->name = $misc['misc_name'];
                    $miscProduct->featured_image = '';
                    $miscProduct->slug = '';
                    $order_product->product = $miscProduct;
                    $order_product->type = 'misc';
                } else if(isset($misc['gift_card_id'])){
                    $giftCard = \Model\Gift_Card::getItem($misc['gift_card_id']);
                    $giftCard->featured_image = $giftCard->image;
                    $order_product->product = $giftCard;
                    $order_product->type = 'gift card';
                }
            }
        }

        if (is_null($this->_viewData->order->user_id)||!($this->_viewData->order->user_id)) {
            $this->_viewData->order->user = null;
        } else {
            $this->_viewData->order->user = \Model\User::getItem($this->_viewData->order->user_id);
        }

        if (is_null($this->_viewData->order->coupon_code)) {
            $this->_viewData->coupon = null;
            $this->_viewData->savings = '0';
        } else {
            $this->_viewData->coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $this->_viewData->order->coupon_code . "'"]);
            if ($this->_viewData->order->coupon_type == 1) {//$
                $this->_viewData->savings = $this->_viewData->order->coupon_amount;
            } else if ($this->_viewData->order->coupon_type == 2) {//%
                $this->_viewData->savings = $this->_viewData->order->subtotal * $this->_viewData->order->coupon_amount / 100;
            }
        }

        if (!is_null($this->_viewData->order->cc_number)) {
            $this->_viewData->order->cc_number = '****' . substr($this->_viewData->order->cc_number, -4);
        }

        $settings = new \Signifyd\Core\SignifydSettings();
        $settings->apiKey = SIGNIFY_API_KEY;

        $apiInstance = new \Signifyd\Core\SignifydAPI($settings);
        $case = $apiInstance->getCase($this->_viewData->order->case_id);
        $this->_viewData->order->orderFraudStatus = $case->reviewDisposition;
    }

    public function mail(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

        $mail = \Model\Mail_Log::getItem($params['id']);
        echo $mail->text;
        $this->template = FALSE;


    }

    public function print_packing_slip(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

        $this->slip_pack($id);
        $this->template = FALSE;


    }


     public function upd_fraud(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';

         $post_array = array(
                                'ApiLogin' => 'YwZzFUKX66lx',
                                'ApiKey' => '5rhsxiIkxDVvgiULV1lTxltcfeamlwUr',
                                'Action' => 'getOrderStatus',
                                'OrderNumber' => $id
                            );

// Convert post array into a query string
                            $post_query = http_build_query($post_array);

// Do the POST
                            $ch = curl_init('https://www.eye4fraud.com/api/');
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec($ch);
                            curl_close($ch);
                            preg_match_all('~<value>(.*)</value>~', $response, $out_status);

                                $order = \Model\Order::getItem($id);
                                $order->fraud_status =  $out_status[0][2];
                                $order->save();
                                redirect($_SERVER["HTTP_REFERER"]);
        $this->template = FALSE;


    }

    public function getVariations(){
        $product_id = isset($_GET['product_id']) ? $_GET['product_id']: null;
        if($product_id){
            $product = \Model\Product::getItem($product_id);
            $colors = $product->getColors();
            $sizes = $product->getSizes();
            $colorHtml = $sizeHtml ='<option value="0">None</option>';
            $price = $product->getPrice($colors[0]->id);
            foreach($colors as $color){
                $colorHtml .= '<option value="'.$color->id.'">'.$color->name.'</option>';
            }
            foreach($sizes as $size){
                $sizeHtml .= '<option value="'.$size->id.'">'.$size->us_size.'</option>';
            }
            echo json_encode(['status'=>"success", 'colorHtml'=>$colorHtml, 'sizeHtml'=>$sizeHtml, 'subtotal'=>$price]);
        }
    }

    public function setColorVariant(){
        $product_id = isset($_GET['product_id']) ? $_GET['product_id']: null;
        $color_id = isset($_GET['color_id']) ? $_GET['color_id']: null;
        if($product_id && $color_id){
            $product = \Model\Product::getItem($product_id);
            echo json_encode(['status'=>"success", 'price'=>$product->getPrice($color_id)]);
        } else {
            echo json_encode(['status'=>"failed"]);
        }
    }

    function export(Array $params = []){
        $this->template = false;
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=orders-'.\Carbon\Carbon::now()->format('Y-m-d').'.csv');
        $output = fopen('php://output', 'w');
        $t = ['Id','DateTime','Bill Name','Ship Name','Bill Zip','Ship Zip','Products','Email','Status','Payment Status','Total','Coupon','Purchase Type'];
        fputcsv($output, $t);
        $orders = \Model\Order::getList(['where'=>'status in (\'New\',\'Paid\',\'Shipped\',\'Complete\',\'Delivered\',\'Processed\')']);
        foreach($orders as $o) {
            $order_products = $o->getOrderProducts();
            if($order_products) {
                foreach ($order_products as $op) {
                    if ($o->getInvoice()) {
                        $payType = 'Invoice';
                    } else if ($o->in_store) {
                        $payType = 'In Store';
                    } else {
                        $payType = 'Online';
                    }
                    $row = [$o->id, date('M j, Y h:i:sa', strtotime($o->insert_time)), $o->billName(), $o->shipName(), $o->bill_zip, $o->ship_zip, $op->getProductName(), $o->email, $o->status, $o->payment_status, '$' . number_format($o->total, 2), $o->coupon_code, $payType];
                    fputcsv($output, $row);
                }
            }
        }
    }

    public function getShippingCost(){
        $country = $_GET['country'];
        $shipping = \Model\Shipping_Method::getItem(null,['where'=>"country = '{$country}'"]);
        if($shipping){
            echo json_encode(['status'=>'success', 'shipping'=>json_decode($shipping->cost,true)[0]]);
        } else {
            echo json_encode(['status'=>'failed', 'shipping'=>80]);
        }
    }

    public function create_order(Array $arr = []){
        parent::update($arr);
    }

    //Admin checkout flow -- because we're crazy
    public function create_order_post(){
        //Set Subtotal
        $subtotal = 0;
        for($i = 0; $i < count($_POST['product_id']); $i++){
            if($_POST['product_id'][$i] > 0 && $_POST['custom_price'][$i] == ''){
                $product = \Model\Product::getItem($_POST['product_id'][$i]);
                $subtotal += $product->getPrice($_POST['color'][$i]);
            } else if($_POST['custom_price'] > 0){
                $subtotal += $_POST['custom_price'][$i];
            }
        }
        $_POST['subtotal'] = $subtotal;
        $calculatedTotal = $subtotal;

        if(isset($_POST['gift_card']) && ($gc = \Model\Gift_Card::validateGiftCard($_POST['gift_card']))){
            if($gc->getRemainingAmount() > $subtotal){
                $_POST['gift_card_amount'] = $gift_card_amount = $subtotal;
                $_POST['payment_method'] = 5;
                $calculatedTotal = 0;
            } else {
                $_POST['gift_card_amount'] = $gift_card_amount = round($gc->getRemainingAmount(),2);
                $calculatedTotal -= $gift_card_amount;
            }
        }
        //Set Taxes
//        if((isset($_POST['tax-free']) && $_POST['tax-free']) || $_POST['ship_state'] != 'NY'){
        if((isset($_POST['tax-free']) && $_POST['tax-free'])){
            $_POST['tax_rate'] = 0;
            $_POST['tax'] = $tax = 0;
        } else {
            $_POST['tax_rate'] = 8.875;
            $_POST['tax'] = $tax = round($calculatedTotal * .08875,2);
        }
        $calculatedTotal += $_POST['tax'];

        //Set shipping cost
        if($_POST['ship_country'] == 'United States'){
            $shipping_cost = 0;
        } else if($shipping = \Model\Shipping_Method::getItem(null,['where'=>"country = '{$_POST['ship_country']}'"])){
            $shipping_cost = json_decode($shipping->cost,true)[0];
        } else {
            $shipping_cost = 80;
        }
        $calculatedTotal += $shipping_cost;
        $_POST['shipping_cost'] = round($shipping_cost,2);

        //Set discount -- N/A
        $discount = 0;
        $calculatedTotal -= $discount;
//        $coupon = \Model\Coupon::getItem(null,['where'=>"code = '{$_POST['coupon_code']}'"]);
//        $_POST['coupon_type'] = $coupon->discount_type;
//        $_POST['coupon_amount'] = $coupon->discount_amount;
//        $discount = $coupon->applyCoupon($subtotal);

        //Set guest_id
        $_POST['guest_id'] = session_id();

        $_POST['status'] = 'New';
        $_POST['ref_num'] = generateToken();

        //Set total
        $_POST['total'] = $calculatedTotal;

        $_POST['insert_time'] = date('Y-m-d H:i:s.u');
        $order = \Model\Order::loadFromPost();
        $order->product_id=$order->color=$order->size=$order->misc_name=$order->custom_price='';

        if($order->save()){
            if(isset($_GET['inv']) && $_GET['inv'] && ($invoice = \Model\Invoice::getByRef($_GET['inv']))){
                $invoice->order_id = $order->id;
                $invoice->save();
            }
            $html = '';
            for($i = 0; $i < count($_POST['product_id']); $i++){
                $orderProduct = new \Model\Order_Product();
                $orderProduct->order_id = $order->id;
                $orderProduct->product_id = $_POST['product_id'][$i];
                $orderProduct->quantity = 1; //add quantity
                if($_POST['product_id'][$i] > 0){ //id is product
                    $product = \Model\Product::getItem($_POST['product_id'][$i]);
                    $product->total_sales += 1;
                    $product->save();
                    $unit_price = $_POST['custom_price'][$i] ? : $product->price($_POST['color'][$i]);
                    $product_image = $product->featuredImage(null,$_POST['color'][$i]);
                    $product_name = $product->name;
                    $product_price = $unit_price * $orderProduct->quantity;
                    $product_color = \Model\Color::getItem($_POST['color'][$i])->name;
                    $product_size = \Model\Size::getItem($_POST['size'][$i]);

                    $orderProduct->unit_price = $unit_price;
                    $orderProduct->details = json_encode(['color'=>$_POST['color'][$i], 'size'=>$_POST['size'][$i]]);
                    $orderProduct->save();
                } else { //id is miscellaneous charge
                    $unit_price = $_POST['custom_price'][$i];
                    $product_image = '';
                    $product_name = $_POST['misc_name'][$i];
                    $product_price = $unit_price * $orderProduct->quantity;
                    $product_color = '';
                    $product_size = new stdClass(); $product_size->us_size = ''; $product_size->eur_size = '';

                    $orderProduct->unit_price = $unit_price;
                    $orderProduct->details = json_encode(['misc_name'=>$_POST['misc_name'][$i]]);
                }
                $orderProduct->save();

                $html .=
                    '<tr style="height:75.0pt">
                            <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p class="MsoNormal"><img width="100px!important;" src="https://djinyc.com'.UPLOAD_URL.'products/'.$product_image.'"></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product_name.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($product_price,2).'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product_color.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">US: '.$product_size->us_size.'<br>EU: '.$product_size->eur_size.'</span></p>
                            </td>
                        </tr>';
            }

            if($order->payment_method == 1) {
                $expiry = str_pad($order->cc_expiration_month, 2, 0) . substr($order->cc_expiration_year, -2);
                $firstData = new Payment\FirstData(PAYEEZY_ID, PAYEEZY_PASSWORD);
                $firstData->setAmount($order->total);
                $firstData->setCreditCardNumber($order->cc_number);
                $firstData->setCreditCardName($order->card_name);
                $firstData->setCreditCardType($_POST['cc_type']);
                $firstData->setCreditCardVerification($order->ccv);
                $firstData->setCreditCardExpiration($expiry);
                $firstData->setCurrency('USD');
                $firstData->setReferenceNumber($order->id);
                if ($order->user_id) {
                    $firstData->setCustomerReferenceNumber($order->user_id);
                }
                $firstData->process();
                if ($firstData->isSuccess()) {
                    $order->status = \Model\Order::$status[1];
                    $order->save();

                    $email = new \Email\MailMaster();
                    $mergeFields = [
                        'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
                        'DATE' => date('Y-m-d h:i:s'),
                        'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                        'SHIPPING' => $order->getShippingAddr(),
                        'BILLING' => $order->getBillingAddr(),
                        'ITEMS' => $html,
                        'SUBTOTAL' => number_format($order->subtotal, 2),
                        'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                        'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                        'TAXFEE' => number_format($order->tax, 2),
                        'TOTAL' => number_format($order->total, 2)
                    ];
                    $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();

                    if($order->gift_card && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card)) && $order->gift_card_amount){
                        $giftCard->removeAmount($order->gift_card_amount);
                    }
                    redirect('/admin/orders/update/' . $order->id);
                } else {
                    $order->status = \Model\Order::$status[2];
                    $order->error_message = $firstData->getErrorMessage();
                    $order->save();
                    $n = new \Notification\ErrorHandler($firstData->getErrorMessage());
                    $_SESSION["notification"] = serialize($n);
                    redirect('/admin/orders/create_order');
                }
            } else if($order->payment_method == 3){
                $order->status = \Model\Order::$status[1];
                $order->save();

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
                    'DATE' => date('Y-m-d h:i:s'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format($order->tax, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];

                if($order->gift_card && $order->gift_card_amount && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card))){
                    $giftCard->removeAmount($order->gift_card_amount);
                }
                
                $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();
                redirect('/admin/orders/update/' . $order->id);
            } else if($order->payment_method == 5){
                $order->status = \Model\Order::$status[1];
                $order->save();

                if($order->gift_card && $order->gift_card_amount && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card))){
                    $giftCard->removeAmount($order->gift_card_amount);
                }

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
                    'DATE' => date('Y-m-d h:i:s'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format($order->tax, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];
                $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();
                redirect('/admin/orders/update/' . $order->id);
            } else {
                $order->status = \Model\Order::$status[2];
                $order->save();
                $n = new \Notification\ErrorHandler('Invalid payment method ('.\Model\Order::$payment_method[$order->payment_method].'). Please try again.');
                $_SESSION["notification"] = serialize($n);
                redirect('/admin/orders/create_order');
            }
        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION["notification"] = serialize($n);
            redirect('/admin/orders/create_orders');
        }
    }

    public function update_post()
    {
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {

            $order = \Model\Order::getItem($_POST['id']);

            /*if ($order->status != $_POST['status'] && $_POST['status'] == \Model\Order::$status[3] && trim($_POST['tracking_number']) != ""){
                $this->sendShippedEmail($order, $_POST['tracking_number']);
            }*/

            if (isset($_POST['status'])) {
                $order->status = $_POST['status'];
                if ($_POST['status'] !== $_POST['old_status']) {
                    $log_status = new \Model\Log_Status();
                    $log_status->order_id = $_POST['id'];
                    $log_status->status = $_POST['status'];
                    $log_status->admin_id = \Emagid\Core\Membership::userId();
                    $log_status->save();
                }


            }

            if(isset($_POST['fulfilment'])){
                foreach ($_POST['fulfilment'] as $orderProductId=>$receiveItemId){
                    $orderProduct = \Model\Order_Product::getItem($orderProductId);
                    $orderProduct->receive_item_id = $receiveItemId;
                    $orderProduct->save();
                }
            }

            $order->tracking_number = $_POST['tracking_number'];
            if (isset($_POST['cc_number'])) {
                if ('****' . substr($order->cc_number, -4) != $_POST['cc_number']) {
                    $order->cc_number = $_POST['cc_number'];
                }
            }
            if (isset($_POST['cc_expiration_month'])) {
                $order->cc_expiration_month = $_POST['cc_expiration_month'];
            }
            if (isset($_POST['cc_expiration_year'])) {
                $order->cc_expiration_year = $_POST['cc_expiration_year'];
            }

            if (isset($_POST['cc_ccv'])) {
                $order->cc_ccv = $_POST['cc_ccv'];
            }


            if (isset($_POST['payment_method'])) {
                $order->payment_method = $_POST['payment_method'];
            }

            $order->bill_first_name = $_POST['bill_first_name'];
            $order->bill_last_name = $_POST['bill_last_name'];
            $order->bill_address = $_POST['bill_address'];
            $order->bill_address2 = $_POST['bill_address2'];


            if (isset($_POST['bill_state'])) {
                $order->bill_state = $_POST['bill_state'];
            }
            if (isset($_POST['bill_country'])) {
                $order->bill_country = $_POST['bill_country'];
            }


            $order->bill_city = $_POST['bill_city'];


            $order->bill_zip = $_POST['bill_zip'];

            $order->ship_first_name = $_POST['ship_first_name'];
            $order->ship_last_name = $_POST['ship_last_name'];
            $order->ship_address = $_POST['ship_address'];
            $order->ship_address2 = $_POST['ship_address2'];


            if (isset($_POST['ship_state'])) {
                $order->ship_state = $_POST['ship_state'];
            }
            if (isset($_POST['ship_country'])) {
                $order->ship_country = $_POST['ship_country'];
            }


            $order->ship_zip = $_POST['ship_zip'];
            $order->note = $_POST['note'];

            $order->phone = $_POST['phone'];
//            $subtotal = $order->subtotal;
//            if (!is_null($order->coupon_code)) {
//                if ($order->coupon_type == 1) {
//                    $subtotal = $subtotal - $order->coupon_amount;
//                } else if ($order->coupon_type == 2) {
//                    $subtotal = $subtotal * (1 - ($order->coupon_amount / 100));
//                }
//            }

            /*$logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());  
            $orderchange = new \Model\Orderchange();  
            $orderchange->order_id = $_POST['id'];
            $orderchange->user_id =  $logged_admin->id;
            $orderchange->save() ; */


            if (!empty($_POST['mail'])) {
                $email = new \Emagid\Email();
                global $emagid;
                $emagid->email->from->email = 'orders@djinyc.com'; //TODO st-dev get email
                $mail_log = new \Model\Mail_Log();
                $mail_log->from_ = "orders@djinyc.com"; //TODO st-dev get email

                if (is_null($order->user_id)) {
                    $email->addTo($order->email);
                    $mail_log->to_ = $order->email;
                } else {
                    $email->addTo(\Model\User::getItem($order->user_id)->email);
                    $mail_log->to_ = \Model\User::getItem($order->user_id)->email;
                }
                $email->addBcc("orders@djinyc.com"); //TODO st-dev get email
                if ($_POST['status'] !== $_POST['old_status']) {
                    $subject_array = Array();
                    foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
                        $product = \Model\Product::getItem($order_product->product_id);
                        $brand = \Model\Brand::getItem($product->brand);
                        $subject_array[] = $brand->name;
                        $subject_array[] = $product->mpn;

                        //$subject_array[] = $order_product->id;
                    }
                    if ($_POST['status'] == "Shipped") {

                        $email->subject('Your order has shipped! ' . implode(" ", $subject_array) . " DJI Order #" . $order->id);
                        $mail_log->subject = 'Your order has shipped! ' . implode(" ", $subject_array) . "DJI Order #" . $order->id;
                    } else {
                        $email->subject('Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " DJI Order #" . $order->id);
                        $mail_log->subject = 'Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " DJI Order #" . $order->id;
                    }
                }
                $subject = $_POST['subject'];
                $mailtitle = $_POST['mailtitle'];
                $status = $_POST['status'];
                $text = $_POST['mail'];
                $email->subject($subject);
                $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="https://djinyc.com/content/frontend/img/logo.png" alt="DJI"></center>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">' . $mailtitle . '</span></b> </p>
                        </td>
                        <td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</a></span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Client:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:green">' . $order->ship_first_name . ' ' . $order->ship_last_name . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';


                $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%">
                <tbody>
                     <p align="justify">' . $text . '</p>
                </tbody>
                </table>';

                $mail_log->text = $email->body;

                $mail_log->order_id = $order->id;
                $mail_log->save();
                // echo $email->body;
                // echo $email->subject;
//                $email->send();

            }

            if(isset($_POST['tracking_email']) && $_POST['tracking_email'] == 'on'){
                $html = '';
                $html .= '<tr>';
                foreach ($order->getOrderProducts() as $array) {
                    $product = \Model\Product::getItem($array->product_id);
                    $color = json_decode($array->variation, true)['color'];
                    $size = json_decode($array->variation, true)['size'];
                    $html .= '
                        <td width="192" style="width: 192px;" valign="top">
                            <table width="192" border="0" cellpadding="0" cellspacing="0" align="center" class="force-row" style="width: 192px;"><tr>
                                    <td class="col" valign="top" style="padding-left:12px;padding-right:12px;padding-top:18px;padding-bottom:12px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="img-wrapper">
                                            <tr>
                                                <td style="padding-bottom:18px"><a href="https://djinyc.com/products/'.$product->slug.'"><img src="'.$product->featuredImage() . '" border="0" alt="The Albatross flew across the ocean" width="168" height="110" hspace="0" vspace="0" style="max-width:100%; " class="image"></a></td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0"><tr>
                                                <td class="subtitle" style="font-family:Helvetica, Arial, sans-serif;font-size:16px;line-height:22px;font-weight:600;color:#24262b;padding-bottom:6px">
                                                    <a href="https://djinyc.com/products/'.$product->slug.'">' . $product->name . '</a>
                                                </td></tr>
                                        </table>
                                        <div class="col-copy" style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;text-align:left;color:#24262b">';
                    if ($product->basePrice() != $product->getPrice($color, $size)) {
                        $html .= '<a href="https://djinyc.com/products/'.$product->slug.'"><span style="text-decoration: line-through;color:rgba(36, 38, 43, 0.5);">$' . number_format($product->basePrice() * $array->quantity, 2) . '</span></a> ';
                    }
                    $html .= '<a href="https://djinyc.com/products/'.$product->slug.'"><span style="color:#24262b;">$' . number_format($product->getPrice($color, $size) * $array->quantity, 2) . '</span></a>
                                            </div>
                                            <br>
                                        </td>
                                    </tr>
                                </table>
                            </td>';
                }
                $html .= '</tr>';
                $email = new \Email\MailMaster();
                $mergeFields = [
                    'NAME'=>$order->billName(),
                    'TRACKING_NUMBER'=>$order->tracking_number,
                    'ITEMS'=>$html
                ];
                $email->setTo(['email'=>$order->email,'name'=>$order->shipName(), 'type'=>'to'])->setMergeTags($mergeFields)->setTemplate('dji-shipping')->send();
            }

           /* if (isset($_POST['shipping_method'])) {
                if ($order->shipping_method != $_POST['shipping_method']) {
                    $order->shipping_method = \Model\Shipping_Method::getItem($_POST['shipping_method']);
                    $costs = json_decode($order->shipping_method->cost);
                    $order->shipping_method->cost = 0;
                    $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
                    foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                        if (is_null($range) || trim($range) == '') {
                            $range = 0;
                        }
                        if ($subtotal >= $range && isset($costs[$key])) {
                            $order->shipping_cost = $costs[$key];
                        }
                    }
                    $order->shipping_method = $order->shipping_method->id;
                }
            }*/
//           if(isset($_POST['shipping_cost'])){
//               if($order->shipping_cost != $_POST['shipping_cost']){
//                   $order->shipping_cost = $_POST['shipping_cost'];
//               }
//           }

//            if (isset($_POST['ship_state'])) {
//                $shippingState = preg_replace('/\s+/', '', strtolower($_POST['ship_state']));
//                if ($shippingState == 'ny' ||$shippingState=='newyork') {
//                    $order->tax_rate = 8.875;
//                    $order->tax = $subtotal * ($order->tax_rate / 100);
//                } else {
//                    $order->tax_rate = null;
//                    $order->tax = null;
//                }
//            }

//            $order->total = $subtotal + $order->shipping_cost + $order->tax - $order->coupon_amount;

            if ($order->save()) {
                $n = new \Notification\MessageHandler('Order saved.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($order->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . $this->_content . '/update/' . $order->id);
            }

        }
        redirect(ADMIN_URL . $_POST['redirectTo']);
    }

    public function fulfill_items(Array $arr = []){
        $this->_viewData->page_title = 'Manage Fulfillment';
        $this->_viewData->order_producuts = \Model\Order_Product::getList(['where'=>"order_id = {$arr['id']}"]);
        parent::update($arr);
    }

    public function fulfill_items_post(){
        if(isset($_POST['fulfillment'])){
            foreach ($_POST['fulfillment'] as $op_key=>$value){
                foreach ($value as $val){
                    if($val > 0){
                        $ri = \Model\Receive_Item::getItem($val);
                        $ri->order_product_id = $op_key;
                        $ri->save();
                    }
                }
            }
        }
        redirect('/admin/orders/update/'.$_POST['id']);
    }

    public function mass_email(Array $arr = []){
        $this->_viewData->orders = isset($_GET['orders']) && $_GET['orders'] ? \Model\Order::getList(['where'=>'id in ('.urldecode($_GET['orders']).')','orderBy'=>'id desc']): [];
        parent::update($arr);
    }

    public function mass_email_post(){
        $orders = \Model\Order::getList(['where'=>'id in ('.urldecode($_GET['orders']).')']);
        foreach($orders as $order){
            $body = sprintf(\Model\Order::$mass_email_template,$order->shipName(),\Model\Order::$mass_email[$_POST['email_type']]);
            $email = new \Email\MailMaster();
            $mergeTags = ['BODY'=>$body];
            $email->setTo(['email'=>$order->email,'name'=>$order->shipName(),'type'=>'to'])->setMergeTags($mergeTags)->setTemplate('modern-vice-mass-email');
            $email->send();

            if($order->email_log){
                $log = json_decode($order->email_log,true);
            } else {
                $log = [];
            }
            $log[] = ['type'=>$_POST['email_type'],'datetime'=>date('Y-m-d H:i:s'),'email_text'=>\Model\Order::$mass_email[$_POST['email_type']]];
            $order->email_log = json_encode($log);
            $order->save();
        }
        redirect(ADMIN_URL.$_POST['redirectTo']);
    }

    public function send_progress(){
        $order = \Model\Order::getItem($_POST['order_id']);
        $orderProduct = \Model\Order_Product::getItem($_POST['order_product_id']);
        if($order && $order->email && $orderProduct && $_POST['status'] != 0){
            $productName = $orderProduct->getProductName();
            $steps = [
                1=>"Hi there! We’ve received the material components for your $productName. At this stage, we will begin cutting the material to design your shoe for the stitching process. Stay tuned for an update once your shoes have been fully stitched.",
                2=>"Hello! The upper material of your $productName has been fully stitched. Next step is to shape your shoes with our foot molds. Once your shoes have been placed on the foot mold, we’ll send you an update. Thanks for your patience as we get closer to finishing your shoes!",
                3=>"Hi there! Your $productName are on the foot mold to fully take shape. We’re adjusting the soles and placing them on the shoes so your order can be completed. Please allow 3-5 days for us to ship your shoes. Upon shipping your order, you’ll receive an email with the tracking number."];
            $html = '<tr class="tc" style="text-align: center" align="center">
                            <td class="f18" style="font-size: 18px">
                                    <p style="margin-bottom: 35px;color:#000000;">'.$steps[$_POST['status']].'</p>
                            </td>
                        </tr>';

            $email = new \Email\MailMaster();
            $mergeTags = [
                'STATUS'=>$html
            ];
            $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeTags)->setTemplate('modern-vice-wip')->send();

//            $email = new \Emagid\Email();
//            $email->addTo($order->email)
//                ->addBcc('info@modernvice.com')
//                ->addBcc('keenon@emagid.com')
//                ->addBcc('jordan@modernvice.com')
//                ->addBcc('eitan@emagid.com');
//            $email->subject('Modern Vice Order Status');
//            $email->body = $html;
//            $email->send();

            $orderProduct->status = $_POST['status'];
            $orderProduct->save();
            echo json_encode(['status'=>'success','message'=>'Email Sent']);
        } else {
            echo json_encode(['status'=>'failed','message'=>'Email failed to send. Speak with administrator.']);
        }
    }


    /*  public function update_post_status() {   
            $id = $_POST['id'];
            $status = $_POST['status'];
            $order = \Model\Order::getItem($_POST['id']);
            $order->status = $status;
            $order->save();
        }*/


    /*     private function sendShippedEmail($order, $trackingNumber){
            $email = new \Emagid\Email();
            if (is_null($order->user_id)){
                $email->addTo($order->email);
            } else {
                $email->addTo(\Model\User::getItem($order->user_id)->email);
            }
            $email->bccTo("orders@modernvice.com");
            $email->subject('Order shipped');
            $email->body = '<p><a href="www.modernvice.com"><img src="http://modernvice.com/content/frontend/img/logo.png" /></a></p>'
                          .'<p>Your modernvice order was shipped!</p>'
                          .'<p>The tracking number is: <b>'.$trackingNumber.'</b></p>'
                          .'<p>Regards,<br />modernvice Team</p>'
                          .'<p>Find us on <a href="https://www.facebook.com/pages/modernvicecom/202088860134">Facebook</a> and <a href="https://twitter.com/modernvice">Twitter</a>.</p>';
            $email->send();
        } */

    public function getGiftCardValidation(){
        $arr = ['status'=>'failed','message'=>'Invalid gift card','value'=>0];
        if(isset($_POST['gift_card']) && $_POST['gift_card'] && ($gc = \Model\Gift_Card::validateGiftCard($_POST['gift_card']))){
            $arr = ['status'=>'success','message'=>'Valid gift card','value'=>$gc->amount];
        }
        echo json_encode($arr);
    }

    public function getCcNumber()
    {
        if (isset($_POST['password']) && $_POST['password'] == 'test5343') {
            echo json_encode(\Model\Order::getItem($_POST['order_id'])->cc_number);
        } else {
            echo json_encode("false");
        }
    }

    public function unlock()
    {
        if (isset($_POST['pwd']) && $_POST['pwd'] == 'test5343') {
            echo json_encode("/admin/orders/delete/{$_POST['orderId']}");
        } else {
            echo json_encode("false");
        }
    }

    public function search()
    {
        global $emagid;
        $db = $emagid->getDb();
        $sql = "
select o.id,o.viewed,o.insert_time,o.tracking_number,o.bill_first_name||' '||o.bill_last_name as bill_name,o.status,o.payment_status,o.total,o.payment_method,p.name as products,op.details
from public.order o
left join order_products op on o.id = op.order_id and o.active = 1
left join product p on op.product_id = p.id";
        $parsed = trim(preg_replace('/\s+/',' ',strtolower(urldecode($_GET['keywords']))));
        $expl = explode(' ',$parsed);
        $impl = "'%".implode("%','%",$expl)."%'";
        $where = [];
        if(is_numeric($_GET['keywords'])){
            $where[] = "o.id = {$_GET['keywords']}";
        }
        $where[] = "lower(email) like '%$parsed%'";
        $where[] = "lower(ship_first_name)||' '||lower(ship_last_name) like all(array[$impl])";
        $where[] = "lower(bill_first_name)||' '||lower(bill_last_name) like all(array[$impl])";
        $where[] = "lower(p.name) like all(array[$impl])";
        $limit = ' limit 20';
        $orderBy = ' order by id desc';

        $sql .= ' where o.active = 1 and ('.implode(' or ',$where).')';
        $sql .= $orderBy;
//        $sql .= $limit;
        $result = $db->execute($sql);

        $arr = [];
        $id = '';
        $productList = [];
        foreach($result as $r){
            if($id != $r['id']){
                $id = $r['id'];
                $productList = [];
            }
            if($r['products']){
                $productList[] = $r['products'];
            } else if($r['details']){
                $json = json_decode($r['details'], true);
                $productList[] = $json['misc_name'] . '(Custom)';
            }
//            $obj = \Model\Order::getItem($r['id']);
//            if ($obj && $obj->getInvoice()) {
//                $style = 'C6DDFF';
//            } elseif ($obj && $obj->in_store) {
//                $style = 'ffff9e';
//            } else {
//                $style = '';
//            }
            $r['products'] = $productList;
//            $r['style'] = $style;
            $arr[$r['id']] = $r;

        }
        rsort($arr);
        echo json_encode($arr);
    }


    public function search_by_mpn()
    {
        $mpn = trim($_GET['keywords']);
        $mpn = str_replace('%20', ' ', $mpn);
        $mpn = str_replace('+', ' ', $mpn);
        $mpn = str_replace('%2F', '/', $mpn);
        $mpn = urldecode($mpn);

        $product = \Model\Product::getList(['where' => "mpn = '$mpn'"]);
        $a = Array();

        echo '[';
        foreach ($product as $pr) {
            $order_product = \Model\Order_Product::getList(['where' => 'product_id = ' . $pr->id]);


            foreach ($order_product as $op) {
                $orders = \Model\Order::getList(['where' => 'id = ' . $op->order_id]);


                foreach ($orders as $order) {

                    $a[] = '{ "id": "' . $order->id . '", 
            "insert_time": "' . $order->insert_time . '", 
            "tracking_number": "' . $order->tracking_number . '",
            "status": "' . $order->status . '",
            "bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
            "products": "' . $pr->name . '",
            "total": "' . $order->total . '"}';


                }
            }
        }

        echo implode(',', $a);
        echo "]";

    }


    public function pay(Array $params = [])
    {
        if (isset($params['id']) && is_numeric($params['id'])) {

            $order = \Model\Order::getItem($params['id']);

            if (!is_null($order)) {

                $localTransaction = $this->emagid->db->execute('SELECT ref_trans_id FROM transaction WHERE order_id = ' . $order->id);

                if (isset($localTransaction[0]) && isset($localTransaction[0]['ref_trans_id'])) {
                    $refTransId = $localTransaction[0]['ref_trans_id'];

                    $transaction = new AuthorizeNetAIM;
                    $response = $transaction->priorAuthCapture($refTransId);

                    $n = new \Notification\MessageHandler($response->response_reason_text);
                    $_SESSION["notification"] = serialize($n);
                }

                redirect(ADMIN_URL . 'orders/update/' . $order->id);

            }

        }

        redirect(ADMIN_URL . 'orders');
    }

    private function slip_pack($order)
    {
        global $emagid;
        $emagid->email->from->email = 'orders@djinyc.com';

        $email = new \Emagid\Email();

        $order = \Model\Order::getItem($order);


        $email->subject('Order packing slip!');///wtf?
        $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="http://djinyc.com/content/frontend/img/logo.png" alt="Luggage"></center>
</td>
</tr>
</tbody>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:100.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Packing slip</span></b> </p>
                        </td>
                        <td width="108" style="width:1.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:15.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Number:</span></b> </p>
                        </td>
                        <td width="118" style="width:8.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:15.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:1.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:15.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order Date:</span></b> </p>
                        </td>
                        <td width="118" style="width:8.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:15.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . date('M d, Y h:i:sa',strtotime($order->insert_time)) . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';

        /* if ($order->payment_method == 1) {
            $email->body .= 'We have received your order and we are currently processing it.<br>';
            $email->body .= 'Your credit card will not be charged until the item’s availability has been established, at which time you will receive a confirmation email.';
            $email->body .= '<p>Then, as soon as your item ships you will receive another email with the tracking information.</p>';
            $email->body .= '<p>If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p>';
        } else if ($order->payment_method == 2) {
            $email->body .= '<p>We will contact you shortly for more information. If you have any questions or concerns please do not hesitate to call us at 877.752.6919 or e-mail us at support@modernvice.com</p>';
        } */


        $shp = array($order->ship_first_name, $order->ship_last_name, $order->ship_address, $order->ship_address2,
            $order->ship_country, $order->ship_city, $order->ship_state, $order->ship_zip);


        $blng = array($order->bill_first_name, $order->bill_last_name, $order->bill_address, $order->bill_address2,
            $order->bill_country, $order->bill_city, $order->bill_state, $order->bill_zip);


        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:20.25pt">
                        <td width="50%" style="width:50.0%;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping to:</span></b> </p>
                        </td>
                        <td width="50%" style="width:50.0%;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Billing to:</span></b> </p>
                        </td>
                    </tr>
                    <tr style="height:5.25pt">
                        <td width="650" colspan="2" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
                    </tr>
                    <tr>
                        <td style="background:white;padding:0in 0in 7.5pt 15.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">
                            ' . $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
                            ' . $order->ship_city . ' ' . $order->ship_state . ' <br>
                            ' . $order->ship_zip . ' ' . $order->ship_country . '                       
                            </span></p>
                        </td>
                        <td style="background:white;padding:0in 0in 7.5pt 15.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">';
        if (count(array_diff($shp, $blng)) > 0) {
            $email->body .= $order->bill_first_name . ' ' . $order->bill_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->bill_address . ' ' . $order->bill_address2 . '<br>
                            ' . $order->bill_city . ' ' . $order->bill_state . '<br>
                            ' . $order->bill_zip . ' ' . $order->bill_country;
        } else {
            $email->body .= $order->ship_first_name . ' ' . $order->ship_last_name . '<br>
							' . $order->email . '<br>
							' . $order->phone . '<br>
                            ' . $order->ship_address . ' ' . $order->ship_address2 . '<br>
                            ' . $order->ship_city . ' ' . $order->ship_state . '<br>
                            ' . $order->ship_zip . ' ' . $order->ship_country;
        }
        $email->body .= '</p>
                        </td>
                    </tr>
                </tbody>
            </table>';

        if($order->note) {
            $email->body .= '<h4>Note: ' . $order->note . '</h4>';
        }
        $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:20.25pt">
                        <td width="255" colspan="2" style="width:191.25pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Items</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Qty</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Color</span></b> </p>
                        </td>
                        <td width="49" style="width:36.75pt;border-top:solid #414042 1.0pt;border-left:none;border-bottom:none;border-right:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:20.25pt">
                            <p class="MsoNormal" align="center" style="text-align:center"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Size</span></b> </p>
                        </td>
                         
                    </tr>
                    <tr style="height:5.25pt">
                        <td width="650" colspan="6" style="width:600pt;background:white;padding:0in 0in 0in 0in;height:5.25pt"></td>
                    </tr>';

        foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
            $variant = json_decode($order_product->details,true);
            $color = isset($variant['color']) && $variant['color'] ? \Model\Color::getItem($variant['color']) : null;
            $size = isset($variant['size']) && $variant['size'] ? \Model\Size::getItem($variant['size']) : null;

            $product = $order_product->product_id ? \Model\Product::getItem($order_product->product_id) : null;
            $productName = $product ? $product->name: (isset($variant['misc_name']) ? $variant['misc_name']: (isset($variant['gift_card_id']) ? \Model\Gift_Card::getItem($variant['gift_card_id'])->name: ''));
            $productImage = $product ? $product->featuredImage(null,$color->id) : '';
            $custom = !$product && isset($variant['misc_name']) ? '(Custom Item)': '';

            $colorName = $color ? $color->name: '';
            $sizeHtml = $size ? 'US: '.$size->us_size.' EU: '.$size->eur_size : '';

            $style = $order_product->clearance ? 'color:green': '';
            $name = $order_product->clearance ? '(CLEARANCE)': '';

            $email->body .= '<tr style="height:75.0pt">'
                . '<td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal"><img  width="400px!important;" src="https://djinyc.com/content/uploads/products/' . $productImage . '"></p></td>'
                . '<td width="100" valign="top" style="width:127.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 3.75pt 0in;height:75.0pt">
                            <p><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042;'.$style.'"> ' . $productName . ' ' . $custom . '<br>' .$name . '</span></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order_product->quantity . '</span></b></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $colorName . '</span></b></p>
                        </td>'
                . '<td width="50" valign="top" style="width:37.5pt;border:none;border-bottom:solid #414042 1.0pt;background:white;padding:0in 0in 15.0pt 0in;height:75.0pt">
                            <p align="center" style="text-align:center"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">'.$sizeHtml.'</span></b></p>
                        </td> </tr>
                ';
        }
        $email->body .= '</tbody>
            </table>
            <p class="MsoNormal">&nbsp;</p>';
        if (!is_null($order->coupon_code)) {
            $savings = ($order->coupon_type == 1) ? '$' : '';
            $savings .= number_format($order->coupon_amount, 2);
            $savings .= ($order->coupon_type == 2) ? '%' : '';
        } else {
            $savings = 0;
        }

        $show_saving = 'display:none;';

        $a = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt;padding-left: 604px;">
                <tbody>
                    <tr>
                         
                        <td style="padding:0in 0in 0in 0in">
                            <table border="0" cellspacing="0" cellpadding="0" width="200" style="width:150.0pt">
                                <tbody>
                                    <tr style="height:20.25pt">
                                        <td width="200" colspan="2" style="width:150.0pt;background:#dcddde;padding:0in 0in 0in 15.0pt;height:20.25pt">
                                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Order Summary</span></b></p>
                                        </td>
                                    </tr><tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sub Total:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->subtotal, 2) . '</span></p>
                                        </td>
                                    </tr>
                                     
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Shipping:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->shipping_cost, 2) . '</span></p>
                                        </td>
                                    </tr>
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;background:white;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Sales Tax:</span></b> </p>
                                        </td>
                                        <td width="75" style="width:56.25pt;background:white;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><span style="font-size:9.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->tax, 2) . '</span></p>
                                        </td>
                                    </tr>
                                    <tr style="height:20.25pt">
                                        <td width="125" style="width:93.75pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 3.75pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:black">Total:</span></b></p>
                                        </td>
                                        <td width="75" style="width:56.25pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 15.0pt 0in 0in;height:20.25pt">
                                            <p class="MsoNormal" align="right" style="text-align:right"><b><span style="font-size:10.5pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">$' . number_format($order->total, 2) . '</span></b></p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>';


        /*  $email->addTo($order->email);
          $email->addBcc('info@modernvice.com');
          $email->addBcc('orders@modernvice.com');
          $mail_log = new \Model\Mail_Log();
          $mail_log->to_ = $order->email;
          $mail_log->from_ = "orders@modernvice.com";
          $mail_log->subject = "Order packing slip";
          $mail_log->order_id = $order->id;
          $mail_log->text = $email->body;
          $mail_log->save();*/
        echo $email->body; //sends to the customer


    }

    function emailHead(){
        return '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
    <title>Three columns with images</title>

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }

        .ReadMsgBody {
            width: 100%;
            background-color: #ebebeb;
        }

        table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        .yshortcuts a {
            border-bottom: none !important;
        }

        @media screen and (max-width: 599px) {
            .force-row,
            .container {
                width: 100% !important;
                max-width: 100% !important;
            }
        }

        @media screen and (max-width: 400px) {
            .container-padding {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }

        .ios-footer a {
            color: #aaaaaa !important;
            text-decoration: underline;
        }

        @media screen and (max-width: 599px) {
            .col {
                width: 100% !important;
                border-top: 1px solid #eee;
                padding-bottom: 0 !important;
            }

            .cols-wrapper {
                padding-top: 18px;
            }

            .img-wrapper {
                float: right;
                max-width: 40% !important;
                height: auto !important;
                margin-left: 12px;
            }

            .subtitle {
                margin-top: 0 !important;
            }
        }

        @media screen and (max-width: 400px) {
            .cols-wrapper {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .content-wrapper {
                padding-left: 12px !important;
                padding-right: 12px !important;
            }
        }
    </style>
</head>

<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
    <tr>
        <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">
            <br>

            <!-- 600px container (white background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">

                <tr>
                    <td class="content" align="left" style="background-color:#ffffff">

                        <table class="tc" style="background: #000000; margin-left: auto; margin-right: auto; max-width: 768px; text-align: center; width: 100%;border-radius: 0px 0px 0 0;" bgcolor="#000000">
                            <tbody>
                            <tr>
                                <td>
                                    <a href="https://djinyc.com/">
                                        <img style="width:192px;border: 0; display: inline-block; margin: 20px 0; outline: none; text-decoration: none" alt="DJI Logo" src="http://djinyc.com/content/frontend/assets/img/logoTextWhite.png">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="background: #fff; border: 1px solid #ccc; border-collapse: separate; border-radius: 0px; margin-left: auto; margin-right: auto; max-width: 768px; padding: 60px 40px; width: 100%;border:1px solid #e1e1e1;" bgcolor="#fff">
                            <tbody>

                            <tr>
                                <td>
                                    <table width="600" border="0" cellpadding="0" cellspacing="0" class="force-row" style="width: 600px;">
                                        <tr>
                                            <td class="cols-wrapper" style="padding-left:12px;padding-right:12px">
                                                <table border="0" width="576" cellpadding="0" cellspacing="0" style="width: 576px;">
                                                       ';
    }

    function emailFoot(){
        return '</table>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr class="tc" style="text-align: center" align="center">
                                <td class="f18" style="font-size: 18px">
                                    <p style="margin-bottom: 35px;color:#000000;">Questions?<br> Email us at
                                        info@djinyc.com or give us a call at (212) 777-1851 to learn more! </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="tc" style="color:#9c9c9c;background: #f4f4f4; margin: 0 auto 0; max-width: 768px; text-align: center; width: 100%" bgcolor="#f4f4f4">
                            <tbody>
                            <tr class="tc" style="text-align: center" align="center">
                                <td class="f24 lh-title" style="border-collapse: separate; color: #9c9c9c; font-size: 24px; line-height: 1.2; padding: 12px 0 15px">Keep in Touch</td>
                            </tr>
                            <tr class="tc" style="text-align: center" align="center">
                                <td class="f18 lh-copy" style="font-size: 18px; line-height: 1.4;color:#9c9c9c;">If you have any questions, concerns, or suggestions,
                                    <br>please email us:
                                    <a href="mailto:info@djinyc.com" style="color:#000000;text-decoration: none;">info@djinyc.com</a>
                                </td>
                            </tr>
                            <tr class="tc" style="text-align: center" align="center">
                                <td style="padding-top: 30px">
                                    <a style="display: inline-block" href="https://twitter.com/dji">
                                        <img style="border: 0; display: inline-block; outline: none; width:36px;height:36px;text-decoration: none" alt="Twitter logo" src="http://djinyc.com/content/frontend/assets/img/twitterCircleBtn.png">
                                    </a>
                                    <a style="display: inline-block; padding: 0 25px" href="https://www.facebook.com/dji">
                                        <img style="border: 0; display: inline-block; outline: none; text-decoration: none;width:36px;height:36px;" alt="Facebook logo" src="http://djinyc.com/content/frontend/assets/img/facebookCircleBtn.png">
                                    </a>
                                    <a style="display: inline-block" href="https://www.instagram.com/dji/">
                                        <img style="border: 0; display: inline-block; outline: none; text-decoration: none;width:36px;height:36px;" alt="Instagram logo" src="http://djinyc.com/content/frontend/assets/img/instaCircleBtn.png">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="f14" style="font-size: 14px;color:#9c9c9c;">©2016 <span style="font-weight: 600;color:#9c9c9c;">dji</span> Adoni Group Inc., All rights reserved.
                                </td>
                            </tr>
                            <tr>
                                <td class="f14" style="color: #9c9c9c; font-size: 14px">247 West 38th Street, 301</td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="https://djinyc.com/">
                                        <img style="border: 0; width:60px;display: inline-block; margin-top: 30px; outline: none; text-decoration: none" alt="DJI Logo" src="http://djinyc.com/content/frontend/assets/img/header_logo.png">
                                    </a>
                                </td>
                            </tr>
                            </tbody></table>

                    </td>
                </tr>

            </table>
            <!--/600px container -->


        </td>
    </tr>
</table>
<!--/100% background wrapper-->

</body>
</html>';
    }

}
