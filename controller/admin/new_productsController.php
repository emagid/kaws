<?php

use Emagid\Html\Form;

class new_productsController extends adminController {
	
	function __construct(){
		parent::__construct("New_Product");
	}

	function index(Array $params = []){
		if (!Empty($_GET['how_many']))
		 {	
		 	 
		 	$params['limit'] = $_GET['how_many'];

		 }
		 else{
		 	$params['limit'] = 10;
		 }

			if (!Empty($_GET['how_many']) && !Empty($_GET['status_show'])){
				$st=str_replace ('%20',' ',$_GET['status_show']);
		 		$params['queryOptions']['where'] = " brand = '$st'";
		 		$params['limit'] = $_GET['how_many'];
			}

		 if (!Empty($_GET['status_show']))
		 {	
		 	$st=str_replace ('%20',' ',$_GET['status_show']);
		 	$params['queryOptions']['where'] = " brand = '$st'";

		 }
		 /*else{
		 	$params['queryOptions']['orderBy'] = "mpn ASC";
		 } */
		$this->_viewData->hasCreateBtn = true; 
		$this->_viewData->brands = \Model\Brand::getList();
		 
		parent::index($params); 
	
	}

	public function import(Array $params = [])
	{
		set_time_limit(0);
		$handle = fopen($_FILES['csv']['tmp_name'], "r");
		$headSkip = true;
		$id1 = '';
		$id2 = '';
		$id3 = '';
		$allArr = [];
		if ($handle) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if (empty($data[0])) {
				} else {
					if($headSkip){
						$headSkip = false;
					} else {
						if(!array_key_exists($data[0],$allArr)){
							$id1 = strtolower($data[7]);$id2 = strtolower($data[9]);$id3 = strtolower($data[11]);
							$allArr[$data[0]] = ['slug'=>$data[0],'name'=>$data[1],'description'=>$data[2],'tags'=>$data[5],'price'=>$data[19],'msrp'=>$data[20],'meta_title'=>isset($data[31])?$data[31]:'', 'meta_description'=>isset($data[32])?$data[32]:'',
												'options'=>[$id1=>[$data[8]],$id2=>[$data[10]],$id3=>[$data[12]]], 'images'=>[$data[24]]];
						} else {
							if(!in_array($data[8],$allArr[$data[0]]['options'][$id1])){
								$allArr[$data[0]]['options'][$id1][] = $data[8];
							}
							if(!in_array($data[10],$allArr[$data[0]]['options'][$id2])){
								$allArr[$data[0]]['options'][$id2][] = $data[10];
							}
							if(!in_array($data[12],$allArr[$data[0]]['options'][$id3])){
								$allArr[$data[0]]['options'][$id3][] = $data[12];
							}
							if(isset($data[24]) && $data[24] != ''){
								$allArr[$data[0]]['images'][] = $data[24];
							}
						}
					}
				}
			}
			fclose($handle);
		}
		foreach($allArr as $arr){
			$product = new \Model\Product();
			$product->slug = $arr['slug'];
			$product->name = $arr['name'];
			$product->description = $arr['description'];
			$product->tags = $arr['tags'];
			$product->price = $arr['price'];
			$product->msrp = $arr['msrp'] != ''?$arr['msrp']:$arr['price'];
			$product->meta_title = $arr['meta_title'];
			$product->meta_description= $arr['meta_description'];
			$product->options = json_encode($arr['options']);
			if(array_key_exists('',$arr['options'])){
				unset($arr['options'][""]);
			}
			if(isset($arr['options']['size'])){
				$product->size = json_encode($arr['options']['size']);
			}
			$arrColor = [];
			if(isset($arr['options']['color'])){
				foreach($arr['options']['color'] as $color){
					if($c = \Model\Color::getItem(null,['where'=>"name = '{$color}'"])){
						$arrColor[] = $c->id;
					} else {
						$strReplace = str_replace(',','-',trim($color));
						$strReplace = str_replace('/','-',trim($strReplace));
						$strReplace = str_replace('&','-',trim($strReplace));
						$strReplace = str_replace(' ','-',trim($strReplace));
						$c = new \Model\Color();
						$c->name = $color;
						$c->slug = $strReplace;
						$c->save();
						$arrColor[] = $c->id;
					}
				}
				$product->color = json_encode($arrColor);
			}
			$product->save();
			foreach($arr['images'] as $image){
				$prod_image = new \Model\New_Product_Image();
				$uniqueId = uniqid($product->slug);
				file_put_contents(__DIR__ . '/../../content/uploads/products/' . $uniqueId . '.jpg', file_get_contents($image));
				$prod_image->new_product_id = $product->id;
				$prod_image->image = $uniqueId . '.jpg';
				$prod_image->save();
			}
		}


		$n = new \Notification\MessageHandler("Import success!");
		$_SESSION["notification"] = serialize($n);
		redirect(SITE_URL . 'admin/products');


	}
	function export(Array $params = []){
		$this->template = false;
		$br = $_GET['brand'];
		$this->_viewData->hasCreateBtn = true;
		$this->_viewData->product = \Model\New_Product::getList(['where'=>' brand = '.$br]);
		$brand = \Model\Brand::getItem($br);
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$brand->name.'.csv');
		$output = fopen('php://output', 'w');
		$t=array("Name","Mpn","Sell price","MSRP", "Availability", 'Image File', 'Mpn 2', 'Mpn 3');
		fputcsv($output, $t);
		$row ='';
		foreach($this->_viewData->product as $b) {
		$row = array($b->name,$b->mpn,$b->price,$b->msrp, $b->getAvailability(), $b->featured_image,$b->mpn2,$b->mpn3);
		fputcsv($output, $row);  
		}   
		parent::index($params);
	}

	public function update(Array $arr = []) {
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);

		$carbon = \Carbon\Carbon::now();

		$this->_viewData->product_categories = \Model\New_Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_questions = \Model\Question::getList(['where'=>' new_product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_images = [];
		if(isset($arr['id'])){
			$this->_viewData->new_products = \Model\New_Product::getList(['where'=>"id not in ({$arr['id']})",'orderBy'=>'name']);
		}
		$this->_viewData->brands = \Model\Brand::getList(['orderBy'=>'name']);
		$this->_viewData->categories = \Model\Category::getList(['orderBy'=>'name']);
		$this->_viewData->colors = \Model\Color::getList(['orderBy'=>'name']);
		$this->_viewData->sizes = \Model\Size::getList(['orderBy'=>'us_size']);
		$this->_viewData->materials = \Model\Material::getList(['orderBy'=>'name']);
		$this->_viewData->soles = \Model\Sole::getList(['orderBy'=>'name']);
		$this->_viewData->events = \Model\Event::getList(['where'=>"end_date > '{$carbon->format('Y-m-d H:i:s')}'"]);

		parent::update($arr);
	}


	public function update_post()
	{
//		if($prov = \Model\\Model\New_Product::getItem($_POST['id'])) {
//			foreach ($prov->getAllProductImages() as $productImage) {
//				if ($productImage->id != $_POST['leg_shot']) {
//					$productImage->leg_shot = 0;
//					$productImage->save();
//				} else {
//					$productImage->leg_shot = 1;
//					$productImage->save();
//				}
//			}
//		}
		if(isset($_POST['new_product_category']) && count($_POST['new_product_category']) > 0){
			$arr = $_POST['new_product_category'];
			foreach($_POST['new_product_category'] as $pCatId){
				$pCat = \Model\Category::getItem($pCatId);
				$parentId = $pCat->parent_category;
				if($parentId != 0) {
					do {
						$parent = \Model\Category::getItem($parentId);
						if (!in_array($parent->id, $arr)) {
							$arr[] = (string)$parent->id;
						}
						$parentId = $parent->parent_category;
					} while ($parent->parent_category > 0);
				}
			}
			$_POST['new_product_category'] = $arr;
		}
		//handle linked list logic
		if(isset($_POST['linked_list']) && $_POST['linked_list']) {
			$linkedList = $_POST['linked_list'];
			\Model\New_Product::changeLink($linkedList,$_POST['id']);
		} else {
			\Model\New_Product::changeLink([],$_POST['id']);
		}
		//json encode list
		$jsonList = ['color','size','sole','related_products','linked_list'];
		foreach($jsonList as $post) {
			if (isset($_POST[$post]) && count($_POST[$post]) > 0) {
				$_POST[$post] = json_encode($_POST[$post]);
			}
		}
		//set value to 0 list
		$arr = ['length','width','height', 'quantity', 'heel_height'];
		foreach($arr as $ar){
			if(isset($_POST[$ar]) && $_POST[$ar] == null){
				$_POST[$ar] = 0;
			}
		}
		parent::update_post();
	}

	public function sort_images()
	{
		$display_order = 1;
		foreach($_POST['ids'] as $id){
			$prod_img = \Model\New_Product_Image::getItem($id);
			$prod_img->display_order = $display_order;
			$prod_img->save();
			$display_order++;
		}
	}

	public function save_variant(){
		$material = [$_POST['material']];
		$color = $_POST['color'];
		$sole = $_POST['sole'];
		$price = $_POST['price'] ? : 0.0;
		$quantity = $_POST['quantity'] ? : 0.0;
		$madeToOrder = $_POST['mto'] ? : 'off';
		$productId = $_POST['productId'];

		$echo = ['redirect'=>$_POST['redirect']];
		if($material && $color && $sole){
			$arr = \Model\Variation::$lowerVariations;
			$arrList = [];
			foreach($arr as $array){
				if(isset($$array)){
					$arrList[] = $$array;
				}
			}
			$list = \Model\Variation::combinations($arrList);
			foreach($list as $combo){
				$variant = [];
				for($i = 0; $i < count($combo); $i++){
					$variant[$arr[$i]] = $combo[$i];
				}
				$variation = \Model\Variation::addVariation($variant);
				\Model\New_Product_Attributes::addAttribute($productId,$variation->id,['price'=>$price, 'quantity'=>$quantity, 'madeToOrder'=>$madeToOrder]);
			}
		} else {
			$n = new \Notification\ErrorHandler("Requirements unmet");
			$_SESSION['notification'] = serialize($n);
		}
		echo json_encode($echo);
	}

	public function delete_variant()
	{
		$id = isset($_POST['id']) && $_POST['id'] != '' ? $_POST['id'] : null;
		$prod_id = isset($_POST['prod_id']) && $_POST['prod_id'] != '' ? $_POST['prod_id'] : null;
		if ($id && $prod_id) {
			$variation = \Model\Variation::getItem($id);
			$pAttr = \Model\New_Product_Attributes::getList(['where' => "variation_id = $variation->id and new_product_id = $prod_id"]);
			foreach ($pAttr as $pa) {
				\Model\New_Product_Attributes::delete($pa->id);
			}
			echo json_encode(['status' => "success"]);
		} else {
			echo json_encode(['status' => "failed", 'message' => "Failed to delete"]);
		}
	}

	public function save_attribute(){
		$product_id = $_POST['product_id'] ? : null;
		$color_id = $_POST['color_id'] ? : null;
		$price = $_POST['price'];
		$mto = $_POST['mto'];
		$event = $_POST['event'];
		$status = $_POST['status'];
		$arr = ['price'=>$price, 'mto'=>$mto, 'event'=>$event, 'status'=>$status];
		if($product_id && $color_id){
			foreach($arr as $key=>$ar){
				$prod_attr = \Model\New_Product_Attributes::getItem(null,['where'=>"new_product_id = $product_id and color_id = $color_id and name='{$key}'"]) ? : new \Model\New_Product_Attributes();
				$prod_attr->new_product_id = $product_id;
				$prod_attr->color_id = $color_id;
				$prod_attr->name = $key;
				$prod_attr->value = $ar;
				$prod_attr->save();
			}
			echo json_encode(['status'=>'success', 'message'=>'Saved']);
		} else {
			echo json_encode(['status'=>'failed', 'message'=>'Variant failed to save']);
		}
	}

	public function multi_update(Array $arr = []) {
		$id_array = $_GET;
		
		$q = implode("," , $id_array);

		if (count($_GET) <= 1){
			$n = new \Notification\ErrorHandler('You must select at least one product to Multi Edit.');
           	$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'products');
		}

		$this->_viewData->q=$q;
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->product_categories = \Model\New_Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_collections = \Model\Product_Collection::get_product_collections($pro->id);
		$this->_viewData->product_materials = \Model\Product_Material::get_product_materials($pro->id);
		$this->_viewData->product_notes = \Model\Note::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_notes as $note){
			$note->admin = \Model\Admin::getItem($note->admin_id);
			$note->insert_time = new DateTime($note->insert_time);
			$note->insert_time = $note->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_images = [];
		$this->_viewData->brands = \Model\Brand::getList();
		$this->_viewData->categories = \Model\Category::getList();
		$this->_viewData->colors = \Model\Color::getList();
		$this->_viewData->collections = \Model\Collection::getList();
		$this->_viewData->materials = \Model\Material::getList();

		parent::update($arr);
	}
	
	public function multi_update_post(){

		$q = explode(",", $_POST['id']);
		$a = $_POST;
		
		foreach($q as $productId){
			if (is_numeric($productId)){
				$product = Model\New_Product::getItem($productId);
				
				if ((isset($a['product_category'])) && (count($a['product_category'])>0)  )  {
					for ($x = 0; $x < count($a['product_category']); $x++){
						$category = \Model\New_Product_Category::getItem(null, ['where' => "new_product_id = '" . $productId . "' and category_id = ".$a['product_category'][$x]]);
						if (!isset($category))	{
							$category = new Model\Product_Category();
							$category->new_product_id = $productId;
							$category->category_id = $a['product_category'][$x];
							$category->save();
						}
					}
				}

				if ((isset($a['product_collection'])) && (count($a['product_collection'])>0)  )  {
					for ($x = 0; $x < count($a['product_collection']); $x++){
						$collection = \Model\Product_Collection::getItem(null, ['where' => "product_id = '" . $productId . "' and collection_id = ".$a['product_collection'][$x]]);
						if (!isset($collection))	{
							$collection = new Model\Product_Collection();
							$collection->product_id = $productId;
							$collection->collection_id = $a['product_collection'][$x];
							$collection->save();
						}
					}
				} 

				if ((isset($a['product_material'])) && (count($a['product_material'])>0)  )  {
					for ($x = 0; $x < count($a['product_material']); $x++){
						$material = \Model\Product_Material::getItem(null, ['where' => "product_id = '" . $productId . "' and material_id = ".$a['product_material'][$x]]);
						if (!isset($material))	{
							$material = new Model\Product_Material();
							$material->product_id = $productId;
							$material->material_id = $a['product_material'][$x];
							$material->save();
						}
					}
				}

				foreach($a as $key1 => $value1)	{
					if (!empty($value1) && $key1 != "id"){
						$product->$key1 = $value1;
						$product->save();
					}
				}
			}
		}
		redirect(ADMIN_URL.'products');
	}

	 

	protected function afterObjSave($obj){
		if (isset($_POST['note'])){
			$note = new \Model\Note();
			$note->name = $_POST['note']['name'];
			$note->description = $_POST['note']['description'];
			$note->admin_id = \Emagid\Core\Membership::userId();
			$note->product_id = $obj->id;
			$note->save();
		}
	}

	public function save_variant_image(){
		$img_id = $_POST['product_image_id'];
		$color_id = $_POST['color_id'];
		$prod_img = \Model\New_Product_Image::getItem($img_id);
		if($prod_img){
			$prod_img->color_id = json_encode($color_id);
			if($prod_img->save()){
				echo json_encode(['status'=>"success",'message'=>"Success"]);
			} else {
				echo json_encode(['status'=>"failed",'message'=>"Failed to save"]);
			}
		} else {
			echo json_encode(['status'=>"failed",'message'=>"Product Image does not exist... somehow"]);
		}
	}

	public function save_legshot_image(){
		$id = $_POST['product_image_id'];
		$colId = $_POST['color_id'];
		$prodId = $_POST['product_id'];

		$prodImg = \Model\New_Product_Image::getList(['where'=>"new_product_id = $prodId and color_id like '%$colId%'"]);
		if($prodImg) {
			foreach ($prodImg as $pi) {
				if ($pi->id != $id || ($pi->id == $id && $pi->legshot == 1)) {
					$pi->legshot = 0;
				} else {
					$pi->legshot = 1;
				}
				$pi->save();
			}
			echo json_encode(['status' => "success", 'message' => "Success"]);
		} else {
			echo json_encode(['status'=>"failed",'message'=>"Failed"]);
		}
	}

	function upload_images($params) {
		header("Content-type:application/json");
		$data = [];
		$data['success'] = false;
		$data['redirect'] = false;
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
		if((int)$id>0) {
			$product = \Model\New_Product::getItem($id);
			if($product==null) {$data['redirect']=true;}
			$this->save_upload_images($_FILES['file'], $product->id, UPLOAD_PATH.'new_products'.DS,[]);
			$data['success'] = true;
		}
	
		//print_r($_FILES);
		echo json_encode($data);exit();
	}
	
	public function save_upload_images($files,$obj_id,$upload_path,$sizes=[]) {
		$counter = 1;
		// get highest display order counter if any images exist for specified listing
		$image_high = \Model\New_Product_Image::getItem(null,['where'=>'new_product_id='.$obj_id,'orderBy'=>'display_order','sort'=>'DESC']);
		if($image_high!=null) {
			$counter = $image_high->display_order+1;
		}
		foreach($files['name'] as $key=>$val) {
			$product_image = new \Model\New_Product_Image();
			$temp_file_name = $files['tmp_name'][$key];
			$file_name = uniqid() . "_" . basename($files['name'][$key]);
			$file_name = str_replace(' ', '_', $file_name);
			$file_name = str_replace('-', '_', $file_name);
	
			$allow_format = array('jpg', 'png', 'gif','JPG');
			$filetype = explode("/", $files['type'][$key]);
			$filetype = strtolower(array_pop($filetype));
			//$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
			//$filetype = array_pop($filetype);
			if($filetype == 'jpeg' || $filetype=="JPG"){$filetype = 'jpg';}
	
			if(in_array($filetype, $allow_format)){
	
				$img_path = compress_image($temp_file_name, $upload_path . $file_name, 60,$files['size'][$key]);
				if($img_path===false) {
					move_uploaded_file($temp_file_name, $upload_path . $file_name);
				}
				 //move_uploaded_file($temp_file_name, $upload_path . $file_name);
				 
				if(count($sizes)>0) {
					foreach($sizes as $key=>$val) {
						if(is_array($val) && count($val)==2) {
							resize_image_by_minlen($upload_path,
							$file_name, min($val), $filetype);
	
							$path = images_thumb($file_name,min($val),
									$val[0],$val[1], file_format($file_name));
							$image_size_str = implode('_',$val);
							copy($path, $upload_path.$image_size_str.$file_name);
						}
					}
				}
//				$variant_image = new \Model\Product_Attributes();
//				$variant_image->product_id = $obj_id;
//				$variant_image->name = 'image';
//				$variant_image->value = $file_name;
//				$variant_image->save();
	
				$product_image->image = $file_name;
				$product_image->new_product_id = $obj_id;
				$product_image->display_order = $counter;
				$product_image->save();
			}
			$counter++;
		}
	}

	function delete_prod_image($params)
	{
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
		$product = \Model\New_Product_Image::getItem($id);
		$this->emagid->getDb()->execute('Delete  from  new_product_images where id='.$id);
		redirect($_POST['redirectTo'] = ADMIN_URL.'new_products/update/'.$product->new_product_id);
	}

	public function search(){
		$products = \Model\New_Product::search($_GET['keywords']);

		echo '[';
		foreach($products as $key=>$product){
			$colorHtml = '';
			foreach(json_decode($product->color,true) as $value){
				$stat = \Model\New_Product_Attributes::getStatusById($product->id,$value);
				$color = \Model\Color::getItem($value);
				$style = $context = '';
				if($stat && $stat->value == 2){$style = "style='color:green'";$context = '(Sold Out)';} elseif($stat && $stat->value == 3){$style = "style='color:red'";$context = '(Hidden)';}
				$colorHtml .= '<div '.$style.'>-'.$color->name.' '.$context.'</div>';
			}
			echo '{ "id": "'.$product->id.'", "name": "'.$product->name.'", "featured_image": "'.$product->featured_image.'", "price":"'.$product->price.'", "msrp":"'.$product->msrp.'", "colorHtml":"'.$colorHtml.'", "availability":"'.$product->getAvailability().'" }';
			if ($key < (count($products)-1)){
				echo ",";
			}
		}
		echo ']';
	}   

	public function check_slug(){
		$slug = $_POST['slug'];
		$id = $_POST['id'];
		if($id>0){
			$list = \Model\New_Product::getItem(null,['where'=>"slug = '".$slug."' and id = '".$id."'"]);
				 if (isset($list) && count($list) == 1){
				 	echo "0";
				 }else{
					 	$list = \Model\New_Product::getList(['where'=>"slug = '".$slug."' "]);
					 if (isset($list) && count($list) > 0){
					 	echo "1";
					 }else{
					 	echo "0";
					 }
				 }
		}else{
			$list = \Model\New_Product::getList(['where'=>"slug = '".$slug."' "]);
				 if (isset($list) && count($list) > 0){
				 	echo "1";
				 }else{
				 	echo "0";
				 }
		}
		
	}   

	public function google_product_feed(){
        if(isset($_SESSION["google_feed_generated"]) && $_SESSION["google_feed_generated"] == 1){
            $this->_viewData->generated = true;
            unset($_SESSION["google_feed_generated"]);
        }
        $this->loadView($this->_viewData);
    }

    public function google_product_feed_post(){
		set_time_limit(0);
    	$genders = [ 'women' => 'Female', 'collaborations' => 'Female', 'men' => 'Male'];
//    	$type = "Apparel & Accessories > Jewelry > Watches";
    	$condition = "new";
        $availability = "in stock"; // "out of stock"
        $gpc = 187;
//        $identifier_exists = "TRUE";
        $age_group = "Adult";
        $brand = "Modern Vice";
        $size_system = "US";

        $f = fopen(UPLOAD_PATH."/google_product_feed.txt","w"); fclose($f); //clear the file
        $f = fopen(UPLOAD_PATH."/google_product_feed.txt","a"); //open with append write mode

//        $header = "id\ttitle\tdescription\tgoogle product category\tlink\timage link\tcondition\tavailability\tprice\tbrand\tmpn\tcolor\tage group\tgender";
        $header = "item_group_id\ttitle\tdescription\tgoogle product category\tlink\timage link\tcondition\tavailability\tprice\tbrand\tidentifier exists\tcolor\tage group\tgender\tsize\tsize_system\tid\tproduct type";
        fwrite($f, $header);

		$sql2 = 'select product.id, product.name, product.description, product.slug, product.featured_image, product.price, product.color, product.size from product where product.active = 1 order by id';
        $products = $this->emagid->db->getResults($sql2);

        $line_items = "";
        foreach($products as $product){
			$prod = \Model\New_Product::getItem($product['id']);

			$colorList = $product['color'] ? implode(',',json_decode($product['color'],true)) : 0;
			$colorSql = "select color.id, color.name from color where active = 1 and id in ({$colorList})";
			$colors = $this->emagid->db->getResults($colorSql);

			$sizeList = $product['size'] ? implode(',',json_decode($product['size'],true)): 0;
			$sizeSql = "select size.id, size.us_size from public.size where active = 1 and id in ({$sizeList})";
			$sizes = $this->emagid->db->getResults($sizeSql);

//			$colors = $prod->getColors();
//			$sizes = $prod->getSizes();
			foreach($colors as $col) {
				foreach ($sizes as $siz) {
					$item_group_id = $product['id'];
					$id = $product['id'].''.str_pad($col['id'],3,'0',STR_PAD_LEFT).''.str_pad($siz['id'],3,'0',STR_PAD_LEFT);
					$title = "Modern Vice {$product['name']} {$col['name']}";

					if ($product['description'] != '') {
						$description = str_replace('\\', '\\\\', $product['description']);
						$description = str_replace('<p>', ' ', $description);
						$description = str_replace('</p>', ' ', $description);
						$description = str_replace('<br>', ' ', $description);
						$description = str_replace('<br />', ' ', $description);
						$description = str_replace('<br/>', ' ', $description);
						$description = str_replace("\n", " ", $description);
						$description = str_replace('\n', ' ', $description);
						$description = str_replace("\r", " ", $description);
						$description = str_replace('\r', ' ', $description);
						$description = str_replace("\t", " ", $description);
						$description = str_replace('\t', ' ', $description);
						$description = str_replace(PHP_EOL, ' ', $description);
					} else {
						$description = $product['name'];
					}
					$link = "https://www.modernvice.com/products/" . $product['slug']; //TODO st-dev check if valid url

					$image_link = ($pi=\Model\New_Product_Image::getItem(null,['where'=>"new_product_id = $prod->id and color_id like '%\"{$col['id']}\"%' and (legshot is null or legshot = 0)", 'orderBy'=>"display_order"])) ? $pi->image: $product['featured_image'];
					$image_link = "https://www.modernvice.com" . UPLOAD_URL . 'products/' . $image_link; //TODO st-dev check if valid url

					$pCatSql = "select category_id from new_product_categories where active = 1 and new_product_id = {$prod->id}";
					$pCategories = $this->emagid->db->getResults($pCatSql);
					$pList = implode(',',flatten($pCategories));

					$categoriesSql = "select name, parent_category from category where active = 1 and id in ({$pList})";
					$categories = $this->emagid->db->getResults($categoriesSql);
//					$pCategories = \Model\New_Product_Category::getList(['where'=>"product_id = $prod->id"]);
//					$categories = array_map(function($item){return \Model\Category::getItem($item->category_id);},$pCategories);
					$arr = [];
					foreach($categories as $category){
						if($category['parent_category']){
							$parentSql = "select name from category where active = 1 and id = {$category['parent_category']}";
							$parent = $this->emagid->db->getResults($parentSql);
//							$parent = \Model\Category::getItem($category['parent_category']);
							$arr[] = trim($parent[0]['name']).' > '.trim($category['name']);
						}
					}
					//product type single column
					$product_type = implode(',',$arr);

					$price = $prod->price($col['id']) . ' USD';
					$brand = 'Modern Vice';
					$identifier_exists = 'FALSE';
					$color = $col['name'];
					$gender = $prod->getMainCategory() ? $genders[$prod->getMainCategory()->slug] : 'Female';
					$size = $siz['us_size'];


//					$line_item = "\n".$id."\t".$title."\t".$description."\t".$gpc."\t".$link."\t".$image_link."\t".$condition."\t".$availability."\t".$price."\t".$brand."\t".$mpn."\t".$identifier_exists."\t".$color."\t".$age_group."\t".$gender."\t".$type;
					$line_item = "\n" . $item_group_id . "\t" . $title . "\t" . $description . "\t" . $gpc . "\t" . $link . "\t" . $image_link . "\t" . $condition . "\t" . $availability . "\t" .
						$price . "\t" . $brand . "\t" . $identifier_exists . "\t" . $color . "\t" . $age_group . "\t" . $gender . "\t" . $size . "\t" . $size_system . "\t" . $id ."\t".$product_type;
					fwrite($f, $line_item);
				}
			}
		}
        fclose($f); 
        $_SESSION["google_feed_generated"] = 1;
        redirect(ADMIN_URL.'products/google_product_feed');
    }
     
}