<?php

class bannersController extends adminController {
	
	function __construct(){
		parent::__construct("Banner");
	}

	function update(Array $params = []){}
	function update_post(Array $params = []){}
//
//	function afterImageUpload($image){
//		if($this->emagid->route['action'] == 'featured_update'){
//			if ($_POST['featured_id'] == 1 || $_POST['featured_id'] == 4 || $_POST['featured_id'] == 5){
//				$image->resize('370_310'.$image->fileName, false, 370, 310);
//			} else if ($_POST['featured_id'] == 2 || $_POST['featured_id'] == 3 || $_POST['featured_id'] == 6) {
//				$image->resize('370_220'.$image->fileName, false, 370, 220);
//			}
//		}
//	}

	function main(Array $params = []){
		$this->_viewData->hasCreateBtn = true;
		$params['queryOptions'] = ['where'=>"banner_type = 1"];
		parent::index($params);
	}

	function main_update(Array $params = []){
		$this->_viewData->products = \Model\Product::getList(['orderBy'=>'name']);
		$params['overrideView'] = 'main_update';
		parent::update($params);
	}

	function main_update_post(){
		$_POST['options'] = [];
//		buttons
		for($i = 0;$i < count($_POST['button_title']); $i++){
			$_POST['options']['buttons'][] = ['title'=>$_POST['button_title'][$i],'url'=>$_POST['button_url'][$i],'type'=>$_POST['button_type'][$i],'button_order'=>$_POST['button_order'][$i]];
		}
		$_POST['options'] = json_encode($_POST['options']);
//		$_POST['options'] = json_encode(['product_id'=>$_POST['product_id']]);
		$obj = new $this->_model($_POST);

    	if ($obj->save()){
    		$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
    		if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
    			$image = new \Emagid\Image();
    			$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
    			$image->resize('1200_540'.$image->fileName, true, 1200);
    			$obj->$imageType = $image->fileName;
    			$obj->save();
    		}
            $n = new \Notification\MessageHandler('Banner saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/main');
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/main_update/'.$obj->id);
    	}
	}

	function main_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/main';
		parent::delete($params);
	}

	function main_delete_image(Array $params = []){
		$banner = \Model\Banner::getItem($params['id']);

		if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)){
			\Emagid\Image::delete('banners/'.$banner->image);
		}

		$banner->image = null;
		$banner->is_deal = 0;
		$banner->save();

		redirect(ADMIN_URL.'banners/main_update/'.$banner->id);
	}

	function featured(Array $params = []){
		if(count((array)\Model\Banner::getList(['where'=>"banner_type = 2"])) < 1){
			$this->_viewData->hasCreateBtn = true;
		}
		$params['queryOptions'] = ['where'=>"banner_type = 2"];
		parent::index($params);
	}

	function featured_update(Array $params = []){
//		$this->_viewData->categories = \Model\Category::getList();
		parent::update($params);
	}

	function featured_update_post(){
//		$_POST['options'] = json_encode(['category'=>$_POST['category_id']]);
		$obj = new $this->_model($_POST);

    	if ($obj->save()){
    		$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
    		if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
    			$image = new \Emagid\Image();
    			$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
    			$this->afterImageUpload($image);
    			$obj->$imageType = $image->fileName;
    			$obj->save();
    		}
            $n = new \Notification\MessageHandler('Banner saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/featured');
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.'banners/featured_update/'.$obj->featured_id);
    	}
	}

	function featured_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/featured';
		parent::delete($params);
	}

	function featured_delete_image(Array $params = []){
		$banner = \Model\Banner::getItem(null, ['where'=>'featured_id = '.$params['id']]);

		if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)){
			\Emagid\Image::delete('banners/'.$banner->image);
		}

		$banner->image = null;
		$banner->is_deal = 0;
		$banner->save();

		redirect(ADMIN_URL.'banners/featured_update/'.$banner->featured_id);
	}

	function selection(Array $params = []){
		if(count(\Model\Banner::getList(['where'=>"banner_type = 3"])) < 1){
			$this->_viewData->hasCreateBtn = true;
		}
		$params['queryOptions'] = ['where'=>"banner_type = 3"];
		parent::index($params);
	}

	function selection_update(Array $params = []){
		$this->_viewData->categories = \Model\Category::getList();
		parent::update($params);
	}

	function selection_update_post(){
		$arr = [];
		$inc = 0;
		foreach($_POST['image'] as $img){
			$arr[] = ['img'=>$img,'display_order'=>$inc];
			$inc++;
		}
		$_POST['options'] = json_encode($arr);
		$obj = new $this->_model($_POST);

		if($obj->save()){
			$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
            if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
                $image = new \Emagid\Image();
                $image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
                $this->afterImageUpload($image);
                $obj->$imageType = $image->fileName;
                $obj->save();
            }
			$n = new \Notification\MessageHandler('Banner saved.');
			$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'banners/selection');
		} else {
			$n = new \Notification\ErrorHandler($obj->errors);
			$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'banners/selection_update/'.$obj->featured_id);
		}
	}

	function selection_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/selection';
		parent::delete($params);
	}

	function selection_delete_image(Array $params = []){
		$banner = \Model\Banner::getItem($params['id']);
        if (file_exists(UPLOAD_PATH.'banners/'.$banner->image)){
            \Emagid\Image::delete('banners/'.$banner->image);
        }

        $banner->image = null;
        $banner->is_deal = 0;
        $banner->save();
		$redirectTo = ADMIN_URL.'banners/selection_update/'.$banner->id;
		redirect($redirectTo);
	}

	function alternate(Array $params = []){
		$this->_viewData->hasCreateBtn = true;
		$params['queryOptions'] = ['where'=>"banner_type = 4"];
		parent::index($params);
	}

	function alternate_update(Array $params = []){
		$this->_viewData->categories = \Model\Category::getList();
		parent::update($params);
	}

	function alternate_update_post(){
		$_POST['options'] = json_encode(['alt_image'=>$_POST['alt_image'],'button_text'=>$_POST['button_text']]);
		$obj = new $this->_model($_POST);

		if ($obj->save()){
			$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
			if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
				$image = new \Emagid\Image();
				$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
				$this->afterImageUpload($image);
				$obj->$imageType = $image->fileName;
				$obj->save();
			}
			if($imageType != '' && $_FILES['alt_image']['error'] == 0) {
				$image = new \Emagid\Image();
				$image->upload($_FILES['alt_image'], UPLOAD_PATH.$this->_content.DS);
				$this->afterImageUpload($image);
				$obj->options = json_encode(['alt_image'=>$image->fileName,'button_text'=>$_POST['button_text']]);
				$obj->save();
			}
			$n = new \Notification\MessageHandler('Banner saved.');
			$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'banners/alternate');
		} else {
			$n = new \Notification\ErrorHandler($obj->errors);
			$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'banners/media_update/'.$obj->featured_id);
		}
	}

	function media_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/media';
		parent::delete($params);
	}

	function speciality(Array $params = []){
		$this->_viewData->hasCreateBtn = true;
		$params['queryOptions'] = ['where'=>"banner_type = 5"];
		parent::index($params);
	}

	function speciality_update(Array $params = []){
		$this->_viewData->categories = \Model\Category::getList();
		parent::update($params);
	}

	function speciality_update_post(){
		$obj = new $this->_model($_POST);

		if ($obj->save()){
			$imageType = isset($_FILES['image'])?'image':(isset($_FILES['featured_image'])?'featured_image':'');
			if($imageType != '' && $_FILES[$imageType]['error'] == 0) {
				$image = new \Emagid\Image();
				$image->upload($_FILES[$imageType], UPLOAD_PATH.$this->_content.DS);
				$this->afterImageUpload($image);
				$obj->$imageType = $image->fileName;
				$obj->save();
			}
			$n = new \Notification\MessageHandler('Banner saved.');
			$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'banners/speciality');
		} else {
			$n = new \Notification\ErrorHandler($obj->errors);
			$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'banners/speciality_update/'.$obj->featured_id);
		}
	}

	function speciality_delete(Array $params = []){
		$_POST['redirectTo'] = ADMIN_URL.'banners/speciality';
		parent::delete($params);
	}

}






















































