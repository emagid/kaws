<?php

class ticketsController extends adminController{
    function __construct()
    {
        parent::__construct('Service_Ticket');
    }

    public function index(Array $params = [])
    {
//        $this->_viewData->hasCreateBtn = true;

//        if($_SERVER['QUERY_STRING']){
//            parse_str($_SERVER['QUERY_STRING'], $statusArr);
//            $queryBuild = implode(',', $statusArr['status']);
//            $params['queryOptions'] = ['where' => "service_status_id in ($queryBuild)"];
//        } else {
//            $params['queryOptions'] = ['where' => ['service_status_id' => \Model\Service_Status::getEstimatePendingStatus()->id]];
//        }

        $this->_pageSize = 50;
        parent::index($params);
    }

    function beforeLoadUpdateView()
    {
        if (is_null($this->_viewData->service_ticket->viewed) || !$this->_viewData->service_ticket->viewed){
            $this->_viewData->service_ticket->viewed = true;
            $this->_viewData->service_ticket->save();
        }
    }

    public function search(){
        global $emagid; $db = $emagid->getDb();

        $where = [];
        $andWhere = ['1=1'];

        if($_GET['keywords']){
            $keyword = strtolower(urldecode($_GET['keywords']));
            if(is_numeric($keyword)){
                $where[] = "service_ticket.id = '$keyword'";
            }
            $where[] = "lower(firstname) like '%$keyword%'";
            $where[] = "lower(lastname) like '%$keyword%'";
            $where[] = "lower(product) like '%$keyword%'";
            $where[] = "lower(model) like '%$keyword%'";
            $where[] = "lower(service_status.name) like '%$keyword%'";
        }

        if($_GET['date_start'] && $_GET['date_end']){
            $start = (new \Carbon\Carbon($_GET['date_start']))->startOfDay()->toDateTimeString();
            $end = (new \Carbon\Carbon($_GET['date_end']))->endOfDay()->toDateTimeString();
            $andWhere[] = "(service_ticket.insert_time >= '$start' and service_ticket.insert_time <= '$end') or (service_ticket.modified_time >= '$start' and service_ticket.modified_time <= '$end')";
        }

        if(!$where){
            $searchWhere = '1=1';
        } else {
            $searchWhere = implode(' or ',$where);
        }
        $searchAndWhere = implode(' and ',$andWhere);
        $searchQuery = "select service_ticket.id, service_ticket.insert_time, modified_time, firstname, lastname, product, model, service_status.name as status from service_ticket join service_status on service_status.id = service_ticket.service_status_id where service_status.active = 1 and service_ticket.active = 1 and ({$searchWhere}) and ($searchAndWhere) order by service_ticket.insert_time desc limit 200";
//        dd($searchQuery);
        $searchResult = $db->execute($searchQuery);

//        $searchResult = array_map(function(&$item){
//            if(is_numeric($item['availability'])){
//                $item['availability'] = \Model\Product::$availability[$item['availability']];
//            }
//            if(is_numeric($item['type'])){
//                $item['type'] = \Model\Product::$type[$item['type']];
//            }
//            return $item;},$searchResult);

        echo json_encode($searchResult);
    }
    
    public function approve_estimate()
    {
        if(!$_POST['id']){
            $this->toJson(['success' => 0, 'message' => 'Add Estimate failed']);
        }
        $admin = \Model\Admin::getItem($_POST['manager']);

        $ticket = \Model\Service_Ticket::getItem($_POST['id']);

        $log = $ticket->createLog("APPROVED ESTIMATE","$admin->first_name $admin->last_name");

        /**
         * Send Client email
         */
        $link = SITE_DOMAIN.'/repair_invoice/'.$ticket->invoice_number;
        $html = "<p>Dear {$ticket->firstname}, Your repair request has been approved, please click <a href='$link'>$link</a> to finish your request</p>";
        //(new \EmagidService\MailMaster())->setFromAddress('info@emagid.com')->setSubject('Your repair service has been approved')->setHtml($html)->addTo(['email' => $ticket->email, 'name' => $ticket->firstname, 'type' => 'to'])->send();

        $this->toJson(['success' => 1, 'message' => 'Saved',"log"=>$log]);
    }

    public function add_discount()
    {
        if(!$_POST['id']){
            $this->toJson(['success' => 0, 'message' => 'Add discount failed']);
        }

        $ticket = \Model\Service_Ticket::getItem($_POST['id']);

        $ticket->discount = $_POST['amount'];

        $ticket->save();
    }

    public function set_shipping()
    {
        if(!$_POST['id']){
            $this->toJson(['success' => 0, 'message' => 'Set shipping failed']);
        }

        $ticket = \Model\Service_Ticket::getItem($_POST['id']);

        $ticket->shipping = $_POST['amount'];

        $ticket->save();
    }

    public function delete_existing_parts()
    {
        if(!$_POST['id']){
            $this->toJson(['success' => 0, 'message' => 'Add discount failed']);
        }

        $ticket = \Model\Service_Ticket::getItem($_POST['id']);

        $index = $_POST['index'];

        $data = $ticket->getEstimateData();

        unset($data['parts'][$index]);

        $ticket->estimate_details = json_encode($data);

        $ticket->save();
    }
    
    public function add_estimate()
    {
        if(!$_POST['id']){
            $this->toJson(['success' => 0, 'message' => 'Add Estimate failed']);
        }

        $estimateStatusId = \Model\Service_Status::getSubmittedEstimateStatus()->id;

        $ticket = \Model\Service_Ticket::getItem($_POST['id']);
        $ticket->estimate_details = json_encode($_POST['payload']);
        $ticket->assigned_manager = $_POST['manager'];
        $ticket->service_status_id = $estimateStatusId;
        $ticket->invoice_number = uniqid();

        $ticket->save();

        $ticket->createLog('ADD ESTIMATE');

        $ticketHistory = new \Model\Service_History();
        $ticketHistory->service_ticket_id = $ticket->id;
        $ticketHistory->service_status_id = $estimateStatusId;
        $estimate = json_decode($ticket->estimate_details,true);
        $cost = '';
        foreach($estimate['parts'] as $part){
            $cost .= $part['name'].' : '.$part['price'].'<br/>';
        }
        $ticketHistory->comment = $cost;
        $ticketHistory->save();

        /**
         * Send manager email
         */
        $manager = \Model\Admin::getItem($ticket->assigned_manager);
        $html = "Dear {$manager->first_name}, you have new repair service assigned, please review at admin";
        (new \EmagidService\MailMaster())->setFromAddress('info@emagid.com')->setSubject('DJI New Ticket Assigned')->setHtml($html)->addTo(['email' => $manager->email, 'name' => $manager->first_name, 'type' => 'to'])->send();

        $this->toJson(['success' => 1, 'message' => 'Saved']);

    }

    public function update(Array $arr = []) {
        $ticket = new $this->_model(isset($arr['id'])?$arr['id']:null);
        $histories = $ticket->getHistories();
        $history = [];
        $received = false;
        foreach($histories as $obj){
            $history[$obj->service_status_id] = $obj->getStatusName();
            //if($obj)
        }
        $this->_viewData->rma_history = $history;
        $this->_viewData->page_title = "Service ID: $ticket->id";
        parent::update($arr);
    }

    function update_post()
    {
        $ticket = \Model\Service_Ticket::getItem($_POST['id']);
        if(isset($_POST['email'])){
            $ticket->email = $_POST['email'];
            $ticket->save();
        }
        $history = isset($_POST['history'])?$_POST['history']:false;
        $technician = isset($_POST['technician'])?$_POST['technician']:false;
        unset($_POST['history']);
        unset($_POST['technician']);

        $obj = \Model\Service_Ticket::getItem($_POST['id']);

        $obj->modified_time = \Carbon\Carbon::now()->toDateTimeString();

        $obj->save();

        if($history && $history['status'] != 0 && $obj->id){
            // Service History
            $sh = new \Model\Service_History();
            $sh->active = 1;
            $sh->insert_time = date('Y-m-d G:i:s');
            $sh->service_ticket_id = $obj->id;
            $sh->service_status_id = $history['status'];
            $sh->comment = $history['comment'];
            $sh->notify = isset($history['notify'])&&$history['notify']!=''?1:0;
            $sh->save();

            if(in_array($sh->service_status_id,[4,6,12,3,16,21]) && $sh->notify) {
                $html = '';

                $sent = new EmagidService\MailMaster();
                switch ($sh->service_status_id) {
                    case 4: //RMA Approved
                        $sent->setSubject(\Model\Email_Type::getItem(1)->subject);
                        $html = \Model\Email_Type::getItem(1)->message;
                        break;
                    case 6: //Received
                        $sent->setSubject(\Model\Email_Type::getItem(2)->subject);
                        $html = \Model\Email_Type::getItem(2)->message;
                        $received = $ticket->product.' '.$ticket->product_model.' with '.$sh->comment;
                        $html = str_replace('%extras%',$received,$html);
                        $sh->comment = $received;
                        $sh->save();
                        break;
//                    case 37: //Estimate submitted by Manager
                    case 12: //Estimate approved by Manager
                        $sent->setSubject(\Model\Email_Type::getItem(3)->subject);
                        $html = \Model\Email_Type::getItem(3)->message;

                        $approveHTML = "<a href='http://".SITE_DOMAIN."/service/estimate/".$obj->id."/21'>Approve Estimate</a>";
                        $declineHTML = "<a href='http://".SITE_DOMAIN."/service/estimate/".$obj->id."/16'>Decline Estimate</a> ";
                        $estimateHtml = '';

                        $data = $obj->getEstimateData();
                        $managerName = $obj->getAssignedManagerName();
                        if($obj->estimate_details && $obj->assigned_manager) {
                            $total = 0;
                            $estimateHtml .= '<h4>Existing Details</h4> <div class="box">' .
                                '<table class="table">' .
                                '<tbody>' .
                                '<tr>' .
                                '<td>Manager:</td>' .
                                '<td colspan="3">' . $managerName . '</td>' .
                                '</tr>';
                            $estimateHtml .= '<tr>'.
                                             '<th>Item</th>'.
                                             '<th>Quantity</th>'.
                                             '<th>Unit Price</th>'.
                                             '<th>Total Price</th>'.
                                             '</tr>';
                            $parts = [];
                            foreach (array_merge($data['parts'], []) as $index => $d) {
                                if(isset($parts[$d['id']]) && $d['id'] == 0 ) {
                                    $parts[] = [
                                        'name'=>$d['name'],
                                        'quantity'=>1,
                                        'uPrice'=>$d['price'],
                                        'tPrice'=>$d['price']
                                    ];
                                }
                                else if(isset($parts[$d['id']])){
                                    $parts[$d['id']]['quantity'] = intval($parts[$d['id']]['quantity'])+1;
                                    $parts[$d['id']]['tPrice'] = floatval($d['price'])*floatval($parts[$d['id']]['quantity']);
                                } else {
                                    $parts[$d['id']] = [
                                        'name'=>$d['name'],
                                        'quantity'=>1,
                                        'uPrice'=>$d['price'],
                                        'tPrice'=>$d['price']
                                    ];
                                }
                            }
                            foreach($parts as $part){
                                $total += floatval($part['tPrice']);
                                $estimateHtml .= "<tr>".
                                    "<td>{$part['name']}</td>" .
                                    '<td>'.$part['quantity'].'</td>' .
                                    '<td>$'.$part['uPrice'].'</td>' .
                                    '<td>$'.$part['tPrice'].'</td>' .
                                    "</tr>";
                            }
                            if ($obj->discount) {
                                $total -= floatval($obj->discount);
                                $estimateHtml .= "<tr>" .
                                    "<td>Discount</td>" .
                                    "<td colspan='2'>- {$obj->discount}</td>" .
                                    "</tr>";
                            }
                            if ($obj->shipping) {
                                $total += floatval($obj->shipping);
                                $estimateHtml .= "<tr>" .
                                    "<td>Shipping</td>" .
                                    "<td colspan='2'> {$obj->shipping}</td>" .
                                    "</tr>";
                            }
                            $estimateHtml .= "<tr>" .
                                "<td>Total</td>" .
                                '<td colspan="4">$'.$total.'</td>'.
                                "</tr>" .
                                "</tbody>" .
                                "</table>" .
                                "</div>";
                        }
                        $html = str_replace('%invoice%', "{$estimateHtml}", $html);
                        $html = str_replace('%approveLink%', "{$approveHTML}", $html);
                        $html = str_replace('%declineLink%', "{$declineHTML}", $html);
                        break;
                    case 21: //order complete
                        $sent->setSubject(\Model\Email_Type::getItem(4)->subject);
                        $html = \Model\Email_Type::getItem(4)->message;
                        break;
                    case 16: //order complete
                        $sent->setSubject(\Model\Email_Type::getItem(5)->subject);
                        $html = \Model\Email_Type::getItem(5)->message;
                        break;
                    case 3: //order complete
                        $sent->setSubject(\Model\Email_Type::getItem(6)->subject);
                        $html = \Model\Email_Type::getItem(6)->message;
                }

                $html = str_replace('%custName%',"{$obj->firstname} {$obj->lastname}",$html);
                $html = str_replace('%customerName%',"{$obj->firstname} {$obj->lastname}",$html); // TODO synch dbs
                $html = str_replace('%productId%',"{$obj->product} {$obj->product_model}",$html);
                $html = str_replace('%serviceId%',$obj->id,$html);
                $html = str_replace('%invoiceLink%',"<a href='http://".SITE_DOMAIN.'/repair_invoice/'.$obj->invoice_number."'>".
                                                                       SITE_DOMAIN.'/repair_invoice/'.$obj->invoice_number."</a>",$html);

                $email = $obj->email;
                $fname = $obj->firstname;
                $sent->setTemplate('dji-service-update')
//                $sent->setHtml($html)
//                    ->setFromAddress('eitan@emagid.com')
//                    ->setFromName('DJI Service')
                    ->addTo(['email' => $email, 'name' => 'test', 'type' => 'to'])
                    ->addMergeTags([$email => [
                        'FIRST_NAME' => $fname,
                        'COMMENT' => $html
                    ]]);
                $sent->send();



            } else if($sh->notify){
                $email = $obj->email;
                $fname = $obj->firstname;
                (new EmagidService\MailMaster())
                    ->setTemplate('dji-service-update')
                    ->addTo(['email' => $email, 'name' => 'test', 'type' => 'to'])
                    ->addMergeTags([$email => [
                        'FIRST_NAME' => $fname,
                        'COMMENT' => $sh->comment
                    ]])
                    ->send();
            }

            /**
             * Update ticket to last history status
             */
            $obj->service_status_id = $sh->service_status_id;
            $obj->save();
        }

        if($technician && $technician['comment'] && $obj->id){
            // Service Technician note
            $sh = new \Model\Service_Technician();
            $sh->service_ticket_id = $obj->id;
            $sh->comment = $technician['comment'];
            $sh->save();
        }

        redirect(ADMIN_URL.'tickets/update/'.$_POST['id']);
    }
    function mass_update_status_post(){
        foreach ($_POST['tickets'] as $ticket_id){
            // Service History
            $sh = new \Model\Service_History();
            $sh->service_ticket_id = $ticket_id;
            $sh->service_status_id = $_POST['status'];
            $sh->comment = "Mass update";
            $sh->notify = 0;
            $sh->save();

            $st = \Model\Service_Ticket::getItem($ticket_id);
            $st->service_status_id = $_POST['status'];
            $st->save();
        }
        redirect(ADMIN_URL.'tickets/');
    }

}