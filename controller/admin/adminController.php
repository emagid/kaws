<?php

use Emagid\Html\Form,
    Emagid\Pagination,
    Emagid\Core\Membership;

/**
 * V1.8
 * Base class to be extended for admin controllers
 */

class adminController extends \Emagid\Mvc\Controller {

    protected $_model = "";
    protected $_content = "";
    
    protected $_viewData;
    
    protected $_pageSize = 10;

    function __construct($model, $content = null){
//        ini_set('display_errors',1);
        parent::__construct();

        if (!\Emagid\Core\Membership::isInRoles(['admin'])){
            redirect(ADMIN_URL.'login/logout');
        }

        $this->_viewData = (object)[
            'logged_admin' => \Model\Admin::getItem(\Emagid\Core\Membership::userId()),
        ];

        $admin_permissions = strtolower($this->_viewData->logged_admin->permissions);
        $admin_permissions = str_replace(' ', '_', $admin_permissions);
        $admin_permissions = explode(',', $admin_permissions);
        if ($this->emagid->route['controller'] == 'dashboard' || $this->emagid->route['controller'] == 'reporting' || in_array($this->emagid->route['controller'], $admin_permissions, true)){
            $this->_model = "\Model\\".$model;
            $this->_content = is_null($content)?strtolower($model).'s':$content;
            if ($this->emagid->route['action'] == 'update'){
                $this->_viewData->page_title = 'Manage '.ucwords(str_replace('_', ' ', $model));
            } else {
                $this->_viewData->page_title = 'Manage '.ucwords(str_replace('_', ' ', $this->_content));
            }
            if (is_null($this->_viewData->logged_admin->permissions)){
                $this->_viewData->admin_sections = \Model\Admin::getNestedSections('');
            } else {
                $this->_viewData->admin_sections = \Model\Admin::getNestedSections($this->_viewData->logged_admin->permissions);
            }
        } else {
            redirect(ADMIN_URL.'login');
        }
    }
    
    public function index(Array $params = []){
        if ($this->emagid->route['controller'] == 'products'){
            $options = ['where'=>'active = 1', 'page_size'=>$params['limit']];
        } else {
            $options = ['where'=>'active = 1', 'page_size'=>$this->_pageSize];
        }
        
        if (isset($params['queryOptions'])){
            $options = array_merge($options, $params['queryOptions']);
        }

        $this->_viewData->new_orders =  \Model\Order::getCount(['where'=>"viewed is null or viewed = false"]);
        $this->_viewData->new_questions = \Model\Question::getList(['where'=>"status = '".\Model\Question::$status[0]."'"]);
        $page = new Pagination($this->_model, $options);
        $list = $page->getList();
        $page->current_page_index += 1;
        $this->_viewData->pagination = $page;
        
        $content = $this->_content;
        $this->_viewData->$content = $list;
        if($content == 'products'){
            $this->_viewData->page_title .= " ($page->total_records TOTAL)";
        }

        $this->beforeLoadIndexView();

        if (isset($params['overrideView'])){
            $this->loadView($params['overrideView'], $this->_viewData);
        } else {
            $this->loadView($this->_viewData);
        }
    }
    
    public function update(Array $arr = []){
        $obj = new $this->_model(isset($arr['id'])?$arr['id']:null);
                    $this->_viewData->new_orders =  \Model\Order::getCount(['where'=>"viewed is null or viewed = false"]);
            $this->_viewData->new_questions = \Model\Question::getList(['where'=>"status = '".\Model\Question::$status[0]."'"]);
        $content = str_replace("\model\\", "", strtolower($this->_model));
        $this->_viewData->$content = $obj;
        $this->_viewData->form = new Form($obj);

        $this->beforeLoadUpdateView();

        if (isset($arr['overrideView'])){
            $this->loadView($arr['overrideView'], $this->_viewData);
        } else {
            $this->loadView($this->_viewData);
        }
    }
    
    public function update_post() {
        $obj = new $this->_model($_POST); 

        if ($obj->save()){
            foreach($_FILES as $fileType=>$file){
                if ($file['error'] == 0){
                    $s3 = new EmagidService\S3();
                    $name = $s3->upload($_FILES[$fileType]);
                    $obj->$fileType = $name;
                    $obj->save();
                }
            }
            $this->update_relationships($obj);
            $this->afterObjSave($obj);
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content).' #'.$obj->id.' saved.');
            $_SESSION["notification"] = serialize($n);
        } else {
            $n = new \Notification\ErrorHandler($obj->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL.$this->_content.'/update/'.$obj->id);
        }

        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } else {
            redirect(ADMIN_URL.$this->_content);
        }
    }

    protected function update_relationships($obj) {
        $obj_class = new \ReflectionClass($obj); 
        $props = $obj_class->getStaticProperties();
        foreach ($props['relationships'] as $relationship) {
            $rel_class = new $relationship['class_name']();
            $rel_props = new \ReflectionClass($rel_class);
            $rel_props = $rel_props->getStaticProperties();
            if (isset($_POST[$relationship['name']])){
                $ids = $_POST[$relationship['name']];
                $existing = $rel_class::getList(['sql'=>'select * from '.$rel_props['tablename'].' where '.$relationship['remote'].' = '.$obj->id]);
                foreach($existing as $rel_obj){
                    $i = array_search($rel_obj->$relationship['remote_related'], $ids);
                    if ($i === false){
                        //deactivate record existent but not selected
                        $rel_class::delete($rel_obj->id);
                    } else {
                        //activate record existent and selected
                        if ($rel_obj->active != 1){
                            $this->emagid->getDb()->execute('update '.$rel_props['tablename'].' set active = 1 where id = '.$rel_obj->id);
                        }
                        unset($ids[$i]);                        
                    }
                }
                //insert new records
                foreach($ids as $newId){
                    $this->emagid->getDb()->execute('insert into '.$rel_props['tablename'].' ('.$relationship['remote'].', '.$relationship['remote_related'].') values ('.$obj->id.', '.$newId.')');
                }
            } else {
                //deactivate all records if none selected
                $this->emagid->getDb()->execute('update '.$rel_props['tablename'].' set active = 0 where '.$relationship['remote'].' = '.$obj->id);
            }
        }
    }
    
    public function delete(Array $arr = []) {
        $class = new $this->_model();
        $class::delete($arr['id']);

        $content = str_replace("\Model\\", "", $this->_model);
        $content = str_replace('_', ' ', $content);
        $n = new \Notification\MessageHandler(ucwords($content).' deleted.');
        $_SESSION["notification"] = serialize($n);

        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } else {
            redirect(ADMIN_URL.$this->_content);
        }
    }
    
    function delete_image($params) {
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
        if($id>0) {
            $class = new $this->_model();
            $obj = $class::getItem($id);
            if($obj==null) {
                // record does not exist, redirect to index page
                redirect(ADMIN_URL.$this->_content);
            } else {
                // successful delete of image, redirect to item page
                if (isset($_GET['featured_image']) && $_GET['featured_image'] == 1){
                    $imgField = 'featured_image';
                } else if (isset($_GET['banner']) && $_GET['banner'] == 1){
                    $imgField = 'banner';
                } else if(isset($_GET['swatch']) && $_GET['swatch'] == 1){
                    $imgField = 'swatch';
                } else if(isset($_GET['type'])){
                    $imgField = $_GET['type'];
                } else {
                    $imgField = 'image';
                }
                if($obj->$imgField!="") {
                    \Emagid\Image::delete($this->_content.DS.$obj->$imgField);
                    $s3 = new \EmagidService\S3();
                    $s3->delete($obj->$imgField);
                    $obj->$imgField = "";
                    $obj->save();
                    if (isset($_POST['redirectTo'])){
                        redirect($_POST['redirectTo']);
                    } else {
                        redirect(ADMIN_URL.$this->_content.'/update/'.$obj->id);
                    }
                }
            }
        }
        // invalid id for item or invalid file, redirect to index page
        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } else {
            redirect(ADMIN_URL.$this->_content);
        }
    }

    function admin_certify() {
        if (isset($_POST['pwd'])) {
            $password = \Emagid\Core\Membership::hash($_POST['pwd'], $this->_viewData->logged_admin->hash, null );
            if ($password['password'] == $this->_viewData->logged_admin->password){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function delete_prod_image($params) {
       $id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
       $product = \Model\Product_Image::getItem($id);
      $this->emagid->getDb()->execute('Delete  from  product_images where id='.$id);
       // invalid id for item or invalid file, redirect to index page
       if (isset($_POST['redirectTo'])){
           redirect($_POST['redirectTo']);
       } else {
           redirect(ADMIN_URL.'products/update/'.$product->product_id);
       }
    }

    public function generateSlugs(){
        $model = '\Model\Brand';
        $list = $model::getList();

        foreach($list as $obj){
            if (is_null($obj->slug) || trim($obj->slug) == ""){
                $slug = $obj->name;
                $slug = preg_replace ( "/[^\w-]/" , '-' , $slug );
                $slug = preg_replace ( "/[-]+/" , '-' , $slug );
                $slug = strtolower($slug);
                $obj->slug = self::uniqueSlug($model, $slug);
                $obj->save();
            }
        }

        dd($model::getList());
    }

    private static function uniqueSlug($model, $slug, $count = 0){
        $list = $model::getList(['where'=>"slug = '".$slug."' "]);
        if (isset($list) && count($list) > 0){
            return self::uniqueSlug($model, $slug.'-'.++$count, $count);
        } else {
            return $slug;
        }
    }

    public function download(Array $params = []){
        header('Content-Disposition: attachment; filename="'.$params['id'].'"');
        readfile(UPLOAD_PATH.$params['id']);
    }

    protected function afterImageUpload($image){}
    protected function afterObjSave($obj){}
    protected function beforeLoadIndexView(){}
    protected function beforeLoadUpdateView(){}

    public function toJson($array)
    {
        header('Content-Type: application/json');
        echo json_encode($array);
        exit();
    }

}





































