<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 5/18/16
 * Time: 4:34 PM
 */

class status_managementController extends adminController {
    function __construct(){
        parent::__construct("Order_Status", 'status_management');
        if ($this->emagid->route['action'] == 'update'){
            $this->_viewData->page_title = 'Manage Order Status';
        } else {
            $this->_viewData->page_title = 'Manage Order Status';
        }
    }

    function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        $params['queryOptions'] = ['orderBy' => 'id asc'];
        parent::index($params);
    }

    public function status()
    {
        $statusMessage = [];
        $order = \Model\Order::getItem($_GET['orderId']);
        $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);
        $products_mpn=[];
        $brands=[];
        foreach ($order_products as $op) {
            $product = \Model\Product::getItem($op->product_id);
            $brand = \Model\Brand::getItem($product->brand);
            $products_mpn[] = $product->mpn;
            $brands[] = $brand->name ;
        }
        foreach(\Model\Order_Status::getList() as $status){
            /**
             * Add more here if needed
             */
            $temp = array('*|FIRST_NAME|*','*|BRAND|*','*|MPN|*','*|ORDER#|*');
            $real = array($order->bill_first_name,implode(',', $brands),implode(',', $products_mpn), $order->id);
            $message = str_replace($temp, $real, $status->message);
            $subject = str_replace($temp, $real,$status->subject);
            $mailtitle = str_replace($temp, $real,$status->mailtitle);

            $statusMessage[$status->name] = [$subject,$message,$mailtitle];
        }

        $message = isset($statusMessage[$_GET['status']])?$statusMessage[$_GET['status']]:'';
        echo json_encode(['subject' => $message[0],'text'=>$message[1],'mailtitle'=>$message[2]]);
    }
}