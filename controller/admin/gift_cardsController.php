<?php

class gift_cardsController extends adminController{
    public function __construct()
    {
        parent::__construct('Gift_Card');
    }

    public function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $params['queryOptions']['where'] = 'gift_card_type = 1';
        $this->_viewData->gift_obj = \Model\Gift_Card::getList(['where'=>'gift_card_type = 2']);
        parent::index($params); // TODO: Change the autogenerated stub
    }

    public function update_post()
    {
        $dateRange = explode('-',$_POST['daterange']);
        if($_POST['old_amount'] != $_POST['amount']){
            $_POST['remaining'] = $_POST['amount'];
        }
        $_POST['remaining'] = $_POST['remaining'] ? : $_POST['amount'];
        $_POST['start_date'] = date('Y-m-d H:i:s',strtotime(trim($dateRange[0])));
        $_POST['end_date'] = date('Y-m-d H:i:s',strtotime(trim($dateRange[1])));
        parent::update_post(); // TODO: Change the autogenerated stub
    }

    function generateGiftCode(){
        echo strtoupper(generateToken());
    }
}