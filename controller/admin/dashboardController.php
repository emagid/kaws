<?php

use Emagid\Core\Membership,
    Emagid\Html\Form,
    Model\Admin,
    Model\Banner;

class dashboardController extends adminController {

    function __construct(){
        parent::__construct('Dashboard');
    }
    public function index(Array $params = []){
        $logged_admin = $this->_viewData->logged_admin ? : null;
        $firstElement = reset($this->_viewData->admin_sections);
        $firstKey = key($this->_viewData->admin_sections);
        if($logged_admin && $logged_admin->username != 'emagid'){
            if(!empty($firstElement)){
                redirect(ADMIN_URL.strtolower($firstElement[0]));
            } else {
                redirect(ADMIN_URL.strtolower($firstKey));
            }
        }
//		$this->_viewData->new_orders = \Model\Order::getCount(['where'=>"viewed is null or viewed = false"]);
        $this->_viewData->new_orders = 0;

//		$this->_viewData->recent_orders = \Model\Order::getList(['orderBy'=>'insert_time DESC', 'limit'=>5]);
        $this->_viewData->recent_orders = [];

//		$this->_viewData->new_questions = \Model\Question::getList(['where'=>"status = '".\Model\Question::$status[0]."'"]);
        $this->_viewData->new_questions = [];

        $statusList = \Model\Order::$status;
        unset($statusList[2]);unset($statusList[6]);unset($statusList[8]);unset($statusList[10]);unset($statusList[11]);
        $allStatus = implode("','",$statusList);
//		$this->_viewData->monthly_sales = \Model\Order::getList(['where'=>"status in ('".$allStatus."') and EXTRACT(MONTH FROM insert_time)::INTEGER = ".date('m')." and EXTRACT(YEAR FROM insert_time)::INTEGER = ".date('Y')]);
        $this->_viewData->monthly_sales = [];
        $this->_viewData->monthly_sum = 0;
        foreach($this->_viewData->monthly_sales as $sale){
            $this->_viewData->monthly_sum += $sale->total;
        }

        $previous = \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->hour(23)->minute(59)->second(59)->toDateTimeString();
        $beforePrevious = \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeek()->toDateTimeString();

//		$this->_viewData->weekly_sales = \Model\Order::getList(['where'=>"status in ('".$allStatus."') and insert_time between '$beforePrevious' and '$previous'"]);
        $this->_viewData->weekly_sales = [];
        $this->_viewData->weekly_sum = 0;
        foreach($this->_viewData->weekly_sales as $item){
            $this->_viewData->weekly_sum += $item->total;
        }

        $this->_viewData->recent_clients = \Model\User::getList(['orderBy'=>'insert_time DESC', 'limit'=>5]);
        foreach($this->_viewData->recent_clients as $client){
            $client->insert_time = new DateTime($client->insert_time);
            $client->insert_time = $client->insert_time->format('m-d-Y H:i:s');
        }

        $this->_viewData->page_title = 'Dashboard';
        $this->setGraphsData();
        $this->loadView($this->_viewData);
    }

    private function setGraphsData(){
        $dataset = [
            'previous'=>[
                date("Y-m", mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")-1))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")-1))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")-1))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")-1))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")-1))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")-1))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m"), date("d"),   date("Y")-1))=>(object)[]
            ],
            'current'=>[
                date("Y-m", mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")))=>(object)[],
                date("Y-m", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")))=>(object)[],
                date("Y-m")=>(object)[]
            ]
        ];
        $weeklySet = [
            'previous'=>[
                \Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(6)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(5)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(4)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(3)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(2)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(1)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->format('Y-m-d')=>(object)[]
            ],
            'current'=>[
                \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(6)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(5)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(4)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(3)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(2)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(1)->format('Y-m-d')=>(object)[],
                \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->format('Y-m-d')=>(object)[]
            ]
        ];
        foreach($dataset as $period=>$set){
            foreach($set as $date=>$data){
                $year = explode('-', $date)[0];
                $month = explode('-', $date)[1];
                $dataset[$period][$date] = \Model\Order::getDashBoardData($year, $month);
            }
        }
        foreach($weeklySet as $period=>$set){
            foreach($set as $date=>$obj){
                $present = \Carbon\Carbon::createFromTimestamp(strtotime($date));
                $past = \Carbon\Carbon::createFromTimestamp(strtotime($date))->subWeek();
                $weeklySet[$period][$date] = \Model\Order::getWeeklyDashboard($present,$past);
            }
        }
        $this->_viewData->graphsData = $dataset;
        $this->_viewData->weeklySet = $weeklySet;
    }
}