<?php
class wishlistController extends adminController {
  
	function __construct(){
		parent::__construct("User");
		if ($this->emagid->route['action'] == 'update'){
			$this->_viewData->page_title = 'Manage Wishlist';
		} else {
			$this->_viewData->page_title = 'Manage Wishlist';
		}
	}

	function index(Array $params = []){
//        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->wishlist = \Model\User_Favorite::getList();
		$this->_viewData->coupons = \Model\Coupon::getList();
        parent::index($params);
    }

   
	function send_coupon() {
		if((!isset($_POST['id'])) || (($_POST['coupon']=="none")))
		{
			 
			$n = new \Notification\ErrorHandler('You must select coupon and user row');
           	$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'wishlist');
		}
		else{
	   		$coupon = \Model\Coupon::getItem($_POST['coupon']);
	   		if($coupon->type==1){
	   			$type = "$";
	   		}else{
	   			$type = "%";
	   		}
			for ($x = 0; $x < count($_POST['id']); $x++){
				$wishlist = \Model\Wishlist::getItem($_POST['id'][$x]);
				$user = \Model\User::getItem($wishlist->user_id);
	   			$product = \Model\Product::getItem($wishlist->product_id);
	   			global $emagid;
				$emagid->email->from->email = 'noreply@itmbustbetime.com';
				$email = new \Emagid\Email();
				$email->addTo($user->email);
				$email->subject($type.$coupon->discount_amount.'  off for '.$product->name);
				$email->body = "Hello, $user->first_name $user->last_name! Get $type$coupon->discount_amount off for $product->name! Just use this promocode:$coupon->code!";
				if($email->send()){
					echo "Email for $user->first_name $user->last_name whith $type$coupon->discount_amount off for $product->name has been sent!";
				 }
			}
			$n = new \Notification\MessageHandler('Coupon mails are sent!');
           	$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'wishlist');
		}
		 
	    
	 /*  $coupon_id= $_POST['coupon_id']; 
	   $user_id = $_POST['user_id']; 
	   $product_id	= $_POST['product_id'];

	   $user = \Model\User::getItem($user_id);
	   $coupon = \Model\Coupon::getItem($coupon_id);
	   $product = \Model\Product::getItem($product_id);*/

		/*global $emagid;
		$emagid->email->from->email = 'noreply@itmbustbetime.com';

		$email = new \Emagid\Email();

		$email->addTo($user->email);
		$email->subject($coupon->discount_amount.'% off for '.$product->name);
		$email->body = "Hello, $user->last_name $user->first_name! Get $coupon->discount_amount% off for $product->name! Just use this promocode:$coupon->code!";
		if($email->send()){
			echo "Email for $user->last_name $user->first_name whith $coupon->discount_amount% off for $product->name has been sent!";
		 }*/
    }
}
































