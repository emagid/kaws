<?php

class shipping_methodsController extends adminController {
	
	function __construct(){
		parent::__construct("Shipping_Method");
	}

	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		

		parent::index($params);
	}

	function beforeLoadIndexView(){
		foreach ($this->_viewData->shipping_methods as $shipping_method){
			$shipping_method->cost = json_decode($shipping_method->cost);
			$shipping_method->cart_subtotal_range_min = implode(' / ', json_decode($shipping_method->cart_subtotal_range_min));
		}
	}

	function beforeLoadUpdateView(){
		$this->_viewData->shipping_method->cost = json_decode($this->_viewData->shipping_method->cost);
		$this->_viewData->shipping_method->cart_subtotal_range_min = json_decode($this->_viewData->shipping_method->cart_subtotal_range_min);
	}

	function update_post(){
		$obj = new $this->_model($_POST); 

		$obj->cost = json_encode($obj->cost);
		$obj->cart_subtotal_range_min = json_encode($obj->cart_subtotal_range_min);

    	if ($obj->save()){
    		if ($obj->is_default){
    			\Model\Shipping_Method::clearDefault($obj->id);
    		}
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content).' saved.');
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.$this->_content);
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.$this->_content.'/update/'.$obj->id);
    	}
	}
  
}