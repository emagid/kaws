<?php

class questionsController extends adminController {
	
	function __construct(){
		parent::__construct("Question");
	}

    public function beforeLoadIndexView(){
    	
     	foreach($this->_viewData->questions as $question){
		 	$question->product = \Model\Product::getItem($question->product_id);
		 	$question->insert_time = new DateTime($question->insert_time);
		 	$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');

		}
			
    }

    public function beforeLoadUpdateView(){
    	$this->_viewData->question_product = \Model\Product::getItem($this->_viewData->question->product_id);
    	$this->_viewData->answers = \Model\Answer::getList();
    	if (is_null($this->_viewData->question->viewed) || !$this->_viewData->question->viewed){
    		$this->_viewData->question->viewed = true;
    		$this->_viewData->question->save();
    	}
	}
	 public function multi_delete(){
    	 if(!$_POST){
    	 		$n = new \Notification\ErrorHandler("You have to select as minimum 1 question");
	           	$_SESSION["notification"] = serialize($n);
	           	redirect(ADMIN_URL.'questions/');
    	 }else{
    	 	foreach ($_POST as $key => $value) {
    			$this->emagid->getDb()->execute("DELETE from question  where  id = '" . $value. "' ");
    	 	}
    	 		$n = new \Notification\MessageHandler('Questions removed!');
	           	$_SESSION["notification"] = serialize($n);
	           	redirect(ADMIN_URL.'questions/');
    	 }
	}
	public function update_post(Array $params = []){
		if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0){
			$question = \Model\Question::getItem($_POST['id']);

			$question->status = $_POST['status'];
			$question->answer = $_POST['answer'];
			if($question->answer){
				$question->admin_reply = 1;
			}
			$question->answer = $_POST['answer'];

//			$question_answer  = new \Model\Question_Answer();
//			$question_answer->answer = $_POST['answer'];
//			$question_answer->author_id = \Emagid\Core\Membership::userId();
//			$question_answer->question_id = $_POST['id'];
//			$question_answer->save();
//			if ($question->status =="Approved")
//			{
//				TODO st-dev uncomment when preparing to send email
//				$email = new \Emagid\Email();
//				$product = \Model\Product::getItem($question->product_id);
//
//				$email->addBcc($question->reply_email);
//				$email->addBcc('info@modernvice.com'); //TODO st-dev get valid email
//				$email->subject('You have the one new answer!');
//				$email->body .= '<p><a href="www.modernvice.com"><img src="https://modernvice.com/content/frontend/img/logo.png" /></a></p><br />'
//							   .'<p>You  have the answer about your question:</p>';
//
//				$email->body .= '<h2>';
//				$email->body .=	'<a href="http://modernvice.com/product/'.$product->slug.'">'.$product->name.'</a>';
//				$email->body .= '</h2>';
//				$email->body .= '<p>Subject:'.$question->subject.'</p>';
//				$email->body .= '<p>'.$question->text_question.'</p>';
//				$email->body .= '<p>------------------------------------</p>';
//				$email->body .= '<p>Answer:</p>';
//				$email->body .= '<p>'.$question->answer.'</p>';
//
//				$email->send();
//			}
			  

			  if ($question->save()){
				$n = new \Notification\MessageHandler('Question saved.');
	           	$_SESSION["notification"] = serialize($n);
	    	} else {
	    		$n = new \Notification\ErrorHandler($question->errors);
	           	$_SESSION["notification"] = serialize($n);
	           	redirect(ADMIN_URL.$this->_content.'/update/'.$question->id);
	    	}  
		}
		redirect(ADMIN_URL.'questions');
	}
  	
}