<?php

class ticket_statusController extends adminController{
    function __construct()
    {
        parent::__construct('Service_Status');
    }

    public function index(Array $params = [])
    {

        $this->_viewData->hasCreateBtn = true;
        $this->_pageSize = 100;
        parent::index($params);
    }


    function update_post()
    {
        $id = $_POST['id'];
        $_POST['redirectTo'] = "/admin/ticket_status/update/$id";
        parent::update_post();
    }
}