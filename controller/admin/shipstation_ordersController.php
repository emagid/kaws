<?php

use Emagid\Html\Form;
use Emagid\Core\Membership;

//require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class shipstation_ordersController extends adminController
{

    function __construct()
    {
        parent::__construct("Order");
    }

    public function index(Array $params = [])
    {
        set_time_limit(8);
        $ss = new ShipStation();
        $ss->setSsApiKey(SHIPSTATION_KEY);
        $ss->setSsApiSecret(SHIPSTATION_SECRET);

        $shipStores = $ss->getStores();
        $stores = [];
        $defaultStore = ''; /** first store */
        foreach($shipStores as $key=>$store){
            if($key == 0){
                $defaultStore = $store->storeId;
            }
            $stores[$store->storeId] = $store->storeName." ($store->marketplaceName)";
        }
        //$stores['emagid'] = 'eMagid (Website)';
        $this->_viewData->stores = $stores;
        $filterParams = [];

        $orderParams = [];
        $orderParams['page'] = 1;
//        $orderParams['storeId'] = $defaultStore;
        $orderParams['pageSize'] = 50;
        $orderParams['sortDir'] = 'desc';
        $isEmagidWebsite = false;

        if (!Empty($_GET['how_many']))
        {
            $orderParams['pageSize'] = $filterParams['how_many'] = $_GET['how_many'];
        }

        if(isset($_GET['page'])){
            $orderParams['page'] = $filterParams['page'] = $_GET['page'];
        }

        if(isset($_GET['store'])){
            $orderParams['storeId'] = $filterParams['store'] = $_GET['store'];
            if($filterParams['store'] == 'emagid') $isEmagidWebsite = true;
        }

        $this->_viewData->start = '';
        $this->_viewData->end = '';


        if(isset($_GET['t'])){
            $get = explode(',',urldecode($_GET['t']));
            $start = date('Y-m-d H:i:s',$get[0]); $end = date('Y-m-d H:i:s',$get[1]);
            $this->_viewData->start = $start;
            $this->_viewData->end = $end;
            $orderParams['orderDateStart'] = $start;
            $orderParams['orderDateEnd'] = $end;
            $filterParams['t'] = $_GET['t'];
        }

        if(isset($_GET['id'])){
            $ssOrder = $ss->getOrder($_GET['id']);
            $ssOrders = new stdClass();
            if($ssOrder){
                $ssOrders->orders = [$ssOrder];
            } else {
                if($foundOrder = \Model\Order::getItem($_GET['id'])){
                    $ssOrders->orders= [\Model\Order::wrapShipstation($foundOrder)];
                }

            }
            $ssOrders->total = 1;
            $ssOrders->pages = 1;
            $ssOrders->page = 1;
        } else {
            if($isEmagidWebsite){
                $ssOrders = new stdClass();
                $ssOrders->orders = \Model\Order::getShipstationOrder($orderParams);
                $ssOrders->total = 1;
                $ssOrders->pages = 1;
                $ssOrders->page = 1;
            } else {
                $ssOrdersEmagid = \Model\Order::getShipstationOrder($orderParams);
//                dd($ssOrdersEmagid);
                $ssOrders = $ss->getOrders($orderParams);
                $ssOrders->orders = array_merge($ssOrders->orders, $ssOrdersEmagid);
            }
            
        }

        $this->_viewData->ssOrders = $ssOrders;
        $this->_viewData->params = $filterParams;
//        dd($ssOrders,$orderParams,$this->_viewData);
        parent::index($params);
    }

    function export(Array $params = []){
        $this->template = false;
        $ss = new ShipStation();
        $ss->setSsApiKey(SHIPSTATION_KEY);
        $ss->setSsApiSecret(SHIPSTATION_SECRET);

        $orderParams['pageSize'] = 500;
        if(isset($_GET['t'])){
            $get = explode(',',urldecode($_GET['t']));
            $start = date('Y-m-d H:i:s',$get[0]); $end = date('Y-m-d H:i:s',$get[1]);
            $orderParams['orderDateStart'] = $start;
            $orderParams['orderDateEnd'] = $end;
        }

        $ssOrders = $ss->getOrders($orderParams);

        $shipStores = $ss->getStores();
        $stores = [];
        foreach($shipStores as $key=>$store){
            $stores[$store->storeId] = $store->storeName." ($store->marketplaceName)";
        }

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=orders-'.\Carbon\Carbon::now()->format('Y-m-d').'.csv');
        $output = fopen('php://output', 'w');
        $t = ['Id','Date','Tracking#','Status','Store','Bill Name','Products','Total'];
        fputcsv($output, $t);
        while($ssOrders->page <= $ssOrders->pages){
            foreach($ssOrders->orders as $o) {
                $products = [];
                foreach($o->items as $item){
                    $products[] = str_replace(["\r","\n"],'',strReplace($item->name, 50, '...'));
                }

                $row = [
                    $o->orderId,
                    date('M j, Y h:i:sa', strtotime($o->createDate)),
                    '',
                    $o->orderStatus,
                    $stores[$o->advancedOptions->storeId],
                    $o->shipTo->name,
                    implode('||', $products),
                    '$'.$o->orderTotal
                ];
                fputcsv($output, $row);
            }
            $orderParams['page'] = $ssOrders->page + 1;
            $ssOrders = $ss->getOrders($orderParams);
        }
    }

    public function export_emails(Array $params = [])
    {
        $this->template = false;
        $ss = new ShipStation();
        $ss->setSsApiKey(SHIPSTATION_KEY);
        $ss->setSsApiSecret(SHIPSTATION_SECRET);

        $orderParams['pageSize'] = 500;
        $orderParams['storeId'] = '186177';
        $ssOrders = $ss->getOrders($orderParams);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=order-emails-'.\Carbon\Carbon::now()->format('Y-m-d').'.csv');
        $output = fopen('php://output', 'w');
        $t = ['Email'];
        fputcsv($output, $t);
        while($ssOrders->page <= $ssOrders->pages){
            foreach($ssOrders->orders as $o) {
                if(!$o->customerEmail) continue;
                $row = [
                    $o->customerEmail
                ];
                fputcsv($output, $row);
            }
            $orderParams['page'] = $ssOrders->page + 1;
            $ssOrders = $ss->getOrders($orderParams);
        }
    }

    public function beforeLoadIndexView()
    {
        $this->_viewData->products = [];
        foreach ($this->_viewData->orders as $order) {
            $order->insert_time = new DateTime($order->insert_time);
            $order->insert_time = $order->insert_time->format('m-d-Y H:i:s');
            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);
            $this->_viewData->products[$order->id] = [];
            foreach ($order_products as $order_product) {
                $product = \Model\Product::getItem($order_product->product_id);
                $product->brand_name = \Model\Brand::getItem($product->brand)->name;
                $this->_viewData->products[$order->id][] = $product;
            }
        }
    }

    public function update(Array $arr = [])
    {
        $ss = new ShipStation();
        $ss->setSsApiKey(SHIPSTATION_KEY); $ss->setSsApiSecret(SHIPSTATION_SECRET);
        $order = $ss->getOrder($arr['id']);
        if($l_order = \Model\Order::getItem($order->orderNumber)){
            $l_order->tracking_number = $order;
        }
        $this->_viewData->ssOrder = $order;
        $this->_viewData->comments = \Model\Shipstation_Log::getList(['where' => ['shipstation_id' => $arr['id']]]);
        parent::update($arr); // TODO: Change the autogenerated stub
    }

    public function create_order(Array $arr = []){
        parent::update($arr);
    }
    
    public function create_comment(){
        $id = $_POST['id'];
        $content = $_POST['content'];
        \Model\Shipstation_Log::log($id, $content);
    }

    //Admin checkout flow -- because we're crazy
    public function create_order_post(){
        //Set Subtotal
        $subtotal = 0;
        for($i = 0; $i < count($_POST['product_id']); $i++){
            if($_POST['product_id'][$i] > 0 && $_POST['custom_price'][$i] == ''){
                $product = \Model\Product::getItem($_POST['product_id'][$i]);
                $subtotal += $product->getPrice($_POST['color'][$i]);
            } else if($_POST['custom_price'] > 0){
                $subtotal += $_POST['custom_price'][$i];
            }
        }
        $_POST['subtotal'] = $subtotal;
        $calculatedTotal = $subtotal;

        if(isset($_POST['gift_card']) && ($gc = \Model\Gift_Card::validateGiftCard($_POST['gift_card']))){
            if($gc->getRemainingAmount() > $subtotal){
                $_POST['gift_card_amount'] = $gift_card_amount = $subtotal;
                $_POST['payment_method'] = 5;
                $calculatedTotal = 0;
            } else {
                $_POST['gift_card_amount'] = $gift_card_amount = round($gc->getRemainingAmount(),2);
                $calculatedTotal -= $gift_card_amount;
            }
        }
        //Set Taxes
//        if((isset($_POST['tax-free']) && $_POST['tax-free']) || $_POST['ship_state'] != 'NY'){
        if((isset($_POST['tax-free']) && $_POST['tax-free'])){
            $_POST['tax_rate'] = 0;
            $_POST['tax'] = $tax = 0;
        } else {
            $_POST['tax_rate'] = 8.875;
            $_POST['tax'] = $tax = round($calculatedTotal * .08875,2);
        }
        $calculatedTotal += $_POST['tax'];

        //Set shipping cost
        if($_POST['ship_country'] == 'United States'){
            $shipping_cost = 0;
        } else if($shipping = \Model\Shipping_Method::getItem(null,['where'=>"country = '{$_POST['ship_country']}'"])){
            $shipping_cost = json_decode($shipping->cost,true)[0];
        } else {
            $shipping_cost = 80;
        }
        $calculatedTotal += $shipping_cost;
        $_POST['shipping_cost'] = round($shipping_cost,2);

        //Set discount -- N/A
        $discount = 0;
        $calculatedTotal -= $discount;
//        $coupon = \Model\Coupon::getItem(null,['where'=>"code = '{$_POST['coupon_code']}'"]);
//        $_POST['coupon_type'] = $coupon->discount_type;
//        $_POST['coupon_amount'] = $coupon->discount_amount;
//        $discount = $coupon->applyCoupon($subtotal);

        //Set guest_id
        $_POST['guest_id'] = session_id();

        $_POST['status'] = 'New';
        $_POST['ref_num'] = generateToken();

        //Set total
        $_POST['total'] = $calculatedTotal;

        $_POST['insert_time'] = date('Y-m-d H:i:s.u');
        $order = \Model\Order::loadFromPost();
        $order->product_id=$order->color=$order->size=$order->misc_name=$order->custom_price='';

        if($order->save()){
            if(isset($_GET['inv']) && $_GET['inv'] && ($invoice = \Model\Invoice::getByRef($_GET['inv']))){
                $invoice->order_id = $order->id;
                $invoice->save();
            }
            $html = '';
            for($i = 0; $i < count($_POST['product_id']); $i++){
                $orderProduct = new \Model\Order_Product();
                $orderProduct->order_id = $order->id;
                $orderProduct->product_id = $_POST['product_id'][$i];
                $orderProduct->quantity = 1; //add quantity
                if($_POST['product_id'][$i] > 0){ //id is product
                    $product = \Model\Product::getItem($_POST['product_id'][$i]);
                    $product->total_sales += 1;
                    $product->save();
                    $unit_price = $_POST['custom_price'][$i] ? : $product->price($_POST['color'][$i]);
                    $product_image = $product->featuredImage(null,$_POST['color'][$i]);
                    $product_name = $product->name;
                    $product_price = $unit_price * $orderProduct->quantity;
                    $product_color = \Model\Color::getItem($_POST['color'][$i])->name;
                    $product_size = \Model\Size::getItem($_POST['size'][$i]);

                    $orderProduct->unit_price = $unit_price;
                    $orderProduct->details = json_encode(['color'=>$_POST['color'][$i], 'size'=>$_POST['size'][$i]]);
                    $orderProduct->save();
                } else { //id is miscellaneous charge
                    $unit_price = $_POST['custom_price'][$i];
                    $product_image = '';
                    $product_name = $_POST['misc_name'][$i];
                    $product_price = $unit_price * $orderProduct->quantity;
                    $product_color = '';
                    $product_size = new stdClass(); $product_size->us_size = ''; $product_size->eur_size = '';

                    $orderProduct->unit_price = $unit_price;
                    $orderProduct->details = json_encode(['misc_name'=>$_POST['misc_name'][$i]]);
                }
                $orderProduct->save();

                $html .=
                    '<tr style="height:75.0pt">
                            <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p class="MsoNormal"><img width="100px!important;" src="https://djinyc.com'.UPLOAD_URL.'products/'.$product_image.'"></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product_name.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($product_price,2).'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product_color.'</span></p>
                            </td>
                            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">US: '.$product_size->us_size.'<br>EU: '.$product_size->eur_size.'</span></p>
                            </td>
                        </tr>';
            }

            if($order->payment_method == 1) {
                $expiry = str_pad($order->cc_expiration_month, 2, 0) . substr($order->cc_expiration_year, -2);
                $firstData = new Payment\FirstData(PAYEEZY_ID, PAYEEZY_PASSWORD);
                $firstData->setAmount($order->total);
                $firstData->setCreditCardNumber($order->cc_number);
                $firstData->setCreditCardName($order->card_name);
                $firstData->setCreditCardType($_POST['cc_type']);
                $firstData->setCreditCardVerification($order->ccv);
                $firstData->setCreditCardExpiration($expiry);
                $firstData->setCurrency('USD');
                $firstData->setReferenceNumber($order->id);
                if ($order->user_id) {
                    $firstData->setCustomerReferenceNumber($order->user_id);
                }
                $firstData->process();
                if ($firstData->isSuccess()) {
                    $order->status = \Model\Order::$status[1];
                    $order->save();

                    $email = new \Email\MailMaster();
                    $mergeFields = [
                        'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
                        'DATE' => date('Y-m-d h:i:s'),
                        'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                        'SHIPPING' => $order->getShippingAddr(),
                        'BILLING' => $order->getBillingAddr(),
                        'ITEMS' => $html,
                        'SUBTOTAL' => number_format($order->subtotal, 2),
                        'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                        'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                        'TAXFEE' => number_format($order->tax, 2),
                        'TOTAL' => number_format($order->total, 2)
                    ];
                    $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();

                    if($order->gift_card && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card)) && $order->gift_card_amount){
                        $giftCard->removeAmount($order->gift_card_amount);
                    }
                    redirect('/admin/orders/update/' . $order->id);
                } else {
                    $order->status = \Model\Order::$status[2];
                    $order->error_message = $firstData->getErrorMessage();
                    $order->save();
                    $n = new \Notification\ErrorHandler($firstData->getErrorMessage());
                    $_SESSION["notification"] = serialize($n);
                    redirect('/admin/orders/create_order');
                }
            } else if($order->payment_method == 3){
                $order->status = \Model\Order::$status[1];
                $order->save();

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
                    'DATE' => date('Y-m-d h:i:s'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format($order->tax, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];

                if($order->gift_card && $order->gift_card_amount && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card))){
                    $giftCard->removeAmount($order->gift_card_amount);
                }
                
                $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();
                redirect('/admin/orders/update/' . $order->id);
            } else if($order->payment_method == 5){
                $order->status = \Model\Order::$status[1];
                $order->save();

                if($order->gift_card && $order->gift_card_amount && ($giftCard = \Model\Gift_Card::validateGiftCard($order->gift_card))){
                    $giftCard->removeAmount($order->gift_card_amount);
                }

                $email = new \Email\MailMaster();
                $mergeFields = [
                    'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
                    'DATE' => date('Y-m-d h:i:s'),
                    'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
                    'SHIPPING' => $order->getShippingAddr(),
                    'BILLING' => $order->getBillingAddr(),
                    'ITEMS' => $html,
                    'SUBTOTAL' => number_format($order->subtotal, 2),
                    'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
                    'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
                    'TAXFEE' => number_format($order->tax, 2),
                    'TOTAL' => number_format($order->total, 2)
                ];
                $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();
                redirect('/admin/orders/update/' . $order->id);
            } else {
                $order->status = \Model\Order::$status[2];
                $order->save();
                $n = new \Notification\ErrorHandler('Invalid payment method ('.\Model\Order::$payment_method[$order->payment_method].'). Please try again.');
                $_SESSION["notification"] = serialize($n);
                redirect('/admin/orders/create_order');
            }
        } else {
            $n = new \Notification\ErrorHandler(implode('<br/>',array_map(function($error){return $error['message'];},$order->errors)));
            $_SESSION["notification"] = serialize($n);
            redirect('/admin/orders/create_orders');
        }
    }

    public function update_post()
    {
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {

            $order = \Model\Order::getItem($_POST['id']);

            /*if ($order->status != $_POST['status'] && $_POST['status'] == \Model\Order::$status[3] && trim($_POST['tracking_number']) != ""){
                $this->sendShippedEmail($order, $_POST['tracking_number']);
            }*/

            if (isset($_POST['status'])) {
                $order->status = $_POST['status'];
                if ($_POST['status'] !== $_POST['old_status']) {
                    $log_status = new \Model\Log_Status();
                    $log_status->order_id = $_POST['id'];
                    $log_status->status = $_POST['status'];
                    $log_status->admin_id = \Emagid\Core\Membership::userId();
                    $log_status->save();
                }


            }

            $order->tracking_number = $_POST['tracking_number'];
            if (isset($_POST['cc_number'])) {
                if ('****' . substr($order->cc_number, -4) != $_POST['cc_number']) {
                    $order->cc_number = $_POST['cc_number'];
                }
            }
            if (isset($_POST['cc_expiration_month'])) {
                $order->cc_expiration_month = $_POST['cc_expiration_month'];
            }
            if (isset($_POST['cc_expiration_year'])) {
                $order->cc_expiration_year = $_POST['cc_expiration_year'];
            }

            if (isset($_POST['cc_ccv'])) {
                $order->cc_ccv = $_POST['cc_ccv'];
            }


            if (isset($_POST['payment_method'])) {
                $order->payment_method = $_POST['payment_method'];
            }

            $order->bill_first_name = $_POST['bill_first_name'];
            $order->bill_last_name = $_POST['bill_last_name'];
            $order->bill_address = $_POST['bill_address'];
            $order->bill_address2 = $_POST['bill_address2'];


            if (isset($_POST['bill_state'])) {
                $order->bill_state = $_POST['bill_state'];
            }
            if (isset($_POST['bill_country'])) {
                $order->bill_country = $_POST['bill_country'];
            }


            $order->bill_city = $_POST['bill_city'];


            $order->bill_zip = $_POST['bill_zip'];

            $order->ship_first_name = $_POST['ship_first_name'];
            $order->ship_last_name = $_POST['ship_last_name'];
            $order->ship_address = $_POST['ship_address'];
            $order->ship_address2 = $_POST['ship_address2'];


            if (isset($_POST['ship_state'])) {
                $order->ship_state = $_POST['ship_state'];
            }
            if (isset($_POST['ship_country'])) {
                $order->ship_country = $_POST['ship_country'];
            }


            $order->ship_zip = $_POST['ship_zip'];
            $order->note = $_POST['note'];

            $order->phone = $_POST['phone'];
//            $subtotal = $order->subtotal;
//            if (!is_null($order->coupon_code)) {
//                if ($order->coupon_type == 1) {
//                    $subtotal = $subtotal - $order->coupon_amount;
//                } else if ($order->coupon_type == 2) {
//                    $subtotal = $subtotal * (1 - ($order->coupon_amount / 100));
//                }
//            }

            /*$logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());  
            $orderchange = new \Model\Orderchange();  
            $orderchange->order_id = $_POST['id'];
            $orderchange->user_id =  $logged_admin->id;
            $orderchange->save() ; */


            if (!empty($_POST['mail'])) {
                $email = new \Emagid\Email();
                global $emagid;
                $emagid->email->from->email = 'orders@djinyc.com'; //TODO st-dev get email
                $mail_log = new \Model\Mail_Log();
                $mail_log->from_ = "orders@djinyc.com"; //TODO st-dev get email

                if (is_null($order->user_id)) {
                    $email->addTo($order->email);
                    $mail_log->to_ = $order->email;
                } else {
                    $email->addTo(\Model\User::getItem($order->user_id)->email);
                    $mail_log->to_ = \Model\User::getItem($order->user_id)->email;
                }
                $email->addBcc("orders@djinyc.com"); //TODO st-dev get email
                if ($_POST['status'] !== $_POST['old_status']) {
                    $subject_array = Array();
                    foreach (\Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]) as $order_product) {
                        $product = \Model\Product::getItem($order_product->product_id);
                        $brand = \Model\Brand::getItem($product->brand);
                        $subject_array[] = $brand->name;
                        $subject_array[] = $product->mpn;

                        //$subject_array[] = $order_product->id;
                    }
                    if ($_POST['status'] == "Shipped") {

                        $email->subject('Your order has shipped! ' . implode(" ", $subject_array) . " DJI Order #" . $order->id);
                        $mail_log->subject = 'Your order has shipped! ' . implode(" ", $subject_array) . "DJI Order #" . $order->id;
                    } else {
                        $email->subject('Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " DJI Order #" . $order->id);
                        $mail_log->subject = 'Order successfully ' . $order->status . ' ' . implode(" ", $subject_array) . " DJI Order #" . $order->id;
                    }
                }
                $subject = $_POST['subject'];
                $mailtitle = $_POST['mailtitle'];
                $status = $_POST['status'];
                $text = $_POST['mail'];
                $email->subject($subject);
                $email->body = '<table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
<tbody>
<tr>
<td>
<center><img border="0"   src="https://djinyc.com/content/frontend/img/logo.png" alt="DJI"></center>
</td>
</tr>
</tbody>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="850" style="width:600pt">
<tbody>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt"></td>
    </tr>
    <tr style="height:.75pt">
        <td width="650" style="width:487.5pt;padding:0in 0in 0in 0in;height:.75pt">
            <table border="0" cellspacing="0" cellpadding="0" width="650" style="width:600pt">
                <tbody>
                    <tr style="height:22.5pt">
                        <td width="424" rowspan="2" style="width:318.0pt;border:solid #414042 1.0pt;border-left:none;background:#dcddde;padding:0in 0in 0in 15.0pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:24.0pt;font-family:&quot;Arial&quot;,sans-serif;color:black">' . $mailtitle . '</span></b> </p>
                        </td>
                        <td width="108" style="width:81.0pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Order:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-top:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">' . $order->id . '</a></span> </p>
                        </td>
                    </tr>
                    <tr style="height:22.5pt">
                        <td width="108" style="width:81.0pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 7.5pt;height:22.5pt">
                            <p class="MsoNormal"><b><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:#414042">Client:</span></b> </p>
                        </td>
                        <td width="118" style="width:88.5pt;border:none;border-bottom:solid #414042 1.0pt;background:#dcddde;padding:0in 0in 0in 0in;height:22.5pt">
                            <p class="MsoNormal"><span style="font-size:10.0pt;font-family:&quot;Arial&quot;,sans-serif;color:green">' . $order->ship_first_name . ' ' . $order->ship_last_name . '</span> </p>
                        </td>
                    </tr>
                </tbody>
            </table>';


                $email->body .= '<p class="MsoNormal"><span>&nbsp;</span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%">
                <tbody>
                     <p align="justify">' . $text . '</p>
                </tbody>
                </table>';

                $mail_log->text = $email->body;

                $mail_log->order_id = $order->id;
                $mail_log->save();
                // echo $email->body;
                // echo $email->subject;
//                $email->send();

            }

            if(isset($_POST['tracking_email']) && $_POST['tracking_email'] == 'on'){
                $email = new \Email\MailMaster();
                $mergeFields = [
                    'REF_NUM'=>$order->ref_num,
                    'TRACKING_NUMBER'=>$order->tracking_number
                ];
                $email->setTo(['email'=>$order->email,'name'=>$order->shipName(), 'type'=>'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-tracking-email')->send();
            }

           /* if (isset($_POST['shipping_method'])) {
                if ($order->shipping_method != $_POST['shipping_method']) {
                    $order->shipping_method = \Model\Shipping_Method::getItem($_POST['shipping_method']);
                    $costs = json_decode($order->shipping_method->cost);
                    $order->shipping_method->cost = 0;
                    $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
                    foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                        if (is_null($range) || trim($range) == '') {
                            $range = 0;
                        }
                        if ($subtotal >= $range && isset($costs[$key])) {
                            $order->shipping_cost = $costs[$key];
                        }
                    }
                    $order->shipping_method = $order->shipping_method->id;
                }
            }*/
//           if(isset($_POST['shipping_cost'])){
//               if($order->shipping_cost != $_POST['shipping_cost']){
//                   $order->shipping_cost = $_POST['shipping_cost'];
//               }
//           }

//            if (isset($_POST['ship_state'])) {
//                $shippingState = preg_replace('/\s+/', '', strtolower($_POST['ship_state']));
//                if ($shippingState == 'ny' ||$shippingState=='newyork') {
//                    $order->tax_rate = 8.875;
//                    $order->tax = $subtotal * ($order->tax_rate / 100);
//                } else {
//                    $order->tax_rate = null;
//                    $order->tax = null;
//                }
//            }

//            $order->total = $subtotal + $order->shipping_cost + $order->tax - $order->coupon_amount;

            if ($order->save()) {
                $n = new \Notification\MessageHandler('Order saved.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($order->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . $this->_content . '/update/' . $order->id);
            }

        }
        redirect(ADMIN_URL . $_POST['redirectTo']);
    }

    public function mass_email(Array $arr = []){
        if($_GET['orders']){
            $ss = new ShipStation();
            $ss->setSsApiKey(SHIPSTATION_KEY);
            $ss->setSsApiSecret(SHIPSTATION_SECRET);
            $shipStores = $ss->getStores();

            $orders = explode('%2C', $_GET['orders']);
            $ssOrders = [];
            foreach($orders as $orderId){
                $ssOrders[] = $ss->getOrder($orderId);
            }

            $stores = [];
            foreach($shipStores as $key=>$store){
                $stores[$store->storeId] = $store->storeName." ($store->marketplaceName)";
            }
            $this->_viewData->stores = $stores;

            $this->_viewData->orders = $ssOrders;
//            dd($ssOrders);
            parent::update($arr);

        } else {
            redirect('/admin/shipstation_orders');
        }
    }

    public function mass_email_post(){
        $ss = new ShipStation();
        $ss->setSsApiKey(SHIPSTATION_KEY);
        $ss->setSsApiSecret(SHIPSTATION_SECRET);
        $shipStores = $ss->getStores();

        $orders = explode('%2C', $_GET['orders']);
        $ssOrders = [];
        foreach($orders as $orderId){
            $ssOrders[] = $ss->getOrder($orderId);
        }

//        $orders = \Model\Order::getList(['where'=>'id in ('.urldecode($_GET['orders']).')']);
        foreach($ssOrders as $order){
            $body = sprintf(\Model\Order::$mass_email_template,$order->shipTo->name,\Model\Order::$mass_email[$_POST['email_type']]);
            $email = new \Email\MailMaster();
            $mergeTags = ['BODY'=>$body];
            $email->setTo(['email'=>$order->customerEmail,'name'=>$order->shipTo->name,'type'=>'to'])->setMergeTags($mergeTags)->setTemplate('modern-vice-mass-email');
            $email->send();
        }
        redirect(ADMIN_URL.$_POST['redirectTo']);
    }

    public function search()
    {
//        $ss = new ShipStation();
//        $ss->setSsApiKey(SHIPSTATION_KEY);
//        $ss->setSsApiSecret(SHIPSTATION_SECRET);
//        $orderParams = [];
//        $orderParams['sortDir'] = 'desc';
//
//        if(isset($_GET['keywords'])){
//            $orderParams['itemKeyword'] = urldecode($_GET['keywords']);
//        }
//        if(isset($_GET['store'])){
//            $orderParams['storeId'] = $filterParams['store'] = $_GET['store'];
//        }
//        $ssOrders = $ss->getOrders($orderParams);
//        echo json_encode($ssOrders);
        global $emagid;
        $db = $emagid->getDb();
        $sql = "
select o.id,o.viewed,o.insert_time,o.tracking_number,o.bill_first_name||' '||o.bill_last_name as bill_name,o.status,o.payment_status,o.total,o.payment_method,p.name as products,op.details
from public.order o
left join order_products op on o.id = op.order_id and o.active = 1
left join product p on op.product_id = p.id";
        $parsed = trim(preg_replace('/\s+/',' ',strtolower(urldecode($_GET['keywords']))));
        $expl = explode(' ',$parsed);
        $impl = "'%".implode("%','%",$expl)."%'";
        $where = [];
        if(is_numeric($_GET['keywords'])){
            $where[] = "o.id = {$_GET['keywords']}";
        }
        $where[] = "lower(email) like '%$parsed%'";
        $where[] = "lower(ship_first_name)||' '||lower(ship_last_name) like all(array[$impl])";
        $where[] = "lower(bill_first_name)||' '||lower(bill_last_name) like all(array[$impl])";
        $where[] = "lower(p.name) like all(array[$impl])";
        $limit = ' limit 20';
        $orderBy = ' order by id desc';

        $sql .= ' where o.active = 1 and ('.implode(' or ',$where).')';
        $sql .= $orderBy;
//        $sql .= $limit;
        $result = $db->execute($sql);

        $arr = [];
        $id = '';
        $productList = [];
        foreach($result as $r){
            if($id != $r['id']){
                $id = $r['id'];
                $productList = [];
            }
            if($r['products']){
                $productList[] = $r['products'];
            } else if($r['details']){
                $json = json_decode($r['details'], true);
                $productList[] = $json['misc_name'] . '(Custom)';
            }
//            $obj = \Model\Order::getItem($r['id']);
//            if ($obj && $obj->getInvoice()) {
//                $style = 'C6DDFF';
//            } elseif ($obj && $obj->in_store) {
//                $style = 'ffff9e';
//            } else {
//                $style = '';
//            }
            $r['products'] = $productList;
//            $r['style'] = $style;
            $arr[$r['id']] = $r;

        }
        rsort($arr);
        echo json_encode($arr);
    }


    public function search_by_mpn()
    {
        $mpn = trim($_GET['keywords']);
        $mpn = str_replace('%20', ' ', $mpn);
        $mpn = str_replace('+', ' ', $mpn);
        $mpn = str_replace('%2F', '/', $mpn);
        $mpn = urldecode($mpn);

        $product = \Model\Product::getList(['where' => "mpn = '$mpn'"]);
        $a = Array();

        echo '[';
        foreach ($product as $pr) {
            $order_product = \Model\Order_Product::getList(['where' => 'product_id = ' . $pr->id]);


            foreach ($order_product as $op) {
                $orders = \Model\Order::getList(['where' => 'id = ' . $op->order_id]);


                foreach ($orders as $order) {

                    $a[] = '{ "id": "' . $order->id . '", 
            "insert_time": "' . $order->insert_time . '", 
            "tracking_number": "' . $order->tracking_number . '",
            "status": "' . $order->status . '",
            "bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
            "products": "' . $pr->name . '",
            "total": "' . $order->total . '"}';


                }
            }
        }

        echo implode(',', $a);
        echo "]";

    }


    public function pay(Array $params = [])
    {
        if (isset($params['id']) && is_numeric($params['id'])) {

            $order = \Model\Order::getItem($params['id']);

            if (!is_null($order)) {

                $localTransaction = $this->emagid->db->execute('SELECT ref_trans_id FROM transaction WHERE order_id = ' . $order->id);

                if (isset($localTransaction[0]) && isset($localTransaction[0]['ref_trans_id'])) {
                    $refTransId = $localTransaction[0]['ref_trans_id'];

                    $transaction = new AuthorizeNetAIM;
                    $response = $transaction->priorAuthCapture($refTransId);

                    $n = new \Notification\MessageHandler($response->response_reason_text);
                    $_SESSION["notification"] = serialize($n);
                }

                redirect(ADMIN_URL . 'orders/update/' . $order->id);

            }

        }

        redirect(ADMIN_URL . 'orders');
    }
}
