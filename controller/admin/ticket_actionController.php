<?php

class ticket_actionController extends adminController{
    function __construct()
    {
        parent::__construct('Service_Action');
    }

    public function index(Array $params = [])
    {
        $this->_pageSize = 100;
        parent::index($params);
    }


    function update_post()
    {
        $id = $_POST['id'];
        $_POST['redirectTo'] = "/admin/ticket_action/update/$id";
        parent::update_post();
    }
}