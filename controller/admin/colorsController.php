<?php

class colorsController extends adminController{

    function __construct()
    {
        parent::__construct('Color');
    }
    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $params['queryOptions']['orderBy'] = 'name';
        parent::index($params); // TODO: Change the autogenerated stub
    }

    public function search(){
        $colors = \Model\Color::search($_GET['keywords']);

        $arr = [];
        foreach($colors as $key=>$color){
            $arr[] = ['id'=>$color->id,'name'=>$color->name,'slug'=>$color->slug];
        }

        echo json_encode($arr);
    }
}