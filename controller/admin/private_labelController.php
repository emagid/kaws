<?php

class private_labelController extends adminController{

    function __construct()
    {
        parent::__construct('Private_Label');
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $params['queryOptions']['orderBy'] = 'type asc, display_order asc';
        parent::index($params); // TODO: Change the autogenerated stub
    }

    public function update(Array $arr = [])
    {
        $pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
        $displays = [1=>1];
        if($pro){
            global $emagid;
            $db = $emagid->getDb();

            $type = $pro->type;
            $sql = "select display_order from private_label where type = {$type} and display_order is not null order by display_order desc limit 1";
            $res = $db->execute($sql);
            $order = $res ? $res[0]['display_order'] : 0;
            $newDisplays = [];
            for($i = 1; $i <= $order+1; $i++){
                $priv = \Model\Private_Label::getItem(null,['where'=>"type = $type and display_order = $i"]);
                if($priv){
                    $newDisplays[$i] = $i.'. '.$priv->title;
                } else {
                    $newDisplays[$i] = $i;
                }
            }
        }
        $this->_viewData->displays = isset($newDisplays) && $newDisplays? $newDisplays: $displays;
        parent::update($arr); // TODO: Change the autogenerated stub
    }

    function update_post()
    {
        //reindex display_order as necessary
        $private_label = \Model\Private_Label::getItem($_POST['id']);
        $private_labels = \Model\Private_Label::getList(['where'=>"type={$private_label->type} and id != {$_POST['id']}", 'orderBy'=>"display_order"]);
        for($i = 1; $i <= count($private_labels); $i++){
            if($i >= $_POST['display_order']){
                $private_labels[$i-1]->display_order = $i+1;
            } else {
                $private_labels[$i-1]->display_order = $i;
            }
            $private_labels[$i-1]->save();
        }

//        if($private_label->display_order != $_POST['display_order'] && \Model\Private_Label::getItem(null,['where'=>"type = {$private_label->type} and display_order = {$_POST['display_order']}"])){
//            $reindex = \Model\Private_Label::getList(['where'=>"type = $private_label->type and display_order >= {$_POST['display_order']} and id != {$_POST['id']}"]);
//            $indexer = $_POST['display_order'];
//            foreach($reindex as $re){
//
//                $indexer++;
//                $re->display_order = $indexer;
//                $re->save();
//            }
//        }
        $_POST['redirectTo'] = ADMIN_URL.'private_label';
        parent::update_post(); // TODO: Change the autogenerated stub
    }

    //update display order in admin update
    function uDO(){
        $type = $_GET['type'];
        global $emagid;
        $db = $emagid->getDb();

        $sql = "select display_order from private_label where type = {$type} and display_order is not null order by display_order desc limit 1";
        $res = $db->execute($sql);
        $order = $res ? $res[0]['display_order'] : 0;
        $newDisplays = [];
        for($i = 1; $i <= $order+1; $i++){
            $priv = \Model\Private_Label::getItem(null,['where'=>"type = $type and display_order = $i"]);
            if($priv){
                $newDisplays[$i] = $i.'. '.$priv->title;
            } else {
                $newDisplays[$i] = $i;
            }
        }
        $options = '';
        $selected = null;
        $private_label = null;
        if(isset($_GET['id']) && $_GET['id']){
            $private_label = \Model\Private_Label::getItem($_POST['id']);
        }
        foreach($newDisplays as $id=>$display){
            $selected = $private_label && $private_label->display_order == $id ? 'selected': null;
            $options .= "<option value='".$id."' $selected>".$display."</option>";
        }

        echo json_encode(['status'=>"success", 'appendHtml'=>$options]);
    }
}