<?php

use Emagid\Html\Form;
use EmagidService\S3;

class partsController extends adminController {
	
	function __construct(){
		parent::__construct("Product");
	}

	function index(Array $params = [])
    {
        $this->_viewData->start = '';
        $this->_viewData->end = '';
        $this->_viewData->parts = \Model\Product::getRepairProducts();
        if (!Empty($_GET['how_many'])) {
            $params['limit'] = $_GET['how_many'];
            $this->_viewData->limit = $_GET['how_many'];
        } else {
            $params['limit'] = 50;
            $this->_viewData->limit = 50;
        }

		if (!Empty($_GET['tab'])){
			$this->_viewData->tab = $_GET['tab'];
		}


        if (!Empty($_GET['how_many']) && !Empty($_GET['status_show'])) {
            $st = str_replace('%20', ' ', $_GET['status_show']);
            $params['queryOptions']['where'] = " brand = '$st'";
            $params['limit'] = $_GET['how_many'];
        }

        if (!Empty($_GET['status_show'])) {
            $st = str_replace('%20', ' ', $_GET['status_show']);
            $params['queryOptions']['where'][] = " brand = '$st'";

        }
        $params['queryOptions']['where'][] = " id IN ".\Model\Product::getRepairIds()." ";
        if (isset($_GET['ava'])) {
            $params['queryOptions']['where'][] = " availability = {$_GET['ava']} ";
        }
        $params['queryOptions']['where'][] = " type != '1' ";
        if (isset($_GET['type'])) {
            $params['queryOptions']['where'][] = " type = '{$_GET['type']}' ";
        }
        if(isset($_GET['t'])){
            $get = explode(',',urldecode($_GET['t']));
            $start = date('Y-m-d H:i:s',$get[0]); $end = date('Y-m-d H:i:s',$get[1]);
            $this->_viewData->start = $start;
            $this->_viewData->end = $end;
            if(isset($params['queryOptions']['where'])){
                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
            } else {
                $params['queryOptions']['where'][] = "insert_time between '$start' and '$end'";
            }
        }
        if (!Empty($_GET['search'])) {
            $keywords = $_GET['search'];
            $explodeTags = explode(' ', strtolower(urldecode($keywords)));
            $tags = "'%" . implode("%','%", $explodeTags) . "%'";
            $params['queryOptions']['where'] = " lower(slug) like '%" . strtolower(urldecode($keywords)) . "%' or lower(name) like '%" . strtolower(urldecode($keywords)) . "%' or
            lower(name) like all(array[" . $tags . "]) or lower(part_number) like '%" . strtolower(urldecode($keywords)) . "%' or lower(ean) like '%" . strtolower(urldecode($keywords)) . "%' or 
            lower(upc) like '%" . strtolower(urldecode($keywords)) . "%'";
            $params['queryOptions']['orderBy'] = 'type,name';
            $this->_viewData->search = urldecode($keywords);
        }
        if(isset($params['queryOptions']['where'])){
            $params['queryOptions']['where'] = implode(' and ',$params['queryOptions']['where']);
        }
        /*else{
            $params['queryOptions']['orderBy'] = "mpn ASC";
        } */
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->brands = \Model\Brand::getList();
//		 dd($params);
        parent::index($params);

    }

	public function import(Array $params = [])
	{
		set_time_limit(0);
		$handle = fopen($_FILES['csv']['tmp_name'], "r");
		$headSkip = true;
		$id1 = '';
		$id2 = '';
		$id3 = '';
		$allArr = [];
		if ($handle) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if (empty($data[0])) {
				} else {
					if($headSkip){
						$headSkip = false;
					} else {
						if(!array_key_exists($data[0],$allArr)){
							$id1 = strtolower($data[7]);$id2 = strtolower($data[9]);$id3 = strtolower($data[11]);
							$allArr[$data[0]] = ['slug'=>$data[0],'name'=>$data[1],'description'=>$data[2],'tags'=>$data[5],'price'=>$data[19],'msrp'=>$data[20],'meta_title'=>isset($data[31])?$data[31]:'', 'meta_description'=>isset($data[32])?$data[32]:'',
												'options'=>[$id1=>[$data[8]],$id2=>[$data[10]],$id3=>[$data[12]]], 'images'=>[$data[24]]];
						} else {
							if(!in_array($data[8],$allArr[$data[0]]['options'][$id1])){
								$allArr[$data[0]]['options'][$id1][] = $data[8];
							}
							if(!in_array($data[10],$allArr[$data[0]]['options'][$id2])){
								$allArr[$data[0]]['options'][$id2][] = $data[10];
							}
							if(!in_array($data[12],$allArr[$data[0]]['options'][$id3])){
								$allArr[$data[0]]['options'][$id3][] = $data[12];
							}
							if(isset($data[24]) && $data[24] != ''){
								$allArr[$data[0]]['images'][] = $data[24];
							}
						}
					}
				}
			}
			fclose($handle);
		}
		foreach($allArr as $arr){
			$product = new \Model\Product();
			$product->slug = $arr['slug'];
			$product->name = $arr['name'];
			$product->description = $arr['description'];
			$product->tags = $arr['tags'];
			$product->price = $arr['price'];
			$product->msrp = $arr['msrp'] != ''?$arr['msrp']:$arr['price'];
			$product->meta_title = $arr['meta_title'];
			$product->meta_description= $arr['meta_description'];
			$product->options = json_encode($arr['options']);
			if(array_key_exists('',$arr['options'])){
				unset($arr['options'][""]);
			}
			if(isset($arr['options']['size'])){
				$product->size = json_encode($arr['options']['size']);
			}
			$arrColor = [];
			if(isset($arr['options']['color'])){
				foreach($arr['options']['color'] as $color){
					if($c = \Model\Color::getItem(null,['where'=>"name = '{$color}'"])){
						$arrColor[] = $c->id;
					} else {
						$strReplace = str_replace(',','-',trim($color));
						$strReplace = str_replace('/','-',trim($strReplace));
						$strReplace = str_replace('&','-',trim($strReplace));
						$strReplace = str_replace(' ','-',trim($strReplace));
						$c = new \Model\Color();
						$c->name = $color;
						$c->slug = $strReplace;
						$c->save();
						$arrColor[] = $c->id;
					}
				}
				$product->color = json_encode($arrColor);
			}
			$product->save();
			foreach($arr['images'] as $image){
				$prod_image = new \Model\Product_Image();
				$uniqueId = uniqid($product->slug);
				file_put_contents(__DIR__ . '/../../content/uploads/products/' . $uniqueId . '.jpg', file_get_contents($image));
				$prod_image->product_id = $product->id;
				$prod_image->image = $uniqueId . '.jpg';
				$prod_image->save();
			}
		}


		$n = new \Notification\MessageHandler("Import success!");
		$_SESSION["notification"] = serialize($n);
		redirect(SITE_URL . 'admin/products');


	}
	function export(Array $params = []){
		$this->template = false;
		$this->_viewData->hasCreateBtn = true;
		$this->_viewData->product = \Model\Product::getList();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=product'.\Carbon\Carbon::now()->toDateTimeString().'.csv');
		$output = fopen('php://output', 'w');
		$t=array("Name",'Price','Description','Type','Part No','EAN','UPC','Remark');
		fputcsv($output, $t);
		$row ='';
		foreach($this->_viewData->product as $b) {
		$row = array($b->name,$b->getPrice(),$b->description,\Model\Product::$type[$b->type],$b->part_number,$b->ean,$b->upc,$b->remark);
		fputcsv($output, $row);  
		}   
		parent::index($params);
	}

	public function update(Array $arr = []) {
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
        if (!Empty($_GET['tab'])){
            $this->_viewData->tab = $_GET['tab'];
        }

		$carbon = \Carbon\Carbon::now();

		$this->_viewData->product_categories = \Model\Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_accessories = \Model\Product_Accessory::get_product_accessories($pro->id);
//		$this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
//		foreach($this->_viewData->product_questions as $question){
//			$question->insert_time = new DateTime($question->insert_time);
//			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
//		}
		$this->_viewData->product_images = [];
		if(isset($arr['id'])){
//			$this->_viewData->products = \Model\Product::getList(['where'=>"id not in ({$arr['id']})",'orderBy'=>'name']);
            if($pro->type == 1){
                $cat = $this->_viewData->product_categories[0];
                $except = ' and product_id <> '. $pro->id;
                $products = \Model\Product_Category::get_category_products($cat,"",$except,3);
                $this->_viewData->spares = $products;
            }
		}
//		$this->_viewData->brands = \Model\Brand::getList(['orderBy'=>'name']);
		$this->_viewData->categories = \Model\Category::getList(['orderBy'=>'name']);
		$this->_viewData->colors = \Model\Color::getList(['orderBy'=>'name']);

//		$this->_viewData->materials = \Model\Material::getList(['orderBy'=>'name']);
//		$this->_viewData->soles = \Model\Sole::getList(['orderBy'=>'name']);
//		$this->_viewData->events = \Model\Event::getList(['where'=>"end_date > '{$carbon->format('Y-m-d H:i:s')}'"]);

		parent::update($arr);
	}


	public function update_post()
	{
//        $_POST['specs'] = [];
//        for($i = 0;$i < count($_POST['spec_name']); $i++){
//            $_POST['specs'][] = ['name'=>$_POST['spec_name'][$i],'details'=>$_POST['spec_details'][$i]];
//        }
//        $_POST['specs'] = json_encode($_POST['specs']);

        $_POST['feature_1'] = [];
        for($i = 0;$i < count($_POST['feature_title_1']); $i++){
            $_POST['feature_1'][] = ['feature_title'=>$_POST['feature_title_1'][$i],'feature_description'=>$_POST['feature_description_1'][$i]];
        }
        $_POST['feature_1'] = json_encode($_POST['feature_1']);

        $_POST['feature_2'] = [];
        for($i = 0;$i < count($_POST['feature_title_2']); $i++){
            $_POST['feature_2'][] = ['feature_title'=>$_POST['feature_title_2'][$i],'feature_description'=>$_POST['feature_description_2'][$i]];
        }
        $_POST['feature_2'] = json_encode($_POST['feature_2']);

        $_POST['feature_3'] = [];
        for($i = 0;$i < count($_POST['feature_title_3']); $i++){
            $_POST['feature_3'][] = ['feature_title'=>$_POST['feature_title_3'][$i],'feature_description'=>$_POST['feature_description_3'][$i]];
        }
        $_POST['feature_3'] = json_encode($_POST['feature_3']);

        $_POST['feature_4'] = [];
        for($i = 0;$i < count($_POST['feature_title_4']); $i++){
            $_POST['feature_4'][] = ['feature_title'=>$_POST['feature_title_4'][$i],'feature_description'=>$_POST['feature_description_4'][$i]];
        }
        $_POST['feature_4'] = json_encode($_POST['feature_4']);
//		if(isset($_POST['product_category']) && count($_POST['product_category']) > 0){
//			$arr = $_POST['product_category'];
//			foreach($_POST['product_category'] as $pCatId){
//				$pCat = \Model\Category::getItem($pCatId);
//				$parentId = $pCat->parent_category;
//				if($parentId != 0) {
//					do {
//						$parent = \Model\Category::getItem($parentId);
//						if (!in_array($parent->id, $arr)) {
//							$arr[] = (string)$parent->id;
//						}
//						$parentId = $parent->parent_category;
//					} while ($parent->parent_category > 0);
//				}
//			}
//			$_POST['product_category'] = $arr;
//		}

		//handle linked list logic
//		if(isset($_POST['linked_list']) && $_POST['linked_list']) {
//			$linkedList = $_POST['linked_list'];
//			\Model\Product::changeLink($linkedList,$_POST['id']);
//		} else {
//			\Model\Product::changeLink([],$_POST['id']);
//		}
		//json encode list
//		$jsonList = ['color','size','sole','related_products','linked_list'];
//		foreach($jsonList as $post) {
//			if (isset($_POST[$post]) && count($_POST[$post]) > 0) {
//				$_POST[$post] = json_encode($_POST[$post]);
//			}
//		}
		//set value to 0 list
//		$arr = ['length','width','height', 'quantity', 'heel_height'];
//		foreach($arr as $ar){
//			if(isset($_POST[$ar]) && $_POST[$ar] == null){
//				$_POST[$ar] = 0;
//			}
//		}
		parent::update_post();
	}

	public function sort_images()
	{
		$display_order = 1;
		foreach($_POST['ids'] as $id){
			$prod_img = \Model\Product_Image::getItem($id);
			$prod_img->display_order = $display_order;
			$prod_img->save();
			$display_order++;
		}
	}

	public function sort_colors()
	{
		$display_order = 1;
		foreach(json_decode($_POST['data'],true)[0] as $item){
			$prodAttr = \Model\Product_Attributes::searchBy($item['product_id'],$item['color_id'],'display_order') ? : new \Model\Product_Attributes();
			$prodAttr->product_id = $item['product_id'];
			$prodAttr->color_id = $item['color_id'];
			$prodAttr->name = 'display_order';
			$prodAttr->value = $display_order;
			$prodAttr->save();
			$display_order++;
		}
	}

	public function save_variant(){
		$material = [$_POST['material']];
		$color = $_POST['color'];
		$sole = $_POST['sole'];
		$price = $_POST['price'] ? : 0.0;
		$quantity = $_POST['quantity'] ? : 0.0;
		$madeToOrder = $_POST['mto'] ? : 'off';
		$productId = $_POST['productId'];

		$echo = ['redirect'=>$_POST['redirect']];
		if($material && $color && $sole){
			$arr = \Model\Variation::$lowerVariations;
			$arrList = [];
			foreach($arr as $array){
				if(isset($$array)){
					$arrList[] = $$array;
				}
			}
			$list = \Model\Variation::combinations($arrList);
			foreach($list as $combo){
				$variant = [];
				for($i = 0; $i < count($combo); $i++){
					$variant[$arr[$i]] = $combo[$i];
				}
				$variation = \Model\Variation::addVariation($variant);
				\Model\Product_Attributes::addAttribute($productId,$variation->id,['price'=>$price, 'quantity'=>$quantity, 'madeToOrder'=>$madeToOrder]);
			}
		} else {
			$n = new \Notification\ErrorHandler("Requirements unmet");
			$_SESSION['notification'] = serialize($n);
		}
		echo json_encode($echo);
	}
    public function orderSpares(){
        $increment = 1;
        foreach($_POST['ids'] as $id){
            $prod = \Model\Product_Accessory::getItem($id);
            $prod->display_order = $increment;
            $prod->save();
            $increment++;
        }
        echo 'fail';
    }
	public function delete_variant()
	{
		$id = isset($_POST['id']) && $_POST['id'] != '' ? $_POST['id'] : null;
		$prod_id = isset($_POST['prod_id']) && $_POST['prod_id'] != '' ? $_POST['prod_id'] : null;
		if ($id && $prod_id) {
			$variation = \Model\Variation::getItem($id);
			$pAttr = \Model\Product_Attributes::getList(['where' => "variation_id = $variation->id and product_id = $prod_id"]);
			foreach ($pAttr as $pa) {
				\Model\Product_Attributes::delete($pa->id);
			}
			echo json_encode(['status' => "success"]);
		} else {
			echo json_encode(['status' => "failed", 'message' => "Failed to delete"]);
		}
	}

	public function save_attribute(){
		$product_id = $_POST['product_id'] ? : null;
		$color_id = $_POST['color_id'] ? : null;
		$price = $_POST['price'];
		$mto = $_POST['mto'];
		$event = $_POST['event'];
		$status = $_POST['status'];
		$arr = ['price'=>$price, 'mto'=>$mto, 'event'=>$event, 'status'=>$status];
		if($product_id && $color_id){
			foreach($arr as $key=>$ar){
				$prod_attr = \Model\Product_Attributes::getItem(null,['where'=>"product_id = $product_id and color_id = $color_id and name='{$key}'"]) ? : new \Model\Product_Attributes();
				$prod_attr->product_id = $product_id;
				$prod_attr->color_id = $color_id;
				$prod_attr->name = $key;
				$prod_attr->value = $ar;
				$prod_attr->save();
			}
			echo json_encode(['status'=>'success', 'message'=>'Saved']);
		} else {
			echo json_encode(['status'=>'failed', 'message'=>'Variant failed to save']);
		}
	}

	public function multi_update(Array $arr = []) {
		$id_array = $_GET;
		
		$q = implode("," , $id_array);

		if (count($_GET) <= 1){
			$n = new \Notification\ErrorHandler('You must select at least one product to Multi Edit.');
           	$_SESSION["notification"] = serialize($n);
			redirect(ADMIN_URL.'products');
		}

		$this->_viewData->q=$q;
		$pro = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->product_categories = \Model\Product_Category::get_product_categories($pro->id);
		$this->_viewData->product_collections = \Model\Product_Collection::get_product_collections($pro->id);
		$this->_viewData->product_materials = \Model\Product_Material::get_product_materials($pro->id);
		$this->_viewData->product_notes = \Model\Note::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_notes as $note){
			$note->admin = \Model\Admin::getItem($note->admin_id);
			$note->insert_time = new DateTime($note->insert_time);
			$note->insert_time = $note->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_questions = \Model\Question::getList(['where'=>' product_id = '.$pro->id, 'orderBy'=>' insert_time DESC ']);
		foreach($this->_viewData->product_questions as $question){
			$question->insert_time = new DateTime($question->insert_time);
			$question->insert_time = $question->insert_time->format('m-d-Y H:i:s');
		}
		$this->_viewData->product_images = [];
		$this->_viewData->brands = \Model\Brand::getList();
		$this->_viewData->categories = \Model\Category::getList();
		$this->_viewData->colors = \Model\Color::getList();
		$this->_viewData->collections = \Model\Collection::getList();
		$this->_viewData->materials = \Model\Material::getList();

		parent::update($arr);
	}
	
	public function multi_update_post(){

		$q = explode(",", $_POST['id']);
		$a = $_POST;
		
		foreach($q as $productId){
			if (is_numeric($productId)){
				$product = Model\Product::getItem($productId);
				
				if ((isset($a['product_category'])) && (count($a['product_category'])>0)  )  {
					for ($x = 0; $x < count($a['product_category']); $x++){
						$category = \Model\Product_Category::getItem(null, ['where' => "product_id = '" . $productId . "' and category_id = ".$a['product_category'][$x]]);
						if (!isset($category))	{
							$category = new Model\Product_Category();
							$category->product_id = $productId;
							$category->category_id = $a['product_category'][$x];
							$category->save();
						}
					}
				}

				if ((isset($a['product_collection'])) && (count($a['product_collection'])>0)  )  {
					for ($x = 0; $x < count($a['product_collection']); $x++){
						$collection = \Model\Product_Collection::getItem(null, ['where' => "product_id = '" . $productId . "' and collection_id = ".$a['product_collection'][$x]]);
						if (!isset($collection))	{
							$collection = new Model\Product_Collection();
							$collection->product_id = $productId;
							$collection->collection_id = $a['product_collection'][$x];
							$collection->save();
						}
					}
				} 

				if ((isset($a['product_material'])) && (count($a['product_material'])>0)  )  {
					for ($x = 0; $x < count($a['product_material']); $x++){
						$material = \Model\Product_Material::getItem(null, ['where' => "product_id = '" . $productId . "' and material_id = ".$a['product_material'][$x]]);
						if (!isset($material))	{
							$material = new Model\Product_Material();
							$material->product_id = $productId;
							$material->material_id = $a['product_material'][$x];
							$material->save();
						}
					}
				}

				foreach($a as $key1 => $value1)	{
					if (!empty($value1) && $key1 != "id"){
						$product->$key1 = $value1;
						$product->save();
					}
				}
			}
		}
		redirect(ADMIN_URL.'products');
	}

	 

	protected function afterObjSave($obj){
		if (isset($_POST['note'])){
			$note = new \Model\Note();
			$note->name = $_POST['note']['name'];
			$note->description = $_POST['note']['description'];
			$note->admin_id = \Emagid\Core\Membership::userId();
			$note->product_id = $obj->id;
			$note->save();
		}
//		$existing = \Model\Product_Spec::getList(['where'=>"product_id = $obj->id"]);
		$existing = \Model\Product_Spec::getList(['sql'=>"select * from product_spec where product_id = $obj->id and active = 1"]);
        foreach ($existing as $spec_id){
            if(!in_array($spec_id->spec_id,$_POST['spec_category'])){
                $item = \Model\Product_Spec::getItem(null,['where'=>"product_id = $obj->id and spec_id = $spec_id->spec_id"]);
                \Model\Product_Spec::delete($item->id);
            }
        }
//        dd('test',$_POST['spec_category']);
		if(isset($_POST['spec_category'])){
            for($i = 0;$i < count($_POST['spec_category']); $i++){
                \Model\Product_Spec::addAttribute($obj->id,$_POST['spec_category'][$i],['value'=>$_POST['spec_value'][$i], 'display_order'=>$_POST['spec_display_order'][$i],'status'=>$_POST['spec_status'][$i]]);
            }

        }
        if(isset($_POST['posttag'])&&$_POST['posttag']){
            $_POST['redirectTo'] = ADMIN_URL.'products/update/'.$obj->id.'?tab='.str_replace('#','',$_POST['posttag']);
        }
	}

	public function save_variant_image(){
		$img_id = $_POST['product_image_id'];
		$color_id = $_POST['color_id'];
		$prod_img = \Model\Product_Image::getItem($img_id);
		if($prod_img){
			$prod_img->color_id = json_encode($color_id);
			if($prod_img->save()){
				echo json_encode(['status'=>"success",'message'=>"Success"]);
			} else {
				echo json_encode(['status'=>"failed",'message'=>"Failed to save"]);
			}
		} else {
			echo json_encode(['status'=>"failed",'message'=>"Product Image does not exist... somehow"]);
		}
	}

	public function save_legshot_image(){
		$id = $_POST['product_image_id'];
		$colId = $_POST['color_id'];
		$prodId = $_POST['product_id'];

		$prodImg = \Model\Product_Image::getList(['where'=>"product_id = $prodId and color_id like '%$colId%'"]);
		if($prodImg) {
			foreach ($prodImg as $pi) {
				if ($pi->id != $id || ($pi->id == $id && $pi->legshot == 1)) {
					$pi->legshot = 0;
				} else {
					$pi->legshot = 1;
				}
				$pi->save();
			}
			echo json_encode(['status' => "success", 'message' => "Success"]);
		} else {
			echo json_encode(['status'=>"failed",'message'=>"Failed"]);
		}
	}

	function upload_images($params) {
		header("Content-type:application/json");
		$data = [];
		$data['success'] = false;
		$data['redirect'] = false;
		$id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
		if((int)$id>0) {
			$product = \Model\Product::getItem($id);
			if($product==null) {$data['redirect']=true;}
			$this->save_upload_images($_FILES['file'], $product->id, UPLOAD_PATH.'products'.DS,[]);
			$data['success'] = true;
		}
	
		//print_r($_FILES);
		echo json_encode($data);exit();
	}
	
	public function save_upload_images($files,$obj_id,$upload_path,$sizes=[]) {
		$counter = 1;
		// get highest display order counter if any images exist for specified listing
		$image_high = \Model\Product_Image::getItem(null,['where'=>'product_id='.$obj_id,'orderBy'=>'display_order','sort'=>'DESC']);
		if($image_high!=null) {
			$counter = $image_high->display_order+1;
		}
		foreach($files['name'] as $key=>$val) {
			$product_image = new \Model\Product_Image();
			$temp_file_name = $files['tmp_name'][$key];
			$file_name = uniqid() . "_" . basename($files['name'][$key]);
			$file_name = str_replace(' ', '_', $file_name);
			$file_name = str_replace('-', '_', $file_name);
	
			$allow_format = array('jpg', 'png', 'gif','JPG');
			$filetype = explode("/", $files['type'][$key]);
			$filetype = strtolower(array_pop($filetype));
			//$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
			//$filetype = array_pop($filetype);
			if($filetype == 'jpeg' || $filetype=="JPG"){$filetype = 'jpg';}
	
			if(in_array($filetype, $allow_format)){
	
				$img_path = compress_image($temp_file_name, $upload_path . $file_name, 60,$files['size'][$key]);
				if($img_path===false) {
					move_uploaded_file($temp_file_name, $upload_path . $file_name);
				}
				 //move_uploaded_file($temp_file_name, $upload_path . $file_name);
				 
				if(count($sizes)>0) {
					foreach($sizes as $key=>$val) {
						if(is_array($val) && count($val)==2) {
							resize_image_by_minlen($upload_path,
							$file_name, min($val), $filetype);
	
							$path = images_thumb($file_name,min($val),
									$val[0],$val[1], file_format($file_name));
							$image_size_str = implode('_',$val);
							copy($path, $upload_path.$image_size_str.$file_name);
						}
					}
				}
//				$variant_image = new \Model\Product_Attributes();
//				$variant_image->product_id = $obj_id;
//				$variant_image->name = 'image';
//				$variant_image->value = $file_name;
//				$variant_image->save();
	
				$product_image->image = $file_name;
				$product_image->product_id = $obj_id;
				$product_image->display_order = $counter;
				$product_image->save();
			}
			$counter++;
		}
	}

	public function search(){
		global $emagid; $db = $emagid->getDb();
		$keyword = strtolower(urldecode($_GET['keywords']));

		$where = [];
		if(is_numeric($keyword)){
			$where[] = "id = $keyword";
			$where[] = "price = $keyword";
		}
		$where[] = "lower(name) like '%$keyword%'";
		$where[] = "lower(part_number) like '%$keyword%'";
		$where[] = "lower(ean) like '%$keyword%'";
		$where[] = "lower(upc) like '%$keyword%'";


		$searchWhere = implode(' or ',$where);
		$searchWhere = "( $searchWhere ) AND id IN ".\Model\Product::getRepairIds();
		$searchQuery = "select id,featured_image,name,msrp,price,availability,type from product where active = 1 and ({$searchWhere}) order by type,name";
		$searchResult = $db->execute($searchQuery);

		$searchResult = array_map(function(&$item){
            if(is_numeric($item['availability'])){
                $item['availability'] = \Model\Product::$availability[$item['availability']];
            }
            if(is_numeric($item['type'])){
                $item['type'] = \Model\Product::$type[$item['type']];
            }
            if($item['featured_image']){
                $s3 = new EmagidService\S3();
                $item['featured_image'] =  $s3->getUrlByKey($item['featured_image']);
            }
            return $item;},$searchResult);

		echo json_encode($searchResult);
	}

	public function check_slug(){
		$slug = $_POST['slug'];
		$id = $_POST['id'];
		if($id>0){
			$list = \Model\Product::getItem(null,['where'=>"slug = '".$slug."' and id = '".$id."'"]);
				 if (isset($list) && count($list) == 1){
				 	echo "0";
				 }else{
					 	$list = \Model\Product::getList(['where'=>"slug = '".$slug."' "]);
					 if (isset($list) && count($list) > 0){
					 	echo "1";
					 }else{
					 	echo "0";
					 }
				 }
		}else{
			$list = \Model\Product::getList(['where'=>"slug = '".$slug."' "]);
				 if (isset($list) && count($list) > 0){
				 	echo "1";
				 }else{
				 	echo "0";
				 }
		}
		
	}


	public function sync_kadro()
	{
		$product = \Model\Product::getItem($_POST['id']);
		$feedJson = [];
		$feedJson['Feed'] = [];

		$productData = [];
		$productData['SKU'] = $product->sku?:$product->upc;
		$productData['ProductId'] = $product->id;
		$productData['UPC'] = $product->upc;
		$productData['EAN'] = $product->ean;
		$productData['Brand'] = "DJI";
		$productData['Condition'] = "New";
		$productData['ShortDescription'] = $product->details;
		$productData['LongDescription'] = $product->description;
		$productData['Status'] = "active";
		$productData['Price'] = $product->getPrice();
		$productData['Height'] = $product->height;
		$productData['Length'] = $product->length;
		$productData['Width'] = $product->width;
		$s3 = new S3();
		$productData['Media'][] = [
			'URL' => $product->featuredImage(),
			'Title' => null,
			'Position' => 0,
			'LastModified' => $product->insert_time,
			'Type' => 'Primary',
			'MediaType' => 'Image',
		];
		foreach($product->getAllProductImages() as $image){
			$productData['Media'][] = [
				'URL' => $s3->getUrlByKey($image->image),
				'Title' => null,
				'Position' => $image->display_order,
				'LastModified' => $image->insert_time,
				'Type' => 'Primary',
				'MediaType' => 'Image',
			];
		}

		$productData['InventoryDetail']['Allocation']['TotalQuantity'] = $product->quantity;

		$singleFeed = [];
		$singleFeed['Type'] = 'Product';
		$singleFeed['Element'] = [];
		$singleFeed['Element']['Id'] = $product->id;
		$singleFeed['Element']['Type'] = 'Update';
		$singleFeed['Element']['Product'] = $productData;

		$feedJson['Feed'][] = $singleFeed;

		$client = new GuzzleHttp\Client();
		$response = $client->post('http://marketplaces.kadro.com/oauth/token',
			[
				'body' => [
					'client_id' => '39abf87dcf46959e55de449ba0561d9b',
					'client_secret' => '1dd1c8d1ffe7cda918f62ba57b9da8',
					'grant_type' => 'client_credentials'
				],
				'headers' => [
					'Accept' => 'application/json'
				]
			]);

		if($response->getStatusCode() == 200){
			$tokenData = json_decode((string) $response->getBody(), true);
			$token = $tokenData['access_token'];
		} else {
			return $this->toJson(['status' => false, 'message' => 'Authorization failed']);
		}

		$updateResponse = $client->post('http://marketplaces.kadro.com/webservice/updateProducts?framework_version=1&app_version=1&filename=feed.json',
			[
				'body' => [
					'framework_version' => 1,
					'app_version' => 1,
					'filename' => 'feed.json',
					'feed' => json_encode($feedJson),
				],
				'headers' => [
					'Authorization' => "Bearer ${token}",
					'Content-Type' => 'application/x-www-form-urlencoded',
					'Accept' => 'application/json',
				]
			]);
		if($updateResponse->getStatusCode() == 200){
			$this->toJson(['status' => true]);
		} else {
			$this->toJson(['status' => false, 'message' => 'Update failed, it could be some required fields are missing.']);
		}
	}


	public function google_product_feed(Array $params = []){
        if(isset($_SESSION["google_feed_generated"]) && $_SESSION["google_feed_generated"] == 1){
            $this->_viewData->generated = true;
            unset($_SESSION["google_feed_generated"]);
        }
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=google_feed.csv');
        $output = fopen('php://output', 'w');
        $t = array("id","title","description","google product category","condition","price","availability","link","image link","gtin", "mpn", "brand");
        fputcsv($output, $t);
        foreach(\Model\Product::getList(['where'=>"type = '1'"]) as $p) {
            if($p->availability === null){
                \Model\Product::delete($p->id);
                break;
            }
            $row = array($p->id,"DJI ".$p->name,"DJI ".$p->name,2546,"New",$p->price." USD",\Model\Product::$feed_availability[$p->availability],"https://djinyc.com/products/$p->slug",$p->featuredImage(),
                $p->upc,$p->part_number,"DJI");
            fputcsv($output, $row);
        }
    }

    public function google_product_feed_post(){
		set_time_limit(0);
    	$genders = [ 'women' => 'Female', 'collaborations' => 'Female', 'men' => 'Male'];
//    	$type = "Apparel & Accessories > Jewelry > Watches";
    	$condition = "new";
        $availability = "in stock"; // "out of stock"
        $gpc = 187;
//        $identifier_exists = "TRUE";
        $age_group = "Adult";
        $brand = "Modern Vice";
        $size_system = "US";

        $f = fopen(UPLOAD_PATH."/google_product_feed.txt","w"); fclose($f); //clear the file
        $f = fopen(UPLOAD_PATH."/google_product_feed.txt","a"); //open with append write mode

//        $header = "id\ttitle\tdescription\tgoogle product category\tlink\timage link\tcondition\tavailability\tprice\tbrand\tmpn\tcolor\tage group\tgender";
        $header = "item_group_id\ttitle\tdescription\tgoogle product category\tlink\timage link\tcondition\tavailability\tprice\tbrand\tidentifier exists\tcolor\tage group\tgender\tsize\tsize_system\tid\tproduct type";
        fwrite($f, $header);

		$sql2 = 'select product.id, product.name, product.description, product.slug, product.featured_image, product.price, product.color, product.size from product where product.active = 1 order by id';
        $products = $this->emagid->db->getResults($sql2);

        $line_items = "";
        foreach($products as $product){
			$prod = \Model\Product::getItem($product['id']);

			$colorList = $product['color'] ? implode(',',json_decode($product['color'],true)) : 0;
			$colorSql = "select color.id, color.name from color where active = 1 and id in ({$colorList})";
			$colors = $this->emagid->db->getResults($colorSql);

			$sizeList = $product['size'] ? implode(',',json_decode($product['size'],true)): 0;
			$sizeSql = "select size.id, size.us_size from public.size where active = 1 and id in ({$sizeList})";
			$sizes = $this->emagid->db->getResults($sizeSql);

//			$colors = $prod->getColors();
//			$sizes = $prod->getSizes();
			foreach($colors as $col) {
				foreach ($sizes as $siz) {
					$item_group_id = $product['id'];
					$id = $product['id'].''.str_pad($col['id'],3,'0',STR_PAD_LEFT).''.str_pad($siz['id'],3,'0',STR_PAD_LEFT);
					$title = "Modern Vice {$product['name']} {$col['name']}";

					if ($product['description'] != '') {
						$description = str_replace('\\', '\\\\', $product['description']);
						$description = str_replace('<p>', ' ', $description);
						$description = str_replace('</p>', ' ', $description);
						$description = str_replace('<br>', ' ', $description);
						$description = str_replace('<br />', ' ', $description);
						$description = str_replace('<br/>', ' ', $description);
						$description = str_replace("\n", " ", $description);
						$description = str_replace('\n', ' ', $description);
						$description = str_replace("\r", " ", $description);
						$description = str_replace('\r', ' ', $description);
						$description = str_replace("\t", " ", $description);
						$description = str_replace('\t', ' ', $description);
						$description = str_replace(PHP_EOL, ' ', $description);
					} else {
						$description = $product['name'];
					}
					$link = "https://www.modernvice.com/products/" . $product['slug']; //TODO st-dev check if valid url

					$image_link = ($pi=\Model\Product_Image::getItem(null,['where'=>"product_id = $prod->id and color_id like '%\"{$col['id']}\"%' and (legshot is null or legshot = 0)", 'orderBy'=>"display_order"])) ? $pi->image: $product['featured_image'];
					$image_link = "https://www.modernvice.com" . UPLOAD_URL . 'products/' . $image_link; //TODO st-dev check if valid url

					$pCatSql = "select category_id from product_categories where active = 1 and product_id = {$prod->id}";
					$pCategories = $this->emagid->db->getResults($pCatSql);
					$pList = implode(',',flatten($pCategories));

					$categoriesSql = "select name, parent_category from category where active = 1 and id in ({$pList})";
					$categories = $this->emagid->db->getResults($categoriesSql);
//					$pCategories = \Model\Product_Category::getList(['where'=>"product_id = $prod->id"]);
//					$categories = array_map(function($item){return \Model\Category::getItem($item->category_id);},$pCategories);
					$arr = [];
					foreach($categories as $category){
						if($category['parent_category']){
							$parentSql = "select name from category where active = 1 and id = {$category['parent_category']}";
							$parent = $this->emagid->db->getResults($parentSql);
//							$parent = \Model\Category::getItem($category['parent_category']);
							$arr[] = trim($parent[0]['name']).' > '.trim($category['name']);
						}
					}
					//product type single column
					$product_type = implode(',',$arr);

					$price = $prod->price($col['id']) . ' USD';
					$brand = 'Modern Vice';
					$identifier_exists = 'FALSE';
					$color = $col['name'];
					$gender = $prod->getMainCategory() ? $genders[$prod->getMainCategory()->slug] : 'Female';
					$size = $siz['us_size'];


//					$line_item = "\n".$id."\t".$title."\t".$description."\t".$gpc."\t".$link."\t".$image_link."\t".$condition."\t".$availability."\t".$price."\t".$brand."\t".$mpn."\t".$identifier_exists."\t".$color."\t".$age_group."\t".$gender."\t".$type;
					$line_item = "\n" . $item_group_id . "\t" . $title . "\t" . $description . "\t" . $gpc . "\t" . $link . "\t" . $image_link . "\t" . $condition . "\t" . $availability . "\t" .
						$price . "\t" . $brand . "\t" . $identifier_exists . "\t" . $color . "\t" . $age_group . "\t" . $gender . "\t" . $size . "\t" . $size_system . "\t" . $id ."\t".$product_type;
					fwrite($f, $line_item);
				}
			}
		}
        fclose($f); 
        $_SESSION["google_feed_generated"] = 1;
        redirect(ADMIN_URL.'products/google_product_feed');
    }
     
}