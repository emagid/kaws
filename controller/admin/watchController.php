<?php

class watchController extends adminController{

	function __construct(){
        parent::__construct('Watch');
    }
	function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }
}