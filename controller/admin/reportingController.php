<?php

use Emagid\Core\Membership,
Emagid\Html\Form,
Model\Admin,
Model\Banner; 

class reportingController extends adminController {
	
	function __construct(){
		parent::__construct('Web Reports');
	}

	public function index(Array $params = []){
	    $totals = \Model\Order::getIntervalCosts();
		//var_dump($totals);
	    $this->_viewData->total_sales = $totals['total'];
		$this->_viewData->total_taxes = $totals['tax'];
		$this->_viewData->total_fees = $totals['shipping'];
		$this->_viewData->avg = $totals['avg'];
		$this->_viewData->orders_count = $totals['count'];
		$this->_viewData->total_tax_percent = $totals['tax_percentage'];
		$this->_viewData->total_ship_percent = $totals['ship_percentage'];
		$this->_viewData->total_valid_orders = $totals['count'];

		$params = [];
		if(isset($_GET['start'])){ $params['firstDate'] =   $this->_viewData->start = $_GET['start']; }
		if(isset($_GET['end'])){ $params['lastDate'] =      $this->_viewData->end = $_GET['end']; }
		if(isset($_GET['group'])){ $params['group'] =       $this->_viewData->group = $_GET['group']; } else {
            $this->_viewData->group = 'day';
        }
		$this->_viewData->results = \Model\Order::getIntervalCosts($params);

	    $logged_admin = $this->_viewData->logged_admin ? : null;
		$firstElement = reset($this->_viewData->admin_sections);
		$firstKey = key($this->_viewData->admin_sections);
		if($logged_admin && $logged_admin->username != 'emagid'){
			if(!empty($firstElement)){
				redirect(ADMIN_URL.strtolower($firstElement[0]));
			} else {
				redirect(ADMIN_URL.strtolower($firstKey));
			}
		}
		$this->_viewData->new_orders = \Model\Order::getCount(['where'=>"viewed is null or viewed = false"]);

		$this->_viewData->recent_orders = \Model\Order::getList(['orderBy'=>'insert_time DESC', 'limit'=>5]);

//		$this->_viewData->new_questions = \Model\Question::getList(['where'=>"status = '".\Model\Question::$status[0]."'"]);
        $this->_viewData->new_contact = \Model\Contact::getCount(['where'=>"viewed is null or viewed = false"]);
		$allStatus = implode("','",\Model\Order::$status);
//		$this->_viewData->monthly_sales = \Model\Order::getList(['where'=>"status in ('".$allStatus."') and EXTRACT(MONTH FROM insert_time)::INTEGER = ".date('m')." and EXTRACT(YEAR FROM insert_time)::INTEGER = ".date('Y')]);
		$this->_viewData->monthly_sales = \Model\Order::getList(['where'=>"status NOT IN ('Declined', 'Canceled', 'Incomplete PayPal') and EXTRACT(MONTH FROM insert_time)::INTEGER = ".date('m')." and EXTRACT(YEAR FROM insert_time)::INTEGER = ".date('Y')]);
		$this->_viewData->monthly_sum = 0;
		foreach($this->_viewData->monthly_sales as $sale){
			$this->_viewData->monthly_sum += $sale->total;
		}

		$previous = \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->format('Y-m-d');
		$beforePrevious = \Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeek()->format('Y-m-d');

		$this->_viewData->weekly_sales = \Model\Order::getList(['where'=>"insert_time between '$beforePrevious' and '$previous'"]);
		$this->_viewData->weekly_sum = 0;
		foreach($this->_viewData->weekly_sales as $item){
			$this->_viewData->weekly_sum += $item->total;
		}

		$this->_viewData->recent_clients = \Model\User::getList(['orderBy'=>'insert_time DESC', 'limit'=>5]);
		foreach($this->_viewData->recent_clients as $client){
			$client->insert_time = new DateTime($client->insert_time);
			$client->insert_time = $client->insert_time->format('m-d-Y H:i:s');
		}

		$this->_viewData->page_title = 'Dashboard';
		$this->setGraphsData();
		$this->loadView($this->_viewData);
	}

	private function setGraphsData(){
		$dataset = [
			'previous'=>[
				date("Y-m", mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")-1))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m"), date("d"),   date("Y")-1))=>(object)[]
			],
			'current'=>[
				date("Y-m", mktime(0, 0, 0, date("m")-6, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-5, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-4, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-3, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-2, date("d"),   date("Y")))=>(object)[],
				date("Y-m", mktime(0, 0, 0, date("m")-1, date("d"),   date("Y")))=>(object)[],
				date("Y-m")=>(object)[]
			]
		];
		$weeklySet = [
				'previous'=>[
						\Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(6)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(5)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(4)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(3)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(2)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(1)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->subYear()->previous(\Carbon\Carbon::SUNDAY)->format('Y-m-d')=>(object)[]
				],
				'current'=>[
						\Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(6)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(5)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(4)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(3)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(2)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->subWeeks(1)->format('Y-m-d')=>(object)[],
						\Carbon\Carbon::now()->previous(\Carbon\Carbon::SUNDAY)->format('Y-m-d')=>(object)[]
				]
		];
		foreach($dataset as $period=>$set){
			foreach($set as $date=>$data){
				$year = explode('-', $date)[0];
				$month = explode('-', $date)[1];
				$dataset[$period][$date] = \Model\Order::getDashBoardData($year, $month,'none');
			}
		}
		foreach($weeklySet as $period=>$set){
			foreach($set as $date=>$obj){
				$present = \Carbon\Carbon::createFromTimestamp(strtotime($date));
				$past = \Carbon\Carbon::createFromTimestamp(strtotime($date))->subWeek();
				$weeklySet[$period][$date] = \Model\Order::getWeeklyDashboard($present,$past,'none');
			}
		}
		$this->_viewData->graphsData = $dataset;
		$this->_viewData->weeklySet = $weeklySet;
	}
}










































