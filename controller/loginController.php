<?php

class loginController extends siteController {

	public function index_post(Array $params = []){
		\Model\User::login($_POST['email'],$_POST['password']);

		redirect($_POST['redirect-url']);

        redirect('/');
	}

	public function register_post(Array $params = [])
	{

        redirect('/');
        $user = \Model\User::loadFromPost();
		$hash = \Emagid\Core\Membership::hash($user->password);
		$user->password = $hash['password'];
		$user->hash = $hash['salt'];
        $emails = \Model\Newsletter::getItem(null,['where'=>"active = 1 and email = '".$user->email."'"]);

		if ($user->save()) {

			$customerRole = new \Model\User_Roles();
			$customerRole->role_id = 2;
			$customerRole->user_id = $user->id;
			$customerRole->save();
            if(!$emails){
                $newsletter = \Model\Newsletter::loadFromPost();
                $newsletter->save();
            }
			/** Set user as wholesale */
			if(isset($_POST['wholesale_opt_in']) && $_POST['wholesale_opt_in'] == 'on'){
				$wholesaleParams = [];
				$wholesaleList = ['w_name','w_email','w_phone','w_country','w_address','w_address2','w_city','w_state','w_zip'];
				foreach($wholesaleList as $value){
					$key = str_replace('w_','',$value);
					$wholesaleParams[$key] = $_POST[$value];
				}
				$wholesale = new \Model\Wholesale($wholesaleParams);
				$wholesale->save();
				$user->wholesale_id = $wholesale->id;
				$user->save();

				$mailmaster = new \Email\MailMaster();
				$mailmaster->setTo(['email'=>$wholesale->email,'name'=>$wholesale->name,'type'=>'to'])->setMergeTags([])->setTemplate('');
//				$mailmaster->send();
			}

			$n = new \Notification\MessageHandler('Welcome to Dji! Your account was successfully created.');
			$_SESSION["notification"] = serialize($n);

//			$email = new \Emagid\Email();
//			$email->addTo($user->email);
//			$email->subject('Welcome, ' . $user->full_name() . '!');
//			$email->body = '<p><a href="www.modernvice.com"><img src="https://modernvice.com/content/frontend/img/logo.png" /></a></p>'
//					. '<p><b>Dear ' . $user->full_name() . '</b></p>'
//					. '<p>Welcome to <a href="www.modernvice.com">modernvice.com</a>. To log in when visiting our site just click My Account at the top of every page, and then enter your e-mail address and password.</p>'
//					. '<p>When you log in to your account, you will be able to do the following:</p>'
//					. '<ul>'
//					. '<li>Proceed through checkout faster when making a purchase</li>'
//					. '<li>Check the status of orders</li>'
//					. '<li>View past orders</li>'
//					. '<li>Make changes to your account information</li>'
//					. '<li>Change your password</li>'
//					. '<li>Store alternative addresses (for shipping to multiple family members and friends!)</li>'
//					. '</ul>'
//					. '<p>If you have any questions about your account or any other matter, please feel free to contact us at support@modernvice.com or by phone at 877.752.6919.</p>'
//					. '<p>Thanks again!</p>'
//					. '<p>Find us on <a href="//TODO st-dev Modern Vice facebook URL">Facebook</a> and <a href="//TODO st-dev Modern Vice twitter url">Twitter</a>.</p>';
//			$email->send();

			$this->index_post($params);
		} else {
			$n = new \Notification\ErrorHandler($user->errors);
			$_SESSION["notification"] = serialize($n);
		};

		redirect($_POST['redirect-url']);
	}

	public function logout(){
		\Emagid\Core\Membership::destroyAuthenticationSession();
		if(isset($_SESSION['customerDetails']))
			unset($_SESSION['customerDetails']);
		redirect(SITE_URL);
	}

}