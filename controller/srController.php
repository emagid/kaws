<?php

class srController extends siteController {
	
    public function index(Array $params = [])
    {

        redirect('/');
        $where = '';
        $isTyped = false;
        if(isset($_GET['keywords'])&& $_GET['keywords']){
            $isTyped = true;
            $keywords = $_GET['keywords'];
            $explodeTags = explode(' ',strtolower(urldecode($keywords)));
            $tags = "'%".implode("%','%",$explodeTags)."%'";
            $this->viewData->keywords = $keywords;
            $this->viewData->params['keywords'] = $this->viewData->keywords;
            $where .= " (lower(product.slug) like '%" . strtolower(urldecode($keywords)) . "%' or lower(product.name) like '%" . strtolower(urldecode($keywords)) ."%' or
            lower(product.name) like all(array[". $tags."]) or lower(product.part_number) like '%". strtolower(urldecode($keywords)). "%' or lower(product.ean) like '%". strtolower(urldecode($keywords)). "%' or 
            lower(product.upc) like '%". strtolower(urldecode($keywords)). "%') and " ;
        }
        $where .= "product.active=1 AND product.availability != 0";

        if (count($this->filters->where) > 0){
            $isTyped = true;
            $where = array_merge(explode('AND', $where), $this->filters->where);
            $where = implode(' AND ', $where);
        }else{
            $where .= " AND product.type='1'";
        }
        if($isTyped){
            $parameters = [
                'where'=>$where];

            $sortBy = 'Sort By';
            if(isset($_GET['sortBy'])){
                switch($_GET['sortBy']){
                    case 'new':
                        $parameters['orderBy'] = 'insert_time desc';
                        $sortBy = 'Newest';
                        break;
                    case 'lth':
                        $parameters['orderBy'] = 'price asc';
                        $sortBy = 'Price: Low to High';
                        break;
                    case 'htl':
                        $parameters['orderBy'] = 'price desc';
                        $sortBy = 'Price: High to Low';
                        break;
                }
            }
            $conditions = [];
            $conditions = array_merge($parameters, $conditions);
            $products = \Model\Product::getList($conditions);
            $this->viewData->sortBy = $sortBy;
            $this->viewData->products = $products;
            $this->viewData->keywords = urldecode($keywords);
            $this->configs['Meta Title'] = "Result";
            $this->loadView($this->viewData);

        }else{
            redirect('/');
        }
    }

}