<?php

class blogController extends siteController
{

    public function index(Array $params = [])
    {
        redirect('/');
        $this->configs['Meta Title'] = "DJI | Blog";
        $this->loadView($this->viewData);
    }

    public function blog(Array $params = [])
    {
        redirect('/');
        $slug = (isset($params['blog_slug'])) ? $params['blog_slug'] : '';
        $this->viewData->blog = $blog = \Model\Blog::getItem(\Model\Blog::getIdBySlug($slug));

        $this->loadView($this->viewData);
    }

    public function blog_category(array $params = [])
    {
        redirect('/');
        $slug = (isset($params['b_category_slug'])) ? $params['b_category_slug'] : '';
        $blog_category = \Model\Blog_Category::getItem(\Model\Blog_Category::getIdBySlug($slug));
        $this->viewData->blogs = \Model\Blog::getList(["where" => "blog_category_id = {$blog_category->id}"]);
        $this->viewData->blog_category = $blog_category = \Model\Blog_Category::getItem(null, ['WHERE' => 'blog_category.active = 1 AND blog_category.slug LIKE \'' . $slug . '\'']);
        $this->loadView($this->viewData);
    }

}