<?php

class cartController extends siteController {

	public function index(Array $params = []){
        redirect('/');
		$this->configs['Meta Title'] = "Checkout - Shopping Bag";
        $shippings = [];
        foreach(\Model\Shipping_Method::getList() as $ship){
            if(in_array($ship->name,$shippings) === false){
                $shippings[json_decode($ship->cost,true)[0]] = $ship->name;
            }
            if($ship->is_default){
                $this->viewData->default_speed = $ship->name;
            }
        }
        krsort($shippings);
        $this->viewData->shipping_speeds = $shippings;

		if (count($this->viewData->cart->products) > 0){
			$arr = [];
			foreach($this->viewData->cart->products as $prod){
				$prod->details = json_decode($prod->variation,true);
				$arr[] = $prod;
			}
			$this->viewData->products = $arr;
		} else {
			$this->viewData->products = [];
		}
		/*if (isset($_SESSION['coupon'])){
			$coupon = \Model\Coupon::getItem(null, ['where'=>"code = '".$_SESSION['coupon']."'"]);
			if ((!is_null($coupon) 
					&& time() >= $coupon->start_time && time() < $coupon->end_time
					&& $this->viewData->subtotal >= $coupon->min_amount
					&& \Model\Order::getCount(['where'=>"coupon_code = '".$coupon->code."'"]) < $coupon->num_uses_all)){
				if ($coupon->discount_type == 1){
					$subtotal = $subtotal - $coupon->discount_amount;
				} else if ($coupon->discount_type == 2) {
					$subtotal = $subtotal * (1 - ($coupon->discount_amount/100));
				}
			}
		}

		$this->viewData->shipping_methods = [];
		$shipping_methods = \Model\Shipping_Method::getList(['orderBy'=>'id asc']);
		foreach ($shipping_methods as $shipping_method){
			$costs = json_decode($shipping_method->cost);
			$shipping_method->cost = 0;
			$shipping_method->cart_subtotal_range_min = json_decode($shipping_method->cart_subtotal_range_min);
			foreach($shipping_method->cart_subtotal_range_min as $key=>$range){
				if (is_null($range) || trim($range) == ''){
					$range = 0;
				}
				if ($subtotal >= $range && isset($costs[$key])){
					$shipping_method->cost = $costs[$key];
				}
			}
			$this->viewData->shipping_methods[] = $shipping_method;
		}

		if (isset($_COOKIE['shippingMethod'])){
			$this->viewData->shipping_method = $_COOKIE['shippingMethod'];
		} else {
			$this->viewData->shipping_method = \Model\Shipping_Method::getItem(null, ['orderBy'=>'is_default desc']);
			$this->viewData->shipping_method = $this->viewData->shipping_method->id;
			setcookie('shippingMethod', $this->viewData->shipping_method, time()+60*60*24*100, "/");
		}*/

		parent::index($params);
	}

	public function add($params)
	{
	    //If product is not Active, fail add to cart || If product color is not Active, fail to add to cart
        $cartIds = isset($_COOKIE['cart']) && $_COOKIE['cart'] ? json_decode($_COOKIE['cart'],true) : [];
		$carts = $this->viewData->cart->cart;
        $cart = new \Model\Cart();
        if($this->viewData->user){
            $cart->user_id = $this->viewData->user->id;
            $cart->guest_id = session_id();
        } else {
            $cart->user_id = 0;
            $cart->guest_id = session_id();
        }
        $cart->product_id = $_POST['product_id'];
        $cart->quantity = $_POST['quantity'];
        $total = $this->viewData->cart->total + (\Model\Product::getItem($_POST['product_id'])->getPrice(null,null,false,$this->viewData->user)) * $_POST['quantity'];
        $html='';
        $message = 'failed';
        foreach($carts as $ca){
//            $product = \Model\Product::getItem($ca->product_id);
            $rCart = \Model\Cart::getItem($ca->id);
			if(!in_array($ca->id,$cartIds)){
				$cartIds[] = $ca->id;
			}
            if($ca->product_id==$cart->product_id){
                $rCart->quantity = $cart->quantity + $rCart->quantity;
                $rCart->save();
				setcookie('cart',json_encode($cartIds), time()+60*60*24*100, "/");
                $html = $this->buildCart();
                $this->toJson(['status' => "success", 'message' => "Updated",'cartHtml'=>$html,'total'=>number_format($total,2),'quantity'=>count($this->viewData->cart->cart)]);
            }
        }
        if ($cart->save()) {
            setcookie('cart',json_encode($cartIds), time()+60*60*24*100, "/");
            $html = $this->buildCart();
            $this->toJson(['status' => "success", 'message' => "Inserted",'cartHtml'=>$html,'total'=>number_format($total,2),'quantity'=>count($this->viewData->cart->cart)+1]);
        } else {
            $this->toJson(['status'=>"failed",'message'=>"Failed to add to cart"]);
        }

	}

	public function remove_product($params){
//		unset($this->viewData->cart->products[$params['id']]);
//		unset($this->viewData->cart->productDetails[$params['id']]);
//		$this->updateCookies();
		$id = isset($params['id']) && $params['id'] > 0 ? $params['id']: null;
		if($id){
			$cart = \Model\Cart::getItem($id);
			$cart->delete($id);

			if(isset($_COOKIE['cart']) && $_COOKIE['cart']){
				$cookie = json_decode($_COOKIE['cart'],true);
				unset($cookie[array_search($id,$cookie)]);
				setcookie('cart',json_encode($cookie), time()+60*60*24*100, "/");
			}
		}
		if(isset($_SESSION['coupon']) && $_SESSION['coupon']){
			$total = array_sum(array_map(function($item){
				$prod = \Model\Product::getItem($item->product_id);
				$color = json_decode($item->variation,true)['color'];
				return $prod->getPrice($color);
			},(array)\Model\Cart::getActiveCart($this->viewData->user ? $this->viewData->user->id: 0,session_id())));
			$carbon = time();
			$coupon = \Model\Coupon::getItem(null,['where'=>"lower(code) = lower('{$_SESSION['coupon']}') and start_time < '$carbon' and end_time > '$carbon' and (min_amount = 0 or min_amount <= $total)"]);
			if(!$coupon){
				unset($_SESSION['coupon']);
			}
		}
		redirect(SITE_URL.'checkout/payment');
	}

	private function updateCookies(){
		setcookie('cartProducts', json_encode($this->viewData->cart->products), time()+60*60*24*100, "/");
		if(isset($this->viewData->cart->productDetails)){
			setcookie('cartProductDetails',json_encode($this->viewData->cart->productDetails), time()+60*60*24*100, "/");
		}

	}
	
	public function updateShipping(Array $params = []){
		if(isset($_GET['id']) && is_numeric($_GET['id']) && !is_null(\Model\Shipping_Method::getItem($_GET['id']))){
			setcookie('shippingMethod', $_GET['id'], time()+60*60*24*100, "/");
		}
	}

	public function applyCoupon(Array $params = []){
		if (isset($_GET['code'])){
			$coupon = \Model\Coupon::getItem(null, ['where'=>" code = '".$_GET['code']."'"]);
			if (is_null($coupon)){
				$_SESSION['coupon'] = null;
				echo json_encode(['error'=>['code'=>1,'message'=>'Invalid coupon code.'], 'coupon'=>null]);
			} else if (time() >= $coupon->start_time && time() < $coupon->end_time) {
				$cartTotal = (float) str_replace(',', '', $this->viewData->cart->total);
				if ($cartTotal >= $coupon->min_amount){
					$already_used = \Model\Order::getCount(['where'=>"coupon_code = '".$coupon->code."'"]);
					if (is_null($already_used)){
						$already_used = 0;
					}
					
					if (is_null($coupon->num_uses_all) || $already_used < $coupon->num_uses_all){
						$_SESSION['coupon'] = $coupon->code;
						echo json_encode(['error'=>['code'=>0,'message'=>''], 'coupon'=>$coupon]);
					} else {
						$_SESSION['coupon'] = null;
						echo json_encode(['error'=>['code'=>4,'message'=>'Coupon sold out.'], 'coupon'=>null]);
					}
				} else {
					$_SESSION['coupon'] = null;
					echo json_encode(['error'=>['code'=>2,'message'=>'Coupon requires a minimum amount of '.$coupon->min_amount.'.'], 'coupon'=>null]);		
				}
			} else {
				$_SESSION['coupon'] = null;
				echo json_encode(['error'=>['code'=>3,'message'=>'Coupon currently unavailable.'], 'coupon'=>null]);
			}
		} else {
			$_SESSION['coupon'] = null;
			echo json_encode(['error'=>['code'=>1,'message'=>'Invalid coupon code.'], 'coupon'=>null]);
		}
	}

	public function buildCart($total = null){
	    $total = $total?:$this->viewData->cart->total;
        $sesh_id = session_id();
        $user_id = $this->viewData->user ? $this->viewData->user->id: 0;
        $where = 'active = 1';
        if($user_id){
            $user = \Model\User::getItem($user_id);
            $where .= " and user_id = {$user_id}";
        } else {
            $user = null;
            $where .= " and guest_id = '{$sesh_id}'";
        }
        $campaign = $this->viewData->cart->campaign;
        $carts = \Model\Cart::getList(['where'=>$where]);
        $html='<div class="total">$'.number_format($total,2).'</div>';
        foreach($carts as $ca){
            $cproduct = \Model\Campaign_Product::getItem(null,['where'=>"product_id = $ca->product_id AND campaign_id = $campaign->id"]);
            $product = \Model\Product::getItem($ca->product_id);
            $rCart = \Model\Cart::getItem($ca->id);
            $limit ='<button class="button" type="button" data-operation="inc" style="display:none" data-quantity="'. $rCart->quantity .'" data-cart_id="'. $ca->id .'">
								+
							</button>';
            if($cproduct->max_per_user > $rCart->quantity){
                $limit ='<button class="button" type="button" data-operation="inc" data-quantity="'. $rCart->quantity .'" data-cart_id="'. $ca->id .'">
								+
							</button>';
            }
            $html .= '<div class="header_cart_prod" data-quantity="' . $rCart->quantity . '" data-prod_slug="' . $product->slug . '">
                		<div class="content">
							<div class="left_float"><img src="' . $product->featuredImage() .'"/></div>
							<div class="cart_text">
								<p class="name">'. $product->name .'</p>
								<p class="price">&#36;' . $rCart->quantity . ' USD' . $product->getPrice(null,null,false,$user) .'</p>
							</div>
            		  	</div>
            		  <div class="cartUpdate" data-cart_id="'. $ca->id .'" data-quantity="'. $rCart->quantity .'">'.
							$limit
							.'<button class="button" type="button" data-operation="dec" data-quantity="'. $rCart->quantity .'" data-cart_id="'. $ca->id .'">
								-
							</button>
							<button type="button" data-operation="remove" data-quantity="'. $rCart->quantity .'" data-cart_id="'. $ca->id .'">
								REMOVE
							</button>
					  </div>
            		  </div>';
        }
        return $html;

    }
	public function buildCart_old()
	{
//		$cart = $this->viewData->cart;
		$sesh_id = session_id();
		$user_id = $this->viewData->user ? $this->viewData->user->id: 0;
		$where = 'active = 1';
		if($user_id){
			$where .= " and user_id = {$user_id}";
		} else {
			$where .= " and guest_id = '{$sesh_id}'";
		}
		$cart = \Model\Cart::getList(['where'=>$where]);
		$html = '';
		$total = 0;
		$clearance = $categoryCouponBlock = $detailAmount = 0;
		foreach ($cart as $c) {
			if($c->product_id){
				$product = \Model\Product::getItem($c->product_id);
				$color_id = json_decode($c->variation,true)['color'];
				$colorObj = \Model\Color::getItem($color_id);
				$sizeId = json_decode($c->variation,true)['size'];
				$sizeObj = \Model\Size::getItem($sizeId);
				$couponObj = \Model\Coupon::checkDiscount();
				$categoryIds = array_map(function($i){return $i->id;},$product->getAllCategories(true));

				$name = $product->name;
				$price = $product->getPrice($color_id,$sizeId);
				if ($c->clearance && \Model\Clearance::checkClearance($c->product_id, $color_id, $sizeId)) {
					if($couponObj && $couponObj->active_clearance == 1) {
						$total += $product->getPrice($color_id, $sizeId,true);
					} else {
						$clearance += ($price = $product->getPrice($color_id, $sizeId, true));
					}
					$name .= '(Clearance)';
				} else if ($couponObj && ($couponObj->active_categories == '' || $couponObj->active_categories == null || json_decode($couponObj->active_categories,true) == [])) {
					$categoryCouponBlock += $product->getPrice($color_id, $sizeId);
				} else if ($couponObj && count(array_intersect(json_decode($couponObj->active_categories,true),$categoryIds)) != count($categoryIds)) {
					$categoryCouponBlock += $product->getPrice($color_id, $sizeId);
				} else {
					$total += $product->getPrice($color_id, $sizeId);
				}
				$url = '/products/' . $product->slug;
				$img = UPLOAD_URL . 'products/' . $product->featuredImage(null,$color_id);
				$colorText = strlen($colorObj->name) < 20 ? $colorObj->name: substr($colorObj->name,0,20).'...';
				$sObjUs = $sizeObj->us_size;
				$sObjEu = $sizeObj->eur_size;
			} else {
				$detail = json_decode($c->details,true);
				$detailAmount = $detail['amount'];
//				$total += $detail['amount'];
				$gc = \Model\Gift_Card::getItem($detail['gift_card_id']);

				$url = '/gift_cards/product/'.$detail['gift_card_id'];
				$img = UPLOAD_URL.'gift_cards/'.$gc->image;
				$name = $gc->name;
				$colorText = '';
				$sObjUs = '';
				$sObjEu = '';
				$price = number_format($detail['amount'],2);
			}

			$html .= '<li>
					<div class="item_container">
						<div class="bagEditItem">
							<div class="editClose circle_x">
								<circle>
									<span></span>
									<span></span>
								</circle>
							</div>
							<div class="mediaWrapper item_img">
								<a class="cartImg" href="'.$url.'">
									<img src="'.$img.'">
								</a>
								<div class="remove_item_cart btn btn_black" data-cart_id="'.$c->id.'">
									<p>Remove</p>
								</div>
							</div>
							<div class="item_info">
								<div class="itemName">
									<h4 class="as_l">' . $name . '</h4>
								</div>
								<div class="itemName">
									<h4 class="as_l">' . $colorText . '</h4>
								</div>
								<div class="itemName">
									<h4 class="as_l">US: ' . $sObjUs . ' | EU: '. $sObjEu .'</h4>
								</div>
								<div class="product_price">
									<h4 class="as_l">$' . $price . '</h4>
								</div>
							</div>
						</div>
					</div>
				</li>';
		}
		$discount = 0;
		$discountText = '';
		if(($coupon = \Model\Coupon::checkDiscount())){
			$discount = $coupon->getDiscountAmount($total);
			$discountText = $coupon->discount_type == 1 ? "OFF": "$coupon->discount_amount% OFF";
			$total -= $discount;
		}
		//TODO Sale tier Christmas 2016, remove when unnecessary
		$total -= \Model\Sale::tiers($total);
		//
		$total += $clearance;
		$total += $categoryCouponBlock;
		$total += $detailAmount;
		if ($html) {
			return ['status'=>"success",'sum' => '$'.number_format($total,2), 'cart' => $html, 'discount', 'discount'=>$discount,'discountText'=>$discountText];
		} else {
			return ['status'=>"failed",'message'=>"Failed to load"];
		}
	}

	public function updateCart(){
		$cart_id = $_POST['cart_id'];
		$quantity = $_POST['quantity'];
		$op = $_POST['op'];
		$total = 0;
//		$color_id = $_POST['color_id'];
//		$size_id = $_POST['size_id'];
        $cart = \Model\Cart::getItem($cart_id);

        if($cart_id){

            $currQuantity = $cart->quantity;

            $currTotal = $this->viewData->cart->total;
            switch($op) {
                case "inc":
                    $cart->quantity += 1;
                    $total = $currTotal + (\Model\Product::getItem($cart->product_id)->getPrice(null,null,false,$this->viewData->user));
                    break;
                case "dec":
                    $cart->quantity -= 1;
                    $total = $currTotal - (\Model\Product::getItem($cart->product_id)->getPrice(null,null,false,$this->viewData->user));
                    break;
                case "remove":
                    $cart->quantity = 0;
                    $total = $currTotal - (\Model\Product::getItem($cart->product_id)->getPrice(null,null,false,$this->viewData->user)) * $currQuantity;
                    break;
                case "update":
                    $total = $quantity;
                    break;
            }
            $quant = count($this->viewData->cart->cart);
            $quantity = $cart->quantity;
            if($quantity == 0){
                $this->deleteItem();
            }
            if($quantity == 0 || $total == 0){
                $quant -= 1;
            }
        }




//		$variation = json_decode($cart->variation,true);
//		$variation['color'] = $color_id;
//		$variation['size'] = $size_id;
//		$var = json_encode($variation);
//		$cart->variation = $var;

        if(!$cart && $_POST['dropdown']){
            $html = $this->buildCart();
            $this->toJson(['status'=>"success",'total'=>'0'. number_format($total,2),'cartHtml'=>$html,'quantity'=>$quant]);
        } else {
            if($cart->save()){
                if($_POST['dropdown']){
                    $html = $this->buildCart($total);
                    $this->toJson(['status'=>"success",'total'=>'&#36;'. number_format($total,2),'cartHtml'=>$html,'quantity'=>$quant]);
                } else {
                    $this->toJson(['status'=>"success"]);
                }
            } else {
                $this->toJson(['status'=>"failed", 'message'=>"Failed to update"]);
            }
        }
	}
	public function deleteItem(){
		$cart_id = $_POST['cart_id'];
		if($cart_id){
			$cart=\Model\Cart::getItem($cart_id);
			$cart->delete($cart_id);
			if(isset($_SESSION['coupon']) && $_SESSION['coupon']){
				$total = array_sum(array_map(function($item){
					$prod = \Model\Product::getItem($item->product_id);
					$color = json_decode($item->variation,true)['color'];
					$size = json_decode($item->variation,true)['size'];
					$couponObj = \Model\Coupon::checkDiscount();
					$categoryIds = array_map(function($i){return $i->id;},$prod->getAllCategories(true));
					if($item->clearance && \Model\Clearance::checkClearance($item->product_id,$color,$size)){
						if($couponObj && $couponObj->active_clearance == 1){
							return $prod->getPrice($color,$size);
						} else {
							return 0;
						}
					} else if ($couponObj && ($couponObj->active_categories == '' || $couponObj->active_categories == null || json_decode($couponObj->active_categories,true) == [])) {
						return 0;
					} else if ($couponObj && count(array_intersect(json_decode($couponObj->active_categories,true),$categoryIds)) != count($categoryIds)) {
						return 0;
					} else {
						return $prod->getPrice($color,$size);
					}
				},(array)\Model\Cart::getActiveCart($this->viewData->user ? $this->viewData->user->id: 0,session_id())));
				$carbon = time();
				$coupon = \Model\Coupon::getItem(null,['where'=>"lower(code) = lower('{$_SESSION['coupon']}') and start_time < '$carbon' and end_time > '$carbon' and (min_amount = 0 or min_amount <= $total)"]);
				if(!$coupon || $total == 0){
					unset($_SESSION['coupon']);
				}
			}
			if(!isset($_POST['dropdown']))$this->toJson(['status'=>"success"]);
		}else{
			if(!isset($_POST['dropdown']))$this->toJson(['status'=>"failed",'message'=>"Failed to delete"]);
		}
	}
}