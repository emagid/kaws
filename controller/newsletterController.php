<?php

class newsletterController extends siteController {
	
	public function add(){
		$emails = \Model\Newsletter::getItem(null,['where'=>"active = 1 and email = '".$_POST['email']."'"]);
		if(!$emails){
			$newsletter = \Model\Newsletter::loadFromPost();
			$newsletter->discount_redeemed = 0;
			if ($newsletter->save()){
				$this->addToMailChimp($newsletter->email);

				$n = new \Notification\MessageHandler('Your email was included on our newsletter list.');
				$_SESSION["notification"] = serialize($n);

				/*$email = new \Emagid\Email();
				global $emagid;
				$emagid->email->from->email = 'report@emagid.com';
				$email->addTo($newsletter->email);
				$email->subject('Newsletter subscription success');
				$email->body = '<p><a href="www.modernvice.com"><img src="https://modernvice.com/content/frontend/img/logo.png" /></a></p>'
					.'<p>Thanks for signing up to receive our newsletter! We\'ll keep you informed on the hottest trends and insider deals at modernvice.com.</p>'
					.'<p>Regards,<br />Modern Vice Team</p>'
					.'<p>Find us on <a href="//TODO st-dev modernvice FB">Facebook</a> and <a href="//TODO st-dev modernvice twitter">Twitter</a>.</p>';
				$email->send();
				$this->toJson(['status' => "success", 'message' => "Your email was included on our newsletter list."]);*/

				$email = new \Email\MailMaster();
				$email->setTo(['email'=>$_POST['email'],'type'=>'to'])->setTemplate('modern-vice-newsletter-email')->send();
				$this->setPopupCookie();
				$this->toJson(['status'=>'success']);
			}
		} else {
			$this->toJson(['status' => "fail", 'message' => "You're already registered to our newsletter list."]);
//			$n = new \Notification\ErrorHandler("You're already signed up for our newsletter.");
//			$_SESSION['notification'] = serialize($n);
		}

//      	redirect($_POST['redirectTo']);
	}

	public function setPopupCookie(){
		setcookie('newsletterPopup','false',time()+60*60*24*100,'/');
	}

	private function addToMailChimp($email){
		$apikey = MAILCHIMP_API;
        $auth = base64_encode( 'user:'.$apikey );

        $data = array(
            'apikey'        => $apikey,
            'email_address' => $email,
            'status'        => 'subscribed'
        );
        $json_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://us9.api.mailchimp.com/3.0/lists/6e5d8beec8/members/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);                                                                                      

        $result = curl_exec($ch);

        //dd(json_decode($result));

        // var_dump($result);
        // die('Mailchimp executed');
	}

}