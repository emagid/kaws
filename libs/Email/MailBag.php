<?php
namespace Email;
use function GuzzleHttp\Psr7\build_query;
use Model\Mail_Log;

/**
 * Created by PhpStorm.
 * User: Foran
 * Date: 2/12/2018
 * Time: 5:49 PM
 */
class MailBag{
    private $mandrill;

    //Query Strings
    private $startDate; //format yyyy-mm-dd
    private $endDate;  //format yyyy-mm-dd
    private $tags = ['kaws'];
    private $query = [
        'email'=>'',
        'full_email'=>'',
        'subject'=>'',
        'tags'=>'',
        'opens.ua'=>'',
        'opens.ip'=>'',
        'opens.location'=>'',
    ];
    private $limit = 10;

    public $search_terms = [
        'startDate' => '',
        'endDate' => '',
        'tags' => null,
        'query' => 'kaws',
        'limit' => ''
        ];

    protected $response_emails;
    protected $response_email;


    public function __construct()
    {
        if(class_exists('\Mandrill')) {
            global $emagid;
            $this->mandrill = new \Mandrill($emagid->email->api_key);
        } else {
            throw new \Exception('Could not find Mandrill class. You may include it in the config array when instantiating Emagid;');
        }
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
//    public function setTags($tags)
//    {
//        if(is_array($tags)){
//            $this->tags = array_merge(['kaws'],$tags);
//        } else {
//            $this->tags = ['kaws',$tags];
//        }
//    }

    /**
     * @param mixed $tags
     */
//    public function addTags($tags){
//        if(is_array($tags)){
//            $this->tags = array_merge($this->tags,$tags);
//        } else {
//            $this->tags[] = $tags;
//        }
//    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param mixed $query
     */
    public function setQuery(Array $query = [])
    {
        $this->query = [
            'email'=>'',
            'full_email'=>'',
            'sender'=>'kaws@emagid.com',
            'subject'=>'',
            'tags'=>'',
            'opens.ua'=>'',
            'opens.ip'=>'',
            'opens.location'=>'',
        ];
        $this->addToQuery($query);
    }

    public function addToQuery(Array $query = [])
    {
        $this->query = array_merge($this->query,$query);
    }

    private function buildQuery(){
        $strings = [];
        foreach ($this->query as $field => $val){
            if($val != ''){
                $strings[] = "$field:$val";
            }
        }
        return implode(' AND ',$strings);
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function retrieveEmails(Array $searchTerms = []){
        foreach ($this->search_terms AS $field => $val){
            if($searchTerms[$field] && $searchTerms[$field] != ''){
                if($field == 'query'){
                    $query = $this->setQuery($searchTerms[$field]);
                    $this->$field = $this->search_terms[$field] = $query;
                } else {
                    $this->$field = $this->search_terms[$field] = $searchTerms[$field];
                }
            }
        }
        $this->response_emails = $this->mandrill->messages->search($this->buildQuery(),$this->startDate,$this->endDate,null,null,[$this->mandrill->apikey],$this->limit);
        return $this->response_emails;
    }

    public function retrieveEmailContents($mandrill_id = null){
        if($mandrill_id == null && $this->response_email){
            $mandrill_id = $this->response_email['_id'];
        }
        if($mandrill_id){
            return $this->mandrill->messages->content($mandrill_id);
        }
        return false;
    }

    public function retrieveEmailStats($mandrill_id){
        $this->response_email = $this->mandrill->messages->info($mandrill_id);
        return $this->response_email;
    }

    /**
     * @return mixed
     */
    public function getResponseEmails()
    {
        return $this->response_emails;
    }

    /**
     * @return mixed
     */
    public function getResponseEmail()
    {
        return $this->response_email;
    }

}