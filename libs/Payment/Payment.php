<?php

namespace Payment;

require_once(__DIR__.'/../Payeezy/Payeezy.php');
require_once(__DIR__.'/../../conf/emagid.payment.php');

class Payment{
    private static $payeezy;
    private $cardName;
    private $cardNum;
    private $cardType;
    private $cardCVV;
    private $cardExpiry;
    private $amount;
    private $merchantRef;
    private $currencyCode;
    private $method;
    private $transaction;
    private $errorCode;
    private $errorMsg;

    function __construct()
    {
        self::$payeezy = new \Payeezy();
        self::$payeezy->setApiKey(PAYEEZY_KEY);
        self::$payeezy->setApiSecret(PAYEEZY_SECRET);
        self::$payeezy->setMerchantToken(PAYEEZY_MERCHANT_TOKEN);
        self::$payeezy->setTokenUrl("https://api-cert.payeezy.com/v1/transactions/tokens");
        self::$payeezy->setUrl("https://api-cert.payeezy.com/v1/transactions");
    }

    /**
     * @return mixed
     */
    public function getCardName()
    {
        return $this->cardName;
    }

    /**
     * @param mixed $cardName
     */
    public function setCardName($cardName)
    {
        $this->cardName = $this->processInput($cardName);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardNum()
    {
        return $this->cardNum;
    }

    /**
     * @param mixed $cardNum
     */
    public function setCardNum($cardNum)
    {
        $this->cardNum = $this->processInput($cardNum);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param mixed $cardType
     */
    public function setCardType($cardType)
    {
        $this->cardType = $this->processInput($cardType);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardCVV()
    {
        return $this->cardCVV;
    }

    /**
     * @param mixed $cardCVV
     */
    public function setCardCVV($cardCVV)
    {
        $this->cardCVV = $this->processInput($cardCVV);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardExpiry()
    {
        return $this->cardExpiry;
    }

    /**
     * @param mixed $cardExpiry
     */
    public function setCardExpiry($cardExpiry)
    {
        $this->cardExpiry = $this->processInput($cardExpiry);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $this->processInput($amount);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMerchantRef()
    {
        return $this->merchantRef;
    }

    /**
     * @param mixed $merchantRef
     */
    public function setMerchantRef($merchantRef)
    {
        $this->merchantRef = $this->processInput($merchantRef);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param mixed $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $this->processInput($currencyCode);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $this->processInput($method);
        return $this;
    }



    public function processInput($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return strval($data);
    }

    public function transactionFields(){
        $arr = [
            'amount'=>$this->getAmount(),
            'card_number'=>$this->getCardNum(),
            'card_type'=>$this->getCardType(),
            'card_holder_name'=>$this->getCardName(),
            'card_cvv'=>$this->getCardCVV(),
            'card_expiry'=>$this->getCardExpiry(),
            'merchant_ref'=>$this->getMerchantRef(),
            'currency_code'=>$this->getCurrencyCode(),
            'method'=>$this->getMethod()
        ];
        return $arr;
    }

    public function authorize(){
        $auth = json_decode(self::$payeezy->authorize($this->transactionFields()));
        $this->transaction = $auth;
    }

    public function purchase(){
        $pur = json_decode(self::$payeezy->purchase($this->transactionFields()));
        $this->transaction = $pur;
    }

    public function tokenize(){
        $pur = json_decode(self::$payeezy->getTokenPayload($this->transactionFields()));
        $this->transaction = $pur;
    }
}