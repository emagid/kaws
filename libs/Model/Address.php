<?php
namespace Model;

class Address extends \Emagid\Core\Model {
	static $tablename = "address";
	public static $fields = [
		'user_id',	
		'is_default'=>['type'=>'boolean'],
		'first_name',
		'last_name',
		'phone',
		'address',
		'address2',
		'city',
		'state'=>['required'=>true],
		'country'=>['required'=>true],
		'zip'=>['required'=>true],
		'label'
	];

    public function formatted() {
        $addr = $this->address . ' ' . $this->address2 . '<br>' . $this->city . ', ' . $this->state . '. ' . $this->zip . ' ' . $this->country ;
        return str_replace(', .','.',$addr);
    }
}