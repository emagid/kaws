<?php
namespace Model;

use Carbon\Carbon;
use EmagidService\S3;

class Product extends \Emagid\Core\Model
{

    public static $tablename = "product";
    public static $type = [1=>'Main',2=>'Combo',3=>'Spare',4=>'License Key',5=>'Single Waypoint',6=>'200 Waypoint',7=>'Kit',8=>'Code',9=>'Card'];

    public static $fields = [
        'brand',
        'name' => ['required' => true],
        'price' => ['type' => 'numeric', 'required' => true],
        'description',
        'gender', //[1=>"Unisex", 2=>"Men", 3=>"Women"]
        'msrp',
        'quantity',
        'slug' => ['required' => true, 'unique' => true],
        'tags',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'featured_image',
        'deal_msg',
        'length',
        'sale_icon',
        'width',
        'height',
        'condition',
        'type',
        'color',
        'size',
        'availability', // TODO st-dev: replaced day_delivery with availability
        'age_group',
        'sub_brand',
        'alias',
        'options',
        'video_link',
        'parent_variant',
        'heel_height',
        'made_to_order', //boolean
        'sole',
        'related_products',
        'details',
        'total_sales',
        'default_color_id',
        'linked_list',
        'linked_name',
        'part_number',
        'ean',
        'upc',
        'remark',
        'specs',
        'sku',
        'feature_name_1',
        'feature_name_2',
        'feature_name_3',
        'feature_name_4',
        'feature_1',
        'feature_2',
        'feature_3',
        'feature_4',
        'feature_image_1',
        'feature_image_2',
        'banner',
        'display_banner',
        'preorder',
        'image_alt'
    ];

    static $comparison = ['battery','max_speed','max_tilt','max_wind_resist','max_flight_time','temperature_range','vision_sys','obj_avoidance','fov','camera','sensor','lens','iso_range','mech_shutter_speed','elec_shutter_speed','image_size','piv_image_size','still_photo_modes','video_recording_modes','max_video_bitrate','photo','video','supported_sd_cards','operating_temp_range','intelligent_flight_battery','capacity','voltage','battery_type','operating_system'];

    static $relationships = [
        [
            'name' => 'product_category',
            'class_name' => '\Model\Product_Category',
            'local' => 'id',
            'remote' => 'product_id',
            'remote_related' => 'category_id',
            'relationship_type' => 'many'
        ],
        [
            'name' => 'product_accessory',
            'class_name' => '\Model\Product_Accessory',
            'local' => 'id',
            'remote' => 'product_id',
            'remote_related' => 'accessory_product_id',
            'relationship_type' => 'many'
        ]
    ];

//    static $availability = [2=>'Active',1=>'Sold Out',0=>'Hidden', 3 => 'Pre-order'];
    static $availability = [2=>'Active',1=>'Sold Out',0=>'Hidden'];
    static $feed_availability = [2=>'In Stock',1=>'Out of Stock',0=>'Out Of Stock', 3 => 'Preorder'];

    static $SORT_MAP = [
        'product.insert_time desc', // newest
        'product.total_sales desc', // best seller
        'product.price desc', // price high to low
        'product.price asc', // price low to high
    ];

    static $sale_icons = [
        1=>'cyber',
        2=>'gift',
        3=>'limited',
        4=>'limited_deal',
        5=>'on_sale',
        6=>'shipping'
    ];
    
    public function isPartOfActiveCampaign(){
        $products = Campaign::getActiveCampaignProducts();
        foreach($products as $product){
            if($product->id == $this->id){
                return true;
            } 
        }
        
        return false;
    }

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id, name, featured_image,price, msrp, quantity, slug, availability,type from product where active = 1 and type = '1' and (";
        if (is_numeric($keywords)) {
            $sql .= "CAST(id as TEXT) like '" . $keywords . "%' or ";
        }
        $explodeTags = explode(' ',strtolower(urldecode($keywords)));
        $tags = "'%".implode("%','%",$explodeTags)."%'";
        $sql .= " lower(product.slug) like '%" . strtolower(urldecode($keywords)) . "%' or lower(product.name) like '%" . strtolower(urldecode($keywords)) ."%' or
            lower(product.name) like all(array[". $tags."]) or lower(product.part_number) like '%". strtolower(urldecode($keywords)). "%' or lower(product.ean) like '%". strtolower(urldecode($keywords)). "%' or 
            lower(product.upc) like '%". strtolower(urldecode($keywords)). "%')
             order BY product.name ASC limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public static function adminSearch($keywords, $limit = 20)
    {
        $colors = Color::getList(['where'=>"lower(name) like '%".strtolower($keywords)."%'"]);
        $sql = "select id, name, featured_image,price, msrp, slug, color,availability from product where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "CAST(id as TEXT) like '" . $keywords . "%' or ";
        }
        $colorList = implode('"%\',\'%"',array_map(function($item){return $item->id;},$colors));
        $explodeTags = explode(' ',strtolower(urldecode($keywords)));
        $tags = "'%".implode("%','%",$explodeTags)."%'";
//        $sql .= " lower(name) like '%" . strtolower(urldecode($keywords)) . "%' or
//            lower(tags) like any(array[". $tags."])
//            ) order BY name ASC limit " . $limit;
        $sql .= " lower(name) like all (array[".$tags."])";
        if($colorList){
            $sql .= " or color like any(array['%\"$colorList\"%'])";
        }
        $sql .= ") order BY name ASC limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public static function getPriceSum($productsStr)
    {
        $total = 0;
        foreach (explode(',', $productsStr) as $productId) {
            $product = self::getItem($productId);
            $total += $product->price;
        }
        return $total;
    }

    /**
     * @param null $color_id = for querying price variations in Product_Attributes
     * @param null $size_id
     * @param bool $clearance
     * @return mixed
     */
    public function price($color_id = null, $size_id = null, $clearance = false, $user = null){
        $p = $this->price;
        $queryColorId = $color_id ? : $this->default_color_id;
        $queryArr = ["product_id = $this->id","color_id like '%\"$queryColorId\"%'"];
        $queryArr[] = "size_id like '%\"$size_id\"%'";
        if($user && $user->wholesale_id && ($ws = Wholesale::getItem(null,['where'=>"id = $user->wholesale_id and status = 1"])) && ($wsItem = Wholesale_Item::getItem(null,['where'=>"wholesale_id = $ws->id and product_id = $this->id"]))){
            return $wsItem->price;
        } else if($clearance && ($clear = Clearance::checkClearance($this->id,$queryColorId,$size_id))){
            $value = $clear->price;
        } else if(($productAttribute = Product_Attributes::getItem(null,['where'=>"product_id = $this->id and color_id = $queryColorId and name = 'price'"])) && $productAttribute->value > 0){
//            Increment
//            $value = $p + $productAttribute->value;

//            Override
            $value = $productAttribute->value;
        } else {
            $value = $p;
        }
        if(($eventAttribute = Product_Attributes::getEventById($this->id,$queryColorId)) && $eventAttribute->value > 0){
            $event = Event::getItem($eventAttribute->value);
            if($event && $event->validateEvent()) {
                return $event->calculateEvent($value);
            }
        }
        $now = Carbon::now();
        $sales = Sale::getList(['where'=>"'$now' between start_date::timestamp and end_date::timestamp"]);
        if($sales){
            foreach($sales as $sale){
                $value = $sale->calculateSale($value,$this->id);
            }
            return $value;
        } else {
            return $value;
        }
    }

    public function basePrice($color_id = null,$user = null){
        $p = $this->price;
        $queryColorId = $color_id ? : $this->default_color_id;
        if($user && $user->wholesale_id && ($ws = Wholesale::getItem(null,['where'=>"id = $user->wholesale_id and status = 1"])) && ($wsItem = Wholesale_Item::getItem(null,['where'=>"wholesale_id = $ws->id and product_id = $this->id"]))){
            return $wsItem->price;
        } else if(($productAttribute = Product_Attributes::getItem(null,['where'=>"product_id = $this->id and color_id = $queryColorId and name = 'price'"]))&& $productAttribute->value > 0){
//            return $p + $productAttribute->value;
            return $productAttribute->value;
        } else {
            return $p;
        }
    }

    /**
     * @param null $color_id = for querying price variations in Product_Attributes
     * @param null $size_id
     * @return mixed
     */
    public function msrp($color_id = null,$size_id = null,$clearance = false){
        $m = $this->msrp;
        $value = $m;
        $queryColorId = $color_id ? : $this->default_color_id;
        if(($productAttribute = Product_Attributes::getItem(null,['where'=>"product_id = $this->id and color_id = $queryColorId and name = 'price'"]))&& $productAttribute->value > 0){
            $value = $productAttribute->value;
        }
        if(($eventAttribute = Product_Attributes::getEventById($this->id,$queryColorId)) && $eventAttribute->value > 0){
            $event = Event::getItem($eventAttribute->value);
            if($event->validateEvent()) {
                return $event->calculateEvent($value);
            }
        }
        $now = Carbon::now();
        $sales = Sale::getList(['where'=>"'$now' between start_date::timestamp and end_date::timestamp",'orderBy'=>"sale_type desc"]);
        if($sales){
            foreach($sales as $sale){
                $value = $sale->calculateSale($value,$this->id);
            }
            return $value;
        } else {
            return $value;
        }
    }

    public function getPrice($color_id = null,$size_id = null,$clearance = false,$user = null){
        if($user && $user->wholesale_id && ($ws = Wholesale::getItem(null,['where'=>"id = $user->wholesale_id and status = 1"])) && ($wsItem = Wholesale_Item::getItem(null,['where'=>"wholesale_id = $ws->id and product_id = $this->id"]))){
            return $wsItem->price;
        } else if($this->msrp != 0){
            return $this->msrp($color_id,$size_id,$clearance);
        } else {
            return $this->price($color_id,$size_id,$clearance,$user);
        }
    }

    public function getPriceRange(){
        $res = [];
        $res[] = $this->getPrice();
        $pa = Product_Attributes::getList(['where'=>"product_id = $this->id and name = 'price'"]);
        foreach($pa as $p){
            if($p->value){
                $res[] = $p->value;
            }
        }

//        $sql = "select p.price,p.msrp,pa.value from product p inner join product_attributes pa on pa.product_id = p.id where p.id = {$this->id} and pa.name = 'price'";
//        global $emagid;
//        $db = $emagid->getDb();
//        $result = $db->execute($sql);
//        $filtered = array_filter(flatten($result),function($i){if(floatval($i)){return $i;}});
        return ['min'=>min($res),'max'=>max($res)];
    }
//    public function getPrice($color_id = null)
//    {
//        $queryColorId = $color_id ?: $this->default_color_id;
//        if ($this->msrp != 0) {
//            if (($productAttribute = Product_Attributes::getItem(null, ['where' => "product_id = $this->id and color_id = $queryColorId and name = 'price'"])) && $productAttribute->value > 0) {
//                return $this->price($color_id);
//            } else {
//
//                return $this->msrp($color_id);
//            }
//        } else {
//            return $this->price($color_id);
//        }
//    }
    

    public function isDiscounted($color_id = null){
        $color = $color_id ? : $this->default_color_id;
        if($this->msrp != '' && floatval($this->msrp) > 0){
            return true;
        } else if (Sale::onSale($this->id)) {
            return true;
        } else if(Event::eventSale($this->id,$color)){
            return true;
        } else {
            return false;
        }
    }

    public function getFeaturedImage(){

    }

    public function getAvailability(){
        return self::$availability[$this->availability];
    }

    public function getSubBrands(){
        return Brand::getList(['where'=>"parent_id = $this->brand"]);
    }

    public static function generateToken()
    {
        return md5(uniqid(rand(), true));
    }

    public function featuredImage($category_id = null, $color_id = null){
        if($category_id && ($prodCat = Product_Category::getItem(null,['where'=>['product_id'=>$this->id,'category_id'=>$category_id]])) && $prodCat->image){
            return $prodCat->image;
        } elseif($color_id){
            return ($pr = Product_Image::getItem(null,['where'=>"product_id = {$this->id} and color_id like '%\"$color_id\"%' and (legshot = 0 or legshot is null)", 'orderBy'=>'display_order'])) ? $pr->image : '';
        } elseif($this->featured_image != ''){
            $s3 = new S3();
            return $s3->getUrlByKey($this->featured_image);
        } elseif($product = Product_Image::getItem(null,['where'=>"product_id = {$this->id} and image != ''"])) {
            return $product->image;
        } else {
            return '';
        }
    }
    public function featured1(){
        $s3 = new S3();
        return $this->feature_image_1 ? $s3->getUrlByKey($this->feature_image_1): '';
    }
    public function featured2(){
        $s3 = new S3();
        return $this->feature_image_2 ? $s3->getUrlByKey($this->feature_image_2): '';
    }
    public function getBanner(){
        $product = self::getItem($this->id);
        if($product->banner != ''){
            $s3 = new S3();
            return $s3->getUrlByKey($this->banner);
        }  else {
            return '';
        }
    }


    public function getSpecValue($spec_id = null){
        $prod_spec = Product_Spec::getItem(null,['where'=>"product_id = $this->id and spec_id = $spec_id"]);
        if($prod_spec){
            return $prod_spec->value;
        }else{
            return '-';
        }
    }
    public function getAllProductImages($getDefaultColor = true,$color_id = null){
        $arr = [];
//        if($this->featured_image != ''){
//            $arr[] = $this->featured_image;
//        }
        $where = "product_id = '{$this->id}'";
        if($pro = Product_Image::getList(['where'=>$where, 'orderBy'=>"display_order"])){
            foreach($pro as $item){
                $arr[] = $item->image;
            }
        }
        return $arr;
    }

    public function getLegShot(){
        $where = "product_id = $this->id and legshot = 1";
        if($this->default_color_id){
            $where .= " and color_id like '%$this->default_color_id%'";
        }
        $prodImg = Product_Image::getItem(null,['where'=>$where]);
        if($prodImg){
            return $prodImg->image;
        } else {
            return null;
        }
    }

    public function videoLink()
    {
        if($this->video_link!=''){
            $s3 = new S3();
            return $s3->getUrlByKey($this->video_link);
        }

        return false;
    }

    public function getColors(){
        $list = json_decode($this->color,true);
        $color_sort = Product_Attributes::getList(['where'=>'product_id = '.$this->id.' and name = \'display_order\'']);
        if($color_sort){
            $sql = "select c.* from color c inner join product_attributes pa on pa.color_id = c.id where product_id = {$this->id} and pa.name = 'display_order' and c.active = 1 order by value";
            $colors = Color::getList(['sql'=>$sql]);
            $ar = [];
            foreach($colors as $color){
                if(in_array($color->id,$list)) {
                    $ar[] = $color;
                }
            }
            return $ar;
        } else if($list){
            $imp = implode(',',$list);
            return Color::getList(['where'=>"id in ($imp)", 'orderBy'=>'name']);
        } else {
            return [];
        }
    }

    public function getSizes(){
        $list = json_decode($this->size,true);
        if($list){
            $imp = implode(',',$list);
            return Size::getList(['where'=>"id in ($imp)", 'orderBy'=>"us_size"]);
        } else {
            return [];
        }
    }

    public function getMainCategory()
    {
        $productCats = Product_Category::getList(['where' => ['product_id' => $this->id]]);

        if(!$productCats){
            return null;
        }
        foreach($productCats as $productCat){

            $category = Category::getItem($productCat->category_id);
            if($category && $category->parent_category == 0){
                return $category;
            }
        }

        return null;
    }

    public function getAllCategories($sub_only = false){
        $prodCat = Product_Category::getList(['where'=>'product_id = '.$this->id]);
        $categories = [];
        foreach($prodCat as $item){
            $category = Category::getItem($item->category_id);
            if($sub_only == false || ($sub_only && $category->parent_category != 0)){
                $categories[] = $category;
            }
        }
        return $categories;
    }

    public function relatedProducts($number = 6)
    {
        $count = 1;
        $result = [];
        if(!$this->related_products){
            return null;
        }
        $relatedProductCats = json_decode($this->related_products,true);
        foreach($relatedProductCats as $cat){
            if( $pro = Product::getItem($cat)) {
                $result[] = $pro;
                $count++;
            }
            if($count > $number){
                break;
            }
        }

        return $result;
    }

    public function getLinkedProducts(){
        if($this->linked_list){
            return array_map(function($item){return Product::getItem($item);},json_decode($this->linked_list,true));
        } else {
            return [];
        }
    }

    public static function changeLink($linkedListArray,$product_id){
        $product = self::getItem($product_id);
        $linked = $linkedListArray;
        array_push($linked,$product_id);
        //change product and linked products links
        if($diff = array_diff(json_decode($product->linked_list,true),$linked)) {
            foreach ($diff as $d) {
                $product = self::getItem($d);
                $arr = [];
                foreach ($diff as $dAgain) {
                    if ($dAgain != $d) {
                        $arr[] = $dAgain;
                    }
                }
                $product->linked_list = json_encode($arr);
                $product->save();
            }
        }
        foreach ($linked as $ll) {
            $product = self::getItem($ll);
            $arr = [];
            foreach ($linked as $llAgain) {
                if ($llAgain != $ll) {
                    $arr[] = $llAgain;
                }
            }
            $product->linked_list = json_encode($arr);
            $product->save();
        }
        //change unlinked products links

        //remove a products link to all linked items
        if($linkedListArray == []){
            foreach(json_decode($product->linked_list,true) as $value){
                $prod = self::getItem($value);
                $arr = json_decode($prod->linked_list,true);
                unset($arr[array_search($product_id,$arr)]);
                $prod->linked_list = json_encode($arr);
                $prod->save();
            }
            $product->linked_list = json_encode([]);
            $product->save();
        }
    }

    public static function topSellers($limit = 5){
        global $emagid;
        $db = $emagid->getDb();
        $sql = "select product_id, count(*) from order_products where product_id > 0 group by 1 order by count desc limit $limit;";
        return $db->execute($sql);
    }

    public function isCouponBlocked(){
        $categories = array_map(function($catId){return Category::getItem($catId);},Product_Category::get_product_categories($this->id));
        foreach($categories as $category){
            if($category->coupon_block){
                return true;
            }
        }
        return false;
    }

    public function getAllSpares($limit = 12){
        $arr = Product_Accessory::get_product_accessories($this->id,12);
        if($arr && count($arr)>0){
            $spares = [];
            foreach ($arr as $p){
                $spares[] = self::getItem($p);
            }
        }else{
            $prodCat = Product_Category::get_product_categories($this->id);
            $prodCat = $prodCat[0];
            $pc = Product_Category::getList(['where'=> "category_id = {$prodCat} and active = 1"]);
            $arr = [];
            foreach ($pc as $c){
                if($c->product_id != $this->id){
                    $arr[] = $c->product_id;
                }
            }
            $spares = self::getList(['where'=>"id in (". implode(",",$arr).") and type = '3'","orderBy"=>"spare_display_order","limit"=>$limit]);
        }

        return $spares;

}
    public function getAllProductVideos($getDefaultColor = true,$color_id = null){
        //TODO: to be implemented
        return false;
    }

    public static function getRepairProducts()
    {
        $category = Category::getItem(null, ['where' => "lower(name) = 'repair'"]);
        $cps = Product_Category::getList(['where' => ['category_id' => $category->id]]);
        $data = [];
        foreach($cps as $cp){
            $dp = self::getItem($cp->product_id);
            if($dp != null){
                $data[] = $dp;
            }
        }

        return $data;
    }

    public static function getRepairIds(){
        $category = Category::getItem(null, ['where' => "lower(name) = 'repair'"]);
        $cps = Product_Category::getList(['where' => ['category_id' => $category->id]]);
        $data = [];
        foreach($cps as $cp){
            $data[] = $cp->product_id;
        }
        $ids = '('.implode(',',$data).')';
        return $ids;
    }
}