<?php

namespace Model;

use Carbon\Carbon;
use Emagid\Core\Model;

class Clearance_Details extends Model{
    static $tablename = 'clearance_details';
    static $fields = [
        'clearance_id',
        'size_id',
        'color_id',
        'price'=>['type'=>'numeric'],
        'start_date'=>['required',true],
        'end_date'=>['required',true],
        'starting_quantity',
        'current_quantity'
    ];

    static function getActive(){
        $carbon = Carbon::now();
        return self::getList(['where'=>"start_date < '$carbon' and end_date > '$carbon' and current_quantity > 0"]);
    }

    static function checkClearance($product_id, $color_id, $size_id){
        if(($c = self::getItem(null,['where'=>"product_id = $product_id and color_id like '%\"$color_id\"%' and size_id like '%\"$size_id\"%' and current_quantity > 0 and now() between start_date and end_date"]))){
            return $c;
        } else {
            return null;
        }
    }

    static function checkClearanceV2($product_id,$color_id,$size_id){
        $clearance = Clearance::getItem(null,['where'=>"product_id = $product_id"]);
        if(($c = self::getItem(null,['where'=>"clearance_id = $clearance->id and color_id = $color_id and size_id = $size_id"]))){
            return $c;
        } else {
            return null;
        }
    }
}