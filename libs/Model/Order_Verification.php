<?php

namespace Model;

use Emagid\Core\Model;

//require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class Order_Verification extends Model{
	static $tablename = 'order_verifications';
	static $fields = ["user_identifier",
        "wanted_orders",
        "unwanted_orders",
        "generated_order",
        "combined"];

	public function getWanted(){
	    $wanted = $this->wanted_orders;
	    $wanted = str_replace('{','[',$wanted);
	    $wanted = str_replace('}',']',$wanted);
	    return json_decode($wanted,true);
    }

	public function getUnwanted(){
	    $unwanted = $this->unwanted_orders;
	    $unwanted = str_replace('{','[',$unwanted);
	    $unwanted = str_replace('}',']',$unwanted);
	    return json_decode($unwanted,true);
    }

    public function prepare_for_gen(){
        global $emagid;
        $db = $emagid->getDb();
        $response = $this;
        $id = $this->id;
        if($response == null){
            $n = new \Notification\ErrorHandler('Record does not exist');
            $_SESSION['notification'] = serialize($n);
            return false;
        }

        $goodSQL = "SELECT o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name || ' ' || o.ship_last_name AS name, count(brc.*) AS brown_count, count(grc.*) AS grey_count, count(blc.*) AS black_count FROM public.order o
                    INNER JOIN order_verifications v ON v.id = $id and o.id::text = any(v.wanted_orders)
                    LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.active = 1 AND brc.product_id = 105
                    LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.active = 1 AND grc.product_id = 106
                    LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.active = 1 AND blc.product_id = 107
                    WHERE o.active = 1
                    GROUP BY o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name, o.ship_last_name";
        $badSQL  = "SELECT o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name || ' ' || o.ship_last_name AS name, count(brc.*) AS brown_count, count(grc.*) AS grey_count, count(blc.*) AS black_count FROM public.order o
                    INNER JOIN order_verifications v ON v.id = $id and o.id::text = any(v.unwanted_orders)
                    LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.active = 1 AND brc.product_id = 105
                    LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.active = 1 AND grc.product_id = 106
                    LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.active = 1 AND blc.product_id = 107
                    WHERE o.active = 1
                    GROUP BY o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name, o.ship_last_name";


        $good_orders = [];
        $bad_orders  = [];
        if($response->generated_order){
            $genSQL = "SELECT o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name || ' ' || o.ship_last_name AS name, count(brc.*) AS brown_count, count(grc.*) AS grey_count, count(blc.*) AS black_count FROM public.order o
                    INNER JOIN order_verifications v ON v.id = $id and o.id = v.generated_order
                    LEFT JOIN order_products brc ON o.id = brc.order_id AND brc.active = 1 AND brc.product_id = 105
                    LEFT JOIN order_products grc ON o.id = grc.order_id AND grc.active = 1 AND grc.product_id = 106
                    LEFT JOIN order_products blc ON o.id = blc.order_id AND blc.active = 1 AND blc.product_id = 107
                    WHERE o.active = 1
                    GROUP BY o.id, o.status, o.fulfillment_status, o.email, o.phone, o.total,o.ship_first_name, o.ship_last_name";
            $this->_viewData->generated_order = $db->getResults($genSQL)[0];
        }

        $good_totals = 0;
        $good_results = $db->getResults($goodSQL);
        foreach ($good_results as $row){
            $oid = $row['id'];
            $total = 0;
            $transactions = [];
            $authNetDetails = new \AuthorizeNetTD();
            $_transactions = \Model\Transaction::getList(['where'=>"order_id = $oid AND ref_tran_id IS NOT NULL AND ref_tran_id != 0"]);
            foreach($_transactions AS $localTransaction){
                $transaction = $authNetDetails->getTransactionDetails($localTransaction->ref_tran_id);
                $trans = $transaction->xml;
                $trow = ['id'=>$localTransaction->ref_tran_id,'order_id'=>$row['id'],'type'=>$localTransaction->type, 'amount'=>$localTransaction->amount, 'status'=>$trans->transaction->transactionStatus];
                $toadd = $localTransaction->amount;
                if($trans->transactionType == 'refundTransaction' ){
                    $toadd *= -1;
                }
                if(in_array($trans->transaction->transactionStatus,['authorizedPendingCapture','capturedPendingSettlement','refundPendingSettlement','settledSuccessfully']) === false ){
                    $toadd = 0;
                }
                $total = $total + $toadd;
                $transactions[] = $trow;
            }
            $good_totals = $good_totals + $total;
            $row['transactions'] = $transactions;
            $good_orders[$row['id']] = $row;
        }

        $bad_totals = 0;
        $bad_results = $db->getResults($badSQL);
        foreach ($bad_results as $row){
            $oid = $row['id'];
            $total = 0;
            $transactions = [];
            $authNetDetails = new \AuthorizeNetTD();
            $_transactions = \Model\Transaction::getList(['where'=>"order_id = $oid AND ref_tran_id IS NOT NULL AND ref_tran_id != 0"]);
            foreach($_transactions AS $localTransaction){
                $transaction = $authNetDetails->getTransactionDetails($localTransaction->ref_tran_id);
                $trans = $transaction->xml;
                $trow = ['id'=>$localTransaction->ref_tran_id,'order_id'=>$row['id'],'type'=>$localTransaction->type, 'amount'=>$localTransaction->amount, 'status'=>$trans->transaction->transactionStatus];
                $toadd = $localTransaction->amount;
                if($trans->transactionType == 'refundTransaction' ){
                    $toadd *= -1;
                }
                if(in_array($trans->transaction->transactionStatus,['authorizedPendingCapture','capturedPendingSettlement','refundPendingSettlement','settledSuccessfully']) === false ){
                    $toadd = 0;
                }
                $total = $total + $toadd;
                $transactions[] = $trow;
            }
            $bad_totals = $bad_totals + $total;
            $row['transactions'] = $transactions;
            $bad_orders[$row['id']] = $row;
        }

        $response->good_totals = $good_totals;
        $response->bad_totals = $bad_totals;

        return [$good_totals,$bad_totals];
    }

    public function generate_order($paid_amt,$refund_amt){
        $message = '';
        $totalPaid = $paid_amt+$refund_amt;

        global $emagid;
	    $db = $emagid->getDb();
	    $difference = 0;
	    $goodOrders  = $db->getResults("SELECT * from  PUBLIC.order WHERE active = 1 and COALESCE(tracking_number,'') = '' and status in ('Active','Updated','Attn Needed', 'Possible Duplicate') and id::VARCHAR IN(SELECT unnest(wanted_orders) FROM order_verifications where id = $this->id and active = 1 and coalesce(generated_order,0) = 0 )");
	    if(count($goodOrders) == 1) {
	        $this->generated_order = $goodOrders[0]['id'];
	        $sOrder = Order::getItem($goodOrders[0]['id']);
	        $sOrder->status = 'Active';
	        $sOrder->fulfillment_status = 'Ready';

	        $difference = $sOrder->difference;

            $sNote = new Note_Log();
            $sNote->order_id = $sOrder->id;
            $sNote->admin_id = 1;
            $sNote->note = "Order kept through email verification";
            $sNote->save();
            if($sOrder->email != $this->user_identifier){
                $sNote->id = 0;
                $sNote->note = "Order email changed to known good email";
                $sNote->save();
            }
            $sOrder->email = $this->user_identifier;
            $sOrder->save();

            $message .= "Order #$sOrder->id kept and marked Ready"."<br/>";

	        $this->save();
            echo "One good order: $this->generated_order"."<br />";
        } else if(count($goodOrders) > 1){
	        foreach ($goodOrders as $i => $pgorder){
	            unset($pgorder['insert_time']);
                if($pgorder['add_updated'] == 1 || ($pgorder['ship_address2'] != '')){
                    $newOrder = new Order($goodOrders[$i]);
                    break;
                }
                $newOrder = new Order($goodOrders[$i]);
            }
            $newOrder->id = 0;
            $newOrder->total = $paid_amt;
            $newOrder->email = $this->user_identifier;
            $newOrder->generated = 1;
            if(!$newOrder->save()){
                return false;
            } else {
                $this->generated_order = $newOrder->id;
                $this->save();
                $db->execute("INSERT INTO order_products (product_id, order_id, quantity, unit_price, details, clearance, status ) SELECT product_id, $newOrder->id, quantity, unit_price, details, clearance, status FROM order_products where active = 1 AND order_id::VARCHAR in (select unnest(wanted_orders) from order_verifications where id = $this->id);");
                $db->execute("UPDATE order_products set active = 0 WHERE order_id = $newOrder->id AND id IN (SELECT max(id) from order_products where active = 1 and order_id = $newOrder->id group by product_id having count(*) > 1)");
                $db->execute("INSERT INTO public.transaction (order_id, authorize_net_data, ref_tran_id, type, amount, cvv_result_code, avs_result_code, import, notes, raw) SELECT $newOrder->id, authorize_net_data, ref_tran_id, type, amount, cvv_result_code, avs_result_code, import, notes, raw FROM public.transaction WHERE active = 1 AND order_id::VARCHAR in (select unnest(wanted_orders) from order_verifications where id = $this->id);");
                $db->execute("INSERT INTO note_log (order_id, note, admin_id) SELECT $newOrder->id, note, 0 FROM note_log WHERE active = 1 AND order_id::VARCHAR in (select unnest(wanted_orders) from order_verifications where id = $this->id);");
                foreach($goodOrders as $mo){
//                echo "update public.order set status = 'Merged' WHERE id = {$mo['id']}"."<br />";
                    $mid = $mo['id'];
                    $db->execute("update public.order set status = 'Merged',fulfillment_status = '' WHERE id = {$mo['id']};");
                    echo("UPDATE public.order SET status = '{$mo['status']}' WHERE id = {$mo['id']};"."<br />");
                }
                $bd = $newOrder->calculate_total();
                foreach($bd['updates'] as $field => $value){
                    $newOrder->$field = $value;
                }
                $newOrder->difference = $difference = round(array_sum($bd['charges']) - $paid_amt,2);
                $newOrder->save();
                if($difference < 0) { //Overpaid
                    $tranResults = $db->getResults("SELECT ref_tran_id, order_id, amount from public.transaction WHERE order_id = {$newOrder->id} AND type IN ('','AUTH_CAPTURE')");
                    foreach ($tranResults as $transaction){
                        $transaction = (Object)$transaction;
                        $tranid = $transaction->ref_tran_id;
                        $amount = abs($difference);

                        $authTransaction = new \AuthorizeNetAIM();
                        $authNetDetails = new \AuthorizeNetTD();
                        $transaction = $authNetDetails->getTransactionDetails($tranid);
                        $trans = $transaction->xml;
                        $cc_number = $trans->transaction->payment->creditCard->cardNumber;
                        $note_text = "Refunding ${$amount} on Order #{$newOrder->id}. transaction: {$tranid} <br/>";
                        $message .= $note_text;

                        echo($note_text);

                        $note = new Note_Log();
                        $note->admin_id = 0;
                        $note->order_id = $newOrder->id;
                        $note->note = $note_text;
                        $note->save();

                        $note = new Note_Log();
                        $note->admin_id = 0;
                        $note->order_id = $newOrder->id;

                        $authTransaction->setCustomFields(['x_invoice_num' => $newOrder->id,'x_duplicate_window'=>0]);
                        $response = $authTransaction->credit($tranid,$amount,substr($cc_number,-4));
                        if($response->approved){
                            $refund_amt += $amount;
                            $newOrder->total -= $amount;
                            $newOrder->difference -= $amount;
                            $newOrder->fulfillment_status = 'Ready';
                            $newOrder->save();

                            $difference = 0;
                            $note->note = "Refund successful <br />";
                            $message .= $note->note;
                            $note->save();
                            var_dump($note);

                            $localTransaction = new \Model\Transaction();
                            $localTransaction->order_id = $newOrder->id;
                            $localTransaction->ref_tran_id = $response->transaction_id;
                            $localTransaction->amount = $response->amount;
                            $localTransaction->avs_result_code = $response->avs_response;
                            $localTransaction->cvv_result_code = $response->cavv_response;
                            $localTransaction->type = "REFUND";
                            $localTransaction->save();
                            break;
                        } else {
                            $note->note = "Refund unsuccessful: {$response->response_reason_text}<br />";
                            $message .= $note->note;
                            $note->save();
                            var_dump($note);
                        }

                    }
                } else if($difference > 0){ //underpaid

                }
                echo("DELETE FROM public.order WHERE id = $newOrder->id;"."<br />");
                echo("DELETE FROM order_products WHERE order_id = $newOrder->id;"."<br />");
                echo("DELETE FROM public.transaction WHERE order_id = $newOrder->id;"."<br />");
                echo("DELETE FROM note_log WHERE order_id = $newOrder->id"."<br />;");
                echo("UPDATE order_verifications SET generated_order = NULL WHERE id = $this->id;"."<br />");
            }
        } else {

        }

        $badOrders  = $db->getResults("SELECT * from PUBLIC.order WHERE active = 1 and status in ('Active','Updated','Attn Needed', 'Possible Duplicate') and id::VARCHAR IN(SELECT unnest(unwanted_orders) FROM order_verifications where id = $this->id and active = 1 )");
	    foreach ($badOrders as $order){
            $bOrder = Order::getItem($order['id']);
            $tranResults = $db->getResults("SELECT ref_tran_id, order_id, amount from public.transaction WHERE order_id = {$order['id']} AND type IN ('','AUTH_CAPTURE')");
            foreach ($tranResults as $transaction){
                $transaction = (Object)$transaction;
                $tranid = $transaction->ref_tran_id;
                $amount = $transaction->amount;
                $order_id = $transaction->order_id;

                if($difference > 0 ){
                    $amount = $amount-$difference;
                    $dnote = new Note_Log();
                    $dnote->admin_id = 1;
                    $dnote->order_id = $order_id;
                    if($amount < 0){
                        $difference = $difference - $amount;
                        $refund_amt -= $amount;
                        $dnote->note = "Refund not granted on Order #$order_id, transaction #$tranid to make up for balance of generated order #$this->generated_order";
                        $message .= $dnote->note;
                        $dnote->save();
                        continue;
                    } else {
                        $refund_amt -= $difference;
                        $difference = 0;
                        $dnote->note = "Partial Refund of $".number_format($amount,2)." granted on Order #$order_id, transaction #$tranid to make up for balance of generated order #$this->generated_order";
                        $message .= $dnote->note;
                        $dnote->save();
                    }
                }

                $authTransaction = new \AuthorizeNetAIM();
                $authNetDetails = new \AuthorizeNetTD();
                $transaction = $authNetDetails->getTransactionDetails($tranid);
                $trans = $transaction->xml;
                $cc_number = $trans->transaction->payment->creditCard->cardNumber;
                $note_text = "Refunding ${$amount} on Order #{$order_id}. transaction: {$tranid}"."<br/>";

                $message .= $note_text;

                echo($note_text);

                $note = new Note_Log();
                $note->admin_id = 0;
                $note->order_id = $order_id;
                $note->note = $note_text;
                $note->save();

                $note = new Note_Log();
                $note->admin_id = 0;
                $note->order_id = $order_id;

                $authTransaction->setCustomFields(['x_invoice_num' => $order_id,'x_duplicate_window'=>0]);
                $response = $authTransaction->credit($tranid,$amount,substr($cc_number,-4));
                if($response->approved){
                    $bOrder->total -= $amount;
                    $bOrder->difference -= $amount;
                    $bOrder->status = 'Canceled';
                    $bOrder->save();

                    $note->note = "Refund successful"."<br />";
                    $message .= $note->note;

                    $note->save();

                    $localTransaction = new \Model\Transaction();
                    $localTransaction->order_id = $order_id;
                    $localTransaction->ref_tran_id = $response->transaction_id;
                    $localTransaction->amount = $response->amount;
                    $localTransaction->avs_result_code = $response->avs_response;
                    $localTransaction->cvv_result_code = $response->cavv_response;
                    $localTransaction->type = "REFUND";
                    $localTransaction->save();
                } else {
                    $note->note = "Refund unsuccessful: {$response->response_reason_text}"."<br />";
                    $message .= $note->note;
                    echo("Refund unsuccessful: {$response->response_reason_text}"."<br />");
                    $note->save();
                }
            }
            echo ("Order #$bOrder->id changed to 'Canceled'"."<br />");
            $message .= "Order #$bOrder->id changed to 'Canceled'"."<br />";
            if($bOrder->total == 0){
                $bOrder->status = 'Refunded';
                $bOrder->save();
                echo ("Order #$bOrder->id changed to 'Refunded'"."<br />");
                $message .= "Order #$bOrder->id changed to 'Refunded'"."<br />";
            }
        }


        $this->combined = 1;
	    $this->save();
	    if($this->generated_order){
	        if($this->sendEmail($refund_amt,$difference,$totalPaid)){
	            $message .= "Email sent to user $this->user_identifier";
            } else {
                $message .= "Email failed to send to user $this->user_identifier";
            }
        }

        $n = new \Notification\MessageHandler($message);
        $_SESSION['notification'] = serialize($n);
    }

    public function sendEmail($refund_amt,$balance,$totalPaid){ //-balance is for them, + balance is for us

        $totalPaid =    number_format($totalPaid,2);
        $balance =      number_format($balance,2);
        $refund_amt =   number_format($refund_amt,2);
        if($balance < 0){
            $balance = '0.00';
        } else if ($balance > 0 ){

        }


        $order = Order::getItem($this->generated_order);
        if($order->difference > 0){
            $order->fulfillment_status = '';
            return false;
        }
        $html = '';
        foreach ($order->getOrderProducts() as $orderProduct){
            $product = \Model\Product::getItem($orderProduct->product_id);
            $img = "https://kawsone.com" . UPLOAD_URL . 'products/' . $product->featuredImage();
            if(strpos($img,"amazonaws") !== false) $img = $product->featuredImage();
            $name = $product->name;
            $price = $product->price;
            $html .=
                '<tr style="height:75.0pt">
                        <td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$img.'"></p>
                        </td>
                        <td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$name.'</span></p>
                        
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$orderProduct->quantity.'</span></p>
                        
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($price * $orderProduct->quantity,2).'</span></p>
                        </td>
                    </tr>';
        }

        $email = new \Email\MailMaster();
        $mergeFields = [
            'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
            'ORDER_NUMBER' => $order->id,
            'SHIPPING' => $order->getShippingAddr(),
            'BILLING' => $order->getBillingAddr(),
            'ITEMS' => $html,
            'SUBTOTAL' => number_format($order->subtotal, 2),
            'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
            'TAXFEE' => number_format(($order->ship_country=='United States'||$order->ship_country=='US')?$order->tax:$order->duty, 2),
            'TAXNAME' => ($order->ship_country=='United States'||$order->ship_country=='US')?'Tax':'Duty & Taxes',
            'TOTAL' => number_format($order->total, 2),
            'AMOUNT_PAID'   => $totalPaid,
            'BALANCE_DUE'   => $balance,
            'REFUND_AMOUNT' => $refund_amt
        ];
//        $order->email = 'Garrett@emagid.com';
        $email->setTo(['email' => $order->email, 'name' => $order->shipName(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('generated-order-confirmation');
        $return = true;
        try {
            $emailResp = $email->send();
        } catch(Mandrill_Error $e){
            $return = false;
        }
        var_dump($emailResp);
        return $return;
    }
}