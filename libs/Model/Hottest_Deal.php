<?php

namespace Model;

class Hottest_Deal extends \Emagid\Core\Model{

    static $tablename = 'hottest_deal';

    static $fields = [
        'product_id',
        'title',
        'subtitle',
        'discount',
        'status',
        'original_price',
        'start_date',
        'end_date'
    ];

    public function getProduct($product_id = null){
        if(is_null($product_id)) {
            return Product::getItem(null, ['where' => "id = $this->product_id"]);
        } else {
            return Product::getItem(null, ['where' => "id = $product_id"]);
        }
    }

    public static function getActiveDeals(){
        $now = date('Y-m-d h:i:s',getdate()[0]);
        $where = "active = 1 AND status = 1";

        $hottestDeal = [];
        $activeDeals = Hottest_Deal::getList(['where'=>"active = 1 AND status = 1"]);
        if($activeDeals != 0 && $activeDeals != null){
            foreach($activeDeals as $activeDeal){
                if(($activeDeal->start_date <= $now && $activeDeal->end_date >= $now) ||
                    (is_null($activeDeal->start_date) && $activeDeal->end_date >= $now) ||
                    ($activeDeal->start_date <= $now && is_null($activeDeal->end_date)) ||
                    (is_null($activeDeal->start_date) && is_null($activeDeal->end_date))){
                    $hottestDeal[] = $activeDeal;
                }
            }
        }
        return $hottestDeal;
    }
}