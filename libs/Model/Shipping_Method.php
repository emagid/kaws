<?php
namespace Model;

class Shipping_Method extends \Emagid\Core\Model {
	static $tablename = "shipping_method";

	public static $fields = [
		'is_default'=>['type'=>'boolean'],
		'name'=>['required'=>true],
		'cart_subtotal_range_min',
		'cost'=>['type'=>'integer'],
		'international',
		'country',
        'price',
        'addon',
        'disbursement',
        'duty'
	];

	static function clearDefault($except = 0){
		self::execute('update '.self::$tablename.' set is_default = false where id <> '.$except);
	}

	public function getCost(){
		return json_decode($this->cost,true)[0];
	}
}