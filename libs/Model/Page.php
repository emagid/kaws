<?php 

namespace Model; 

class Page extends \Emagid\Core\Model {

	static $tablename = 'page'; 
	
	public static $fields =  [
		'title'=>['required'=>true], 
		'slug'=>['required'=>true,'unique'=>true],
		'description', 
		'featured_image',
		'meta_title', 
		'meta_keywords', 
		'meta_description'
	];
	
}