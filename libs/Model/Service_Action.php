<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 3/21/17
 * Time: 3:01 PM
 */

namespace Model;

class Service_Action extends \Emagid\Core\Model {
    static $tablename = "service_action";
    public static $fields = [
        'insert_time',
        'name'
    ];
}