<?php

namespace Model;

use Emagid\Core\Model;

class Note_Log extends Model{
    static $tablename = 'note_log';
    static $fields = [
        'insert_time',
        'order_id',
        'note',
        'admin_id'
    ];
}