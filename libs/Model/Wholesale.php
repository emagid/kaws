<?php

namespace Model;

use Emagid\Core\Model;

class Wholesale extends Model{
	static $tablename = 'wholesale';
	static $fields = [
		"name",
		"status",
		"email"=>['unique'=>true],
		"phone",
		"address",
		"address2",
		"city",
		"state",
		"zip",
		'country',
		'image',
		'payment_type', /** CC ONLY */
		'terms'
	];

	static $status = ['Pending','Accepted','Rejected'];
	static $payment_type = ['Pre-Auth','Purchase'];
	static $terms = ['Credit Card','Check','Bank Wire','Cash'];

	public function getItemList(){
		return Wholesale_Item::getList(['where'=>"wholesale_id = $this->id"]);
	}

	public function isSelectedProduct($product_id){
        return Wholesale_Item::getItem(null, ['where' => "wholesale_id = $this->id and product_id = $product_id"]) ? true : false;
    }
}