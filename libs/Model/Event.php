<?php

namespace Model;

use Carbon\Carbon;
use Emagid\Core\Model;

class Event extends Model{
    static $tablename = 'event';
    static $fields = [
        'title',
        'icon',
        'discount_amount',
        'discount_type',
        'start_date',
        'end_date'
    ];
    static $discount_type = [1=>'%', 2=>'-'];

    public function getIcon(){
        return UPLOAD_URL.'events/'.$this->icon;
    }

    public function calculateEvent($price){
        if($this->validateEvent()){
            if($this->discount_type == 1){
                return $price * (1 - $this->discount_amount/100);
            } else if($this->discount_type == 2){
                return $price - $this->discount_amount;
            } else {
                return $price;
            }
        } else {
            return false;
        }
        //Percent
    }

    public function validateEvent(){
        $start = Carbon::createFromTimestamp(strtotime($this->start_date));
        $end = Carbon::createFromTimestamp(strtotime($this->end_date));
        return Carbon::now()->between($start,$end);
    }

    public static function eventSale($product_id,$color_id){
        if(($product_attribute = Product_Attributes::getEventById($product_id,$color_id)) && $product_attribute->value > 0){
            $event = Event::getItem($product_attribute->value);
            return $event && $event->validateEvent();
        } else {
            return false;
        }
    }
}