<?php 

namespace Model; 

class Location extends \Emagid\Core\Model {

	static $tablename = 'location';
	
	public static $fields =  [
		'name'=>['required'=>true],
		'insert_time'
	];
	
}