<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 3/21/17
 * Time: 3:01 PM
 */

namespace Model;

class Shipstation_Log extends \Emagid\Core\Model
{
    static $tablename = "shipstation_log";
    public static $fields = [
        'id',
        'active',
        'insert_time',
        'shipstation_id',
        'log'
    ];

    public static function log($shipId, $log)
    {
        $model = new self();
        $model->shipstation_id = $shipId;
        $model->log = $log;
        return $model->save();
    }
}