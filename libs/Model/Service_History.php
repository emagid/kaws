<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 3/21/17
 * Time: 3:01 PM
 */

namespace Model;

class Service_History extends \Emagid\Core\Model {
    static $tablename = "service_history";
    public static $fields = [
        'id',
        'active',
        'insert_time',
        'service_ticket_id',
        'service_status_id',
        'notify',
        'comment'
    ];

    public function getStatusName()
    {
        $status = Service_Status::getItem($this->service_status_id);
        return $status->name;
    }
}