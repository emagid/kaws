<?php

namespace Model;

use Carbon\Carbon;
use Emagid\Core\Model;

class Sale extends Model{
    static $tablename = 'sales';
    static $fields = [
        'title',
        'start_date',
        'end_date',
        'discount_amount',
        'discount_type',
        'sale_type',
        'sale_options'
    ];
    static $typeSymbol = [1=>'%',2=>'-'];
    static $buyType = [1=>'Dollars',2=>'Items'];
    static $saleType = [1=>'Site Wide',2=>'Category',3=>'Buy X, Get Y'];

    public function getDiscountSymbol(){
        return self::$typeSymbol[$this->discount_type];
    }

    /**
     * @param $numericValue float
     * @param $productId int
     * @return mixed float
     */
    public function calculateSale($numericValue, $productId){
        if($this->sale_type == 1) {
            if ($this->discount_type == 1) {
                return $numericValue * (1-($this->discount_amount / 100));
            } else {
                return $numericValue - $this->discount_amount;
            }
        } else if($this->sale_type == 2){
            $sale_options = json_decode($this->sale_options,true);
            $prodList = reset($sale_options);
            if(in_array($productId,$prodList)){
                if ($this->discount_type == 1) {
                    return $numericValue * (1-($this->discount_amount / 100));
                } else {
                    return $numericValue - $this->discount_amount;
                }
            } else {
                return $numericValue;
            }
        } else {
            return $numericValue;
        }
    }

    /**
     * If the product does not exist, then return false;
     * If the product does exist, check for site wide sale (sale_type == 1), return true;
     * Else if not site wide, check category sale (sale_type == 2, product selected in array), return true;
     * Else, product is not on sale, return false;
     *
     * @param $productId int
     * @return bool
     */
    public static function onSale($productId){
        $carbon = Carbon::now();
        $sales = self::getList(['where'=>"'$carbon' between start_date::timestamp and end_date::timestamp"]);
        if($product = Product::getItem($productId) && $sales){
            foreach($sales as $sale){
                if($sale_opt = json_decode($sale->sale_options,true)){
                    $arr = reset($sale_opt);
                } else {
                    $arr = [];
                }
                if($sale->sale_type == 1){
                    return true;
                } else if($sale->sale_type == 2 && in_array($productId,$arr)){
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }


    //Hardcoded logic for December 2016 Holiday Tier system
    public static function tiers($total){
        $start = Carbon::createFromDate(2016,12,25);
        $end = Carbon::createFromDate(2016,12,26)->hour(23)->minute(59)->second(59);
        $now = Carbon::now();
        if($start->lte($now) && $end->gt($now)){
            switch($total){
                case $total >= 298 && $total < 398:
                    return 25;
                    break;
                case $total >= 398 && $total < 598:
                    return 50;
                    break;
                case $total >= 598:
                    return 100;
                    break;
                default:
                    return 0;
            }
        }
        else {
            return 0;
        }
    }
}