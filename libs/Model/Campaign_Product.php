<?php
namespace Model;

class Campaign_Product extends \Emagid\Core\Model {
 
    static $tablename = "campaign_product";

    public static $fields = [
  		'product_id',
  		'campaign_id',
  		'quantity',
        'max_per_user',
        'notify_level',
        'winner'
    ];
    
    public static function deleteProductById($camId, $pId){
        $cp = self::getItem(null, ['where' => ['product_id' => $pId, 'campaign_id' => $camId]]);
        if($cp){
            self::delete($cp->id);    
        }
        
    }
}