<?php

namespace Model;

use Emagid\Core\Model;

class Product_Inventory extends Model{
	static $tablename = 'product_inventory';
	static $fields = ["inventory_id",
                      "product_id",
                      "quantity"];
}