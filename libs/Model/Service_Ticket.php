<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 3/21/17
 * Time: 3:01 PM
 */

namespace Model;

class Service_Ticket extends \Emagid\Core\Model {
    static $tablename = "service_ticket";
    public static $fields = [
        'id',
        'insert_time',
        'modified_time',
        'viewed',
        'firstname',
        'lastname',
        'email',
        'phone',
        'product',
        'model',
        'quantity',
        'service_action_id',
        'service_status_id',
        'comment',
        'product_model',
        'assigned_manager',
        'invoice_number',
        'estimate_details',
        'discount',
        'shipping',
        'user_id'
    ];

    public function getStatus()
    {
        return Service_Status::getItem($this->service_status_id);
    }

    public function getStatusName()
    {
        $status = $this->service_status_id?Service_Status::getItem($this->service_status_id):null;
        return $status?$status->name:'-';
    }

    public function getHistories()
    {
        return Service_History::getList(['where' => ['service_ticket_id' => $this->id]]);
    }

    public function getEstimateData()
    {
        return json_decode($this->estimate_details, true);
    }

    public function getAssignedManger()
    {
        if(!$this->assigned_manager) return null;
        $admin = Admin::getItem($this->assigned_manager);
        return $admin;
    }

    public function getAssignedManagerName()
    {
        if($manager = $this->getAssignedManger()){
            return $manager->first_name . ' ' . $manager->last_name;
        } else {
            return '-';
        }
    }

    public function getEstimateHistoryData()
    {
        $estimateStatusId = Service_Status::getEstimateStatus()->id;
        $histories = Service_History::getList(['where' => ['service_ticket_id' => $this->id, 'service_status_id' => $estimateStatusId]]);

        $data = [];
        foreach($histories as $history){
            $data[] = json_decode($history->comment, true);
        }
        $data = array_unique($data,SORT_REGULAR);

        return $data;
    }

    public function getTechnicians()
    {
        return Service_Technician::getList(['where' => ['service_ticket_id' => $this->id]]);
    }

    public function createLog($action,$manager = null)
    {
        if(in_array(strtoupper($action), Service_Log::$ACTION)){
            $action = strtoupper($action);
            $log = new Service_Log();
            $log->active = 1;
            $log->insert_time = date('Y-m-d G:i:s');
            $log->service_ticket_id = $this->id;
            if($manager != null){
                $log->action = strtoupper($manager)." ".$action;
            } else {
                $log->action = $action;
            }
            $log->save();
            return $log;
        }
    }
}