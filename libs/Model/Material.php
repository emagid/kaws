<?php

namespace Model;

use Emagid\Core\Model;

class Material extends Model{
    static $tablename = 'material';
    static $fields = [
        'name',
        'slug',
        'swatch'
    ];

    public function name(){
        return $this->name;
    }

    public function swatch(){
        return $this->swatch ?
            '/content/uploads/materials/' . $this->swatch :
            FRONT_IMG . 'color_swatch_' . strtolower(str_replace(" ", "_", $this->name)) . '.png';
    }
}