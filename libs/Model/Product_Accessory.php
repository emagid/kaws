<?php

namespace Model;

class Product_Accessory extends \Emagid\Core\Model {
  
  static $tablename = "product_accessory";
  
  public static $fields = [
      'product_id',
      'accessory_product_id',
      'display_order'

  ];
  
  static $relationships = [
      [
          'name'=>'product',
          'class_name' => '\Model\Product',
          'local'=>'product_id',
          'remote'=>'id',
          'relationship_type' => 'one'
      ],
      [
          'name'=>'accessory_product',
          'class_name' => '\Model\Product',
          'local'=>'accessory_product_id',
          'remote'=>'id',
          'relationship_type' => 'one'
      ],
  ];

    public static function get_product_accessories($product_id, $limit = "") {
        $pc = self::getList(['where'=>"product_id={$product_id} AND active=1", 'limit'=>$limit,'orderBy'=>'display_order']);
        $arr = [];
        foreach($pc as $c) {
            $arr[] = $c->accessory_product_id;
        }
        return $arr;
    }


}
