<?php

namespace Model;

use Emagid\Core\Model;

class Product_Spec extends Model{
    static $tablename = 'product_spec';
    static $fields = [
        'product_id',
        'spec_id',
        'value',
        'display_order',
        'status'
    ];
    static $status = [1=>'Active',2=>'Hidden'];
    /**
     * $name_value_map must contain $key=>$value pairing that map to name and value fields respectively
    */

    public static function addAttribute($product_id, $spec_id, $name_value_map = []){
        $pSpec = self::getItem(null,['where'=>"product_id = $product_id and spec_id = $spec_id"])?:new self();
        $pSpec->product_id = $product_id;
        $pSpec->spec_id = $spec_id;
        foreach ($name_value_map as $name=>$value){
            $pSpec->$name = $value;
            $pSpec->save();
        }
        return false;
    }

    public static function getSpecs($product_id = null,$comparison = false){
        $sql = "select * from product_spec where product_id = $product_id and active = 1";
        $nestedProdSpecs = [];
        if($comparison){
            $sql .= " and status = 1";
        }
        $sql .= " order by display_order";
        $pSpList = self::getList(['sql'=>$sql]);
        $specList = array_map(function ($sp){
            $r = Spec::getItem($sp->spec_id);
            $r->value = $sp->value;
            return $r;
        },$pSpList);
        foreach($specList as $spec){
            $category = $spec->getCategory()->name;
            $nestedProdSpecs[$category][] = $spec;
        }
        return $nestedProdSpecs;

    }


    public static function getMaterialList($product_id = null){
        $sql = "SELECT DISTINCT ON (variation_id) * FROM product_attributes WHERE active = 1";
        if($product_id){
            $sql .= " and product_id = $product_id";
        }
        $paList = self::getList(['sql'=>$sql]);
        if($paList){
            $arr = array_map(function($item){
                return $item->variation_id;
            },$paList);
            $arrList = implode(',',$arr);
            $varList = Variation::getList(['where'=>"id in ($arrList)"]);
            $matList = [];
            $taken = [];
            foreach($varList as $var) {
                $matSlug = explode('_', $var->name)[0];
                $slug = Variation::extrapolateSlug($matSlug);
                if (in_array($slug, $taken)) {
                } else {
                    $matList[] = Material::getItem(null, ['where' => "slug = '$slug'"]);
                    $taken[] = $slug;
                }
            }
            return $matList;
        } else {
            return null;
        }
    }

    public static function getSoleList($product_id, $material){
        if($material == 0 || $material == '' || is_null($material)){
            $product = Product::getItem($product_id);
            if($product->sole){
                $soleImp = implode(',',json_decode($product->sole,true));
                $color = Sole::getList(['where'=>"id in ({$soleImp})", 'orderBy'=>"name"]);
                return $color;
            } else{
                return null;
            }
        }
        $material = is_numeric($material) ? Material::getItem($material)->slug: $material;
        $sql = "select * from variation where id in (select distinct on (variation_id) variation_id from product_attributes where product_id = $product_id) and name like '%material:$material%'";
        $variation = Variation::getList(['sql'=>$sql]);
        if($variation){
            $arr = array_map(function($variation){
                $name = explode('_',$variation->name);
                $slug = Variation::extrapolateSlug($name[2]);
                return $slug;
            },$variation);
            $soleSlug = implode("','",$arr);
            return Sole::getList(['where'=>"slug in ('$soleSlug')", 'orderBy'=>"name"]);
        } else {
            return [];
        }
    }

    //Defunct function, material argument phased out. Equivalent to Product::getColors()
    public static function getColorList($product_id, $material){
        if($material == 0 || $material == '' || is_null($material)){
            $product = Product::getItem($product_id);
            if($product->color){
                $colorImplode = implode(',',json_decode($product->color,true));
                $color = Color::getList(['where'=>"id in ({$colorImplode})", 'orderBy'=>"name"]);
                return $color;
            } else{
                return [];
            }
        }
        $material = is_numeric($material) ? Material::getItem($material)->slug: $material;
        $sql = "select * from variation where id in (select distinct on (variation_id) variation_id from product_attributes where product_id = $product_id and active = 1) and name like '%material:$material%'";
        $variation = Variation::getList(['sql'=>$sql]);
        if($variation){
            $arr = array_map(function($variation){
                $name = explode('_',$variation->name);
                $slug = Variation::extrapolateSlug($name[1]);
                return $slug;
            },$variation);
            $colorSlugs = implode("','",$arr);
            return Color::getList(['where'=>"slug in ('$colorSlugs')", 'orderBy'=>"name"]);
        } else {
            return [];
        }
    }

    public function getImage(){
        return $this->name == 'image' ? UPLOAD_URL.'products/'.$this->value:null;
    }

    public function isLegshot(){
        return self::getItem(null,['where'=>"product_id = $this->product_id and variation_id = $this->variation_id and value = '$this->value' and name = 'legshot'"]);
    }

    public static function getEventById ($product_id, $color_id){
        return self::getItem(null,['where'=>"product_id = {$product_id} and color_id = $color_id and name = 'event'"]);
    }

    public static function getStatusById ($product_id, $color_id){
        return self::getItem(null,['where'=>"product_id = {$product_id} and color_id = $color_id and name = 'status'"]);
    }

    public static function searchBy($product_id,$color_id,$name,$value=null){
        $where = ['product_id'=>$product_id,'color_id'=>$color_id,'name'=>$name];
        if($value){
            $where['value'] = $value;
        }
        return self::getItem(null,['where'=>$where]);
    }

}