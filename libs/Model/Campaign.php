<?php
namespace Model;

use Carbon\Carbon;

class Campaign extends \Emagid\Core\Model {
 
    static $tablename = "campaign";

    public static $status = ['Active','Inactive','Over', 'Pending', 'Sold Out'];
    public static $type = ['Standard', 'Lottery', 'Auction'];

    public static $fields = [
  		'name' => ['required'=>true],
  		'type',
  		'terms',
        'start_time',
        'end_time',
  		'status',
        'c_limit'=>['type'=>'numeric']
    ];

    public function getProducts(){
        $sql = "select p.* from public.campaign_product as cp join public.product as p on p.id=cp.product_id where cp.campaign_id={$this->id} and cp.active = 1 and p.active = 1 order by p.name";
        return Product::getList(['sql' => $sql]);
    }

    public function getProductsCount(){
        $counts = [];
        foreach ($this->getProducts() AS $product){
            $countSQL = "SELECT SUM(op.quantity) FROM order_products op,public.order o WHERE op.order_id = o.id AND 1 = ALL(ARRAY[o.active,op.active]) AND o.status NOT IN ('Declined','Canceled','Refunded','Incomplete PayPal','Incomplete Amazon','Banned','Limit Reached') AND o.campaign_id = $this->id AND op.product_id = $product->id";
            global $emagid;
            $db = $emagid->getDb();
            $results = $db->getResults($countSQL);
            $count = $results[0]['sum'];
            $product->purchased_count = $count;
            $counts[$product->id] = $product;
        }
        return $counts;
    }

    public static function getAllProductsCount($cid = ''){
        $counts = [];

        if($cid != ''){
            $cid = " AND campaign_id = $cid";
        }

        $sql = "SELECT p.* from public.campaign_product as cp join public.product AS p ON p.id=cp.product_id where cp.active = 1 and p.active = 1 $cid order by cp.campaign_id DESC, p.name";
        $products = Product::getList(['sql' => $sql]);
        foreach ($products AS $product){
            $countSQL = "SELECT SUM(op.quantity) FROM order_products op,public.order o WHERE op.order_id = o.id AND 1 = ALL(ARRAY[o.active,op.active]) AND (o.status = 'Active' OR o.status = 'Updated' )AND o.campaign_id IN (SELECT id FROM campaign WHERE active = 1) AND op.product_id = $product->id";
//            var_dump($countSQL);
            global $emagid;
            $db = $emagid->getDb();
            $results = $db->getResults($countSQL);
            $count = $results[0]['sum'];
            if($product->id == 101) {$count += 384; $count += 36;}
            $product->purchased_count = $count;
            $counts[$product->id] = $product;
        }
        return $counts;
    }

    public function getCampaignProducts(){
        return Campaign_Product::getList(['where' => "campaign_id = {$this->id}"]);
    }

    public function select_winners($count = 0){
        if($count == 0) $count = $this->c_limit;
        $count = $count - \Model\Lotto_Entry::getCount(['where'=>"campaign_id = $this->id AND winner = 1"]);
        $remaining = $count;
        $winners = [];
        foreach (Lotto_Entry::getList(['where'=>"campaign_id = {$this->id}",'orderBy'=>"random()",'limit'=>$count]) AS $entrant){
            $entrant->winner = 1;
            $entrant->ref_num = generateToken();
            if($entrant->save()){
                $remaining --;
                $winners[] = $entrant->id;
            };
        }
//        $this->status = 'Over';
//        $this->email_winners();
        $this->save();
        if($remaining < 0) $remaining = 0;
        return $remaining;
    }

    public function email_winners(Array $entryArr = [] ){
        if(count($entryArr) == 0){
            $entries = "";
        } else {
            $entries = 'AND id IN ('.implode(',',$entryArr).')';
        }
        $mergeFields = [
            'NAME' =>'',
            'PRODUCT'=>'',
            'REDEEM_LINK'=>'',
            'CAMPAIGN'=>$this->name,
        ];
        $notifications = [];
        foreach (Lotto_Entry::getList(['where'=>"campaign_id = {$this->id} $entries"]) AS $entrant){
            $mailMaster = new \Email\MailMaster();


            $now = Carbon::now();
//            $entrant->email_date = $now;
//            $entrant->save();
            $user = User::getItem($entrant->user_id);
            $product = Product::getItem($entrant->product_id);

            $html = '<tr style="height:75.0pt">
						<td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$product->featuredImage().'"></p>
						</td>
						<td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product->name.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">1</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($product->getPrice(),2).'</span></p>
						</td>
					</tr>';

            $mergeFields['NAME'] = $user->full_name();
            $mergeFields['PRODUCT_NAME'] = $product->name;
            $mergeFields['BILLING'] = $user->getAddress()->formatted();
            $mergeFields['PRODUCT_NAME'] = $product->name;
            $mergeFields['ITEMS'] = $html;
            $mergeFields['END_DATE'] = date("M d, Y",$this->end_time);;
            $mergeFields['ORDER_LINK'] = "https://".$_SERVER['SERVER_NAME'].'/redeem/'.$entrant->ref_num;

            $mailMaster->setTo(['email' => $user->email, 'name' => $user->full_name(), 'type' => 'to'])
                ->setMergeTags($mergeFields);
            if(strtotime($now) > $this->end_time || $this->status == 'Over' ){
                if($entrant->winner){
                    $mailMaster->setTemplate('kaws-lottery-winner-email');
                    if($entrant->redeemed == 1){
                        continue;
                    }

                } else {
                    $mailMaster->setTemplate('kaws-lottery-loser-email');
                }
            } else {
                $mailMaster->setTemplate('lottery-signup');
            }
            $mail = $mailMaster;
            try {
                $sent = $mail->send();
                $entrant->email_date = $now;
                $entrant->save();
            } catch(Mandrill_Error $e){
                $n = new \Notification\ErrorHandler('We could not send you an email at this time. We will contact you to confirm your order');
                $_SESSION['notification'] = serialize($n);
            }
//            var_dump($mail);

        }
    }
    public function email_confirm(Array $entryArr = [] ){
        if(count($entryArr) == 0){
            $entries = "";
        } else {
            $entries = 'AND id IN ('.implode(',',$entryArr).')';
        }
        $mergeFields = [
            'NAME' =>'',
            'PRODUCT'=>'',
            'REDEEM_LINK'=>'',
            'CAMPAIGN'=>$this->name,
        ];
        $notifications = [];
        foreach (Lotto_Entry::getList(['where'=>"campaign_id = {$this->id} $entries"]) AS $entrant){
            $mailMaster = new \Email\MailMaster();


            $now = Carbon::now();

            $user = User::getItem($entrant->user_id);
            $product = Product::getItem($entrant->product_id);

            $html = '<tr style="height:75.0pt">
						<td width="100" valign="top" style="width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p class="MsoNormal" style="text-align: right"><img width="100px!important;" src="'.$product->featuredImage().'"></p>
						</td>
						<td width="140" valign="top" style="text-align: left; width:75.0pt;border:none;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                            <p><span style="font-weight: bold;font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">'.$product->name.'</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">1</span></p>
						
                            <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$'.number_format($product->getPrice(),2).'</span></p>
						</td>
					</tr>';

            $mergeFields['NAME'] = $user->full_name();
            $mergeFields['PRODUCT_NAME'] = $product->name;
            $mergeFields['BILLING'] = $user->getAddress()->formatted();
            $mergeFields['PRODUCT_NAME'] = $product->name;
            $mergeFields['ITEMS'] = $html;
            $mergeFields['END_DATE'] = date("M d, Y",$this->end_time);;
            $mergeFields['ORDER_LINK'] = "https://".$_SERVER['SERVER_NAME'].'/redeem/'.$entrant->ref_num;

            $mailMaster->setTo(['email' => $user->email, 'name' => $user->full_name(), 'type' => 'to'])
                ->setMergeTags($mergeFields);
            if(false){
                if($entrant->winner){
                    $mailMaster->setTemplate('kaws-lottery-winner-email');
                } else {
                    $mailMaster->setTemplate('kaws-lottery-loser-email');
                }
            } else {
                $mailMaster->setTemplate('lottery-signup');
            }
            $mail = $mailMaster;
            try {
                $mail->send();
//                $entrant->email_date = $now;
//                $entrant->save();
            } catch(Mandrill_Error $e){
                $n = new \Notification\ErrorHandler('error sending email');
                $_SESSION['notification'] = serialize($n);
            }
        }
    }

    public static function getActiveCampaignProducts(){
        $campaigns = self::getList(['where' => "status = 'Active'"]);
        if(count($campaigns) > 0){
            $campaign = $campaigns[0];
            return $campaign->getProducts();
        }
        return [];
    }
  
}