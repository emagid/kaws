<?php

namespace Model;

use Emagid\Core\Model;

class Sale_Banner extends Model{
    static $tablename = 'sale_banner';
    static $fields = [
        'title',
        'subtitle',
        'display_order',
        'url'
    ];
}