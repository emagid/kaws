<?php

namespace Model;

use Carbon\Carbon;
use Emagid\Core\Model;

class Clearance extends Model{
    static $tablename = 'clearance';
    static $fields = [
        'product_id',
        'size_id',
        'color_id',
        'order_id',
        'price'=>['type'=>'numeric'],
        'start_date'=>['required',true],
        'end_date'=>['required',true],
        'starting_quantity',
        'current_quantity'
    ];

    static function getActive($params = []){
        $carbon = Carbon::now();
        $where = "start_date < '$carbon' and end_date > '$carbon' and current_quantity > 0";
        $arr = [];
        if($params){
            foreach($params as $option=>$values){
                switch($option){
                    case 'sizes':
                        $str = "'%\"" . implode("\"%','%\"", $values) . "\"%'";
                        $where .= " and size_id like any(array[$str])";
                        break;
                }
            }
            if(isset($params['limit']) && $params['limit']){
                $arr['limit'] = $params['limit'];
            }
            if(isset($params['offset']) && $params['offset'] != -1){
                $arr['offset'] = $params['offset'];
            }
        }
        $arr['where'] = $where;
        return self::getList($arr);
    }

    static function checkClearanceById($id){
        $clearance = self::getItem($id);
        $now = time();
        if($clearance && $clearance->current_quantity > 0 && strtotime($clearance->start_date) < $now && strtotime($clearance->end_date) > $now){
            return $clearance;
        } else {
            return null;
        }
    }

    static function checkClearance($product_id, $color_id, $size_id){
        if(($c = self::getItem(null,['where'=>"product_id = $product_id and color_id like '%\"$color_id\"%' and size_id like '%\"$size_id\"%' and current_quantity > 0 and now() between start_date and end_date"]))){
            return $c;
        } else {
            return null;
        }
    }

    static function checkClearanceV2($product_id,$color_id,$size_id){
        if(($c = self::getItem(null,['where'=>"product_id = $product_id and color_id = $color_id and size_id = $size_id"]))){
            return $c;
        } else {
            return null;
        }
    }

    function getClearanceDetails(){
        return Clearance_Details::getList(['where'=>"clearance_id = $this->id"]);
    }
}