<?php

namespace Model;

use Emagid\Core\Model;

class Transaction extends Model{
    static $tablename = 'transaction';
    static $fields = [
        'insert_time',
        'order_id',
        'authorize_net_data',
        'ref_tran_id',
        'amount',
        'type',
        'avs_result_code',
        'cvv_result_code',
        'import',
        'raw',
        'notes'
    ];
}