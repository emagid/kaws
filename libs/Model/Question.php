<?php 

namespace Model; 

class Question extends \Emagid\Core\Model {

	static $tablename = 'question'; 

	public static $status = ['Received','Approved','Rejected'];
	
	public static $fields =  [
		'insert_time',
		'product_id',
		'subject' => ['required'=>true],
		'body' => ['required'=>true],
		'sender_email',
		'status',
		'answer',
		'viewed',
		'admin_reply'
	];

	public function adminReply(){
		switch($this->admin_reply){
			case 0:
				echo 'Unanswered';break;
			case 1:
				echo 'Replied';break;
		}
	}
	
}