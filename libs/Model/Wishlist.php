<?php 

namespace Model; 

class Wishlist extends \Emagid\Core\Model {

	static $tablename = 'wishlist'; 
	
	public static $fields =  [
		'user_id',
		'product_id'
	];
	
	public static function getProductsByUserId($id){
		return \Model\Product::getList(['where'=>'product.id in (select wishlist.product_id from '.self::$tablename.' where wishlist.user_id = '.$id.' and wishlist.active = 1)']);
	}

}