<?php

namespace Model;

use Emagid\Core\Model;

class Transfer_Item extends Model{
	static $tablename = 'transfer_items';
	static $fields = [
        "order_id",
        "item_id",
        "status"
    ];
}