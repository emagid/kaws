<?php

namespace Model;

use Emagid\Core\Model;
use EmagidService\S3;

class Watch extends Model{
	static $tablename = 'watch';
	static $fields = ["video"];

	public function getVideo(){
	    $s3 = new S3();
	    return $this->video ? $s3->getUrlByKey($this->video): '';
    }
}