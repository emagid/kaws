<?php 

namespace Model; 

class Attribute extends \Emagid\Core\Model {

	static $tablename = 'attribute'; 
	
	public static $fields =  [
		'frontend_label', 
		'attribute_code',
		'attribute_id'
	];
	
}