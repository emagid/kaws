<?php

namespace Model;

use Emagid\Core\Model;
use EmagidService\S3;

class Blog_Category extends Model{
    static $tablename = 'blog_category';
    static $fields = [
        "id",
        "insert_time",
        "parent_id",
        "name",
        "display_order",
        "slug",
        "banner"
    ];

    public static function getOptions($self_id = 0)
    {
        $_categories = Blog_Category::getList(["orderBy"=>"display_order ASC"]);
        $categories = [];
        foreach($_categories as $category){
            if($category->id != $self_id){
                $categories[$category->id] = $category->name;
            }
        }
        return $categories;
    }

    public function getBanner(){
        $product = self::getItem($this->id);
        if($product->banner != ''){
            $s3 = new S3();
            return $s3->getUrlByKey($this->banner);
        }  else {
            return '';
        }
    }
}

