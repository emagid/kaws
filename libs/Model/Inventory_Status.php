<?php

namespace Model;

use Emagid\Core\Model;

class Inventory_Status extends Model{
	static $tablename = 'inventory_status';
	static $fields = ["name","display_order","sold_status","count_to_total"];

    public static function getSoldStatus(){
        return self::getItem(null,['where'=>'sold_status = 1'])->id;
    }

    public static function unavailable_statuses(){
        $statuses = [];
        foreach (self::getList(['where'=>'count_to_total = 0']) as $status){
            $statuses[] = $status->id;
        }
        return $statuses;
    }
}