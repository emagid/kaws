<?php

namespace Model;

use Emagid\Core\Model;

class Back_Order extends Model{
	static $tablename = 'back_order';
	static $fields = ["product_id","part_no","ean","upc","pi_num","qty_request","qty_receive","status"];
	static $status = ['Incomplete','Complete']; /** Replaced by Inventory Status */
}