<?php

namespace Model;

use Emagid\Core\Model;

class Private_Label extends Model{
    static $tablename = 'private_label';
    static $fields = [
        'title',
        'subtitle',
        'featured_image',
        'case_study',
        'type', //1=>Header Content, 2=>Case Study, 3=>Services, 4=>Additional
        'display_order'
    ];

    static $type = [1=>'Intro', 2=>'Case Study', 3=>'Services', 4=>'Additional'];

    public function getType(){
        if(array_key_exists($this->type,self::$type)){
            return self::$type[$this->type];
        } else{
            return null;
        }
    }

    public function getFeaturedImage(){
        return UPLOAD_URL.'private_labels/'.$this->featured_image;
    }
}