<?php

namespace Model;

use Emagid\Core\Model;

class Wholesale_Payment_Items extends Model{
	static $tablename = 'wholesale_payment_items';
	static $fields = ["product_id","sku","unit_price",'wholesale_payment_id'];
}