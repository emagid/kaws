<?php

namespace Model;

use Emagid\Core\Model;

class Wholesale_Payment extends Model{
	static $tablename = 'wholesale_payment';
	static $fields = ["order_id","tracking_number","status","response_code",'response_msg','total'];

	public function getItems($product_id = null){
		$where = "wholesale_payment_id = $this->id";
		$where .= $product_id ? " and product_id = $product_id": '';
		return Wholesale_Payment_Items::getList(['where'=>$where]);
	}
}