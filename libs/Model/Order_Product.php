<?php

namespace Model;

class Order_Product extends \Emagid\Core\Model {
    static $tablename = "order_products";
  
    public static $fields = [
  		'order_id',
  		'product_id',
  		'quantity',
  		'unit_price',
		'details',
		'clearance',
		'status'
    ];

	public function getProductName(){
		$json = json_decode($this->details, true);
		if($this->product_id && ($product = Product::getItem($this->product_id))){
			return $product->name;
		} else if(isset($json['misc_name'])){
			return $json['misc_name'] . '(Custom)';
		} else {
			return '';
		}
	}
	 
}