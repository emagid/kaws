<?php

namespace Model;

use Emagid\Core\Model;

class Invoice extends Model{
    static $tablename = 'invoice';
    static $fields = [
        'insert_time',
        'to_first_name',
        'to_last_name',
        'to_email',
        'from_first_name',
        'from_last_name',
        'subject',
        'body',
        'details',
        'ref_num',
        'status',
        'order_id',
        'notes',
        'tax_free',
        'shipping'
    ];
    static $status = ['New','Paid','Canceled','Refunded'];

    public function fullName(){
        return $this->to_first_name.' '.$this->to_last_name;
    }

    public static function getByRef($ref_num){
        if(($invoice = Invoice::getItem(null,['where'=>"ref_num = '$ref_num'"]))){
            return $invoice;
        } else {
            return null;
        }
    }
}