<?php

namespace Model;

use Emagid\Core\Model;
use Email\MailBag;

class Mail_Log extends Model{
    static $tablename = 'mail_log';
    static $fields = [
        'slug',
        'mandrill_id',
        'text',
        'from_',
        'to_',
        'subject',
        'state'
    ];

    static $states = [
        1=>"sent",
        2=>"bounced",
        3=>"rejected",
        4=>"soft-bounced",
        5=>"spam",
        6=>"unsub",
    ];

    public static function wrap(Array $mail = []){
        $mailBag = new MailBag();
        $content = $mailBag->retrieveEmailContents($mail['_id']);

        $log = New self();
        $log->mandrill_id = $mail['_id'];
        $log->state = $mail['state'];
        $log->text = $content['html'];
        $log->subject = $mail['subject'];
        $log->from_ = $mail['sender'];
        $log->to_ = $mail['email'];
        $log->opens = $mail['opens'];
        $log->delivery_date = date('M d, Y',$mail['ts']);
        if(isset($mail['reject'])){
            $log->reject_reason = $mail['reject']['reason'];
        }
        return $log;
    }
}