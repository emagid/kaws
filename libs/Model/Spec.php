<?php 

namespace Model; 

use EmagidService\S3;

class Spec extends \Emagid\Core\Model {

	static $tablename = 'spec';
	
	public static $fields =  [
		'category',
		'name',
        'icon'
	];

    public function getCategory(){
        if($this->category != 0){
            return self::getItem($this->category);
        } else {
            return '';
        }
    }

    public function getIcon(){
        $s3 = new S3();
        if($this->icon && ($icon = $s3->getUrlByKey($this->icon))){
            return $icon;
        }
        return null;
    }
	
}