<?php 

namespace Model; 

class Orderchange extends \Emagid\Core\Model {

	static $tablename = 'orderchange'; 
	
	public static $fields =  [
		'user_id', 
		'order_id',
		'before_change',
		'after_change',
		'insert_time'
	];
	
}