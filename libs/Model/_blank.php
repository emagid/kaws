* <?php 

namespace Model;

class _blank extends \Emagid\Core\Model {
	static $tablename = '_blank'; 
	
	/** the common way, including data annotations **/ 
	public static $fields = [
		'image',
		'link',
		'title' => ['required']
	];	

	/*
	* DO NOT ADD the following fields :
	* 1: active - make sure the default value is : 1
	* 2: id - should be Primary key and datatype SERIAL in the DB 
	* 3: insert_time - make sure the default value is : now()
	* 4: update_time - make sure the default value is : now()
	*/


	/** Alternative to inline meta information, you don't have to use it yet  **/ 
	static $meta = [
		'required' => ['title']
	];






	/* Hooks */ 
	/* the following functions will be called automatically, they are optional. 
	   the names are self explanatory 
	 */
	function beforeUpdate() {

	}

	function afterUpdate() {
		
	}

	function beforeInsert() {
		
	}

	function afterInsert() {
		
	}


	function beforeCheckRequired(){

	}

	function beforeValidate(){
		
	}

}