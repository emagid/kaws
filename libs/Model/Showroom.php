<?php
namespace Model;

class Showroom extends \Emagid\Core\Model {
	static $tablename = "showroom";
	public static $fields = [
		'first_name',
		'last_name',
		'email',
		'phone',
		'datetime',
		'interest'
	];
}