<?php

namespace Model;

use Carbon\Carbon;
use Emagid\Core\Model;

class Gift_Card extends Model{
    static $tablename = 'gift_card';
    static $fields = [
        'name',
        'code',
        'amount',
        'start_date',
        'end_date',
        'remaining',
        'gift_card_type',
        'image',
        'order_id'
    ];

    static $type = [1=>'Individual gift card',2=>'Gift card item'];

    public static function validateGiftCard($code){
        $carbon = Carbon::now()->format('Y-m-d H:i:s');
        if(($giftCard = self::getItem(null,['where'=>"code = '{$code}' and start_date < '$carbon' and end_date > '$carbon' and remaining != 0"]))){
            return $giftCard;
        } else {
            return null;
        }
    }

    function getRemainingAmount(){
        return $this->remaining ? : $this->amount;
    }

    function removeAmount($total){
        if($this->getRemainingAmount() > $total){
            $this->remaining = $this->getRemainingAmount()-$total;
        } else {
            $this->remaining = 0;
        }
        $this->save();
    }

    static function getGiftObj(){
        return self::getList(['where'=>'gift_card_type = 2','orderBy'=>'amount']);
    }

    static function generateNewGiftCard($id = null,$order_id = null,$amount = null){
        $gift = new Gift_Card();

        if($id){
            $gc = Gift_Card::getItem($id);
            $gift->name = 'Online Purchase $'.$gc->amount.' Card';
            $gift->amount = $gift->remaining = $gc->amount;
        } else {
            $gift->amount = $gift->remaining = $amount;
            $gift->name = "Auto Generated $".$amount." card for Qualifying purchase";
        }
        $gift->code = self::generateGiftCode();
        $gift->start_date = date('Y-m-d H:i:s');
        $gift->end_date = Carbon::now()->addYear()->format('Y-m-d H:i:s');
        $gift->order_id = $order_id ?: 0;
        $gift->save();
        return $gift;
    }

    static function generateGiftCode(){
        return strtoupper(generateToken());
    }
}