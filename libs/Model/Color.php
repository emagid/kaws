<?php

namespace Model;

use Emagid\Core\Model;

class Color extends Model{
    static $tablename = 'color';
    static $fields = [
        'name',
        'slug'=>['unique'=>true],
        'swatch',
        'made_to_order'
    ];

    public function name(){
        return $this->name;
    }

    public function swatch(){
        return $this->swatch ?
            '/content/uploads/colors/' . $this->swatch :
            FRONT_IMG . 'color_swatch_' . strtolower(str_replace(" ", "_", $this->name)) . '.png';
    }

    public static function search($terms, $limit = 100){
        $sql = "select id,name,slug from color where active = 1 and ";
        if(is_numeric($terms)){
            $sql .= "id = $terms or ";
        }

        $slug = preg_replace('/[^\w-]/','-',strtolower($terms));
        $slug = preg_replace('/[-]+/','-',$slug);
        $sql .= "(lower(name) like '%".strtolower(urldecode($terms))."%' or lower(slug) like '%".strtolower(urldecode($slug))."%') limit $limit";

        return self::getList(['sql'=>$sql]);
    }
}