<?php

namespace Model;

use Emagid\Core\Model;

class Inventory_Threshold extends Model{
	static $tablename = 'Inventory_Thresholds';
	static $fields = [
        "inv_id",
        "prod_id",
        "threshold"
    ];

    public static function get_low_counts($inv_id){
        $low_counts = [];
        foreach (self::getList(['where'=>"inv_id = '$inv_id'"]) as $thresh){
            $product = Product::getItem($thresh->product_id);
            if(!is_numeric($inv_id)){
                $count_inv = Actual_Inventory::getItem(null,['where'=>"product_id = $thresh->product_id AND $inv_id < $thresh->threshold "]);
                if($count_inv){
                    $low_counts[$product->name] = [$count_inv->$inv_id,$thresh->threshold];
                } else if($thresh->threshold > 0 && !$count_inv){
                    $low_counts[$product->name] = [0,$thresh->threshold];
                }
            } else {
                $count_inv = Product_Inventory::getItem(null,['where'=>"product_id = $thresh->product_id AND inventory_id = $inv_id AND quantity < $thresh->threshold"]);
                if($count_inv){
                    $low_counts[$product->name] = [$count_inv->quantity,$thresh->threshold];
                } else if($thresh->threshold > 0 && !$count_inv){
                    $low_counts[$product->name] = [0,$thresh->threshold];
                }
            }
        }
        return $low_counts;
    }
}