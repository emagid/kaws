<?php

namespace Model;
use EmagidService\S3;

class Labels extends \Emagid\Core\Model {
	static $tablename = 'labels';
	
	public static $fields = [
	    'insert_time',
	    'shipped_time',
        'order_id',
        'tracking_number',
        'claimed',
        'image',
        'type'
	];

	public static $types = [
	        1=>'Shipping',
        2=>'Return'
    ];

	public function image(){
        $s3 = new S3();
        if($this->image){
            return $s3->getUrlByKey($this->image);
        }
        return '';
    }

	function beforeUpdate() {

	}

	function afterUpdate() {
		
	}

	function beforeInsert() {
		
	}

	function afterInsert() {
		
	}


	function beforeCheckRequired(){

	}

	function beforeValidate(){
		
	}

}