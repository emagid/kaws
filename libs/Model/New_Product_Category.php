<?php

namespace Model;

class New_Product_Category extends \Emagid\Core\Model {
  
  static $tablename = "new_product_categories";
  
  public static $fields = [
    'new_product_id',
    'category_id',
    'image',
    'display_order'
  ];
  
  static $relationships = [
  	[
  		'name'=>'new_product',
  		'class_name' => '\Model\New_Product',
  		'local'=>'new_product_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  	[
  		'name'=>'category',
  		'class_name' => '\Model\Category',
  		'local'=>'category_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  ];

  public static function get_product_categories($product_id, $limit = "") {
    $pc = self::getList(['where'=>"new_product_id={$product_id} AND active=1", 'limit'=>$limit]);
    $arr = [];
    foreach($pc as $c) {
      $arr[] = $c->category_id;
    }
    return $arr;
  }

  public static function get_category_products($category_id, $limit = "", $except = "") {
  	$where = "category_id={$category_id} AND active=1 ";
  	if ($except != ""){
  		$where .= $except;
  	}
    $pc = self::getList(['where'=>$where, 'limit'=>$limit]);
    $list = [];
    foreach($pc as $c) {
      $list[] = New_Product::getItem($c->product_id);
    }
    return $list;
  }
  
}
