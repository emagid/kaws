<?php

namespace Model;

use Emagid\Core\Model;

class Size extends Model{
    static $tablename = 'size';
    static $fields = [
        'us_size',
        'eur_size'
    ];

    public function name(){
        $us = floatval($this->us_size);
        $eur = floatval($this->eur_size);
        return "EUR $eur | US $us";
    }
}