<?php

namespace Model;

use Emagid\Core\Model;

class Cart extends Model{
    static $tablename = 'cart';
    static $fields = [
        'user_id',
        'guest_id',
        'product_id',
        'quantity',
        'variation',
        'details',
        'clearance'
    ];

    public static function getActiveCart($user_id = null,$guest_id = null){
        $cartIds = isset($_COOKIE['cart']) && $_COOKIE['cart'] ? json_decode($_COOKIE['cart'],true): '';
        $where = 'active = 1';

        if($cartIds){
            $whereArr[] = "id in (".implode(',',$cartIds).")";
        }
        if($user_id){
            $whereArr[] = "user_id = $user_id";
        } else if($guest_id && $user_id == null){
            $whereArr[] = "guest_id = '{$guest_id}'";
        }
        $where .= " AND product_id IN (SELECT id FROM product WHERE active = 1) AND (".implode(' or ',$whereArr).")";

        foreach (self::getList(['where'=>$where]) as $cart){
            $cp = Campaign_Product::getItem(null,['where'=>"product_id = $cart->product_id"]);
            if($cp->max_per_user < $cart->quantity){
                $cart->quantity = $cp->max_per_user;
                $cart->save();
            }
        }
        return self::getList(['where'=>$where]);
    }

    public function getProduct(){
        return Product::getItem($this->product_id);
    }

    public function getColor(){
        return json_decode($this->variation,true)['color'];
    }

    public function getSize(){
        return json_decode($this->variation,true)['size'];
    }

    public function featuredImage()
    {
        $color_id = json_decode($this->variation, true)['color'];
        $prodImg = Product_Image::getItem(null, ['where' => "product_id = $this->product_id and color_id like '%\"$color_id\"%' and (legshot != 1 or legshot is null)", 'orderBy' => "display_order"]);
        if ($prodImg) {
            return $prodImg->image;
        } else {
            return '';
        }
    }
}