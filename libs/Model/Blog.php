<?php

namespace Model;

use Emagid\Core\Model;
use EmagidService\S3;

class Blog extends Model{
	static $tablename = 'blog';
	static $fields = [
	    "title",
        "subtitle",
        "body",
        "featured_image",
        "create_date",
        "modify_date",
        "author",
        "modify_author",
        "footer",
		"blog_category_id",
		"slug"
    ];

	public function getFeaturedImage(){
	    $s3 = new S3();
	    return $this->featured_image ? $s3->getUrlByKey($this->featured_image): '';
    }
}