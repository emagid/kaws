<?php 

namespace Model; 

class Admin extends \Emagid\Core\Model {
	
	static $tablename = 'administrator'; 

	//static $role = ['0'=>'custom', '1'=>'super', '2'=>'administrator', '3'=>'manager'];


	static $admin_sections =  ['Content','Banners','Pages', 'Administrators','Users',
		'Categories','Products','Parts','Catalog','Clearances','Popup Ads','Inventory','Inventory Status','Wholesales','Shipstation Orders',
        'Colors','Collections','Materials','System','Coupons','Order Management','Orders','Customer Center', 'Questions','Showrooms','Blogs','Blog Categorys','Watch',
        'Main Banners', 'Featured Banners', 'Alternate','Incomplete Orders','Deal of the Week', 'Shipping Methods', 'Newsletter', 'News', 'Configs','Hottest Deal', 'Selection', 'Media', 'Speciality', 'Instagram','Private Label', 'Sale Banners',
		'Sizes', 'Abandoned Cart', 'Custom Inventory', 'Archived Orders', 'Inquiry', 'Sales','Press','Status Management', 'Invoices', 'Events', 'Gift Cards','Reviews','Reports', 'Email Types', 'Blacklists'];
	
	static $admin_sections_nested = [
        'Campaigns'=>['Campaigns', 'Products'],
        'Order Management'=>['Orders', 'Archived Orders','Incomplete Orders','Fulfillment'],
		'Service' => ['Contacts','Email Logs'],
		'Marketing'=>[],
        'Customer Center'=>['Users','Blacklists'],
		'System'=>['Pages','Shipping Methods', 'Locations', 'Configs', 'Reports'],
		'Administrators'=>[],
	];
	
	public static $fields =  [
		'first_name'=>[
			'name'=>'First Name',
			'required'=>true
		], 
		'last_name'=>[
			'name'=>'Last Name',
			'required'=>true,
		],
		'email'=>[
			'type'=>'email',
			'required'=>true,
			'unique'=>true
		],
		'username'=>[
			'required'=>true,
			'unique'=>true
		],
		'password'=>[
			'type'=>'password',
			'required'=>true,
		],
		'hash', 
		'permissions'
	];
	
	function full_name() {
		return $this->first_name.' ' .$this->last_name;
	}
	
	public static function login($username , $password){
		$list = self::getList(['where'=>"lower(username) = lower('{$username}')"]);
		if (count($list) > 0) {
			$admin = $list[0];
			$password = \Emagid\Core\Membership::hash($password, $admin->hash, null );

			if ($password['password'] == $admin->password ) {

				$adminRoles = \Model\Admin_Roles::getList(['where' => 'active = 1 and admin_id = '.$admin->id]);
				$rolesIds = [];
				foreach($adminRoles as $role){
					$rolesIds[] = $role->role_id;
				}
				$rolesIds = implode(',', $rolesIds);
				
				$roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
	    		$rolesNames = [];
				foreach($roles as $role){
					$rolesNames[] = $role->name;
				}

	    		\Emagid\Core\Membership::setAuthenticationSession($admin->id,$rolesNames, $admin);

				return true;
			}
		}
		return false;
	}
	
	public static function getNestedSections ($permissions = null){
		if (is_null($permissions)){
			return self::$admin_sections_nested;
		} else {
			$permissions = explode(',', $permissions);			
			$sections = self::$admin_sections_nested;
			$authorized = [];
			foreach ($sections as $parent=>$children){
				if (in_array($parent, $permissions)){
					$authorized[$parent] = [];
					foreach ($children as $child){
						if (in_array($child, $permissions)){
							array_push($authorized[$parent], $child);
						}
					}
				}
			}
			return $authorized;
		}
	}
}




