<?php

namespace Model;

use Emagid\Core\Model;

class Press extends Model{
    static $tablename = 'press';
    static $fields = [
        'title',
        'featured_image',
        'url',
        'display_date',
        'display_order'
    ];
}