<?php

namespace Model;

use Emagid\Core\Membership;

class Blacklist extends \Emagid\Core\Model {
	static $tablename = 'blacklist';
	public static $fields  =  [
		'field',
		'value',
        'preview',
        'blacklist_id' //Creating a blacklist of types 4 or 5 will create 1 of each. Blacklists of type 5 will use this field to show which type 4s they are related to
	];

	public static $field_types = [
	    1 => 'email',
        2 => 'cc_number',
        3 => 'user_ip',
        4 => 'place_id',
        5 => 'composite',
        6 => 'phone'
    ];

	public static function display_types(){
        $display_names = [];
        foreach (self::$field_types AS $id => $field){
            switch($id){
                case 1:
                    $display_names[$id] = 'Email Address';
                    break;
                case 2:
                    $display_names[$id] = 'Credit Card Number';
                    break;
                case 3:
                    $display_names[$id] = 'Ip Address';
                    break;
                case 4:
                    $display_names[$id] = 'Mailing  Address';
                    break;
                case 5:
                    $display_names[$id] = 'Mailing Address';
                    break;
                case 6:
                    $display_names[$id] = 'Phone Number';
                    break;
                default:
                    $display_names[$id] = $field;
                    break;
            }
        }
        return $display_names;
    }

}