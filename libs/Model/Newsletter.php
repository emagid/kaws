<?php

namespace Model;

class Newsletter extends \Emagid\Core\Model {
	static $tablename = "newsletter";
	public static $fields = [
		'email'=>['required'=>true, 'type'=>'email'],
		'product_id',
		'is_expired',
		'create_date',
		'form_type',
		'discount_redeemed'
	];

	static $form_type = [1=>'Footer', 2=>'Quick View'];
	static function deleteCoupon($newsletter){
		$nl = self::getItem($newsletter->id);
		$nl->is_expired = 0;
		$nl->save();
	}

	static function checkCoupon($email){
		if($newsletter = Newsletter::getItem(null,['where'=>['email' => $email,'discount_redeemed'=>'0']])){
			return $newsletter;
		} else {
			return false;
		}
	}

	public function applyCoupon($subtotal){
		//Apply 10% coupon
		return $subtotal * (1-.1);
	}
	public function getDiscountAmount($subtotal){
		//Apply 15% coupon
		return $subtotal * (.15);
	}
}
