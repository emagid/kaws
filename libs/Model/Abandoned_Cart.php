<?php
namespace Model;

use Carbon\Carbon;
use Emagid\Core\Model;
use Email\MailMaster;
use Notification\ErrorHandler;
use Notification\MessageHandler;

class Abandoned_Cart extends Model{
    static $tablename = 'abandoned_cart';
    static $fields = [
        'update_time',
        'user_id',
        'guest_id',
        'checkout_fields',
        'cart',
        'send_email',
        'device',
        'browser',
        'version'
    ];

    public function getUsername(){
        if($this->user_id > 0){
            return ucwords(User::getItem($this->user_id)->full_name());
        } else {
            return 'Guest ID: '.$this->guest_id;
        }
    }

    public function getFullName(){
        //if applicable
        $details = json_decode($this->checkout_fields,true);
        $firstName = isset($details['ship_first_name']) ? $details['ship_first_name']: '';
        $lastName = isset($details['ship_last_name']) ? $details['ship_last_name']: '';
        return trim($firstName .' '. $lastName);
    }

    public function getCartProducts(){
        return array_map(function($item){
            $sql = "SELECT * FROM cart WHERE id = $item";
            return Cart::getItem(null,['sql'=>$sql]);
        },json_decode($this->cart));
    }

    public function getProductNames(){
        $cart = $this->getCartProducts();
        $prod = [];
        foreach($cart as $c) {
            if ($c) {
                $prod[] = $c->getProduct()->name;
            }
        }
        return $prod;
    }

    public static function getAllByField($param = 'email'){
        return self::getList(['where'=>"checkout_fields like '%$param%'"]);
    }

    public function getEmail(){
        $details = json_decode($this->checkout_fields,true);
        return isset($details['email']) ? $details['email'] : '';
    }

    //For siteController logic
    public function sendEmail()
    {
        $ac = $this;
        if ($ac->getEmail()) {
            $html = '';
            $cart = $ac->getCartProducts();
            $chunk = array_chunk($cart, 3);
            foreach ($chunk as $cart) {
                $html .= '<tr>';
                foreach ($cart as $array) {
                    $product = Product::getItem($array->product_id);
                    $color = json_decode($array->variation, true)['color'];
                    $size = json_decode($array->variation, true)['size'];
                    $html .= '
                        <td width="192" style="width: 192px;" valign="top">
                            <table width="192" border="0" cellpadding="0" cellspacing="0" align="center" class="force-row" style="width: 192px;"><tr>
                                    <td class="col" valign="top" style="padding-left:12px;padding-right:12px;padding-top:18px;padding-bottom:12px">
                                        <table border="0" cellpadding="0" cellspacing="0" class="img-wrapper">
                                            <tr>
                                                <td style="padding-bottom:18px"><a href="https://djinyc.com/products/'.$product->slug.'"><img src="'.$product->featuredImage() . '" border="0" alt="The Albatross flew across the ocean" width="168" height="110" hspace="0" vspace="0" style="max-width:100%; " class="image"></a></td>
                                            </tr>
                                        </table>
                                        <table border="0" cellpadding="0" cellspacing="0"><tr>
                                                <td class="subtitle" style="font-family:Helvetica, Arial, sans-serif;font-size:16px;line-height:22px;font-weight:600;color:#24262b;padding-bottom:6px">
                                                    <a href="https://djinyc.com/products/'.$product->slug.'">' . $product->name . '</a>
                                                </td></tr>
                                        </table>
                                        <div class="col-copy" style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:20px;text-align:left;color:#24262b">';
                    if ($product->basePrice() != $product->getPrice($color, $size)) {
                        $html .= '<a href="https://djinyc.com/products/'.$product->slug.'"><span style="text-decoration: line-through;color:rgba(36, 38, 43, 0.5);">$' . number_format($product->basePrice() * $array->quantity, 2) . '</span></a> ';
                    }
                    $html .= '<a href="https://djinyc.com/products/'.$product->slug.'"><span style="color:#24262b;">$' . number_format($product->getPrice($color, $size) * $array->quantity, 2) . '</span></a>
                                            </div>
                                            <br>
                                        </td>
                                    </tr>
                                </table>
                            </td>';
                }
                $html .= '</tr>';
            }
            $email = new MailMaster();
            $mergeTags = [
                'NAME'=>'Valued Customer',
                'ITEMS'=>$html
            ];
            if($ac->getFullName()){
                $mergeTags['NAME'] = $ac->getFullName();
            }
            $email->setTo(['email' => $ac->getEmail(), 'name' => ucwords($ac->getFullName()), 'type' => 'to'])->setTemplate('dji-abandoned-cart')->setMergeTags($mergeTags);
            $email->send();
            $ac->send_email = 1;
            $ac->save();
        }
    }
}