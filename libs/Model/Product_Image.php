<?php

namespace Model;

use Emagid\Core\Model;
use EmagidService\S3;

class Product_Image extends Model{
    static $tablename = 'product_images';
    static $fields = [
        'product_id',
        'image',
        'display_order',
        'color_id',
        'legshot',
        'image_alt'
    ];

    public function exists_image($size=[]) {
        $size_str = (count($size)==2) ? implode("_",$size) : "";
//        if($this->image!="" && file_exists(dirname(__FILE__).UPLOAD_URL.'products/'.$size_str.$this->image)) {
        if($this->image!="") {
            return true;
        }
        return false;
    }

//    public function get_image_url($size=[]) {
//        $size_str = (count($size)==2) ? implode("_",$size) : "";
//        return UPLOAD_URL.'products/'.$size_str.$this->image;
//    }

    public function get_image_url($size=[]) {

        $s3 = new S3();
        $img_path = $s3->getUrlByKey($this->image);

        return $img_path;
    }
}