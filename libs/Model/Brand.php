<?php
namespace Model;

class Brand extends \Emagid\Core\Model {
 
    static $tablename = "brand";

    public static $fields = [
  		'name' => ['required'=>true],
  		'description',
  		'image',
  		'slug' => ['required'=>true, 'unique'=>true],
  		'meta_title',
  		'meta_keywords',
  		'meta_description',
  		'banner',
  		'parent_id',
        'menu_display',
        'image_alt'
    ];

    protected static $relationships = [
        [
            'name'=>'products', 
            'class_name' => '\Model\Product',
            'local'=>'id',
            'remote'=>'brand',
            'relationship_type' => 'many'
        ]
    ];

    public static function search($keywords, $limit = 20){
        $sql = "select id, name, image from brand where active = 1 and (";
        if (is_numeric($keywords)){
            $sql .= "id = ".$keywords." or ";
        }
        $sql .= " lower(name) like '%".strtolower(urldecode($keywords))."%') limit ".$limit;
        return self::getList(['sql' => $sql]);
    }

    public function getParentBrands(){
        return self::getList(['where'=>"parent_id = 0 or parent_id is null and id != '".$this->id."' and active = 1", 'orderBy'=>'name']);
    }

    public function getChildrenBrands(){
        return self::getList(['where'=>"parent_id = $this->id and active = 1", 'orderBy'=>"name"]);
    }

    public function brandDisplay(){
        if (is_null($this->banner)){
            return '/content/frontend/img/banner_brands.jpg';
        } else {
            return UPLOAD_URL.'brands/1200_344'.$this->banner;
        }
    }
  
}