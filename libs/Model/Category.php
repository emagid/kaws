<?php

namespace Model;

use EmagidService\S3;

class Category extends \Emagid\Core\Model {

  	static $tablename = "category";

  	public static $fields = [ 
	    'name' => ['required'=>true],
	    'description',
	    'title',
	    'slug' => ['required'=>true,'unique'=>true],
	    'parent_category',
	    'meta_title',
		'meta_keywords',
		'meta_description',
	    'banner',
		'collection',
		'alias',
		'display_order',
		'featured_product',
		'banner_text_color',
		'featured_category',
		'new_arrivals',
		'product_preview_ids',
		'info_banners',
		'hide_banner_text',
		'coupon_block',
		'banner_link',
        'banner_description',
		'default_product',
        'display',
		'icon'
	];
  	
	static $relationships = [
      [
        'name'=>'product_category',
        'class_name' => '\Model\Product_Category',
        'local'=>'id',
        'remote'=>'category_id',
        'remote_related'=>'product_id',
        'relationship_type' => 'many'
      ],
    ];

	public static function getNested($parentId,$showDisplay = false){
		$displaySql = $showDisplay ? "": "and display = 1";
		$categories = self::getList(['where'=>"active = 1 AND parent_category = {$parentId} $displaySql", 'orderBy'=>"display_order"]);
		
		foreach($categories as $category){
			$category->children = self::getNested($category->id, $showDisplay);
		};
			
		return $categories;
	}

	public function getParent(){
		if($this->parent_category != 0){
			return self::getItem($this->parent_category);
		} else {
			return '';
		}
	}
	public function getName(){
		return $this->alias != '' ? $this->alias: $this->name;
	}

	public function buildMenu(){
		$active = isset($this->emagid->route['category_slug']) && $this->emagid->route['category_slug'] == $this->slug? 'active': '';
		$dropdown = count($this->children) > 0?'dropdown':'';
		$init_menu = '<li class="'.$active.' '.$dropdown.'"><a class="dropdown-tiggle dropdown-toggle-special" role="button" aria-expanded="false" href="'.SITE_URL.'collections/'.$this->slug.'">' .$this->getName(). '</a>';
		$html = $init_menu;
		if(count($this->children) > 0) {
			$html .= '<ul class="dropdown-menu" role="menu">';
			foreach ($this->children as $child) {
				$html .= $this->buildChildMenu($child);
			}
			$html .= '</ul>';
		}
		$html .= '</li>';
		return $html;
	}

	function buildChildMenu($value){
		$parent = Category::getItem($value->parent_category);
		$html = '<li><a href="'.SITE_URL.'collections/'.$parent->slug.'/'.$value->slug.'">'.$value->getName().'</a>';
//		$html = '<ul class="dropdown-menu" role="menu"><li><a href="'.SITE_URL.'category/'.$value->slug.'">'.$value->getName().'</a>';
		if(count($value->children) > 0) {
			$html .= '<ul class="dropdown-menu" role="menu">';
			foreach($value->children as $child){
				$html .= $this->buildChildMenu($child);
			}
			$html .= '</ul>';
		}
		$html .= '</li>';
//		$html .= '</li></ul>';

		return $html;
	}

	public function buildSortable(){
		$display = $this->display == 1 ? 'checked': '';
		$html = '<li data-name="'.$this->name.'" data-id="'.$this->id.'"><input class="display_category" type="checkbox" value="'.$this->display.'" '.$display.'/>'.$this->name.'<ol>';
		if($this->children){
			foreach($this->children as $child){
				$html .= $this->buildChildSortable($child);
			}
		}
		$html .= '</ol></li>';
		return $html;
	}

	function buildChildSortable($child){
		$element = $child;
		$display = $element->display == 1 ? 'checked': '';
		$html = '<li data-name="'.$element->name.'" data-id="'.$element->id.'"><input class="display_category" type="checkbox" value="'.$element->display.'" '.$display.'/>'.$element->name;
//		$html = '<li data-name="'.$element->name.'" data-id="'.$element->id.'">'.$element->name.'<ol>';
		if($element->children){
			foreach($element->children as $c) {
				$html .= $this->buildChildSortable($c);
			}
		}
		$html .= '</li>';
//		$html .= '</ol></li>';
		return $html;
	}

	/*
	 * for category admin sortable
	 * */
	public static function updateRelationship($id,$children){
		$display = 1;
		foreach($children as $child){
			$c = self::getItem($child->id);
			$c->parent_category = $id;
			$c->display_order = $display;
			d($c);
			$c->save();
			if($child->children[0]){
				self::updateRelationship($child->id,$child->children[0]);
			}
			$display++;
		}
	}

	public function getProductCategory($limit = null, $offset = null){
		$query['where'] = "category_id = $this->id";
		if($limit){
			$query['limit'] = $limit;
		}
		if($offset){
			$query['offset'] = $offset;
		}
		$query['orderBy'] = "product_id desc";
		return Product_Category::getList($query);
	}

	public function getProducts($limit = null, $offset = null){
		$prodCat = $this->getProductCategory($limit,$offset);
		$products = [];
		foreach($prodCat as $pc) {
			if (Product::getItem($pc->product_id)) {
				$products[] = Product::getItem($pc->product_id);
			}
		}
		return $products;
	}

	public function getProductsByType($user, $type = 1){
	    $wsql = '';
        if($user && $user->wholesale_id && ($ws = Wholesale::getItem(null,['where'=>"id = $user->wholesale_id and status = 1"])) ){
            $wsItems = $ws->getItemList();
            $wholesaleProductIds = array_map(function($item){return $item->product_id;},$wsItems);
            $wsql = "and p.id in (".implode(',',$wholesaleProductIds).")";
        }

        $sql = "select p.* from product p inner join product_categories pc on pc.product_id = p.id where p.active = 1 and pc.active =1 and p.type = '$type' and pc.category_id = $this->id $wsql ORDER BY pc.display_order";
        if($type === 0){//show everything
            $sql = "select p.* from product p inner join product_categories pc on pc.product_id = p.id where p.active = 1 and pc.active =1 and pc.category_id = $this->id $wsql ORDER BY pc.display_order";
        }
	    return Product::getList(['sql'=>$sql]);
    }

	public static function array_flatten($array) {

		$return = array();
		foreach ($array as $key => $value) {
			if (is_array($value)){ $return = array_merge($return, self::array_flatten($value));}
			else {$return[$key] = $value;}
		}
		return $return;

	}

	public function getIcon(){
		$s3 = new S3();
		return $this->icon ? $s3->getUrlByKey($this->icon): '';
	}

	public function getSubNav($column = null){
		$where = "category_id = $this->id";
		if($column){
			$where .= " and column_num = $column";
		}
		return Subnav::getList(['where'=>$where, 'orderBy'=>'display_order']);
	}

	public function getSubNavColImage($col){
		return Subnav::getItem(null,['where'=>"category_id = $this->id and display_order = 1 and column_num = $col"]);
	}

	public function getFeaturedProduct(){
		return Product::getItem($this->featured_product);
	}

	public static function filterSql($params = []){
		$where = '';
		if(isset($params['where']) && $params['where']){
			$where = implode(' AND ',$params['where']);
		}
		$orderBy = '(SELECT min(display_order) from product_categories where product_categories.product_id = product.id and product_categories.active=1),id';
		if(isset($params['orderBy']) && $params['orderBy']){
			$orderBy = implode(',',$params['orderBy']);
		}
		if(isset($params['limit']) && $params['limit']){
			$limit = $params['limit'];
		}
		if(isset($params['offset']) && $params['offset'] !== false){
			$offset = $params['offset'];
		}

		$sql = 'SELECT '.implode(',',array_map(function($value){return 'product.'.$value;},Product::getSelectFields())).'
		FROM product WHERE availability != 0 AND '.$where.'
		ORDER BY '.$orderBy;
		if(isset($limit) && isset($offset)) {
			$sql .= ' LIMIT ' . $limit . '
					OFFSET ' . $offset;
		}
		return $sql;
	}

    public static function search($keywords, $limit = 20)
    {
        $sql = "select * from public.category where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "id = " . $keywords . " or ";
        }
        $sql .= "lower(name) like '%" . strtolower(urldecode($keywords)) . "%') limit " . $limit;
        return self::getList(['sql' => $sql]);
    }
    public static function getDisplayCategories($parentId,$isMain){
        $where = "active = 1 and display = 1 and ";
        if($isMain == 0){
           $where .= "id = {$parentId}";
        }else{
            $where .= "parent_category = {$parentId}";
        }
        return self::getList(['where'=>$where,'orderBy'=>'display_order']);
    }
    public static function getDefaultProduct($id){
        $cat = self::getItem($id);
        if($cat->default_product>0){
            return Product::getItem($cat->default_product);
        }else{
            $prodCat = $cat->getProductCategory();
            foreach($prodCat as $pc) {
                $prod = Product::getItem($pc->product_id);
                if ($prod && $prod->type=='1') {
                    return $prod;
                }
            }

        }
        return null;
    }
}