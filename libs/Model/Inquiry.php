<?php

namespace Model;

class Inquiry extends \Emagid\Core\Model{
    static $tablename = 'Inquiry';
    static $fields = [
        'name',
        'email',
        'phone',
        'estimated_budget',
        'description',
        'featured_image',
        'token',
        'questionnaire',
        'notes',
        'status'
    ];
    static $status = ['New', 'Accepted', 'Rejected'];

    public function featuredImage(){
        $inquiry = self::getItem($this->id);
        if($inquiry->featured_image != ''){
            return $inquiry->featured_image;
        } else {
            return '';
        }
    }

    public static function validateQuestionnaireToken($token){
        if($token){
            $ques = self::getItem(null,['where'=>"token = '$token' and (questionnaire = '' or questionnaire is null)"]);
            return $ques;
        } else {
            return false;
        }
    }
}