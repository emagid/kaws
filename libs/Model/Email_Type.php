<?php

namespace Model;

class Email_Type extends \Emagid\Core\Model {

    static $tablename = 'email_type';

    public static $fields =  [
        'title'=>['required'=>true],
        'message',
        'subject'=>['required'=>true],
    ];

}