<?php
namespace Model;

class Popup_Ad extends \Emagid\Core\Model {
	static $tablename = "Popup_Ad";
	public static $fields = [
		'title',
		'subtitle',
		'style',
		'product_id',
		'coupon_id',
		'options',
		'featured_image'
	];

	static $styles = ['No Styles'];
}