<?php

namespace Model;

class Answer extends \Emagid\Core\Model {
  static $tablename = "answer";
  public static $fields = ['text'=>['required'=>true],'name'=>['required'=>true]];
}
