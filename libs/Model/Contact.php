<?php
namespace Model;

use Email\MailMaster;
use s3Bucket\s3Handler;

class Contact extends \Emagid\Core\Model{

    static $tablename = "contact";
    public static $admin_reply = [1=>'Unanswered',2=>'Admin Replied',3=>'Other Replied'];

    public static $fields = [
        'insert_time',
        'name' => ['required' => true],
        'email'=>['required'=>true],
        'message',//questions
        'answer',
        'viewed',
        'admin_reply',
        'subject'
    ];

    public function adminReply(){
        switch($this->admin_reply){
            case 1:
                echo 'Unanswered';break;
            case 2:
                echo 'Admin Replied';break;
            case 3:
                echo 'Other Replied';break;
        }
    }
}