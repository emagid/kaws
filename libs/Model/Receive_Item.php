<?php

namespace Model;

use Emagid\Core\Model;

class Receive_Item extends Model{
	static $tablename = 'receive_items';
	static $fields = [
	    "receiving_id",
        "image",
        "name",
        "sku",
        "status",
        "inventory",
        'order_product_id',
        'product_id'
    ];
}