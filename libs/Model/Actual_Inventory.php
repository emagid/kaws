<?php

namespace Model;

use Emagid\Core\Model;

class Actual_Inventory extends Model{
	static $tablename = 'actual_inventory';
	static $fields = ["product_id","amazon","ebay","square","warehouse", 'apt_310','website', 'total'];

	static $places = ['amazon'=>'Amazon',
                      'ebay'=>'eBay',
                      'square'=>'Square',
                      'warehouse'=>'Warehouse',
                      'apt_310'=>'Apt_310',
                      'website' => 'Website'];

	public function get_total_w_custom(){
	    $total = $this->total;
	    foreach (Product_Inventory::getList(['where'=>"product_id = {$this->product_id}"]) as $inventory){
	        $total += $inventory->quantity;
        }
        return $total;
    }

    public function get_total_o_custom(){
	    $total = 0;
        foreach (Product_Inventory::getList(['where'=>"product_id = {$this->product_id}"]) as $inventory){
            $total += $inventory->quantity;
        }
        return $total;
    }

    public function include_custom(){

        foreach (Custom_Inventory::getList() as $ci){
            $inventory = Product_Inventory::getItem(null,['where'=>"product_id = {$this->product_id} AND inventory_id = {$ci->id}"]);
            if($inventory != null){
                $quantity = $inventory->quantity;
            } else {
                $quantity = '';
            }
            $this->{"inventory_$ci->id"} = $quantity;
        }
    }

    public static function recalculate($product_id){
        $total = 0;
        $actual_inv = self::getItem(null,['where'=>"product_id = $product_id"]);
        $unavails = '('.implode(',',Inventory_Status::unavailable_statuses()).')';
        foreach (\Model\Actual_Inventory::$places as $value){
            $inv = strtolower($value);
            $actual_inv->$inv = \Model\Receive_Item::getCount(['where'=>"product_id = $product_id AND status NOT IN $unavails AND inventory = '$value'"]) ? : 0;
            $total += $actual_inv->$inv;
        }
        foreach (\Model\Custom_Inventory::getList() as $inv){
            $pInv = \Model\Product_Inventory::getItem(null,['where'=>"product_id = $product_id and inventory_id = $inv->id"]) ? : new \Model\Product_Inventory();
            $pInv->product_id = $actual_inv->product_id;
            $pInv->inventory_id = $inv->id;
            $pInv->quantity  =  \Model\Receive_Item::getCount(['where'=>"product_id = $product_id AND status NOT IN $unavails AND inventory = '{$inv->id}'"])?:0;
            $pInv->save();
        }
        $actual_inv->total = $total;
        $actual_inv->save();
        return $total;
    }
}