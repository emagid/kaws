<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 3/21/17
 * Time: 3:01 PM
 */

namespace Model;

class Service_Status extends \Emagid\Core\Model {
    static $tablename = "service_status";
    public static $fields = [
        'insert_time',
        'name'
    ];

    public static function getSubmittedEstimateStatus()
    {
        return self::getItem(37);
    }

    public static function getEstimateStatus()
    {
        return self::getItem(12);
    }

    public static function getEstimatePendingStatus()
    {
        return self::getItem(30);
    }

    public static function getApproveStatus()
    {
        return self::getItem(22);
    }

    public static function getPaidStatus()
    {
        return self::getItem(26);
    }

    public static function getPaidFailedStatus()
    {
        return self::getItem(40);
    }
}