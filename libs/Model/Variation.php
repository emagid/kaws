<?php

namespace Model;

use Emagid\Core\Model;

class Variation extends Model{

    /**
     * Variation name is a combination of possible variations for a single product
     * Name sequence: {material}_{color} ie. leather-black
     * Variation ID is placed in Product_Attribute
     * Further variants should be appended using '_'
     **/

    static $tablename = 'variation';
    static $fields = [
        'name'
    ];

    /**
     * Models for Variation sequence
     **/
    static $variations = ['Material','Color','Sole','Size'];
    static $lowerVariations = ['material','color','sole','size'];

    /**
     * @params $name_value_map = ['material'=>'leather','color'=>'red',...]
     * @return \Model\Variation
    */
    public static function addVariation($name_value_map = []){
        //['material'=>'leather','color'=>'c', 'sole'=>'derp']
        $name = [];
        foreach($name_value_map as $key=>$value){
            $modelName = ucfirst(strtolower($key));
            $model = "\Model\\$modelName";
            $value = is_numeric($value) ? $model::getItem($value)->slug : $value;

            $name[] = "$key:$value";
        }
        $name = implode('_',$name);
        $variation = Variation::getItem(null,['where'=>"name = '$name'"])?:new Variation();
        $variation->name = $name;
        if($variation->save()){
            return $variation;
//            $arr = ['price'=>$price,'image'=>$image];
//            Product_Attributes::addAttribute($product_id,$variation->id,$arr);
        } else {
            return null;
        }
    }

    public function getVarName(){
        $val = explode('_',$this->name);
        $names = [];
        if(count($val) <= self::$variations){
            for($x = 0; $x < count($val); $x++){
                $model = "\\Model\\".self::$variations[$x];
                $slug = self::extrapolateSlug($val[$x]);
                $item = $model::getItem(null,['where'=>"slug = '$slug'"]);
                $names[] = mb_strimwidth(self::$variations[$x],0,3,'').': '.$item->name;
            }
        }
        return implode(' - ', $names);
    }

    public static function extrapolateSlug($variationSlug){
        return substr($variationSlug,strpos($variationSlug,':')+1);
    }

    /**
     * Nested function for getting every variation in product/update.php
     * @params $arrays = [['a','b','c'],['d','e','f'],['g','h','i']
     * @return nested array
    */
    public static function combinations($arrays, $i = 0) {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }

        // get combinations from subsequent arrays
        $tmp = self::combinations($arrays, $i + 1);

        $result = array();

        // concat each array from tmp with each element from $arrays[$i]
        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                    array_merge(array($v), $t) :
                    array($v, $t);
            }
        }

        return $result;
    }

    public function getAttribute($product_id = null, $name = null, $value = null){
        $params = ["active = 1", "variation_id = $this->id"];
        $arr = ['product_id', 'name', 'value'];
        foreach($arr as $ar){
            if($$ar){
                $params[] = "{$ar} = '{$$ar}'";
            }
        }
        $imploded = implode(' and ',$params);
        return Product_Attributes::getItem(null,['where'=>$imploded]);
    }
}