<?php

namespace Model;

use Emagid\Core\Model;

class Shop_Instagram extends Model{
    static $tablename = 'Shop_Instagram';
    static $fields = [
        'product_id',
        'title',
        'featured_image',
        'url',
        'display_date',
        'display_order'
    ];
}