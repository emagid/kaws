<?php
namespace Model;

class Order extends \Emagid\Core\Model
{
    static $tablename = "public.order";

    public static $status = ['New', 'Paid', 'Declined',
        'Shipped', 'Complete', 'Delivered',
        'Canceled', 'Discontinued', 'Returned','Rejected',
        'Processed', 'Refunded', 'Incomplete PayPal',
        'Banned','Limit Reached','Possible Duplicate',
        'Active','Updated','Attn Needed','Merged'];
    public static $fulfillment_status = ['Processed', 'Shipped', 'Returned','Ready','Rejected'];
    public static $payment_method = [1=>"Credit Card", 2=>"PayPal", 3=>"Cash", 4=>"Deposit", 5=>"Gift Card", 6=>"Bank Wire", 7=>'Check'];
    public static $mass_email_template = 'Dear %1$s, <br><br>
We would like to thank you for your patience and support as we continue to work arduously on your order.<br><br>
We want you to know that we are focused on completing your shoes with the quickest turnaround possible. As stated on our website, we take 10-20 Business days to complete your pair of shoes. However, the influx of orders has unfortunately extended our production time.<br><br>
Your Order is currently in the stage described below:<br><br>
%2$s
We sincerely apologize that your order has taken longer than anticipated and we truly appreciate your support.<br><br><br><br>
Warmest Regards,<br><br>Modern Vice';
    public static $mass_email = [
        'Stage 1'=>'We&#39;ve received the materials &amp; components for your shoe(s). At this stage, we cut the materials in the shapes and sizes determined by the pattern of the shoe. From this point, the materials for this order are sent to the stitching station.<br><br>
We will update you once your shoe is fully stitched.<br><br>',
        'Stage 2'=>'The upper material of your shoe(s) have been fully stitched. At this point we will shape your shoes with our foot molds. Once your shoes have been placed on the foot mold, we allow the upper material to sit on the mold and then re attach the soles.<br><br>',
        'Stage 3'=>'Your shoe(s) are on the foot mold to fully take shape. We&#39;re adjusting the soles and placing them on the shoes so your order can be completed. Please allow 3-5 days for us to ship your shoes. Upon shipping your order, you&#39;ll receive an email with the tracking number.<br><br>'
    ];
    //public static $status = ['New', 'Paid', 'Payment Declined', 'Shipped', 'Complete', 'Pending Paypal', 'Canceled', 'Imported as Processing', 'Returned'];

    public static $fields = [
        'insert_time',
        'viewed' => ['type' => 'boolean'],
        // billing info
        'bill_first_name' => ['name' => 'Bill - First Name'],
        'bill_last_name' => ['name' => 'Bill - Last Name'],
        'bill_address' => ['name' => 'Bill - Address'],
        'bill_address2',
        'bill_city' => [ 'name' => 'Bill - City'],
        'bill_state' => ['name' => 'Bill - State'],
        'bill_country',
        'bill_zip' => ['name' => 'Bill - Zip'],
        //shipping info
        'ship_first_name' => ['name' => 'Shipping - First Name'],
        'ship_last_name' => ['name' => 'Shipping - Last Name'],
        'ship_address' => ['name' => 'Shipping - Address'],
        'ship_address2',
        'ship_city' => ['name' => 'Shipping - City'],
        'ship_state' => ['name' => 'Shipping - State'],
        'ship_country',
        'ship_zip' => ['name' => 'Shipping - Zip'],
        //card info
        'cc_number' => ['type' => 'numeric', 'name' => 'Credit Card Number'],
        'cc_expiration_month' => ['type' => 'numeric', 'name' => 'Credit Card Expiration Month'],
        'cc_expiration_year' => ['type' => 'numeric', 'name' => 'Credit Card Expiration Year'],
        'cc_ccv' => ['type' => 'numeric', 'name' => 'Credit Card CCV'],
        // price info
        'payment_method',
        'shipping_method',
        'shipping_cost' => ['numeric'],
        'shipping_charge' => ['numeric'],
        'subtotal' => ['numeric'],
        'total' => ['numeric'],
        'tax',
        'tax_rate',
        //shipping and tracking info
        'status',
        'tracking_number',
        'user_id',
        'orig_total',
        'coupon_code',
        'coupon_type',
        'coupon_amount',
        'gift_card',
        'email' => ['type' => 'email'],
        'phone',
        'note',
        'comment',
        'fraud_status',
        'guest_id',
        'card_name',
        'error_message',
        'ref_num',
        'in_store',
        'paypal_id',
        'gift_card_amount',
        'newsletter_discount',
        'payment_status',
        'email_log',
        'trans_id',
        'discount',
        'payment_id',
        'ship_id',
        'ship_status',
        'ship_message',
        'wholesale_id',
        'wholesale_payment_type',
        'ticket_id',
        'additional_charge',
        'auth_number',
        'ticket_id',
        'is_preorder',
        'case_id',
        'received_payment_method',
        'wholesale_term',
        'allow_returns',
        'fulfillment_status',
        'user_ip',
        'campaign_id',
        'entry_id',
        'place_id',
        'difference',
        'add_updated',
        'add_visited',
        'add_update_sent',
        'duty',
        'transid',
        'charge_added',
        'charge_email_sent',
        'refunded',
        'generated',
        'discarded',
        'pull_attempts',
        'original_order_id' 
    ];

    function shipName()
    {
        return $this->ship_first_name . ' ' . $this->ship_last_name;
    }

    function billName()
    {
        return $this->bill_first_name . ' ' . $this->bill_last_name;
    }

    function getShippingAddr()
    {
        $addr = $this->ship_address . ' ' . $this->ship_address2 . '<br>' . $this->ship_city . ', ' . $this->ship_state . '. ' . $this->ship_zip . ' ' . $this->ship_country ;
        return str_replace(', .','.',$addr);
    }

    

    public function duplicate(){
        $new_order = $this;
        $order_id = $this->id;
        $new_order->id = 0;
        $new_order->original_order_id = $order_id;
        if($new_order->save()){
            $dupeLabelSQL =
            "INSERT INTO labels (insert_time,active,shipped_time,order_id,tracking_number,claimed,image,type)
            SELECT insert_time,active,shipped_time,$new_order->id,tracking_number,claimed,image,type
            FROM labels WHERE order_id = $order_id";
            $dupeNoteSQL = 
            "INSERT INTO note_log (insert_time,active,order_id,note,admin_id)
            SELECT insert_time,active,$new_order->id,note,admin_id
            FROM note_log WHERE order_id = $order_id";
            $dupeTransactionSQL = 
            "INSERT INTO public.transaction (insert_time,active,order_id,authorize_net_data,ref_tran_id,amount,type,avs_result_code,cvv_result_code,import,raw,notes)
            SELECT insert_time,active,$new_order->id,authorize_net_data,ref_tran_id,amount,type,avs_result_code,cvv_result_code,import,raw,notes
            FROM public.transaction WHERE order_id = $order_id";
            $dupeProductSQL = 
            "INSERT INTO order_products (insert_time,active,order_id,product_id,quantity,unit_price,details,clearance,status)
            SELECT insert_time,active,$new_order->id,product_id,quantity,unit_price,details,clearance,status
            FROM order_products WHERE order_id = $order_id";

            global $emagid;
            $db = $emagid->getDb();
            $db->execute($dupeTransactionSQL);
            $db->execute($dupeProductSQL);
            $db->execute($dupeLabelSQL);
            $db->execute($dupeNoteSQL);
            return $new_order;
        }
        return false;
    }

    function getBillingAddr()
    {
        $addr = $this->bill_address . ' ' . $this->bill_address2 . '<br>' . $this->bill_city . ', ' . $this->bill_state . '. ' . $this->bill_zip . ' ' . $this->bill_country ;
        return str_replace(', .','.',$addr);
    }

    function getExpiry(){
        $m = str_pad($this->cc_expiration_month,2,'0',STR_PAD_LEFT);
        $y = substr($this->cc_expiration_year,-2);
        return $m.$y;
    }

    public function beforeValidate()
    {
        if ($this->payment_method == 1) {
//            self::$fields['cc_number']['required'] = true;
//            self::$fields['cc_expiration_month']['required'] = true;
//            self::$fields['cc_expiration_year']['required'] = true;
        }
//        if ($this->bill_country == 'United States') {
//            self::$fields['bill_state']['required'] = true;
//        }
//        if ($this->ship_country == 'United States') {
//            self::$fields['ship_state']['required'] = true;
//        }
    }

    public function getNotes(){
        return Note_Log::getList(['where'=>"order_id = $this->id"]);
    }

    public function getLabels(){
        return Labels::getList(['where'=>"order_id = $this->id"]);
    }

    public static function getDashBoardData($year, $month, $failures = 'all')//$failures = 'all', shows all orders, failures included,
    {                                                                        //            'none' shows none of the failures
        $data = new \stdClass();                                             //            'only' shows only failed orders

        $allStatus = implode("','", Order::$status);
        switch($failures){
            case 'none':
                $allStatus = preg_replace('/(Declined,|Canceled,|Incomplete PayPal,)/','',$allStatus);
                break;
            case 'only':
                $allStatus = preg_replace('/(Declined,|Canceled,|Incomplete PayPal,)/','',$allStatus);
                break;

        }

        $sql = " FROM " . self::$tablename . " WHERE ";
        $sql .= " EXTRACT(MONTH FROM insert_time)::INTEGER = " . $month;
        $sql .= " AND EXTRACT(YEAR FROM insert_time)::INTEGER = " . $year;
        $sql .= " AND status in ('" . $allStatus . "')";

        $data->monthly_sales = 0;
        $monthly_sales = self::execute("SELECT SUM(total) AS s " . $sql);
        if (count($monthly_sales) > 0) {
            $data->monthly_sales = $monthly_sales[0]['s'];
        }

        $data->gross_margin = 0;

        $data->monthly_orders = 0;
        $monthly_orders = self::execute("SELECT COUNT(id) AS c " . $sql);
        if (count($monthly_orders) > 0) {
            $data->monthly_orders = $monthly_orders[0]['c'];
        }

        return $data;
    }

    public static function getWeeklyDashboard($present, $past, $failures = 'all')//$failures = 'all', shows all orders, failures included,
    {                                                                            //            'none' shows none of the failures
        $statuses = "('Declined','Canceled','Incomplete PayPal')";               //            'only' shows only failed orders
        switch ($failures) {
            case 'all':
                $statuses = '';
                break;
            case 'none':
                $statuses = $statuses = "AND status NOT IN ".$statuses;
                break;
            case 'only':
                $statuses = $statuses = "AND status IN ".$statuses;
                break;
        }
        $sql = "SELECT SUM (total) FROM ".self::$tablename." WHERE (insert_time between '$past' and '$present')".$statuses;
        $weekly = self::execute($sql);
        $data = new \stdClass();
        $data->weekly_sales = $weekly[0]['sum'];
        return $data;
    }

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id,insert_time,status,tracking_number,total,payment_status,ship_first_name, ship_last_name, bill_first_name,bill_last_name from public.order where active = 1 and (";
        $parsed = strtolower(urldecode($keywords));
        $expl = explode(' ',$parsed);
        $impl = "'%".implode("%','%",$expl)."%'";
        if (is_numeric($keywords)) {
            $sql .= "id = " . $keywords . " or ";
        }
        $sql .= "
        lower(email) like '%$parsed%' or
        lower(ship_first_name) like any(array[$impl]) or
        lower(ship_first_name) like any(array[$impl]) or
        lower(bill_first_name) like any(array[$impl]) or
        lower(bill_last_name) like any(array[$impl]))
        limit $limit";
        return self::getList(['sql' => $sql]);
    }

    public function calcTogether(Array $products = []){
        $breakdown = [];
        $charges = [];
        $update = [
            'shipping_cost'=>0,
            'duty'=>0,
            'subtotal'=>0,
            'tax'=>0,
            'tax_rate'=>0,
        ];
        $totalPackages = 0;
        $shipping = Shipping_Method::getItem(null,['where'=>"Country = '$this->ship_country'"]);
        if($this->id == 0 && !empty($products)){
            foreach ($products as $prod_id => $qty){
                $totalPackages += $qty;
                $product = Product::getItem($prod_id);
                $breakdown[] = $product->name.' ('.$product->color.') - x'.$qty;
                $charges[] = $product->price*$qty;
            }
        } else if($this->getOrderProducts() && count($this->getOrderProducts()) > 0){
            foreach ($this->getOrderProducts() AS $orderProduct){
                $totalPackages += $orderProduct->quantity;
                $product = Product::getItem($orderProduct->product_id);
                $breakdown[] = $product->name.' ('.$product->color.') - x'.$orderProduct->quantity;
                $charges[] = $product->price*$orderProduct->quantity;
            }
        }
        $update['subtotal'] = array_sum($charges);
        $country = $this->bill_country;
        if(strlen($country) > 2){
            $countryKeys = array_flip(get_countries());
            $country = $countryKeys[$country];
        }
        if($country == 'US'){
            $breakdown[] = 'Tax';
            $tax = 0;
            if($this->bill_state == 'NY' || strtolower(trim($this->state)) == 'new york'){
                $update['tax_rate'] = 8.875;
                $tax = array_sum($charges)*.08875;
            } else if($this->bill_state == 'NJ' || strtolower(trim($this->state)) == 'new jersey'){
                $update['tax_rate'] = 6.625;
                $tax = array_sum($charges)*.06625;
            }
            $charges[]=$tax;
            $update['tax'] = $tax;
        } else {
            $breakdown[] = "Duty - ".get_countries()[$country].", ".$shipping->duty."%";
            $breakdown[] = "Disbursement - ".get_countries()['$country'].", $".$shipping->disbursement[1];
            $update['duty'] = ($shipping->duty*$update['subtotal']) + $shipping->disbursement;
            $charges[] = ($shipping->duty*$update['subtotal']);
            $charges[] = $shipping->disbursement;
        }
        $price = $shipping->price;
        $addon = $shipping->addon;
        $shipCharge = $price + (($totalPackages-1)*$addon);
        $breakdown[] = "Shipping - $shipping->name";
        $charges[] = $shipCharge;
        $update['shipping_cost'] = $shipCharge;

        return ['breakdown'=>$breakdown,'charges'=>$charges,'updates'=>$update];
    }

    public function calculate_total(Array $products = []){
        if($this->campaign_id = 6){
            return $this->calcTogether($products);
        }
        $breakdown = [];
        $charges = [];
        $update = [
          'shipping_cost'=>0,
          'duty'=>0,
          'subtotal'=>0,
          'tax'=>0,
          'tax_rate'=>0,
        ];
        $totalPackages = 0;
        foreach ($this->getOrderProducts() AS $orderProduct){
            $totalPackages += $orderProduct->quantity;
            $product = Product::getItem($orderProduct->product_id);
            $breakdown[] = $product->name.' ('.$product->color.') - x'.$orderProduct->quantity;
            $charges[] = $product->price*$orderProduct->quantity;
        }
        $update['subtotal'] = array_sum($charges);
        $country = $this->bill_country;
        if(strlen($country) > 2){
            $countryKeys = array_flip(get_countries());
            $country = $countryKeys[$country];
        }
        if($country == 'US'){
            $breakdown[] = 'Tax';
            $tax = 0;
            if($this->bill_state == 'NY' || strtolower(trim($this->state)) == 'new york'){
                $update['tax_rate'] = 8.875;
                $tax = array_sum($charges)*.08875;
            } else if($this->bill_state == 'NJ' || strtolower(trim($this->state)) == 'new jersey'){
                $update['tax_rate'] = 6.625;
                $tax = array_sum($charges)*.06625;
            }
            $charges[]=$tax;
            $update['tax'] = $tax;
        } else {
            $duty_charges = get_duty()[$country];
            $breakdown[] = "Duty - ".get_countries()[$country].", ".$duty_charges[0]."%";
            $breakdown[] = "Disbursement - ".get_countries()['$country'].", $".$duty_charges[1];
            $update['duty'] = $duty_charges[0]*array_sum($charges) + $duty_charges[1];
            $charges[] = $duty_charges[0]*array_sum($charges);
            $charges[] = $duty_charges[1];
        }
        $shipping = Shipping_Method::getItem(null,['where'=>"country = '$country'"]);
        $ship_charges = json_decode($shipping->cost,true);
        $shipCharge = $ship_charges[0] + (($totalPackages-1)*$ship_charges[1]);
        $breakdown[] = "Shipping - $shipping->name";
        $charges[] = $shipCharge;
        $update['shipping_cost'] = $shipCharge;

        return ['breakdown'=>$breakdown,'charges'=>$charges,'updates'=>$update];
    }

    public function getOrderProducts(){
        $orderProducts = Order_Product::getList(['where'=>"order_id = $this->id"]);
        return $orderProducts ? : null;
    }

    function getInvoice(){
        if($invoice = Invoice::getItem(null,['where'=>"order_id = $this->id"])){
            return $invoice;
        } else {
            return null;
        }
    }

    public static function getIntervalCosts($params = []){

        $total = 0;
        $authTotal = 0;
        $paypalTotal = 0;
        $tax = 0;
        $shipping = 0;
        $count = 0;

        $current = ['day'  =>date('j'),
                    'month'=>date('n'),
                    'week' =>date('W'),
                    'year' =>date('Y')];
        $group =   ['day'=>"insert_time::date AS day",
                    'week'=>"date_trunc('week', insert_time)::date
   || ' to '
   || (date_trunc('week', insert_time)+ '6 days'::interval)::date AS week",
                    'month'=>"to_char(insert_time, 'Mon') || ' ' || EXTRACT(YEAR FROM insert_time)::integer AS month",
                    'year'=>"EXTRACT(YEAR FROM insert_time)::integer AS year"];



        if(!isset($params['group'])){
            $params['group'] = 'day';
        }

        $firstDate = 0;
        $lastDate = 'insert_time::date <= current_date ';
        if(isset($params['lastDate'])){
            $lastDate = "insert_time::date <= '{$params['lastDate']}'::date ";
        }
        if(isset($params['firstDate'])){
            $firstDate = "insert_time::date >= '{$params['firstDate']}'::date ";
        } else {
            $firstDate = "insert_time::date >= '2017-11-01'::date ";
        }

        $sql = "SELECT COUNT(*) as count, SUM(total) as total, SUM(CASE WHEN lower(payment_status) = 'authorizenet success' OR lower(payment_status) = 'firstdata success' THEN total ELSE 0 END) AS authorize, SUM(CASE WHEN lower(payment_status) = 'paypal payment complete' THEN total ELSE 0 END) AS paypal, SUM(tax::float) as tax, SUM(shipping_cost) as shipping_fees, ";
                $sql .= $group[$params['group']];
                $sql .= " FROM public.order WHERE ";
                $sql .= $firstDate.'AND '.$lastDate;
                $sql .= " AND status NOT IN ('Declined', 'Canceled', 'Incomplete PayPal') ";
                $sql .= " AND active = 1";
                $sql .= " GROUP BY {$params['group']}";
                $sql .= " ORDER BY {$params['group']} DESC";
        global $emagid;
        $db = $emagid->getDb();

        $avgsql = 'SELECT COUNT(*) as count, SUM(total) as sum, AVG(total) as avg ';
        $avgsql .= "FROM public.order WHERE ";
        $avgsql .= $firstDate.'AND '.$lastDate;
        $avgsql .= " AND status NOT IN ('Declined', 'Canceled', 'Incomplete PayPal') ";
        $avgsql .= " AND active = 1";

        $results = $db->execute($sql);
        foreach ($results as $result){
            $total      += floatval($result['total']);
            $paypalTotal += floatval($result['paypal']);
            $authTotal += floatval($result['authorize']);
            $tax        += floatval($result['tax']);
            $shipping   += floatval($result['shipping_fees']);
        }

        $avg = $db->execute($avgsql)[0];
        $count = $avg['count'];
        $average = $avg['avg'];
        $percentage = ($total !== 0 ? ($tax / $total) : 0) * 100;
        $shipPercent = ($total !== 0 ? ($shipping / $total) : 0) * 100;
        return ['results'=>$results,
                'total'=>$total,
                'authTotal'=>$authTotal,
                'paypalTotal'=>$paypalTotal,
                'tax'=>$tax,
                'shipping'=>$shipping,
                'count'=>$count,
                'avg'=>$average,
                'sql'=>$sql,
                'tax_percentage'=>$percentage,
                'ship_percentage'=>$shipPercent];
    }

    function invoicePaid(){
        $invoice = $this->getInvoice();
        if($invoice){
            $invoice->status = Invoice::$status[1];
            $invoice->save();
        }
    }

    public static function getShipstationOrder($orderParams)
    {
        $query = 'select * from public.order where active = 1';
        if(isset($orderParams['orderDateStart']) && isset($orderParams['orderDateEnd'])){
            $query .= " and insert_time >= '{$orderParams['orderDateStart']}' and insert_time <= '{$orderParams['orderDateEnd']}'";
        }
        
        $orders = Order::getList(['sql' => $query]);
        return array_map(function($item){
            return Order::wrapShipstation($item);
        }, $orders);
    }

    public static function wrapShipstation($order)
    {
        $st = new \stdClass();
        $st->orderId = $order->id;
        $st->createDate = $order->insert_time;
        $st->tracking_number = $order->tracking_number;
        $st->orderStatus = $order->status;
        if(isset($st->advancedOptions)){
        } else {
            $st->advancedOptions = new \stdClass();
        }
        $st->advancedOptions->storeId = 'emagid';
        if(isset($st->shipTo)){
        } else {
            $st->shipTo = new \stdClass();
        }
        $st->shipTo->name = "{$order->ship_first_name} {$order->ship_last_name}";
        $st->items = array_map(function($item){
            $p = Product::getItem($item->product_id);
            if($p) return $p;
        }, Order_Product::getList(['where' => "order_id = '{$order->id}'"]));
        $st->orderTotal = $order->total;
        $st->type = 'emagid';
        return $st;
    }

    public function validate_order(){
        $banned = false;
        $climited = false;
        $plimited = false;
        $msg = '';
        foreach (Blacklist::$field_types AS $fid => $field){
            if($fld = 5){
                $ship_address = [
                    'address'=>     $this->ship_address,
                    'address2'=>    $this->ship_address2,
                    'city'=>        $this->ship_city,
                    'zip'=>         $this->ship_zip,
                    'state'=>       $this->ship_state,
                    'country'=>     $this->ship_country,
                ];
                $bill_address = [
                    'address'=>     $this->bill_address,
                    'address2'=>    $this->bill_address2,
                    'city'=>        $this->bill_city,
                    'zip'=>         $this->bill_zip,
                    'state'=>       $this->bill_state,
                    'country'=>     $this->bill_country,
                ];
                $sAddr2 = $ship_address['address2']?"AND LOWER(value)::json->>'address2' = LOWER('{$ship_address['address2']}')":'';
                $bAddr2 = $bill_address['address2']?"AND LOWER(value)::json->>'address2' = LOWER('{$bill_address['address2']}')":'';


                if(Blacklist::getCount(['where'=>"field = $fid AND 
                (
                (LOWER(value)::json->>'address' = LOWER('{$ship_address['address']}')
                $sAddr2
                AND LOWER(value)::json->>'city' = LOWER('{$ship_address['city']}')
                AND LOWER(value)::json->>'zip' = LOWER('{$ship_address['zip']}')
                AND LOWER(value)::json->>'state' = LOWER('{$ship_address['state']}')
                AND LOWER(value)::json->>'country' = LOWER('{$ship_address['country']}')) 
                OR 
                (LOWER(value)::json->>'address' = LOWER('{$bill_address['address']}')
                $bAddr2
                AND LOWER(value)::json->>'city' = LOWER('{$bill_address['city']}')
                AND LOWER(value)::json->>'zip' = LOWER('{$bill_address['zip']}')
                AND LOWER(value)::json->>'state' = LOWER('{$bill_address['state']}')
                AND LOWER(value)::json->>'country' = LOWER('{$bill_address['country']}'))
                )"]) > 0){
                    $banned = true;
                }
                echo("SELECT * FROM blacklist WHERE field = $fid AND 
                (
                (LOWER(value)::json->'address' = LOWER('{$ship_address['address']}')
                $sAddr2
                AND LOWER(value)::json->'city' = LOWER('{$ship_address['city']}')
                AND LOWER(value)::json->'zip' = LOWER('{$ship_address['zip']}')
                AND LOWER(value)::json->'state' = LOWER('{$ship_address['state']}')
                AND LOWER(value)::json->'country' = LOWER('{$ship_address['country']}')) 
                OR 
                (LOWER(value)::json->'address' = LOWER('{$bill_address['address']}')
                $bAddr2
                AND LOWER(value)::json->'city' = LOWER('{$bill_address['city']}')
                AND LOWER(value)::json->'zip' = LOWER('{$bill_address['zip']}')
                AND LOWER(value)::json->'state' = LOWER('{$bill_address['state']}')
                AND LOWER(value)::json->'country' = LOWER('{$bill_address['country']}'))
                )");
            } else {
                if(Blacklist::getCount(['where'=>"field = $fid AND value = '{$this->$field}' "]) > 0){
                    $banned = true;
                }
            }
        }
        $campaign = Campaign::getItem($this->campaign_id);
        if($campaign->c_limit != 0){
            $countSql = "SELECT SUM(op.quantity), c.name, c.id ".
                "FROM order_products op, campaign c, public.order o ".
                "WHERE o.id = op.order_id AND c.id = o.campaign_id ".
                "AND (o.email = '{$this->email}' OR o.user_ip = '{$this->user_ip}') AND o.active = 1 AND op.active = 1 AND c.id = $campaign->id ".
                "AND o.status NOT IN ('Declined','Canceled','Refunded','Banned','Limit Reached') ".
                "GROUP BY o.email, c.name, c.id";
            global $emagid;
            $db = $emagid->getDb();
            $count = $db->getResults($countSql)[0]['sum'];
            if($campaign->c_limit < $count ){
                $climited = true;
            }
        }
        foreach ($campaign->getCampaignProducts() as $cp){
            if($cp->quantity == 0) continue;
            $pCountSql = "SELECT sum(op.quantity)
                          FROM PUBLIC.order o, 
                                order_products op
                          WHERE  op.order_id = o.id 
                             AND ( o.email = '{$this->email}'
                             OR o.user_ip = '{$this->user_ip}')
                             AND op.active = 1
                             AND o.active = 1
                             AND o.campaign_id = {$cp->campaign_id}
                             AND op.product_id = {$cp->product_id}
                             AND o.status NOT IN ('Declined','Canceled','Refunded','Banned','Limit Reached')
                          GROUP BY op.product_id, o.campaign_id";
            global $emagid;
            $db = $emagid->getDb();
            $pCount = $db->getResults($pCountSql)[0]['sum'];
            if($pCount && $pCount > $cp->max_per_user){
                $plimited = true;
                $product = Product::getItem($cp->product_id);
                $msg .= "You can not purchase any more {$product->name}s<br/>";
            }
        }


        if($banned) {
            $this->status = self::$status[12];
            $n = new \Notification\ErrorHandler('There was an issue processing your order');
            $_SESSION['notification'] = serialize($n);
        } else if ($plimited) {
            $this->status = self::$status[13];
            $n = new \Notification\ErrorHandler($msg);
            $_SESSION['notification'] = serialize($n);
        } else if ($climited) {
            $this->status = self::$status[13];
            $n = new \Notification\ErrorHandler('You can not purchase any more products in this campaign');
            $_SESSION['notification'] = serialize($n);
        }
        $this->save();
        if($this->status == self::$status[13]){
            Order::delete($this->id);
        }
        return !$banned && !$climited && !$plimited;
    }
}