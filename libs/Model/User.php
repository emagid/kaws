<?php

namespace Model;

use Emagid\Core\Membership;

class User extends \Emagid\Core\Model {
    static $tablename = "public.user";

    public static $fields  =  [
    	'email'=>['type'=>'email'],
    	'password','hash',
    	'first_name'=>['name'=>'First Name'],
    	'last_name'=>['name'=>'Last Name'],
        'phone',
        'ref_id',
        'company_name',
        'address',
        'address_1',
        'city',
        'state',
        'zip',
        'dob',
        'memo',
        'square_id',
        'first_visit',
        'trans_count',
        'total_spend',
        'subscribe',
        'is_stc',
        'wholesale_id'
    	 ];

    /**
    * concatenates first name and last name to create full name string and returns it
    * @return type: string of full name
    */
    function full_name() {
        return $this->first_name.' '.$this->last_name;
    }

    /**
    * Verify login and create the authentication cookie / session 
    */
    public static function login($email , $password){
        $user = self::getList(['where'=>"email = '".$email."'"]);
        if (count($user)>0){
            $user = $user[0];
            $hash = \Emagid\Core\Membership::hash($password, $user->hash);            
            if ($hash['password'] == $user->password) {
                $userRoles = \Model\User_Roles::getList(['where' => 'active = 1 and user_id = '.$user->id]);
                $rolesIds = [];
                foreach($userRoles as $role){
                    $rolesIds[] = $role->role_id;
                }
                $rolesIds = implode(',', $rolesIds);

                $roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
                $rolesNames = [];
                foreach($roles as $role){
                    $rolesNames[] = $role->name;
                }

                \Emagid\Core\Membership::setAuthenticationSession($user->id, $rolesNames, $user);

                return true;
            } else {
            	$n = new \Notification\ErrorHandler('Incorrect email or password.');
	           	$_SESSION["notification"] = serialize($n);
            }
        } else {
        	$n = new \Notification\ErrorHandler('Email not found.');
	        $_SESSION["notification"] = serialize($n);
        }
    }

    public function getAddress(){
        return Address::getItem(null,['where'=>"user_id = $this->id"]);
    }

    /**
     * Generate a user account after checkout
     * @param $order : Order|Object
     * @return User|Object
     */
    public static function checkoutUser($order){
        $email = $order->email;
        $first_name = $order->ship_first_name;
        $last_name = $order->ship_last_name;
        $password = 'MV1'.str_pad($order->id,5,0,STR_PAD_LEFT);
        $phone = $order->phone;

        if(($user = self::getItem(null,['where'=>"email = '$email'"]))){
            return $user;
        } else {
            $hash = Membership::hash($password);

            $user = new User();
            $user->email = $email;
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->password = $hash['password'];
            $user->hash = $hash['salt'];
            $user->phone = $phone;
            if($user->save()){
                $user_role = new User_Roles();
                $user_role->user_id = $user->id;
                $user_role->role_id = 2;
                $user_role->save();
                return $user;
            } else {
                return null;
            }
        }
    }
    
    public static function addToMailChimp($email){
        $apikey = MAILCHIMP_API;
        $auth = base64_encode( 'user:'.$apikey );

        $data = array(
            'apikey'        => $apikey,
            'email_address' => $email,
            'status'        => 'subscribed'
        );
        $json_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://us9.api.mailchimp.com/3.0/lists/6e5d8beec8/members/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

        $result = curl_exec($ch);

        //dd(json_decode($result));

        // var_dump($result);
        // die('Mailchimp executed');
    }

    public function getPasswordResetLink()
    {
        return SITE_DOMAIN."/reset/password/$this->hash";
    }
}