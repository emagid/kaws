<?php
namespace Model;

use Carbon\Carbon;

class New_Product extends \Emagid\Core\Model
{

    public static $tablename = "new_product";

    public static $fields = [
        'name' => ['required' => true],
        'price' => ['type' => 'numeric', 'required' => true],
        'description',
        'msrp' => ['type' => 'numeric'],
        'quantity' => ['type' => 'numeric'],
        'slug' => ['required' => true, 'unique' => true],
        'tags',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'featured_image',
        'color',
        'size',
        'availability', // TODO st-dev: replaced day_delivery with availability
        'video_link',
        'heel_height',
        'related_products',
        'details',
        'total_sales' => ['type' => 'numeric'],
        'default_color_id' => ['type' => 'numeric'],
        'linked_list',
        'linked_name'
    ];

    static $relationships = [
        [
            'name' => 'new_product_category',
            'class_name' => '\Model\New_Product_Category',
            'local' => 'id',
            'remote' => 'new_product_id',
            'remote_related' => 'category_id',
            'relationship_type' => 'many'
        ]
    ];

    static $SORT_MAP = [
        'product.insert_time desc', // newest
        'product.total_sales desc', // best seller
        'product.price desc', // price high to low
        'product.price asc', // price low to high
    ];

    public static function search($keywords, $limit = 20)
    {
        $colors = Color::getList(['where'=>"lower(name) like '%".strtolower($keywords)."%'"]);
        $sql = "select id, name, featured_image,price, msrp, slug, color from product where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "CAST(id as TEXT) like '" . $keywords . "%' or ";
        }
        $colorList = implode('"%\',\'%"',array_map(function($item){return $item->id;},$colors));
        $explodeTags = explode(' ',strtolower(urldecode($keywords)));
        $tags = "'%".implode("%','%",$explodeTags)."%'";
//        $sql .= " lower(name) like '%" . strtolower(urldecode($keywords)) . "%' or
//            lower(tags) like any(array[". $tags."])
//            ) order BY name ASC limit " . $limit;
        $sql .= " lower(name) like all (array[".$tags."])";
        if($colorList){
            $sql .= " or color like any(array['%\"$colorList\"%'])";
        }
        $sql .= ") order BY name ASC limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public static function getPriceSum($productsStr)
    {
        $total = 0;
        foreach (explode(',', $productsStr) as $productId) {
            $product = self::getItem($productId);
            $total += $product->price;
        }
        return $total;
    }

    /**
     * @param null $color_id = for querying price variations in Product_Attributes
     * @param null $size_id
     * @return mixed
     */
    public function price($color_id = null, $size_id = null){
        $p = $this->price;
        $queryColorId = $color_id ? : $this->default_color_id;
        $queryArr = ["product_id = $this->id","color_id like '%\"$queryColorId\"%'"];
        $queryArr[] = "size_id like '%\"$size_id\"%'";
        if(($clear = Clearance::checkClearance($this->id,$queryColorId,$size_id))){
            $value = $clear->price;
        } else if(($productAttribute = Product_Attributes::getItem(null,['where'=>"product_id = $this->id and color_id = $queryColorId and name = 'price'"])) && $productAttribute->value > 0){
//            Increment
//            $value = $p + $productAttribute->value;

//            Override
            $value = $productAttribute->value;
        } else {
            $value = $p;
        }
        if(($eventAttribute = Product_Attributes::getEventById($this->id,$queryColorId)) && $eventAttribute->value > 0){
            $event = Event::getItem($eventAttribute->value);
            if($event && $event->validateEvent()) {
                return $event->calculateEvent($value);
            }
        }
        $now = Carbon::now();
        $sales = Sale::getList(['where'=>"'$now' between start_date::timestamp and end_date::timestamp"]);
        if($sales){
            foreach($sales as $sale){
                $value = $sale->calculateSale($value,$this->id);
            }
            return $value;
        } else {
            return $value;
        }
    }

    public function basePrice($color_id = null){
        $p = $this->price;
        $queryColorId = $color_id ? : $this->default_color_id;
        if(($productAttribute = Product_Attributes::getItem(null,['where'=>"product_id = $this->id and color_id = $queryColorId and name = 'price'"]))&& $productAttribute->value > 0){
//            return $p + $productAttribute->value;
            return $productAttribute->value;
        } else {
            return $p;
        }
    }

    /**
     * @param null $color_id = for querying price variations in Product_Attributes
     * @param null $size_id
     * @return mixed
     */
    public function msrp($color_id = null,$size_id = null){
        $m = $this->msrp;
        $value = $m;
        $queryColorId = $color_id ? : $this->default_color_id;
        if(($productAttribute = Product_Attributes::getItem(null,['where'=>"product_id = $this->id and color_id = $queryColorId and name = 'price'"]))&& $productAttribute->value > 0){
            $value = $productAttribute->value;
        }
        if(($eventAttribute = Product_Attributes::getEventById($this->id,$queryColorId)) && $eventAttribute->value > 0){
            $event = Event::getItem($eventAttribute->value);
            if($event->validateEvent()) {
                return $event->calculateEvent($value);
            }
        }
        $now = Carbon::now();
        $sales = Sale::getList(['where'=>"'$now' between start_date::timestamp and end_date::timestamp",'orderBy'=>"sale_type desc"]);
        if($sales){
            foreach($sales as $sale){
                $value = $sale->calculateSale($value,$this->id);
            }
            return $value;
        } else {
            return $value;
        }
    }

    public function getPrice($color_id = null,$size_id = null){
        if($this->msrp != 0){
            return $this->msrp($color_id,$size_id);
        } else {
            return $this->price($color_id,$size_id);
        }
    }
//    public function getPrice($color_id = null)
//    {
//        $queryColorId = $color_id ?: $this->default_color_id;
//        if ($this->msrp != 0) {
//            if (($productAttribute = Product_Attributes::getItem(null, ['where' => "product_id = $this->id and color_id = $queryColorId and name = 'price'"])) && $productAttribute->value > 0) {
//                return $this->price($color_id);
//            } else {
//
//                return $this->msrp($color_id);
//            }
//        } else {
//            return $this->price($color_id);
//        }
//    }
    

    public function isDiscounted($color_id = null){
        $color = $color_id ? : $this->default_color_id;
        if($this->msrp != '' && floatval($this->msrp) > 0){
            return true;
        } else if (Sale::onSale($this->id)) {
            return true;
        } else if(Event::eventSale($this->id,$color)){
            return true;
        } else {
            return false;
        }
    }

    public function getAvailability(){
        switch($this->availability){
            case 0:
                return 'Inactive';break;
            case 1:
                return 'In Stock';break;
            case 2:
                return 'Active';break;
        }
        return null;
    }

    public function getSubBrands(){
        return Brand::getList(['where'=>"parent_id = $this->brand"]);
    }

    public static function generateToken()
    {
        return md5(uniqid(rand(), true));
    }

    public function featuredImage($category_id = null, $color_id = null){
        $product = self::getItem($this->id);
        if($category_id && ($prodCat = Product_Category::getItem(null,['where'=>['product_id'=>$this->id,'category_id'=>$category_id]])) && $prodCat->image){
            return $prodCat->image;
        } elseif($color_id){
            return Product_Image::getItem(null,['where'=>"product_id = {$this->id} and color_id like '%\"$color_id\"%' and (legshot = 0 or legshot is null)", 'orderBy'=>'display_order'])->image;
        } else if($product->featured_image != ''){
            return $product->featured_image;
        } elseif($product = Product_Image::getItem(null,['where'=>"product_id = {$this->id} and image != ''"])) {
            return $product->image;
        } else {
            return '';
        }
    }

    public function getAllProductImages($getDefaultColor = true,$color_id = null){
        $arr = [];
//        if($this->featured_image != ''){
//            $arr[] = $this->featured_image;
//        }
        $where = "product_id = '{$this->id}' and (legshot = 0 or legshot is null)";
        if($this->default_color_id && $getDefaultColor){
            $where .= " and color_id like '%\"$this->default_color_id\"%'";
        } else if($color_id){
            $where .= " and color_id like '%\"$color_id\"%'";
        }
        if($pro = Product_Image::getList(['where'=>$where, 'orderBy'=>"display_order"])){
            foreach($pro as $item){
                $arr[] = $item->image;
            }
        }
        return $arr;
    }

    public function getLegShot(){
        $where = "product_id = $this->id and legshot = 1";
        if($this->default_color_id){
            $where .= " and color_id like '%$this->default_color_id%'";
        }
        $prodImg = Product_Image::getItem(null,['where'=>$where]);
        if($prodImg){
            return $prodImg->image;
        } else {
            return null;
        }
    }

    public function videoLink()
    {
        if($this->video_link){
            return UPLOAD_URL.'products/'.$this->video_link;
        }

        return false;
    }

    public function getColors(){
        $list = json_decode($this->color,true);
        if($list){
            $imp = implode(',',$list);
            return Color::getList(['where'=>"id in ($imp)", 'orderBy'=>'name']);
        } else {
            return [];
        }
    }

    public function getSizes(){
        $list = json_decode($this->size,true);
        if($list){
            $imp = implode(',',$list);
            return Size::getList(['where'=>"id in ($imp)", 'orderBy'=>"us_size"]);
        } else {
            return [];
        }
    }

    public function getMainCategory()
    {
        $productCats = Product_Category::getList(['where' => ['product_id' => $this->id]]);

        if(!$productCats){
            return null;
        }
        foreach($productCats as $productCat){

            $category = Category::getItem($productCat->category_id);
            if($category && $category->parent_category == 0){
                return $category;
            }
        }

        return null;
    }

    public function relatedProducts($number = 6)
    {
        $count = 1;
        $result = [];
        if(!$this->related_products){
            return null;
        }
        $relatedProductCats = json_decode($this->related_products,true);
        foreach($relatedProductCats as $cat){
            if( $pro = Product::getItem($cat)) {
                $result[] = $pro;
                $count++;
            }
            if($count > $number){
                break;
            }
        }

        return $result;
    }

    public function getLinkedProducts(){
        if($this->linked_list){
            return array_map(function($item){return Product::getItem($item);},json_decode($this->linked_list,true));
        } else {
            return [];
        }
    }

    public static function changeLink($linkedListArray,$product_id){
        $product = self::getItem($product_id);
        $linked = $linkedListArray;
        array_push($linked,$product_id);
        //change product and linked products links
        if($product) {
            if ($diff = array_diff(json_decode($product->linked_list, true), $linked)) {
                foreach ($diff as $d) {
                    $product = self::getItem($d);
                    $arr = [];
                    foreach ($diff as $dAgain) {
                        if ($dAgain != $d) {
                            $arr[] = $dAgain;
                        }
                    }
                    $product->linked_list = json_encode($arr);
                    $product->save();
                }
            }
            foreach ($linked as $ll) {
                $product = self::getItem($ll);
                $arr = [];
                foreach ($linked as $llAgain) {
                    if ($llAgain != $ll) {
                        $arr[] = $llAgain;
                    }
                }
                $product->linked_list = json_encode($arr);
                $product->save();
            }
            //change unlinked products links

            //remove a products link to all linked items
            if ($linkedListArray == []) {
                foreach (json_decode($product->linked_list, true) as $value) {
                    $prod = self::getItem($value);
                    $arr = json_decode($prod->linked_list, true);
                    unset($arr[array_search($product_id, $arr)]);
                    $prod->linked_list = json_encode($arr);
                    $prod->save();
                }
                $product->linked_list = json_encode([]);
                $product->save();
            }
        }
    }

    public static function topSellers($limit = 5){
        global $emagid;
        $db = $emagid->getDb();
        $sql = "select product_id, count(*) from order_products where product_id > 0 group by 1 order by count desc limit $limit;";
        return $db->execute($sql);
    }

    public function isCouponBlocked(){
        $categories = array_map(function($catId){return Category::getItem($catId);},Product_Category::get_product_categories($this->id));
        foreach($categories as $category){
            if($category->coupon_block){
                return true;
            }
        }
        return false;
    }
}