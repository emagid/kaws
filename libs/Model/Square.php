<?php

namespace Model;

use GuzzleHttp\Client;
use SquareConnect\Api\LocationApi;

class Square {
    private $accessToken;
    private $http;

    public function __construct(){
        $this->accessToken = SQUARE_KEY;
        $this->http = new Client([
            'base_uri' => 'https://connect.squareup.com/v1'
        ]);
        $this->requestHeader = ['headers' => ['Authorization' => "Bear {$this->accessToken}"]];
    }

    /**
     * @return \SquareConnect\Model\Location[]
     */
    public function listLocations(){
        $app = new LocationApi();
        $result = $app->listLocations($this->accessToken);
        return $result->getLocations();
    }

    public function listItems()
    {
        $response = $this->http->request('GET', '/8569PCK5R8FR0/items', $this->requestHeader);
        return $response->getBody();
    }


    public function listVariationInventory()
    {

    }

}