<?php

namespace Model;

use Emagid\Core\Model;

class Lotto_Entry extends Model{
    static $tablename = 'lotto_entries';
    static $fields = [
        'insert_time',
        'campaign_id',
        'product_id',
        'user_id',
        'winner',
        'redeemed',
        'ref_num',
        'email_date',
        'claimed_date'
    ];

    public function user(){
        return User::getItem($this->user_id);
    }

    public function swatch(){
        return $this->swatch ?
            '/content/uploads/colors/' . $this->swatch :
            FRONT_IMG . 'color_swatch_' . strtolower(str_replace(" ", "_", $this->name)) . '.png';
    }

    public static function search($terms, $limit = 100){
        $sql = "select id,name,slug from color where active = 1 and ";
        if(is_numeric($terms)){
            $sql .= "id = $terms or ";
        }

        $slug = preg_replace('/[^\w-]/','-',strtolower($terms));
        $slug = preg_replace('/[-]+/','-',$slug);
        $sql .= "(lower(name) like '%".strtolower(urldecode($terms))."%' or lower(slug) like '%".strtolower(urldecode($slug))."%') limit $limit";

        return self::getList(['sql'=>$sql]);
    }
}