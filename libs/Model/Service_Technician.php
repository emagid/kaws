<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 3/21/17
 * Time: 3:01 PM
 */

namespace Model;

class Service_Technician extends \Emagid\Core\Model
{
    static $tablename = "service_technician";
    public static $fields = [
        'id',
        'active',
        'insert_time',
        'service_ticket_id',
        'comment'
    ];
}