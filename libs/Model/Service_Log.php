<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 3/21/17
 * Time: 3:01 PM
 */

namespace Model;

class Service_Log extends \Emagid\Core\Model
{
    static $tablename = "service_log";
    public static $fields = [
        'id',
        'active',
        'insert_time',
        'service_ticket_id',
        'action'
    ];

    static $ACTION = ['CREATED', 'ADD ESTIMATE', 'ESTIMATE APPROVED', 'PAID', 'PAID FAILED'];
}