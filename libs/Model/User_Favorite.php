<?php

namespace Model;

use Emagid\Core\Model;

class User_Favorite extends Model {
    static $tablename = 'user_favorite';
    static $fields = [
        'active',
        'user_id',
        'product_id'
    ];

    public static function changeFavorite($userId,$productId){
        $sql = "SELECT * FROM user_favorite where user_id = {$userId} and product_id = {$productId}";
        $item = self::getItem(null,['sql'=>$sql]);
        if($item){
            $item->active = $item->active ? $item->active = 0: $item->active = 1;
        } else {
            $item = new self;
            $item->user_id = $userId;
            $item->product_id = $productId;
        }
        if($item->save()){
            return true;
        } else {
            return false;
        }
    }
}