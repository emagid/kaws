<?php

namespace Model;

use Emagid\Core\Model;

class Receiving extends Model{
	static $tablename = 'receiving';
	static $fields = ["product_id",
		"part_no",
		"ean",
		"upc",
		"pi_num",
		"pi_name",
		"code",
		"qty",
		"tracking_no",
		"shipping_date",
		"status",
		"unit_price"];
	static $status = ['Incomplete','Complete']; /** Replaced by Inventory Status */
}