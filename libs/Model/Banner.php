<?php 

namespace Model;

class Banner extends \Emagid\Core\Model {
	static $tablename = 'banner';

	public static $fields = [
		'title',
		'image',
		'description',
		'url',
		'options',
		'order_num',
		'featured',
		'banner_type',
//		'alignment',
		'text_color',
        'tagline',
        'class',
        'image_alt'
	];

	static $alignment = [1=>'Left',2=>'Center',3=>'Right'];
	static $text_color = [1=>'White',2=>'Black'];
}