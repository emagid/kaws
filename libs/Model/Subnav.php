<?php

namespace Model;

use Emagid\Core\Model;

class Subnav extends Model{
    static $tablename = 'subnav';
    static  $fields = [
        'category_id',
        'sub_category_id',
        'image',
        'display_order',
        'column_num',
        'url',
        'image_alt'
    ];
}