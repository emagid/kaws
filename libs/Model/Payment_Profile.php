<?php

namespace Model;

class Payment_Profile extends \Emagid\Core\Model {
	static $tablename = 'payment_profile';

	public static $fields = [
		'user_id',
		'first_name',
		'last_name',
		'phone',
		'address'=>['required'=>true],
		'address2',
		'city'=>['required'=>true],
		'state'=>['required'=>true],
		'zip'=>['required'=>true, 'type'=>'numeric'],
		'country'=>['required'=>true],
		'cc_number'=>['type'=>'numeric'],
		'cc_expiration_month'=>['type'=>'numeric'],
		'cc_expiration_year'=>['type'=>'numeric'],
		'cc_ccv'=>['type'=>'numeric'],
		'label'
	];
}