<?php

namespace Model;

use Carbon\Carbon;

class Coupon extends \Emagid\Core\Model
{

    static $tablename = "coupon";
    public static $fields = [
        'name',
        'code' => ['unique' => true],
        'discount_type' => ['type' => 'numeric', 'required' => true, 'name' => 'Discount Type'], //1 - $, 2 - %, 3 - shipping
        'discount_amount' => ['type' => 'numeric', 'required' => true, 'name' => 'Discount Amount'],
        'min_amount' => ['type' => 'numeric', 'required' => true, 'name' => 'Minimum Amount'],
        'num_uses_all' => ['type' => 'numeric', 'required' => true, 'name' => 'Number of uses'],
        'uses_per_user' => ['type' => 'numeric', 'name' => 'Uses per user'],
        'description',
        'display',
        'start_time',
        'end_time',
        'active_categories',
        'active_products',
        'active_clearance',
        'active_shipping'
    ];

    public function applyCoupon($value, $ship_id = null)
    {
        if($this->discount_type == 1){
            return $value - $this->discount_amount;
        } else if($this->discount_type == 2) {
            return $value * (1 - ($this->discount_amount / 100));
        } else {
            $disc = Shipping_Method::getItem($ship_id);
            $disc = json_decode($disc->cost,true)[0];
            return $value - $disc;
        }
    }

    public function getDiscountAmount($value, $ship_id = null){
        if($this->discount_type == 1){
            return $this->discount_amount;
        } else if($this->discount_type == 2) {
            return $value * ($this->discount_amount / 100);
        } else {
            $disc = Shipping_Method::getItem($ship_id);
            $disc = json_decode($disc->cost,true)[0];
            return $disc;
        }
    }

    public static function checkDiscount(){
        //Apply newsletter coupon
//        if(isset($_SESSION['newsletterDiscount']) && $_SESSION['newsletterDiscount']){
//            $newsletter = Newsletter::getItem(null,['where'=>['email'=>$_SESSION['newsletterDiscount'], 'is_expired'=>1]]);
//            return $newsletter;
//        }
        //Apply regular coupon
        if(isset($_SESSION['coupon']) && $_SESSION['coupon']){
            $carbon = time();
            $coupon = Coupon::getItem(null,['where'=>"lower(code) = lower('{$_SESSION['coupon']}') and start_time < '$carbon' and end_time > '$carbon'"]);
            if(Order::getCount(['where'=>['coupon_code'=>$coupon->code, 'active'=>1]]) < $coupon->num_uses_all || $coupon->num_uses_all == 0 ){
                if($coupon->discount_type == 3){
                    if( isset($_SESSION['ship_id']) && in_array($_SESSION['ship_id'],json_decode($coupon->active_shipping,true)) === false ){
                        return null;
                    }
                }
                return $coupon;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}
