<?php

namespace Model;

class Product_Category extends \Emagid\Core\Model {
  
  static $tablename = "product_categories";
  
  public static $fields = [
    'product_id',
    'display_order',
    'category_id'
//    'image',
  ];
  
  static $relationships = [
  	[
  		'name'=>'product',
  		'class_name' => '\Model\Product',
  		'local'=>'product_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  	[
  		'name'=>'category',
  		'class_name' => '\Model\Category',
  		'local'=>'category_id',
  		'remote'=>'id',
      	'relationship_type' => 'one'
  	],
  ];

  public static function get_product_categories($product_id, $limit = "") {
    $pc = self::getList(['where'=>"product_id={$product_id} AND active=1", 'limit'=>$limit]);
    $arr = [];
    foreach($pc as $c) {
      $arr[] = $c->category_id;
    }
    return $arr;
  }

  public static function get_category_products($category_id, $limit = "", $except = "",$type = "") {
    $where = "category_id={$category_id} AND active=1 ";
  	if ($except != ""){
  		$where .= $except;
  	}
    $pc = self::getList(['where'=>$where, 'limit'=>$limit]);
    $list = [];
    foreach($pc as $c) {
        $p = \Model\Product::getItem($c->product_id);
        if($type != ""){
            if($p->type != $type){
                continue;
            }
        }
      $list[] = $p;
    }

    return $list;
  }


}
