<?php

namespace Model;

use Emagid\Core\Model;

class Review extends Model{
    static $tablename = 'review';
    static $fields = [
        'product_id',
        'rating',
        'name',
        'email',
        'title',
        'approved',
        'body',
        'helpful'=>['type'=>'numeric'],
        'unhelpful'=>['type'=>'numeric']
    ];
}