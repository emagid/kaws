<?php
namespace Model;

use Carbon\Carbon;

class Combo extends \Emagid\Core\Model
{

    public static $tablename = "combo";

    public static $fields = [
        'name' => ['required' => true],
        'price' => ['type' => 'numeric', 'required' => true],
        'description',
        'msrp',
        'product_id',
        'quantity',
        'slug' => ['required' => true, 'unique' => true],
        'meta_title',
        'meta_keywords',
        'meta_description',
        'featured_image',
        'availability', // TODO st-dev: replaced day_delivery with availability
        'video_link'

    ];
    public function videoLink()
    {
        if($this->video_link){
            return UPLOAD_URL.'combo/'.$this->video_link;
        }

        return false;
    }
    public function getAvailability(){
        return Product::$availability[$this->availability];
    }
}