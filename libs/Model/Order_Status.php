<?php

namespace Model;

use Emagid\Core\Model;

class Order_Status extends Model{
    static $tablename = 'order_status';
    static $fields = [
        'name',
        'message',
        'subject',
        'mailtitle'
    ];
}