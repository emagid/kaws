<?php 

namespace Model; 

class Faq extends \Emagid\Core\Model {

	static $tablename = 'faq';
    static $category =[1=>'Privacy Policy',2=>'Return and Shipping Policy'];
	
	public static $fields =  [
		'question',
		'answer',
		'display_order',
		'category'
	];
	
}