$(document).ready(function(){


	var scrollTop = $(window).scrollTop();
	$(window).scroll(function() {


		var heroHeight = $('section.productDetailMainHero').height();

		// var allVideosTop = $('.productDetailSection.videosSection').offset().top;
		var actionBoxHeight = $('.productDetailsActionBox').height();
		var actionBoxVariationsHeight = $('.productDetailsActionBox > .variations').height();
		var actionBoxReleaser=(allVideosTop - actionBoxHeight - 48);
		var actionBoxPosAbs = actionBoxReleaser - heroHeight;
		var scrollTop = $(window).scrollTop();


		if(scrollTop > (heroHeight - 58)){
//			$('.productDetailsActionBox').addClass('sticky');
		}else{
//			$('.productDetailsActionBox').removeClass('sticky');
		}

		if(scrollTop > (heroHeight + 7)){
			$('.productDetailsSubHeader.mainLinksListWrapper').addClass('sticky');
		}else{
			$('.productDetailsSubHeader.mainLinksListWrapper').removeClass('sticky');
		}

		if(scrollTop > actionBoxReleaser){
			$('.stickyElementsWrapper.mainContentWidth').addClass('');
			$('.productDetailsActionBox.sticky').css({'top':actionBoxPosAbs});
		}else{
			$('.stickyElementsWrapper.mainContentWidth').removeClass('');
			$('.productDetailsActionBox.sticky').css({'top':'0px'});
		}


	    $(".productDetailSection").each(function() {
	    	var number = $(this).attr("data-vertical_screen");
	    	var $related_nav_toggle=$("ul.productDetailsSubHeader_Links li a[data-vertical_screen_nav="+number+"]");
	    	var top_position=$(this).offset().top - 77;
	    	var height = $(this).height();
	    	var bottom_position=top_position+height;
	    	var pageHeight = $(document).height();
	    	var windowHeight = $(window).height();
	    	var footerHeight = $(".mainFooterWrapper").height();
	    	var pageFooterDifference = pageHeight - footerHeight;
	    	var scrollPageFooterDiff = pageFooterDifference - scrollTop;

	    	if(scrollTop>top_position&&scrollTop<bottom_position){
	    		$("ul.productDetailsSubHeader_Links li a").removeClass("activePageSection");
	    		$related_nav_toggle.addClass("activePageSection");
	    	}


		});
	 });


	$(document).on('click','.scrollTrigger',function(e){
		e.preventDefault();
		var number = $(this).attr("data-vertical_screen_nav");
		var $related_section=$(".productDetailSection[data-vertical_screen="+number+"]");
		goToByScroll($related_section); 
	});


	var heroVideos = new Swiper('.swiperContainer-productHero-videos', {
		loop: false,
		onlyExternal:true,
		slidesPerView:3,
		centeredSlides:true,
		loopedSlides:3,
		loopAdditionalSlides:3,
		spaceBetween:20,
		nextButton:'.swiper-button-next',
		prevButton:'.swiper-button-prev'      
    }); 

    var allVideosSwiper = new Swiper('.swiperContainer-allVideos', {
		loop: true,
		slidesPerView:6,
		spaceBetween:20,
		nextButton:'.swiper-button-next',
		prevButton:'.swiper-button-prev',
		breakpoints: {
			480: {
			  slidesPerView:1,
			},
			510: {
			  slidesPerView:2,
			},
			// when window width is <= 480px
			690: {
			  slidesPerView:3,
			},
			768: {
			  slidesPerView:4,
			},
			// when window width is <= 640px
			1080: {
			  slidesPerView:5,
			}
		}		    
    });

	var mainProductImagesSwiper = new Swiper('.swiperContainer-mainProductImages', {
      loop: true,
      fade: {
		  crossFade: true
	  },
      effect:'fade',
      nextButton:'.swiper-button-next',
      prevButton:'.swiper-button-prev'      
    });    

	function goToByScroll($related_section){
		var scrollToPosition = $related_section.offset().top - 58;
	    $('html,body').animate({
	        scrollTop: scrollToPosition},
	        '1400','easeOutSine');
	}


  $(".button").on("click", function() {

    var $button = $(this);
    var oldValue = $button.closest('.numbersRow').find("input").val();

    if ($button.hasClass('inc')) {
  	  var newVal = parseFloat(oldValue) + 1;
  	} else {
	   // Don't allow decrementing below zero
      if (oldValue > 0) {
        var newVal = parseFloat(oldValue) - 1;
	    } else {
        newVal = 0;
      }
	  }

    $button.closest('.numbersRow').find("input").val(newVal);
    if(newVal==1){
    	$('.button.dec').addClass('disabled');
    }else{
    	$('.button.dec').removeClass('disabled');
    }
  });

});


