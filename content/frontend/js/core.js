$(document).ready(function () {

    $(document).on("click",".searchInputWrapper",function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
        $('body').addClass('headerSearchFieldActive');
        $('.searchInputWrapper input.searchInput').focus();
    });
    $(document).on('click','body header #toggle',function(){
        $(this).closest('header').addClass("open");
        // $('.headerContent').toggle();
        // $('.mobile-toggle').toggle();
    });

    $(document).on('click','body header.open a',function(event){
        if($(this).attr('href') !== 'mobile-shop'){
            event.stopImmediatePropagation();
            $(this).closest('header').removeClass('open').addClass('closing').delay(500).queue(function(changeClasses){
                $(this).closest('header').removeClass('closing');
                changeClasses();
            });
        }
    });
    $(document).keyup(function(e) {
        if ((e.keyCode == 27) && ($('body header').hasClass('open'))) { // escape key maps to keycode `27`
            $('body header').removeClass('open').addClass('closing').delay(500).queue(function(changeClasses){
                $('body header').removeClass('closing');
                changeClasses();
            });         
        }
    });
    $(document).on("click","body.headerSearchFieldActive",function(event){
        $('body').removeClass('headerSearchFieldActive');
    });

    $(document).on('click','.collapsedSection a.expandParent',function(event){
        var collapsedSection = $(this).closest('.collapsedSection');
        collapsedSection.toggleClass('expanded');
        if(collapsedSection.hasClass('expanded')){
            $(this).html('View Less');
        } else {
            $(this).html('View All Specs');
        }
    });
    $(document).on('click','a.comparisonToggle',function(event){
        var $comparisonLinkWrapper = $(this).closest('.comparisonToggleWrapper').find('.comparisonLinkWrapper');
        var comparisonAmount = +($comparisonLinkWrapper.find('.count').text());
        if($(this).hasClass('activeComparison')){
            comparisonAmountNew=comparisonAmount-1;

            $(this).removeClass('activeComparison');
            $comparisonLinkWrapper.addClass('hidden');
            $(".comparisonLinkWrapper .count").each(function( index ) {
                $(this).text(comparisonAmountNew);
            });
        }else{
            var comparisonAmountNew=comparisonAmount+1;
            $(this).addClass('activeComparison');
            $comparisonLinkWrapper.removeClass('hidden');
            $(".comparisonLinkWrapper .count").each(function( index ) {
                $(this).text(comparisonAmountNew);
            });
        }
        $.post('/comparisons/up_comp',{product_id:$(this).data('id')});
    });

    $('#ccnumber').keyup(function() {
        console.log('asdasd');
        var card = '';
        var $credit_card_icons = $("#creditCardIcons");
        var cc_input = this.value;
        var cc_input_first = cc_input.substr(0, 1);
        var cc_input_first_two = cc_input.substr(0, 2);
        if((cc_input_first === "4") && (cc_input.length <= 19)){
            $credit_card_icons.attr("class","visa");
            card = "visa";
        } else if((cc_input_first === "3") && (cc_input.length <= 19)){
            if((cc_input_first_two === "34") || (cc_input_first_two === "37")){
                $credit_card_icons.attr("class","amex");
                card = "amex";
            }
            else{
                $credit_card_icons.attr("class","");
            }
        } else if((cc_input_first === "6") && (cc_input.length <= 19)){
            if((cc_input_first_two === "60") || (cc_input_first_two === "65") || (cc_input_first_two === "62")){
                $credit_card_icons.attr("class","disc");
                card = "disc";
            }
            else{
                $credit_card_icons.attr("class","");
            }
        } else if((cc_input_first === "5") && (cc_input.length <= 19)){
            if((cc_input_first_two === "51") || (cc_input_first_two === "52") || (cc_input_first_two === "53") || (cc_input_first_two === "54") || (cc_input_first_two === "55")){
                $credit_card_icons.attr("class","mc");
                card = "mc";
            }
            else{
                $credit_card_icons.attr("class","");
            }
        } else{
            $credit_card_icons.attr("class","");
        }
        window.dispatchEvent(new CustomEvent('cardChange', { detail: card }));
    });

    function textLength(value){
       var minLength = 20;
       if(value.length < minLength) return false;
       return true;
    }

    $('.modal_formWrapper .inputRow > input').keydown( function(e){
        if( $(this).val().length > 0 ) {
            $(this).addClass('containsText');
        } else{
            $(this).removeClass('containsText');
        }
    });

    $('.cartIcon').on('click',function(){
        $('.cartView').toggle();
    });

    $(document).on('change','#orderDetail [name=ref_num]',function (e) {
        var url = '/order/'+this.value;
        $(this).parent().attr('action',url);
    });

    $(document).on('click','.addToCart',function(){
        var self = $(this);
        var product_id = self.attr('data-product_id');
        // var product_color = self.attr('data-color_id');
        var quantity = self.parent().parent().find('[name=product-quantity]').val();
        quantity = quantity?quantity:1;
        // var color = $('.checkboxRadioModule');
        // var colorSelect = color.find('input:checked').val();
        // if(colorSelect=='on'){
        //     colorSelect = product_color;
        // }
        $.ajax({
            type:'POST',
            url:'/cart/add',
            data:{product_id:product_id,quantity:quantity},
            success:function (data) {
                if(data.status == 'success'){

                }
            },
            async:false
        });
        // $.post('/cart/add',{product_id:product_id,quantity:quantity},function(data){
        //     if(data.status == 'success'){
        //         $("body header .cartView .priceBar h6").html(data.total);
        //         $("body header .cartView .variations").html(data.cartHtml);
        //         if(self.hasClass('mainProd')){
        //             $('html,body').animate({scrollTop:$('.suggestedAccessories').offset().top},'slow');
        //             self.html('<p>Checkout</p>')
        //         } else if(self.hasClass('accessProd') || self.hasClass('buyNow')){
        //             $('html,body').animate({scrollTop:0},'slow');
        //         }
        //         //goog_report_conversion('/cart');
        //         //window.location.replace('/cart');
        //     }
        // })
        // if(self.hasClass('accessProd') || self.hasClass('buyNow')){
        //     $("body header .cartView").show();
        // }
    });

    $(document).on('click','#signout',function(){
        $.post('/login/logout','',function(){
            window.location.replace('/');
        })
    });

    $(document).on('change','.quantityValue',function(){
        var val = $(this).val();
        var cart_id = $(this).attr("data-cart_id");
        $.post('/cart/updateCart',{cart_id:cart_id,quantity:val},function(data){
            if(data.status == 'success'){
                window.location.reload();
            }
        })
    })

    $(document).on('click','#cart_popout .cartUpdate button',function(){
        var cart_id     = $(this).data("cart_id");
        var val         = $(this).data("quantity");
        var operation   = $(this).data('operation');

        if(val < 1) val= 0;

        $.post('/cart/updateCart',{cart_id:cart_id,quantity:val,dropdown:1,op:operation},function(data){
            if(data.status == 'success'){
                $('#cart_popout .cart_content').html(data.cartHtml).parent().slideDown(100);
                var total = 0;
                $('.header_cart_prod').each(function(){
                    total = total + $(this).data('quantity');
                });
                $('#header_cart_total').text(total);
            }
        });
    })

    $(document).on('click','#cart_popout .fa-remove',function(){
        $('#cart_popout').slideUp(100);
    });

    $(document).on('click','.expand',function(){
        $('#cart_popout').slideUp(100);
    });

    $(document).on('click','a.cartUpdate-btn',function(){
        var cart_id     = $(this).data("cart_id");
        var val         = $(this).data("quantity");
        var operation   = $(this).data('operation');

        switch (operation){
            case "inc":
                val = val+1;
                break;
            case "dec":
                val = val-1;
                if(val < 1) val = 0;
                break;
            case "remove":
                val = 0;
                break;
        }
        if(val){
            $.post('/cart/updateCart',{cart_id:cart_id,quantity:val,dropdown:1,op:operation},function(data){
                if(data.status == 'success'){
                    location.reload();
                }
            });
        } else {
            window.location = '/cart/remove_product/'+cart_id;
        }
    })

     $("div").on("click", ".checkboxUX", function (event) {
        event.stopImmediatePropagation()
        event.preventDefault();     	
        var $inner_checkbox = $(this).find("input");
        var $option_clicked = $(this);
        if($option_clicked.attr("id") === "paypalPayment") {
            if ($option_clicked.hasClass("selected")) {
                $(".creditCardPaymentFormSection").show();
                $(".creditCardPaymentFormSection").find("input, select").prop("disabled", false);
                $("input#creditCardPayBtn").show();

                $("#paypalPayBtnClicker").hide();
            } else {
                $(".creditCardPaymentFormSection").find("input, select").prop("disabled", true);
                $(".creditCardPaymentFormSection").hide();
                $("input#creditCardPayBtn").hide();
                $("#paypalPayBtnClicker").show();
            }
        }
        if ($option_clicked.hasClass("optionBoxRadio")) {
            var $option_wrapper = $option_clicked.closest(".radioOptionsModule");
            $option_wrapper.find(".optionBoxRadio").removeClass("selected");
            $option_wrapper.find(".optionBoxRadio").find("input").attr('checked', false);
        }

        if ($option_clicked.hasClass("selected")) {
            $option_clicked.removeClass("selected");
            if ($inner_checkbox.length > 0) {
                $inner_checkbox.attr('checked', false);
            }
        } else {
            $option_clicked.addClass("selected");
            if ($inner_checkbox.length > 0) {
                $inner_checkbox.attr('checked', true);
            }
        }
        var slug = $(this).attr('data-prod_slug');
         if(slug!=undefined && slug.length>0){
             window.location.href = "/products/"+ slug;
         }

     });


    // var reviewsSwiper = new Swiper('.reviewsSwiper-container', {
    //   loop: true,
    //   fade: {
    //       crossFade: true
    //   },
    //   autoplay:6500,
    //   effect:'fade',
    //   nextButton:'.swiper-button-next',
    //   prevButton:'.swiper-button-prev'      
    // });   
    // $('#headerSearch').autocomplete({
    //     minLength: 2,
    //     delay: 0,
    //     appendTo: '.searchResultsContent.searchResultsContentDesktop > ul',
    //     source: function(request,response){
    //         $.post('/home/query',{q:request.term}, function(data){
    //             response(data);
    //         })
    //     }
    // }).data('ui-autocomplete')._renderItem = function(ul,item){
    //     $(".searchWrapper.col .searchResultsDropdown").addClass("activeSearchResultsDropdown");
    //     return $( "<li></li>" )
    //         .data("item.autocomplete",item)
    //         .append( "<a href='"+item.value+"'><img width='100px' src='"+ item.image+"'><p>" + item.label + "</p></a>" )
    //         .appendTo(ul);
    // }


    $(".questionsSeachWrapper").on("keyup", "input.searchFaqField", function () {
        if ($(".searchFaqField").val().length >= 1) {
            $(".searchFaqField").closest(".questionsSeachWrapper").addClass("faqSearchActive");
        } else {
            $(".searchFaqField").closest(".questionsSeachWrapper").removeClass("faqSearchActive");
        }

    });        
    $('.searchFaqField').on('keyup',function(){
        var val = $(this).val();
        var qUI = $('.questionUI');
        $.each(qUI,function(i,e){
            var jE = $(e);
            var ques = jE.find('h4');
            var ans = jE.find('p');
            if(ques.html().toLowerCase().indexOf(val) >= 0 || ans.html().toLowerCase().indexOf(val) >= 0){
                jE.parent('.col').show();
            } else {
                jE.parent('.col').hide();
            }
        });
    });

    $('#mobile-shop').on('click', function(e){
        e.preventDefault();
        $('#mobile-category').toggle();
        $('.mobile-not-shop').toggle();
    })


    // HEADER SUBNAV SHOWS
    // show subnav
    $('.mainLinksListWrapper li').mouseover(function(){
        $($(this).children('.sub-menu')[0]).slideDown(100);
    });

    $('.mainLinksListWrapper li').mouseleave(function(){
        $($(this).children('.sub-menu')[0]).slideUp(100);
        $('.spark_color_choice').fadeOut(300);
        $('.sub-menu a:first-child img').css('display', 'block');
        $('.spark_color_choice img:last-child').css('display', 'block');
    });

    // show img
    $('.sub-menu a').mouseover(function(){
        $($(this).children('img')[0]).fadeIn(100);
    });

    $('.sub-menu a').mouseleave(function(){
        $($(this).children('img')[0]).fadeOut(100);
    });

    // show colors
    $('.spark_link').click(function(e){
        e.preventDefault();
        $($(this).next('.spark_color_choice')).fadeIn(300);
    });

    $('.color').mouseover(function(){
        $($('.color').children('img')).fadeOut(100);
        $($(this).children('img')).fadeIn(100);
    });



    // Redirect to Phatom 3 series scroll
    $('#redirect_slide').click(function(e){


        $(document).ready(function() {
                console.log(title)
                var title = $('h6.dji-text')[1];
                $('html,body').animate({
                    scrollTop: $(title).offset().top},
                'slow');
        });

    });


    // FIGURE SWITCH
    $('.thumbnails a').click(function(e){
        e.preventDefault();
        $('.thumbnails img').removeClass('selected');
        $($(this).children('img')).addClass('selected');
        var src = $($(this).children('img')).attr('src');
        // var img = "../assets/img/" + src.substring(29, src.length);
        $('.figures').css('background-image', "url('" + src + "')");
    });


    // OVERLAY POPUPS
    $('footer a').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        $('.' + id).fadeIn();
    });

    $('#lotto_button').click(function(e){
        $('.lotto.popup').fadeIn();
    });

    $('.off_overlay').click(function(){
        $('.popup').fadeOut();
    });


    // track order form
    $('.return_form .button').click(function(){
        $('#ticket').slideDown();
        $(this).slideUp();
    });



});
Date.prototype.toISOStringWithOffset = function() {
    var tzo = -this.getTimezoneOffset(),
        dif = tzo >= 0 ? '+' : '-',
        pad = function(num) {
            var norm = Math.abs(Math.floor(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return this.getFullYear() +
        '-' + pad(this.getMonth() + 1) +
        '-' + pad(this.getDate()) +
        'T' + pad(this.getHours()) +
        ':' + pad(this.getMinutes()) +
        ':' + pad(this.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
}
function buildNotification(type,message)
{
    var alert = '';
    if(type == 'message' || type == true){
        alert = ['success','Notification:'];
    } else {
        alert = ['danger','An Error Occurred:'];
    }
    var formattedMsg = message;
    var html = '';
    if(Array.isArray(message)){
        formattedMsg = '<ul>';
        for(i = 0; i < m.length; i++){
            entry = m[i];
            formattedMsg += '<li>';
            formattedMsg += $entry;
            formattedMsg += '</li>';
        }
        formattedMsg += '</ul>';
    }
    html += "<div class='alert alert-"+alert[0]+"'>";
    html += "<strong>"+alert[1]+" </strong>";
    html += formattedMsg;
    html += "</div>";

    if($('.mainPageContainer > div.notification').length == 0){
       $('.mainPageContainer').append('<div class="notification">');
    }
    $('.mainPageContainer > div.notification').html(html);
}






