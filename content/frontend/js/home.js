$(document).ready(function(){
	var homeBannerSwiper = new Swiper('.banner-swiper-container', {
      loop: true,
      nextButton:'.swiper-button-next',
      prevButton:'.swiper-button-prev'
    });
	var enterpriseSwiper = new Swiper('.swiper-container-enterprise', {
      loop: true,
      nextButton:'.swiper-button-next',
      prevButton:'.swiper-button-prev'      
    });    
});