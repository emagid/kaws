// admin login validation
$("#admin_login").validate({
    rules: {
        username:{
            required: true,
        },
        password:{
            required: true,
        }
    },
    messages:{
        username:{ 
            required:"Please enter your username",
        },
        password:{ 
            required:"Please enter your password",
        }
    }
});



// responsive footer height
var bumpIt = function() {
    $('body').css('margin-bottom', $('#footer').outerHeight());
},
didResize = true;
bumpIt();

$(window).resize(function() {
    didResize = true;
});
setInterval(function() {
    if(didResize) {
        didResize = true;
        bumpIt();
    }
}, 250);
