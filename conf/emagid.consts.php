<?php 
/** Setting up an EmagidPHP project . **/ 

define('SITE_NAME' , 'KAWSONE' );
define('SITE_EMAIL' , 'shop@kawsone.com' );


define("ROOT_DIR",__DIR__.'/../');
define('DS' , DIRECTORY_SEPARATOR);
define('SITE_DOMAIN' , $_SERVER['SERVER_NAME'] );

// $site_url = dirname($_SERVER['SCRIPT_NAME'])."/";
// if($site_url=='//') {$site_url='/';} 
$site_url = '/';
define('SITE_URL' , $site_url );
define('SITE_TEMPLATES' , SITE_URL . 'templates/modernvice/' );

// FRONTEND
define('FRONT_IMG' , SITE_URL . 'content/frontend/assets/img/' );
define('FRONT_VIDEO' , SITE_URL . 'content/frontend/assets/video/' );
define('FRONT_ASSETS' , SITE_URL . 'content/frontend/assets/' );
define('FRONT_JS' , SITE_URL . 'content/frontend/js/' );
define('FRONT_LIBS' , SITE_URL . 'content/frontend/libs/' );
define('FRONT_CSS' , SITE_URL . 'content/frontend/css/' );

// ADMIN
define('ADMIN_URL' , SITE_URL . 'admin/' );
define('ADMIN_IMG' , SITE_URL . 'content/admin/images/' );
define('ADMIN_JS' , SITE_URL . 'content/admin/js/' );
define('ADMIN_CSS' , SITE_URL . 'content/admin/css/' );
define('ADMIN_ICONS' , SITE_URL . 'content/admin/icons/' );

// UPLOAD
define('IMAGE_UPLOAD' , SITE_URL . '/content/uploads/' );
define('UPLOAD_URL' , SITE_URL . 'content/uploads/');
define('UPLOAD_PATH' , ROOT_DIR . DS. 'content'.DS.'uploads'.DS);

define('MIN_URL' , SITE_URL . 'content/media/min/');
define('MIN_PATH' , ROOT_DIR . DS.'content'.DS.'media'.DS.'min'.DS);

define('THUMB_URL' , SITE_URL . 'content/media/thumb/');
define('THUMB_PATH' , ROOT_DIR . DS.'content'.DS.'media'.DS.'thumb'.DS);

// kaws API
define('KAWS_API_HOST','http://kaws-env-prod.us-east-1.elasticbeanstalk.com/');

define('CRYPT_KEY' , 'emagidphp');//password to (en/de)crypt

//Sandbox
//define("AUTHORIZENET_API_LOGIN_ID", "3AJ2PJ94hf2");
//define("AUTHORIZENET_TRANSACTION_KEY", "8AsXzt79687G5N8u");
//define("AUTHORIZENET_SANDBOX", true);

//Live
define("AUTHORIZENET_API_LOGIN_ID", "3AJ2PJ94hf2");
define("AUTHORIZENET_TRANSACTION_KEY", "8AsXzt79687G5N8u");
define("AUTHORIZENET_SANDBOX", false);

define("ZOHO_AUTHTOKEN", "d6f6027ce68cec1b4b6de743d60a16a7");
define("MAILCHIMP_API", "115c42b5f8d01f94eb3ded31805cc579-us9");

//aws
define('AWS_ACCESS_KEY_ID', 'AKIAIIA26LCYGRM44TIA');
define('AWS_SECRET_ACCESS_KEY', '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');
define('S3_BUCKET', 'kaws-images');
define('S3_URL', 'https://s3.amazonaws.com/kaws-images/');

define('MANDRILL_KEY', 'dO56LPKlBOpL0dMtWq8_ug');


//PAYPAL - Sandbox
//define("PAYPAL_CLIENT_ID", "Acc99t6Gvyp2g__-M1zKm462wDTlQYLNB8e-wSJCvbwuteT5AacE9WbmVWpK7DZB4Q4GxqsFaovLdTAV");
//define("PAYPAL_CLIENT_SECRET", "EOxIabOd1WO4BiWv2a77dC36iTWNxbIDmcAA17eWO0uWy16uHnYIjyjnF4sHd2ePUq3Bj2Q4HZ1iasN_");

//Zendesk - DEV
//define("ZENDESK_API_TOKEN","ZHhNEEtQUYWV3otjvLcQrTtdxWT1O6gpnky8dJgj");
//define("ZENDESK_USERNAME","garrett@emagid.com");
//define("ZENDESK_SUBDOMAIN","emagid-dev");
define("ZENDESK_API_TOKEN","EcCgkdoNzz0ClNi8UVQEAURYykDtrasLGWzkJZse");
define("ZENDESK_USERNAME","accounts@kaws.com");
define("ZENDESK_SUBDOMAIN","kawsone");

//UPS
define('UPS_ACCESS_KEY','ED3B91939A422E08');
define('UPS_USER_ID','KAWSONE');
define('UPS_SECRET','KAWSstudio51!');


//reCaptcha
define("RECAPTCHA_SITE_KEY","6LdF4xIUAAAAADKgsUB02hWcFuPNQMKLt_IDj82j");
define("RECAPTCHA_SECRET_KEY","6LdF4xIUAAAAAJ646aCBl9YTAPly59P35N0j95ec");

define("SQUARE_APP","sq0idp-ojKAFze6RsCbPZSXATpFEQ");
define("SQUARE_KEY","sq0atp-GpNuPL3fqVvlE-b6KERgUQ");

define("SHIPSTATION_KEY","3dcb64ef1ea945438b409607d0cd2010");
define("SHIPSTATION_SECRET","4460f36bcb024496ae0970f5bbb4eb93");

//Key to match sku2u apicalls
define('SKU2U_MATCHKEY','281a4d0d3ff99c88cbf8e35265a551693b70261f51ecab806bb88e23dca7cfcd');

// image sizes
$image_sizes = [[100,100],[400,300],[1024,768]];