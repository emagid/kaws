<?php

require_once("emagid.payment.php");
include("emagid.consts.php");
include("emagid.db.php");

$site_routes = [
'routes'=>[
		[
		 'name' => 'login',
		 'area' => 'admin',
		 'template' => 'admin',
		 'pattern'=>"admin/login/{?action}",
		 'controller'=>'login',
		 'action'=>'login',
		],
		[
		 'name' => 'admin',
		 'area' => 'admin',
		 'template' => 'admin',
		 'pattern'=>"admin/{?controller}/{?action}/{?id}",
		 'controller'=>'dashboard',
		 'action'=>'index',
		 'authorize' => [
		 	'roles' => ['admin'],
		 	'login_url' => '/admin/login'
		 ]
		],
		[
		 'name' => 'page',
		 'pattern'=>"pages/{page_slug}",
		 'controller'=>'pages',
		 'action'=>'page'
		],
		[
		 'name' => 'shop',
		 'pattern'=>"shop/{page_slug}",
		 'controller'=>'home',
		 'action'=>'index'
		],
		[
		 'name' => 'shop',
		 'pattern'=>"shop",
		 'controller'=>'home',
		 'action'=>'index'
		],
		[
		 'name' => 'estimate',
		 'pattern'=>"service/estimate/{id}/{response}",
		 'controller'=>'service',
		 'action'=>'estimate'
		],
		[
		 'name' => 'logout',
		 'pattern'=>"logout",
		 'controller'=>'login',
		 'action'=>'logout',
		],
		[
		 'name' => 'article',
		 'pattern'=>"news/article/{article_slug}",
		 'controller'=>'news',
		 'action'=>'article'
		],
		[
		 'name' => 'category',
		 'pattern'=>"categories/{category_slug}/{?subcategory}",
		 'controller'=>'categories',
		 'action'=>'category'
		],
		[
			'name' => 'repair_service',
			'pattern'=>"repair_invoice/{?invoice_number}",
			'controller'=>'repair_invoice',
			'action'=>'index'
		],

        [
        'name' => 'all_category',
        'pattern'=>"all_accessories/{category_slug}",
        'controller'=>'categories',
        'action'=>'all'
        ],
        [
        'name' => 'accessory',
        'pattern'=>"accessories/{product_slug}",
        'controller'=>'accessories',
        'action'=>'accessory'
        ],
		[
		 'name' => 'brand',
		 'pattern'=>"brand/{brand_slug}",
		 'controller'=>'brands',
		 'action'=>'brand'
		],
		[
		 'name' => 'product',
		 'pattern'=>"products/{product_slug}",
		 'controller'=>'products',
		 'action'=>'product'
		],
		[
		 'name' => 'blog',
		 'pattern'=>"blog/{blog_slug}",
		 'controller'=>'blog',
		 'action'=>'blog'
		],
		[
		 'name' => 'blog',
		 'pattern'=>"blogs/{b_category_slug}",
		 'controller'=>'blog',
		 'action'=>'blog_category'
		],
		[
		 'name' => 'lottery redemption',
		 'pattern'=>"redeem/{redemption_slug}",
		 'controller'=>'lottery',
		 'action'=>'redeem'
		],
		[
		 'name' => 'order',
		 'pattern'=>"order/{order_id}",
		 'controller'=>'orders',
		 'action'=>'order'
		],
		[
		 'name' => 'sample sale',
		 'pattern'=>"sample-sale",
		 'controller'=>'clearance',
		 'action'=>'index'
		],
		[
		 'name' => '249 and under',
		 'pattern'=>"249-and-under",
		 'controller'=>'categories',
		 'action'=>'category',
		 'category_slug'=>'women',
		'subcategory'=>'sale'
		],
		[
			'name' => 'resetPassword',
			'pattern' => "reset/password/{hash}",
			'controller' => 'reset',
			'action' => 'password'
		]
	]
];

$emagid_config = array (
		'debug'=>true,          // When debug is enabled, Kint debugging plugin is enabled.
								//  you can use d($my_var) , dd($my_var) , Kint::Trace() , etc...
								//  documentation available here : http://raveren.github.io/kint/

		'root'=>SITE_URL,

		'template'=>'dji',  // template must be found in /templates/<template_name>/<template_name>.php //TODO change template
								// so in this example we will have /templates/default/default.php
								// please open the template file to see how the view is being rendered .

		'connection_string'=> array(
			'driver'   => DB_DRIVER,
            'db_name'  => DB_NAME,
            'username' => DB_USER,
            'password' => DB_PWD,
            'host'     => DB_HOST
        ),

        'include_paths'=>array(
        	'libs/Mandrill',
        	'libs/Mailchimp'
        ),

        'email'=> array(
        	'api_key' => 'dO56LPKlBOpL0dMtWq8_ug', //Emagid key
//        	'api_key' => 'R_06CiWixNdYm0O5hcfY_w', //MV key
        	'from' => array(
        		'email'	=>'shop@kawsone.com', //TODO change email || TODO st-dev validate email
        		'name'	=>'KAWSONE' //TODO change email || TODO st-dev validate email
        	)
        )
);
