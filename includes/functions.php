<?php

/**Format date and time functions*/
function db_to_time($datetime = ""){
	$unixdatetime = strtotime($datetime);
	$date_time = strftime("%m-%d-%Y | %I:%M %p", $unixdatetime);
	return strftime($date_time, '0 ');
		}//close db_to_time
		
		function db_to_date($datetime = ""){
			$unixdatetime = strtotime($datetime);
			return strftime("%B %d, %Y", $unixdatetime);
		}//close db_to_time
		
		function time_to_db($time=""){
			if($time == ""){$time = time();}
			$mysql_datetime = strftime("%Y-%m-%d %H:%M:%S", $time);
			return $mysql_datetime;
		}//close time to db
		
		function date_to_db($time=""){
			if($time == ""){$time = time();}
			$mysql_date = strftime("%Y-%m-%d", $time);
			return $mysql_date;
		}//close date to db
		
	/**
	* Function to calculate the same day one month in the future.
	*
	* This is necessary because some months don't have 29, 30, or 31 days. If the
	* next month doesn't have as many days as this month, the anniversary will be
	* moved up to the last day of the next month.
	*
	* @param $start_date (optional)
	*   UNIX timestamp of the date from which you'd like to start. If not given,
	*   will default to current time.
	*
	* @return $timestamp
	*   UNIX timestamp of the same day or last day of next month.
	*/
	function jjg_calculate_next_month($start_date = FALSE) {
		if ($start_date) {
		$now = $start_date; // Use supplied start date.
	} else {
		$now = time(); // Use current time.
	}
	
	  // Get the current month (as integer).
	$current_month = date('n', $now);
	
	  // If the we're in Dec (12), set current month to Jan (1), add 1 to year.
	if ($current_month == 12) {
		$next_month = 1;
		$plus_one_month = mktime(0, 0, 0, 1, date('d', $now), date('Y', $now) + 1);
	}
	  // Otherwise, add a month to the next month and calculate the date.
	else {
		$next_month = $current_month + 1;
		$plus_one_month = mktime(0, 0, 0, date('m', $now) + 1, date('d', $now), date('Y', $now));
	}
	
	$i = 1;
	  // Go back a day at a time until we get the last day next month.
	while (date('n', $plus_one_month) != $next_month) {
		$plus_one_month = mktime(0, 0, 0, date('m', $now) + 1, date('d', $now) - $i, date('Y', $now));
		$i++;
	}
	
	return $plus_one_month;
}

	/**
	* Function to calculate the same day one year in the future.
	*
	* @param $start_date (optional)
	*   UNIX timestamp of the date from which you'd like to start. If not given,
	*   will default to current time.
	*
	* @return $timestamp
	*   UNIX timestamp of the same day or last day of next month.
	*/
	function jjg_calculate_next_year($start_date = FALSE) {
		if ($start_date) {
		$now = $start_date; // Use supplied start date.
	} else {
		$now = time(); // Use current time.
	}
	$month = date('m', $now);
	$day = date('d', $now);
	$year = date('Y', $now) + 1;
	  $plus_one_year = strtotime("$year-$month-$day"); // Use ISO 8601 standard.
	  return $plus_one_year;
	}

	
	/**
	* Password hash with crypt
	* @param string $info
	* @param string $encdata 
	*/	
	function hasher($info, $encdata = false) { 
		$strength = "08";
				//check password
				//if encrypted data (the password in database) is passed, check it against input password ($info) 
		if ($encdata) { 
			if (substr($encdata, 0, 60) == crypt($info, "$2a$".$strength."$".substr($encdata, 60))) { 
				return true; 
			}else { 
				return false; 
			} 
		}else{
				//create new hashed password
				//make a salt and hash it with input password, and add salt to end 
			$salt = ""; 
			for ($i = 0; $i < 22; $i++) { 
				$salt .= substr("./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", mt_rand(0, 63), 1); 
			} 
				//return 82 char string (60 char hash & 22 char salt) 
			return crypt($info, "$2a$".$strength."$".$salt).$salt; 
		} 
	}


	/**
	* limited output to desired num of words
	* @param string $content
	* @param number/int $num_words 
	*/	
	function limited_the_content($content, $num_words = 200){
		$stripped_content = strip_tags($content);
		$words = explode(' ', $stripped_content);
		if(count($words) < $num_words){
			echo $stripped_content;
		}else{
			$words = array_slice($words, 0, $num_words);
					//echo implode(' ', $words) . '...';
			echo implode(' ', $words);	
				}//end if
			}

	/**
	* output human readabel status
	* @param string/number $status
	*/	
	function get_status($status){

		switch ($status) {
			case 1:
			return "activated";
			break;
			case 0:
			return "deactivated";
			break;
			case 2:
			return "quick link";
			break;
			
			default:
					# code...
			break;
		}
	}
	
		/**
	* add a element to the beginning of the array
	* @param array $myarray
	* @param ***   $value
	* @param number/int... $position
	*/	
	function array_insert($myarray,$value,$position=0){
		$fore=($position==0)?array():array_splice($myarray,0,$position);
		$fore[]=$value;
		$ret=array_merge($fore,$myarray);
		return $ret;
	}		


/////////////////////Get countries and US/Canadian states
	function get_states($canada = false){
	    if($canada){
            $states = array(
                'AB' => 'Alberta',
                'BC' => 'British Columbia',
                'MB' => 'Manitoba',
                'NB' => 'New Brunswick',
                'NF' => 'Newfoundland',
                'NT' => 'Northwest Territory',
                'NS' => 'Nova Scotia',
                'NU' => 'Nunavut',
                'ON' => 'Ontario',
                'PE' => 'Prince Edward Island',
                'QC' => 'Québec',
                'SK' => 'Saskatchewan',
                'YT' => 'Yukon Territory');
        } else {
            $states = array(
                'AL' => 'Alabama',
                'AK' => 'Alaska',
                'AZ' => 'Arizona',
                'AR' => 'Arkansas',
                'CA' => 'California',
                'CO' => 'Colorado',
                'CT' => 'Connecticut',
                'DE' => 'Delaware',
                'FL' => 'Florida',
                'GA' => 'Georgia',
                'HI' => 'Hawaii',
                'ID' => 'Idaho',
                'IL' => 'Illinois',
                'IN' => 'Indiana',
                'IA' => 'Iowa',
                'KS' => 'Kansas',
                'KY' => 'Kentucky',
                'LA' => 'Louisiana',
                'ME' => 'Maine',
                'MD' => 'Maryland',
                'MA' => 'Massachusetts',
                'MI' => 'Michigan',
                'MN' => 'Minnesota',
                'MS' => 'Mississippi',
                'MO' => 'Missouri',
                'MT' => 'Montana',
                'NE' => 'Nebraska',
                'NV' => 'Nevada',
                'NH' => 'New Hampshire',
                'NJ' => 'New Jersey',
                'NM' => 'New Mexico',
                'NY' => 'New York',
                'NC' => 'North Carolina',
                'ND' => 'North Dakota',
                'OH' => 'Ohio',
                'OK' => 'Oklahoma',
                'OR' => 'Oregon',
                'PA' => 'Pennsylvania',
                'RI' => 'Rhode Island',
                'SC' => 'South Carolina',
                'SD' => 'South Dakota',
                'TN' => 'Tennessee',
                'TX' => 'Texas',
                'UT' => 'Utah',
                'VT' => 'Vermont',
                'VA' => 'Virginia',
                'WA' => 'Washington',
                'WV' => 'West Virginia',
                'WI' => 'Wisconsin',
                'WY' => 'Wyoming',
                'DC' => 'District of Columbia',
                'GU' => 'Guam',
                'PR' => 'Puerto Rico',
                'VI' => 'Virgin Islands');
        }
        return $states;
    }

function get_countries($uscanada = false){
	$countries = array(
        'US' => 'United States',
//	      'AL' => 'Albania',
//        'DZ' => 'Algeria',
//        'AS' => 'American Samoa',
//        'AD' => 'Andorra',
//        'AO' => 'Angola',
//        'AI' => 'Anguilla',
//        'AG' => 'Antigua & Barbuda',
//        'AR' => 'Argentina',
//        'AM' => 'Armenia',
//        'AW' => 'Aruba',
        'AU' => 'Australia',
        'AT' => 'Austria',
//        'AZ' => 'Azerbaijan',
//        'PT' => 'Azores',
//        'BS' => 'Bahamas',
//        'BH' => 'Bahrain',
//        'BD' => 'Bangladesh',
//        'BB' => 'Barbados',
//        'BY' => 'Belarus',
        'BE' => 'Belgium',
//        'BZ' => 'Belize',
//        'BJ' => 'Benin',
//        'BM' => 'Bermuda',
//        'BT' => 'Bhutan',
//        'BO' => 'Bolivia',
//        'BQ' => 'Bonaire',
//        'BA' => 'Bosnia and Herzegovina',
//        'BW' => 'Botswana',
//        'BR' => 'Brazil',
//        'VG' => 'British Virgin Isles',
//        'BN' => 'Brunei',
//        'BG' => 'Bulgaria',
//        'BF' => 'Burkina Faso',
//        'BI' => 'Burundi',
//        'KH' => 'Cambodia',
//        'CM' => 'Cameroon',
        'CA' => 'Canada',
        'CN' => 'Peoples Rep of China',
//        'IC' => 'Canary Islands',
//        'CV' => 'Cape Verde',
//        'KY' => 'Cayman Islands',
//        'CF' => 'Central African Rep',
//        'TD' => 'Chad',
//        'CL' => 'Chile',
//        'CO' => 'Colombia',
//        'CG' => 'Congo',
//        'CD' => 'Congo, Democratic Rep',
//        'CK' => 'Cook Islands',
//        'CR' => 'Costa Rica',
//        'CI' => 'Cote d\' Ivoire',
//        'HR' => 'Croatia',
//        'CB' => 'Curacao',
//        'CY' => 'Cyprus',
        'CZ' => 'Czech Republic',
        'DK' => 'Denmark',
//        'DJ' => 'Djibouti',
//        'DM' => 'Dominica',
//        'DO' => 'Dominican Republic',
//        'EC' => 'Ecuador',
//        'TL' => 'East Timor',
//        'EG' => 'Egypt',
//        'SV' => 'El Salvador',
//        'EN' => 'England',
//        'GQ' => 'Equatorial Guinea',
//        'ER' => 'Eritrea',
//        'EE' => 'Estonia',
//        'ET' => 'Ethiopia',
//        'FO' => 'Faeroe Islands',
//        'FJ' => 'Fiji',
        'FI' => 'Finland',
        'FR' => 'France',
//        'GF' => 'French Guiana',
//        'PF' => 'French Polynesia',
//        'GA' => 'Gabon',
//        'GM' => 'Gambia',
//        'GE' => 'Georgia',
        'DE' => 'Germany',
//        'GH' => 'Ghana',
//        'GI' => 'Gibraltar',
        'GR' => 'Greece',
//        'GL' => 'Greenland',
//        'GD' => 'Grenada',
//        'GP' => 'Guadeloupe',
//        'GU' => 'Guam',
//        'GT' => 'Guatemala',
//        'GG' => 'Guernsey',
//        'GN' => 'Guinea',
//        'GW' => 'Guinea-Bissau',
//        'GY' => 'Guyana',
//        'HT' => 'Haiti',
//        'NL' => 'Holland',
//        'HN' => 'Honduras',
        'HK' => 'Hong Kong',
        'HU' => 'Hungary',
        'IS' => 'Iceland',
        'IE' => 'Republic of Ireland',
//        'IN' => 'India',
//        'ID' => 'Indonesia',
//        'IQ' => 'Iraq',
        'IL' => 'Israel',
        'IT' => 'Italy',
//        'JM' => 'Jamaica',
        'JP' => 'Japan',
//        'JE' => 'Jersey',
//        'JO' => 'Jordan',
//        'KZ' => 'Kazakhstan',
//        'KE' => 'Kenya',
//        'KI' => 'Kiribati',
//        'FM' => 'Kosrae',
//        'KW' => 'Kuwait',
        'KR' => 'South Korea',
//        'KG' => 'Kyrgyzstan',
//        'LA' => 'Laos',
//        'LV' => 'Latvia',
//        'LB' => 'Lebanon',
//        'LS' => 'Lesotho',
//        'LR' => 'Liberia',
//        'LI' => 'Liechtenstein',
//        'LT' => 'Lithuania',
        'LU' => 'Luxembourg',
//        'MO' => 'Macau',
//        'MK' => 'Macedonia',
//        'MG' => 'Madagascar',
//        'PT' => 'Madeira',
//        'MW' => 'Malawi',
//        'MY' => 'Malaysia',
//        'MV' => 'Maldives',
//        'ML' => 'Mali',
//        'MT' => 'Malta',
//        'MH' => 'Marshall Islands',
//        'MQ' => 'Martinique',
//        'MR' => 'Mauritania',
//        'MU' => 'Mauritius',
//        'MX' => 'Mexico',
//        'FM' => 'Micronesia',
//        'MD' => 'Moldova',
//        'MC' => 'Monaco',
//        'MN' => 'Mongolia',
//        'ME' => 'Montenegro',
//        'MS' => 'Montserrat',
//        'MA' => 'Morocco',
//        'MZ' => 'Mozambique',
//        'NA' => 'Namibia',
//        'NP' => 'Nepal',
        'NL' => 'Netherlands',
//        'AN' => 'Netherlands Antilles',
//        'NC' => 'New Caledonia',
        'NZ' => 'New Zealand',
//        'NI' => 'Nicaragua',
//        'NE' => 'Niger',
//        'NG' => 'Nigeria',
//        'NF' => 'Norfolk Island',
//        'NB' => 'Northern Ireland',
//        'MP' => 'N. Mariana Islands',
        'NO' => 'Norway',
//        'OM' => 'Oman',
//        'PK' => 'Pakistan',
//        'PW' => 'Palau',
//        'PA' => 'Panama',
//        'PG' => 'Papua New Guinea',
//        'PY' => 'Paraguay',
//        'PE' => 'Peru',
//        'PH' => 'Philippines',
//        'PL' => 'Poland',
//        'FM' => 'Ponape',
        'PT' => 'Portugal',
        'PR' => 'Puerto Rico',
        'QA' => 'Qatar',
//        'CG' => 'Republic of Congo',
//        'YE' => 'Republic of Yemen',
//        'RE' => 'Reunion',
//        'RO' => 'Romania',
//        'MP' => 'Rota',
//        'RU' => 'Russia',
//        'RW' => 'Rwanda',
//        'AN' => 'Saba',
//        'MP' => 'Saipan',
//        'WS' => 'Samoa',
//        'SM' => 'San Marino',
//        'SA' => 'Saudi Arabia',
//        'GB' => 'Scotland',
//        'SN' => 'Senegal',
//        'RS' => 'Serbia',
//        'SC' => 'Seychelles',
//        'SL' => 'Sierra Leone',
        'SG' => 'Singapore',
//        'SK' => 'Slovakia',
//        'SI' => 'Slovenia',
//        'SB' => 'Solomon Islands',
//        'ZA' => 'South Africa',
        'ES' => 'Spain',
//        'LK' => 'Sri Lanka',
//        'AN' => 'St. Barthelemy',
//        'KN' => 'St. Christopher',
//        'VI' => 'St. Croix',
//        'AN' => 'St. Eustatius',
//        'VI' => 'St. John',
//        'KN' => 'St. Kitts & Nevis',
//        'LC' => 'St. Lucia',
//        'AN' => 'St. Maarten',
//        'GP' => 'St. Martin',
//        'VI' => 'St. Thomas',
//        'VC' => 'St Vincent/Grenadine',
//        'SR' => 'Suriname',
//        'SZ' => 'Swaziland',
        'SE' => 'Sweden',
        'CH' => 'Switzerland',
//        'PF' => 'Tahiti',
        'TW' => 'Taiwan',
//        'TJ' => 'Tajikistan',
//        'TZ' => 'Tanzania',
        'TH' => 'Thailand',
//        'MP' => 'Tinian',
//        'TG' => 'Togo',
//        'TO' => 'Tonga',
//        'VG' => 'Tortola',
//        'TT' => 'Trinidad & Tobago',
//        'FM' => 'Truk',
//        'TN' => 'Tunisia',
//        'TR' => 'Turkey',
//        'TM' => 'Turkmenistan',
//        'TC' => 'Turks & Caicos Isles',
//        'TV' => 'Tuvalu',
//        'UG' => 'Uganda',
//        'UA' => 'Ukraine',
//        'VC' => 'Union Island',
//        'AE' => 'United Arab Emirates',
        'GB' => 'United Kingdom',
//        'UY' => 'Uruguay',
//        'VI' => 'US Virgin Islands',
//        'UZ' => 'Uzbekistan',
//        'VU' => 'Vanuatu',
//        'VA' => 'Vatican City State',
//        'VE' => 'Venezuela',
//        'VN' => 'Vietnam',
//        'VG' => 'Virgin Gorda',
//        'GB' => 'Wales',
//        'WF' => 'Wallis & Futuna Isle',
//        'FM' => 'Yap',
//        'ZM' => 'Zambia',
//        'ZW' => 'Zimbabwe'
    );

    return $countries;
}

//close get countries and US states

function get_duty($code = '', $total=0){
    $duties = array(
        'AU' => [0,0],
        'AT' => [0.247,2.96],
        'BE' => [0.257,3.64],
        'CA' => [0.15,1.54],
        'CN' => [0.17,0.63],
        'CZ' => [0.257,3.01],
        'DK' => [0.297,4.81],
        'FI' => [0.287,2.96],
        'FR' => [0.247,3.71],
        'DE' => [0.237,2.72],
        'GR' => [0.287,9.86],
        'HK' => [0,0],
        'HU' => [0.317,3.57],
        'IS' => [0.24,0],
        'IE' => [0.277,3.71],
        'IL' => [0.17,3.6],
        'IT' => [0.267,3.71],
        'JP' => [0,0],
        'QA' => [0.05,30.48],
        'KR' => [0.18,0.99],
        'LU' => [0.217,3.83],
        'NL' => [0.257,3.21],
        'NZ' => [0.2,1.6],
        'NO' => [0.25,4.29],
        'PT' => [0.277,3.95],
        'PR' => [0,0],
        'SG' => [0.07,2.29],
        'ES' => [0.257,3.71],
        'SE' => [0.297,3.08],
        'CH' => [0.077,4.44],
        'TW' => [0.125,1.03],
        'TH' => [0.17,1.28],
        'GB' => [0.247,2.78],
        'US' => [0,0]
    );
    if($code == ''){
        return $duties;
    } else {
        $t = $duties[$code][0];
        $d = $duties[$code][1];
        $tpercent = $total*($t);
        $tTotal = $tpercent +$d;
        return $tTotal;
    }
}

function get_month(){
	$months = array(
		'01' => 'January',
		'02' => 'February',
		'03' => 'March',
		'04' => 'April',
		'05' => 'May',
		'06' => 'June',
		'07' => 'July',
		'08' => 'August',
		'09' => 'September',
		'10' => 'October',
		'11' => 'November',
		'12' => 'December'
		);


	return $months;
}
//close get month

function get_day(){
	$days = array();
	for($i=1; $i<=31; $i++){
		if(strlen($i) == 1){ $i = '0' . $i; }
		$days[$i] = $i;
	}
	return $days;
}

function get_year($min, $max){
	$years = range($min, $max);
	return $years;
}

function get_year_for_age(){
	$age_limit = date('Y') - 18;
	$years = range(1905, $age_limit);
	return $years;
}

function get_card_type(){
	$type = array(
		'1' => 'visa',
		'2' => 'mastercard',
		'3' => 'discover',
		'4' => 'amex'
		);

	return $type;
}
//close get month


/*
//string $filename = example.jpg
//string $folder = directory under UPLOAD_PATH
//int $width
//int $height
//CONSTANT definded in config.php
//define('UPLOAD_URL' , SITE_URL . 'content/upload/');
//define('UPLOAD_PATH' , __DIR__ . '\\content\\upload\\');
//define('MIN_URL' , SITE_URL . 'content/media/min/');
//define('MIN_PATH' , __DIR__ . '\\content\\media\\min\\');

//return string resized image url
*/
function resize_image($filename, $folder, $width, $height, $filetype = "jpg"){
	$ori_image_url = UPLOAD_PATH . '\\' . $folder . '\\' . $filename;
	$size = $width . '_' . $height;
	$res_image_url = MIN_PATH . $size . '\\' . $filename;

	$image_dir = dirname($res_image_url);
                //d(MIN_PATH);
                //d($image_dir);
                //d(is_dir($image_dir));

	if(!is_dir($image_dir)){mkdir($image_dir, 0777);}
                //d($res_image_url);
                //d(file_exists($res_image_url));
	if(!file_exists($res_image_url)){
                     ////////
                    // ----------Image Resizing Function--------
		$target_file = $ori_image_url;
		$resized_file = $res_image_url;
		$wmax = $width;
		$hmax = $height;
		
		ak_img_resize($target_file, $resized_file, $wmax, $hmax, $filetype);
                    /////////
	}
	return MIN_URL . $size . '/' . $filename;
}//close resize_image


function excerpt($max_length, $value){
	if(strlen($value) > $max_length){
		$value = substr($value, 0, $max_length);
		$value .= "...";
	}
	return $value;
}

/*
	$table_name = string
	$culumns = array('culumn1', 'culumn2');
	$keywords = string, snow man falling from the sky
	$where = string, example1 = 1 AND example1 != 2 OR example3 LIKE '%hello%'
	$distinct = string, use as GROUP BY '{$distinct}'
	$primary_key = string, count($primary_key) as occurrences
	$search_mode= string "more" or "less" for more or less result
*/
	function search($table_name, $culumns, $keywords, $where="", $distinct="id", $primary_key="id", $order_by="id", $sort="ASC", $search_mode="more"){
		
		$keywords = trim(urldecode($keywords));
		
				////////////////////////////////////prepare keyword for search
		$string = "";
		$new_string = "";
		
		$string = $keywords;
				$string = strtolower(htmlentities(urldecode($string)));//make the keyword lowercase and filter the html tag out
				
					if(strlen($string) >= 2){//if the entered keyword is longer than 2 characater, do the search function below   //2
						
						$string = PorterStemmer::Stem($string);//removing the commoner morphological and inflexional endings from words in English
						$clean_string = new cleaner();
						$string = $clean_string->parseString($string);//remove the stupid words e.x me, I , do, what, and so on
						
					//if(strlen($string) >= 2){//2.5
						//if the clean string short than 2 chanracters, output nothing
						
								$split = explode(" ",$string);//split each word in the entered keyword and put them into an array
								//print_r($split);exit();
								//test the result of explode
								
								
								//filter out any word that less than 3 characters long in the array that created above
								//and make them a new string varibale
								foreach ($split as $array => $value) {//3
										if (strlen($value) >= 1) {//4
											$new_string .= ' '.$value.' ';
										  }//close4
										}//end the foreach loop //close 3
										
								$new_string=substr($new_string,0,(strLen($new_string)-1));//remove the last space " "
									//echo $new_string; exit();
								
								//make the new string variable to an array again, because we need the array function to do the slq
								$split_stemmed = explode(" ",$new_string);
								///////////orginal $sql = "SELECT DISTINCT COUNT(*) As occurrences, field1, field2, field3 FROM tablename WHERE (";
								
							//}//close 2.5
						}//close 2
						
						
						$sql = "SELECT *, COUNT({$primary_key}) As occurrences FROM {$table_name} ";
						
			//////////////////if senatized keyword is not shorter than 2 characters long, add keyword search query
						if(strlen($string) >= 2){
							$keyword_sql = "WHERE (";
								
								
							while(list($key,$val)=each($split_stemmed)){//5
									  if($val<>" " and strlen($val) > 0){//6
									  	$val = '%'. $val .'%';
									 // $val = '%'. $val;
									  	
									  	$keyword_sql .= "(";
									  		foreach($culumns as $culumn){
									  			$keyword_sql .= "{$culumn} LIKE '{$val}' OR ";
									  		}
										$keyword_sql = substr($keyword_sql,0,(strLen($keyword_sql)-3));//this will eat the last OR
										if($search_mode == "more"){
											$keyword_sql .= ") OR ";
}elseif($search_mode == "less"){
	$keyword_sql .= ") AND";
}


									  }//close 6
							}//end the while loop //close5
							
									  $keyword_sql = substr($keyword_sql,0,(strLen($keyword_sql)-4));//this will eat the last OR
									  //$sql_find_all_for_count = $sql . ") GROUP BY id";
									  if($where != ""){
									  	$keyword_sql .= ")  AND " . $where;  
}else{
	$keyword_sql .= ")";
}

$keyword_sql .= " GROUP BY {$distinct} ";  


$sql = $sql . $keyword_sql;
$sql .= "ORDER BY {$order_by} {$sort}";

			}else{//if search keyword is shorter than 2
				
				$keyword_sql = "WHERE ";
				$keyword_sql .= "(";
					foreach($culumns as $culumn){
						$keyword_sql .= "{$culumn} LIKE '%" . $string . "%' OR ";
					}
						$keyword_sql = substr($keyword_sql,0,(strLen($keyword_sql)-3));//this will eat the last OR
						
						
						if($where != ""){
							$keyword_sql .= ") AND " . $where;  
}else{
	$keyword_sql .= ")";
}
$keyword_sql .= " GROUP BY {$distinct} ";  

$sql = $sql . $keyword_sql;
$sql .= "ORDER BY {$order_by} {$sort} LIMIT 30";
}
			//close keyword search query



		//dd($sql);
return $sql;

	}//close search function
	
	
	
	function params_string($params, $exception = array()){
		$url_var = ""; 
		if(is_array($params) && count($params)>0){
			foreach($params as $key => $val){
				if(!in_array($key, $exception)){
					$url_var .= $key . "/"  . $val . "/";
				}
			}
		}
		return $url_var;
	}

	function file_format($filename){
		$filename_array = explode(".", $filename);
		$format =  array_pop($filename_array);
		return $format;
	}

	function special_chars_clean($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

	   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}



	/*Resize image base on min length of the shorter side(width or height)*/
	function zy_img_resize($target, $newcopy, $min_len, $ext) {
		list($w_orig, $h_orig) = getimagesize($target);

		$change_scale = "";
		if($w_orig > $h_orig)
		{
			$h = $min_len;
			$change_scale = $h/$h_orig;
			$w = $w_orig*$change_scale;
		}else{
			$w = $min_len;
			$change_scale = $w/$w_orig;
			$h = $h_orig*$change_scale;
		}

		$img = "";
		$ext = strtolower($ext);
		if ($ext == "gif"){ 
			$img = imagecreatefromgif($target);
		} else if($ext =="png"){ 
			$img = imagecreatefrompng($target);
		} else { 
			$img = imagecreatefromjpeg($target);
		}
		$tci = imagecreatetruecolor($w, $h);
// imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
		imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
		if ($ext == "gif"){ 
			imagegif($tci, $newcopy);
		} else if($ext =="png"){ 
			imagepng($tci, $newcopy);
		} else { 
			imagejpeg($tci, $newcopy, 84);
		}
	}
	

/*
    Check if the image already has re-sized. If yes, retrun the path. If not, generated one and return the path.
*/
    function resize_image_by_minlen($folder_name=NULL, $filename, $minlen, $filetype = "jpg"){
    	if($folder_name !== NULL)
    	{
    		$ori_image_url = $folder_name;//UPLOAD_PATH . $folder_name . DS . $filename;
    	}else{
    		$ori_image_url = UPLOAD_PATH . $filename;
    	}


    	$res_image_url = MIN_PATH . $minlen . DS . $filename;

    	$image_dir = dirname($res_image_url);

    	if(!is_dir($image_dir)){mkdir($image_dir, 0777);}

    	if(!file_exists($res_image_url)){

// ----------Call Image Resizing Function--------
    		$target_file = $ori_image_url;
    		$resized_file = $res_image_url;

    		zy_img_resize($target_file, $resized_file, $minlen, $filetype);

    	}
    	return MIN_URL . $minlen . '/' . $filename;
}//close resize_image_by_minlen

/*After we have a resied image, we use this function to generate the crop version*/
function images_thumb($filename, $minlen, $width, $height, $filetype){
	$size = $width . '_' . $height;
	$target_file = MIN_PATH . $minlen . DS . $filename;
	$thumb_file = THUMB_PATH . $size . DS . $filename;

	$image_dir = dirname($thumb_file);

	if(!is_dir($image_dir)){mkdir($image_dir, 0777);}
	if(!file_exists($thumb_file)){
		ak_img_thumb($target_file, $thumb_file, $width, $height, $filetype);
	}
	return THUMB_PATH . $size . DS . $filename;
}


	/**
     * prints out script tag
     * @param type $name: name of script, or if script is in subfolder, then put subfolder/path/to/script.js
     */
	function script($name,$full_path=FRONT_JS) {
		echo "<script type=\"text/javascript\" src=\"".$full_path."{$name}\"></script>";
	}
	
    /**
     * 
     * @param type $name
     * @param type $full_path: default path is in content/css. if css is not there then it can be overwritten
     */
    function css($name,$full_path = FRONT_CSS) {
    	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$full_path."{$name}\">";
    }
    
    function link_to($href,$text,$html_objects = ['class'=>"btn btn-primary btn-add"]) {
    	$attrs = "";
    	foreach($html_objects as $key=>$val) {
    		$attrs .= "{$key}='{$val}'";
    	}
    	return "<a href='{$href}' {$attrs}>{$text}</a>";
    }
    
    function filter_get() {
      $get_filter = array();
      foreach($_GET as $key=>$value) {
        $get_filter[$key] = filter_input(INPUT_GET,$key);
      }
      return $get_filter;
    }
    /**
     * function to save only for single image
     * @param type $file
     * @param type $obj
     * @param type $field_name
     * @param type $upload_path
     * @param type $sizes
     */
    function save_image($file,$obj,$field_name,$upload_path,$sizes) {
      if($file && !empty($file) && is_array($file) && $file['error'] != 4){//dd($file);
            
                    if($obj->{$field_name} != "" && file_exists($upload_path.$obj->{$field_name})){
                      unlink($upload_path . $obj->{$field_name});}

                    $tem_file_name = $file['tmp_name'];
                    $file_name = uniqid() . "_" . basename($file['name']);
                    $file_name = str_replace(' ', '_', $file_name);
                    $file_name = str_replace('-', '_', $file_name);
                    
                    
                    $allow_format = array('jpg', 'png', 'gif'); 
                    $filetype = explode("/", $file['type']); //$file['type'] has value like "image/jpeg"
                    $filetype = strtolower(array_pop($filetype));
                    if($filetype == 'jpeg'){$filetype = 'jpg';}
                    if(in_array($filetype, $allow_format)){
                      
                      $img_path = compress_image($tem_file_name, $upload_path . $file_name, 60,$file['size']);
                      if($img_path===false) {
                        move_uploaded_file($tem_file_name, $upload_path . $file_name);
                      }
                      
                        //move_uploaded_file($tem_file_name, $upload_path . $file_name);
                        $obj->{$field_name} = $file_name;
                        
                        foreach($sizes as $key=>$val) {
                        	if(is_array($val) && count($val)==2) {
                        		resize_image_by_minlen($upload_path, 
                        		$obj->{$field_name}, min($val), $filetype);

                    			$path = images_thumb($obj->{$field_name},min($val), 
                    				$val[0],$val[1], file_format($obj->{$field_name}));
                    			$image_size_str = implode('_',$val);
                    			copy($path, $upload_path.$image_size_str.$file_name);
                    		}
                		}
                		
                		$obj->save();
                    }
                
                     
        }//close upload featured_image
        
        
    }
    
        function compress_image($source_url, $destination_url, $quality,$size,$btsize=535396) {
		$info = getimagesize($source_url);
       if($size<$btsize) {return false;}
    		if ($info['mime'] == 'image/jpeg') {
        			$image = imagecreatefromjpeg($source_url);
                    imagejpeg($image, $destination_url, $quality);
          }

    		elseif ($info['mime'] == 'image/gif') {
        			$image = imagecreatefromgif($source_url);
                    imagegif($image, $destination_url, $quality);
            }

   		elseif ($info['mime'] == 'image/png') {
        			$image = imagecreatefrompng($source_url);
                    imagepng($image, $destination_url, $quality);
        }

    		//imagejpeg($image, $destination_url, $quality);
		return $destination_url;
	}
    
    function get_token() {
          if(isset($_SESSION['csrf_token'])) {
            return $_SESSION['csrf_token'];
          } else {
            $_SESSION['csrf_token'] = md5(uniqid(rand(),TRUE));
            return  $_SESSION['csrf_token'];
          }
        }

function generateToken()
{
	$old_md5 = md5(uniqid(time(),true));
	$new_md5 = substr($old_md5,0,10);

	return $new_md5;
}
function generateLongToken()
{
	$md5 = md5(uniqid(time(),true));

	return $md5;
}
    function strReplace($content, $limit, $strReplace)
    {
        return strlen($content) < $limit ? $content : substr($content, 0, $limit) . $strReplace;
    }


/**
 *  Given a file, i.e. /css/base.css, replaces it with a string containing the
 *  file's mtime, i.e. /css/base.1221534296.css.
 *
 *  @param $file  The file to be loaded.  Must be an absolute path (i.e.
 *                starting with slash).
 *
 * @return string The new version of the file to load
 */
function auto_version($file)
{
    if(strpos($file, '/') !== 0 || !file_exists($_SERVER['DOCUMENT_ROOT'] . $file))
        return $file;

    $mtime = filemtime($_SERVER['DOCUMENT_ROOT'] . $file);
    return preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
}