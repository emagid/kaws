<?php

require_once(__DIR__."/../../libs/Emagid/emagid.php");
require_once(__DIR__."/../../conf/emagid.conf.php");
require_once(__DIR__.'/../../includes/functions.php');
$emagid = new \Emagid\Emagid($emagid_config);

function execute(){
    $filePath = __DIR__.'/price_list_4-27-17.csv';
    $arrResult = [];
    $head = 2;

    $handle = fopen($filePath,'r');
    if($handle){
        $start = 0;
        while (($data = fgetcsv($handle,1000,',')) !== false){
            if(empty($data[3]) || $start < $head){
                $start++;
            } else {
                $arrResult[] = $data;
            }
        }
        fclose($handle);
    }

    $mainCategory = $subCategory = $type = '';
    $typeList = ['main'=>1,'combo'=>2,'spare'=>3,'license key'=>4,'single waypoint'=>5,'200 waypoint'=>6,'kit'=>7,'code'=>8,'card'=>9];
    foreach ($arrResult as $res){
        $mainCategory = $res[0] ? : $mainCategory;
        $subCategory = $res[1] ? : $subCategory;
        $type = $res[2] ? : $type;
        $productName = $res[3];
        $description = $res[4];
        $partNo = $res[5];
        $ean = $res[6];
        $upc = $res[7];
        $price = filter_var($res[8],FILTER_SANITIZE_NUMBER_FLOAT);

        $productType = $typeList[strtolower(trim($type))];

        $product = \Model\Product::getItem(null,['where'=>"(part_number = '".trim($partNo)."')"]) ? : new \Model\Product();
        if($product->id == 0){
            $name = trim($productName);
            $slug = strtolower(str_replace(' ','-',$name));
            $product->name = $name;
            $product->slug = $slug;
            $product->part_number = $partNo;
            $product->ean = $ean;
            $product->upc = $upc;
            $product->description = $description;
            $product->type = $productType;
            $product->quantity = 10;
        }
        $product->price = $price;
        $product->save();
        $productId = $product->id;
        echo "Product saved ($productId): $product->name\n";

        $category = \Model\Category::getItem(null,['where'=>"lower(name) = '".trim(strtolower($mainCategory))."'"]) ? : new \Model\Category();
        if($category->id == 0){
            $name = trim($mainCategory);
            $slug = strtolower(str_replace(' ','-',$name));
            $category->name = ucwords(strtolower($name));
            $category->slug = $slug;
            $category->save();
        }
        $mainCategoryId = $category->id;
//        echo "Main Category saved ($mainCategoryId): $category->name\n";

        $subCat = \Model\Category::getItem(null,['where'=>"lower(name) = '".trim(strtolower($subCategory))."'"]) ? : new \Model\Category();
        if($subCat->id == 0){
            $name = trim($subCategory);
            $slug = strtolower(str_replace(' ','-',$name));
            $subCat->name = $name;
            $subCat->slug = $slug;
            $subCat->save();
        }
        $subCategoryId = $subCat->id;
//        echo "Sub Category saved ($subCategoryId): $subCat->name\n";


        $prodCat = \Model\Product_Category::getItem(null,['where'=>"product_id = $productId and category_id in ($mainCategoryId,$subCategoryId)"]) ? : new \Model\Product_Category();
        if($prodCat->id == 0){
            $prodCat->product_id = $productId;
            $prodCat->category_id = $subCategoryId;
            $prodCat->save();
        }
//        echo "Product Categoy saved ($prodCat->id): $prodCat->product_id / $prodCat->category_id\n";
    }
}
execute();