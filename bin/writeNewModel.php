<?php

require_once(__DIR__."/../libs/Emagid/emagid.php");
require_once(__DIR__."/../conf/emagid.conf.php");
require_once(__DIR__.'/../includes/functions.php');
$emagid = new \Emagid\Emagid($emagid_config);

/**
 * argv[1] = Model name
 * argv[2] = Fields (comma separated. ie. name, addr1, addr2){exclude non-editable fields like id, active, insert_time)
 * Optional - argv[3] = Database name
 */
function createModel()
{
    global $argv;
    if (isset($argv[1]) && $argv[1]) {
        $className = str_replace(' ','_',ucwords(str_replace('_',' ',strtolower($argv[1]))));
        $dbName = isset($argv[3]) && $argv[3] ? strtolower($argv[3]) : strtolower($argv[1]);
        $fieldArray = isset($argv[2]) && $argv[2] ? json_encode(explode(",", $argv[2])) : '[]';

        $file = fopen(__DIR__.'/../libs/Model/'.$className . '.php', 'w') or die('Unable to open file');
//        $file = fopen($className . '.php', 'w') or die('Unable to open file');

        $phpTag = "<?php\n\n";
        $namespace = 'namespace Model;' . "\n\n";
        $use = 'use Emagid\Core\Model;'."\n\n";
        $class = "class " . $className . " extends Model{\n";
        $tablename = "\t" . 'static $tablename = \'' . $dbName . "';\n";
        $fields = "\t" . 'static $fields = ' . $fieldArray . ";\n";
        $close = '}';
        fwrite($file, $phpTag);
        fwrite($file, $namespace);
        fwrite($file, $use);
        fwrite($file, $class);
        fwrite($file, $tablename);
        fwrite($file, $fields);
        fwrite($file, $close);
        echo "Write $className.php complete\n";
    } else {
        var_dump('No model name');
    }
}
function createAdminController()
{
    global $argv;
    if (isset($argv[1]) && $argv[1]) {
        $className = str_replace(' ','_',ucwords(str_replace('_',' ',strtolower($argv[1]))));
        $controllerName = strtolower($className) . 'sController';

        $file = fopen(__DIR__.'/../controller/admin/' . $controllerName . '.php', 'w') or die('Unable to open file');
//        $file = fopen($controllerName . '.php', 'w') or die('Unable to open file');

        $phpTag = "<?php\n\n";
        $class = "class " . $controllerName . " extends adminController{\n\n";
        $__construct =
    "\tfunction __construct(){
        parent::__construct('$className');
    }\n";
        $index =
    "\t".'function index(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }'."\n";
        $close = '}';

        fwrite($file,$phpTag);
        fwrite($file,$class);
        fwrite($file,$__construct);
        fwrite($file,$index);
        fwrite($file,$close);
        echo "Write $controllerName.php complete\n";
    } else {
        var_dump('No model name');
    }
}
function createAdminViews(){
    global $argv;
    if (isset($argv[1]) && $argv[1]) {
        $className = strtolower($argv[1]);
        if(mkdir(__DIR__."/../views/admin/{$className}s")){
            echo "Write $className folder complete\n";
            $indexFile = fopen(__DIR__."/../views/admin/{$className}s/index.php",'w');
            $updateFile = fopen(__DIR__."/../views/admin/{$className}s/update.php",'w');

            $indexString =
'<?php if (count($model->'.$className.'s) > 0) { ?>
    <div class="box box-table">
        <table class="table" id="data-list">
            <thead>
            <tr>';
            foreach(explode(',',$argv[2]) as $value){
                $indexString .= "
                <th width='20%'>".ucwords(str_replace('_',' ',strtolower($value)))."</th>";
            }
            $indexString .= '
                <th width="1%" class="text-center">Edit</th>
                <th width="1%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->'.$className.'s as $obj) { ?>
                <tr class="originalProducts">';
            foreach(explode(',',$argv[2]) as $value){
                $indexString .= '
                    <td><a href="<?php echo ADMIN_URL; ?>'.$className.'s/update/<?php echo $obj->id; ?>"><?php echo $obj->'.$value.'; ?></a></td>';
            }
                $indexString .= '
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>'.$className.'s/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>'.$className.'s/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm(\'Are You Sure?\');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class=\'paginationContent\'></div>
        </div>
    </div>
<?php } ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = \'<?= ADMIN_URL.\''.$className.'s\';?>\';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>';



            $updateString =
'<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->'.$className.'->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>';
            foreach(explode(',',$argv[2]) as $value){
                $updateString .= '
                <div class="form-group">
                    <label>'.ucwords(str_replace('_',' ',strtolower($value))).'</label>
                    <?php echo $model->form->editorFor("'.$value.'"); ?>
                </div>';
            }
            $updateString .= '
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL.\''.$className.'/\');?>;
    $(document).ready(function () {
        $("input[name=\'name\']").on(\'keyup\', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, \'-\');
            val = val.replace(/[-]+/g, \'-\');
            $("input[name=\'slug\']").val(val.toLowerCase());
        });
    });
</script>';
            fwrite($indexFile,$indexString);
            echo "Write index.php complete\n";

            fwrite($updateFile,$updateString);
            echo "Write update.php complete\n";
        } else {
            var_dump('Failed to create VIEWS FOLDER: '.$className);
        }
    } else {
        var_dump('No model name');
    }
}
createModel();
createAdminController();
createAdminViews();