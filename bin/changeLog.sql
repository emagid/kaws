-- Jan 9
create table campaign(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
type character varying,
name character varying,
status character varying
);

create table campaign_product(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
campaign_id INTEGER,
product_id INTEGER,
quantity INTEGER default 0,
notify_level INTEGER default 0,
winner INTEGER default 0
);

-- Jan 11
ALTER TABLE campaign_product ADD COLUMN max_per_user INTEGER DEFAULT 3;

-- Jan 19
ALTER TABLE public.order ADD COLUMN allow_returns SMALLINT DEFAULT 0;

-- Feb 7
ALTER TABLE public.order ADD COLUMN fulfillment_status CHARACTER VARYING;
UPDATE order_products SET quantity = 1 WHERE quantity IS NULL;

-- Feb 9
ALTER TABLE campaign ADD COLUMN c_limit INTEGER;

CREATE TABLE lotto_entries(
id serial PRIMARY KEY,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP without TIME ZONE NOT NULL DEFAULT now(),
email_date TIMESTAMP without TIME ZONE,
claimed_date TIMESTAMP without TIME ZONE,
product_id INTEGER,
campaign_id INTEGER,
user_id INTEGER,
winner SMALLINT DEFAULT 0,
redeemed SMALLINT DEFAULT 0,
ref_num CHARACTER VARYING
);

-- Feb 12
ALTER TABLE public.order ADD COLUMN user_ip CHARACTER VARYING;
ALTER TABLE public.order ADD COLUMN entry_id INTEGER;
ALTER TABLE public.order ADD COLUMN campaign_id INTEGER;

-- Feb 13

ALTER TABLE mail_log ADD COLUMN slug CHARACTER VARYING;
ALTER TABLE mail_log ADD COLUMN mandrill_id INTEGER;
ALTER TABLE mail_log ADD COLUMN state INTEGER;

-- Feb 27

CREATE SEQUENCE public.blacklist_users_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.blacklist_users(
id integer NOT NULL DEFAULT nextval('blacklist_users_id_seq'::regclass),
active smallint default 1,
insert_time timestamp without time zone not null default now(),
email character varying not null,
ip_address character varying,
cc_number character varying,
CONSTRAINT blacklist_users_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- Feb 28

DROP TABLE blacklist_users;

CREATE TABLE public.blacklist(
id SERIAL,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
field INTEGER NOT NULL,
value CHARACTER VARYING NOT NULL,
CONSTRAINT blacklist_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- Mar 1

ALTER TABLE public.order ADD COLUMN place_id CHARACTER VARYING;

-- Mar 14

ALTER TABLE blacklist ADD COLUMN blacklist_id INTEGER;

-- Mar 24

ALTER TABLE public.order ADD COLUMN duty NUMERIC(10,2) DEFAULT 0;

-- Mar 28

ALTER TABLE public.order ADD COLUMN orig_total NUMERIC(10,2) DEFAULT 0;

-- Mar 29

ALTER TABLE transaction ADD COLUMN import SMALLINT DEFAULT 0;
ALTER TABLE transaction ADD COLUMN notes CHARACTER VARYING;

-- Apr 02

CREATE TABLE public.note_log(
id SERIAL,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
order_id INTEGER,
note CHARACTER VARYING,
admin_id INTEGER,
CONSTRAINT note_log_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

--Apr 24

ALTER TABLE campaign ADD COLUMN terms CHARACTER VARYING;
ALTER TABLE campaign ADD COLUMN end_time CHARACTER VARYING;
ALTER TABLE campaign ADD COLUMN start_time CHARACTER VARYING;

-- Apr 26

CREATE TABLE public.labels(
id SERIAL,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
shipped_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
order_id INTEGER,
tracking_number CHARACTER VARYING,
image CHARACTER VARYING,
type INTEGER,
CONSTRAINT labels_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- Jun 10

ALTER TABLE shipping_method ADD COLUMN duty NUMERIC(9, 3);
ALTER TABLE shipping_method ADD COLUMN disbursement NUMERIC(9, 3);
ALTER TABLE shipping_method ADD COLUMN price NUMERIC(9, 3);
ALTER TABLE shipping_method ADD COLUMN addon NUMERIC(9, 3);
ALTER TABLE shipping_method ADD COLUMN campaign_id INTEGER;
ALTER TABLE public.transaction ADD COLUMN raw JSONB;

-- Jun 11

UPDATE shipping_method SET name = 'Domestic shipping' WHERE name IN ('U.S. Shipping','US shipping');
UPDATE shipping_method SET price = 30, addon = 4  WHERE name = 'Domestic shipping';
UPDATE shipping_method SET price = 56, addon = 4  WHERE name = 'Canada shipping';
UPDATE shipping_method SET price = 85, addon = 20 WHERE name = 'Europe Shipping';
UPDATE shipping_method SET price = 90, addon = 20 WHERE name = 'Asian/Israel/South Pacific';

ALTER TABLE blacklist ADD COLUMN preview CHARACTER VARYING;

-- Jun 14

ALTER TABLE public.order ADD COLUMN difference NUMERIC (10,2);
ALTER TABLE public.order ADD COLUMN add_updated INTEGER DEFAULT 0;
ALTER TABLE public.order ADD COLUMN add_visited INTEGER DEFAULT 0;
ALTER TABLE public.order ADD COLUMN add_update_sent INTEGER DEFAULT 0;

-- Jun 18
ALTER TABLE public.order ADD COLUMN charge_added INTEGER DEFAULT 0;
ALTER TABLE public.order ADD COLUMN charge_email_sent INTEGER DEFAULT 0;

-- Jun 22
ALTER TABLE public.order ADD COLUMN refunded INTEGER DEFAULT 0;

-- Jun 26
ALTER TABLE order_verifications ADD COLUMN generated_order INTEGER;
ALTER TABLE public.order ADD COLUMN generated INTEGER DEFAULT 0;
ALTER TABLE public.order ADD COLUMN discarded INTEGER DEFAULT 0;

-- Jun 27
ALTER TABLE order_verifications ADD COLUMN combined INTEGER default 0;

-- Jul 19
ALTER TABLE labels ADD COLUMN claimed INTEGER default 0;

-- Sep 24
ALTER TABLE public."order" ADD COLUMN pull_attempts INTEGER DEFAULT 0; 