<?php

require_once(__DIR__."/../libs/Emagid/emagid.php");
require_once(__DIR__."/../conf/emagid.conf.php");
require_once(__DIR__.'/../includes/functions.php');
$emagid = new \Emagid\Emagid($emagid_config);

require_once(__DIR__.'/../libs/ShipStation-wrapper/libraries/shipstation/Shipstation.class.php');
require_once(__DIR__.'/../libs/ShipStation-wrapper/libraries/unirest/Unirest.php');
function ship(){
    $curl = curl_init();
    curl_setopt($curl,CURLOPT_URL,'https://ssapi.shipstation.com/orders');
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_USERPWD,SHIPSTATION_KEY.':'.SHIPSTATION_SECRET);
    $result = curl_exec($curl);
    file_put_contents(__DIR__.'../shipJson.txt',$result);
    echo 'complete';
    curl_close($curl);
}

function shipApi(){
    $ship = new ShipStation();
    $ship->setSsApiKey(SHIPSTATION_KEY);
    $ship->setSsApiSecret(SHIPSTATION_SECRET);
    $result = $ship->getOrders([]);
    print_r(json_encode($result));
}

function shipmentCall(){
    $ship = new ShipStation();
    $ship->setSsApiKey(SHIPSTATION_KEY);
    $ship->setSsApiSecret(SHIPSTATION_SECRET);
    $result = $ship->getShipments([]);
    file_put_contents(__DIR__.'../shipJson.txt',json_encode($result));
//    print_r($result);
    echo 'done';
}
function getByOrder(){
    $ship = new ShipStation();
    $ship->setSsApiKey(SHIPSTATION_KEY);
    $ship->setSsApiSecret(SHIPSTATION_SECRET);
    $result = $ship->getOrder(1147892);
    file_put_contents(__DIR__.'../shipJson.txt',json_encode($result));
//    print_r($result);
    echo 'done';
}
function createOrder(){
    $shipTo = new stdClass();
    $shipTo->name = 'Haro Brimly';
    $shipTo->company       = null;
    $shipTo->street1 = '123 fake st';
    $shipTo->street2 = null;
    $shipTo->street3 = null;
    $shipTo->city = 'New York';
    $shipTo->state = 'NY';
    $shipTo->postalCode = '10001';
    $shipTo->country = 'US';
    $shipTo->phone = '555-555-5555';
    $shipTo->residential   = null;

    $billTo = new stdClass();
    $billTo->name = 'Haro Brimly';
    $billTo->company       = null;
    $billTo->street1       = null;
    $billTo->street2       = null;
    $billTo->street3       = null;
    $billTo->city          = null;
    $billTo->state         = null;
    $billTo->postalCode    = null;
    $billTo->country       = null;
    $billTo->phone         = null;
    $billTo->residential   = null;

    $items = [];
    $item = new stdClass();
    $item->lineItemKey = 1;
    $item->productId = 1;
    $item->sku = 'test123';
    $item->name = 'test';
    $item->quantity = 1;
    $item->unitPrice = 150;
    $items[] = $item;

    $advancedOptions    = new stdClass();
//    $advancedOptions->warehouseId = 81987;
    $advancedOptions->storeId = 186177;

    $order = new stdClass();
    $order->orderNumber = 'TEST-ORDER-API-DOCS';
    $order->orderDate = date('Y-m-d').'T'.date('H:i:s').'.0000000';
    $order->orderStatus = 'awaiting_shipment';
    $order->billTo = $billTo;
    $order->shipTo = $shipTo;
    $order->items = $items;
    $order->advancedOptions = $advancedOptions;


    $ship = new ShipStation();
    $ship->setSsApiKey(SHIPSTATION_KEY);
    $ship->setSsApiSecret(SHIPSTATION_SECRET);
    $result = $ship->addOrder($order);
    file_put_contents(__DIR__.'../shipJson.txt',json_encode($result));
//    print_r($result);
    print_r($order);
    if($result){
        print_r($result);
    } else {
        print_r($ship->getLastError());
    }
}
createOrder();
//getByOrder();
//shipmentCall();
//shipApi();
//ship();