<?php
require(__DIR__.'/../index.php');

$filePaths = [__DIR__.'/customer.csv'];
$arrResult = [];
$head = true;
foreach($filePaths as $filePath) {
    $handle = fopen($filePath, 'r');
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            if($head){
                $head = false;
            } else {
                $arrResult[] = $data;
            }
        }
        fclose($handle);
        $head = true;
    }
}
//dd($arrResult);
for($x = 0; $x < count($arrResult); $x++){

    $ref_id = $arrResult[$x][0];
    $fname = $arrResult[$x][1];
    $lname = $arrResult[$x][2];
    $email = $arrResult[$x][3];
    $phone = str_replace(') ','', $arrResult[$x][4]);
    $phone = str_replace('(','', $phone);
    $phone = str_replace('-','', $phone);
    $company_name = $arrResult[$x][6];
    $address = $arrResult[$x][7];
    $address_1 = $arrResult[$x][8];
    $city = $arrResult[$x][9];
    $state = $arrResult[$x][10];
    $zip = $arrResult[$x][11];
    $DOB = $arrResult[$x][12];
    $memo = $arrResult[$x][13];
    $square_id = $arrResult[$x][14];
    $first_visit = $arrResult[$x][15];
    $trans_count = $arrResult[$x][17];
    $total_spend = str_replace('$','', $arrResult[$x][18]);
    $subscribe = $arrResult[$x][19];


    $user = new \Model\User();

    $user->email = $email;
    $user->first_name = $fname;
    $user->last_name = $lname;
    $user->ref_id = $ref_id;
    $user->phone = $phone;
    $user->company_name = $company_name;
    $user->address = $address;
    $user->address_1 = $address_1;
    $user->city = $city;
    $user->state = $state;
    $user->zip = $zip;
    $user->dob = $DOB;
    $user->memo = $memo;
    $user->square_id = $square_id;
    $user->first_visit = $first_visit;
    $user->trans_count = $trans_count;
    $user->total_spend = floatval($total_spend);
    $user->subscribe = $subscribe;



    $hash = \Emagid\Core\Membership::hash('12345');
    $user->password = $hash['password'];
    $user->hash = $hash['salt'];

    $user->save();

    echo "$email saved \n";
}
