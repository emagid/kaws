<?php

require_once(__DIR__."/../libs/Emagid/emagid.php");
require_once(__DIR__."/../conf/emagid.conf.php");
require_once(__DIR__.'/../includes/functions.php');
$emagid = new \Emagid\Emagid($emagid_config);

function back_order()
{
    $filePaths = [__DIR__ . '/back_order.csv'];
    $arrResult = [];
    $head = true;

    foreach ($filePaths as $filePath) {
        $handle = fopen($filePath, 'r');
        if ($handle) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($head) {
                    $head = false;
                } else {
                    $arrResult[] = $data;
                }
            }
            fclose($handle);
            $head = true;
        }
    }

    foreach ($arrResult as $key => $arr) {
        $partNo = $arr[0];
        $ean = $arr[1];
        $upc = $arr[2];
        //$itemName = $arr[3]; /**Inferred from upc->product_id relation*/
        $pi = $arr[4];
        $qty_req = $arr[5];
        $qty_rec = $arr[6];
        $isComplete = $arr[7];

        if ($upc) {
            $inventory = new \Model\Back_Order();
            $inventory->part_no = $partNo;
            $inventory->ean = $ean;
            $inventory->upc = $upc;
            $inventory->pi_num = $pi;
            $inventory->qty_request = $qty_req;
            $inventory->qty_receive = $qty_rec ?: ($isComplete == 'TRUE' ? $qty_req : 0);
            $inventory->product_id = ($p = \Model\Product::getItem(null, ['where' => "upc = '$upc'"])) ? $p->id : 0;
            /** Expand on status range in future */
            $inventory->status = $isComplete == 'TRUE' ? 1 : 0;
            $inventory->save();
            echo "Insert new Back Order: $upc\n";
        }
    }
}

function receiving(){
    $filePaths = [__DIR__ . '/receiving.csv'];
    $arrResult = [];
    $head = true;

    foreach ($filePaths as $filePath) {
        $handle = fopen($filePath, 'r');
        if ($handle) {
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                if ($head) {
                    $head = false;
                } else {
                    $arrResult[] = $data;
                }
            }
            fclose($handle);
            $head = true;
        }
    }

    foreach($arrResult as $row=>$arr){
        $partNo = $arr[0];
        $ean = $arr[1];
        $upc = $arr[2];
        $piNum = $arr[3];
        $piName = $arr[4];
        $shippingCode = $arr[7];
        $qty = $arr[8];
        $tracking = $arr[9];
        $shipDate = date('Y-m-d H:i:s',strtotime($arr[10]));
        $isComplete = $arr[11];

//        $propertyArray = ['product_id'=>$product_id,'part_no'=>$partNo,'ean'=>$ean,'upc'=>$upc,'pi_num'=>$piNum,'pi_name'=>$piName,'code'=>$shippingCode,'qty'=>$qty,'tracking_no'=>$tracking,'shipping_date'=>$shipDate,'status'=>$isComplete];
//        $receiving = new \Model\Receiving();
//        $receiving->loadProperties($propertyArray);
        if($upc) {
            $receiving = new \Model\Receiving();
            $receiving->product_id = ($p = \Model\Product::getItem(null, ['where' => "upc = '$upc'"])) ? $p->id : 0;
            $receiving->part_no = $partNo;
            $receiving->ean = $ean;
            $receiving->upc = $upc;
            $receiving->pi_num = $piNum;
            $receiving->pi_name = $piName;
            $receiving->code = $shippingCode;
            $receiving->qty = $qty;
            $receiving->tracking_no = $tracking;
            $receiving->shipping_date = $shipDate;
            $receiving->status = $isComplete;
            if ($receiving->save()) {
                echo "Insert new Receiving: $row \n";
            } else {
                echo "Failed to insert Receiving: $row \n";
            }
        }
    }
}

function actualInventory(){
    global $emagid;
    $db = $emagid->getDb();
    $sql = "select product_id, sum(qty) as qty from receiving GROUP BY product_id;";
    $actualData = $db->getResults($sql);

    foreach($actualData as $data){
        $d = new \Model\Actual_Inventory();
        $d->product_id = $data['product_id'];
        $d->warehouse = $data['qty'];
        $d->total = $data['qty'];
        $d->amazon = 0;
        $d->ebay = 0;
        $d->square = 0;
        $d->save();
    }

}

function camrise(){ /** Edge Case function */
    $filePaths = [__DIR__.'/camrise_runnable.csv'];
    $arrResult = [];
    $head = true;

    foreach($filePaths as $filePath){
        $handle = fopen($filePath,'r');
        if($handle){
            while(($data = fgetcsv($handle,1000,',')) != false){
                if($head){
                    $head = false;
                } else {
                    $arrResult[] = $data;
                }
            }
            fclose($handle);
            $head = true;
        }
    }

    foreach($arrResult as $row=>$arr){
        $piNum = $arr[1];
        $name = trim(str_replace('RC/TOY MODEL','',$arr[2]));
        $code = $arr[3];
        $qty = $arr[4];
        $unitPrice = $arr[6];
        $trackingNum = $arr[9];
        $date = date('Y-m-d H:i:s',strtotime($arr[10]));

        $product = \Model\Product::getItem(null,['where'=>"lower(name) = lower('$name')"]);
        $backOrder = new \Model\Back_Order();
        $backOrder->pi_num = $piNum;
        $backOrder->product_id = $product->id;
        $backOrder->part_no = $product->part_number;
        $backOrder->ean = $product->ean;
        $backOrder->upc = $product->upc;
        $backOrder->qty_request = $backOrder->qty_receive = $qty;
        $backOrder->save();
        echo "Back Order created\n";

        $receiving = new \Model\Receiving();
        $receiving->product_id = $product->id;
        $receiving->part_no = $product->part_number;
        $receiving->ean = $product->ean;
        $receiving->upc = $product->upc;
        $receiving->pi_num = $piNum;
        $receiving->pi_name = $arr[2];
        $receiving->code = $code;
        $receiving->qty = $qty;
        $receiving->tracking_no = $trackingNum;
        $receiving->shipping_date = $date;
        $receiving->unit_price = $unitPrice;
        $receiving->save();
        echo "Receiving created\n";

    }
}

//back_order();
//receiving();
//actualInventory();
camrise();