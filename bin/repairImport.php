<?php

require(__DIR__.'/../index.php');

$filePaths = [
    __DIR__.'/csv/NEW.csv'
];

$arrResult = [];
$head = true;
foreach($filePaths as $filePath) {
    $handle = fopen($filePath, 'r');
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            if (empty($data[0])) {
            } elseif($head){
                $head = false;
            } else {
                $arrResult[] = $data;
            }
        }
        fclose($handle);
        $head = true;
    }
}
//dd($arrResult);
//$type = ['main'=>1,'combo'=>2,'spare'=>3,'license key'=>4,'single waypoint'=>5,'200 waypoint'=>6,'kit'=>7,'code'=>8,'card'=>9];
for($x = 0; $x < count($arrResult); $x++) {
//    $cateData = $arrResult[$x][1];
//    $productType = $arrResult[$x][2];
//    $productType = $type[strtolower(trim($productType))];
    $productName = $arrResult[$x][0];
    $productDescription = $arrResult[$x][1];
    $productPartNum = $arrResult[$x][2];
//    $productEan = $arrResult[$x][5];
//    $productEan = number_format($productEan, 0, '.', '');
//    $productUpc = $arrResult[$x][6];
//    $productUpc = number_format($productUpc, 0, '.', '');

    $productPrice = $arrResult[$x][5] == '/'? 0 :str_replace('$','',$arrResult[$x][5]);

    $product = \Model\Product::getItem(null,['where'=>"lower(name) = '".trim(strtolower($productName))."'"]) ? : new \Model\Product();
    if($product->id == 0){
        $name = trim($productName);
        $slug = strtolower(str_replace(' ','-',$name));
        $product->name = $name;
        $product->slug = $slug;
        $product->part_number = $productPartNum;
//        $product->ean = $productEan;
//        $product->upc = $productUpc;
        $product->price = floatval($productPrice);
//        $product->type = $productType;
        $product->quantity = 10;
//        dd($product);
        $product->save();
    }
    $productId = $product->id;
    $category = \Model\Category::getItem(null,['where'=>"lower(name) = 'repair'"]);
    $categoryId = $category->id;
    $prod_cat = \Model\Product_Category::getItem(null,['where'=>"product_id = $productId and category_id = $categoryId"])? : new \Model\Product_Category();
    if($prod_cat->id == 0){
        $prod_cat->product_id = $productId;
        $prod_cat->category_id = $categoryId;
    }
    $prod_cat->save();
    echo "Row: $x, $productName, $productPartNum, $productPrice  saved\n";
}