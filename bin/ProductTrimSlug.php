<?php

include(__DIR__.'/../index.php');

function trimProductSlug(){
    $products = \Model\Product::getList();
    foreach($products as $product){
        $product->slug = rtrim($product->slug,'-');
        $product->save();
    }
}
trimProductSlug();