<?php
require(__DIR__.'/../index.php');

$filePaths = [__DIR__.'/csv/oc_customer.csv'];
$arrResult = [];
foreach($filePaths as $filePath) {
    $handle = fopen($filePath, 'r');
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            $arrResult[] = $data;
        }
        fclose($handle);
    }
}

global $emagid;
$db = $emagid->getDb();

for($x = 0; $x < count($arrResult); $x++){

    $phone = str_replace(') ','', $arrResult[$x][4]);
    $phone = str_replace('(','', $phone);
    $phone = str_replace('-','', $phone);


    $user = new \Model\User();
    $user->id = $arrResult[$x][0];
    $user->email = $arrResult[$x][3];
    $user->first_name = $arrResult[$x][1];
    $user->last_name = $arrResult[$x][2];
    $user->phone = $phone;
    $user->company_name = $arrResult[$x][5];
    $user->address = $arrResult[$x][6];
    $user->address_1 = $arrResult[$x][7];
    $user->city = $arrResult[$x][8];
    $user->state = '';
    $user->zip = $arrResult[$x][9];
    $user->is_stc = 1;

    $hash = \Emagid\Core\Membership::hash('12345');
    $user->password = $hash['password'];
    $user->hash = $hash['salt'];

    $sql = "INSERT INTO public.user (email, first_name, last_name, phone, company_name, address, address_1, city, state, zip, is_stc, password, hash) 
VALUES ('{$user->email}', '{$user->first_name}', '{$user->last_name}', '{$user->phone}', '{$user->company_name}', '{$user->address}', '{$user->address_1}', '{$user->city}', '{$user->state}', '{$user->zip}', '{$user->is_stc}', '{$user->password}', '{$user->hash}')";
    $db->getResults($sql);

    echo "{$user->email} saved \n";
}
