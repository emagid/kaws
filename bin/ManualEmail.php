<?php

include(__DIR__.'/../index.php');

function manualEmail(){
    $order = \Model\Order::getItem(152);
    $orderProducts = \Model\Order_Product::getList(['where'=>"order_id = $order->id"]);
    $html = '';
    foreach($orderProducts as $orderProduct) {
        $product = \Model\Product::getItem($orderProduct->product_id);
        $json = json_decode($orderProduct->details,true);
        $colorObj = \Model\Color::getItem($json['color']);
        $sizeObj = \Model\Size::getItem($json['size']);
        $html .=
            '<tr style="height:75.0pt">
            <td width="100" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                <p class="MsoNormal"><img width="100px!important;" src="https://modernvice.com' . UPLOAD_URL . 'products/' . $product->featuredImage(null, $colorObj->id) . '"></p>
            </td>
            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $product->name . '</span></p>
            </td>
            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $orderProducts->quantity . '</span></p>
            </td>
            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">$' . number_format($product->getPrice($colorObj->id) * $orderProducts->quantity, 2) . '</span></p>
            </td>
            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">' . $colorObj->name . '</span></p>
            </td>
            <td width="140" valign="top" style="width:75.0pt;border:none;border-bottom:solid #e5e5e5 1.0pt;background:white;padding:0in 7.5pt 0in 7.5pt;height:75.0pt">
                <p><span style="font-size:9.0pt;font-family:&quot;Montserrat&quot;,sans-serif;color:#110c0e">US: ' . $sizeObj->us_size . '<br>EU: ' . $sizeObj->eur_size . '</span></p>
            </td>
        </tr>';
    }
    $email = new \Email\MailMaster();
    $mergeFields = [
        'ORDER_NUMBER' => "1" . str_pad($order->id, 5, 0, STR_PAD_LEFT),
        'DATE' => date('Y-m-d h:i:s'),
        'NAME' => $order->ship_first_name . ' ' . $order->ship_last_name,
        'SHIPPING' => $order->getShippingAddr(),
        'BILLING' => $order->getBillingAddr(),
        'ITEMS' => $html,
        'SUBTOTAL' => number_format($order->subtotal, 2),
        'DISCOUNT' => number_format($order->coupon_amount, 2), // update later
        'SHIPPINGFEE' => number_format($order->shipping_cost, 2),
        'TAXFEE' => number_format($order->tax, 2),
        'TOTAL' => number_format($order->total, 2)
    ];
    $email->setTo(['email' => $order->email, 'name' => $order->first_name . ' ' . $order->last_name, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('modern-vice-order-template')->send();

}