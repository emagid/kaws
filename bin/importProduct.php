<?php

require(__DIR__.'/../index.php');

$filePath = __DIR__.'/csv/order_213083-214083.csv';

$handle = fopen($filePath, 'r');

global $emagid;
$db = $emagid->getDb();

if ($handle) {
    while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
        import($data, $db);
    }
    fclose($handle);
    $head = false;
}

function import($arrResult, $db){
    if($arrResult[0] == 'order'){
        return ;
    }
    $data = [
        'order' => $arrResult[0],
        'name' => $arrResult[1],
        'email' => $arrResult[2],
        'phone' => $arrResult[3],
        'address' => $arrResult[4],
        'product' => $arrResult[5],
        'subtotal' => $arrResult[6],
        'shipping' => $arrResult[7],
        'tax' => $arrResult[8],
        'total' => $arrResult[9],
    ];
    $email = strtolower(trim($data['email']));
    $userSql = "select id from public.user where lower(email)='{$email}'";
    $name = explode(' ',$data['name'],2);
    $existingUser = $db->getResults($userSql);
    $phone = $data['phone'];
    $userId = 0;
    if(!$existingUser){
        $newUser = $db->getResults("insert into public.user (active, email, first_name, last_name, phone, password) values (1, '{$email}', '{$name[0]}', '{$name[1]}', '{$phone}', '********') returning id");
//        $newUser = $db->getResults($userSql);
        $userId = $newUser[0]['id'];
    } else {
        $userId = $existingUser[0]['id'];
    }

    $_address = explode(',',$data['address']);
    if(count($_address) == 6) {
        $address = (object)[
            'name'    => trim($_address[0]),
            'address' => trim($_address[1]),
            'address2'=> NULL,
            'city'    => trim($_address[2]),
            'state'   => trim($_address[3]),
            'zip'     => trim($_address[4]),
            'country' => trim($_address[5]),
        ];
    } else {
        $address = (object)[
            'name'    => trim($_address[0]),
            'address' => trim($_address[1]),
            'address2'=> trim($_address[2]),
            'city'    => trim($_address[3]),
            'state'   => trim($_address[4]),
            'zip'     => trim($_address[5]),
            'country' => trim($_address[6]),
        ];

    }
    //var_dump($address);
    $order = new \Model\Order();

    $order->id                = str_replace(',','',str_replace('$','',trim($data['order'])));
    $order->subtotal          = str_replace(',','',str_replace('$','',trim($data['subtotal'])));
    $order->shipping_charge   = str_replace(',','',str_replace('$','',trim($data['shipping'])));
    $order->tax               = str_replace(',','',str_replace('$','',trim($data['tax'])));
    $order->total             = str_replace(',','',str_replace('$','',trim($data['total'])));
    $order->phone             = trim($phone);
    $order->user_id           = $userId;


    $order->bill_first_name = $order->ship_first_name = $userId->first_name;
    $order->bill_last_name  = $order->ship_last_name  = $userId->last_name;
    $order->bill_address    = $order->ship_address    = $address->address;
    $order->bill_address2   = $order->ship_address2   = $address->address2;
    $order->bill_city       = $order->ship_city       = $address->city;
    $order->bill_state      = $order->ship_state      = $address->state;
    $order->bill_country    = $order->ship_country    = $address->country;
    $order->bill_zip        = $order->ship_zip        = $address->zip;
    $order->email = $data['email'];
    $sql = "INSERT INTO public.\"order\" (id, active, bill_first_name, bill_last_name, bill_address, bill_address2, bill_city, bill_state, bill_country, bill_zip, ship_first_name, ship_last_name, ship_address, ship_address2, ship_city, ship_state, ship_country, ship_zip, subtotal, shipping_cost, tax, total, phone, user_id) ".
        "VALUES ('{$order->id}', 1, '$name[0]', '$name[1]', '$address->address', '$address->address2', '$address->city', '$address->state', '$address->country', '$address->zip', '$name[0]', '$name[1]', '$address->address', '$address->address2', '$address->city', '$address->state', '$address->country', '$address->zip', $order->subtotal,$order->shipping_charge,$order->tax,$order->total,'$order->phone',$order->user_id) ";
    $db->getResults($sql);

    foreach (json_decode($data['product'],true) as $deets){
        $_product = $deets['product'];
        $productName = $_product['name'];
        $productSql = "select id from public.product where name='{$productName}'";
        $existingProduct = $db->getResults($productSql);
        $productId = 0;
        if(!$existingProduct){
            $product = new \Model\Product();
            $productSlug = $productName;
            $productSlug =  preg_replace ( "/[^\w-]/" , '-' , $productSlug);
            $productSlug =  preg_replace ( "/[-]+/" , '-' , $productSlug);
            $productSlug =  strtolower($productSlug);
            $productSlug =  uniqueSlug('\Model\Product',$productSlug);
            $productMsrp  = str_replace('$','',$deets['price']);
            $productPrice  = str_replace('$','',$deets['price']);
            $db->getResults("insert into public.product (active, name, slug, msrp, price) values (1, '{$productName}', '{$productSlug}', '{$productMsrp}', '{$productPrice}')");
            $newProduct = $db->getResults($productSql);
            $productId = $newProduct[0]['id'];
        } else {
            $productId = $existingProduct[0]['id'];
        }

        $order_productDetails    = json_encode(['color'=>strtoupper($_product['color'])]);
        $order_productUnit_price = str_replace('$','',$deets['price']);

        $db->getResults("insert into public.order_products (active, order_id, product_id, details, unit_price, quantity) values (1, {$order->id}, '{$productId}', '{$order_productDetails}', '{$order_productUnit_price}', 1)");
    }

    echo "INSERTED ORDER #".$order->id."\n";
}

function uniqueSlug($model, $slug, $count = 0){
    $list = $model::getList(['where'=>"slug = '".$slug."' "]);
    if (isset($list) && count($list) > 0){
        return uniqueSlug($model, $slug.'-'.++$count, $count);
    } else {
        return $slug;
    }
}