<?php
require(__DIR__.'/../index.php');

$filePaths = [__DIR__.'/order.csv'];
$arrResult = [];
$head = true;
foreach($filePaths as $filePath) {
    $handle = fopen($filePath, 'r');
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            if (empty($data[13]) && strtolower(trim($data[4])) !='shipping' ) {
            }elseif (strtolower($data[18]) == 'refund'){}
            elseif($head){
                $head = false;
            } else {
                $arrResult[] = $data;
            }
        }
        fclose($handle);
        $head = true;
    }
}

for($x = 0; $x < count($arrResult); $x++) {
    $transId = trim($arrResult[$x][13]); //unique
    $quantity = $arrResult[$x][5];
    $ean = $arrResult[$x][7];
    $subtotal = str_replace('$','',$arrResult[$x][9]);
    $subtotal = floatval(str_replace(',','',$subtotal));
    $discount = str_replace('$','',$arrResult[$x][10]);
    $discount = str_replace(',','',$discount);
    $discount = floatval(str_replace('-','',$discount));
    $tax = str_replace('$','',$arrResult[$x][12]);
    $tax = floatval(str_replace(',','',$tax));
    $total = $subtotal - $discount + $tax;
    $payment_id = $arrResult[$x][14];
    $note = $arrResult[$x][16];
    $detail = $arrResult[$x][17];
    $name = trim($arrResult[$x][4]);
    //TODO need to handle refund
//    $event_type = $type[strtolower($arrResult[$x][18])];
    $user_id = $arrResult[$x][21];


    if($user_id){
        $user = \Model\User::getItem(null,['where'=>"lower(square_id) = '".trim(strtolower($user_id))."'"])->id;
    }else{
        $user = null;
    }



    if($ean !='') {
        $product = \Model\Product::getItem(null, ['where' => "lower(ean) = '" . $ean . "'"]);
        if(!$product){
            continue;
        }
    }
    $order = \Model\Order::getItem(null,['where'=>"lower(trans_id) = '".trim(strtolower($transId))."'"])?: new \Model\Order();
    if($order->id == 0){
        $order->note = $note;
        $order->subtotal = $subtotal;
        $order->discount = $discount;
        $order->tax = $tax;
        $order->total = $total;
        $order->trans_id = $transId;
        $order->payment_id = $payment_id;
        $order->detail = $detail;
//        $order->event_type = $detail;
        $order->user_id = $user;
        $order->save();
    }elseif(strtolower($name) == 'shipping'){
        $order->shipping_cost = $total;
        $order->total += $total;
        $order->save();
    }else{
        $order->subtotal += $subtotal;
        $order->discount += $discount;
        $order->tax += $tax;
        $order->total += $total;
        $order->note? : $total;
        $order->save();
    }
//    dd($order);
    $order_id = $order->id;
    if($product){
        $order_prod = \Model\Order_Product::getItem(null,['where'=>"product_id = $product->id and order_id = $order_id"])? : new \Model\Order_Product();
        if($order_prod->id == 0){
            $order_prod->order_id = $order_id;
            $order_prod->product_id = $product->id;
            $order_prod->quantity = intval($quantity);
        }
        $order_prod->save();

    }
    echo "Order : $name saved \n";
}

