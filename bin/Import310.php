<?php
/**
 * Created by PhpStorm.
 * User: Simon
 * Date: 4/17/2017
 * Time: 5:27 PM
 */

require_once(__DIR__."/../libs/Emagid/emagid.php");
require_once(__DIR__."/../conf/emagid.conf.php");
require_once(__DIR__.'/../includes/functions.php');
$emagid = new \Emagid\Emagid($emagid_config);

function import(){
    $filePaths = [
        __DIR__.'/310.csv'
    ];
    $arrResult = [];
    $head = 2;
    foreach ($filePaths as $filePath){
        $handle = fopen($filePath,'r');
        if($handle){
            $start = 0;
            while(($data = fgetcsv($handle,1000,',')) !== FALSE){
                if(empty($data[1]) || $start < $head){
                    $start++;
                } else {
                    $arrResult[] = $data;
                }
            }
            fclose($handle);
            $head = 1;
        }
    }
    $receiving_id = 0;
    $name = $serial = $partNo = $qty = '';
    $ignoreItem = false;
    foreach ($arrResult as $res){
        //If name is not set, set as $res[0], else if name is set and res[0] is not set, remain the same
        $name = $name != '' && $res[0] == '' ? $name: $res[0];
        $serial = $res[1];
        $partNo = $partNo != '' && $res[2] == '' ? $partNo: $res[2];
        $qty = $qty != '' && $res[4] == '' ? $qty: $res[4];

        //If partNo and res[2] exists, create receiving. Else, keep previous receiving_id as foreign key
        $itemOnly = $partNo && $res[2] ? false: true;

        //Create new receiving
        if(!$itemOnly){
            $product = \Model\Product::getItem(null,['where'=>"part_number = '$partNo'"]);
            if($product){
                $receivingArr = ['product_id'=>$product->id,'part_no'=>$partNo,'ean'=>$product->ean,'upc'=>$product->upc,'pi_name'=>$name,'qty'=>filter_var($qty,FILTER_SANITIZE_NUMBER_INT),'unit_price'=>$product->getPrice(),'shipping_date'=>date('Y-m-d H:i:s',time())];
                $receiving = new \Model\Receiving($receivingArr);
                $receiving->save();
                $receiving_id = $receiving->id;
                $ignoreItem = false;
                echo "--- Create Receiving: $product->name ($receiving_id)\n";

                $actual = \Model\Actual_Inventory::getItem(null,['where'=>"product_id = $product->id"]) ? : new \Model\Actual_Inventory();
                $actual->product_id = $product->id;
                $actual->save();
            } else {
                $ignoreItem = true;
                echo "\n--- FAILED RECEIVING: $name\n\n";
            }
        }
        if(validateSerial($serial) && $ignoreItem == false){
            $recItemArr = ['receiving_id'=>$receiving_id,'name'=>$name,'sku'=>$serial,'status'=>2,'inventory'=>'Apt_310'];
            $recItem = new \Model\Receive_Item($recItemArr);
            $recItem->save();
            echo "Create Receiving Item: $recItem->sku ($recItem->id)\n";
        } else if(!validateSerial($serial) && !$ignoreItem && $res[4]){
            for ($i = 0; $i < $qty; $i++){
                $recItemArr = ['receiving_id'=>$receiving_id,'name'=>$name,'sku'=>'','status'=>2,'inventory'=>'Apt_310'];
                $recItem = new \Model\Receive_Item($recItemArr);
                $recItem->save();
            }
            echo "Create Receiving Box: $qty\n";
        }
    }
    foreach (\Model\Actual_Inventory::getList() as $item){
        $total = 0;
        foreach (\Model\Actual_Inventory::$places as $field=>$place){
            $item->$field = $total = \Model\Receive_Item::getCount(['where'=>"receiving_id in (select id from receiving where product_id = $item->product_id) and inventory = '$place'"]);
        }
        $item->total = $total;
        $item->save();
    }
}

function validateSerial($val){
    $lowVal = strtolower($val);
    $notSerialArr = ['box','each','n/a','case','bax'];
    $notSerial = false;

    foreach($notSerialArr as $a) {
        if (stripos($lowVal,$a) !== false || is_numeric($val))
            $notSerial = true;
    }
    if($lowVal == '' || $notSerial)
        return false;
    return true;
}

import();