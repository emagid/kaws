<?php
require(__DIR__.'/../index.php');

$filePaths = [
    'service_ticket' => __DIR__.'/csv/oc_service.csv',
    'service_action' => __DIR__.'/csv/oc_service_action.csv',
    'service_history' => __DIR__.'/csv/oc_service_history.csv',
    'service_status' => __DIR__.'/csv/oc_service_status.csv',
];

$arrResult = [];
$head = false;
foreach($filePaths as $key => $filePath) {
    $handle = fopen($filePath, 'r');
    if ($handle) {
        while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
            $arrResult[$key][] = $data;
        }
        fclose($handle);
        $head = false;
    }
}
global $emagid;
$db = $emagid->getDb();


for($i = 0; $i < count($arrResult['service_status']); $i++) {
    $data = $arrResult['service_status'][$i];
    $x = new \Model\Service_Status();
    $x->id = intval($data[0]);
    $x->name = $data[2];

    $sql = "INSERT INTO service_status (id, name) VALUES ({$x->id}, '{$x->name}')";
    $db->getResults($sql);

    echo "Service Status {$x->id} SAVED \n";
}

for($i = 0; $i < count($arrResult['service_action']); $i++) {
    $data = $arrResult['service_action'][$i];
    $x = new \Model\Service_Action();
    $x->id = $data[0];
    $x->name = $data[2];

    $sql = "INSERT INTO service_action (id, name) VALUES ({$x->id}, '{$x->name}')";
    $db->getResults($sql);

    echo "Service Action {$x->id} SAVED \n";
}

for($i = 0; $i < count($arrResult['service_ticket']); $i++) {
    $data = $arrResult['service_ticket'][$i];
    $x = new \Model\Service_Ticket();
    $x->id = $data[0];
    $x->insert_time = $data[17];
    $x->modified_time = $data[18];
    $x->firstname = $data[4];
    $x->lastname = $data[5];
    $x->email = $data[6];
    $x->phone = $data[7];
    $x->product = $data[8];
    $x->model = $data[9];
    $x->quantity = $data[10];
    $x->service_action_id = $data[13];
    $x->service_status_id = $data[14];
    $x->comment = $data[15];

    $sql = "INSERT INTO service_ticket (id, active, insert_time, modified_time, firstname, lastname, email, phone, product, model, quantity, service_action_id, service_status_id, comment) VALUES ({$x->id}, 1, '{$x->insert_time}', '{$x->modified_time}', '{$x->firstname}', '{$x->lastname}', '{$x->email}', '{$x->phone}', '{$x->product}', '{$x->model}', '{$x->quantity}', '{$x->service_action_id}', '{$x->service_status_id}', '{$x->comment}')";
    $db->getResults($sql);

    echo "Service Ticket {$x->id} SAVED \n";
}

for($i = 0; $i < count($arrResult['service_history']); $i++) {
    $data = $arrResult['service_history'][$i];
    $x = new \Model\Service_Ticket();
    $x->id = $data[0];
    $x->insert_time = $data[5];
    $x->service_ticket_id = $data[1];
    $x->service_status_id = $data[2];
    $x->notify = $data[3];
    $x->comment = $data[4];

    $sql = "INSERT INTO service_history (id, insert_time, service_ticket_id, service_status_id, notify, comment) VALUES ({$x->id}, '{$x->insert_time}', '{$x->service_ticket_id}', '{$x->service_status_id}', '{$x->notify}', '{$x->comment}')";
    $db->getResults($sql);

    echo "Service History {$x->id} SAVED \n";
}