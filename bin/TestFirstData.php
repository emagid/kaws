<?php

require_once(__DIR__."/../libs/Emagid/emagid.php");
require_once(__DIR__."/../conf/emagid.conf.php");
require_once(__DIR__.'/../includes/functions.php');
$emagid = new \Emagid\Emagid($emagid_config);

function createPreAuth(){
    $firstData = new \Payment\FirstData("ME5575-04","2sK8ak8UD4lWJwiWRVcT43c0Fu7pLCee", true);
    $firstData->setApiVersion('v12');
    $firstData->setApiId("404831");
    $firstData->setApiKey("OQl0XDpqygOKzuRlUQgn8oWYxH6ZGtJh");
    $firstData->setTransactionType(\Payment\FirstData::TRAN_PREAUTH);
    $firstData->setCreditCardNumber('4111111111111111');
    $firstData->setCreditCardName('test t');
    $firstData->setCreditCardExpiration('0120');
    $firstData->setAmount('150');

    $firstData->process();
    if($firstData->isError()){
        var_dump($firstData->getErrorMessage(), $firstData->getErrorCode());
    } else {
        var_dump($firstData);
    }
}

function createPreAuthComplete(){
    $firstData = new \Payment\FirstData("ME5575-04","2sK8ak8UD4lWJwiWRVcT43c0Fu7pLCee", true);
    $firstData->setApiVersion('v12');
    $firstData->setApiId("404831");
    $firstData->setApiKey("OQl0XDpqygOKzuRlUQgn8oWYxH6ZGtJh");
    $firstData->setTransactionType(\Payment\FirstData::TRAN_PREAUTHCOMPLETE);
    $firstData->setCreditCardNumber('4111111111111111');
    $firstData->setCreditCardName('test t');
    $firstData->setCreditCardExpiration('0120');
    $firstData->setAmount('50');
    $firstData->setAuthNumber('ET112595');

    $firstData->process();
    if($firstData->isError()){
        var_dump($firstData->getErrorMessage(), $firstData->getErrorCode());
    } else {
        var_dump($firstData);
    }
}

function createTaggedComplete(){
    $firstData = new \Payment\FirstData("ME5575-04","2sK8ak8UD4lWJwiWRVcT43c0Fu7pLCee", true);
    $firstData->setApiVersion('v12');
    $firstData->setApiId("404831");
    $firstData->setApiKey("OQl0XDpqygOKzuRlUQgn8oWYxH6ZGtJh");
    $firstData->setTransactionType(\Payment\FirstData::TRAN_TAGGEDPREAUTHCOMPLETE);
    $firstData->setTransactionTag(143935754);
    $firstData->setAmount('100');
    $firstData->setAuthNumber('ET112595');

    $firstData->process();
    if($firstData->isError()){
        var_dump($firstData->getErrorMessage());
    } else {
        var_dump($firstData);
    }
}

function testRefund(){
    $firstData = new \Payment\FirstData("ME5575-04","2sK8ak8UD4lWJwiWRVcT43c0Fu7pLCee", true);
    $firstData->setApiVersion("v12");
    $firstData->setApiId("404831");
    $firstData->setApiKey("OQl0XDpqygOKzuRlUQgn8oWYxH6ZGtJh");
    $firstData->setTransactionType(\Payment\FirstData::TRAN_REFUND);
    $firstData->setCreditCardNumber("4111111111111111");
    $firstData->setCreditCardName('Fred Flintstone');
    $firstData->setCreditCardExpiration("0918");
    $firstData->setAmount(864);

    $firstData->process();

    if($firstData->isError()){
        var_dump($firstData->getErrorMessage(), $firstData->getErrorCode());
    } else {
        var_dump($firstData);
    }
}
//testRefund();
//createPreAuth();
//createPreAuthComplete();
createTaggedComplete();