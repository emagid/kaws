<?php
date_default_timezone_set('America/New_York');
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
ini_set('display_errors',0);

require_once("libs/Emagid/emagid.php");
require_once("conf/emagid.conf.php");
require_once('includes/functions.php');
require_once('templates/notification_template.php');
//require_once('libs/SquareConnect/autoload.php');
require_once('vendor/autoload.php');
require_once('libs/ShipStation-wrapper/libraries/shipstation/Shipstation.class.php');
require_once('libs/ShipStation-wrapper/libraries/unirest/Unirest.php');
//require_once('libs/GuzzleHttp/functions.php');
$emagid = new \Emagid\Emagid($emagid_config);

if (session_status() == PHP_SESSION_NONE) {
    \session_start();
}
$emagid->loadMvc($site_routes);
