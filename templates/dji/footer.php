<style type="text/css">
   p.overlay_head1{
        font-weight: bold;
        font-size: 18px !important;
        margin-bottom: 17px;
    }

    p.overlay_head{
        font-weight: bold;
        font-size: 15px !important;
        margin-top: 17px;
        margin-bottom: 3px !important;
    }
</style>

<footer>
    <!-- <a id='mailing' href="">Mailing List</a> -->
    <a id='terms' href="">Terms</a>
    <a id='faq' href="">FAQ</a>
    <!-- <a id='track' href="">Track Order</a> -->
    <!-- <a id='cart' href="">Cart</a> -->
</footer>

<section class='mailing popup'>
    <div class='overlay'>
        <div class='off_overlay'></div>
        <div class='overlay_content'>
            <p>Sign up to the mailing list:</p>
            <form>
                <input type="email" name="email" placeholder='email'>
                <input class='button' type="submit">
            </form>
        </div>
    </div>
</section>

<!-- <section class='cart popup'>
    <div class='overlay'>
        <div class='off_overlay'></div>
            <div class='overlay_content'>
                
            </div>
    </div>
</section> -->

<section class="terms popup">
  <div class="overlay">
    <div class="off_overlay"></div>
    <div class="overlay_content">
      <div class="overlay_text">
        <p class="overlay_head1">TERMS OF USE</p>
        <p class="overlay_head">Using this site</p>
        <p>Please use a standards-compliant web browser. Javascript must be enabled to use this site. By using our website, user agree to the Terms of Use. We may change or update these terms so please check this page regularly. We do not represent or warrant that the information on our website is accurate, complete, or current. This includes pricing and availability information. We reserve the right to correct any errors or omissions, and to change or update the information at any time without prior notice.</p>
        <p class="overlay_head">Return Policy</p>
        <p>All sales are final. We do not issue refunds, accept cancellations, or allow exchanges to be made. On a case by case basis under extenuating circumstances, at our discretion, if we determine a refund is warranted, a 5% fee will be deducted from the refund.</p>
        <p class="overlay_head">Purchase for Personal Use Only</p>
        <p>Buyers may purchase products only for personal use and not for resale. By placing your order, you certify that you are purchasing products for personal use only and not for resale. We reserve the right to refuse orders for any reason without explanation.</p>
        <p class="overlay_head">Shipping</p>
        <p>We do not ship to P.O. Boxes or Freight Forwarders. Please allow up to 7 business days for order verification and processing, and an additional 10 business days for delivery.</p>
        <p>All domestic orders are shipped UPS Ground. International orders (to select countries) are shipped UPS Worldwide Expedited. Shipping fees are calculated as flat-rate per country and may include additional fees calculated on a per item basis and determined by order weight. Fees may include an additional handling charge. We are not responsible for any lost, stolen or damaged shipments. Customers are responsible for paying additional shipping and handling fees to have the order re- shipped, if available. The buyer assumes all responsibilities of claims made with the shipping carrier. Please <a href="mailto:support@kawsone.zendesk.com?subject=UPS%20Claims">contact us</a> with any questions regarding UPS claims.</p>
        <p class="overlay_head">International Duty and Tax </p>
        <p>International customers may incur additional charges at the time of checkout. International Duty and Taxes are calculated according to country and included in the order total at the time of checkout. The buyer is responsible to pay for all import Duty and Taxes prior to shipping. We will transfer the fees we collect to UPS to process your international shipment. </p>
        <p class="overlay_head">Sales Tax</p>
        <p>Sales Tax is charged to orders shipped within the states of New York and New Jersey. The applicable tax rate for orders within the state of New York is 8.875%. The applicable tax rate for orders shipped within the state of New Jersey is 6.625%. Shipping cost is also taxed. Sales tax is not charged to orders shipped to states outside of New York and New Jersey. </p>
        <p class="overlay_head">Copyright and Trademarks</p>
        <p>All content of our website including text, graphics, logos, buttons, icons, images and software is property of KAWS INC. and is protected by United States and international copyright laws. The buyer may electronically copy and print hard copies of pages from this web site solely for personal, non-commercial purposes related to placing an order or shopping with KAWSONE. Any other use of our web site, including reproduction and internet links is strictly prohibited without our prior written permission. </p>
        <p class="overlay_head">Secure Processing </p>
        <p>We use secure processing called SSL Encryption Technology, which is the industry standard. SSL (Secure Sockets Layer) is a protocol developed for the transmission of private information over the internet. SSL uses a private key to encrypt the user’s data, including credit card information, so that it cannot be read while being transferred over the internet. </p>
        <p class="overlay_head">Privacy</p>
        <p>We respect our customer’s account information as private and confidential information and will never share it with any outside affiliates or individuals. User information is secured and encrypted with the Secure Sockets Layer Software. The information we collect for orders includes name, e-mail address, billing address, phone number, credit card, internet protocol (IP) address, and geographical region. By providing this information to us, the buyer represents that they own and consent to our use of such personal data. We use this information to verify and process orders, to collect payment and bill for our services, and to contact and respond to buyer’s requests regarding sales and support. At any time, the buyer has the right to withdraw or decline consent. If the buyer does not provide the requested information, we will not be able to provide services to them.</p>
        <p>Users may unsubscribe from receiving promotional or marketing e-mail from KAWSONE at any time by using the “unsubscribe” link in the e-mail received or by <a href="mailto:support@kawsone.zendesk.com?subject=UPS%20Claims">contacting us</a></p>
      </div>
    </div>
  </div>
</section>

<section class="faq popup">
  <div class="overlay">
    <div class="off_overlay"></div>
    <div class="overlay_content">
      <div class="overlay_text">
        <p class="overlay_head1">F.A.Q.</p>
        <p class="overlay_head">I changed my mind. Can I get a refund or make an exchange for a new size/color?</p>
        <p>We do not accept returns for refunds, credits, or exchanges. All sales are final. Please make sure that you have carefully reviewed all information in your order prior to finalizing your purchase, including your billing and shipping address, e-mail and telephone number.</p>
        <p class="overlay_head">Can I track my order?</p>
        <p>All customers receive a shipping confirmation e-mail when their order ships, which includes the UPS tracking number. To track your package, go to the link below, enter your tracking number, and press the "track" button to get shipment details. <a href="https://www.ups.com/WebTracking/track?loc=en_us">https://www.ups.com/WebTracking/track?loc=en_us</a></p>
        <p class="overlay_head">When will I receive my order?</p>
        <p>Please allow up to 7 business days for order processing and verification, and an additional 10 business days for delivery. We do not ship out on Saturdays or Sundays. If your order has not arrived within the aforementioned guidelines, please contact us and we will do our best to track your order for you.</p>
        <p class="overlay_head">What shipping carrier do you use?</p>
        <p>We ship all orders via United Parcel Service.</p>
        <p class="overlay_head">Can I contact you directly with any questions?</p>
        <p>Please visit the MY ORDER page on our website to submit any questions regarding your order. We will try our best to reply to your question(s) within 48 hours.</p>
        <p class="overlay_head">Do you accept check payments? Can I use PayPal?</p>
        <p>We do not accept payments by check, and PayPal is not presently supported on KAWSONE.com. We currently accept Visa or Mastercard payments only.</p>
        <p class="overlay_head">Why does my billing and shipping address have to match?</p>
        <p>We require that your billing and shipping address match as an additional step to prevent credit card fraud.</p>
        <p class="overlay_head">I do not live in the United States. Can I place an order?</p>
        <p>KAWSONE accepts orders from the United States and select International Countries list below: Australia, Austria, Belgium, Canada, China, Czech Republic, Denmark, Finland, France, Germany, Greece, Guam, Hong Kong, Hungary, Iceland, Ireland, Israel, Italy, Japan, South Korea, Liechtenstein, Luxembourg, Netherlands, New Zealand, Norway, Portugal, Puerto Rico, Singapore, Spain, Sweden, Switzerland, Taiwan, Thailand, United Kingdom.</p>
        <p class="overlay_head">Can I order other items that are not offered on the web store?</p>
        <p>We are only offering a select group of items through the web shop at this time. Please add your e-mail to the mailing list to receive product release updates.</p>
        <p class="overlay_head">Do you sell wholesale?</p>
        <p>We do not sell wholesale.</p>
      </div>
    </div>
  </div>
</section>

<section class='track popup'>
    <div class='overlay'>
        <div class='off_overlay'></div>
        <div class='overlay_content'>
            <div class='overlay_text'>
                
                <h1 class='overlay_head1'>Order Management</h1>
                <p class='overlay_head'>Enter the following information to See the progress of your orders</p>
                <form id="orderDetail" action="/orders/order" method="post">
                    <input type="text" name="ref_num" placeholder='Order #'>
                    <label>E-Mail Address</label>
                    <input type="email" name="email" placeholder='email'>
                    <input class='button' type="submit">
                </form>
            </div>
        </div>
    </div>
</section>