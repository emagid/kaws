<style>
    .ui-autocomplete-loading {
        background: white url("https://media.giphy.com/media/xTk9ZvMnbIiIew7IpW/giphy.gif") right center no-repeat;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<? if ($this->emagid->route['controller'] == 'products' || $this->emagid->route['controller'] == 'privatelabel') { ?>
<div class="mainHeader headroom mainHeaderNoScroll mainHeaderClearance" id="mainHeader">
    <? }else{ ?>
    <div class="mainHeader headroom" id="mainHeader">
        <? } ?>
        <div class="row">
            <div class="triggerMenu show770" id="showLeftMenu">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="logoBlock left_float">
                <a class="extend_header">
                    <icon></icon>
                </a>
                <a class="homeLink" href="<?= SITE_URL ?>">
                    <img src="<?= FRONT_IMG ?>logoText.png">
                </a>
            </div>
            <div class="absTransCenter">
                <h2>Sample Sale</h2>
            </div>
            <div class="show770" id="mobileSearchTogglerWrapper">
                <icon class="searchIcon" class="showMobileSearchDropdown"></icon>
            </div>
            <div class="right_float">
                <? if ($this->emagid->route['controller'] == 'products' || $this->emagid->route['controller'] == 'privatelabel') { ?>
                    <div id="shopping_bag_toggle" class="shopping_bag_toggle sidebar_view_toggle" data-sidebar_view="cart">
                        <p style="pointer-events: none" class="">shopping bag</p>
                        <icon style="pointer-events: none" class="media"></icon>
                    </div>
                    <div id="my_account_header_link" class="sidebar_view_toggle" data-sidebar_view="account">
                        <?if($model->user){?>
                        <a href="/account" class="" id="accountOverviewLinkDontOpenSidebar"><p><?=$model->user->full_name()?></p></a>
                        <?}else{?>
                        <p style="pointer-events: none" class="">my account</p>
                        <?}?>
                    </div>
                <? } ?>

            </div>
        </div>
        <div class="searchResultsDropdown">
            <div class="searchResultsContent searchResultsContentDesktop">
                <ul>

                </ul>
            </div>
        </div>
    </div>
    <div class="searchDropdown show770">
        <input type="text" id="headerSearchMobile" class="headerSearch" placeholder="Search">
        <div class="searchResultsDropdown">
            <div class="searchResultsContent searchResultsContentMobile">
                <ul>

                </ul>
            </div>
        </div>
        <a id="cancelMobileSearch">
            <icon>
                <span></span>
                <span></span>
            </icon>
            <p>Close</p>
        </a>
    </div>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        // // var myElement = document.getElementById("mainHeader");
        // // // construct an instance of Headroom, passing the element
        // // var headroom  = new Headroom(myElement,{
        // // 	tolerance : {
        // //         up : 0,
        // //         down : 33
        // //     }
        // // });
        // // // initialise
        // headroom.init();
        function log( message ) {
            $( "<div>" ).text( message ).prependTo( "#log" );
            $( "#log" ).scrollTop( 0 );
        }


        $( "input#headerSearch" ).autocomplete({
              minLength: 2,
              delay:0,
              appendTo:".searchResultsContent.searchResultsContentDesktop > ul",
              source: function (request, response) {
                    $.post('/home/query', {q: request.term}, function(data){
                        response(data);
                    });
                }
            })
            .data( "ui-autocomplete" )._renderItem = function( ul, item ) {

              return $( "<li></li>" )
                .data("item.autocomplete",item)
                .append( "<a href='"+item.value+"'><img src='<?= UPLOAD_URL?>products/"+item.image+"'><p>" + item.label + "</p></a>" )
                .appendTo(ul);
        };   

        $( "input#headerSearchMobile" ).autocomplete({
              minLength: 2,
              delay:0,
              appendTo:".searchResultsContent.searchResultsContentMobile > ul",
              source: function (request, response) {
                    $.post('/home/query', {q: request.term}, function(data){
                        response(data);
                    });
                }
            })
            .data( "ui-autocomplete" )._renderItem = function( ul, item ) {

              return $( "<li></li>" )
                .data("item.autocomplete",item)
                .append( "<a href='"+item.value+"'><img src='<?= UPLOAD_URL?>products/"+item.image+"'><p>" + item.label + "</p></a>" )
                .appendTo(ul);
        };                

    </script>