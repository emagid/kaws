<header>
    <a href="/"><img src="<?= FRONT_IMG ?>kaws_logo.png"></a>
    <!-- <a class="show_cart">CART<span style='opacity: .5; margin-left: 5px;' id="header_cart_total"><?=$model->cart->count?:0 ?></span></a> -->
    <!-- <div id="cart_popout" style="display: none">
        <i class="fa fa-remove"></i>

        <div class="cart_content">
        </div>
        <a class="checkout-btn button" href="/checkout/payment">Go to Checkout</a>
    </div> -->
</header>
<script type ='text/javascript'>
    $(document).ready(function(){
        $('.show_cart').click(function () {
            $.post('/cart/updateCart',{dropdown:true},function (data) {
                if(data.status == 'success'){
                    $('#cart_popout .cart_content').html(data.cartHtml).parent().show();

                    if ( !$('#cart_popout .cart_content .content').is(':visible') ) {
                        $('#cart_popout .cart_content').html('<div class="header_cart_prod"><div class="content"><p style="padding: 20px 0;">No items in cart.</p></div></div>').parent().show();
                    }
                }
            });

        })

        // $('.cartUpdate button:nth-child(3)').click(function(){
        //     // alert('jhi')
        //     if ( !$('#cart_popout .cart_content .content').is(':visible') ) {
        //         $('#cart_popout .cart_content').html('<div class="header_cart_prod"><div class="content"><p style="padding: 20px 0;">No items in cart.</p></div></div>').parent().show();
        //     }
        // });

        
    })
</script>