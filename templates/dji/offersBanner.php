<div class="offersSliderWrapper">
    <div class="offersSwiper">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <p><span>$49 off your order</span><data><em>Use the code MV49</em></data></p>
            </div>
            <div class="swiper-slide">
                <p><span>Free US shipping on all orders</span></p>
            </div>
        </div>
        <div class="offer-swiper-button-prev swiper-button-white"></div>
        <div class="offer-swiper-button-next swiper-button-white"></div>
    </div>
</div>