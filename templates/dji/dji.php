<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>

    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <? if($_SERVER["REQUEST_URI"] == "/corporate/register"){
        $this->configs['Meta Title'] = "Register For a Corporate Account | Camrise, Official DJI Store in NYC";
        $this->configs['Meta Description'] = "Register for a corporate account, submit corporate registration form and join us on the road of redefining industries, Camrise - official DJI store in NYC.";
        $this->configs['Meta Keywords'] = "dji nyc corporate account, dji nyc corporate registration, dji nyc store, dji nyc drone";
    } ?>
    <title>KAWS</title>

    <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">

    <meta name="Keywords" content="<?= $this->configs['Meta Keywords']; ?>">

    <link rel="shortcut icon" height='30px;' href="<?= SITE_URL ?>content/frontend/assets/img/favicon.png">
    <meta property="og:title" content="<?= SITE_NAME ?>"/>

    <meta property="og:type" content="website"/>

    <meta property="og:url" content="<?= SITE_URL ?>"/>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116287744-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116287744-1');
    </script>

    <? require_once('includes.php'); ?>
    <? require_once('includeScripts.php'); ?>
</head>

<?
$bodyClass = $this->emagid->route['controller'];
$bodyClass2 = $this->emagid->route['action'];
?>

    <body class="<?= $bodyClass ?> <?= $bodyClass2 ?>">

        <div class="mainPageContainer row">
            <? require_once('header.php'); ?>


            <?display_notification()?>

            <?php $emagid->controller->renderBody($model); ?>

            
            <? require_once('footer.php'); ?>
        </div>
        <div class="remodal-bg">
             <? require_once('loginModal.php'); ?>
              <? require_once('signupModal.php'); ?>
        </div>

    </body>
</html>