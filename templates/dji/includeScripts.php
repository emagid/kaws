<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js"></script>

<script src="<?=auto_version(FRONT_JS."core.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."remodal/remodal.min.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."swiper/swiper.min.js")?>"></script>
<script src="<?=auto_version(FRONT_LIBS."jquery.sticky.js")?>"></script>
<script src="<?=auto_version(FRONT_JS."slick-lightbox.js")?>"></script>

<? if ($this->emagid->route['controller'] == 'home') { ?>
	<script src="<?=auto_version(FRONT_JS."home.js")?>"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'products') { ?>
	<script src="<?=auto_version(FRONT_JS."product.js")?>"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'accessories') { ?>
	<script src="<?=auto_version(FRONT_JS."product.js")?>"></script>
<? } ?>

<? if ($this->emagid->route['controller'] == 'watch') { ?>
	<script src="<?=auto_version(FRONT_JS."watch.js")?>"></script>
<? } ?>