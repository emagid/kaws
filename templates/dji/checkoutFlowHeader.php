<div class="mainHeader headroom mainHeaderCheckoutFlow" id="mainHeader">
	<div class="row">
		<div class="logoBlock left_float">
			<a class="extend_header">
				<icon></icon>
			</a>
			<a class="homeLink" href="<?=SITE_URL?>">
				<img src="<?=FRONT_IMG?>logoTextWhite.png">
			</a>
		</div>
		<ul class="header_link_list left_float as_r checkoutFlowStepList <?$this->emagid->route['controller']?>">
			<li data-step_name="cart">
				<a href="<?=SITE_URL?>cart">
					<div>
						<p>Shopping Bag</p>
					</div>
				</a>
			</li>
			<li data-step_name="checkemail">
				<div>
					<p>Your Email</p>
				</div>
			</li>
			<li data-step_name="shipping">
				<div>
					<p>Shipping</p>
				</div>
			</li>
			<li data-step_name="payment">
				<div>
					<p>Payment</p>
				</div>
			</li>
		</ul>
		<div class="right_float">
			<a class="shopReturnBtn" href="/">
				<p class="as_r">Return to Store</p>
			</a>
		</div>
	</div>
</div>