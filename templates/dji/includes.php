<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="<?=auto_version(FRONT_CSS."core.css")?>">
<link rel="stylesheet" href="<?=auto_version(FRONT_LIBS."remodal/remodal.css")?>">
<link rel="stylesheet" href="<?=auto_version(FRONT_LIBS."remodal/remodal-default-theme.css")?>">
<link rel="stylesheet" href="<?=auto_version(FRONT_LIBS."swiper/swiper.css")?>">

<link rel="stylesheet" href="<?=auto_version(FRONT_CSS."coreProduct.css")?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.js"></script>
<link rel="stylesheet" href="<?=auto_version(FRONT_JS."slick-lightbox.css")?>">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<script src="<?=auto_version(FRONT_JS."slick-lightbox.js")?>"></script>
<link rel="stylesheet" href="<?= FRONT_CSS ?>font-awesome-4.7.0/css/font-awesome.min.css">


<? if ($this->emagid->route['controller'] == 'home') { ?>

<? } ?>

<? if ($this->emagid->route['controller'] == 'account') { ?>
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."account.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'categories' || $this->emagid->route['controller'] == 'comparisons') { ?>
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."coreCategories.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'comparisons') { ?>
<? } ?>

<? if ($this->emagid->route['controller'] == 'products') { ?>
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."coreAccessories.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'sr') { ?>
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."search_results.css")?>">
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."coreCategories.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'faq' || 'pages') { ?>
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."core_faq.css")?>">
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."coreCategories.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'accessories') { ?>
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."coreAccessories.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'service' || 'about' || 'contact') { ?>
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."service_about.css")?>">
<? } ?>

<? if ($this->emagid->route['controller'] == 'watch') { ?>
     <link rel="stylesheet" href="<?=auto_version(FRONT_CSS."watch.css")?>">
<? } ?>

<? if (($this->emagid->route['controller'] == 'cart')||($this->emagid->route['controller'] == 'checkout') || ($this->emagid->route['controller'] == 'repair_invoice')) { ?>
    <link rel = "stylesheet" type = "text/css" href="<?=auto_version(FRONT_CSS."checkoutFlow.css")?>">
<? } ?>