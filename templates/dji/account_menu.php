<ul class="nav nav-pills myaccount_nav">
	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'user' && (!$this->emagid->route['action']) || $this->emagid->route['action'] == 'index')?'active':''?>"><a href="#">Customer Profile</a></li>
  	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'orders' && $this->emagid->route['action'] == 'index')?'active':''?>"><a href="#">Order History</a></li>
  	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'user' && ($this->emagid->route['action'] == 'payment_methods' || $this->emagid->route['action'] == 'pm_update'))?'active':''?>"><a href="#">Payment Information</a></li>
  	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'user' && ($this->emagid->route['action'] == 'addresses' || $this->emagid->route['action'] == 'ad_update'))?'active':''?>"><a href="#">Stored Addresses</a></li>
  	<li role="presentation" class="<?=($this->emagid->route['controller'] == 'user' && $this->emagid->route['action'] == 'wishlist')?'active':''?>"><a href="#">Wish List</a></li>
</ul>