<div id="responsiveMenu">
	<div class="mainLinksListWrapper">
		<ul>
				<? foreach ($model->categories as $mainCategory) { ?>
					<li>
						<div class="accordionUI accordionUI_leftSidebar">
							<a class="visible">
								<h3 class="as_r"><?=$mainCategory->name?></h3>
								<svg height="16" width="9" class="nui-icon nui-icon-medium-chevron-right " data-reactid=".1oyyz98izgg.1.1.0.$2.0.1.1.0.$=10:0.0.0.0.$2.0.2"><path class="nui-icon-medium-chevron-right-0" d="M8 8L1 1m0 14l7-7" data-reactid=".1oyyz98izgg.1.1.0.$2.0.1.1.0.$=10:0.0.0.0.$2.0.2.0"></path></svg>
							</a>

							<div class="hidden">
								<div class="second-level-back-trigger">
									<svg height="16" width="9" class="nui-icon nui-icon-medium-chevron-left " data-reactid=".1oyyz98izgg.1.1.0.$2.0.1.1.0.$=10:0.0.1.0.0"><path class="nui-icon-medium-chevron-left-0" d="M1 8l7 7M8 1L1 8" data-reactid=".1oyyz98izgg.1.1.0.$2.0.1.1.0.$=10:0.0.1.0.0.0"></path></svg>
									<b><?=$mainCategory->name?></b>
								</div>
								<ul>
									<?if($mainCategory->slug == 'women'){?>
										<li class="leftSidebar_emph"><a href="<?="/collections/{$mainCategory->slug}/new-arrivals"?>">New Arrivals</a></li>
										<li class="leftSidebar_emph"><a href="<?="/collections/{$mainCategory->slug}/all"?>">All Shoes</a></li>
									<?} else {?>
										<li class="leftSidebar_emph"><a href="<?="/collections/{$mainCategory->slug}"?>">Coming Soon</a></li>
									<?}?>
									<?php $sql = "select * from subnav where active = 1 and category_id = {$mainCategory->id} order by column_num ASC, display_order ASC;";
									$subCat = \Model\Subnav::getList(['sql' => "$sql"]);
									$list = []; ?>
									<? foreach ($subCat as $category) {
										$category = \Model\Category::getItem($category->sub_category_id); ?>
										<li>
											<a href="<?= SITE_URL . "collections/$mainCategory->slug/$category->slug" ?>">
												<h5 class="as_l"><?= $category->name ?></h5>
											</a>
										</li>
									<? } ?>
									<?foreach(\Model\Category::getList(['where'=>"parent_category = $mainCategory->id and external_category = 1"]) as $item){?>
									<li>
										<a href="/collections/<?=$mainCategory->slug?>/<?=$item->slug?>"><p><?=$item->name?></p></a>
									</li>
									<?}?>
									<? if ($mainCategory->slug == 'women') { ?>
										<li>
											<a href="/collections/women/bh-br-x-modern-vice"><p>BH&BR x MV</p></a></li>

										<li>
											<a href="/collections/women/sale"><p>Almost Soldout</p></a></li>

										<li>
											<a href="/clearance"><p>Sample Sale</p></a></li>
									<?}?>
								</ul>
							</div>
						</div>
					</li>
				<? } ?>
			<li>
				<a href="<?= SITE_URL . 'collections/women/sale' ?>"><h3 class="as_r">Items on Sale</h3></a>
			</li>
			<li>
				<div class="accordionUI accordionUI_leftSidebar">
					<a class="visible">
						<h3 class="as_r">Our Brand</h3>
						<svg height="16" width="9" class="nui-icon nui-icon-medium-chevron-right " data-reactid=".1oyyz98izgg.1.1.0.$2.0.1.1.0.$=10:0.0.0.0.$2.0.2"><path class="nui-icon-medium-chevron-right-0" d="M8 8L1 1m0 14l7-7" data-reactid=".1oyyz98izgg.1.1.0.$2.0.1.1.0.$=10:0.0.0.0.$2.0.2.0"></path></svg>
					</a>
					<div class="hidden">
						<div class="second-level-back-trigger">
							<svg height="16" width="9" class="nui-icon nui-icon-medium-chevron-left " data-reactid=".1oyyz98izgg.1.1.0.$2.0.1.1.0.$=10:0.0.1.0.0"><path class="nui-icon-medium-chevron-left-0" d="M1 8l7 7M8 1L1 8" data-reactid=".1oyyz98izgg.1.1.0.$2.0.1.1.0.$=10:0.0.1.0.0.0"></path></svg>
							<b>Our Brand</b>
						</div>
						<ul>
                            <li class="bookAppointmentTab leftSidebar_emph">
                                <div>
                                    <a href="<?= SITE_URL ?>nycstore"><p>NYC Store</p></a>
                                </div>
                            </li>
                            <li class="storeNavTab leftSidebar_emph">
                                <div>
                                    <a href="<?= SITE_URL ?>shopinstagram"><p>Shop Insta</p></a>
                                </div>
                            </li>
                            <li class="privateLabelNavTab leftSidebar_emph">
                                <div>
                                    <a href="<?= SITE_URL ?>nycshowroom"><p>Appointments</p></a>
                                </div>
                            </li>
							<li class="aboutLabelNavTab">
								<div>
									<a href="<?= SITE_URL ?>about"><p>About Us</p></a>
								</div>
							</li>
                            <li class="pressLabelNavTab leftSidebar_emph">
                                <div>
                                    <a href="<?= SITE_URL ?>press"><p>Press</p></a>
                                </div>
                            </li>
						</ul>
					</div>
				</div>
			</li>
			<li>
				<a href="/privatelabel"><h3 class="as_r">Private Label</h3></a>
			</li>
			<li>
				<a href="/nycstore"><h3 class="as_r">NYC Store</h3></a>
			</li>
		</ul>
	</div>
	<div class="separator"></div>
	<div class="footer">
		<a class="responsiveMenu-logo-wrapper" href="<?=SITE_URL?>">
			<img src="<?=FRONT_IMG?>logoText.png"/>
		</a>
		<div>
            <div>
                <ul class="socialLinks">
                    <li>
                        <div>
                            <a href="https://www.instagram.com/modernvice/" class="instagram footer_social_icon" style="background-image:url(<?=FRONT_IMG?>instagramIcon.png)"></a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="https://www.pinterest.com/modernvice/" class="pinterest footer_social_icon" style="background-image:url(<?=FRONT_IMG?>pinterestIcon.png)"></a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="https://twitter.com/modernvice" class="twitter footer_social_icon" style="background-image:url(<?=FRONT_IMG?>twitterIcon.png)"></a>
                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="https://www.facebook.com/modernvice" class="facebook footer_social_icon" style="background-image:url(<?=FRONT_IMG?>facebookIcon.png)"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <div>
            	<p class="address">3rd Floor<br>247 West 38th Street<br>New York, 10018</p>
            </div>
			<a href="tel:212 777 1851" class="telStyle"><p class="as_l">212 777 1851</p></a>
		</div>
	</div>
</div>