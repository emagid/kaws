<div class="inventoryFilterBar">
	<div class="content_width_172 row">
		<div class="left_float row">
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions" id="clearFilter"><a><span class="lyon_r"><em><span></span><span></span></em></span><div class="tooltip">Clear all</div></a></div>
			</div>		
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions down_arrow" data-filter_optiontype="color"><a><span class="lyon_r"><em>Color</em></span> <icon></icon></a></div>
				<div class="hidden dropdownContent">
					<div class="filterOption filterOption_color">
						<!-- <div class="right_float">
							<a class="inv_btn tiny_inv_btn">Reset</a>
							<a class="closeFilterMenu cancelIcon">
								<span></span>
							</a>
						</div> -->
						<div class="row">
							<?
		//					$count = count($model->filters->colors);
		//					$count = ($c = count($model->filters->colors)) < 12 ? $c: 12;
							foreach($model->filters->colors as $color){
		//					for($i = 0; $i < $count; $i++){
		//						$color = $model->filters->colors[$i];
								if(isset($_GET['color']) && $_GET['color']){$selectable = explode(',',urldecode($_GET['color']));?>
								<a class="col colorSwatchBox">
									<div class="optionBox <?=in_array($color->id,$selectable) ? 'selected': ''?>">
										<div>
											<div class="media" style="background-image:url('<?=UPLOAD_URL?>colors/<?=$color->swatch?>')"></div>
											<h5 class="as_t"><?=$color->name?></h5>
										</div>
										<input type="checkbox" value="<?=$color->id?>" class="colorFilter" <?=in_array($color->id,$selectable) ? 'checked="checked"': ''?>>
									</div>
								</a>
							<?} else {?>
								<a class="col colorSwatchBox">
									<div class="optionBox">
										<div>
											<div class="media" style="background-image:url('<?=UPLOAD_URL?>colors/<?=$color->swatch?>')"></div>
											<h5 class="as_t"><?=$color->name?></h5>
										</div>
										<input type="checkbox" value="<?=$color->id?>" class="colorFilter">
									</div>
								</a>
							<?}?>
							<?}?>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions down_arrow" data-filter_optiontype="size"><a><span class="lyon_r"><em>Size</em></span> <icon></icon></a></div>
				<div class="hidden dropdownContent">
					<div class="filterOption filterOption_size">
						<!-- <div class="right_float">
							<a class="inv_btn tiny_inv_btn">Reset</a>
							<a class="closeFilterMenu cancelIcon">
								<span></span>
							</a>
						</div> -->
						<div class="row">
							<?foreach($model->filters->size as $size){
								if(isset($_GET['size']) && $_GET['size'] != ''){$sizeList = explode(',',urldecode($_GET['size']));?>
								<a class="col sizeBox">
									<div class="optionBox <?=in_array($size->id,$sizeList) ? 'selected': ''?>">
										<h5 class="as_t"><?=$size->name()?></h5>
										<input type="checkbox" value="<?=$size->id?>" class="sizeFilter" <?=in_array($size->id,$sizeList) ? 'checked="checked"': ''?>>
									</div>
								</a>
							<?} else {?>
								<a class="col sizeBox">
									<div class="optionBox">
										<h5 class="as_t"><?=$size->name()?></h5>
										<input type="checkbox" value="<?=$size->id?>" class="sizeFilter">
									</div>
								</a>
							<?}?>
							<?}?>
						</div>
					</div>
				</div>
			</div>
			<div class="col productsUI-layout-toggler">
				<div class="switch-view" data-view="<?=isset($_GET['view']) && $_GET['view'] == 'grid' ? 'story' : 'grid'?>">
					<div class="centeredContent row centered uiFormatter_exhbitions radioOptionsModule <?=isset($_GET['view']) && $_GET['view'] == 'grid' ? 'Story View' : 'Grid View'?>">
			    		<div class="col ex_uiFormatterOption checkboxUX optionBoxRadio tileIconWrapper" data-uiformat="tiles">
			    			<div class="row">
			    				<span class="col"></span>
			    				<span class="col"></span>
			    			</div>
			    			<div class="row">
			    				<span class="col"></span>
			    				<span class="col"></span>
			    			</div>
			    		</div>
			    		<div class="col ex_uiFormatterOption listIconWrapper optionBoxRadio checkboxUX" data-uiformat="list">
			    			<span></span>
			    			<span></span>
			    			<span></span>
			    		</div>
			    	</div>
				</div>
			</div>
			<div class="col">
				<div class="toggle_dropdown toggleFilterOptions" id="applyFilter"><a><span class="lyon_r"><em>Apply</em></span></a></div>
			</div>

<!--			<div class="col">-->
<!--				<div class="toggle_dropdown toggleFilterOptions down_arrow" data-filter_optiontype="price"><a><span class="lyon_r"><em>Price</em></span> <icon></icon></a></div>-->
<!--			</div>-->
		</div>

	</div>
<!-- 		<div class="activeFilters row">
		<div class="content_width_172 row">
			<div class="right_float">
				<a class="inv_btn tiny_inv_btn">Clear All</a>
			</div>
			<div class="activeFiltersBoxesWrapper">
				<label>Selected Filters:</label>
			</div>
		</div>
	</div> -->
</div>
<script>
	$(document).ready(function(){
		$('.inventoryFilterBar').on('touchstart click','.filterOption .optionBox, .option, .switch-view',function(event){
			var colors = [];
			$.each($('.colorFilter:checked'),function(index,item){
				colors.push(item.value);
			});
			var size = [];
			$.each($('.sizeFilter:checked'),function(index,item){
				size.push(item.value);
			});
			var heel = [];
			$.each($('.heelFilter:checked'),function(index,item){
				heel.push(item.value);
			});
//			var advanced = {};
			var term = '<?=isset($_GET['term']) ? str_replace('+',' ',$_GET['term']): ''?>';
			var sortBy = $(".sort_by_main").data('value');
			var colorStr = colors.join(',');
			var sizeStr = size.join(',');
			var heelStr = heel.join(',');
			var switchView = $('.switch-view').data('view') == 'grid' ? 'story': 'grid';
//			var advancedStr = advanced.join(',');
			var data = {term:term,color:colorStr,size:sizeStr,heel_height:heelStr, sort_by:sortBy, view:switchView};
			$.each($('.advancedFilter'),function(index,item){
				if(item.value != '') {
					data[item.name] = (item.value);
				}
			});
			if($(this).hasClass('switch-view')){
				var val = $(this).data('view');
				data['view'] = val;
			}
			$.each(data,function(key,value){
				if(value == ''){
					delete data[key];
				}
			});
			var queryString;
			if($.param(data)){
				queryString = '?'+ $.param(data);
			} else {
				queryString = '';
			}
			window.location.replace('<?=parse_url($this->emagid->uri)['path']?>'+ queryString);
		});

		$('#applyFilter').on('click',function(){
			var colors = [];
			$.each($('.colorFilter:checked'),function(index,item){
				colors.push(item.value);
			});
			var size = [];
			$.each($('.sizeFilter:checked'),function(index,item){
				size.push(item.value);
			});
			var heel = [];
			$.each($('.heelFilter:checked'),function(index,item){
				heel.push(item.value);
			});
			var term = '<?=isset($_GET['term']) ? $_GET['term']: ''?>';
			var sortBy = $(".sort_by_main").data('value');
			var colorStr = colors.join(',');
			var sizeStr = size.join(',');
			var heelStr = heel.join(',');
			var data = {term:term,color:colorStr,size:sizeStr,heel_height:heelStr, sort_by:sortBy};
			$.each(data,function(key,value){
				if(value == ''){
					delete data[key];
				}
			});
			var queryString;
			if($.param(data)){
				queryString = '?'+ $.param(data);
			} else {
				queryString = '';
			}
			window.location.replace('<?=parse_url($this->emagid->uri)['path']?>'+ queryString);
		});
		$('#clearFilter').on('click',function(){
			window.location.replace("<?=parse_url($this->emagid->uri)['path']?>");
		});

		$(".sort_by_group").on('click', function(){
			$(".sort_by_main")
				.text($(this).text())
				.data('value', $(this).data('value'));

		})
	})
</script>