<div class="emailCaptureModule">
	<div class="media" style="background-image:url('<?=FRONT_IMG?>emailCaptureBackgroundNew.png')"></div>
	<div class="content_width_1000">
		<h1 class="as_m bigger">Join the Party</h1>
		<h1 class="as_m">Get on our mailing list to receive a 10% off coupon via email, and get notified about our upcoming site-launch Labor Day sales.</h1>
		<div class="row row_360">
			<div class="col">
				<form class="mailingListEmailCapture" method="post" action="<?=SITE_URL?>newsletter/add">
					<div class="labelInputWrapper">
						<input type="email" id="mailingListEmail" name="email" placeholder="Email address" required>
						<input type="text" id="form_type" name="form_type" style="display: none;" value="1">
						<input type="submit" value="Get 10% Off" class="btn btn_pink">
					</div>
				</form>
			</div>
<!-- 			<div class="col">
				<h3 class="cortado">and...</h3>
				<div class="bulleth4List">
					<h4 class="as_m">Stay up to date with our newest designs</h4>
					<h4 class="as_m">Be the first to hear about our parties and events</h4>
					<h4 class="as_m">And of course, know about sales before anyone else</h4>
				</div>g
			</div> -->

		</div>
	</div>
</div>

<script>
	var frm = $('.mailingListEmailCapture');
	frm.submit(function (e) {
		$.post(frm.attr('action'),frm.serialize(),function(data){
			if(data.status == 'success'){
				alert(data.message);
			} else{
				alert(data.message);
			}
		})
		e.preventDefault();
	})
</script>