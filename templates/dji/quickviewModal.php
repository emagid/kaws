<div class="mdl mdl_mv mdl_quickView" id="mdl_quickview">
	<!--<div class="newsletterDiscount row">
		<div class="col">
			<h4 class="futura">Join our Newsletter to receive a <span>10%</span> discount on this item</h4>
		</div>
		<div class="col">
			<div class="newsletterSignupInputWrapper">
				<input id="emailAddr" type="email" name="email" placeholder="Your Email" required />
				<input id="redirectTo" value="<?/*=$this->emagid->uri*/?>" />
				<input id = "prodId" type="hidden" name="product_id" >
				<a id="submit" class="btn btn_circle btn_icon btn_pink">Go</a>
			</div>
		</div>
	</div>-->
	<div class="header">
		<div class="right_float">
			<a id="mdl_exit">
		        <icon>
		            <span></span>
		            <span></span>
		        </icon>
		        <p>Close</p>
		    </a>
	    </div>
    </div>
    <div class="bodyContent row quickviewProductContent">
    	<div class="col productMedia">
    		<a class="show_full_product">
	    		<div class="main_media">
	    			<div class="media_enlarged media_enlarged_image media_enlarged_active media_enlarged_1">
	    				<div class="mediaWrapper">
		    				<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_1.jpg)"></div>
		    			</div>
	    			</div>
	    			<div class="media_enlarged media_enlarged_image media_enlarged_2">
	    				<div class="mediaWrapper">
		    				<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_2.jpg)"></div>
		    			</div>
	    			</div>
	    			<div class="media_enlarged media_enlarged_image media_enlarged_3">
	    				<div class="mediaWrapper">
		    				<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_3.jpg)"></div>
		    			</div>
	    			</div>
	    		</div>
    		</a>
    		<div class="media_thumbs">
    			<a class="mediaThumb mediaThumbActive" data-mediaThumb="1">
    				<div class="mediaWrapper">
    					<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_1.jpg)"></div>
    				</div>
    			</a>
    			<a class="mediaThumb" data-mediaThumb="2">
    				<div class="mediaWrapper">
    					<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_2.jpg)"></div>
    				</div>
    			</a>
    			<a class="mediaThumb" data-mediaThumb="3">
    				<div class="mediaWrapper">
    					<div class="media" style="background-image:url(<?=FRONT_IMG?>product_detail_3.jpg)"></div>
    				</div>
    			</a>
    		</div>
    	</div>
    	<div class="col productDataActions">
    		<div class="header">
    			<a>
					<h2 class="as_m quickView_name">Quilted Handler</h2>
				</a>
				<h3 class="as_r quickView_price">$388</h3>
			</div>
			<div class="customizationOption customizationOptionColor accordionUI accordionUI_active">
				<a class="toggleCustomizationOptions visible"><label><span id="color-name">Color: <span></span></span><icon></icon></label></a>
				<div class="hidden customizationOptionsModule color_selection_module radioOptionsModule">
					<div class="row row_of_2" id="color-content">
						<div class="col optionBox optionBoxRadio selected">
							<div class="media colorSwatch" style="background-image:url(<?=FRONT_IMG?>color_swatch_white_snake.png)"></div>
							<p>White Snake</p>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<div class="media colorSwatch" style="background-image:url(<?=FRONT_IMG?>color_swatch_black_snake.png)"></div>
							<p>Black Snake</p>
							<input type="checkbox">
						</div>
					</div>
				</div>
			</div>
			<div class="customizationOption customizationOptionSize accordionUI accordionUI_active">
				<a class="toggleCustomizationOptions visible"><label><span id="size-name">Select a Size</span><icon></icon></label></a>
				<div class="hidden customizationOptionsModule size_selection_module radioOptionsModule">
					<div class="row size_row" id="size-content">
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">6</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">7</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">7.5</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">8</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">8.5</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">9</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">9.5</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">10</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">10.5</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">11</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">11.5</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">12</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">12.5</h3>
							<input type="checkbox">
						</div>
						<div class="col optionBox optionBoxRadio">
							<h3 class="as_r">13</h3>
							<input type="checkbox">
						</div>
					</div>
				</div>
			</div>	
			<div class="add_to_bag_wrapper">
				<div id="madeToOrder">
					<h6 class="cortado">made to order</h6>
					<h5 class="cal_r">Please allow for 15-20 days for this product to ship.</h5>
				</div>
				<a class="btn btn_black addToBag" id="add" data-user_id="<?=$model->user?$model->user->id:0?>">Add to Bag</a>
				<a class="favorite_btn btn btn_gray" data-user_id="<?=$model->user?$model->user->id:0?>">
					<icon>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 241.3 224.7" enable-background="new 0 0 241.3 224.7" xml:space="preserve">
                            <path d="M120.6,34.6c0,0,0.5-0.5,0.8-0.8c9.8-11.2,30.1-30,54.2-30c33.6,0,61.9,28.4,61.9,62c0,22-14.5,50.1-43.3,83.7
                            c-10.8,12.6-23.5,25.8-37.6,39.3c-10,9.5-18.7,17.1-24.2,21.9c-3.4,3-11.9,10.5-11.9,10.5s-8.3-7.5-11.6-10.4
                            C77.2,183.6,3.6,115.4,3.6,65.9c0-33.6,28.3-62,61.9-62c24.2,0,44.4,18.7,54.3,29.9L120.6,34.6z"/>
                        </svg>
                    </icon>
                    <p>Save</p>
				</a>					
			</div>		
			<a class="inv_btn show_full_product">
				Go to full product details
			</a>
    	</div>
    </div>
</div>
<script>
	$(document).ready(function(){
		$(document).on('click','.color_selection_module .row_of_2 .optionBox',function(){
			var color_id = $(this).attr('data-id');
			var product_id = $(this).attr('data-product_id');
			var main = $('.main_media');
			var thumb = $('.media_thumbs');
			var madeToOrder = $('#madeToOrder');
			var quickview_price = $('.quickView_price');
			var addToBag = $('#add');
			$.post('/product/buildImages',{color_id:color_id,product_id:product_id},function(data){
				main.empty();main.append(data.large);
				thumb.empty();thumb.append(data.thumb);
				if(data.madeToOrder){madeToOrder.show()}else{madeToOrder.hide()}
				quickview_price.html("$"+data.price);
				if(data.status){
					addToBag.addClass('addToBag');
					addToBag.html('Add to Bag');
				} else {
					addToBag.removeClass('addToBag');
					addToBag.html('Sold Out');
				}
			})
		});
		$(document).on('click','.newsletterSignupInputWrapper .btn',function () {
			var product_id = $('.addToBag').attr('data-product_id');
			var email = $("#emailAddr").val();
			$('#prodId').val(product_id);
			var form_type = 2;
//			var redirectTo = $('#redirectTo').val();
			if(email && email.indexOf('@')>0){
				$.post('/newsletter/add',{email:email,product_id:product_id,form_type:form_type},function (data) {
					if(data.status == 'success'){
						alert(data.message);
					} else{
						alert(data.message);
					}
				})
			}
		})
	})
</script>