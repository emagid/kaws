<div class="remodal remodal_form" data-remodal-id="modal_signup">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="modalContentWrapper">
	  <form method="post" action="/login/register">
  		<img class='logo' src="<?=FRONT_IMG?>logo_grey.png">
	  	<div class="modal_formWrapper">
	  		<div class="inputRow">
		  		<input name="first_name" type="text" required/>
		  		<label>First Name</label>
			</div>
			<div class="inputRow">
				<input name="last_name" type="text" required/>
				<label>Last Name</label>
			</div>
			<div class="inputRow">
				<input name="phone" type="number" required/>
				<label>Phone Number</label>
			</div>
			<div class="inputRow">
				<input name="email" type="email" required/>
				<label>Email Address</label>
			</div>
			<div class="inputRow">
				<input name="password" type="password" required/>
				<label>Password</label>
			</div>
			<input hidden name="redirect-url" value="<?=$this->emagid->uri?>">
		</div>
	  <button type="submit" class="remodal-confirm">Create Account</button>
	  </form>
  </div>
</div>
<script>
	$(document).ready(function(){
		$('.wholesale_opt_in').on('change',function(){
			if($(this).is(':checked')){
				$('.wholesale_input').show().prop('disabled',false);
			} else {
				$('.wholesale_input').hide().prop('disabled',true);
			}
		})
	});
</script>