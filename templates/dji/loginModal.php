<div class="remodal remodal_form" data-remodal-id="modal_login">
  <button data-remodal-action="close" class="remodal-close"></button>
  <div class="modalContentWrapper">
	  <div id="login-section">
		  <form method="post" action="/login">
			  <img class='logo' src="<?=FRONT_IMG?>logo_grey.png">
			  <div class="modal_formWrapper">
				  <div class="inputRow">
					  <input name="email" type="email" required/>
					  <label>Email Address</label>
				  </div>
				  <div class="inputRow">
					  <input name="password" type="password" required/>
					  <label>Password</label>
				  </div>
			  </div>
			  <input hidden name="redirect-url" value="<?=$this->emagid->uri?>">
			  <button type="submit" class="remodal-confirm">Login</button>
		  </form>
		  <a class="forgot" id="reset-password">Forgot password?</a>
	  </div>
	  <div id="reset-password-section" style="display: none">
		  <p id="reset-password-message" style="color: #5cb85c;display: none">Please check your reset password email</p>
		  <form method="post" action="/reset/email">
			  <div class="modal_formWrapper">
				  <div class="inputRow">
					  <input name="email" type="email" required/>
					  <label>Email Address</label>
				  </div>
			  </div>
			  <button type="submit" class="remodal-confirm">Reset</button>
		  </form>
		  <a class="forgot" id="go-back-login">Back to login</a>
	  </div>
  </div>
</div>
<script>
	$(function(){
		$('#reset-password').on('click', function(e){
			e.preventDefault();
			$('#login-section').hide();
			$('#reset-password-section').show();
		});

		$('#go-back-login').on('click', function(e){
			e.preventDefault();
			$('#login-section').show();
			$('#reset-password-section').hide();
		});



	})
</script>
