<div class="reviewsModule">


	<div class="reviewsSwiper-container">
		<div class="swiper-wrapper">
			<div class="swiper-slide review-slide">
				<div class="mainContentWidth">
					<div class="starRating">
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingHalf.png')"></icon>
					</div>
					<div class="reviewTextWrapper">
						<h5 class="reviewText">Received my battery on the date promised. Charged it right away, checked it out on my DJI-GO IPAD, then made my first flight using it. Everything was as it should be.</h5>
					</div>
					<div class="reviewerNameSource">
						<span class="horizontalSeparator"></span>
						<h5 class="reviewName">Frederick E. <icon>&#x2022;</icon><span>Amazon</span></h5>
					</div>
				</div>
			</div>
			<div class="swiper-slide review-slide">
				<div class="mainContentWidth">
					<div class="starRating">
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingHalf.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingEmpty.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingEmpty.png')"></icon>
					</div>
					<div class="reviewTextWrapper">
						<h5 class="reviewText">Authentic DJI products, fast shipping, affordable prices.</h5>
					</div>
					<div class="reviewerNameSource">
						<span class="horizontalSeparator"></span>
						<h5 class="reviewName">Blaine Pugh <icon>&#x2022;</icon><span>Amazon</span></h5>
					</div>
				</div>
			</div>
			<div class="swiper-slide review-slide">
				<div class="mainContentWidth">
					<div class="starRating">
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingEmpty.png')"></icon>
					</div>
					<div class="reviewTextWrapper">
						<h5 class="reviewText">Speedy delivery in excellent condition and exactly as described. Excellent! Thanks.</h5>
					</div>
					<div class="reviewerNameSource">
						<span class="horizontalSeparator"></span>
						<h5 class="reviewName">M.Reed <icon>&#x2022;</icon><span>Amazon</span></h5>
					</div>
				</div>
			</div>
			<div class="swiper-slide review-slide">
				<div class="mainContentWidth">
					<div class="starRating">
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingHalf.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingEmpty.png')"></icon>
					</div>
					<div class="reviewTextWrapper">
						<h5 class="reviewText">Seller was great! Item arrived as described & the price was unbeatable. There was a small issue when the item arrived, & once I notified the seller, he correcte…</h5>
					</div>
					<div class="reviewerNameSource">
						<span class="horizontalSeparator"></span>
						<h5 class="reviewName">Matt Smithers <icon>&#x2022;</icon><span>Amazon</span></h5>
					</div>
				</div>
			</div>
			<div class="swiper-slide review-slide">
				<div class="mainContentWidth">
					<div class="starRating">
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
						<icon class="djiIcon djiIconStar" style="background-image:url('<?=FRONT_IMG?>starRatingFull.png')"></icon>
					</div>
					<div class="reviewTextWrapper">
						<h5 class="reviewText">Drone perfect, all ok, thanks</h5>
					</div>
					<div class="reviewerNameSource">
						<span class="horizontalSeparator"></span>
						<h5 class="reviewName">Hector G. Garcia Aguirre <icon>&#x2022;</icon><span>Amazon</span></h5>
					</div>
				</div>
			</div>
		</div>
        <div class="swiper-button-prev" style="background-image:url('<?=FRONT_IMG?>productDetailImg_sliderArrow_left.png')"></div>
        <div class="swiper-button-next" style="background-image:url('<?=FRONT_IMG?>productDetailImg_sliderArrow_right.png')"></div>
	</div>


</div>