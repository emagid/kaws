<div class="mainRightSidebar">

	<div class="topSection" style="display:none;visibility: hidden;opacity:0;pointer-events: none;">
		<? if ($this->emagid->route['controller'] == 'products') { ?>
		<?}else{?>

		<?}?>

		<div class="rght" style="display:none;visibility: hidden;opacity:0;pointer-events: none;">
			
		</div>
	</div>
	<div class="activeSection">
		<div id="closeRightSidebar" class="show510">
			<icon style="background-image:url(<?=FRONT_IMG?>chevronLeft.png)"></icon>
		</div>
		<?if(isset($model->cart->cart)&&($model->cart->cart != null)){?>
		<div class="sidebar_view_cart sidebar_view" style="display:none;">
			<div class="header_fixed">
				<a class="btn full_width_btn shoppingBagBtn" href="/cart"><p>View Shopping Bag</p></a>
				<div class="row row_of_2">
					<div class="col subtotal">
						<label class="as_m">subtotal</label>
						<p class="updateSubtotal"><?=$model->cart ? '$'.number_format($model->cart->total - $model->cart->discount,2): 'No items in bag'?></p>
					</div>
				</div>
			</div>
		<? }else{?>
		<div class="sidebar_view_cart sidebar_view sidebar_view_cart_noitems" style="display:none;">
			<div id="closeRightSidebar" class="noItemsCloseSidebarBtn">
				<icon style="background-image:url(<?=FRONT_IMG?>chevronLeft.png)"></icon>
			</div>
			<div class="header_fixed">
				<a class="btn full_width_btn shoppingBagBtn" href="/cart"><p>View Shopping Bag</p></a>
				<div class="row row_of_2">
					<div class="col subtotal">
						<label class="as_m">subtotal</label>
						<p class="updateSubtotal"><?=$model->cart ? '$'.number_format($model->cart->total,2): 'No items in bag'?></p>
					</div>
				</div>
			</div>
		<?}?>
			<div class="floatingCheckoutButton">
				<a class="col btn_oreo shoppingBagBtnCheckbox" href="/checkout/email">
					<p>Checkout</p>
					<icon>
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 2464 184 282" enable-background="new 0 2464 184 282" xml:space="preserve">
							<g>
								<path fill="#ffffff" stroke="#ffffff" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" d="M17.8,2605.1"/>
								<line fill="#ffffff" stroke="#ffffff" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2484.5"/>
								<line fill="#ffffff" stroke="#ffffff" stroke-width="22" stroke-linecap="round" stroke-linejoin="bevel" stroke-miterlimit="10" x1="17.8" y1="2605.1" x2="164" y2="2725.8"/>
							</g>
						</svg>
					</icon>
				</a>
			</div>
			<div class="tabbedView">
				<header class="tabController">
					<div class="title row">
						<h4 class="col as_l shopping_bag_title tab active" data-tab_title="bag"><icon></icon>Shopping Bag <span class="count"><?=$model->cart ? count($model->cart->products): 0?></span></h4>
						<a class="as_l col inv_btn tab favorites" data-tab_title="favorites">
							Saved
							<icon>
								<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="0 0 241.3 224.7" enable-background="new 0 0 241.3 224.7" xml:space="preserve">
	                                <path d="M120.6,34.6c0,0,0.5-0.5,0.8-0.8c9.8-11.2,30.1-30,54.2-30c33.6,0,61.9,28.4,61.9,62c0,22-14.5,50.1-43.3,83.7
	                                c-10.8,12.6-23.5,25.8-37.6,39.3c-10,9.5-18.7,17.1-24.2,21.9c-3.4,3-11.9,10.5-11.9,10.5s-8.3-7.5-11.6-10.4
	                                C77.2,183.6,3.6,115.4,3.6,65.9c0-33.6,28.3-62,61.9-62c24.2,0,44.4,18.7,54.3,29.9L120.6,34.6z"/>
	                            </svg>
							</icon>
						</a>
					</div>
				</header>
				<div class="sidebarBagScrollContainer">
					<div class="shoppingBag_items_wrapper tab_content tab_content_bag tab_content_active">
						<ul class="items">
							<?if(isset($model->cart->cart)&&($model->cart->cart != null)){
								foreach($model->cart->cart as $cart){
									if($cart->product_id){
										$product = \Model\Product::getItem($cart->product_id);
										$color_id = json_decode($cart->variation,true)['color'];
										$size_id = json_decode($cart->variation,true)['size'];
										$defaultcolor = \Model\Color::getItem($color_id);
										$size = \Model\Size::getItem($size_id);

										$url = '/products/' . $product->slug;
										$img = UPLOAD_URL . 'products/' . $product->featuredImage(null,$color_id);
										$name = $product->name;
										$colorText = strlen($defaultcolor->name) < 20 ? $defaultcolor->name: substr($defaultcolor->name,0,20).'...';
										$sObjUs = $size->us_size;
										$sObjEu = $size->eur_size;
										$price = $product->getPrice($color_id,$size_id);
										if($cart->clearance){
											$price = $product->getPrice($color_id,$size_id,true);
											$name .= '(Clearance)';
										}
									} else {
										$detail = json_decode($cart->details,true);
										$gc = \Model\Gift_Card::getItem($detail['gift_card_id']);

										$url = '/gift_cards/product/'.$detail['gift_card_id'];
										$img = UPLOAD_URL.'gift_cards/'.$gc->image;
										$name = $gc->name;
										$colorText = '';
										$sObjUs = '';
										$sObjEu = '';
										$price = number_format($detail['amount'],2);
									}?>
									<li>
										<div class="item_container">
											<div class="bagEditItem">
												<div class="editClose circle_x">
													<circle>
														<span></span>
														<span></span>
													</circle>
												</div>
												<div class="mediaWrapper item_img">
													<a class="cartImg" href="<?=$url?>">
														<img src="<?=$img?>">
													</a>
													<div class="remove_item_cart btn btn_black" data-cart_id="<?= $cart->id?>">
														<p>Remove</p>
													</div>
												</div>
												<div class="item_info">
													<div class="itemName">
														<h4 class="as_l"><?=$name?></h4>
													</div>
													<div class="itemName">
														<h4 class="as_l"><?=$colorText?></h4>
													</div>
													<div class="itemName">
														<h4 class="as_l">US: <?=$sObjUs?> | EU: <?=$sObjEu?></h4>
													</div>
													<div class="product_price">
														<h4 class="as_l">$<?=number_format($price,2)?></h4>
													</div>
												</div>
												<?if($cart->product_id){?>
													<div class="item_info accordionUI">
													<a class="visible btn btn_gray edit_btn">Edit</a>
													<div class="hidden">
														<h4 class="update_cart btn_black btn" data-cart_id="<?=$cart->id?>">Update</h4>
														<div class="customizationOption customizationOptionColor accordionUI accordionUI_active">
															<a class="toggleCustomizationOptions visible"><label><span>Color: <span><?=$defaultcolor->name?></span></span><icon></icon></label></a>
															<div class="hidden customizationOptionsModule color_selection_module selection_module radioOptionsModule">
																<div class="row color-content">
																	<? if ($product->color) {
																		foreach (json_decode($product->color, true) as $item) {
																			$stat = \Model\Product_Attributes::getStatusById($product->id,$item);
																			if (($color = \Model\Color::getItem($item)) && ($stat == null || $stat->value != 3)) {?>
																				<div class="col optionBox optionBoxRadio <?=$color->id == $defaultcolor->id ? 'selected': ''?>">
																					<a href="#">
																						<div class="media colorSwatch"
																							 style="background-image:url(<?=$color->swatch()?>)"></div>
																						<p><?= $color->name ?></p>
																						<input type="checkbox" value="<?= $color->id ?>"
																							   data-color="<?= $color->name ?>" <?=$color->id == $defaultcolor->id ? 'checked="checked"': ''?>>
																					</a>
																				</div>
																			<? } ?>
																		<? } ?>
																	<? } else { ?>
																		<div class="col optionBox optionBoxRadio">
																			<a href="#">
																				<div class="media colorSwatch"
																					 style="background-image:url(<?= FRONT_IMG ?>color_swatch_.png)"></div>
																				<p>Default</p>
																				<input type="checkbox" value="0"
																					   data-color="0">
																			</a>
																		</div>
																	<? } ?>
																</div>
															</div>
														</div>
														<div class="customizationOption customizationOptionSize accordionUI accordionUI_active">
															<a class="toggleCustomizationOptions visible"><label><span id="size-name">Size: <span>EUR <?=floatval($size->eur_size)?></span> <span>US <?=floatval($size->us_size)?></span></span><icon></icon></label></a>
															<div class="hidden customizationOptionsModule size_selection_module radioOptionsModule">
																<!-- <a><p>Size Guide</p></a> -->
																<div class="row row_of_3 size-content">
																	<?foreach($product->getSizes() as $item){
																		if($item != ''){?>
																			<div class="col optionBox optionBoxRadio <?=$item->id == $size->id ? 'selected': ''?>">
																				<!--									<h3 class="as_r">--><?//='US:'.floatval($item->us_size).'|EU:'.floatval($item->eur_size)?><!--</h3>-->
																				<h3 class="as_r"><span>EUR <?=floatval($item->eur_size)?></span> <span>US <?=floatval($item->us_size)?></span></h3>
																				<input type="checkbox" value="<?=$item->id?>" <?=$item->id == $size->id ? 'checked': ''?>>
																			</div>
																		<?}?>
																	<?}?>
																</div>
															</div>
														</div>
														
													</div>
												</div>
												<?}?>
											</div>
										</div>
									</li>
								<?}?>
							<?} else {?>
								<li class="noItemsInCartWrapper">
									<div class="item_container">
										<div class="bagEditItem">
											<div class="item_info">
												<div class="itemName absTransCenter">

													<h4 class="as_l">Your shopping bag<br>is empty</h4>
													<div class="cfBox row shippingBox">
														<icon class="icon col" style="background-image:url('/content/frontend/assets/img/shippingIcon.png')"></icon>
														<p class="as_l">Free 5-day delivery on all in-stock items not made to order.</p>
													</div>													
												</div>
											</div>
										</div>
									</div>
								</li>
							<? } ?>
						</ul>
					</div>
					<div class="shoppingBag_items_wrapper tab_content tab_content_favorites">
						<ul class="items">
							<? if ($model->user && ($fav = \Model\User_Favorite::getList(['where' => "user_id = {$model->user->id}"]))) {
								$uFavorite = array_map(function ($item) {
									return $item->product_id;
								}, $fav);
							} else if (isset($_COOKIE['favorite']) && $uFavorite = json_decode($_COOKIE['favorite'], true)['product']) {
							} else {
								$uFavorite = [];
							}
							foreach ($uFavorite as $uf) {
								$prod = \Model\Product::getItem($uf) ?>
								<li>
									<div class="item_container">
										<div class="bagEditItem">
											<div class="editClose circle_x">
												<circle>
													<span></span>
													<span></span>
												</circle>
											</div>
											<div class="mediaWrapper item_img">
												<a class="cartImg" href="<?= SITE_URL . 'products/' . $prod->slug ?>">
													<img src="<?= UPLOAD_URL . 'products/' . $prod->featuredImage() ?>">
												</a>

												<div class="remove_item_favorite btn btn_black" data-product_id="<?=$uf?>" data-user_id="<?=$model->user?$model->user->id:0?>">
													<p>Remove</p>
												</div>
											</div>
											<div class="item_info">
												<div class="itemName">
													<h4 class="as_l"><?= $prod->name ?></h4>
												</div>
												<div class="product_price">
													<h4 class="as_l">$<?= number_format($prod->getPrice(), 2) ?></h4>
												</div>
											</div>
											<div class="item_edit_options">
												<a class="btn btn_oreo">Add to Bag</a>
											</div>
										</div>
									</div>
								</li>
							<? } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="sidebar_view sidebar_view_account" style="display:none;">
			<div class="tabbedView">
				<header class="tabController">
					<div class="title row">
						<?if($model->user == null){?>
							<h4 class="col as_l shopping_bag_title login_title tab active" data-tab_title="login">Log In</h4>
							<a class="as_l col inv_btn tab register_title" data-tab_title="register">Register</a>
						<?} else {?>
							<h4 id="signout" class="col as_l shopping_bag_title login_title tab active" data-tab_title="login">Sign Out</h4>
						<?}?>
					</div>
				</header>
				<div class="sidebarBagScrollContainer">
					<div class="shoppingBag_items_wrapper tab_content tab_content_login tab_content_active">
						<div class="row loginMainRow">
							<h5 class="as_r"><?=$model->user?'Welcome':'Returning Customer'?></h5>
							<h8 class="as_l"><?=$model->user?$model->user->full_name():'Made in NYC'?></h8>
							<?if(!$model->user){?>
								<div class="formWrapper accountFormWrapper">
									<form method="post" action="/login">
										<div class="row">
											<input name="email" class="dark_text_input" type="text" placeholder="Email address" id="sidebarEmailInput" autocomplete="off">
										</div>
										<div class="row">
											<input name="password" class="dark_text_input" type="password" placeholder="Password" autocomplete="off">
										</div>
										<div class="row">
											<input type="submit" class="btn btn_black btn_full_width" value="Log In">
											<input hidden name="redirect-url" value="<?=$this->emagid->uri?>">
										</div>
										<div class="footer_links">
											<a id="toggleSignupTab">New User? <span>Create an Account</span></a>
											<!-- <a id="closeRightSidebar">Return to Store</a> -->
											<!-- <a>Forgot your Password?</a> -->
										</div>
									</form>
								</div>
							<?} else {?>
								<div class="formWrapper accountFormWrapper">
									<a class="row" href="/account">View Account</a>
								</div>
							<?}?>
						</div>
					</div>
					<?if(!$model->user){?>
						<div class="shoppingBag_items_wrapper tab_content tab_content_register">
							<div class="row loginMainRow">
								<h5 class="as_r">New User</h5>
								<h8 class="as_l">Create an account</h8>
								<div class="separator">
								<p>Create an account to make purchases and enjoy privileged access to exclusive features such as:<br>
Use favorites to save products you love<br>
Complete checkout more quickly<br>
Review order information and tracking
								</div>
								<div class="formWrapper accountFormWrapper">
									<form method="post" action="/login/register">
										<div class="row">
											<div class="row">
												<input name="first_name" class="dark_text_input" type="text" id="firstRegisterField" placeholder="First name">
											</div>
											<div class="row">
												<input name="last_name" class="dark_text_input" id="lname" type="text" placeholder="Last name">
											</div>
											<div class="row">
												<input name="email" class="dark_text_input" type="email" id="email" placeholder="Email address">
											</div>
											<div class="row">
												<input name="phone" class="dark_text_input" type="tel" id="phone" placeholder="Phone number (optional)">
											</div>
											<div class="row">
												<input name="password" class="dark_text_input" type="password" id="password" placeholder="Password">
												<input hidden name="redirect-url" value="<?=$this->emagid->uri?>">
											</div>
										</div>
										<div class="row">
											<input type="submit" class="btn btn_black btn_full_width" value="Create Account">
										</div>
										<div class="footer_links">
											<a id="closeRightSidebar">Return to Store</a>
										</div>
									</form>
								</div>
							</div>
						</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.addCoupon').on('click',function(){
			var container = $(".coupon_container");
			var subtotal = $(".updateSubtotal");
			var code = $('.couponInput').val();
			$.post('/coupon/add',{code:code,subtotal:<?=$model->cart->total?>},function(data){
				var json = $.parseJSON(data);
				if(json.status == 'success'){
					container.html('<span class="couponDiscountAmount">- $'+parseFloat(json.discount).toFixed(2)+'</span> <span class="couponCode">'+json.discountText+'</span>');
					subtotal.html(json.discountTotal);
				} else {
					alert(json.message);
				}
			});
		});
		$('#signout').on('click',function(){
			$.post('/login/logout','',function(){
				window.location.replace('/');
			})
		});
		$('.update_cart').on('click',function(){
			var color_id = $(this).parent().find('.color-content').find('input[type=checkbox]:checked').val();
			var size_id = $(this).parent().find('.size-content').find('input[type=checkbox]:checked').val();
			var cart_id = $(this).attr('data-cart_id');
			$.post('/cart/updateCart',{color_id:color_id,size_id:size_id,cart_id:cart_id},function(data){
				if(data.status == 'success'){
					window.location.replace('<?=$this->emagid->uri?>');
				}
			});
		});
		$(document).on('click','.remove_item_cart',function(){

			var cart_id = $(this).attr('data-cart_id');
			$.post('/cart/deleteItem',{cart_id:cart_id},function(obj){
				if(obj.status=='success'){
					window.location.replace('<?=$this->emagid->uri?>');
				}else{
					alert(obj.message);
				}
			})
		});
		$(document).on('click','.remove_item_favorite',function(){
			var user_id = $(this).attr('data-user_id');
			var product_id = $(this).attr('data-product_id');
			var data = {'user_id': user_id, 'product_id': product_id};
			var th = $('.favorite_btn');
			$.post('/product/changeFavorite', data, function (item) {
				if (item.status == 'success') {
					window.location.replace('<?=$this->emagid->uri?>');

				}else {
					alert(item.message);
				}
			})

		});
	})
</script>
