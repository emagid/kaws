<div class="mdl mdl_mv mdl_notification">
	<div class="header">
		<div class="left_float">
			<p class="as_l" id="notificationText"></p>
		</div>
		<div class="right_float">
			<a id="mdl_exit">
				<icon>
					<span></span>
					<span></span>
				</icon>
				<p>Close</p>
			</a>
		</div>
	</div>
</div>
	