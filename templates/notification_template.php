<?php
/**
 * 
 * Load the notification and remove it from the SESSION variable
 * 
 */
function display_notification() {
  if(isset($_SESSION['notification']))
  {
    try{
      $notification = unserialize($_SESSION['notification']);
      echo "<div class='notification'>" . $notification->getHTML() . "</div>";
      unset($_SESSION['notification']);
    } catch(Exception $e)
    {
      unset($_SESSION['notification']);
    }
  }
}

